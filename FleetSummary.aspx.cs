using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;

public partial class FleetSummary : System.Web.UI.Page
{
    #region VariableDeclaration
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    int startMonth, endMonth;
    private string unitId;
    #endregion


    #region Event Function
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (!Page.IsPostBack)
            {
                BindUnit();
                BindFromMonth();
                BindToMonth();
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;

        }


    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            unitId = Convert.ToString(ddlUnit.SelectedValue);
            if (ddFromMonth.SelectedIndex == 0)
            {
                startMonth = 1;
            }
            else
            {
                startMonth = Convert.ToInt32(ddFromMonth.SelectedValue);
            }
            if (ddToMonth.SelectedIndex == 0)
            {
                endMonth = 12;
            }
            else
            {
                endMonth = Convert.ToInt32(ddToMonth.SelectedValue);
            }
            
            DataTable dtSummary = new DataTable();
            dtSummary = objFleetTrackerSummary.FleetTrackerSummary(unitId, startMonth, endMonth);
            if (dtSummary.Rows.Count > 0)
            {
                lblHeader.Visible = true;
                if (ddlUnit.SelectedItem.ToString() == "All")
                {
                    lblHeader.Text = "Summary All India";
                }
                else
                {
                    lblHeader.Text = "Summary " + ddlUnit.SelectedItem.ToString();
                }
                grvSummary.DataSource = dtSummary;
                grvSummary.DataBind();
                lblMessage.Visible = false;
            }
            else
            {
                DataTable dtSummaryBlank= new DataTable();
                grvSummary.DataSource = dtSummaryBlank;
                grvSummary.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available.";
                lblMessage.ForeColor = Color.Red;

            }

        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }



    }
    #endregion

    #region Helper Function
    private void BindUnit()
    {
        try
        {
            DataTable dtUnit = new DataTable();
            dtUnit = objFleetTrackerSummary.GetUnitName(Convert.ToInt32(Session["UserID"]));
            ddlUnit.DataTextField = "UnitName";
            ddlUnit.DataValueField = "UnitId";
            ddlUnit.DataSource = dtUnit;
            ddlUnit.DataBind();
            ddlUnit.Items.Insert(0,"--Select--");
            ddlUnit.Items.Add(new ListItem("All", "All"));

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }
    private void BindFromMonth()
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddFromMonth.DataTextField = "monthName";
            ddFromMonth.DataValueField = "number";
            ddFromMonth.DataSource = dtFromMonth;
            ddFromMonth.DataBind();
            ddFromMonth.Items.Insert(0,"--Select--");
            //ddFromMonth.Items.Add(new ListItem("0", "--Select--"));

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }


    }
    private void BindToMonth()
    {
        try
        {
            DataTable dtToMonth = new DataTable();
            dtToMonth = objFleetTrackerSummary.GetMonthName();
            ddToMonth.DataTextField = "monthName";
            ddToMonth.DataValueField = "number";
            ddToMonth.DataSource = dtToMonth;
            ddToMonth.DataBind();
            ddToMonth.Items.Insert(0, "--Select--");
            //ddToMonth.Items.Add (new ListItem ("0","--Select--"));

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }

    }

    #endregion

}
