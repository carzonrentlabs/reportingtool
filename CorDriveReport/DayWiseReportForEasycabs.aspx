﻿<%@ Page Title="Day Wise Report For Easycabs" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DayWiseReportForEasycabs.aspx.cs" Inherits="CorDriveReport_DayWiseReportForEasycabs" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 
   <%-- <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>--%>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $("#<%=txtPickUpDate.ClientID%>").datepicker();


            $("#<%=bntGet.ClientID%>").click(function () {

                if (document.getElementById('<%=txtPickUpDate.ClientID%>').value == "") {
                    alert("Please Select  Date");
                    document.getElementById('<%=txtPickUpDate.ClientID%>').focus();
                    return false;
                }
               
                else {
                    ShowProgress();
                }
            });
          
        });


       
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width:1200px">
            <tr>
                <td colspan="5" align="center" bgcolor="#FF6600">
                    <strong>Day Wise Report For Easycabs</strong>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b> PickUp Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   
                    <asp:TextBox ID="txtPickUpDate" runat="server"></asp:TextBox>
                 
                </td>
               
                
                <td style="white-space: nowrap;" align="right">
                 <b> City :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                 
                   <asp:DropDownList ID="ddlCity" runat ="server"> 
                   <asp:ListItem Text="Select City" Value=0></asp:ListItem>
                   <asp:ListItem Text="Delhi" Value="2"></asp:ListItem>
                   <asp:ListItem Text="Mumbai" Value="4"></asp:ListItem>
                   <asp:ListItem Text="Bangalore" Value="7"></asp:ListItem>
                   <asp:ListItem Text="Hyderabad" Value="6"></asp:ListItem>
                   </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
               
            </tr>
             <tr>
                <td colspan="5" align="center">
                 
                      <asp:GridView ID="gvEasycabsDayWiseBooking" runat="server" AutoGenerateColumns="false" Width="60%"> 
                                 <Columns> 
                                 		                          
                                        <asp:BoundField HeaderText="Serial" DataField="Serial" /> 
                                        <asp:BoundField HeaderText="Title" DataField="title" /> 
                                        <asp:BoundField HeaderText="Count" DataField="count" /> 
                                                                             
                                        
                                       
                                    </Columns>
                                  
                                                                   
                     </asp:GridView>
                   
                </td>
            </tr>

            
             <tr>
            
                <td colspan="5" align="center" bgcolor="#FF6600">
                    <strong>Bookings Created by Users</strong>
                </td>
            </tr>
             <tr>
                <td colspan="5" align="center">
                 
                      <asp:GridView ID="gvCreatedByUser" runat="server" AutoGenerateColumns="false" Width="60%"> 
                                 <Columns> 
                                 		<asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                        <asp:BoundField HeaderText="LoginId" DataField="LoginId" /> 
                                        <asp:BoundField HeaderText="Retail Booking" DataField="retail_booking" /> 
                                        <asp:BoundField HeaderText="Corp Booking" DataField="corp_booking" /> 
                                                  
                                        
                                       
                                    </Columns>
                                 
                                                                   
                     </asp:GridView>
                   
                </td>
            </tr>
           
        </table>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
