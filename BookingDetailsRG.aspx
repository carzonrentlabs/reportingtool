﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingDetailsRG.aspx.cs"
    Inherits="BookingDetailsRG" MasterPageFile="~/MasterPage.master" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <link href="DatePickerCSS/ui.all.css" rel="stylesheet" type="text/css" />
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFrom.ClientID%>").datepicker();
            $("#<%=txtTo.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>
     <table border="0" align="center" cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td align="center" colspan="5">
                    <strong><u>Booking Details</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top" style="height: 172px">
                    From Date
                </td>
                <td valign="top" style="height: 172px">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="right" valign="top" style="height: 172px">
                    To Date
                </td>
                <td valign="top" style="height: 172px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td style="height: 172px" valign="top">
                    Location
                </td>
                <td style="width: 183px; height: 172px;" valign="top">
                  <%--  <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList>--%>
                    <asp:ListBox ID="ddlCityName" runat="server" Width="181px" SelectionMode="Multiple" Height="168px"></asp:ListBox>
                </td>
                <td style="height: 172px">
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:Literal runat="server" ID="ltrStudentDetails"></asp:Literal>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    <asp:Label ID="lblValues" runat="server" Text=""></asp:Label></td>
            </tr>
        </tbody>
    </table>

    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
