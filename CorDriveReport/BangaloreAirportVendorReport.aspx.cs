using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;
using System.Drawing;
public partial class CorDriveReport_BangaloreAirportVendorReport : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsExportToExcel = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Text = string.Empty;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(5000);
        DataSet dsBangaloreAirport=new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
                return;
            }
            else
            {

                dsBangaloreAirport = objCordrive.GetBangaloreAirportVendorReport(26, Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                if (dsBangaloreAirport.Tables[0].Rows.Count > 0)
                {
                    grdBangaloreAirport.DataSource = dsBangaloreAirport.Tables[0];
                    grdBangaloreAirport.DataBind();
                }
                else
                {
                    grdBangaloreAirport.DataSource = null;
                    grdBangaloreAirport.DataBind();
                    lblMessage.Text = "Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsExportToExcel = objCordrive.GetBangaloreAirportVendorReport(26, Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
            WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "BangaloreAirportVendorReport.xls");
        }
    }
}
