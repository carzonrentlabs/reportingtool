﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SMSServiceProvider.aspx.cs"
    Inherits="SMSServiceProvider" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="smsProvider" ContentPlaceHolderID="cphPage" runat="server">
    
    <script type="text/javascript">
        function validate() {
            if (document.getElementById('<%=txtProviderName.ClientID %>').value == "" && document.getElementById('<%=txtProviderName.ClientID %>').style.display=="block") {
                alert("Enter provider name.");
                document.getElementById('<%=txtProviderName.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtProviderUrl.ClientID %>').value == "") {
                alert("Enter provider Url.");
                document.getElementById('<%=txtProviderUrl.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtProviderUserName.ClientID %>').value == "") {
                alert("Enter provider user name.");
                document.getElementById('<%=txtProviderUserName.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtProviderPassword.ClientID %>').value == "") {
                alert("Enter provider password.");
                document.getElementById('<%=txtProviderPassword.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtGenderGSM.ClientID %>').value == "") {
                alert("Enter gender GSM.");
                document.getElementById('<%=txtGenderGSM.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlPriority.ClientID %>').selectedIndex == 0) {
                alert("Select priority.");
                document.getElementById('<%=ddlPriority.ClientID %>').focus();
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <center>
        <table cellpadding="2" cellspacing="2" width="100%" border="0">
            <tr>
                <td align="center" colspan="2">
                    <strong><u>Add/Edit SMS Service Provider</u></strong>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Select Option</b>
                </td>
                <td align="left">
                    <asp:RadioButtonList ID="rdAddEdit" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdAddEdit_SelectedIndexChanged"
                        AutoPostBack="true">
                        <asp:ListItem Value="1" Selected="True"><b>Add</b></asp:ListItem>
                        <asp:ListItem Value="0"><b>Edit</b></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Provider Name</b>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtProviderName" runat="server" Width="20%"  style="display:block"  ></asp:TextBox>
                   <%-- <asp:RequiredFieldValidator ID="rfvProviderName" runat="server" ControlToValidate="txtProviderName"
                        Display="Dynamic" ErrorMessage="Enter provider name" ForeColor="Red" SetFocusOnError ="true"></asp:RequiredFieldValidator>--%>
                    <asp:DropDownList ID="ddlProviderName" runat="server" style="display:none"  AutoPostBack="true"
                        OnSelectedIndexChanged="ddlProviderName_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Provider Url Example</b>
                </td>
                <td align="left" style="white-space: true">
                    <asp:Label ID="lblProviderExample" runat="server" ForeColor="green" Text="http://websiteaddress/wip/sendsms?username=@UserName&password=@Password&to=@Mobile&message=@SMSString&msgid=@MSGID"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Provider Url</b>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtProviderUrl" runat="server" Width="50%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvProviderUrl" runat="server" ControlToValidate="txtProviderUrl"
                        Display="Static" ErrorMessage="Enter provider Url" ForeColor="Red" SetFocusOnError ="true"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Provider User Name (Id)</b>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtProviderUserName" runat="server" Width="20%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rvfProviderUserName" runat="server" ControlToValidate="txtProviderUserName"
                        Display="Static" ErrorMessage="Enter provider user name" ForeColor="Red" SetFocusOnError ="true"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Provider Password</b>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtProviderPassword" runat="server" Width="20%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvProviderPassword" runat="server" ControlToValidate="txtProviderPassword"
                        Display="Static" ErrorMessage="Enter provider user name" ForeColor="Red" SetFocusOnError ="true"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Gender GSM</b>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtGenderGSM" runat="server" Width="20%" Text="CRZRNT"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvGenderGSM" runat="server" ControlToValidate="txtGenderGSM"
                        Display="Static" ErrorMessage="Enter provider user name" ForeColor="Red" SetFocusOnError ="true"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Active</b>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlActive" runat="server">
                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Inactive" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <b>Priority</b>
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlPriority" runat="server">
                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                    </asp:DropDownList>
                   <asp:RequiredFieldValidator ID="rfvPriority" runat ="server" ControlToValidate ="ddlPriority" InitialValue ="0" Display ="Static" 
                    ErrorMessage ="Select priority" SetFocusOnError ="true" >
                   
                   </asp:RequiredFieldValidator>
                   
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="bntSubmit" runat="server" Text="Submit" 
                        onclick="bntSubmit_Click" />
                </td>
                <td align="left">
                    <asp:Button ID="bntReset" runat="server" Text="Reset" OnClientClick="this.form.reset();return false;"
                        CausesValidation="false" />
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
