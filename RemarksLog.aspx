<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RemarksLog.aspx.cs" Inherits="RemarksLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Remarks Log</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table cellpadding ="2" cellspacing ="2" border ="0">
                <tr>
                    <td align="center" colspan="2" class="TableTD" valign="top">
                        <asp:Label ID="lblMsg" runat="server" />
                    </td>
                </tr>
                
                <tr>
                    <td align="center" colspan="2" class="TableTD" valign="top">
                        <asp:GridView ID="grdRemarksLog" runat="server" />
                    </td>
                </tr>
                
            </table>
    </div>
    </form>
</body>
</html>
