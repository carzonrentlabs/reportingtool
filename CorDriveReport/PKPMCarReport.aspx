﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PKPMCarReport.aspx.cs" Inherits="CorDriveReport_PKPMCarReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <div>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="4" align="center">
                    <b>PKPM Car Report</b>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblCityName" runat="server">City Name</asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlCity" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblToDate" runat="server">To date</asp:Label>&nbsp;&nbsp;
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblCarModel" runat="server">Attachment Model</asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlCarModel" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblVDP" runat="server">VDP (Y/N)</asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlVdpYN" runat="server">
                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                        <asp:ListItem Value="2" Text="No"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                    <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="Repeater">
        <asp:Repeater ID="rptPKPM" runat="server">
            <HeaderTemplate>
                <table id="sort_table" width="100%">
                    <thead>
                        <tr>
                            <th align="center">
                                Sr.No
                            </th>
                            <th align="center">
                                Car No
                            </th>
                            <th align="center">
                                City Name
                            </th>
                            <th align="center">
                                Car Category
                            </th>
                            <th align="center">
                                Model Name
                            </th>
                            <th align="center">
                                Vendor name
                            </th>
                            <th align="center">
                                Attachment Mode
                            </th>
                            <th align="center">
                                VDP (Y/N)
                            </th>
                            <th align="center">
                                Total Login Hours
                            </th>
                            <th align="center">
                                Total Billed KM
                            </th>
                            <th align="center">
                                Total Billed Hours
                            </th>
                            <th align="center">
                                Duties Done MTD
                            </th>
                            <th align="center">
                                Duties Compleated on COR Drive
                            </th>
                            <th align="center">
                                Total Billed Revenue
                            </th>
                            <th align="center">
                                Open Duties
                            </th>
                            <th align="center">
                                Traget Revenue
                            </th>
                            <th align="center">
                                Short Fall / Excess
                            </th>
                            <th align="center">
                                Car Status
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr id="row_<%#Eval("Id")%>" style="font-family: Arial, Verdana, Sans-Serif;">
                    <td>
                        <%#Eval("Id")%>
                    </td>
                    <td style="text-align: left;white-space :nowrap;">
                        <%#Eval("RegnNo")%>
                    </td>
                    <td style="text-align: left;white-space :nowrap;">
                        <%#Eval("Location")%>
                    </td>
                    <td style="text-align: left;">
                        <%#Eval("CarCatName")%>
                    </td>
                    <td style="text-align: left;">
                        <%#Eval("CarModelName")%>
                    </td>
                    <td style="text-align: left;">
                        <%#Eval("VendorName")%>
                    </td>
                    <td style="text-align: left;">
                        <%#Eval("CarModelName")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("VDPYN")%>
                    </td>
                    <td>
                        <%#Eval("TotalLoginHr")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("TotalKMUsed")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("TotalHrUsed")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("DutyDoneMTD")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("DutyCompleteCorDrive")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("Revenue")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("OpenDuties")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("TargetRevenue")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("shortRevenue")%>
                    </td>
                    <td style="text-align: center;">
                        <%#Eval("CarStatus")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody></table>
            </FooterTemplate>
        </asp:Repeater>
        <div align="center" id="dvPaging" runat="server" visible="false">
            <asp:LinkButton ID="lnkPrev" runat="server" Text="Prev" Font-Bold="true" BackColor="LightBlue"
                OnClick="lnkPrev_Click"></asp:LinkButton>&nbsp;
            <asp:LinkButton ID="lnkNext" runat="server" Text="Next" Font-Bold="true" BackColor="LightBlue"
                OnClick="lnkNext_Click" ></asp:LinkButton>
        </div>
    </div>
</asp:Content>
