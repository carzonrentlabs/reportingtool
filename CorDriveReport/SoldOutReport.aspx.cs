﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using System.Globalization;
public partial class CorDriveReport_SoldOutReport : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }

    private void BindBrachDropDownlist()
    {
        objCordrive = new CorDrive();
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "UnitName";
                    ddlBranch.DataValueField = "UnitId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    protected void bntGet_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(txtFromDate.Text) || string.IsNullOrEmpty(txtToDate.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date and to date should not be blank')", true);
            return;
        }
        else
        {

            StringBuilder strData = new StringBuilder();
            strData = BindExcelData();
            ltlSummary.Text = strData.ToString();
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "SoldOutReport";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();

    }

    private StringBuilder BindExcelData()
    {
        StringBuilder strData = new StringBuilder();
        DateTime fromDate = Convert.ToDateTime(txtFromDate.Text.ToString());
        DateTime toDate = Convert.ToDateTime(txtToDate.Text.ToString());
        objCordrive = new CorDrive();
        DataSet dsDetails = objCordrive.GetSoldOutReport(Convert.ToInt16(ddlBranch.SelectedValue), fromDate, toDate);
        DataView dv = new DataView(dsDetails.Tables[0]);
        try
        {
            if (dsDetails.Tables[0].Rows.Count > 0)
            {
                strData.Append(" <html xmlns:o='urn:schemas-microsoft-com:office:office' ");
                strData.Append(" xmlns:x='urn:schemas-microsoft-com:office:excel' ");
                strData.Append(" xmlns='http://www.w3.org/TR/REC-html40'> ");
                strData.Append(" <head> ");
                strData.Append(" <meta http-equiv=Content-Type content='text/html; charset=windows-1252'> ");
                strData.Append(" <meta name=ProgId content=Excel.Sheet> ");
                strData.Append(" <meta name=Generator content='Microsoft Excel 14'> ");
                strData.Append(" <link rel=File-List href='SoldOut_files/filelist.xml'> ");
                strData.Append(" <style id='SoldOut_10428_Styles'> ");
                strData.Append(" <!--table ");
                strData.Append(" {mso-displayed-decimal-separator:; ");
                strData.Append(" mso-displayed-thousand-separator:;} ");
                strData.Append(" .xl1510428 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:general; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6310428 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:top; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");

                strData.Append(" .xl6410428 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:top; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");

                strData.Append(" .xl6510428 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:'Short Date'; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:top; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6610428 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:'General Date'; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:top; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" --> ");
                strData.Append(" </style> ");
                strData.Append(" </head> ");
                strData.Append(" <body> ");
                strData.Append(" <!--[if !excel]>&nbsp;&nbsp;<![endif]--> ");
                strData.Append(" <!--The following information was generated by Microsoft Excel's Publish as Web ");
                strData.Append(" Page wizard.--> ");
                strData.Append(" <!--If the same item is republished from Excel, all information between the DIV ");
                strData.Append(" tags will be replaced.--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <div id='SoldOut_10428' align=center x:publishsource='Excel'> ");
                strData.Append(" <table border=1 cellpadding=0 cellspacing=0 width=984 style='border-collapse: ");
                strData.Append(" collapse;table-layout:fixed;width:740pt'> ");
                strData.Append(" <col width=73 style='mso-width-source:userset;mso-width-alt:2669;width:55pt'> ");
                strData.Append(" <col width=106 style='mso-width-source:userset;mso-width-alt:3876;width:150pt'> ");
                strData.Append(" <col width=103 style='mso-width-source:userset;mso-width-alt:3766;width:77pt'> ");
                strData.Append(" <col width=97 style='mso-width-source:userset;mso-width-alt:3547;width:100pt'> ");
                strData.Append(" <col width=73 style='mso-width-source:userset;mso-width-alt:2669;width:55pt'> ");
                strData.Append(" <col width=104 style='mso-width-source:userset;mso-width-alt:3803;width:100pt'> ");
                strData.Append(" <col width=56 style='mso-width-source:userset;mso-width-alt:2048;width:42pt'> ");
                strData.Append(" <col width=146 style='mso-width-source:userset;mso-width-alt:5339;width:120pt'> ");
                strData.Append(" <col width=97 style='mso-width-source:userset;mso-width-alt:3547;width:120pt'> ");
                strData.Append(" <col width=52 style='mso-width-source:userset;mso-width-alt:1901;width:120pt'> ");
                strData.Append(" <col width=77 style='mso-width-source:userset;mso-width-alt:2816;width:58pt'> ");
                strData.Append(" <tr height=20 style='height:15.0pt'> ");
                strData.Append(" <td height=20 class=xl6310428 width=73 style='height:15.0pt;width:55pt'>SoldOut ");
                strData.Append(" ID</td> ");
                strData.Append(" <td class=xl6310428 width=106 style='border-left:none;width:80pt'>Unit Name</td> ");
                strData.Append(" <td class=xl6310428 width=103 style='border-left:none;width:77pt'>Category ");
                strData.Append(" Name</td> ");
                strData.Append(" <td class=xl6310428 width=97 style='border-left:none;width:73pt'>Date From</td> ");
                strData.Append(" <td class=xl6310428 width=73 style='border-left:none;width:55pt'>From Time</td> ");
                strData.Append(" <td class=xl6310428 width=104 style='border-left:none;width:78pt'>Date To</td> ");
                strData.Append(" <td class=xl6310428 width=56 style='border-left:none;width:42pt'>To Time</td> ");
                strData.Append(" <td class=xl6310428 width=146 style='border-left:none;width:110pt'>Create ");
                strData.Append(" Date</td> ");
                strData.Append(" <td class=xl6310428 width=97 style='border-left:none;width:73pt'>Modify Date</td> ");
                strData.Append(" <td class=xl6310428 width=52 style='border-left:none;width:39pt'>User ID</td> ");
                strData.Append(" <td class=xl6310428 width=77 style='border-left:none;width:58pt'>Total Hours</td> ");
                strData.Append(" </tr> ");

                DateTime dt;
                if (DateTime.TryParseExact("24/01/2013","d/M/yyyy",CultureInfo.InvariantCulture,DateTimeStyles.None,out dt))

                foreach (DataRow dr in dsDetails.Tables[0].Rows)
                {
                    strData.Append(" <tr height=20 style='height:15.0pt'> ");
                    strData.Append(" <td height=20 style='text-align:center;'>" + dr["SouldOutId"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["UnitName"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["CarCatName"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["DateFrom"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["TimeFrom"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["DateTo"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["TimeTo"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["CreatedDate"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["ModifyDate"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["LoginID"].ToString() + "</td> ");
                    strData.Append(" <td style='text-align:center;'>" + dr["Hourss"].ToString() + "</td> ");
                    strData.Append(" </tr> ");
                }
                strData.Append(" <![if supportMisalignedColumns]> ");
                strData.Append(" <tr height=0 style='display:none'> ");
                strData.Append(" <td width=73 style='width:55pt'></td> ");
                strData.Append(" <td width=106 style='width:80pt'></td> ");
                strData.Append(" <td width=103 style='width:77pt'></td> ");
                strData.Append(" <td width=97 style='width:73pt'></td> ");
                strData.Append(" <td width=73 style='width:55pt'></td> ");
                strData.Append(" <td width=104 style='width:78pt'></td> ");
                strData.Append(" <td width=56 style='width:42pt'></td> ");
                strData.Append(" <td width=146 style='width:110pt'></td> ");
                strData.Append(" <td width=97 style='width:73pt'></td> ");
                strData.Append(" <td width=52 style='width:39pt'></td> ");
                strData.Append(" <td width=77 style='width:58pt'></td> ");
                strData.Append(" </tr> ");
                strData.Append(" <![endif]> ");
                strData.Append(" </table> ");
                strData.Append(" </div> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" </body> ");
                strData.Append(" </html> ");

            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return strData;
    }

}