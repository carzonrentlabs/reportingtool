<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CorDriveInvoiceValidation.aspx.cs" Inherits="CorDriveReport_CorDriveInvoiceValidation"
    Title="Cor drive validation report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="../JQuery/jquery.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });
        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
            else if (Date.parse(document.getElementById('<%=txtFromDate.ClientID %>').value)> Date.parse(document.getElementById('<%=txtToDate.ClientID %>').value))
            {
                alert("From date should not be greater than to date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>

    <center>
        <div style="text-align: center">
            <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
                <tr>
                    <td colspan="5" align="center">
                        <b>Cor Drive Invoice Validation</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblCityName" runat="server">City Name</asp:Label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlCity" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblToDate" runat="server">To date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;
                        <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="text-align: center">
            <asp:GridView ID="grvValidation" runat="server" AutoGenerateColumns="False" BackColor="White"
                BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" AllowPaging="true"
                PageSize="100" OnPageIndexChanging="grvValidation_PageIndexChanging">
                <RowStyle BackColor="White" ForeColor="#330099" />
                <Columns>
                    <asp:BoundField HeaderText="BookingID" DataField="BookingId"  />
                    <asp:BoundField HeaderText="COR Drive Booking" DataField="CorDriveBooking" />
                    <asp:BoundField HeaderText="Billing Basis" DataField="BillingBasis" />
                    <asp:BoundField HeaderText="Client Name" DataField="ClientCoName" />
                    <asp:BoundField HeaderText="Guest Name" DataField="GuestName" />
                    <asp:BoundField HeaderText="Service " DataField="Service" />
                    <asp:BoundField HeaderText="Pickup City" DataField="CityName" />
                    <asp:BoundField HeaderText="Cor Drive PickUp Time" DataField="CorMeterPickupTime" />
                    <asp:BoundField HeaderText="Insta PickUp Time" DataField="InstaPickUpTime" />
                    <asp:BoundField HeaderText="Cor Drive Drop Off Time" DataField="CorMeterDropOffTime" />
                    <asp:BoundField HeaderText="Insta Drop Off Time" DataField="InstaDropOffTime" />
                    
                    <asp:BoundField HeaderText="Gps Garage To Pickup Time" DataField="GpsGarageToPickupTime" />
                    <asp:BoundField HeaderText="Gps Drop To Garage Time" DataField="GpsDropToGarageTime" />
                    <asp:BoundField HeaderText="Total Garage Time" DataField="GarageDistanceTime" />
                    
                    <asp:BoundField HeaderText="Gps Garage To PickUp Distance" DataField="GpsGarageToPickupDistance" />
                    <asp:BoundField HeaderText="Gps Drop To Garage Distance" DataField="GpsDropToGarageDistance" />
                    <asp:BoundField HeaderText="Total Garage Distance" DataField="GPSdistance" />
                    
                    <asp:BoundField HeaderText="Gps Customer Time" DataField="CorMeterCustomerTime" />
                    <asp:BoundField HeaderText="Gps Customer Distance" DataField="GPSToalDistance" />
                    
                    <asp:BoundField HeaderText="KMIn" DataField="KMin" />
                    <asp:BoundField HeaderText="KMOut" DataField="KMout" />
                    <asp:BoundField HeaderText="Time Out" DataField="TimeOut" />
                    <asp:BoundField HeaderText="Time In" DataField="TimeIN" />
                    <asp:BoundField HeaderText="Total Hrs." DataField="TotalHrsUsed" />
                    <asp:BoundField HeaderText="Total KM" DataField="TotalKM" />
                    <asp:BoundField HeaderText="Actual Amount" DataField="TotalCost" />
                    <asp:BoundField HeaderText="Model" DataField="CarModelName" />
                    <asp:BoundField HeaderText="Booked On" DataField="BookedOn" />
                    <asp:BoundField HeaderText="Car" DataField="CarRegNo" />
                    <asp:BoundField HeaderText="Chauffeur/Mobile No" DataField="chauffeurName"  HtmlEncode ="false" />
                    <asp:BoundField HeaderText="Date Out" DataField="DSMadeON" />
                    <asp:BoundField HeaderText="DS Made On" DataField="DSMadeOn" />
                    <asp:BoundField HeaderText="Pickup date" DataField="Createdate" />
                    <asp:BoundField HeaderText="Vendor Name/Vendor Mobile No" DataField="VendorMobileNO" HtmlEncode ="false"/>
                    <asp:BoundField HeaderText="Status" DataField="Staus" />
                    <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" />
                    <asp:BoundField HeaderText="OutStation" DataField="OutstationYN" />
                    <asp:BoundField HeaderText="HDFCLinkStatus" DataField="HDFCLinkStatus" />
                    <asp:BoundField HeaderText="Closed By" DataField="userName" />
                    <asp:BoundField HeaderText="Closed By Branch Name" DataField="unitname" />
                    
                    
                </Columns>
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            </asp:GridView>
        </div>
    </center>
</asp:Content>
