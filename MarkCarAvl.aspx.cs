﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ReportingTool;
public partial class MarkCarAvl : System.Web.UI.Page
{
    clsAutomation objst = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //    if (Convert.ToString(txtRegno.Text).Length!=8)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Invalid Regno. input at least 8 characters!');", true);
        //        return;
        //    }

        DataSet ds = new DataSet();
        ds=objst.GetAvalibilityStatus(txtRegno.Text.Trim(), Convert.ToDateTime(txtTo.Text), Convert.ToInt32(Session["Userid"]));

        if(Convert.ToString(ds.Tables[0].Rows[0][0]) == "2")
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Invalid Regno.');", true);
            return;
        }
        if (Convert.ToString(ds.Tables[0].Rows[0][0]) == "3")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('car not active');", true);
            return;
        }
        if (Convert.ToString(ds.Tables[0].Rows[0][0]) == "4")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('car already marked available');", true);
            return;
        }
        if (Convert.ToString(ds.Tables[0].Rows[0][0]) == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Submitted successfully');", true);
            return;
        }


    }
}