﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SalesRetails.aspx.cs" Inherits="SalesReport_SalesRetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=ddlFromMonth.ClientID %>').value == "") {
                    alert("Please select the from month");
                    document.getElementById('<%=ddlFromMonth.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=ddlFromYear.ClientID %>').value == "") {
                    alert("Please Select the from year");
                    document.getElementById('<%=ddlFromYear.ClientID %>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
        });
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
    </script>
    <div align="center">
        <b>Branch Wise Monthly Revenue Report Cor-Retail</b>
    </div>
    <br />
    <div>
        <table cellpadding="0" cellspacing="0" border="0" align="center">
            <tr>
                <%--<td align="left">
                    <b>From</b>&nbsp;&nbsp;
                </td>--%>
                <td align="left">
                    &nbsp;&nbsp;Month
                    <asp:DropDownList ID="ddlFromMonth" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;Year
                    <asp:DropDownList ID="ddlFromYear" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <%--<td align="left">
                    &nbsp;&nbsp; <b>To</b>&nbsp;&nbsp;
                </td>--%>
                <%--<td align="left">
                    <asp:DropDownList ID="ddlToMonth" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlToYear" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>--%>
                <td align="left">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <asp:Literal ID="ltlSummary" runat="server"></asp:Literal>
    </div>
    <div class="loading" align="center">
        Loading. Please wait.<br />
        <br />
        <img src="../images/loader.gif" alt="" />
    </div>
</asp:Content>
