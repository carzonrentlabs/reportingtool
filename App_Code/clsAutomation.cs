using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;

/// <summary>
/// Summary description for clsAutomation
/// </summary>

namespace ReportingTool
{
    public class clsAutomation
    {
        public clsAutomation()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable GetMonthName()
        {
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Get_MonthName");
        }
        public DataTable GetUnitName(int userId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UserId", userId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_GetUnitNameandID", ObjparamUser);
        }
        public DataTable GetTotalNumberofDays(int day)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@myDateTime", day);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_GetNumDaysInMonth", ObjparamUser);
        }
        public DataTable GetDriverAvailabilityTracker(int userId, int unitId, int month)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@UserId", userId);
            ObjparamUser[1] = new SqlParameter("@UnitId", unitId);
            ObjparamUser[2] = new SqlParameter("@Month", month);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_DriverAvailabeTracker", ObjparamUser);

        }
        public int InsertDriverAvailabilityTracker(int carId, int teamId, int status, int unitId, int monthP, DateTime statusDate, int userId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];
            ObjparamUser[0] = new SqlParameter("@CarId", carId);
            ObjparamUser[1] = new SqlParameter("@Team", teamId);
            ObjparamUser[2] = new SqlParameter("@Status", status);
            ObjparamUser[3] = new SqlParameter("@UnitId", unitId);
            ObjparamUser[4] = new SqlParameter("@MonthP", monthP);
            ObjparamUser[5] = new SqlParameter("@SatatusDate", statusDate);
            ObjparamUser[6] = new SqlParameter("@UserId", userId);
            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_DriveAvailabilityTracker", ObjparamUser);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Pr_DriverInsertUpdateTracker", ObjparamUser);
        }
        public DataTable PresentDriverTracker(int userId, int unitId, int month)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@UserId", userId);
            ObjparamUser[1] = new SqlParameter("@UnitId", unitId);
            ObjparamUser[2] = new SqlParameter("@Month", month);
            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_DriverAvailabeTrackerPresent", ObjparamUser);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_DriverPresentTracker", ObjparamUser);
        }
        public DataTable CheckPresent(int unitid, int month, DateTime statusDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@UnitId", unitid);
            ObjparamUser[1] = new SqlParameter("@MonthPresent", month);
            ObjparamUser[2] = new SqlParameter("@StatusDate", statusDate);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_CheckPresented", ObjparamUser);
        }
        public DataTable GetFleetTracker(int unitId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@UnitId", unitId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_FleetTrakerAailable", ObjparamUser);
        }
        public int InsertFleetTracker(int carId, string reason, DateTime attendanceDate, string remark, int userId, int monthPresent, int unitId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[7];
            ObjparamUser[0] = new SqlParameter("@CarId", carId);
            ObjparamUser[1] = new SqlParameter("@Reason", reason);
            ObjparamUser[2] = new SqlParameter("@UnitId", unitId);
            ObjparamUser[3] = new SqlParameter("@MonthPresent", monthPresent);
            ObjparamUser[4] = new SqlParameter("@AttendanceDate", attendanceDate);
            ObjparamUser[5] = new SqlParameter("@userId", userId);
            ObjparamUser[6] = new SqlParameter("@Remark", remark);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Pr_FleetInsertUpdateTracker", ObjparamUser);
        }
        public DataTable GetPresentFleetTracker(int userId, int unitId, int month)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@UserId", userId);
            ObjparamUser[1] = new SqlParameter("@UnitId", unitId);
            ObjparamUser[2] = new SqlParameter("@Month", month);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Pr_FleetTrackerPresent", ObjparamUser);
        }
        public DataTable FleetTrackerSummary(string unitId, int startMonth, int endMonth)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@UnitId", unitId);
            ObjparamUser[1] = new SqlParameter("@Frommonth", startMonth);
            ObjparamUser[2] = new SqlParameter("@ToMonth", endMonth);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_FleetTrackerSummary", ObjparamUser);

        }
        public DataSet GetCityName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCity");
        }
        public DataSet GetLocations_UserAccessWise(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise", ObjParam);
        }

        public DataSet FillClient()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_getCorpCompanyNames");
        }

        public DataSet GetMonthlyRevenueReport(string dateFrom
            , string dateTo, int cityid, int ClientID
            , int businessType, int CheckBox, int GSTBulkReport
            , int GSTManualReport)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];
            ObjparamUser[0] = new SqlParameter("@FromDate", dateFrom);
            ObjparamUser[1] = new SqlParameter("@ToDate", dateTo);
            ObjparamUser[2] = new SqlParameter("@CityID", cityid);
            ObjparamUser[3] = new SqlParameter("@ClientID", ClientID);

            if (businessType == 3)
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MegaCabMonthlyRevenueReport", ObjparamUser);
            }
            else if (businessType == 2)
            {
                if (CheckBox == 1)
                {
                    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "EasyCabMonthlyRevenueReport", ObjparamUser);
                }
                else
                {
                    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Monthly_Revenue_Report_1", ObjparamUser);
                }
            }
            else
            {
                if (GSTManualReport == 1)
                {
                    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Monthly_Revenue_Report_BulkBatchManual", ObjparamUser);
                }
                else
                {
                    if (GSTBulkReport == 1)
                    {
                        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Monthly_Revenue_Report_BulkBatch", ObjparamUser);
                    }   
                    else
                    {
                        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Monthly_Revenue_Report", ObjparamUser);
                    }
                }
            }
        }

        public DataSet GetExtraAmt(int BookingID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PrcGetExtraAmt", param);
        }

        public DataSet GetExtraAmtColumn()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PrcGetExtraColumns");
        }

        public DataTable GetBranchIncentiveBudget(int fromMonth, int fromYear, int toMonth, int toYear, int userId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];
            ObjparamUser[0] = new SqlParameter("@MonthFrom", fromMonth);
            ObjparamUser[1] = new SqlParameter("@YearFrom", fromYear);
            ObjparamUser[2] = new SqlParameter("@MonthTo", toMonth);
            ObjparamUser[3] = new SqlParameter("@YearTo", toYear);
            ObjparamUser[4] = new SqlParameter("@UserId", userId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_BranchIncetiveBudget", ObjparamUser);
        }
        public DataTable GetBookingDetails(int bookingId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingId", bookingId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_BookingDetails", ObjparamUser);
        }
        public DataTable GetDivision(int ClientCoId) //for sub
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
            ObjparamUser[1] = new SqlParameter("@Flag", 4);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_MiSBookingDetails", ObjparamUser);
        }
        public DataTable GetBrand(int ClientCoId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
            ObjparamUser[1] = new SqlParameter("@Flag", 1);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_MiSBookingDetails", ObjparamUser);
        }
        public DataTable GetLocation(int ClientCoId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
            ObjparamUser[1] = new SqlParameter("@Flag", 2);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_MiSBookingDetails", ObjparamUser);
        }
        public int UpdateDetails(int BookingID, int SubEntityID, int UserID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@BookingID", BookingID);
            ObjparamUser[1] = new SqlParameter("@SubEntityID", SubEntityID);
            ObjparamUser[2] = new SqlParameter("@UserID", UserID);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateMiSBookingDetails", ObjparamUser);
        }
        public DataTable SSCBatchCreationWithinTimeInterval(string fromdate, string todate, int location)
        {
            SqlParameter[] objparamUser = new SqlParameter[3];
            objparamUser[0] = new SqlParameter("@FromDate", fromdate);
            objparamUser[1] = new SqlParameter("@Todate", todate);
            objparamUser[2] = new SqlParameter("@location", location);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_BatchCreationHourlyBasis_17Jan", objparamUser);
        }
        public DataTable SSCBatchAWBDetails(string fromdate, string todate, string location)
        {
            SqlParameter[] objParamUser = new SqlParameter[3];
            objParamUser[0] = new SqlParameter("@DateFrom", fromdate);
            objParamUser[1] = new SqlParameter("@DateTo", todate);
            objParamUser[2] = new SqlParameter("@Location", location);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_SSCBatchCreationDetailsAWB", objParamUser);
        }
        public DataTable GetMandatoryFields(int bookingId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingId", bookingId);
            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetMiscellaneousField", ObjparamUser);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetMiscellaneousField_New", ObjparamUser);
        }

        public DataTable CheckUploadEmailVRFRequired(int bookingId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingId", bookingId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_IsEmailVRFRequired", ObjparamUser);
        }

        public int GetFileIndex(int bookingId,int fileType)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@BookingId", bookingId);
            ObjparamUser[1] = new SqlParameter("@fileType", fileType);
            return (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_FileIndex", ObjparamUser);
        }

        public int AddReplaceEmailVRFFile(int bookingId,string FileName,int sysUserId, string fileType,int actID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];
            ObjparamUser[0] = new SqlParameter("@BookingId", bookingId);
            ObjparamUser[1] = new SqlParameter("@FileName", FileName);
            ObjparamUser[2] = new SqlParameter("@SysUserID", sysUserId);
            ObjparamUser[3] = new SqlParameter("@FileType", fileType);
            ObjparamUser[4] = new SqlParameter("@ActID", actID);
            return (int)SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_AddReplaceEmailVRFFile", ObjparamUser);
        }
        public int UpdateEmailVRFDocUPdate(int bookingId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingID", bookingId);          
            return (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_EmailVRFFileUpdate", ObjparamUser);
        }
        
        public DataTable GetMandatoryFields(int ClientCoId, int MisID, int cityID, string fieldName)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            if (ClientCoId == 727 || ClientCoId == 822 || ClientCoId == 862 || ClientCoId == 863 || ClientCoId == 1417)
            {
                if (fieldName == "Misc2")
                {
                    ObjparamUser[0] = new SqlParameter("@CityId", cityID);
                    return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_MapFlightNumber", ObjparamUser);
                }
                else
                {
                    ObjparamUser[0] = new SqlParameter("@ClientCoid", ClientCoId);
                    ObjparamUser[1] = new SqlParameter("@MiscID", MisID);
                    return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_BrandName", ObjparamUser);
                }
            }
            else if (ClientCoId == 1363 || ClientCoId == 1434)
                if (fieldName == "Misc3")
                {
                    ObjparamUser[0] = new SqlParameter("@MiscID", MisID);
                    return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_UserBookingLocation", ObjparamUser);
                }
                else
                {
                    ObjparamUser[0] = new SqlParameter("@ClientCoid", ClientCoId);
                    ObjparamUser[1] = new SqlParameter("@MiscID", MisID);
                    return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_BrandName", ObjparamUser);
                }

            else
            {
                ObjparamUser[0] = new SqlParameter("@ClientCoid", ClientCoId);
                ObjparamUser[1] = new SqlParameter("@MiscID", MisID);
                return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_BrandName", ObjparamUser);
            }



        }
        public int UpdateMiscellaneousField(Miscellaneous objMis)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[17];
            ObjparamUser[0] = new SqlParameter("@BookingId", objMis.BookingID);
            ObjparamUser[1] = new SqlParameter("@SysUserId", objMis.SysUserId);
            ObjparamUser[2] = new SqlParameter("@Miscellaneous1", objMis.Miscellaneous1);
            ObjparamUser[3] = new SqlParameter("@Miscellaneous2", objMis.Miscellaneous2);
            ObjparamUser[4] = new SqlParameter("@Miscellaneous3", objMis.Miscellaneous3);
            ObjparamUser[5] = new SqlParameter("@Miscellaneous4", objMis.Miscellaneous4);
            ObjparamUser[6] = new SqlParameter("@Miscellaneous5", objMis.Miscellaneous5);
            ObjparamUser[7] = new SqlParameter("@Miscellaneous6", objMis.Miscellaneous6);
            ObjparamUser[8] = new SqlParameter("@Miscellaneous7", objMis.Miscellaneous7);
            ObjparamUser[9] = new SqlParameter("@Miscellaneous8", objMis.Miscellaneous8);
            ObjparamUser[10] = new SqlParameter("@Miscellaneous9", objMis.Miscellaneous9);
            ObjparamUser[11] = new SqlParameter("@Miscellaneous10", objMis.Miscellaneous10);
            ObjparamUser[12] = new SqlParameter("@Miscellaneous11", objMis.Miscellaneous11);
            ObjparamUser[13] = new SqlParameter("@Miscellaneous12", objMis.Miscellaneous12);
            ObjparamUser[14] = new SqlParameter("@Miscellaneous13", objMis.Miscellaneous13);
            ObjparamUser[15] = new SqlParameter("@Miscellaneous14", objMis.Miscellaneous14);
            ObjparamUser[16] = new SqlParameter("@Miscellaneous15", objMis.Miscellaneous15);

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UPdateMiscellaneousField", ObjparamUser);
        }

        public DataSet GetLocations_UserAccessWise_Unit(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_Unit", ObjParam);
        }

        public DataSet GetDailyProductivityReport(int unitId)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@unitId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = unitId;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ClosedBookingByCallCenter", ObjParam);
        }
        public DataSet GetAdvanceReport(string dateFrom, string dateTo)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@Fromdate", dateFrom);
            ObjparamUser[1] = new SqlParameter("@ToDate", dateTo);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ReceiptDetails", ObjparamUser);
        }

        public DataSet GetAdvanceReport(string BookingIDs)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@BookingID", BookingIDs);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetAdvanceBookingDetails", ObjparamUser);
        }

        public DataSet GetSubStaus(int Bookingid, int subid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@Bookingid", Bookingid);
            ObjparamUser[1] = new SqlParameter("@subid", subid);
            //SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "prc_validateSubsidiary", ObjparamUser);
            // bool s = dr.Read();
            //if (s && Convert.ToInt32(dr[0]).Equals(1))
            // {
            //     return 1;
            // }
            // else
            // {
            //     return 0;
            // }
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_validateSubsidiary", ObjparamUser);
        }
        public DataSet GetAvalibilityStatus(string carRegno, DateTime dutyDate,int createdBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@carRegno", carRegno);
            ObjparamUser[1] = new SqlParameter("@dutyDate", dutyDate);
            ObjparamUser[2] = new SqlParameter("@createdBy", createdBy);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_MarkCarAvl", ObjparamUser);
        }

        public DataTable GetClientMandatoryFields(int clientCoID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@ClientCoID", clientCoID);          
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetMandatoryFieldsForClient", ObjparamUser);
        }
        
    }
}