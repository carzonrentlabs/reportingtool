<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DutySlipCollection.aspx.cs" Inherits="CorDriveReport_DutySlipCollection"
    Title="Duty Slip/Parking Collection Receiving Module" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();

            $("#<%=btnExport.ClientID%>,#<%=bntViewDetails.ClientID%>").click(function () {

                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });

        });


        function EnableDisableParkingReceipts() {
            var chkParkingReceipts = document.getElementById('<%=chkParkingReceipts.ClientID %>');
            var txtParkingReceipts = document.getElementById('<%=txtParkingReceipts.ClientID %>');
            txtParkingReceipts.disabled = !chkParkingReceipts.checked;
        }

        function EnableDisableTollReceipts() {
            var chkTollReceipts = document.getElementById('<%=chkTollReceipts.ClientID %>');
            var txtTollReceipts = document.getElementById('<%=txtTollReceipts.ClientID %>');
            txtTollReceipts.disabled = !chkTollReceipts.checked;
        }

        function EnableDisableInterStateReceipts() {
            var chkInterStateReceipts = document.getElementById('<%=chkInterStateReceipts.ClientID %>');
            var txtInterStateReceipts = document.getElementById('<%=txtInterStateReceipts.ClientID %>');
            txtInterStateReceipts.disabled = !chkInterStateReceipts.checked;
        }
    </script>
    <center>
        <table cellpadding="1" cellspacing="1" border="1" style="width: 80%">
            <tr>
                <td colspan="3" align="center" bgcolor="#FF6600">
                    <b>Duty slip/parking Collection</b>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="False" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;" align="center" colspan="3">
                    <b>Booking Id :</b>&nbsp;&nbsp;<asp:TextBox ID="txtBookingId" runat="server"></asp:TextBox>
                    <asp:Button ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click"
                        Font-Bold="True" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Booking Id"
                        ControlToValidate="txtBookingId"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;" align="right">
                    <b>Booking Status :</b>&nbsp;&nbsp;<asp:Label ID="lblBookingStatus" runat="server" />
                </td>
                <td style="white-space: nowrap;" align="right">
                    <b>PickUpCity :</b>&nbsp;&nbsp;<asp:Label ID="lblPickUpCity" runat="server" />
                </td>
                <td style="white-space: nowrap;" align="right">
                    <b>PickUp Date Time :</b>&nbsp;&nbsp;<asp:Label ID="lblPickUpDateTime" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap; height: 20px;" align="right">
                    <b>Dispatched Car :</b>&nbsp;&nbsp;<asp:Label ID="lblDispatchedCar" runat="server" />
                </td>
                <td style="white-space: nowrap; height: 20px;" align="right">
                    <b>Dispatched Chauffeur :</b>&nbsp;&nbsp;<asp:Label ID="lblDispatchedChauffeur" runat="server" />
                </td>
                <td style="white-space: nowrap; height: 20px;" align="right">
                    <b>CorDrive Status :</b>&nbsp;&nbsp;<asp:Label ID="lblCorDriveStatus" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap; height: 20px;" align="right">
                    <b>Client Name :</b>&nbsp;&nbsp;<asp:Label ID="lblClientName" runat="server" />
                </td>
                <td style="white-space: nowrap; height: 20px;" align="right">
                    <b>Guest Name :</b>&nbsp;&nbsp;<asp:Label ID="lblGuestName" runat="server" />
                </td>
                <td style="white-space: nowrap; height: 20px;" align="right">
                    <b>Closed By :</b>&nbsp;&nbsp;<asp:Label ID="lblClosedBy" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <table style="width: 600px">
                        <tr>
                            <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                                <b>Created By </b>
                            </td>
                            <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                                <b>Created Date </b>
                            </td>
                            <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                                <b>Modified By </b>
                            </td>
                            <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                                <b>Modified Date </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap;" align="center">
                                <asp:Label ID="lblCreatedBy" runat="server" />
                            </td>
                            <td style="white-space: nowrap;" align="center">
                                <asp:Label ID="lblCreatedDate" runat="server" />
                            </td>
                            <td style="white-space: nowrap;" align="center">
                                <asp:Label ID="lblModifyBy" runat="server" />
                            </td>
                            <td style="white-space: nowrap;" align="center">
                                <asp:Label ID="lblModifyDate" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;" align="center" bgcolor="#FF6600">
                    <b>Document Type </b>
                </td>
                <td style="white-space: nowrap;" align="center" bgcolor="#FF6600">
                    <b>Collected(Y/N) </b>
                </td>
                <td style="white-space: nowrap;" align="center" bgcolor="#FF6600">
                    <b>Amount(If Any) </b>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;" align="right">
                    <b>Parking Receipts :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="center">
                    <asp:CheckBox ID="chkParkingReceipts" runat="server" onclick="EnableDisableParkingReceipts()" />
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtParkingReceipts" runat="server" BorderStyle="Solid" BorderWidth="1px"
                        Enabled="false" MaxLength="4" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtParkingReceipts"
                        runat="server" ErrorMessage="Only Numbers Allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap; height: 24px;" align="right">
                    <b>Toll Receipts :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap; height: 24px;" align="center">
                    <asp:CheckBox ID="chkTollReceipts" runat="server" onclick="EnableDisableTollReceipts()" />
                </td>
                <td style="white-space: nowrap; height: 24px;" align="left">
                    <asp:TextBox ID="txtTollReceipts" runat="server" BorderStyle="Solid" BorderWidth="1px"
                        Enabled="false" MaxLength="4" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtTollReceipts"
                        runat="server" ErrorMessage="Only Numbers Allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;" align="right">
                    <b>InterState Receipts :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="center">
                    <asp:CheckBox ID="chkInterStateReceipts" runat="server" onclick="EnableDisableInterStateReceipts()" />
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtInterStateReceipts" runat="server" BorderStyle="Solid" BorderWidth="1px"
                        Enabled="false" MaxLength="4" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtInterStateReceipts"
                        runat="server" ErrorMessage="Only Numbers Allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap; height: 24px;" align="right">
                    <b>Paper Duty Slip :</b>
                </td>
                <td style="white-space: nowrap; height: 24px;" align="center">
                    <asp:CheckBox ID="chkPaperDutySlip" runat="server" />
                </td>
                <td style="white-space: nowrap; height: 24px;" align="left">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" style="white-space: nowrap" colspan="3">
                    <asp:Button ID="btnSave" runat="server" Text="Save" Font-Bold="True" OnClick="btnSave_Click" />&nbsp;&nbsp;
                    <asp:Label ID="lblmessage" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table cellpadding="5" cellspacing="0" border="0">
            <tr>
                <td style="white-space: nowrap">
                    From Date :
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                    To Date :
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                    User :
                    <asp:DropDownList ID="ddlUserName" runat="server"></asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="bntViewDetails" runat="server" Text="Veiw Details" CausesValidation="false"
                        OnClick="bntViewDetails_Click" />
                </td>
                <td>
                    <asp:Button ID="btnExport" runat="server" Text="Export to excell" CausesValidation="false"
                        OnClick="btnExport_Click" />
                </td>
            </tr>
        </table>
        <div id="detailShow">
            <asp:GridView ID="gvShowDetails" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="BookingId" HeaderText="BookingId" />
                    <asp:BoundField DataField="ParkingReceipts" HeaderText="Parking Receipts" />
                    <asp:BoundField DataField="TollReceipts" HeaderText="Toll Receipts" />
                    <asp:BoundField DataField="InterStateReceipts" HeaderText="InterState Receipts" />
                    <asp:BoundField DataField="PaperDutySlip" HeaderText="Paper DutySlip" />
                    <asp:BoundField DataField="CreatedDate" HeaderText="Create Date" />
                    <asp:BoundField DataField="CreatedBy" HeaderText="Created By" />
                </Columns>
            </asp:GridView>
        </div>
    </center>
</asp:Content>
