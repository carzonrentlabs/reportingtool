using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CorDriveReport_DelayedStartInCorDrive : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
       // Session["Userid"] = 3122;
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            objCordrive = new CorDrive();
            int rslt = objCordrive.ApprovingDelayedStartInCORDrive(Convert.ToInt32(Session["Userid"]), Convert.ToInt32(txtBookingId.Text), txtRemarks.Text);
            if (rslt > 0)
            {
                ShowMessage("Approved Successfully");
                lblMsg.Text = "Approved Successfully";
                lblMsg.Visible = true;
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                ShowMessage("Not Approved Successfully");
                lblMsg.Text = "Not Approved Successfully";
                lblMsg.Visible = true;
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            ShowMessage(ex.Message.ToString());
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
        }
        
    }
    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }
}
