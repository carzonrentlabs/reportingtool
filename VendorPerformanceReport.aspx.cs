﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using ReportingTool;
using ExcelUtil;


public partial class VendorPerformanceReport : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!Page.IsPostBack)
        {
            FillMonth();
            FillYear();
        }
        lblMsg.Text = "";

    }
   
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet dataset = new DataSet();
            dataset = objAdmin.GetVendorPerformance(Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));

            if (dataset.Tables[0].Rows.Count > 0)
            {
                Response.ClearContent();
                Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                Response.Clear();
                Response.AppendHeader("content-disposition", "attachment;filename=VendorPerformance.xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.ms-excel";
                this.EnableViewState = false;
                Response.Write("\r\n");
                Response.Write("<table border = '1' align = 'center'> ");
                int[] iColumns = { 0, 2, 4, 3, 5, 6, 8, 7, 9, 10, 12, 11, 13, 14, 16, 15, 17 };
                for (int i = 0; i <= dataset.Tables[0].Rows.Count - 1; i++)
                {
                    if (i == 0)
                    {
                        DateTime dt = new DateTime();
                        DateTime dt1 = new DateTime();

                        dt = Convert.ToDateTime(ddlYear.SelectedItem.Text + "-" + ddlMonth.SelectedItem.Value.ToString() + "-1");
                        dt1 = dt.AddMonths(-1);

                        Response.Write("<tr>");
                        Response.Write("<td align='left'></td>");
                        Response.Write("<td align='left' colspan='4' align='center'><b>No of cars on Road</b></td>");
                        Response.Write("<td align='left' colspan='4' align='center'><b>Auto Allocation</b></td>");
                        Response.Write("<td align='left' colspan='4' align='center'><b>Vendor Relationship</b></td>");
                        Response.Write("<td align='left' colspan='4' align='center'><b>Quality Management</b></td>");
                        Response.Write("</tr>");

                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "CityName")
                            {
                                Response.Write("<td align='left'><b>Location</b></td>");
                            }

                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "NoOfCarTarget")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Target</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "NoOfCarAchivedMonth1")
                            {
                                Response.Write("<td align='left'><b>" + dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + " Achieved</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "NoOfCarAchivedMonth2")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Achieved (Till Date)</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "NoOfCarShortfall")
                            {
                                Response.Write("<td align='left'><b>Shortfall</b></td>");
                            }

                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "AllocationTarget")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Target</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "AllocationAchivedMonth1")
                            {
                                Response.Write("<td align='left'><b>" + dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + " Achieved</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "AllocationAchivedMonth2")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Achieved (Till Date)</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "AllocationShortfall")
                            {
                                Response.Write("<td align='left'><b>Shortfall</b></td>");
                            }

                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "VendorTarget")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Limit</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "VendorAchivedMonth1")
                            {
                                Response.Write("<td align='left'><b>" + dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + " Achieved</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "VendorAchivedMonth2")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Achieved (Till Date)</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "VendorShortfall")
                            {
                                Response.Write("<td align='left'><b>Shortfall</b></td>");
                            }

                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "QualityTarget")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Limit</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "QualityAchivedMonth1")
                            {
                                Response.Write("<td align='left'><b>" + dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + " Achieved</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "QualityAchivedMonth2")
                            {
                                Response.Write("<td align='left'><b>" + dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Achieved (Till Date)</b></td>");
                            }
                            if (dataset.Tables[0].Columns[iColumns[j]].Caption.ToString() == "QualityShortfall")
                            {
                                Response.Write("<td align='left'><b>Shortfall</b></td>");
                            }
                        }
                        Response.Write("</tr>");
                    }

                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        Response.Write("<td align='left'>" + dataset.Tables[0].Rows[i][iColumns[j]].ToString().Replace("'", "") + "</td>");
                    }
                    Response.Write("</tr>");
                }
                Response.Write("</table>");
                Response.End();
            }
            else
            {
                Response.Write("<table border = '1' align = 'center' width = '100%'>");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Text = Ex.Message;
            lblMsg.ForeColor = System.Drawing.Color.Red;
        }
    }

    public void FillYear()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("YearName"));
        dt.Columns.Add(new DataColumn("YearValue"));

        dt.Rows.Add("Select", "0");

        int Year;
        Year = 2016;

        for (int i = 0; i < 6; i++)
        {
            dt.Rows.Add((Year + i), (Year + i));
        }

        ddlYear.DataSource = dt;
        ddlYear.DataTextField = dt.Columns["YearName"].ToString();
        ddlYear.DataValueField = dt.Columns["YearValue"].ToString();
        ddlYear.DataBind();
        ddlYear.SelectedValue = System.DateTime.Now.Year.ToString();
    }

    public void FillMonth()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("MonthName"));
        dt.Columns.Add(new DataColumn("MonthValue"));

        dt.Rows.Add("Select", "0");
        dt.Rows.Add("1", "1");
        dt.Rows.Add("2", "2");
        dt.Rows.Add("3", "3");
        dt.Rows.Add("4", "4");
        dt.Rows.Add("5", "5");
        dt.Rows.Add("6", "6");
        dt.Rows.Add("7", "7");
        dt.Rows.Add("8", "8");
        dt.Rows.Add("9", "9");
        dt.Rows.Add("10", "10");
        dt.Rows.Add("11", "11");
        dt.Rows.Add("12", "12");

        ddlMonth.DataSource = dt;
        ddlMonth.DataTextField = dt.Columns["MonthName"].ToString();
        ddlMonth.DataValueField = dt.Columns["MonthValue"].ToString();
        ddlMonth.DataBind();

        ddlMonth.SelectedValue = System.DateTime.Now.Month.ToString();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        DataSet dataset = new DataSet();
        dataset = objAdmin.GetVendorPerformance(Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
        if (dataset.Tables[0].Rows.Count > 0)
        {
            gvVendorPerformance.DataSource = dataset.Tables[0];
            gvVendorPerformance.DataBind();
        }

    }
  
   
    protected void gvVendorPerformance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DateTime dt = new DateTime();
        DateTime dt1 = new DateTime();

        dt = Convert.ToDateTime(ddlYear.SelectedItem.Text + "-" + ddlMonth.SelectedItem.Value.ToString() + "-1");
        dt1 = dt.AddMonths(-1);

        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[1].Text = "Location";
            e.Row.Cells[2].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Target";
            e.Row.Cells[3].Text = dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + "Achieved";
            e.Row.Cells[4].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Achieved (Till Date)";
            e.Row.Cells[5].Text = "Shortfall";

            e.Row.Cells[6].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Target";
            e.Row.Cells[7].Text = dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + "Achieved";
            e.Row.Cells[8].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Achieved (Till Date)";
            e.Row.Cells[9].Text = "Shortfall";

            e.Row.Cells[10].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + " Limit";
            e.Row.Cells[11].Text = dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + "Achieved";
            e.Row.Cells[12].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Achieved (Till Date)";
            e.Row.Cells[13].Text = "Shortfall";


            e.Row.Cells[14].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Limit";
            e.Row.Cells[15].Text = dt1.ToString("MMM") + "'" + dt1.ToString("yyyy") + "Achieved";
            e.Row.Cells[16].Text = dt.ToString("MMM") + "'" + dt.ToString("yyyy") + "Achieved (Till Date)";
            e.Row.Cells[17].Text = "Shortfall";


        }

    }
}