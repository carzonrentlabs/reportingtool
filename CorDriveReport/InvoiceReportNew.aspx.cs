﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

public partial class CorDriveReport_InvoiceReportNew : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    RentMeterService.wsRentmeter _objService = new RentMeterService.wsRentmeter();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindBrachDropDownlist();
            //string script = "$(document).ready(function () { $('[id*=bntGet]').click(); });";
            // ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
        }
        //bntGet.Attributes.Add("Onclick", "return validate()");
        //bntExprot.Attributes.Add("Onclick", "return validate()");
        lblMessate.Visible = false;

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(5000);
        DataSet dsInvoice = new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can't empty.')", true);
                return;
            }
            else
            {
                dsInvoice = objCordrive.GetCorDrivInvoiceReport(Convert.ToInt16(ddlBranch.SelectedValue), Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                if (dsInvoice.Tables[0].Rows.Count > 0)
                {
                    grdInvoice.DataSource = dsInvoice.Tables[0];
                    grdInvoice.DataBind();
                }
                else
                {
                    grdInvoice.DataSource = null;
                    grdInvoice.DataBind();
                    lblMessate.Text = "Record not available.";
                    lblMessate.Visible = true;
                    lblMessate.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        DataSet dsInvoice = new DataSet();
        DataTable dt = new DataTable();
        int counter = 0;
        //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scirpt", "javascript:ShowProgressKill()", true);
        // ScriptManager.RegisterStartupScript(this, this.GetType(), "scirpt", "javascript:ShowProgressKill()", true);
        //string script = "$(document).ready(function () { $('[id*=bntExprot]').click(); });";
        //ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can't empty.')", true);
                return;
            }
            else
            {
                dsInvoice = objCordrive.GetCorDrivInvoiceReport(Convert.ToInt16(ddlBranch.SelectedValue), Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));

                if (dsInvoice != null)
                {
                    if (dsInvoice.Tables.Count > 0)
                    {
                        dt = dsInvoice.Tables[0];
                    }
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {

                            //Response.ClearContent();
                            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                            Response.Clear();
                            Response.AppendHeader("content-disposition", "attachment;filename=CorDriveInvoiceReport.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = "application/vnd.ms-excel";
                            this.EnableViewState = false;
                            Response.Write("\r\n");
                            Response.Write("<table border = '1' align = 'center'> ");
                            int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 };
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                /* code to comment lat and lon location 
                                //Driver Location start
                                double _lat = Convert.ToDouble(dt.Rows[i]["DriverStartLat"].ToString());
                                double _lon = Convert.ToDouble(dt.Rows[i]["DriverStartLon"].ToString());

                                if (_lat != 0 && _lon != 0)
                                {
                                    dt.Rows[i]["DriverStartLocation"] = _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());
                                }
                                else
                                {
                                    dt.Rows[i]["DriverStartLocation"] = "";
                                }
                                //Driver Location end

                                //Passenger start Location start
                                _lat = Convert.ToDouble(dt.Rows[i]["PassengerStartLat"].ToString());
                                _lon = Convert.ToDouble(dt.Rows[i]["PassengerStartLon"].ToString());

                                if (_lat != 0 && _lon != 0)
                                {
                                    dt.Rows[i]["PassengerUpLocation"] = _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());
                                }
                                else
                                {
                                    dt.Rows[i]["PassengerUpLocation"] = "";
                                }
                                //Passenger start Location end


                                //Passenger end Location start
                                _lat = Convert.ToDouble(dt.Rows[i]["PassengerEndLat"].ToString());
                                _lon = Convert.ToDouble(dt.Rows[i]["PassengerEndLon"].ToString());

                                if (_lat != 0 && _lon != 0)
                                {
                                    dt.Rows[i]["PassengerDropoffLocation"] = _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());
                                    dt.Rows[i]["DriverEndLocation"] = _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());

                                }
                                else
                                {
                                    dt.Rows[i]["PassengerDropoffLocation"] = "";
                                    dt.Rows[i]["DriverEndLocation"] = "";
                                }
                                //Passenger end Location end

                                End code to comment lat and lon location  */


                                counter = counter + 1;
                                if (i == 0)
                                {
                                    Response.Write("<tr>");
                                    Response.Write("<td align='left'><b>S.No.</b></td>");
                                    for (int j = 0; j < iColumns.Length; j++)
                                    {
                                        if (dt.Columns[iColumns[j]].Caption.ToString() == "BookingID")
                                        {
                                            Response.Write("<td align='left'><b>Booking ID</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                        {
                                            Response.Write("<td align='left'><b>City</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "ClientCoName")
                                        {
                                            Response.Write("<td align='left'><b>Company Name</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "InstaPickupDate")
                                        {
                                            Response.Write("<td align='left'><b>Insta Pick Up Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "InstaPickupTime")
                                        {
                                            Response.Write("<td align='left'><b>Insta Pick Up Time</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "InstaDropoffdate")
                                        {
                                            Response.Write("<td align='left'><b>Insta Drop Off Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "InstaDropoffTime")
                                        {
                                            Response.Write("<td align='left'><b>Insta Drop Off Time</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverStartDate")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Start Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverStartTime")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Start Time</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverStartLocation")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Start Location</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerStartDate")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Pick Up Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerStartTime")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Pick Up Time</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerUpLocation")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Pick Up Location</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerEndtDate")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Drop Off Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerEndTime")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Drop Off Time</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerDropoffLocation")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Drop Off Loaction</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverEndDate")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive Driver End Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverEndTime")
                                        {
                                            Response.Write("<td align='left'><b>Cor Driver End Time</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverEndLocation")
                                        {
                                            Response.Write("<td align='left'><b>Cor Driver End  Loaction</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "Service")
                                        {
                                            Response.Write("<td align='left'><b>Service</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarRegNo")
                                        {
                                            Response.Write("<td align='left'><b>Car No</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "chauffeurName")
                                        {
                                            Response.Write("<td align='left'><b>Driver Name</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "chauffeurMobileNo")
                                        {
                                            Response.Write("<td align='left'><b>Driver Mobile</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "Tolls")
                                        {
                                            Response.Write("<td align='left'><b>Tolls</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "ParkTollChages")
                                        {
                                            Response.Write("<td align='left'><b>Park Toll Charges</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "InterstateTax")
                                        {
                                            Response.Write("<td align='left'><b>InterState Tax</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "GtoPTime")
                                        {
                                            Response.Write("<td align='left'><b>Garage Run 1 = G to P Time (COR Drive)</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "GtoPKM")
                                        {
                                            Response.Write("<td align='left'><b>Garage Run 1 = G to P KM (COR Drive)</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PtoPTime")
                                        {
                                            Response.Write("<td align='left'><b>Cor Drive P to P Uage Time</b></td>");
                                        }

                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PtoPKM")
                                        {
                                            Response.Write("<td align='left'><b>COR Drive P to P Uage KM</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DtoGTime")
                                        {
                                            Response.Write("<td align='left'><b>Garage Run 2 = D to G Time (COR Drive)</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DtoGKM")
                                        {
                                            Response.Write("<td align='left'><b>Garage Run 2 = D to G KM (COR Drive)</b></td>");
                                        }

                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CordriveBillHour")
                                        {
                                            Response.Write("<td align='left'><b>COR Drive Billed Hours</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CorDriveBillKM")
                                        {
                                            Response.Write("<td align='left'><b>COR Drived Billed KM</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "instaBillHour")
                                        {
                                            Response.Write("<td align='left'><b>Actual Billed Hours</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "instaBillKM")
                                        {
                                            Response.Write("<td align='left'><b>Actual Billed KM</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DifferenceHours")
                                        {
                                            Response.Write("<td align='left'><b>Difference in Hours Billed (Actual-COR Drive)</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DifferenceKM")
                                        {
                                            Response.Write("<td align='left'><b>Differnace in KM Billed (Actual-COR Drive)</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "BookingStatus")
                                        {
                                            Response.Write("<td align='left'><b>Status</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "cordrivePkgApplied")
                                        {
                                            Response.Write("<td align='left'><b>COR Drive Package Applied</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CorDriveBillAmount")
                                        {
                                            Response.Write("<td align='left'><b>COR Drive Billed Amount</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "instakgApplied")
                                        {
                                            Response.Write("<td align='left'><b>Actual Package Applied</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "instaTotalAmount")
                                        {
                                            Response.Write("<td align='left'><b>Actual Billed Amount</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "BillDifference")
                                        {
                                            Response.Write("<td align='left'><b>Billing Differnace</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "BillingBasis")
                                        {
                                            Response.Write("<td align='left'><b>Billing Basis</b></td>");
                                        }
                                    }
                                    Response.Write("</tr>");
                                }
                                Response.Write("<tr>");
                                Response.Write("<td align='left'>" + counter + "</td>");
                                for (int j = 0; j < iColumns.Length; j++)
                                {
                                    Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                                }

                                Response.Write("</tr>");
                            }
                            Response.Write("</table>");

                            Response.End();
                        }
                        else
                        {

                            Response.Write("<table border = 1 align = 'center' width = '100%'>");
                            Response.Write("<td align='center'><b>No Record Found</b></td>");
                            Response.Write("</table>");
                            Response.End();

                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
        finally
        {
            //string script = "$(document).ready(function () { $('[id*=bntExprot]').click(); });";
            //ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
            ////////
            //string modelkey = "$(document).ready(function (){ var modal = $('<div />');modal.removeClass('modal'); $('body').remove(modal); loading.hide();});";
            // ClientScript.RegisterStartupScript(this.GetType(), "load", modelkey, true);
            /////
            //bkLoading.Style.Add("display","none");
        }
    }

    protected void grdInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            DataSet _obj = new DataSet();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField _Dlat = (HiddenField)e.Row.FindControl("hd_Dlat");
                HiddenField _DLon = (HiddenField)e.Row.FindControl("hd_DLon");
                HiddenField _Plat = (HiddenField)e.Row.FindControl("hd_Plat");
                HiddenField _PLon = (HiddenField)e.Row.FindControl("hd_PLon");

                HiddenField _PSlat = (HiddenField)e.Row.FindControl("hd_PSLat");
                HiddenField _PSLon = (HiddenField)e.Row.FindControl("hd_PSLon");


                Label _driverStartLocation = (Label)e.Row.FindControl("lbl_DriverStartLocation");
                Label _passengerstartLocation = (Label)e.Row.FindControl("lbl_PassengerStartLocation");
                Label _passengerDropoffLocation = (Label)e.Row.FindControl("lbl_PassengerDropoffLocation");
                Label _driverendLocation = (Label)e.Row.FindControl("lbl_DriverEndLocation");

                
                //*******************************************************************************************

                //double _Clat = Convert.ToDouble(_lat.Value);
                //double _Clon = Convert.ToDouble(_lon.Value);

                //if (_Clat != 0 && _Clon != 0)
                //{
                //    dt.Rows[i]["CurrentLocation"] = _objService.GetCurrentLocation_LatLon(_Clat.ToString(), _Clon.ToString());
                //}
                //else
                //{
                //    dt.Rows[i]["CurrentLocation"] = "";
                //}

                //Driver Location start
                double _Clat = Convert.ToDouble(_Dlat.Value);
                double _Clon = Convert.ToDouble(_DLon.Value);

                if (_Clat != 0 && _Clon != 0)
                {
                    _driverStartLocation.Text = _objService.GetCurrentLocation_LatLon(_Clat.ToString(), _Clon.ToString());
                }
                else
                {
                    _driverStartLocation.Text = "";
                }
                //Driver Location end

                //Passenger start Location start
                _Clat = Convert.ToDouble(_PSlat.Value);
                _Clon = Convert.ToDouble(_PSLon.Value);

                if (_Clat != 0 && _Clon != 0)
                {
                    _passengerstartLocation.Text = _objService.GetCurrentLocation_LatLon(_Clat.ToString(), _Clon.ToString());
                }
                else
                {
                    _passengerstartLocation.Text = "";
                }
                //Passenger start Location end

                //Passenger end Location start
                _Clat = Convert.ToDouble(_Plat.Value);
                _Clon = Convert.ToDouble(_PLon.Value);

                if (_Clat != 0 && _Clon != 0)
                {
                    _passengerDropoffLocation.Text = _objService.GetCurrentLocation_LatLon(_Clat.ToString(), _Clon.ToString());
                    _driverendLocation.Text = _objService.GetCurrentLocation_LatLon(_Clat.ToString(), _Clon.ToString());
                }
                else
                {
                    _passengerDropoffLocation.Text = "";
                    _driverendLocation.Text = "";
                }
                //Passenger end Location end
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    private void BindBrachDropDownlist()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "Cityname";
                    ddlBranch.DataValueField = "CityId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }
}