<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DriverAvailabilityTracker.aspx.cs"
    Inherits="DriverAvailabilityTracker" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="dvAaiTrak" runat="server" ContentPlaceHolderID="cphPage">

    <script type="text/javascript">
     function ValidationCheck()
     {
         if (document.getElementById ('<%=ddMonth.ClientID%>').selectedIndex==0) 
         {
            alert("Select month..");
            document.getElementById ('<%=ddMonth.ClientID%>').focus();
            return false;
         }
         else if (document.getElementById ('<%=ddlUnit.ClientID%>').selectedIndex==0) 
         {
            alert("Select unit..");
            document.getElementById ('<%=ddlUnit.ClientID%>').focus();
            return false;
         }     
     }
     function PrintGridData()
      {
        var prtGrid = document.getElementById('<%=grvTracker.ClientID %>');
        prtGrid.border = 0;
        var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
        prtwin.document.write(prtGrid.outerHTML);
        prtwin.document.close();
        prtwin.focus();
        prtwin.print();
        prtwin.close();
    }
    </script>

    <div style="width: 60%; margin-left: 40%">
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="4" align ="center" >
                    <b>Driver Availability Tracker</b></td>
            </tr>
            <tr>
            <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px">
                    <label id="lblMonth" for="ddMonth">
                        Select Month</label>
                    <asp:DropDownList ID="ddMonth" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px">
                    <label id="lblUnit" for="ddlUnit">
                        Select Unit</label>
                    <asp:DropDownList ID="ddlUnit" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px">
                    <asp:Button ID="bntGet" runat="server" Text="Get " OnClick="bntGet_Click" OnClientClick="javascript:ValidationCheck();" />
                </td>
            </tr>
        </table>
    </div>
    <asp:Label ID="lblMessage" runat="Server" Visible="false"></asp:Label>
    <br />
    <div>
        <asp:GridView ID="grvTracker" runat="server" OnRowDataBound="grvTracker_RowDataBound"
            AutoGenerateColumns="False" DataKeyNames="carid,TeamId" OnRowCommand="grvTracker_RowCommand"
            BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2"
            ForeColor="Black" GridLines="None">
            <Columns>
                <asp:TemplateField HeaderText="Vechicle" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblVehicle" runat="server" Text='<%#Eval("RegnNo")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblDate" runat="server" Text='<%#Eval("TeamName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="1">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd1" runat="server" Enabled="false">
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="2">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd2" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="3">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd3" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="4">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd4" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="5">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd5" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="6">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd6" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="7">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd7" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="8">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd8" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="9">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd9" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="10">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd10" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="11">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd11" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="12">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd12" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="13">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd13" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="14">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd14" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="15">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd15" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="16">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd16" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="17">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd17" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="18">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd18" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="19">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd19" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="20">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd20" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="21">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd21" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="22">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd22" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="23">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd23" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="24">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd24" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="25">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd25" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="26">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd26" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="27">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd27" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="28">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd28" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="29">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd29" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="30">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd30" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="31">
                    <ItemTemplate>
                        <asp:DropDownList ID="dd31" runat="server" Enabled="false">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Save">
                    <ItemTemplate>
                        <asp:Button ID="bntSave" runat="server" CommandName="addNew" CausesValidation="false"
                            CommandArgument="<%#((GridViewRow)Container).RowIndex %>" Text="Update" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
            <FooterStyle BackColor="Tan" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        <asp:Label ID="lblMess" runat="server" Visible="false"></asp:Label>
        <br />
        <div style="text-align: center">
            <asp:Button ID="bntSaveAll" Text="Save" runat="server" ToolTip="Click to save." Visible="false"
                OnClick="bntSaveAll_Click" />
            <%-- <input type="button" id="bntPrint" value="Print" onclick="PrintGridData()" runat ="server"  visible ="false" />--%>
        </div>
    </div>
</asp:Content>
