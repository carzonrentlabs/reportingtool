using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
using System.IO;
using System.Drawing;
public partial class PerformanceReviewOnClientBasis : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
                btnSubmit.Attributes.Add("onclick", "return validate();");
                bntExport.Attributes.Add("onclick", "return validate();");
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        bntExport.Attributes.Add("onclick", "return validate();");
    }

    private void BindCity()
    {
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityId";
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string FromDate, Todate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid = Convert.ToInt16(ddlCityName.SelectedValue);
        DataTable dtReport = new DataTable();
        dtReport = objAdmin.ClientPerformanceReportCityBased(FromDate, Todate, cityid);
        if (dtReport.Rows.Count > 0)
        {
            lblMessage.Visible = false;
            grvPerformaneReportCityBased.DataSource = dtReport;
            grvPerformaneReportCityBased.DataBind();
        }
        else
        {
            grvPerformaneReportCityBased.DataSource = null;
            grvPerformaneReportCityBased.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = "Record is not available.";
            lblMessage.ForeColor = Color.Red;

        }
    }
    protected void bntExport_Click(object sender, EventArgs e)
    {
        string FromDate, Todate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid = Convert.ToInt16(ddlCityName.SelectedValue);
        DataTable dtReport = new DataTable();
        dtReport = objAdmin.ClientPerformanceReportCityBased(FromDate, Todate, cityid);
        //-------------------
        string attach = "attachment;filename=PerformanceReviewClientBasis.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/ms-excel";
        HttpContext context = HttpContext.Current;
        context.Response.Clear();
        if (dtReport != null)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<tr>");
            foreach (DataColumn dc in dtReport.Columns)
            {
                Response.Write("<td><b>" + dc.ColumnName + "</b></td>");
            }
            //Response.Write(System.Environment.NewLine);
            Response.Write("</tr>");
            foreach (DataRow dr in dtReport.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtReport.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString() + "</td>");
                }
                //Response.Write("\n");
                Response.Write("</tr>");
            }
            Response.Write("</table> ");
            Response.End();
        }
    }
}
