<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModelWise_RCSummary.aspx.cs" Inherits="ModelWise_RCSummary" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    
<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}
</script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="7">
                    <strong><u>Car Aging Summary as on</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 590px">Date</td>
                <td align="left" style="width: 11px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 105px"> City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="7">
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            
        <tr>
        <td align="center" colspan="7">
        <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found" AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="GridView1_PageIndexChanging">
        <Columns>
        
        <asp:TemplateField HeaderText="Car Model">
        <ItemTemplate>
        <asp:Label ID="lblModel" Text='<% #Bind("ModelName") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Less Than 1 Yr ">
        <ItemTemplate>
        <asp:Label ID="lblless1" Text='<% #Bind("LessThan1") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Less Than 2 Yr ">
        <ItemTemplate>
        <asp:Label ID="lblless2" Text='<% #Bind("LessThan2") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Less Than 3 Yr ">
        <ItemTemplate>
        <asp:Label ID="lblless3" Text='<% #Bind("LessThan3") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        <asp:TemplateField HeaderText="Less Than 4 Yr ">
        <ItemTemplate>
        <asp:Label ID="lblLess4" Text='<% #Bind("LessThan4") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Less Than 5 Yr">
        <ItemTemplate>
        <asp:Label ID="lbless5" Text='<% #Bind("LessThan5") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="> 5 Yr Old">
        <ItemTemplate>
        <asp:Label ID="lblmore5" Text='<% #Bind("MoreThan5") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        </Columns>
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        </td>
        </tr>
        
        </tbody>
    </table>
    
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
        top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
        scrolling="no" height="182"></iframe>
    </div>

</asp:Content>