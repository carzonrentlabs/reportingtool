using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class DutyDespatchDetail : clsPageAuthorization
//public partial class DutyDespatchDetail : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    //protected void BindCity()
    //{
    //    ddlCityName.DataTextField = "CityName";
    //    ddlCityName.DataValueField = "CityID";
    //    DataSet dsLocation = new DataSet();
    //    dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
    //    //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
    //    ddlCityName.DataSource = dsLocation;
    //    ddlCityName.DataBind();
    //}

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;

        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        //int cityid;
        //cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=DutyDespatchDetail.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.PendingDsDetails(FromDate, Todate);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 1, 0, 7, 8, 2, 4, 5, 6, 3 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>Location</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToClosedDuties")
                        {
                            Response.Write("<td align='left'><b>Closed Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToOpenDuties")
                        {
                            Response.Write("<td align='left'><b>Open Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NoShowClosed")
                        {
                            Response.Write("<td align='left'><b>No Show Charged</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NoShowClosedNotCharged")
                        {
                            Response.Write("<td align='left'><b>No Show Not Charged</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "LessThan48")
                        {
                            Response.Write("<td align='left'><b>Less Than 48 Hours</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Hours48To96")
                        {
                            Response.Write("<td align='left'><b>Greater Than 48 Hours</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "GreaterThan96")
                        {
                            Response.Write("<td align='left'><b>Greater Than 96 Hours</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridView1.PageIndex = e.NewPageIndex;
    //    FillGrid();
    //}

    void FillGrid()
    {
        string Todate, FromDate;

        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();

        //int cityid;
        //cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.PendingDsDetails(FromDate, Todate);

        GridView1.DataSource = GetBatchDetail;
        GridView1.DataBind();
    }
}