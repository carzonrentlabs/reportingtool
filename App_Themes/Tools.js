var UnderConst = "Under Construction";
var NoRecordFound = "No record found!!!!"

function alert_OnClick()
{
	alert("Under construction!!!!")
}


function MoveToPage(PageName,type)
{
	location.href=PageName +'?type=' + type;
}


function MoveToframe(PageName,FormName)
{
	var z,x;
	z=eval("window.opener."+FormName+".checkit");
	z.value=PageName;
	x=eval("window.opener."+FormName);
	x.submit();
	self.close();
}

function OpenReportNewWindow(PageName){
	win =window.open(PageName, "reportwindow", "width=800 height=700 left=0 top=0 scrollbars=yes resizable")
}

function OpenReport(PageName){
	location.href=PageName;
}


function OpenPopUpWindow(PgName,FieldName,formName,PgFeatures,PgHeight, PgWidth)
{
	var mWidth=PgWidth
	var mHeight=PgHeight
	var fullname=PgName + '?FieldName=' + FieldName + '&FormName=' + formName;
	window.open(fullname , "PopWindow", "width="+mWidth+" height="+mHeight+" left=200 top=260 " + PgFeatures);
}

function WinClose()
{
	window.close();
}

function ActiveWindow()
{
	window.focus()
}

// Description:
// This method will call dialoghost.aspx page as dialog box for selection of items
// depending upon PopUp Type
// Parameters:
// 1. PopUpLocation: Specify Location of Pop Up Web Form just virtual directory
// 2. PopUpType: Specify type of PopUp to be displayed. This variable decide which data to be shown for selection.
// 3. PopUpCaption: Specify Caption of PopUp
// 4. CodeFieldName: Specify Name of Code Field Text Box
// 5. DescFieldName: Specify Name of Desc/Name field text box
// 6. CodeFieldValue: Specify Value of Code Field Text Box which is send as primary selection type
// 7. FormName: Specify calling Web Form Name
// 8. SelectionType: Specify Type of selection to be done i.e. single selection or multi selection
// 9. PgHeight: Specify Height of PopUp Window
//10. PgWidth: Specify Width of PopUp Window
function OpenDialogPopUpWindow(PopUpLocation, PopUpType, PopUpCaption, CodeFieldName, DescFieldName, CodeFieldValue, FormName, SelectionType, PgHeight, PgWidth,WhereClause)
{
	// Declaring local variable to this method
	var myFormObject = new Object();
	var WinSettings;
	var CallingPopUp;
	// Setting local variable Total Number of return field as 1
	var TotalReturnField = "1";

	// Checking whether description/name field is passed
	if (DescFieldName!="" && DescFieldName!=null)
	{
		// If yes then set total number of return field as 2
		TotalReturnField = "2";
	}

	if (CodeFieldValue!="" && CodeFieldValue!=null)
	{
		// If yes then set total number of return field as 2
		TotalReturnField = "3";
	}

	if (WhereClause==null) 
	{
		WhereClause="";
	}
	
	
	// Generating PopUp window setting
	var WinSettings="dialogHeight: " + PgHeight + "px; dialogWidth: " + PgWidth + "px; edge: Raised; center: Yes; scroll: No; help: No; resizable: No; status: No;";

	// Generating PopUp Window URL
	CallingPopUp = PopUpLocation + "dialoghost.aspx?PopUpName=CommonPopUp.aspx&PopUpType=" + PopUpType + "&PopUpCaption=" + PopUpCaption + "&CodeFieldValue=" + CodeFieldValue + "&TotalReturnField=" + TotalReturnField + "&SelectionType=" + SelectionType + "&WhereClause=" + WhereClause

	// Calling ShowModalDialog method of window with required parameter
	// and waiting for activity to be completed in PopUp window.
	// After PopUp window activity is over fetch returned value from PopUp window
	// into local variable
	myFormObject = window.showModalDialog(CallingPopUp, myFormObject, WinSettings);

	// Checking whether PopUp window return any value or
	// closed without returning any value
	if(myFormObject!=null)
	{
		// If PopUp window return value then
		// Set calling web page code and description text box value as returned value from popup window

		// Setting value of Code Text Box as selected codes returned from PopUp window
		eval("document." + FormName + "." + CodeFieldName + ".value = '" + myFormObject.CodeFieldValue + "';");

		// Checking whether Total Number of fields value is return by PopUp window is 2
		if (TotalReturnField == "2")
		{
			// If yes then
			// Set value of Description Text Box as selected Descriptions returned from PopUp window
			eval("document." + FormName + "." + DescFieldName + ".value = '" + myFormObject.DescFieldValue + "';");
		}
		if (TotalReturnField == "3")
		{
			// If yes then
			// Set value of Description Text Box as selected Descriptions returned from PopUp window
			eval("document." + FormName + "." + DescFieldName + ".value = '" + myFormObject.DescFieldValue + "';");
			eval("document." + FormName + "." + CodeFieldValue + ".value = '" + myFormObject.Details + "';");
		}
	}
}	

// OpenPopUpWindowAsDialog:
// Description:
// This method will call dialoghost.aspx page as dialog box for selection of items
// depending upon PopUp Type
// Parameters:
// 1. PopUpLocation: Specify Location of Pop Up Web Form just virtual directory
// 2. PopUpName: Specify Name of Pop Up Web Form
// 3. PopUpCaption: Specify Caption of PopUp
// 4. CodeFieldName: Specify Name of Code Field Text Box
// 5. DescFieldName: Specify Name of Desc/Name field text box
// 6. CodeFieldValue: Specify Value of Code Field Text Box which is send as primary selection type
// 7. FormName: Specify calling Web Form Name
// 8. PgHeight: Specify Height of PopUp Window
// 9. PgWidth: Specify Width of PopUp Window
function OpenPopUpWindowAsDialog(PopUpLocation, PopUpName, PopUpCaption, CodeFieldName, DescFieldName, CodeFieldValue, FormName, PgHeight, PgWidth,WhereClause)
{

	// Declaring local variable to this method
	var myFormObject = new Object();
	var WinSettings;
	var CallingPopUp;
	// Setting local variable Total Number of return field as 1
	var TotalReturnField = "1";

	// Checking whether description/name field is passed
	if (DescFieldName!="" && DescFieldName!=null)
	{
		// If yes then set total number of return field as 2
		TotalReturnField = "2";
	}

	// Generating PopUp window setting
	var WinSettings="dialogHeight: " + PgHeight + "px; dialogWidth: " + PgWidth + "px; edge: Raised; center: Yes; scroll: No; help: No; resizable: No; status: No;";

	// Generating PopUp Window URL
	CallingPopUp = PopUpLocation + "dialoghost.aspx?PopUpName=" + PopUpName + "&PopUpType=&PopUpCaption=" + PopUpCaption + "&CodeFieldValue=" + CodeFieldValue + "&TotalReturnField=" + TotalReturnField + "&SelectionType=&WhereClause=" + WhereClause
	//CallingPopUp = PopUpName + "?CodeFieldValue=" + CodeFieldValue + "&TotalReturnField=" + TotalReturnField

	// Calling ShowModalDialog method of window with required parameter
	// and waiting for activity to be completed in PopUp window.
	// After PopUp window activity is over fetch returned value from PopUp window
	// into local variable
	myFormObject = window.showModalDialog(CallingPopUp, myFormObject, WinSettings);

	// Checking whether PopUp window return any value or
	// closed without returning any value
	if(myFormObject!=null)
	{
		// If PopUp window return value then
		// Set calling web page code and description text box value as returned value from popup window

		// Setting value of Code Text Box as selected codes returned from PopUp window
		eval("document." + FormName + "." + CodeFieldName + ".value = '" + myFormObject.CodeFieldValue + "';");

		// Checking whether Total Number of fields value is return by PopUp window is 2
		if (TotalReturnField == "2")
		{
			// If yes then
			// Set value of Description Text Box as selected Descriptions returned from PopUp window
			eval("document." + FormName + "." + DescFieldName + ".value = '" + myFormObject.DescFieldValue + "';");
		}
	}
}	



/*  ..... Calling Modal Window .....
				WinSetting = center:yes;resizable:no;dialogHeight:250px;dialogWidth:600px;status=no;
				ParentPage = ASPx Page name to be called.
				formObject = Form Object 
				controlName = Control Object*/
function CallWinModal(ParentPage, formObject, controlName, WinSettings)
{
	var ParmA = formObject.value;
	var MyArgs = new Array(ParmA);
		
	var FilePath = "../../Popup/"+ParentPage;

	var MyArgs = window.showModalDialog(FilePath, MyArgs, WinSettings);
	if (MyArgs == null)
	{
		window.alert("Nothing selected!!!!!")
	}
	else
	{
		var x;
		x=eval(formObject + "." + controlName);
		x.value=MyArgs[0].toString();
	}
}
/*  .........CALLING FUNCTION - WHEN TASK IS OVER FOR MODAL WINDOW..........
				objForm = Accept Form Name 
				objText = Accept Text Box Name		*/
function DoneModalTask(objForm, objText)
{
	x=eval(objForm + "." + objText);

	ParmA = x.value;
	if (ParmA=="")
	{
		alert("Please enter some value!!!!");
		x.focus();
	}
	else
	{
		var MyArgs = new Array(ParmA);
		window.returnValue = MyArgs;
		window.close();
	}
}


function setTreeViewValueInParentWindow(fieldName,formName)
{
	var j;
	j=document.forms[0].rdValue.value;
	if (j==""){
		alert('Please select some child value.');
		return;
	}
	x=eval("window.opener." + formName + "." + fieldName);
	x.value=j;
	x.focus();
	x.blur();
	self.close();
}

function setValueInParentWindow(fieldName,formName)
{
	var i;
	var j;
	var found;

	var x=document.forms[0].rdValue.length;

	if ( isNaN(x)) {
		if (document.forms[0].rdValue.checked) {
			//alert('Please select some value.');
			j=document.forms[0].rdValue.value;
		}
		else{
			alert('Please select some value.');
			return false;
		}
	}
	else
	{
		found=0;
		for (i=0 ; i<document.forms[0].rdValue.length; i++){
			if (document.forms[0].rdValue[i].checked){
				j=document.forms[0].rdValue[i].value;
				found=1;
			}
		}
		if (found==0){
			alert('Please select some value');
			return false;
		}
	}
	x=eval("window.opener." + formName + "." + fieldName);
	x.value=j;
	x.focus();
	x.blur();
	self.close();
}

function setValueInParentWindowCheckbox(fieldName,formName)
{
	var i;
	var j="";
	var found;

	var x=document.forms[0].rdValue.length;

	if ( isNaN(x))
	{
		if (document.forms[0].rdValue.checked)
		{
			j=document.forms[0].rdValue.value;
		}
		else
		{
			alert('Please select some value.');
			return false;
		}
	}
	else
	{
		found=0;
		for (i=0 ; i<document.forms[0].rdValue.length; i++)
		{
			if (document.forms[0].rdValue[i].checked)
			{
				if (found==0)
					j=document.forms[0].rdValue[i].value;
				else
					j=j+", "+document.forms[0].rdValue[i].value;
				found=1;
			}
			
		}
		if (found==0)
		{
			alert('Please select some value');
			return false;
		}
	}
	x=eval("window.opener." + formName + "." + fieldName);
	x.value=j;
	x.focus();
	x.blur();
	self.close();
}



function setValueInParentWindowNew(fieldNameCode,fieldNameDesc,formName)
{
	var i;
	var j;
	var k;
	var found;
	var x=document.forms[0].rdValue.length;
	if ( isNaN(x))
	{
		if (document.forms[0].rdValue.checked)
		{
			alert('Please select some value.');
			j=document.forms[0].rdValue.value;
		}
		else
		{
			alert('Please select some value.');
			return false;
		}
	}
	else
	{
		found=0;
		for (i=0 ; i<document.forms[0].rdValue.length; i++)
		{
			if (document.forms[0].rdValue[i].checked)
			{
				j=document.forms[0].rdValue[i].value;
				found=1;
			}
		}
		if (found==0)
		{
			alert('Please select some value');
			return false;
		}
	}
	x=eval("window.opener." + formName + "." + fieldNameCode);
	y=eval("window.opener." + formName + "." + fieldNameDesc);
	var p= j.split("&");
	if (!fieldNameDesc=='')
	{
		x.value=p[0];
		y.value=p[1];
	}
	else
	{
		x.value=p[0];
	}		
	x.focus();
	x.blur();
	self.close();
}

function setValueInParentWindowDate(fieldName,formName,docform)
{
	var i;
	var j;
	var found;
	var x=docform.rdValue.length;
	if ( isNaN(x))
	{
		if (docform.rdValue.checked)
		{
			j=docform.rdValue.value;
		}
		else
		{
			alert('Please select some value.');
			return false;
		}
	}
	else
	{
		found=0;
		for (i=0 ; i<docform.rdValue.length; i++)
		{
			if (docform.rdValue[i].checked)
			{
				j=docform.rdValue[i].value;
				found=1;
			}
		}
		if (found==0)
		{
			alert('Please select some value');
			return false;
		}
	}
	x=eval("window.opener." + formName + "." + fieldName);
	x.value=j;
	x.focus();
	x.blur();
	self.close();
}

function disableHtmlControls(formName, strMode)
{
	if (strMode.toUpperCase()=="VIEW")
	{
		for (var i=0;i<formName.elements.length;i++)
		{
			if (formName.elements[i].type=='button')
			{
				if (formName.elements[i].value=="...")
				{ 
					formName.elements[i].disabled=true;   
				}
			}
		}
	}
}

// ShowMessage:
// Display specified message in Error summary block
// Parameters:
// 1. strErrorMessage: Specify Error Message to be Add into Validation Summary
function ShowMessage( strErrorMeassage )
{

	//if no summary control is available
	if (typeof(Page_ValidationSummaries) != "undefined") 
	{

		var summary ;
	
		//Get Summary Control
		summary = Page_ValidationSummaries[0];
		if (summary.innerHTML == "")
		{
			summary.innerHTML = "<ul></ul>";
		}
	
		if( summary.style.display == "none" )
		{
			var s = "" ;
			s = s + "<ul>" ;
			s = s + "  <li>" ;
			s = s + strErrorMeassage ;
			s = s + "  </li>" ;
			s = s + "</ul>" ;
			summary.innerHTML = s ;
														
		}else
		{
			var s = "" ;
			s = s + "<li>" ;
			s = s + strErrorMeassage ;
			s = s + "</li>" ;
			summary.childNodes(0).innerHTML += s ;
		}
		//document.body.doScroll("pageUp");
	}
}

/* Grid Common Functionalities */
var preEl;
var orgBColor;
var orgTColor;

function GridHighLightTR(id, formname, idcontrolname)
{
	var backColor = '#c9cc99';
	var textColor = 'cc3333';
	if (typeof(preEl) != 'undefined')
	{
		preEl.bgColor = orgBColor;
		try
		{
			GridRowChangeTextColor(preEl, orgTColor, orgTColor);
		}
		catch(e)
		{
			;
		}
	}
	var el = event.srcElement;
	el = el.parentElement;
	orgBColor = el.bgColor;
	orgTColor = el.style.color;
	el.bgColor=backColor;

	try
	{
		GridRowChangeTextColor(el, textColor, backColor);
	}
	catch(e)
	{
		;
	}
	preEl = el;
	eval("document.forms['" + formname + "']." + idcontrolname + ".value = id");
}

function GridHighLightTRWithButton(id, formname, idcontrolname)
{
	var backColor = '#c9cc99';
	var textColor = 'cc3333';
	var x=eval("document.forms['" + formname + "']." + idcontrolname);
	x.value=id;
}


function GridRowChangeTextColor(a_obj, a_color, backColor)
{
	for (i=0; i < a_obj.cells.length; i++)
	{
		a_obj.cells(i).style.color = a_color;
		a_obj.cells(i).style.backgroundColor = backColor;
	}
}

function GridIsAnyRowSelected(formname, idcontrolname)
{
	var SelectedID;
	eval("SelectedID = document.forms['" + formname + "']." + idcontrolname + ".value");
	if (SelectedID == "" || SelectedID == null)
	{
		alert("Please select atleast one row");
		return false;
	}
	else
	{
		return true;
	}
}

// ShowMessage:
// Common functionality for checking Numeric type field
// for usage of this function call this method on onkeypress event of textbox.
// This function help people to enter only numeric type data.
// Parameters:
// 1. objTextBox: Pass reference of the Text box in which only numeric type data is allowed
// 2. isInteger: Pass true if value in the text box is Integer type i.e. no decimal places
//				 otherwise pass false.
// 3. intTotalLength: Pass Maximum length of data allowed in the textbox.
// 4. intDecimalLength: Pass this parameter if textbox contains decimal places and
//						Decimal place can be more than 2.
function CheckNumeric(objTextBox, isInteger, intTotalLength, intDecimalLength)
{
	vKey = window.event.keyCode;
	if (intDecimalLength == null)
	{
		if (!isInteger)
		{
			intDecimalLength = 2;
		}
		else
		{
			intDecimalLength = 0;
		}
	}
	if (objTextBox.value.length > intTotalLength)
	{
		window.event.keyCode = 0;
		return;
	}
	if (vKey == 46)
	{
		if (!isInteger)
		{
			if (objTextBox.value.indexOf(".") != -1)
			{
				window.event.keyCode = 0;
			}
		}
		else
		{
			window.event.keyCode = 0;
		}
	}
	else
	{
		if (!isDigit(vKey))
		{
			window.event.keyCode = 0;
		}
		else
		{
			vPos = objTextBox.value.indexOf(".");
			if (vPos != -1)
			{
				if (objTextBox.value.substr(vPos).length > intDecimalLength)
				{
					window.event.keyCode=0;
				}
			}
		}
	}
}

// ShowMessage:
// This method is used by CheckNumeric method of JS as defined above for validating
// whether input key is between 0 and 9.
// Parameters:
// 1. intKeyValue: Pass currently input key value.
function isDigit (intKeyValue)
{
	return ((intKeyValue >= 48) && (intKeyValue <= 57));
}

//
//LTrim:
//Returns a copy of a string without leading spaces.
//Parameters:
//1. str: the string we want to LTrim
//
function LTrim(str)
{
   var whitespace = new String(" \t\n\r");

   var s = new String(str);

   if (whitespace.indexOf(s.charAt(0)) != -1) {
      // We have a string with leading blank(s)...

      var j=0, i = s.length;

      // Iterate from the far left of string until we
      // don't have any more whitespace...
      while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
         j++;

      // Get the substring from the first non-whitespace
      // character to the end of the string...
      s = s.substring(j, i);
   }
   return s;
}

//
//RTrim:
//Returns a copy of a string without trailing spaces.
//Parameters:
//1. str: the string we want to RTrim
//
function RTrim(str)
{
   var whitespace = new String(" \t\n\r");

   var s = new String(str);

   if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
      // We have a string with trailing blank(s)...

      var i = s.length - 1;       // Get length of string

      // Iterate from the far right of string until we
      // don't have any more whitespace...
      while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
         i--;

      // Get the substring from the front of the string to
      // where the last non-whitespace character is...
      s = s.substring(0, i+1);
   }

   return s;
}

//
//Trim:
//Returns a copy of a string without leading or trailing spaces
//Parameters:
//1. str - the string we want to Trim
//
function Trim(str)
{
   return RTrim(LTrim(str));
}


//
//DecimalAllowed:
//To check that Qty can have the decimal values or not on the basis of UOM.
//Returns true / false.
//Parameters:
//1. uomValue - string of uom
//
function DecimalAllowed(uomValue)
{
	if ((uomValue=="BT")	|| (uomValue=="BOX") || (uomValue=="CC")	|| (uomValue=="DR")		|| (uomValue=="DZ")		||
		(uomValue=="EA")	|| (uomValue=="CRT") || (uomValue=="KLG")	|| (uomValue=="PAA")	|| (uomValue=="PAD")	||
		(uomValue=="PAC")	|| (uomValue=="PAL") || (uomValue=="PCE")	|| (uomValue=="PRS")	|| (uomValue=="RIM")	||
		(uomValue=="ROL")	|| (uomValue=="SET") || (uomValue=="SHT")	|| (uomValue=="ST")		|| (uomValue=="TBG")	||
		(uomValue=="TS")	|| (uomValue=="UNT")
		)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//
//TextAreaMaxLength:
//To check maxlength of Text area/Multiline text box.
//Returns --.
//Parameters:
//1. field - text box object
//2. maxlimit - maxlength required
//
function TextAreaMaxLength(field,maxlimit) 
{
	if (field.value.length > maxlimit)
    {
      field.value = field.value.substring(0, maxlimit);
    }
}

//
//IsNumeric		:
//To check that whether the entered value is numeric or not
//Returns --.
//Parameters:
//1. fieldValue - Value of text box
//
function IsNumeric(fieldValue)
{
	var num = '0123456789.';
	var i,j,found,intDotCtr;
	intDotCtr=0;
	found = false;
	for(i=0;i<fieldValue.length;i++)
	{
		if(fieldValue.charAt(i)=='.')
			intDotCtr++;
	
		for(j=0;j<num.length;j++)
		{
			if(fieldValue.charAt(i) == num.charAt(j))
			{found = true; break;}
		}
		if(found == false) {return false;}
		if(found == true) {found=false;}
	}
	if(parseInt(intDotCtr,10)>0)
		return true;
	return true;
}
//
//IsPipelineExists:
//To check that whether the entered value have any pipeline character or not.
//Returns --true/false
//Parameters:
//1. fieldValue - Value of text box
//
function IsPipelineExists(fieldValue)
{
	var i;
	var found = false;
	for(i=0;i<fieldValue.length;i++)
	{
		if(fieldValue.charAt(i)=='|')
		{
			found = true;
		}
	}
	return found;
}
// ValidateDate:
// This method is same as we define event in server side code for any control
// and its definiton is same like server side event defintion
// and this method is used validating whether input date is a valid date or not.
// Parameters:
// 1. source: Pass source from which this method is called.
// 2. args: Pass arguments of the function
function ValidateDate(source, args)
{
	// Calling internal function for validating value of the control which value is to be validated.
	args.IsValid = ValidateDateValue(args.Value);
}

// ValidateDateValue:
// This method actually validate value passed for valid date and
// this is the method we called from our java script from the application
// for validating whether specified value is a valid date.
// Parameters:
// 1. dtValue: Specify Value which is to be validated for valid Date value
// 2. return: true if valid date otherwise false
function ValidateDateValue(dtValue)
{
	// Return true when passed value is blank string
	// and for validating required field use Required Field Validator control.
	if (dtValue.length == 0)
	{
		return true;
	}
	// Checking whether length of value passed is in between 8 to 10 characters
	// because date value can be enter in following combination:
	// 1. d/m/yyyy
	// 2. dd/m/yyyy
	// 3. d/mm/yyyy
	// 4. dd/mm/yyyy
	// in all above case minimum length is 8 characters and maximum is 10 characters
	// if not satisfied the condition then returning false so that validation control
	// status set as InValid and page is not submitted and also validation control message
	// is displayed in validation summary
	if (dtValue.length < 8 || dtValue.length > 10)
	{
		return false;
	}
	// spliting passed value on the basis of "/" character and stored in array format
	var strDateArr = dtValue.split("/");
	// Checking whether array generated after split is of length 3
	// if not then returning false
	if (strDateArr.length != 3)
	{
		return false;
	}
	// Checking whether first part of the array of length between 1 and 2 characters
	// if not then returning false
	if (strDateArr[0].length < 1 || strDateArr[0].length > 2)
	{
		return false;
	}
	// Checking whether second part of the array of length between 1 and 2 characters
	// if not then returning false
	if (strDateArr[1].length < 1 || strDateArr[1].length > 2)
	{
		return false;
	}
	// Checking whether Last part of the array of length is 4 characters
	// if not then returning false
	if (strDateArr[2].length != 4)
	{
		return false;
	}
	// checking whether value specified in date control contains valid numbers
	if (isNaN(strDateArr[0]))
	{
		return false;
	}
	if (isNaN(strDateArr[1]))
	{
		return false;
	}
	if (isNaN(strDateArr[2]))
	{
		return false;
	}
	// Checking whether day and month part contains any leading zero if yes then removing that
	// because parseint did not consider second item if first element is zero
	var strDay = new String();
	var strMonth = new String();
	strDay = strDateArr[0];
	strMonth = strDateArr[1];
	if (strDay.substr(0, 1) == "0")
	{
		strDateArr[0] = strDay.substr(1, 1);
	}
	if (strMonth.substr(0, 1) == "0")
	{
		strDateArr[1] = strMonth.substr(1, 1);
	}
	// Storing day, month and year part of array in variables after converting them to integer value
	var iDay = parseInt(strDateArr[0]);
	var iMonth = parseInt(strDateArr[1]);
	var iYear = parseInt(strDateArr[2]);
	// Checking whether month is between 1 and 12
	// if not then returning false
	if (iMonth < 1 || iMonth > 12)
	{
		return false;
	}
	// Checking whether Day is between 1 and 31
	// if not then returning false
	if (iDay < 1 || iDay > 31)
	{
		return false;
	}
	// Checking whether month is 4, 6, 9, 11 i.e. April, June, September and November
	if (iMonth == 4 || iMonth == 6 || iMonth == 9 || iMonth == 11)
	{
		// if yes then validate whether day is less than equal to 30
		// because April, June, September and November months has maximum 30 days
		// if not then returning false
		if (iDay > 30)
		{
			return false;
		}
	}
	// Checking whether month is 2 i.e. February
	if (iMonth == 2)
	{
		// if yes then validate whether Year is Leap Year
		if (((iYear % 400) == 0) || (((iYear % 4) == 0) && ((iYear % 100) != 0)))
		{
			// if yes then validate whether day is less than equal to 29
			// because Leap Year February month has maximum 29 days
			// if not then returning false
			if (iDay > 29)
			{
				return false;
			}
		}
		else
		{
			// if not a leap year then validate whether day is less than equal to 28
			// because Non Leap Year February month has maximum 28 days
			// if not then returning false
			if (iDay > 28)
			{
				return false;
			}
		}
	}
	// If date is valid then returning true
	return true;
}

// FormatDateMDY:
// This method actually format the value in to MM/DD/YYYY format and
// this is the method we called from our java script from the application
// for formatting date type value in MM/DD/YYYY format.
// Parameters:
// 1. dtValue: Specify Value which is to be formatted in mm/dd/yyyy format
// 2. return: value formatted in mm/dd/yyyy format
function FormatDateMDY(dtValue)
{
	var strDateString = dtValue;
	var strDateArr = new Array();
	strDateArr = strDateString.split("/");
	var strDay = new String();
	var strMonth = new String();
	var strYear = new String();
	strDay = strDateArr[0];
	strMonth = strDateArr[1];
	strYear = strDateArr[2];
	if (strDay.substr(0, 1) == "0")
	{
		strDateArr[0] = strDay.substr(1, 1);
	}
	if (strMonth.substr(0, 1) == "0")
	{
		strDateArr[1] = strMonth.substr(1, 1);
	}
	var iDay = parseInt(strDateArr[0]);
	var iMonth = (parseInt(strDateArr[1]) - 1);
	var iYear = parseInt(strDateArr[2]);
	return (new Date(iYear, iMonth, iDay));
}

// CompareDateValues:
// This method compare the date value fields based on operator passed
// Parameters:
// 1. dtFromValue: Specify Value which is to be verified against second value
// 2. dtToValue: Specify Value which is to be verified with first value
// 3. sOperator: Specify following types of operators depending upon validation to be done:
//			A. ">": Specify this when u want to validate that first value is greater than second value
//			B. ">=": Specify this when u want to validate that first value is greater than equal to second value
//			C. "<": Specify this when u want to validate that first value is less than second value
//			D. "<=": Specify this when u want to validate that first value is less than equal to second value
//			E. "==": Specify this when u want to validate that first value is equal to second value
//			F. "!=": Specify this when u want to validate that first value is not equal to second value
// 4. return: true if validation is successful or out of first and second value one is blank
//			otherwise return false
function CompareDateValues(dtFromValue, dtToValue, sOperator)
{
	if (dtFromValue.length == 0 && dtToValue.length == 0)
	{
		return true;
	}
	if (!ValidateDateValue(dtFromValue))
	{
		return false;
	}
	if (!ValidateDateValue(dtToValue))
	{
		return false;
	}
	if (dtFromValue.length == 0 || dtToValue.length == 0)
	{
		return true;
	}
	if (sOperator.length == 0)
	{
		return false;
	}
	var strFromDate = new Date();
	var strToDate = new Date();
	strFromDate = FormatDateMDY(dtFromValue);
	strToDate = FormatDateMDY(dtToValue);
	return (eval("strFromDate " + sOperator + " strToDate"));
}

// IsDateLessthanEqualtoCurrentDate:
// This method compare the date value field as less than equal to today's date 
// Parameters:
// Parameters:
// 1. source: Pass source from which this method is called.
// 2. args: Pass arguments of the function
function IsDateLessthanEqualtoCurrentDate(source, args)
{
	// Calling internal function for validating value of the control which value is to be validated.
	args.IsValid = CompareDateWithTodayDate(args.Value, "<=");
}

// CompareDateWithTodayDate:
// This method compare the date value field with today's date based on operator passed
// Parameters:
// 1. dtFromValue: Specify Value which is to be verified against second value
// 2. sOperator: Specify following types of operators depending upon validation to be done:
//			A. ">": Specify this when u want to validate that first value is greater than second value
//			B. ">=": Specify this when u want to validate that first value is greater than equal to second value
//			C. "<": Specify this when u want to validate that first value is less than second value
//			D. "<=": Specify this when u want to validate that first value is less than equal to second value
//			E. "==": Specify this when u want to validate that first value is equal to second value
//			F. "!=": Specify this when u want to validate that first value is not equal to second value
// 3. return: true if validation is successful or out of first and second value one is blank
//			otherwise return false
function CompareDateWithTodayDate(dtFromValue, sOperator)
{
	if (dtFromValue.length == 0)
	{
		return true;
	}
	if (!ValidateDateValue(dtFromValue))
	{
		return false;
	}
	if (sOperator.length == 0)
	{
		return false;
	}
	var strFromDate = new Date();
	var strToDate = new Date();
	strToDate = GetTodaysDateAsDateType();
	strFromDate = FormatDateMDY(dtFromValue);
	return (eval("strFromDate " + sOperator + " strToDate"));
}

// GetTodaysDate:
// This method return Today's Date in DD/MM/YYYY format.
function GetTodaysDate()
{
	var dtToday = new Date();
	var strDay = dtToday.getDate();
	var strMonth = dtToday.getMonth();
	var strYear = dtToday.getFullYear();
	if (strDay.length == 1)
	{
		strDay = "0" + strDay;
	}
	if (strMonth.length == 1)
	{
		strMonth = "0" + strMonth;
	}
	return (strDay + "/" + strMonth + "/" + strYear);
}

// GetTodaysDate:
// This method return Today's Date in DD/MM/YYYY format.
function GetTodaysDateAsDateType()
{
	var dtToday = new Date();
	var iDay = parseInt(dtToday.getDate());
	var iMonth = parseInt(dtToday.getMonth());
	var iYear = parseInt(dtToday.getFullYear());
	return (new Date(iYear, iMonth, iDay));
}
function AddDaysInDate(dDateValue, iAddNosodDays)
{
	var dt = new Date();
	dt = FormatDateMDY(dDateValue);
	var iday = dt.getDate();
	var iMonth = dt.getMonth();
	var iYear = dt.getFullYear();
	var iDaysInMonth = 31;
	if (iMonth == 2)
	{
		iDaysInMonth = 28;
		if (((iYear % 400) == 0) || (((iYear % 4) == 0) && ((iYear % 100) != 0)))
		{
			iDaysInMonth = 29;
		}
	}
	if (iMonth == 3 || iMonth == 5 || iMonth == 8 || iMonth == 10)
	{
		iDaysInMonth = 30;
	}
	iday = iday + iAddNosodDays
	if ((iday) > iDaysInMonth)
	{
		iday = iday - iDaysInMonth;
		iMonth = iMonth + 1;
	}
	if (iMonth > 11)
	{
		iMonth = 1;
		iYear = iYear + 1;
	}
	var strDay = "0" + iday;
	var strMonth = "0" + (iMonth + 1);
	var strYear = iYear;
	if (strDay.length == 3)
	{
		strDay = strDay.substr(1);
	}
	if (strMonth.length == 3)
	{
		strMonth = strMonth.substr(1);
	}
	return ((strDay + "/" + strMonth + "/" + strYear));
}
//This function will load the popup with the the print report option.
function LoadPopupWithPrintOption(PopUpLocation, reportfilename, qstrId, orderNo, title,message, action){
	var myFormObject = new Object();
	var WinSettings;
	var CallingPopUp;
	
	// Generating PopUp window setting
	var WinSettings="dialogHeight: 230px; dialogWidth:500px; edge: Raised; center: Yes; scroll: No; help: No; resizable: No; status: No;";
	// Generating PopUp Window URL
	CallingPopUp = PopUpLocation + "popup/dialoghost.aspx?PopUpName=Notify.aspx&reportfilename=" + reportfilename + "&title="+title+"&message="+message+"&action="+action+"&qstrId="+ qstrId +"&qOrderNo="+ orderNo+ "&PopUpType=&PopUpCaption=Notifications=&TotalReturnField=&SelectionType="
	// Calling ShowModalDialog method of window with required parameter
	// and waiting for activity to be completed in PopUp window.
	// After PopUp window activity is over fetch returned value from PopUp window
	// into local variable
	myFormObject = window.showModalDialog(CallingPopUp, myFormObject, WinSettings);
	document.location.href=action;
}

function OpenReportFile(PopUpLocation,filepath,QId,orderId){

	var myFormObject = new Object();
	var WinSettings;
	var CallingPopUp;
	var WinSettings="dialogHeight: 600px; dialogWidth:800px; edge: Raised; center: Yes; scroll: No; help: No; resizable: yes; status: No;";
	CallingPopUp = PopUpLocation + "popup/dialoghost.aspx?PopUpName=" + filepath + "&qstrId="+ QId +"&qOrderNo="+ orderId + "&PopUpType=&PopUpCaption=Notifications=&TotalReturnField=&SelectionType="
	myFormObject = window.showModalDialog(CallingPopUp, myFormObject, WinSettings);
	if (myFormObject!=null){
		if (myFormObject.printsuccess==true){
			top.returnValue='';
			self.close();
			return false;
		}else{
			alert(myFormObject.errorMessage);
			return false;
		}
	}else{
		alert('There was some problem while printing report. Please try again.');
		return false;
	}
	return false;				
}

/*this is the trim method that will remove all the leading and preceding blank spaces.
function trim(inputString) {
   // Removes leading and trailing spaces from the passed string. Also removes
   // consecutive spaces and replaces it with one space. If something besides
   // a string is passed in (null, custom object, etc.) then return the input.
   if (typeof inputString != "string") { return inputString; }
   var retValue = inputString;
   var ch = retValue.substring(0, 1);
   while (ch == " ") { // Check for spaces at the beginning of the string
      retValue = retValue.substring(1, retValue.length);
      ch = retValue.substring(0, 1);
   }
   ch = retValue.substring(retValue.length-1, retValue.length);
   while (ch == " ") { // Check for spaces at the end of the string
      retValue = retValue.substring(0, retValue.length-1);
      ch = retValue.substring(retValue.length-1, retValue.length);
   }
   while (retValue.indexOf("  ") != -1) { // Note that there are two spaces in the string - look for multiple spaces within the string
      retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length); // Again, there are two spaces in each of the strings
   }
   return retValue; // Return the trimmed string back to the user
} / Ends */