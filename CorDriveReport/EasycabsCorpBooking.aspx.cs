using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;

public partial class CorDriveReport_EasycabsCorpBooking : System.Web.UI.Page
{
    DataSet dsEasycabsCorpBooking = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
       // Session["Userid"] = 3122;
        if (!IsPostBack)
        {
            BindCity();
        }
    }
    public void BindCity()
    {
        DataSet dsLocation = new DataSet();
        try
        {
            objCordrive = new CorDrive();
            dsLocation = objCordrive.GetLocations_UserAccessWise_SummaryCityWiseNew(Convert.ToInt32(Session["UserID"]));
            if (dsLocation.Tables.Count > 0)
            {
                ddlCity.DataTextField = "Cityname";
                ddlCity.DataValueField = "CityId";
                ddlCity.DataSource = dsLocation;
                ddlCity.DataBind();

            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    public void BindGrid()
    {
        try
        {
            objCordrive = new CorDrive();
            dsEasycabsCorpBooking = objCordrive.GetEasycabsCorpBokingDetails(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()), Convert.ToInt32(ddlCity.SelectedValue));
            if (dsEasycabsCorpBooking.Tables[0].Rows.Count > 0)
            {
                gvEasycabsCorpBooking.DataSource = dsEasycabsCorpBooking.Tables[0];
                gvEasycabsCorpBooking.DataBind();
            }
            else
            {
                gvEasycabsCorpBooking.DataSource = dsEasycabsCorpBooking.Tables[0];
                gvEasycabsCorpBooking.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From Date and To Date can not be empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsEasycabsCorpBooking = objCordrive.GetEasycabsCorpBokingDetails(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()), Convert.ToInt32(ddlCity.SelectedValue));
            WorkbookEngine.ExportDataSetToExcel(dsEasycabsCorpBooking, "EasycabsCorpBooking" + ".xls");
        }
    }
}
