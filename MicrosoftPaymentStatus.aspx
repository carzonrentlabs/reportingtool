<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MicrosoftPaymentStatus.aspx.cs" Inherits="MicrosoftPaymentStatus" Title="Untitled Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
    window.history.forward(1);   
        function validate()
        {
            if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
            {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }
                    
            if(document.getElementById('<%=txtTo.ClientID %>').value == "")
            {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>

    <table width="80%" border="0" align="center" id="table1">
        <tbody>
            <tr>
                <td align="center" style="height: 18px">
                    <strong><u>Microsoft Payment Status</u></strong>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table align="center">
                        <tr>
                            <td align="center" style="width: 93px; height: 40px;">
                                From Date</td>
                            <td align="left" style="width: 93px; height: 40px;">
                                <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                            </td>
                            <td align="center" style="width: 28px; height: 40px;">
                                <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                            </td>
                            <td align="center" style="height: 40px; width: 93px;">
                                To Date</td>
                            <td align="center" style="width: 5px; height: 40px;">
                                <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                            </td>
                            <td align="center" style="height: 40px; width: 28px;">
                                <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                            </td>
                            <td align="left" style="height: 22px">
                                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Get" OnClick="btnSubmit_Click" />
                                &nbsp;<asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/images/excel.jpg"
                                    ToolTip="click to download excel format" Height="20px" OnClick="btnExportToExcel_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
            </tr>
        </tbody>
    </table>
    <div align="center">
        <asp:GridView ID="grvPaymentStatus" runat="server" AutoGenerateColumns="false"  BackColor="LightGoldenrodYellow"
            BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both">
            <Columns>
                <asp:TemplateField HeaderText="S.No.">
                    <ItemTemplate>
                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Car Agency" DataField="CarAgency" />
                <asp:BoundField HeaderText="Traveller Name" DataField="TravellerName" />
                <asp:BoundField HeaderText="Duty City" DataField="DutyCity" DataFormatString="{0:dd/MM/yyyy}" />
                <asp:BoundField HeaderText="Car Type" DataField="CarType" />
                <asp:BoundField HeaderText="Rental Date" DataField="RentalDate" DataFormatString="{0:dd/MM/yyyy}" />
                <asp:BoundField HeaderText="Travel Request No" DataField="TravelRequestNo" />
                <asp:BoundField HeaderText="Carzonrent ID" DataField="CarzonrentID" />
                <asp:BoundField HeaderText="Duty Slip Status" DataField="DutySlipStatus" />
                <asp:BoundField HeaderText="Charging Status" DataField="ChargingStatus" />
                <asp:BoundField HeaderText="Original Code" DataField="origincode" />
                <asp:BoundField HeaderText="Amex Status" DataField="AmexLinkStatus" />
                <asp:BoundField HeaderText="Total Cost" DataField="totalcost" />
                <asp:BoundField HeaderText="Log Date" DataField="logdate" DataFormatString="{0:dd/MM/yyyy}" />
            </Columns>
            <FooterStyle BackColor="Tan" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
        </asp:GridView>
    </div>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
