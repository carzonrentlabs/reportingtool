using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;

public partial class BranchIncentiveBudgetReport : System.Web.UI.Page
{
    #region VariableDeclaration
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    
    int startMonth, endMonth;
    private string unitId;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                BindYear(ddlFromYear);
                BindYear(ddlToYear);
                BindMonth(ddlFromMonth);
                BindMonth(ddlToMonth);
            }
        }
        catch (Exception)
        {
            
            throw;
        }
        bntGet.Attributes.Add("onclick", "return validate();");
        bntExprot.Attributes.Add("onclick", "return validate();");

    }

    private void BindMonth(DropDownList ddlMonth)
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2013; i <= 2014; i++)
            {
                ddlYear.Items.Add (new ListItem (i.ToString(),i.ToString()));
            }
            ddlYear.Items.Insert(0,new ListItem ("--Select--","0"));
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        int fromMonth =int.Parse(ddlFromMonth.SelectedValue.ToString());
        int fromYear = int.Parse(ddlFromYear.SelectedValue.ToString());
        int toMonth = int.Parse(ddlToMonth.SelectedValue.ToString());
        int toYear = int.Parse(ddlToYear.SelectedValue.ToString());
        if (ddlFromMonth.SelectedIndex == 0 || ddlFromYear.SelectedIndex == 0 || ddlToMonth.SelectedIndex == 0 || ddlToYear.SelectedIndex == 0)
        {
            return;
           ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:validate();",true );
        }
        DataTable dtBranchIncentive = new DataTable();
        dtBranchIncentive = objFleetTrackerSummary.GetBranchIncentiveBudget(fromMonth, fromYear, toMonth, toYear, Convert.ToInt32(Session["UserID"]));
        if (dtBranchIncentive.Rows.Count > 0)
        {
            lblMessage.Visible = false;
            grvBranchIncentiveReport.DataSource = dtBranchIncentive;
            grvBranchIncentiveReport.DataBind();
        }
        else
        {
            grvBranchIncentiveReport.DataSource = null;
            grvBranchIncentiveReport.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = "Record is not available.";
            lblMessage.ForeColor = Color.Red;
        }

    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        int fromMonth = int.Parse(ddlFromMonth.SelectedValue.ToString());
        int fromYear = int.Parse(ddlFromYear.SelectedValue.ToString());
        int toMonth = int.Parse(ddlToMonth.SelectedValue.ToString());
        int toYear = int.Parse(ddlToYear.SelectedValue.ToString());

        if (ddlFromMonth.SelectedIndex == 0 || ddlFromYear.SelectedIndex == 0 || ddlToMonth.SelectedIndex == 0 || ddlToYear.SelectedIndex == 0)
        {
            return;
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:validate();", true);
        }

        DataTable dtBranchIncentive = new DataTable();
        dtBranchIncentive = objFleetTrackerSummary.GetBranchIncentiveBudget(fromMonth, fromYear, toMonth, toYear, Convert.ToInt32(Session["UserID"]));
        //-------------------
        string attach = "attachment;filename=BudgetReport.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/ms-excel";
        HttpContext context = HttpContext.Current;
        context.Response.Clear();
        if (dtBranchIncentive != null)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<tr>");
            foreach (DataColumn dc in dtBranchIncentive.Columns)
            {
                Response.Write("<td><b>" + dc.ColumnName + "</b></td>");
            }
            //Response.Write(System.Environment.NewLine);
            Response.Write("</tr>");
            foreach (DataRow dr in dtBranchIncentive.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtBranchIncentive.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString() + "</td>");
                }
                //Response.Write("\n");
                Response.Write("</tr>");
            }
            Response.Write("</table> ");
            Response.End();
        }
    }
    
}
