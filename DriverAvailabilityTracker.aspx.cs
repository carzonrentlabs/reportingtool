using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;
public partial class DriverAvailabilityTracker : System.Web.UI.Page
{

    #region Variable Declaration
    //clsAdmin objAdmin = new clsAdmin();
    clsAutomation objAdmin = new clsAutomation();
    int unitId;
    int monthName;
    //int totalNumberofdays;
    const int carIdIndex = 0;
    const int teamIdIndex = 1;
    DataTable dtTrackerPresent;
    bool showbutton;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindMonth();
            BindUnit();
        }
    }
    #region Helper function
    private void BindMonth()
    {
        try
        {
            DataTable dtMonth = new DataTable();
            dtMonth = objAdmin.GetMonthName();
            ddMonth.DataTextField = "monthName";
            ddMonth.DataValueField = "number";
            ddMonth.DataSource = dtMonth;
            ddMonth.DataBind();
            ddMonth.Items.Insert(0, "--Select--");

        }
        catch (Exception Ex)
        {
            lblMess.Visible = true;
            lblMess.Text = Ex.Message;
        }


    }
    private void BindUnit()
    {
        try
        {
            DataTable dtUnit = new DataTable();
            dtUnit = objAdmin.GetUnitName(Convert.ToInt32(Session["UserID"]));
            ddlUnit.DataTextField = "UnitName";
            ddlUnit.DataValueField = "UnitId";
            ddlUnit.DataSource = dtUnit;
            ddlUnit.DataBind();
            ddlUnit.Items.Insert(0, "--Select--");

        }
        catch (Exception Ex)
        {
            lblMess.Visible = true;
            lblMess.Text = Ex.Message;
        }
    }
    #endregion
    #region EventFunction
    protected void bntGet_Click(object sender, EventArgs e)
    {
        if (ddMonth.SelectedIndex == 0 || ddlUnit.SelectedIndex == 0)
        {
            return;
        }
        try
        {
            DataTable dtTotal = new DataTable();
            DataTable dtTracker = new DataTable();
            //DataTable dtTrackerPresent = new DataTable();
            dtTrackerPresent = new DataTable();
            unitId = Convert.ToInt32(ddlUnit.SelectedValue);
            monthName = Convert.ToInt32(ddMonth.SelectedValue);
            dtTotal = objAdmin.GetTotalNumberofDays(monthName);
            ViewState["totalNumberofdays"] = Convert.ToInt32(dtTotal.Rows[0][0]);
            //totalNumberofdays = Convert.ToInt32(dtTotal.Rows[0][0]);
            dtTrackerPresent = objAdmin.PresentDriverTracker(Convert.ToInt32(Session["UserID"]), unitId, monthName);
            //dtTracker = objAdmin.GetDriverAvailabilityTracker(Convert.ToInt32(Session["UserID"]),unitId, monthName);
            if (dtTrackerPresent.Rows.Count > 0)
            {
                try
                {
                    grvTracker.DataSource = dtTrackerPresent;
                    grvTracker.DataBind();

                    //Start For Show hide column
                    if (Convert.ToInt32(ViewState["totalNumberofdays"]) < 31)
                    {
                        try
                        {
                            for (int i = Convert.ToInt32(ViewState["totalNumberofdays"]); i <= 31; i++)
                            {

                                if (i == 28 || i == 29 || i == 30 || i == 31)
                                {
                                    grvTracker.Columns[i + 2].Visible = false;
                                }
                                if (i == 31)
                                {
                                    grvTracker.Columns[i + 2].Visible = true;
                                }

                            }
                        }
                        catch (Exception Ex)
                        {
                            lblMess.Text = Ex.Message;
                        }

                    }
                    else
                    {
                        for (int i = 0; i < 31; i++)
                        {
                            grvTracker.Columns[i + 2].Visible = true;
                        }

                    }
                    //End of Show and Hide
                }
                catch (Exception Ex)
                {
                    lblMess.Visible = true;
                    lblMess.Text = Ex.Message;
                }

            }
            else
            {
                dtTracker = objAdmin.GetDriverAvailabilityTracker(Convert.ToInt32(Session["UserID"]), unitId, monthName);
                //Start for show and hide column
                if (dtTracker.Rows.Count > 0)
                {

                    if (Convert.ToInt32(ViewState["totalNumberofdays"]) < 31)
                    {
                        try
                        {
                            for (int i = Convert.ToInt32(ViewState["totalNumberofdays"]); i <= 31; i++)
                            {

                                if (i == 28 || i == 29 || i == 30 || i == 31)
                                {
                                    grvTracker.Columns[i + 2].Visible = false;
                                }
                                if (i == 31)
                                {
                                    grvTracker.Columns[i + 2].Visible = true;
                                }

                            }
                        }
                        catch (Exception Ex)
                        {
                            lblMess.Text = Ex.Message;
                        }

                    }
                    else
                    {
                        for (int i = 0; i < 31; i++)
                        {
                            grvTracker.Columns[i + 2].Visible = true;
                        }

                    }
                    //End of show hide column..
                    grvTracker.DataSource = dtTracker;
                    grvTracker.DataBind();
                }
                else
                {

                    lblMess.Visible = true;
                    lblMess.ForeColor = Color.Red;
                    lblMess.Text = "Record is not available.";
                    bntSaveAll.Visible = false;
                    showbutton = true;

                }

            }
            lblMessage.Visible = false;
            if (Convert.ToInt32(ddMonth.SelectedValue) != Convert.ToInt32(DateTime.Today.Month))
            {
                bntSaveAll.Visible = false;
            }
            else
            {
                if (showbutton == true)
                {
                    bntSaveAll.Visible = false;
                }
                else
                {
                    bntSaveAll.Visible = true;
                }
            }
            //bntPrint.Visible = false;

        }
        catch (Exception Ex)
        {
            lblMess.Visible = true;
            lblMess.Text = Ex.Message;
        }
    }

    protected void grvTracker_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                if ((Convert.ToInt32(e.Row.RowIndex) + 1) % 2 == 0)
                {
                    Label lblVeh = (Label)e.Row.FindControl("lblVehicle");
                    lblVeh.Text = "";

                }
                DateTime tDay = DateTime.Today;
                int toDays = tDay.Day;
                //DataTable ddSelect = new DataTable();
                DataTable dtCheckStatus = new DataTable();
                //change ddSelect with dtTracherPresent.
                //ddSelect = objAdmin.PresentDriverTracker(Convert.ToInt32(Session["UserID"]), unitId, monthName);
                if (dtTrackerPresent.Rows.Count > 0)   //Status Entered 
                {

                    for (int i = 1; i <= Convert.ToInt32(ViewState["totalNumberofdays"]); i++)
                    {
                        DropDownList ddP = (DropDownList)e.Row.FindControl("dd" + i);
                        if (!string.IsNullOrEmpty(dtTrackerPresent.Rows[e.Row.RowIndex]["day" + i].ToString()))
                        {
                            ddP.SelectedValue = dtTrackerPresent.Rows[e.Row.RowIndex]["day" + i].ToString();
                        }

                    }
                    if (Convert.ToInt32(ddMonth.SelectedValue) != Convert.ToInt32(DateTime.Today.Month))
                    {
                        //ddP.Enabled = false;
                    }
                    else if (toDays >= 3 && toDays <= Convert.ToInt32(ViewState["totalNumberofdays"]))
                    {
                        int j = 2;
                        while (j >= 0)
                        {
                            DropDownList showdd = (DropDownList)e.Row.FindControl("dd" + Convert.ToInt16(toDays - j));
                            //showdd.Enabled = true;
                            if (string.IsNullOrEmpty(dtTrackerPresent.Rows[e.Row.RowIndex]["day" + Convert.ToInt16(toDays - j)].ToString()))
                            {
                                showdd.Enabled = true;
                            }
                            else
                            {

                                showdd.Enabled = false;
                            }
                            j--;
                        }
                    }
                    else if (toDays == 2)
                    {
                        int j = 1;
                        while (j >= 0)
                        {
                            DropDownList showdd = (DropDownList)e.Row.FindControl("dd" + Convert.ToInt16(toDays - j));
                            //showdd.Enabled = true;
                            if (string.IsNullOrEmpty(dtTrackerPresent.Rows[e.Row.RowIndex]["day" + Convert.ToInt16(toDays - j)].ToString()))
                            {
                                showdd.Enabled = true;
                            }
                            else
                            {
                                showdd.Enabled = false;
                            }
                            j--;
                        }
                    }
                    else if (toDays == 1)
                    {
                        DropDownList showdd = (DropDownList)e.Row.FindControl("dd1");
                        //showdd.Enabled = true;
                        if (string.IsNullOrEmpty(dtTrackerPresent.Rows[e.Row.RowIndex]["day" + Convert.ToInt16(toDays)].ToString()))
                        {
                            showdd.Enabled = true;

                        }
                        else
                        {
                            showdd.Enabled = false;
                        }

                    }

                }
                else
                {
                    for (int i = 1; i <= grvTracker.Columns.Count; i++)
                    {
                        if (i <= Convert.ToInt32(ViewState["totalNumberofdays"]))
                        {
                            DropDownList ddP = (DropDownList)e.Row.FindControl("dd" + i);

                            if (Convert.ToInt32(ddMonth.SelectedValue) != Convert.ToInt32(DateTime.Today.Month))
                            {
                                ddP.Enabled = false;
                            }
                            else if (toDays >= 3 && toDays <= Convert.ToInt32(ViewState["totalNumberofdays"]))
                            {
                                int j = 2;
                                while (j >= 0)
                                {
                                    DropDownList showdd = (DropDownList)e.Row.FindControl("dd" + Convert.ToInt16(toDays - j));
                                    showdd.Enabled = true;
                                    j--;
                                }
                            }
                            else if (toDays == 2)
                            {
                                int j = 1;
                                while (j >= 0)
                                {
                                    DropDownList showdd = (DropDownList)e.Row.FindControl("dd" + Convert.ToInt16(toDays - j));
                                    showdd.Enabled = true;
                                    j--;
                                }
                            }
                            else if (toDays == 1)
                            {

                                DropDownList showdd = (DropDownList)e.Row.FindControl("dd1");
                                showdd.Enabled = true;
                            }
                        }
                    }

                }

            }
            catch (Exception Ex)
            {
                lblMess.Visible = true;
                lblMess.Text = Ex.Message;
            }

        }
    }

    protected void grvTracker_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "addNew")
        {
            try
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int carId = Convert.ToInt32(grvTracker.DataKeys[index].Values[carIdIndex]);
                int teamId = Convert.ToInt32(grvTracker.DataKeys[index].Values[teamIdIndex]);
                DateTime tDay = DateTime.Today;
                DateTime statusDate = DateTime.Today;
                int toDays = tDay.Day;
                DropDownList ddS = (DropDownList)grvTracker.Rows[index].FindControl("dd" + toDays);
                int status = Convert.ToInt32(ddS.SelectedValue);
                //DataTable dtTracker = new DataTable();
                int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                int monthDay = Convert.ToInt32(ddMonth.SelectedValue);
                int dtTracker = objAdmin.InsertDriverAvailabilityTracker(carId, teamId, status, unitId, monthDay, statusDate, Convert.ToInt32(Session["UserID"]));
                if (dtTracker >= 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Record Save Successfully.";
                    lblMessage.ForeColor = Color.Red;
                }
            }
            catch (Exception Ex)
            {
                lblMess.Visible = true;
                lblMess.Text = Ex.Message;
            }

        }

    }
    protected void bntSaveAll_Click(object sender, EventArgs e)
    {
        try
        {

            DateTime tDay = DateTime.Today;
            DateTime statusDate;
            int toDays = tDay.Day;
            int dtTracker = 0;
            if (toDays >= 3 && toDays <= Convert.ToInt32(ViewState["totalNumberofdays"]))
            {
                int j = 2;
                while (j >= 0)
                {
                    statusDate = DateTime.Today.AddDays(-j);
                    //foreach (GridView row in grvTracker.Rows )

                    for (int i = 0; i < grvTracker.Rows.Count; i++)
                    {
                        //int index = Convert.ToInt32(e.CommandArgument);
                        int carId = Convert.ToInt32(grvTracker.DataKeys[i].Values[carIdIndex]);
                        int teamId = Convert.ToInt32(grvTracker.DataKeys[i].Values[teamIdIndex]);
                        //DateTime tDay = DateTime.Today;
                        //DateTime statusDate = DateTime.Today;
                        //int toDays = tDay.Day;
                        //DropDownList ddS = (DropDownList)grvTracker.Rows[i].FindControl("dd" + toDays);
                        DropDownList ddS = (DropDownList)grvTracker.Rows[i].FindControl("dd" + Convert.ToInt16(toDays - j));
                        int status = Convert.ToInt32(ddS.SelectedValue);
                        //DataTable dtTracker = new DataTable();
                        int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                        int monthDay = Convert.ToInt32(ddMonth.SelectedValue);
                        dtTracker = objAdmin.InsertDriverAvailabilityTracker(carId, teamId, status, unitId, monthDay, statusDate, Convert.ToInt32(Session["UserID"]));
                    }
                    j--;
                }

            }
            else if (toDays == 2)
            {
                int j = 1;
                while (j >= 0)
                {
                    statusDate = DateTime.Today.AddDays(-j);
                    for (int i = 0; i < grvTracker.Rows.Count; i++)
                    {
                        //int index = Convert.ToInt32(e.CommandArgument);
                        int carId = Convert.ToInt32(grvTracker.DataKeys[i].Values[carIdIndex]);
                        int teamId = Convert.ToInt32(grvTracker.DataKeys[i].Values[teamIdIndex]);
                        //DateTime tDay = DateTime.Today;
                        //DateTime statusDate = DateTime.Today;
                        //int toDays = tDay.Day;
                        //DropDownList ddS = (DropDownList)grvTracker.Rows[i].FindControl("dd" + toDays);
                        DropDownList ddS = (DropDownList)grvTracker.Rows[i].FindControl("dd" + Convert.ToInt16(toDays - j));
                        int status = Convert.ToInt32(ddS.SelectedValue);
                        //DataTable dtTracker = new DataTable();
                        int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                        int monthDay = Convert.ToInt32(ddMonth.SelectedValue);
                        dtTracker = objAdmin.InsertDriverAvailabilityTracker(carId, teamId, status, unitId, monthDay, statusDate, Convert.ToInt32(Session["UserID"]));

                    }
                    j--;
                }

            }
            else if (toDays == 1)
            {
                statusDate = DateTime.Today;
                for (int i = 0; i < grvTracker.Rows.Count; i++)
                {
                    int carId = Convert.ToInt32(grvTracker.DataKeys[i].Values[carIdIndex]);
                    int teamId = Convert.ToInt32(grvTracker.DataKeys[i].Values[teamIdIndex]);
                    DropDownList ddS = (DropDownList)grvTracker.Rows[i].FindControl("dd" + Convert.ToInt32(toDays));
                    int status = Convert.ToInt32(ddS.SelectedValue);
                    //DataTable dtTracker = new DataTable();
                    int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                    int monthDay = Convert.ToInt32(ddMonth.SelectedValue);
                    dtTracker = objAdmin.InsertDriverAvailabilityTracker(carId, teamId, status, unitId, monthDay, statusDate, Convert.ToInt32(Session["UserID"]));

                }
            }
            if (dtTracker >= 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record Save Successfully.";
                lblMessage.ForeColor = Color.Red;
            }

        }
        catch (Exception Ex)
        {
            lblMess.Visible = true;
            lblMess.Text = Ex.Message;
        }

    }
    #endregion


}
