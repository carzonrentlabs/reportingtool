<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DispatchSummary.aspx.cs" Inherits="CorDriveReport_DispatchSummary" Title="Dispatch Summary Report" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Cor Drive Dispatch Summary</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            
        </table>
        <br />
        <div class="Repeater">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                          <asp:GridView ID="grdDispatchSummary" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="Booking ID" DataField="BookingID" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Pickup DateTime" DataField="PickupDateTime" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cityname" DataField="Cityname" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Time Delay" DataField="time_delay" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Dispatch Status" DataField="dispatch_status" HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
             <br />
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                         <asp:GridView ID="grdDispatchDetails" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Booking" DataField="totalBooking" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cor Meter Allotted" DataField="cor_meter_allotted" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Non Cor Meter Allotted" DataField="non_cor_meter_allotted" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="direct_dispatch" DataField="direct_dispatch" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Not_Dispatched" DataField="not_dispatched" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Pending In Future" DataField="pending_in_future" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Not Dispatched Urgent" DataField="not_dispatched_urgent" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Not Dispatched Past Pickup" DataField="not_dispatched_past_pickup" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Not_Dispatched_1_Hr_Left" DataField="not_dispatched_1_hr_left" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Not_Dispatched_2_Hr_Left" DataField="not_dispatched_2_hr_left" HeaderStyle-Wrap="false" />
                               
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
       
    </center>
</asp:Content>

