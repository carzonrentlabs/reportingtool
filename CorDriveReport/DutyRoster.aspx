﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DutyRoster.aspx.cs" Inherits="CorDriveReport_DutyRoster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    
    <link href="../DatePickerCSS/ui.all.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        $(function () {
          

            $('#<%=txtDate.ClientID%>').datepicker();
            $('#<%=txtDate.ClientID%>').datepicker('setDate', new Date());

            $("#<%=bntExprot.ClientID%>").click(function () {
                if (document.getElementById('<%=txtDate.ClientID %>').value == "") {
                    alert("Please Select the From Date");
                    document.getElementById('<%=txtDate.ClientID %>').focus();
                    return false;
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: center">
            <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
                <tr>
                    <td colspan="5" align="center">
                        <b>Duty Roster</b>

                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblCityName" runat="server">City Name</asp:Label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlCity" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;                        
                       <asp:TextBox ID="txtDate" runat="server" Width="100"></asp:TextBox>
                    </td>

                    <td style="height: 20px; white-space: nowrap;">&nbsp;&nbsp;
                        <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:Literal runat="server" ID="ltlSummary"></asp:Literal>
        </div>
    </form>
</body>
</html>
