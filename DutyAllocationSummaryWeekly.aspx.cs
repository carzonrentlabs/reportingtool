using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class DutyAllocationSummaryWeekly : clsPageAuthorization
//public partial class DutyAllocationSummaryWeekly : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string FromDate;
        FromDate = txtFrom.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=BrnachWiseDutiesSummary.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetDutiesAllocationSummary_Weekly(FromDate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 0, 2, 3, 4, 5, 6, 7, 8, 9};
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    Response.Write("<td align='left'><b></b></td>");
                    Response.Write("<td align='center' colspan='2'><b>1st Wk</b></td>");
                    Response.Write("<td align='center' colspan='2'><b>2nd Wk</b></td>");
                    Response.Write("<td align='center' colspan='2'><b>3rd Wk</b></td>");
                    Response.Write("<td align='center' colspan='2'><b>4th Wk</b></td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>Branches</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Target1"
                            || dtEx.Columns[iColumns[j]].Caption.ToString() == "Target2"
                            || dtEx.Columns[iColumns[j]].Caption.ToString() == "Target3"
                            || dtEx.Columns[iColumns[j]].Caption.ToString() == "Target4")
                        {
                            Response.Write("<td align='left'><b>Target</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Implementation1"
                  || dtEx.Columns[iColumns[j]].Caption.ToString() == "Implementation2"
                  || dtEx.Columns[iColumns[j]].Caption.ToString() == "Implementation3"
                  || dtEx.Columns[iColumns[j]].Caption.ToString() == "Implementation4")
                        {
                            Response.Write("<td align='left'><b>Achieved</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string FromDate;
        FromDate = txtFrom.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetDutiesAllocationSummary_Weekly(FromDate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();

            HeaderCell.ColumnSpan = 1;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "1st Wk";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "2nd Wk";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "3rd Wk";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "4th Wk";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;

            HeaderGridRow.Cells.Add(HeaderCell);

            GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);

        }
    }
}