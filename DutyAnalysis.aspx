<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DutyAnalysis.aspx.cs" Inherits="DutyAnalysis" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
    {
        alert("Please Select the From Date");
        document.getElementById('<%=txtFrom.ClientID %>').focus();
        return false;
    }
            
    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}
    </script>

    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="2">
                    <strong><u>Duties Analysis</u></strong></td>
            </tr>
            <tr>
                <td align="center">
                    From Date<%--</td>
                <td align="left">--%>
                &nbsp;&nbsp;
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                    <%--</td>
                <td align="left" style="width: 28px">--%>
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                    &nbsp;&nbsp;
                    <%--</td>            
                <td align="right">--%>
                    To Date&nbsp;&nbsp;<%--</td>
                <td align="left" >--%>
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                    <%--</td>
                <td align="left" style="width: 20px">--%>
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <%--<td align="right" style="width: 105px">City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>--%>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:GridView runat="server" ID="GridView1" PageSize="20" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
                        OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="grvMergeHeader_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="Car No">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegnNo" Text='<% #Bind("RegnNo") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category">
                                <ItemTemplate>
                                    <asp:Label ID="lblCarCatName" Text='<% #Bind("CarCatName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Grade">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrade" Text='<% #Bind("Grade") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dedicated Car Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Current Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblCurrentStatus" Text='<% #Bind("CurrentStatus") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Delhi">
                                <ItemTemplate>
                                    <asp:Label ID="lblSystemAllocationD" Text='<% #Bind("SystemAllocationD") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gurgaon">
                                <ItemTemplate>
                                    <asp:Label ID="lblSystemAllocationG" Text='<% #Bind("SystemAllocationG") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noida">
                                <ItemTemplate>
                                    <asp:Label ID="lblSystemAllocationN" Text='<% #Bind("SystemAllocationN") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airport">
                                <ItemTemplate>
                                    <asp:Label ID="lblSystemAllocationA" Text='<% #Bind("SystemAllocationA") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            
                            <asp:TemplateField HeaderText="Delhi">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeAllocationD" Text='<% #Bind("DeAllocationD") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gurgaon">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeAllocationG" Text='<% #Bind("DeAllocationG") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noida">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeAllocationN" Text='<% #Bind("DeAllocationN") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airport">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeAllocationA" Text='<% #Bind("DeAllocationA") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            
                            <asp:TemplateField HeaderText="Delhi">
                                <ItemTemplate>
                                    <asp:Label ID="lblNetDutySystemAllocationD" Text='<% #Bind("NetDutySystemAllocationD") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gurgaon">
                                <ItemTemplate>
                                    <asp:Label ID="lblNetDutySystemAllocationG" Text='<% #Bind("NetDutySystemAllocationG") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noida">
                                <ItemTemplate>
                                    <asp:Label ID="lblNetDutySystemAllocationN" Text='<% #Bind("NetDutySystemAllocationN") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airport">
                                <ItemTemplate>
                                    <asp:Label ID="lblNetDutySystemAllocationA" Text='<% #Bind("NetDutySystemAllocationA") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="Delhi">
                                <ItemTemplate>
                                    <asp:Label ID="lblOtherDutySystemAllocationD" Text='<% #Bind("OtherDutySystemAllocationD") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gurgaon">
                                <ItemTemplate>
                                    <asp:Label ID="lblOtherDutySystemAllocationG" Text='<% #Bind("OtherDutySystemAllocationG") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noida">
                                <ItemTemplate>
                                    <asp:Label ID="lblOtherDutySystemAllocationN" Text='<% #Bind("OtherDutySystemAllocationN") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airport">
                                <ItemTemplate>
                                    <asp:Label ID="lblOtherDutySystemAllocationA" Text='<% #Bind("OtherDutySystemAllocationA") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            
                            <asp:TemplateField HeaderText="Delhi">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotSystemAllocationD" Text='<% #Bind("NotSystemAllocationD") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gurgaon">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotSystemAllocationG" Text='<% #Bind("NotSystemAllocationG") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noida">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotSystemAllocationN" Text='<% #Bind("NotSystemAllocationN") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airport">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotSystemAllocationA" Text='<% #Bind("NotSystemAllocationA") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="Delhi">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalDutiesD" Text='<% #Bind("TotalDutiesD") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gurgaon">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalDutiesG" Text='<% #Bind("TotalDutiesG") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Noida">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalDutiesN" Text='<% #Bind("TotalDutiesN") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airport">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalDutiesA" Text='<% #Bind("TotalDutiesA") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>