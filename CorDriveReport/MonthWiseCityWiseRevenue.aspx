﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MonthWiseCityWiseRevenue.aspx.cs" Inherits="MonthWiseCityWiseRevenue" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=ddlMonth.ClientID %>').selectedIndex == 0) {
                alert("Select From Month");
                document.getElementById('<%=ddlMonth.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlYear.ClientID %>').selectedIndex == 0) {
                alert("Select From Year.");
                document.getElementById('<%=ddlYear.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <div align="center">
        <b>Branch Wise Month Wise Revenue Report</b>
    </div>
    <br />
    <div>
        <table cellpadding="0" cellspacing="0" border="0" align="center">
            <tr>
                <td align="left">
                    &nbsp;&nbsp;Month
                    <asp:DropDownList ID="ddlMonth" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;Year
                    <asp:DropDownList ID="ddlYear" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <asp:Literal ID="ltlSummary" runat="server"></asp:Literal>
    </div>
</asp:Content>