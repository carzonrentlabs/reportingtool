﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CarCurrentLocation.aspx.cs" Inherits="CorDriveReport_CarCurrentLocation" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=bntGet.ClientID%>").click(function () {
                if ($("#<%=txtRegNo.ClientID%>").val() == "") {
                    alert("Enter Registration number.");
                    $("#<%=txtRegNo.ClientID%>").focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });

            $("#<%=txtRegNo.ClientID%>").keyup(function () {
                var strPass = $("#<%=txtRegNo.ClientID%>").val();
                var first4Letters = strPass.substring(0, 4)
                var spaceLetters = strPass.substring(4, 5)

                var strLength = first4Letters.length;
                var lchar = strPass.charAt((strLength) - 1);

                var cCode = CalcKeyCode(lchar);
                if (cCode < 48 || cCode > 57) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtRegNo.ClientID%>").val(myNumber);
                    alert("Enter first four character only numeric value.")
                }
                if (spaceLetters != "") {
                    var cCode = CalcKeyCode(spaceLetters);
                    if (cCode != 32) {
                        var myNumber = strPass.substring(0, (strPass.length) - 1);
                        $("#<%=txtRegNo.ClientID%>").val(myNumber);
                        alert("Enter one character space.")
                    }
                }
                return false;
            });

            $("#<%=txtBookingId.ClientID%>").keyup(function () {
                var strPass = $("#<%=txtBookingId.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 48 || cCode > 57) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtBookingId.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
                return false;
            });

            $("#<%=bntBookingId.ClientID%>").click(function () {
                if ($("#<%=txtBookingId.ClientID%>").val() == "") {
                    alert("Enter booking id.");
                    $("#<%=txtBookingId.ClientID%>").focus();
                    return false;
                }
                else if (isNaN($("#<%=txtBookingId.ClientID%>").val())) {
                    alert("Enter only numeric value.");
                    $("#<%=txtBookingId.ClientID%>").focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });

        });

        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }

        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
 
    </script>
    <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 40%">
                <tr>
                    <td colspan="4" align="center">
                        <b>Current location</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <b>Enter Registration No.</b>
                    </td>
                    <td align="left">
                        &nbsp;
                        <asp:TextBox ID="txtRegNo" runat="server" MaxLength="25"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:CheckBox ID="chkHistorical" runat="server" /><b>Historical</b>
                    </td>
                    <td align="left">
                        <asp:Button ID="bntGet" runat="server" Text="GO" OnClick="bntGet_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <b>Enter BookingId</b>
                    </td>
                    <td align="left">
                        &nbsp;
                        <asp:TextBox ID="txtBookingId" runat="server" MaxLength="25"></asp:TextBox>
                    </td>
                    <td align="left" colspan="2">
                        &nbsp;<asp:Button ID="bntBookingId" runat="server" Text="Get Map By Booking" OnClick="bntBookingId_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Current Location</b>&nbsp;
                    </td>
                    <td colspan="3">
                        <asp:Label ID="lblCurrentLocation" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="text-align: center">
            <iframe id="ifViewMap" runat="server" visible="false" height="500px" width="1000px">
            </iframe>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
