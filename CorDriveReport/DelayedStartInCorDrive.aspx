<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DelayedStartInCorDrive.aspx.cs" Inherits="CorDriveReport_DelayedStartInCorDrive" Title="Approve Delay Start In COR Drive" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 
   <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
     <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width:300px">
            <tr>
                <td colspan="2" align="center" bgcolor="#FF6600">
                   <b>Approve Delay Start In COR Drive</b> 
                 </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b>Booking Id :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtBookingId" runat="server"></asp:TextBox>
                 </td>
             </tr>
               
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b>Remarks :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtRemarks" runat="server" Height="100px" TextMode="MultiLine" Width="200px"></asp:TextBox>
                 </td>
             </tr>
            
            <tr>
                <td style="white-space: nowrap;" align="center" colspan="2">
                <asp:Button ID="btnApprove" Text="Approve" runat="server" OnClick="btnApprove_Click" />
                 </td>
             </tr>
            
            
            
            
                       
        </table>
       
    </center>
</asp:Content>

