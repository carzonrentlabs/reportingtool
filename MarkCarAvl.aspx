﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MarkCarAvl.aspx.cs" Inherits="MarkCarAvl" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <link href="DatePickerCSS/ui.all.css" rel="stylesheet" type="text/css" />
    <script language="Javascript" src="App_Themes/CommonScript.js" type="text/jscript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/jscript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $("#<%=txtTo.ClientID%>").datepicker();
        });

       function validate() {
            if (document.getElementById('<%=txtRegno.ClientID %>').value == "") {
                alert("Please input regno.");
                document.getElementById('<%=txtRegno.ClientID %>').focus();
                return false;
          if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Please Select To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }

            
            }
        }
</script>
    <table width="80%" border="0" align="center">
        <tr>
            <td align="center">
                <strong><u>Enter Car Reg. No.</u></strong>&nbsp;&nbsp;
                <asp:TextBox ID="txtRegno" runat="server" MaxLength="15"></asp:TextBox>&nbsp;&nbsp;
                <asp:RequiredFieldValidator ID="req1" runat="server" ErrorMessage="Enter Regno." ControlToValidate="txtRegno"></asp:RequiredFieldValidator>
                <strong><u>Enter Date</u></strong>&nbsp;&nbsp;
                <asp:TextBox ID="txtTo" runat="server" MaxLength="10"></asp:TextBox>&nbsp;&nbsp;

                <asp:RequiredFieldValidator ID="req2" runat="server" ErrorMessage="Date required!" ControlToValidate="txtTo"></asp:RequiredFieldValidator>
                   <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="validate()" />
               
            </td>
              <td>
             </td>

        </tr>
        
    </table>
</asp:Content>

