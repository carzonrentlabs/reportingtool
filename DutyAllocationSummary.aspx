<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DutyAllocationSummary.aspx.cs" Inherits="DutyAllocationSummary" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="Javascript" src="App_Themes/CommonScript.js" type="text/javascript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFrom.ClientID%>").datepicker();
            $("#<%=txtTo.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Duties Allocation Summary</u></strong>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 62px; height: 40px;" colspan="8">
                    From Date &nbsp;
                    <%--<input id="txtFrom" runat="server" type="text" readonly="readOnly" />--%>
                    <asp:TextBox ID="txtFrom" runat="server" Width="100px" MaxLength="10">
                    </asp:TextBox>
                    <%--<img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Font-Bold="true"
                        ControlToValidate="txtFrom" Display="Dynamic" ErrorMessage="*">
                    </asp:RequiredFieldValidator>
                    &nbsp;&nbsp; To Date &nbsp;&nbsp;
                    <%--<input id="txtTo" runat="server" type="text" readonly="readOnly" />--%>
                    <asp:TextBox ID="txtTo" runat="server" Width="100px" MaxLength="10">
                    </asp:TextBox>
                    <%--<img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Font-Bold="true"
                        ControlToValidate="txtTo" Display="Dynamic" ErrorMessage="*">
                    </asp:RequiredFieldValidator>
                    &nbsp;&nbsp; City Name: &nbsp;&nbsp;
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="165px">
                    </asp:DropDownList>
                    &nbsp;
                    <asp:DropDownList ID="ddltype" runat="server">
                        <asp:ListItem Text="Daily" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Monthly" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both">
                        <Columns>
                            <%--<asp:TemplateField HeaderText="Date">
        <ItemTemplate>
        <asp:Label ID="lblPickupDate" Text='<% #Bind("PickupDate") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="City Name">
        <ItemTemplate>
        <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Total Duty">
        <ItemTemplate>
        <asp:Label ID="lblTotalDuties" Text='<% #Bind("DutiesPerformed") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Total Auto Allocated Duty">
        <ItemTemplate>
        <asp:Label ID="lblTotalDuties" Text='<% #Bind("TotalDuties") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Auto Duty Allocated">
        <ItemTemplate>
        <asp:Label ID="lblAutoDutyAllocated" Text='<% #Bind("AutoDutyAllocated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="No. of Duties De-allocated">
        <ItemTemplate>
        <asp:Label ID="lblDutyDeallocated" Text='<% #Bind("DutyDeallocated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="% Implementation">
        <ItemTemplate>
        <asp:Label ID="lblImplementation" Text='<% #Bind("Implementation") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>--%>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
