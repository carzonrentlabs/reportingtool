using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;
using ExcelUtil;
public partial class CorDriveReport_LoginSummaryCityWise : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsLoginSummaryCityWise = new DataSet();
    DataSet dsExportToExcel = new DataSet();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Text = string.Empty;
    }
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
            objCordrive = new CorDrive();
            System.Threading.Thread.Sleep(500);
            try
            {
                 if(ddlCity.SelectedIndex != 0)
                 {
                         dsLoginSummaryCityWise = objCordrive.GetLoginSummaryReportCityWise(ddlCity.SelectedItem.Text.ToString());
                         if (dsLoginSummaryCityWise.Tables[0].Rows.Count > 0)
                        {
                            grdLoginSummaryCityWise.DataSource = dsLoginSummaryCityWise.Tables[0];
                            grdLoginSummaryCityWise.DataBind();

                            grdActivityCityWise.DataSource = dsLoginSummaryCityWise.Tables[1];
                            grdActivityCityWise.DataBind();
                           
                        }
                        else
                        {
                            grdLoginSummaryCityWise.DataSource = null;
                            grdLoginSummaryCityWise.DataBind();
                           
                            grdActivityCityWise.DataSource = null;
                            grdActivityCityWise.DataBind();

                            lblMessage.Text = "Record not available.";
                            lblMessage.Visible = true;
                            lblMessage.ForeColor = Color.Red;
                        }
                 }
                else
                {
                     ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please select City.')", true);
                     return;
                }
                    
            }
            catch (Exception Ex)
            {
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = Ex.Message;
            }
        }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        if (ddlCity.SelectedIndex != 0)
        {
            objCordrive = new CorDrive();
            //string CityName = ddlCity.SelectedItem.Text.ToString();
            dsExportToExcel = objCordrive.GetLoginSummaryExportToExcel(ddlCity.SelectedItem.Text.ToString());
            WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "LoginActivityReport.xls");
        }
             
    }
}

