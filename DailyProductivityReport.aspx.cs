﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ReportingTool;
using System.Text;
using System.IO;
public partial class DailyProductivityReport : System.Web.UI.Page
{
    clsAutomation objAuto = new clsAutomation();

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!IsPostBack)
        {
            BindCityDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtFromDate.Text.ToString()) && !string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                lblMessage.Visible = false;
                StringBuilder strData = new StringBuilder();
                strData = GenerateExcel();
                ltlSummary.Text = strData.ToString();
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Pickup date should not be blank";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception)
        {

            throw;
        }

    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
       
            if (!string.IsNullOrEmpty(txtFromDate.Text.ToString()) && !string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                StringBuilder strData = new StringBuilder();
                strData = GenerateExcel();
                ltlSummary.Text = strData.ToString();
                Response.Clear();
                Response.Buffer = true;
                string strFileName = "DailyProductivityReport";
                strFileName = strFileName.Replace("(", "");
                strFileName = strFileName.Replace(")", "");
                Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                ltlSummary.RenderControl(hw);
                Response.Output.Write(sw.ToString());
                Response.End();
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Pickup date should not be blank";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }

       
    }

    private StringBuilder GenerateExcel()
    {
        StringBuilder strBody = new StringBuilder();
        try
        {
            DataSet dsClose = new DataSet();
            int grandtotalRowClose = 0, totalRow = 0;
            int intTotal;
            if (!string.IsNullOrEmpty(txtFromDate.Text.ToString()) && !string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                lblMessage.Visible = false;
                dsClose = objAuto.GetDailyProductivityReport(Convert.ToInt32(ddlCity.SelectedValue));

                if (dsClose.Tables[0].Rows.Count > 0)
                {
                    strBody.Append("<table border = 1 align = 'center'  width = 100%> ");
                    strBody.Append("<tr><td>&nbsp;</td></tr>");
                    strBody.Append("<tr><td><b> Closing / Billing </b></td></tr>");
                    strBody.Append("<tr><td>&nbsp;</td></tr>");
                    strBody.Append("<tr><td>");
                    strBody.Append("<table border='1' align='left' cellpadding='2' cellspacing='0' style='font-family: Arial; font-size: 12px;'>");
                    for (int i = 0; i <= dsClose.Tables[0].Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            strBody.Append("<tr>");
                            for (int j = 0; j < dsClose.Tables[0].Columns.Count; j++)
                            {
                                if (dsClose.Tables[0].Columns[j].Caption.ToString() != "Row")
                                {
                                    if (dsClose.Tables[0].Columns[j].Caption.ToString() == "unitName")
                                    {
                                        strBody.Append("<td style='text-align: center;'><b>Unit Name</b></td>");
                                    }
                                    else if (dsClose.Tables[0].Columns[j].Caption.ToString() == "UserName")
                                    {
                                        strBody.Append("<td style='text-align: center;'><b>User Name</b></td>");
                                    }
                                    else if (dsClose.Tables[0].Columns[j].Caption.ToString() == "RoleName")
                                    {
                                        strBody.Append("<td style='text-align: center;'><b>Role</b></td>");
                                    }
                                    else if (dsClose.Tables[0].Columns[j].Caption.ToString() == "Target")
                                    {
                                        strBody.Append("<td style='text-align: center;'><b>Target</b></td>");
                                    }
                                    //else if (dsClose.Tables[0].Columns[j].Caption.ToString() == "Averagehandlingtime")
                                    //{
                                    //    strBody.Append("<td style='text-align: center;'><b>Target = 7*60 = 420 / Average handling time</b></td>");
                                    //}
                                    else
                                    {
                                        strBody.Append("<td style='text-align: center;'><b>" + Convert.ToDateTime(dsClose.Tables[0].Columns[j].Caption.ToString()).Day.ToString()
                                            + "/" + Convert.ToDateTime(dsClose.Tables[0].Columns[j].Caption).ToString("MMM")
                                            + " (" + Convert.ToDateTime(dsClose.Tables[0].Columns[j].Caption).DayOfWeek.ToString().Substring(0, 3) + ")"
                                            + "</b></td>");
                                    }
                                }
                            }
                            strBody.Append("<td style='text-align: center;'><b>Total</b></td>");
                            strBody.Append("<td style='text-align: center;'><b>Total MTD</b></td>");
                            strBody.Append("<td style='text-align: center;'><b>% Achieved</b></td>");
                            strBody.Append("</tr>");
                        }
                        strBody.Append("<tr>");
                        totalRow = 0;
                        foreach (DataColumn dc in dsClose.Tables[0].Columns)
                        {
                            if (dc.Caption.ToString() != "Row")
                            {
                                if (int.TryParse(dsClose.Tables[0].Rows[i][dc].ToString(), out intTotal) && dc.Caption != "UserName" && dc.Caption != "unitName" && dc.Caption != "Averagehandlingtime" && dc.Caption != "RoleName" && dc.Caption != "Target")
                                {
                                    totalRow = totalRow + intTotal;
                                    strBody.Append("<td><div style='text-align: center;'>" + intTotal + "</div></td>");
                                }
                                else
                                {
                                    strBody.Append("<td><div style='text-align: center;'>" + dsClose.Tables[0].Rows[i][dc] + "</div></td>");
                                }
                            }
                        }
                        strBody.Append("<td><div style='text-align: center;'>" + totalRow + "</div></td>");
                        if (string.IsNullOrEmpty(dsClose.Tables[0].Rows[i]["Target"].ToString()) || Convert.ToInt32(dsClose.Tables[0].Rows[i]["Target"].ToString()) == 0)
                        {
                            strBody.Append("<td><div style='text-align: center;'>&nbsp;</div></td>");
                            strBody.Append("<td><div style='text-align: center;'>&nbsp;</div></td>");
                        }
                        else
                        {
                            strBody.Append("<td><div style='text-align: center;'>" + Convert.ToInt32(dsClose.Tables[0].Rows[i]["Target"]) * 26 + "</div></td>");
                            strBody.Append("<td><div style='text-align: center;'>" + Math.Round((Convert.ToDouble(totalRow) / (Convert.ToInt32(dsClose.Tables[0].Rows[i]["Target"]) * 26) * 100), 2) + "</div></td>");
                        }
                        strBody.Append("</tr>");
                        grandtotalRowClose = grandtotalRowClose + totalRow;
                    }
                    strBody.Append("<tr>");
                    foreach (DataColumn dc in dsClose.Tables[0].Columns)
                    {
                        if (dc.Caption.ToString() == "unitName")
                        {
                            strBody.Append("<td><div style='text-align: center;'>Total</div></td>");
                        }
                        else if (dc.Caption.ToString() == "UserName")
                        {
                            strBody.Append("<td><div style='text-align: center;'>&nbsp;</div></td>");
                        }
                        else if (dc.Caption.ToString() == "RoleName")
                        {
                            strBody.Append("<td><div style='text-align: left;'>&nbsp;</div></td>");
                        }
                        else if (dc.Caption.ToString() == "Target")
                        {
                            strBody.Append("<td><div style='text-align: left;'>&nbsp;</div></td>");
                        }
                        //else if (dc.Caption.ToString() == "Averagehandlingtime")
                        //{
                        //    strBody.Append("<td><div style='text-align: center;'>&nbsp;</div></td>");
                        //}
                        else
                        {
                            string tt;
                            tt = dsClose.Tables[0].Compute("sum([" + dc.Caption.ToString() + "])", " ").ToString(); //DS.Tables[0].Compute("Sum( + bktotal + ", " ");
                            strBody.Append("<td><div style='text-align: center;'>" + tt + "</div></td>");
                        }

                    }
                    strBody.Append("<td><div style='text-align: center;'>" + grandtotalRowClose + "</div></td>");
                    strBody.Append("</tr>");
                    strBody.Append("</table>");
                    strBody.Append("</td></tr>");
                }
                return strBody;
            }
        }
        catch (Exception Ex)
        {
            strBody.Append(Ex.Message);
            return strBody;
        }
        return strBody;
    }

    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objAuto.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "UnitName";
                    ddlCity.DataValueField = "UnitID";
                    ddlCity.DataSource = dsLocation.Tables[0];
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
}