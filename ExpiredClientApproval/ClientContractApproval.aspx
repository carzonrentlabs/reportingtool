<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClientContractApproval.aspx.cs" Inherits="CorDriveReport_ExpiredClientApproval_ClientContractApproval"
    Title="Client Contract Approval Form" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

    <script type="text/javascript">
        $(function()
        {
         $("#<%=btnApprove.ClientID%>").click(function () {
                var chkboxrowcount = $("#<%=gvExpiredContact.ClientID%> input[id*='chkRow']:checkbox:checked").size();
                 var totalRecord = $("table[id*=gvExpiredContact]").find("TR").length - 1;
                if (chkboxrowcount == 0) {
                    alert("Please select at least one client .");
                    return false;
                }
                else {

                    var r = confirm("You have selected " + chkboxrowcount + " records.");              
                    var total = 0;
                    var id = 0;  
                    if (r == true) {
                        for (var i = 2; i <= totalRecord; i++) {
                            if (i.toString().length == 1) {
                                id = "0" + i;
                            }
                            else {
                                id = i;
                            }
                            var isChecked = $("#ctl00_cphPage_gvExpiredContact_ctl" + id + "_chkRow").is(':checked');
                             
                            if ($("#ctl00_cphPage_gvExpiredContact_ctl" + id + "_txtRemarks").val() == "" && isChecked == true) {
                                total = eval(total + 1);
                                alert("Enter remarks");
                                $("#ctl00_cphPage_gvExpiredContact_ctl" + id + "_txtRemarks").focus();
                                return false;
                            }
                        }
                        if (total == 0) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        return false;
                    }
                }
            });
        });
    
        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                    $("td", $(this).closest("tr")).addClass("selected");
                } else {
                    $(this).removeAttr("checked");
                    $("td", $(this).closest("tr")).removeClass("selected");
                }
            });
        });
        $("[id*=chkRow]").live("click", function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                $("td", $(this).closest("tr")).removeClass("selected");
                chkHeader.removeAttr("checked");
            } else {
                $("td", $(this).closest("tr")).addClass("selected");
                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
    </script>

    <div align="center">
        <h3>
            Client Contract Approval Form
        </h3>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <asp:GridView ID="gvExpiredContact" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Sr.No">
                    <ItemTemplate>
                        <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkHeader" runat="server"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkRow" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Client Name">
                    <ItemTemplate>
                        <asp:Label ID="lblClientName" runat="server" Text='<%# Bind("ClientName") %>'></asp:Label>\
                        <asp:HiddenField ID="hdClientId" runat="server" Value='<%# Bind("ClientCoID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City Name">
                    <ItemTemplate>
                        <asp:Label ID="lblCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Contract Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblContractStartDate" runat="server" Text='<%# Bind("ContractStartDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Contract End Date">
                    <ItemTemplate>
                        <asp:Label ID="lblContractEndDate" runat="server" Text='<%# Bind("ContractEndDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="De Activation Date">
                    <ItemTemplate>
                        <asp:Label ID="lblDeActivationDate" runat="server" Text='<%# Bind("DeActivationDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remarks">
                    <ItemTemplate>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="White" ForeColor="#330099" />
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
        </asp:GridView>
        <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click"
            Visible="false" />
    </div>
</asp:Content>
