<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DriverEnderReportNew.aspx.cs" Inherits="DriverEnderReportNew" Title="Cor Drive Inovice Report" %>

<asp:Content ID="chInvoiceReport" ContentPlaceHolderID="cphPage" runat="Server">

    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>

    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>

    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>

    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();

            $("#<%=grdInvoice.ClientID%> a[id*='lnkView']").click(function () {
                var caption = "Map View";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var BookingId = linkId.replace("lnkView", "hd_CarId")
                var BookingId = $("#" + BookingId).val();
                //alert(hdImageID);
                var clientCoindivId = BookingId.split("_");
                var url = "http://insta.carzonrent.com/cormetergoogleAPI/MapWithSatelliteView.aspx?DutySlipNo=" + BookingId
                //return GB_showFullScreen(caption, url)
                return GB_showCenter(caption, url, 400, 800)
            });

            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                    alert("Please Select the From Date");
                    document.getElementById('<%=txtFromDate.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                    alert("Please Select the To Date");
                    document.getElementById('<%=txtToDate.ClientID %>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            $("#<%=bntExprot.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                    alert("Please Select the From Date");
                    document.getElementById('<%=txtFromDate.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                    alert("Please Select the To Date");
                    document.getElementById('<%=txtToDate.ClientID %>').focus();
                    return false;
                }
            });
        });
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() 
        {  
            setTimeout(function () 
            {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            },200);
        }

//        $('form').live("submit", function () {
//            ShowProgress();
//        });
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Driver Ended Report</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Location</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlBranch" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup From Date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup To date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Status</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlStatus" runat ="server" >
                            <asp:ListItem Value ="A" Text ="--All--"></asp:ListItem>
                            <asp:ListItem Value ="O"  Text ="Open"></asp:ListItem>
                            <asp:ListItem Value ="C"  Text ="Close"></asp:ListItem>
                            <asp:ListItem Value ="P"  Text ="Pending"></asp:ListItem>
                            <asp:ListItem Value ="N"  Text ="Now Show"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                    <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div class="Repeater" style="width: 1350px; height: 400px; overflow: auto">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdInvoice" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdInvoice_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="SNo.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.DataItemIndex+1  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="BookingId" DataField="BookingID" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Company Name" DataField="ClientCoName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cor Drive Start Date" DataField="DriverStartDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cor Drive Start Time" DataField="DriverStartTime" HeaderStyle-Wrap="false" />
                                <asp:TemplateField HeaderText="Cor Drive Start Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_DriverStartLocation" runat="server" Text='<% #Eval("DriverStartLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Cor Drive Pick Up Date" DataField="PassengerStartDate"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cor Drive Pick Up Time" DataField="PassengerStartTime"
                                    HeaderStyle-Wrap="false" />
                                <asp:TemplateField HeaderText="Cor Drive Pick Up Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PassengerStartLocation" runat="server" Text='<% #Eval("PassengerUpLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Cor Drive Drop Off Date" DataField="PassengerEndtDate"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cor Drive Drop Off Time" DataField="PassengerEndTime"
                                    HeaderStyle-Wrap="false" />
                                <asp:TemplateField HeaderText="Cor Drive Drop Off Loaction">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_PassengerDropoffLocation" runat="server" Text='<% #Eval("PassengerDropoffLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Cor Drive Driver End Date" DataField="DriverEndDate"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Cor Driver End Time" DataField="DriverEndTime" HeaderStyle-Wrap="false" />
                                <asp:TemplateField HeaderText="Cor Driver End  Loaction">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_DriverEndLocation" runat="server" Text='<% #Eval("DriverEndLocation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Service" DataField="Service" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Car No" DataField="CarRegNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Driver Name" DataField="chauffeurName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Driver Mobile" DataField="chauffeurMobileNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Tolls" DataField="Tolls" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Park Toll Charges" DataField="ParkTollChages" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="InterState Tax" DataField="InterstateTax" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Garage Run 1 = G to P Time (COR Drive)" DataField="GtoPTime"
                                    HeaderStyle-Wrap="true" />
                                <asp:BoundField HeaderText="Garage Run 1 = G to P KM (COR Drive)" DataField="GtoPKM"
                                    HeaderStyle-Wrap="true" />
                                <asp:BoundField HeaderText="Cor Drive P to P Uage Time" DataField="PtoPTime" HeaderStyle-Wrap="true" />
                                <asp:BoundField HeaderText="COR Drive P to P Uage KM" DataField="PtoPKM" HeaderStyle-Wrap="true" />
                                <asp:BoundField HeaderText="Garage Run 2 = D to G Time (COR Drive)" DataField="DtoGTime"
                                    HeaderStyle-Wrap="true" />
                                <asp:BoundField HeaderText="Garage Run 2 = D to G KM (COR Drive)" DataField="DtoGKM"
                                    HeaderStyle-Wrap="true" />
                                <asp:BoundField HeaderText="COR Drive Billed Hours" DataField="CordriveBillHour"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="COR Drived Billed KM" DataField="CorDriveBillKM" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Status" DataField="BookingStatus" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="COR Drive Package Applied" DataField="cordrivePkgApplied"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="COR Drive Billed Amount" DataField="CorDriveBillAmount"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Billing Basis" DataField="BillingBasis" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" HeaderStyle-Wrap="false" />
                                <asp:TemplateField HeaderText="Routing Map">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkView" runat="server" Text="View"></asp:LinkButton>
                                        <asp:HiddenField ID="hd_CarId" runat="server" Value='<% #Eval("BookingID") %>' />
                                        <asp:HiddenField ID="hd_Dlat" runat="server" Value='<% #Eval("DriverStartLat") %>' />
                                        <asp:HiddenField ID="hd_DLon" runat="server" Value='<% #Eval("DriverStartLon") %>' />
                                        <asp:HiddenField ID="hd_Plat" runat="server" Value='<% #Eval("PassengerEndLat") %>' />
                                        <asp:HiddenField ID="hd_PLon" runat="server" Value='<% #Eval("PassengerEndLon") %>' />
                                        <asp:HiddenField ID="hd_PSLat" runat="server" Value='<% #Eval("PassengerStartLat") %>' />
                                        <asp:HiddenField ID="hd_PSLon" runat="server" Value='<% #Eval("PassengerStartLon") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
