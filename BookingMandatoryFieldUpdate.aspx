﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingMandatoryFieldUpdate.aspx.cs"
    Inherits="BookingMandatoryFieldUpdate" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script language="Javascript" src="App_Themes/CommonScript.js" type="text/jscript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/jscript"></script>
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <link href="../greybox/gb_styles.css" rel="stylesheet" type="text/css" />


    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=bntGet.ClientID%>").click(function () {
                if ($("#<%=txtBookingId.ClientID%>").val() == "") {
                    alert("Enter booking Id");
                    return false;
                }
            });

            $("#<%=txtBookingId.ClientID%>").keyup(function () {
                var bookingID = this.id;
                var strPass = $("#" + bookingID).val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#" + bookingID).val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            $("#<%=lnkViewEmail.ClientID%>,#<%=lnkVRF.ClientID%>,#<%=lnkViewAddEmail.ClientID%>,#<%=lnkAddVRF.ClientID%>").click(function () {

                // debugger;
                var caption = "Email";
                var linkId = this.id
                var type = 1;
                var bookingId = $("#<%=txtBookingId.ClientID%>").val();
                if (linkId.includes("lnkViewEmail")) {
                    type = 1;
                }
                else if (linkId.includes("lnkVRF")) {
                    type = 2;
                }
                else if (linkId.includes("lnkViewAddEmail")) {
                    type = 3;
                }
                else if (linkId.includes("lnkAddVRF")) {
                    type = 4;
                }


                if (bookingId == 0) {

                    var url = "images/FileNA.pdf"
                }
                else {

                    var url = "../ImageViewer.aspx?BookingID=" + bookingId.toString() + "&MailYN=1&type=" + type;
                    // var url = "http://insta.carzonrent.com/CORCallTaker/Invoice/PrintInvoice?onlyDS=1&BookingID=" + bookingId.toString();                   
                    // var url = "http://insta.carzonrent.com/MailUpload/Mails/" + bookingId  + ".pdf";

                }
                //alert(url);
                // return GB_showFullScreen(caption, url)
                return GB_showCenter(caption, url, 500, 700)
            });




            $("#<%=bntSubmit.ClientID%>").click(function () {

                if ($("#<%=lblMisc1.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc1.ClientID%>").css("display") == "block" && $("#<%=txtMisc1.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 1");
                        return false;
                    }
                    else if ($("#<%=ddlMisc1.ClientID%>").css("display") == "block" && $("#<%=ddlMisc1.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 1");
                        return false;
                    }

                }
                if ($("#<%=lblMisc2.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc2.ClientID%>").css("display") == "block" && $("#<%=txtMisc2.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 2");
                        return false;
                    }
                    else if ($("#<%=ddlMisc2.ClientID%>").css("display") == "block" && $("#<%=ddlMisc2.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 2");
                        return false;
                    }
                }
                if ($("#<%=lblMisc3.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc3.ClientID%>").css("display") == "block" && $("#<%=txtMisc3.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 3");
                        return false;
                    }
                    else if ($("#<%=ddlMisc3.ClientID%>").css("display") == "block" && $("#<%=ddlMisc3.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 3");
                        return false;
                    }
                }
                if ($("#<%=lblMisc4.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc4.ClientID%>").css("display") == "block" && $("#<%=txtMisc4.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 4");
                        return false;
                    }
                    else if ($("#<%=ddlMisc4.ClientID%>").css("display") == "block" && $("#<%=ddlMisc4.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 4");
                        return false;
                    }
                }
                if ($("#<%=lblMisc5.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc5.ClientID%>").css("display") == "block" && $("#<%=txtMisc5.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 5");
                        return false;
                    }
                    else if ($("#<%=ddlMisc5.ClientID%>").css("display") == "block" && $("#<%=ddlMisc5.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 5");
                        return false;
                    }
                }
                if ($("#<%=lblMisc6.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc6.ClientID%>").css("display") == "block" && $("#<%=txtMisc6.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 6");
                        return false;
                    }
                    else if ($("#<%=ddlMisc6.ClientID%>").css("display") == "block" && $("#<%=ddlMisc6.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 6");
                        return false;
                    }
                }
                if ($("#<%=lblMisc7.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc7.ClientID%>").css("display") == "block" && $("#<%=txtMisc7.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 7");
                        return false;
                    }
                    else if ($("#<%=ddlMisc7.ClientID%>").css("display") == "block" && $("#<%=ddlMisc7.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 7");
                        return false;
                    }
                }
                if ($("#<%=lblMisc8.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc8.ClientID%>").css("display") == "block" && $("#<%=txtMisc8.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 8");
                        return false;
                    }
                    else if ($("#<%=ddlMisc8.ClientID%>").css("display") == "block" && $("#<%=ddlMisc8.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 8");
                        return false;
                    }
                }
                if ($("#<%=lblMisc9.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc9.ClientID%>").css("display") == "block" && $("#<%=txtMisc9.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 9");
                        return false;
                    }
                    else if ($("#<%=ddlMisc9.ClientID%>").css("display") == "block" && $("#<%=ddlMisc9.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 9");
                        return false;
                    }
                }
                if ($("#<%=lblMisc10.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc10.ClientID%>").css("display") == "block" && $("#<%=txtMisc10.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 10");
                        return false;
                    }
                    else if ($("#<%=ddlMisc10.ClientID%>").css("display") == "block" && $("#<%=ddlMisc10.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 10");
                        return false;
                    }
                }
                if ($("#<%=lblMisc11.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc11.ClientID%>").css("display") == "block" && $("#<%=txtMisc11.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 11");
                        return false;
                    }
                    else if ($("#<%=ddlMisc11.ClientID%>").css("display") == "block" && $("#<%=ddlMisc11.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 11");
                        return false;
                    }
                }
                if ($("#<%=lblMisc12.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc12.ClientID%>").css("display") == "block" && $("#<%=txtMisc12.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 12");
                        return false;
                    }
                    else if ($("#<%=ddlMisc12.ClientID%>").css("display") == "block" && $("#<%=ddlMisc12.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 12");
                        return false;
                    }
                }
                if ($("#<%=lblMisc13.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc13.ClientID%>").css("display") == "block" && $("#<%=txtMisc13.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 13");
                        return false;
                    }
                    else if ($("#<%=ddlMisc13.ClientID%>").css("display") == "block" && $("#<%=ddlMisc13.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 13");
                        return false;
                    }
                }
                if ($("#<%=lblMisc14.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc14.ClientID%>").css("display") == "block" && $("#<%=txtMisc14.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 14");
                        return false;
                    }
                    else if ($("#<%=ddlMisc14.ClientID%>").css("display") == "block" && $("#<%=ddlMisc14.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 14");
                        return false;
                    }
                }
                if ($("#<%=lblMisc15.ClientID%>").html() != "") {
                    if ($("#<%=txtMisc15.ClientID%>").css("display") == "block" && $("#<%=txtMisc15.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 15");
                        return false;
                    }
                    else if ($("#<%=ddlMisc15.ClientID%>").css("display") == "block" && $("#<%=ddlMisc15.ClientID%>").val() == "") {
                        alert("enter Miscellaneous 15");
                        return false;
                    }
                }

                ShowProgress();

            });
            $("#<%=flEmailUpload.ClientID%>,#<%=flVRFUpload.ClientID%>").change(function () {
                var maxFileSize =  1597152; // 4MB -> 4 * 1024 * 1024
                var fileUpload = $(this);
               // alert(fileUpload);
                if (fileUpload.val() == '') {
                    return false;
                }
                else {
                    if (fileUpload[0].files[0].size < maxFileSize) {
                        $('#<%=btnUploadDOC.ClientID%>').prop('disabled', false);
                        return true;
                    } else {
                        alert("File size should not more than 2 MB")
                        $(this).val("");
                        $('#<%=lblMessage.ClientID%>').text('File too big !')
                        return false;
                    }
                }

            });


        });
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

    </script>
    <style type="text/css">
        div.ex1 {
            float: left;
            width: 200px;
            height: 30px;
            margin: 2px; /*border: 3px solid #73AD21;*/
            white-space: nowrap;
            text-align: left;
        }

        div.ex2 {
            float: right;
            width: 200px;
            height: 30px;
            margin: 2px; /*border: 3px solid #73AD21;*/
            white-space: nowrap;
            text-align: left;
        }
    </style>
    <table width="100%" border="0">
        <tr>
            <td colspan="3" align="center">
                <b>Update Mandatory Flields</b>
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space: nowrap">
                <strong><u>Booking Id</u></strong>&nbsp;&nbsp;
                <asp:TextBox ID="txtBookingId" runat="server" MaxLength="10"></asp:TextBox>&nbsp;&nbsp;
                  <asp:Button ID="bntGet" runat="server" Text="Get It" OnClick="bntGet_Click" />
            </td>
            <td align="left">
                <asp:RadioButtonList ID="rdupdate" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Mandatory Fields" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Email/VRF Upload" Value="1"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td align="left">
                <b>Client</b>&nbsp;
                <asp:DropDownList ID="ddlClientName" runat="server" Width="200px"></asp:DropDownList>&nbsp;
                <asp:ImageButton ID="ImgFormat" runat="server" ImageUrl="images/excel.jpg" OnClick="ImgFormat_Click" />&nbsp;&nbsp;
                <asp:FileUpload ID="fldUploadMandate" runat="server" accept="application/xlsx"/>
                <asp:Button ID="btnUPloadExcel" runat="server" Text="Upload"  OnClick="btnUPloadExcel_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Panel ID="pnlUploadDOC" runat="server" Visible="false">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <strong>Email Required</strong>&nbsp;&nbsp;
                                <asp:Image runat="server" ID="ImageEmailPreview" Height="15px" Width="15px" />
                                <asp:FileUpload ID="flEmailUpload" runat="server" accept="application/pdf" />
                            </td>
                            <td style="white-space: nowrap;">
                                <asp:RadioButtonList ID="rblEditaddReplace" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="Add" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Replace"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:LinkButton ID="lnkViewEmail" Text="View Email" runat="server" Visible="false" ToolTip="Click to view email"></asp:LinkButton>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lnkViewAddEmail" Text="View Added Email" runat="server" Visible="false" ToolTip="Click to view added email"></asp:LinkButton>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>VRF Required</strong>&nbsp;&nbsp;
                             <asp:Image runat="server" ID="ImageVRFPreview" Height="15px" Width="15px" />
                                <asp:FileUpload ID="flVRFUpload" runat="server" accept="application/pdf" />
                            </td>
                            <td style="white-space: nowrap;">
                                <asp:RadioButtonList ID="rblVRFaddReplace" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Text="Add" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Replace"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:LinkButton ID="lnkVRF" Text="View VRF" runat="server" Visible="false" ToolTip="Click to view VRF"></asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkAddVRF" Text="View Added VRF" runat="server" Visible="false" ToolTip="Click to view added VRF file"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                <asp:Button ID="btnUploadDOC" runat="server" Text="Upload File" OnClick="btnUploadDOC_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <center>
        <asp:Panel ID="pnlMandatory" runat="server" Visible="false">
            <div style="width: 40%">
                <div runat="server" id="tr1" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous1" runat="server" Text="Miscellaneous1"></asp:Label>&nbsp;--%>
                        <b>Miscellaneous1</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc1" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc1" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc1" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr2" style="display: none;">
                    <div class="ex1">
                        <%-- <asp:Label ID="lblMiscellaneous2" runat="server" Text="Miscellaneous2"></asp:Label>&nbsp;--%>
                        <b>Miscellaneous2</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc2" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc2" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc2" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <div runat="server" id="tr3" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous3" runat="server" Text="Miscellaneous3"></asp:Label>--%>
                        <b>Miscellaneous3</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc3" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc3" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc3" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr4" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous4" runat="server" Text="Miscellaneous4"></asp:Label>--%>
                        <b>Miscellaneous4</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc4" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc4" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc4" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr5" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous5" runat="server" Text="Miscellaneous5"></asp:Label>--%>
                        <b>Miscellaneous5</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc5" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc5" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc5" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr6" style="display: none;">
                    <div class="ex1">
                        <%-- <asp:Label ID="lblMiscellaneous6" runat="server" Text="Miscellaneous6"></asp:Label>--%>
                        <b>Miscellaneous6</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc6" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc6" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc6" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr7" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous7" runat="server" Text="Miscellaneous7"></asp:Label>--%>
                        <b>Miscellaneous7</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc7" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc7" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc7" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr8" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous8" runat="server" Text="Miscellaneous8"></asp:Label>--%>
                        <b>Miscellaneous8</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc8" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc8" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc8" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr9" style="display: none;">
                    <div class="ex1">
                        <%-- <asp:Label ID="lblMiscellaneous9" runat="server" Text="Miscellaneous9"></asp:Label>--%>
                        <b>Miscellaneous9</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc9" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc9" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc9" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr10" style="display: none;">
                    <div class="ex1">
                        <%--<asp:Label ID="lblMiscellaneous10" runat="server" Text="Miscellaneous10"></asp:Label>--%>
                        <b>Miscellaneous10</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc10" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc10" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc10" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr11" style="display: none;">
                    <div class="ex1">
                        <b>Miscellaneous11</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc11" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc11" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc11" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr12" style="display: none;">
                    <div class="ex1">
                        <b>Miscellaneous12</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc12" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc12" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc12" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr13" style="display: none;">
                    <div class="ex1">
                        <b>Miscellaneous13</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc13" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc13" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc13" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr14" style="display: none;">
                    <div class="ex1">
                        <b>Miscellaneous14</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc14" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc14" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc14" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="tr15" style="display: none;">
                    <div class="ex1">
                        <b>Miscellaneous15</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc15" runat="server"></asp:Label>
                    </div>
                    <div class="ex2">
                        <asp:TextBox ID="txtMisc15" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:DropDownList ID="ddlMisc15" runat="server" Style="display: none;">
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div runat="server" id="trSubmit" style="display: none;">
                    <div style="text-align: center;">
                        <asp:Button ID="bntSubmit" runat="Server" Text="Update" OnClick="bntSubmit_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
