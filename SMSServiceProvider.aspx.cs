﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ReportingTool;
using System.Drawing;

public partial class SMSServiceProvider : System.Web.UI.Page
{
    private SMSProvider objSMSProvider = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Convert.ToInt16(rdAddEdit.SelectedValue) == 1)
            {
                AddProvider();
                lblMessage.Text = "";
            }
            else
            {
                EditProvider();
                lblMessage.Text = "";
            }
            bntSubmit.Attributes.Add("onclick", "return validate()");
        }
        bntSubmit.Attributes.Add("onclick", "return validate()");

    }
    protected void rdAddEdit_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt16(rdAddEdit.SelectedValue) == 1)
        {
            AddProvider();
        }
        else
        {
            EditProvider();
        }


    }

    private void AddProvider()
    {
        try
        {
            //ddlProviderName.Visible = false;
            //txtProviderName.Visible = true;
            ddlProviderName.Style.Add("display", "none");
            txtProviderName.Style.Add("display", "block");
            Emptycontrol();

        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
        }
    }

    private void EditProvider()
    {
        try
        {
            //ddlProviderName.Visible = true;
            //txtProviderName.Visible = false;
            ddlProviderName.Style.Add("display", "block");
            txtProviderName.Style.Add("display", "none");
            BindProviderName();

        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
        }
    }

    private void BindProviderName()
    {
        objSMSProvider = new SMSProvider();
        DataTable dtProviderName = new DataTable();
        try
        {
            dtProviderName = objSMSProvider.GetSMSProviderName();
            if (dtProviderName.Rows.Count > 0)
            {
                ddlProviderName.DataSource = dtProviderName;
                ddlProviderName.DataTextField = "ProviderName";
                ddlProviderName.DataValueField = "ID";
                ddlProviderName.DataBind();
                ddlProviderName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlProviderName.DataSource = null;
                ddlProviderName.DataBind();
            }
        }
        catch (Exception Ex)
        {

            throw new Exception(Ex.Message);
        }
    }
    protected void ddlProviderName_SelectedIndexChanged(object sender, EventArgs e)
    {
        objSMSProvider = new SMSProvider();
        DataTable dtProviderDetails = new DataTable();
        if (ddlProviderName.SelectedIndex > 0)
        {
            lblMessage.Text = "";
            try
            {
                dtProviderDetails = objSMSProvider.GetProviderDetails(Convert.ToInt16(ddlProviderName.SelectedValue));
                if (dtProviderDetails.Rows.Count > 0)
                {
                    txtProviderUrl.Text = dtProviderDetails.Rows[0]["URL"].ToString();
                    txtProviderUserName.Text = dtProviderDetails.Rows[0]["UserName"].ToString();
                    txtProviderPassword.Text = dtProviderDetails.Rows[0]["password"].ToString();
                    txtGenderGSM.Text = dtProviderDetails.Rows[0]["SenderGSM"].ToString();
                    ddlActive.SelectedValue = dtProviderDetails.Rows[0]["Active"].ToString();
                    ddlPriority.SelectedValue = dtProviderDetails.Rows[0]["Priority"].ToString();
                }
                else
                {
                    lblMessage.Text = "Record not availalbe.";
                    lblMessage.ForeColor = Color.Red;
                }


            }
            catch (Exception Ex)
            {

                lblMessage.Text = Ex.Message;
            }


        }
        else
        {
            Emptycontrol();
            lblMessage.Text = "Select provider name";
            lblMessage.ForeColor = Color.Red;
        }

    }
    protected void bntSubmit_Click(object sender, EventArgs e)
    {
        objSMSProvider = new SMSProvider();
        DataTable dtPriority = new DataTable();
        if (Convert.ToInt16(rdAddEdit.SelectedValue) == 1)
        {
            dtPriority = objSMSProvider.CheckPriorityOfSMS(Convert.ToInt16(ddlPriority.SelectedValue));
        }
        else
        {
            dtPriority = objSMSProvider.CheckPriorityOfSMS(Convert.ToInt16(ddlPriority.SelectedValue),Convert.ToInt16(ddlProviderName.SelectedValue));
        }
        int status;

        if (ddlPriority.SelectedIndex == 0)
        {
            lblMessage.Text = "Select priority.";
            lblMessage.ForeColor = Color.Red;
            return;
        }
        else if (dtPriority.Rows.Count > 0)
        {
            lblMessage.Text = "Priority already given to another provider.";
            lblMessage.ForeColor = Color.Red;
            return;
        }
        else
        {
            lblMessage.Text = "";
            if (Convert.ToInt16(rdAddEdit.SelectedValue) == 1)
            {
                try
                {
                    status = objSMSProvider.InsertProviderDetails(txtProviderName.Text, txtProviderUrl.Text, txtProviderUserName.Text,
                        txtProviderPassword.Text, txtGenderGSM.Text, Convert.ToInt16(ddlActive.SelectedValue), Convert.ToInt16(ddlPriority.SelectedValue), Convert.ToInt32(Session["UserID"]));
                    if (status == 1)
                    {
                        lblMessage.Text = "This provider already exists.";
                        lblMessage.ForeColor = Color.Red;
                    }
                    else if (status > 1)
                    {
                        lblMessage.Text = "Provider added successfully.";
                        lblMessage.ForeColor = Color.Red;
                        Emptycontrol();
                    }

                    else
                    {
                        lblMessage.Text = "Getting error";
                        lblMessage.ForeColor = Color.Red;
                    }


                }
                catch (Exception Ex)
                {
                    lblMessage.Text = Ex.Message;
                }
            }
            else
            {
                try
                {
                    status = objSMSProvider.EditProviderDetails(Convert.ToInt16(ddlProviderName.SelectedValue), txtProviderUrl.Text, txtProviderUserName.Text,
                        txtProviderPassword.Text, txtGenderGSM.Text, Convert.ToInt16(ddlActive.SelectedValue), Convert.ToInt16(ddlPriority.SelectedValue), Convert.ToInt32(Session["UserID"]));
                    if (status > 0)
                    {
                        lblMessage.Text = "Edit successfully.";
                        lblMessage.ForeColor = Color.Red;
                    }
                    else
                    {
                        lblMessage.Text = "Getting error";
                        lblMessage.ForeColor = Color.Red;
                    }
                }
                catch (Exception Ex)
                {
                    lblMessage.Text = Ex.Message;
                }
            }
        }

    }

    private void Emptycontrol()
    {
        txtProviderName.Text = "";
        txtProviderUrl.Text = "";
        txtProviderUserName.Text = "";
        txtProviderPassword.Text = "";
        txtGenderGSM.Text = "CRZRNT";
        ddlActive.SelectedValue = "1";
        ddlPriority.SelectedValue = "0";

        //foreach (Control control in objcontrol.Controls)
        //{
        //    if ((control.GetType() == typeof(TextBox)))
        //    {
        //        ((TextBox)(control)).Text = "";
        //    }
        //    if ((control.GetType() == typeof(DropDownList)))
        //    {
        //        if (((DropDownList)(control)).Items.Count > 0)
        //        {
        //            ((DropDownList)(control)).ClearSelection();
        //            ((DropDownList)(control)).SelectedIndex = 0;
        //        }
        //    }
        //    if ((control.GetType() == typeof(CheckBox)))
        //    {
        //        ((CheckBox)(control)).Checked = false;
        //    }

        //    if (control.HasControls() && control.GetType() != typeof(MultiView))
        //    {
        //        InitializeControls(control);
        //    }
        //}
    }

}