﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_DayWiseReportForEasycabs : System.Web.UI.Page
{
   
    DataSet dsDayWiseReport = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    public void BindGrid()
    {
        try
        {
            objCordrive = new CorDrive();
            dsDayWiseReport = objCordrive.GetDaywiseReportforEasycabs(Convert.ToDateTime(txtPickUpDate.Text.Trim()),Convert.ToInt32(ddlCity.SelectedValue));
            if (dsDayWiseReport.Tables[0].Rows.Count > 0)
            {

                gvEasycabsDayWiseBooking.DataSource = dsDayWiseReport.Tables[0];
                gvEasycabsDayWiseBooking.DataBind();

                gvCreatedByUser.DataSource = dsDayWiseReport.Tables[1];
                gvCreatedByUser.DataBind();
            }
            else
            {
                gvEasycabsDayWiseBooking.DataSource = null;
                gvEasycabsDayWiseBooking.DataBind();

                gvCreatedByUser.DataSource = null;
                gvCreatedByUser.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
}