<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BranchPerformanceReport.aspx.cs" Inherits="BranchPerformanceReport"  MasterPageFile ="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
    window.history.forward(1);   
        function validate()
        {
            if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
            {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }
                    
            if(document.getElementById('<%=txtTo.ClientID %>').value == "")
            {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>

    <table width="80%" border="0" align="center" id="table1">
        <tbody>
            <tr>
                <td align="center" colspan="10" style="height: 18px">
                    <strong><u>Branch Performance Report</u></strong>&nbsp;</td>
            </tr>
            <tr>
                <td align="left" style="width: 62px; height: 40px;">
                    From Date</td>
                <td align="left" style="width: 93px; height: 40px;">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px; height: 40px;">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 64px; height: 40px;">
                    To Date</td>
                <td align="left" style="width: 5px; height: 40px;">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 20px; height: 40px;">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right">
                    City Name:</td>
                <td style="width: 210px">
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList></td>
                <td align="left" style="height: 22px">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Get >>" OnClick="btnSubmit_Click" />
                    &nbsp;
                    <asp:Button ID="bntExport" runat="server" Text="Exprot To Excel" OnClick="bntExport_Click"/>
                </td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
            </tr>
        </tbody>
    </table>
    <div>
        <asp:GridView ID="grvPerformaneReport" runat="server" AutoGenerateColumns="true"
            BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2"
            ForeColor="Black" GridLines="Both">
            <FooterStyle BackColor="Tan" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
        </asp:GridView>
    </div>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
