using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;

public partial class CorDriveReport_CorDriveInvoiceValidation : System.Web.UI.Page
{

    CorDrive objCordrive = new CorDrive();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCityDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");

    }
    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation.Tables[0];
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();

    }
    private void BindGrid()
    {
        DataSet ds = new DataSet();
        lblMessage.Visible = false;
        ds = objCordrive.GetCorDriveInovieCloseDetails(Convert.ToInt32(ddlCity.SelectedValue.ToString()), Convert.ToDateTime(txtFromDate.Text.ToString()), Convert.ToDateTime(txtToDate.Text.ToString()));
        if (ds.Tables[0].Rows.Count > 0)
        {
            grvValidation.DataSource = ds.Tables[0];
            grvValidation.DataBind();
        }
        else
        {
            grvValidation.DataSource = null;
            grvValidation.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = "Record not available";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            grvValidation.AllowPaging = false;
            this.BindGrid();

            grvValidation.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grvValidation.HeaderRow.Cells)
            {
                cell.BackColor = grvValidation.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grvValidation.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grvValidation.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grvValidation.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grvValidation.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
    protected void grvValidation_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvValidation.PageIndex = e.NewPageIndex;
        //this.BindGrid();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}
