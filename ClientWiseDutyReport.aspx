<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ClientWiseDutyReport.aspx.cs" Inherits="ClientWiseDutyReport" Title="Untitled Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">


    </script>

    <script language="javascript" type="text/javascript">
window.history.forward(1);
    </script>

    <table width="802" border="0" align="center" id="TABLE1">
        <tbody>
            <tr>
                <td align="center" colspan="8" style="height: 18px">
                    <strong><u>Client Wise Duty And Revenue Report</u></strong>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Get Details" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:ImageButton ID="btnExportToExcel" runat="server" ImageUrl="~/images/excel.jpg"
                        ToolTip="click to download excel format" Height="20px" OnClick="btnExportToExcel_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <asp:GridView ID="grv_DutySummary" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnRowDataBound="grv_DutySummary_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="S.No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Client Name" DataField="ClientName" />
                            <asp:BoundField HeaderText="Total Booking" DataField="TotalBooking" />
                            <asp:BoundField HeaderText="Total Dispatch" DataField="TotalDispatch" />
                            <asp:BoundField HeaderText="Total Close" DataField="TotalClose" />
                            <asp:BoundField HeaderText="Total Revenue" DataField="TotalRevenue" />
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
