using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
using System.Data.SqlClient;

public partial class CorDriveReport_ExpiredClientApproval_ClientContractApproval : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Userid"])))
            {
                BindExpiredClientData(Convert.ToInt32(Session["Userid"]));
            }
            else
            {
                BindExpiredClientData(0);
            }

        }
    }
    private void BindExpiredClientData(int SysUserId)
    {
        try
        {
            DataTable objDt = new DataTable();
            objDt = objAdmin.GetExpiredClientContractData(SysUserId);
            if (objDt != null)
            {
                if (objDt.Rows.Count > 0)
                {
                    btnApprove.Visible = true;
                    gvExpiredContact.DataSource = objDt;
                    gvExpiredContact.DataBind();
                }
                else
                {
                    btnApprove.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message.ToString());
        }
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow row in gvExpiredContact.Rows)
            {
                CheckBox chk_row = row.FindControl("chkRow") as CheckBox;
                HiddenField _hd_clientCoId = row.FindControl("hdClientId") as HiddenField;
                Label _contractStartDate = row.FindControl("lblContractEndDate") as Label;
                TextBox _remarks = row.FindControl("txtRemarks") as TextBox;

                if (chk_row != null)
                {
                    if (chk_row.Checked == true)
                    {
                        if (_remarks != null)
                        {
                            bool IsApproved = false;
                            IsApproved = UpdateClientContractExpiryDate(Convert.ToInt32(_hd_clientCoId.Value), Convert.ToDateTime(_contractStartDate.Text), Convert.ToInt32(Session["Userid"].ToString()), _remarks.Text);

                            if (IsApproved == true)
                            {
                                lblMessage.Visible = true;
                                lblMessage.Font.Bold = true;
                                lblMessage.ForeColor = System.Drawing.Color.Green;
                                lblMessage.Text = "Client has been approved";
                                if (!string.IsNullOrEmpty(Convert.ToString(Session["Userid"])))
                                {
                                    BindExpiredClientData(Convert.ToInt32(Session["Userid"].ToString()));
                                }
                            }
                            else
                            {
                                lblMessage.Visible = true;
                                lblMessage.Font.Bold = true;
                                lblMessage.ForeColor = System.Drawing.Color.Red;
                                lblMessage.Text = "Client is not approved dut to some error occured..";
                            }
                        }
                        else
                        {

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message.ToString());
        }
    }

    private bool UpdateClientContractExpiryDate(int ClientCoId, DateTime ContractExpiryDate, int ApprovedBy, string Remarks)
    {
        bool _IsUpdated = false;

        try
        {
            int _UpdateStatus = objAdmin.UpdateClientContractDate(ClientCoId, ContractExpiryDate, ApprovedBy, Remarks);

            if (_UpdateStatus > 0)
            {
                _IsUpdated = true;
            }

        }
        catch (Exception ex)
        {
            _IsUpdated = false;
            Console.WriteLine(ex.Message.ToString());
        }
        return _IsUpdated;
    }
}
