using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using ExcelUtil;
public partial class CorDriveReport_BookingDetailsByTransaction : System.Web.UI.Page
{
    DataSet dsBooking = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
        //Session["Userid"] = 3122;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            //if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From Date or To Date can not be empty.')", true);
            //    return;
            //}
            //else
            //{
                objCordrive = new CorDrive();
                dsBooking = objCordrive.GetBookingDetailsByTransaction(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                if (dsBooking.Tables[0].Rows.Count > 0)
                {
                    gvBookingDetails.DataSource = dsBooking.Tables[0];
                    gvBookingDetails.DataBind();
                }
                else
                {
                    gvBookingDetails.DataSource = dsBooking.Tables[0];
                    gvBookingDetails.DataBind();
                }

           // }
           
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From Date and To Date can not be empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsExportToExcel = objCordrive.GetBookingDetailsByTransaction(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
            WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "BookingDetailsByGateway" + ".xls");
        }
    }
}
