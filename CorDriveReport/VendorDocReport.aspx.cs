using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;
public partial class CorDriveReport_VendorDocReport : System.Web.UI.Page
{
    DataSet dsVendorDocDetails = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
       // Session["Userid"] = 3122;
        if(!IsPostBack)
        {
            BindCity();
        }
    }
    public void BindCity()
    {
        DataSet dsLocation = new DataSet();
        try
        {
            objCordrive = new CorDrive();
            dsLocation = objCordrive.GetLocations_UserAccessWise_SummaryCityWiseNew(Convert.ToInt32(Session["UserID"]));
            if (dsLocation.Tables.Count > 0)
            {
                ddlCity.DataTextField = "Cityname";
                ddlCity.DataValueField = "CityId";
                ddlCity.DataSource = dsLocation;
                ddlCity.DataBind();
             
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
        //try
        //{
        //    objCordrive = new CorDrive();
        //    dsVendorDocDetails = objCordrive.GetVendorDocDetials(Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(ddlStatus.SelectedValue));
        //    if (dsVendorDocDetails.Tables[0].Rows.Count > 0)
        //    {
        //        gvVendorDocReport.DataSource = dsVendorDocDetails.Tables[0];
        //        gvVendorDocReport.DataBind();
        //    }
        //    else
        //    {
        //        gvVendorDocReport.DataSource = dsVendorDocDetails.Tables[0];
        //        gvVendorDocReport.DataBind();
        //    }
                      
        //}
        //catch (Exception ex)
        //{
        //    lblMsg.Text = ex.Message.ToString();
        //    lblMsg.Visible = true;
        //}
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindGrid();
        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=VendorDocReport.xls");
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gvVendorDocReport.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

        //dsExportToExcel = objCordrive.GetVendorDocDetials(Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(ddlStatus.SelectedValue));
        //WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "VendorDocReport" + ".xls");
    }
    protected void gvVendorDocReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (string.IsNullOrEmpty(e.Row.Cells[13].Text) == false)
                {
                    if (CheckDate(e.Row.Cells[13].Text.Trim()))
                    {
                        DateTime RCIssueDate = Convert.ToDateTime(e.Row.Cells[13].Text);
                        if (RCIssueDate < System.DateTime.Now)
                        {
                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[13].ForeColor = System.Drawing.Color.Red;

                        }
                    }
                }
                if (string.IsNullOrEmpty(e.Row.Cells[14].Text) == false)
                {
                    if (CheckDate(e.Row.Cells[14].Text.Trim()))
                    {
                        DateTime PermitExpiryDate = Convert.ToDateTime(e.Row.Cells[14].Text);
                        if (PermitExpiryDate < System.DateTime.Now)
                        {
                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[14].ForeColor = System.Drawing.Color.Red;

                        }
                    }
                }
                if(string.IsNullOrEmpty(e.Row.Cells[15].Text.Trim()) == false)
                {
                    if (CheckDate(e.Row.Cells[15].Text.Trim()))
                    {
                        DateTime InsuranceExpiryDate = Convert.ToDateTime(e.Row.Cells[15].Text);
                        // if (Convert.ToDateTime(e.Row.Cells[15].Text) < System.DateTime.Now)
                        if (InsuranceExpiryDate < System.DateTime.Now)
                        {
                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[15].ForeColor = System.Drawing.Color.Red;

                        }
                    }
                }
                if (string.IsNullOrEmpty(e.Row.Cells[16].Text.Trim()) == false)
                {
                    if(CheckDate(e.Row.Cells[16].Text.Trim()))
                    {
                         DateTime FitnessExpiryDate = Convert.ToDateTime(e.Row.Cells[16].Text.Trim());
                         if (FitnessExpiryDate < System.DateTime.Now)
                         {
                             e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                             e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                             e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                             e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                             e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                             e.Row.Cells[16].ForeColor = System.Drawing.Color.Red;

                         }
                    }
                                     
                   
                }
                if (string.IsNullOrEmpty(e.Row.Cells[31].Text) == false)
                {
                    if (CheckDate(e.Row.Cells[31].Text.Trim()))
                    {
                        DateTime DLExpiryDate = Convert.ToDateTime(e.Row.Cells[31].Text);
                        if (DLExpiryDate < System.DateTime.Now)
                        {
                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[31].ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
                if (string.IsNullOrEmpty(e.Row.Cells[20].Text) == false)
                {
                    string PanNo = e.Row.Cells[20].Text.Trim().ToString();
                    System.Text.RegularExpressions.Regex rPan = new System.Text.RegularExpressions.Regex(@"^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$");
                    if (PanNo.Length > 0)
                    {
                        if ((!rPan.IsMatch(PanNo.Trim())) || (PanNo.Length < 10 || PanNo.Length > 10))
                        {
                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[20].ForeColor = System.Drawing.Color.Red;
                        }

                    }

                }
                if (string.IsNullOrEmpty(e.Row.Cells[24].Text) == false)
                {
                    if (CheckDate(e.Row.Cells[24].Text.Trim()))
                    {
                        DateTime AgreementEndDate = Convert.ToDateTime(e.Row.Cells[24].Text);
                        if (AgreementEndDate < System.DateTime.Now)
                        {
                            e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[2].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[3].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[4].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[5].BackColor = System.Drawing.Color.Red;
                            e.Row.Cells[24].ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
               
            }
        }
        catch(Exception ex)
        {
            Response.Write(ex.Message);
            return;
            lblMsg.Text = ex.Message.ToString();
      
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    public void BindGrid()
    {
        try
        {
            objCordrive = new CorDrive();
            dsVendorDocDetails = objCordrive.GetVendorDocDetials(Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(ddlStatus.SelectedValue));
            if (dsVendorDocDetails.Tables[0].Rows.Count > 0)
            {
                gvVendorDocReport.DataSource = dsVendorDocDetails.Tables[0];
                gvVendorDocReport.DataBind();
            }
            else
            {
                gvVendorDocReport.DataSource = dsVendorDocDetails.Tables[0];
                gvVendorDocReport.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
    protected bool CheckDate(String date)
    {
        DateTime Temp;
        if (DateTime.TryParse(date, out Temp) == true)
            return true;
        else
            return false;
    }
}
