﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SSCBatchCreationWithin72Hours.aspx.cs"
    Inherits="SSCBatchCreationWithin72Hours" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <link href="DatePickerCSS/ui.all.css" rel="stylesheet" type="text/css" />
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        window.history.forward(1);
        function validate() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Select From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Select To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
           /* else if (document.getElementById('<%=ddlLocation.ClientID %>').selectedIndex == 0) {
                alert("select Location.");
                document.getElementById('<%=ddlLocation.ClientID %>').focus();
                return false;
            
            }*/
            else if (Date.parse(document.getElementById('<%=txtFrom.ClientID %>').value) > Date.parse(document.getElementById('<%=txtTo.ClientID %>').value)) {
                alert("From date should be greather than to date.")
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }
        }
        $(document).ready(function () {
            $("#<%=txtTo.ClientID %>").datepicker();
            $("#<%=txtFrom.ClientID %>").datepicker();
        });
  
    </script>
    <table width="60%" border="0" align="center" id="table1">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>SSC Batch Creataion Delay </u></strong>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" style="white-space: nowrap">
                    From Date
                </td>
                <td style="white-space: nowrap; text-align: right;">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td style="white-space: nowrap; text-align: left;">
                   <%-- <img alt="Click here to open the calendar and select the date corresponding to 'From Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />--%>
                </td>
                <td align="right" style="white-space: nowrap">
                    To Date
                </td>
                <td style="white-space: nowrap; text-align: right">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td style="white-space: nowrap; text-align: left;">
                    <%--<img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />--%>
                </td>
                <td align="right" style="white-space: nowrap">Location</td>
                <td><asp:DropDownList ID="ddlLocation" runat ="server" ></asp:DropDownList> </td>
                <td align="center" colspan="8" style="height: 22px">
                    <asp:Button ID="btnGet" runat="server" Text="Get" OnClick="btnGet_Click" />
                </td>
                <td>
                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" OnClick="btnExportToExcel_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:Label ID="txtMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" align="center">
        <tbody>
            <tr bgcolor="LightGoldenrodYellow"><td align="center"><b><font color="Black">Aging >=72 Hrs</font></b></td></tr>
            <tr>
                <td>
                    <asp:GridView ID="GrvCCSBatch" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
                        DataKeyNames="UnitName" OnRowCommand="GrvCCSBatch_RowCommand" OnRowDataBound="GrvCCSBatch_RowDataBound">
                        <Columns>
                            <%--<asp:TemplateField HeaderText="S.No." ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblSNo" runat="server" Text="<%#Container.DisplayIndex + 1%>">"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField DataField="UnitName" HeaderText="Location" ReadOnly="True" HeaderStyle-Wrap="false"
                                ItemStyle-Wrap="false" />
                            <asp:BoundField DataField="Created" HeaderText="SSC BATCH CREATED" ReadOnly="True"
                                HeaderStyle-Wrap="false" />
                            <asp:BoundField DataField="NotCreated" HeaderText="SSC BATCH NOT CREATED" ReadOnly="True"
                                HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                            <%--<asp:BoundField HeaderText="AWB Details Not Available" DataField="AWBDetails" ReadOnly="true"
                                HeaderStyle-Wrap="false" />--%>
                            <asp:BoundField DataField="GrandTotal" HeaderText="Grand Total" ReadOnly="True" HeaderStyle-Wrap="false" />
                            <asp:TemplateField HeaderText="AWB Details Not Available" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblAWBDetails" runat="server" Text='<%#Eval("AWBDetails") %>' Width="50px"></asp:Label>
                                    <asp:Button ID="bntExport" runat="server" Text="Export to Excel" CommandName="Export"
                                        CommandArgument="<%#((GridViewRow)Container).RowIndex %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
