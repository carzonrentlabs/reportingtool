﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for GetSessionFromToken
/// </summary>
public class GetSessionFromToken
{
     public static int? GetSession(string SessionToken)
    {
        if (string.IsNullOrEmpty(SessionToken))
        {
            //bpc
            throw new Exception("Cannot set Session with a null/empty token");

        }

      // Session.Timeout = 30;
        string DecodedToken = System.Text.Encoding.UTF8.GetString(
                Convert.FromBase64String(SessionToken));
        string[] TokenParts = DecodedToken.Split('|');
        if (TokenParts != null && TokenParts.Length >= 6)
        {
            if (TokenParts[0] == "Carz12345")
            {
                DateTime tokenCreationTime = Convert.ToDateTime(TokenParts[5]);
                if (DateTime.Now.Subtract(tokenCreationTime).TotalMinutes > 24 * 60)
                {
                    return null;
                }
                int CorporateModuleLoginId = 0;
                if (Int32.TryParse(TokenParts[4], out CorporateModuleLoginId))
                {

                }
                bool IsCorporateModule = TokenParts[2] == "1" ? true : false;
                return Convert.ToInt32(TokenParts[1]);
            }
        }
        return null;

    }
}