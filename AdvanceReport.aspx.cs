using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using ReportingTool;
using ExcelUtil;

public partial class MonthlyRevenueReport : System.Web.UI.Page
{
    #region Variable Declaration
    string dateFrom, dateTo;
    int cityId;
    Decimal totalCost = 0;
    clsAutomation objMonthlyRevenue = new clsAutomation();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["Userid"] = 4;
        if (!Page.IsPostBack)
        {
            bntNextImport.Attributes.Add("onclick", "return ValidationCheck();");
        }
    }

    protected void bntNextImport_Click(object sender, EventArgs e)
    {
        dateFrom = txtFrom.Text.ToString();
        dateTo = txtTo.Text.ToString();

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=AdvanceReport.xls"); //excel

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel"; //excel

        this.EnableViewState = false;
        Response.Write("\r\n");
        DataSet excel = new DataSet();
        excel = objMonthlyRevenue.GetAdvanceReport(dateFrom.ToString(), dateTo.ToString());

        dtEx = excel.Tables[0];

        DataSet ExtraAmt = new DataSet();
        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingID") { Response.Write("<td align='left'><b>Booking ID</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AdvBaseFare") { Response.Write("<td align='left'><b>AdvBaseFare</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalTax") { Response.Write("<td align='left'><b>TotalTax</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AdvTotalAmount") { Response.Write("<td align='left'><b>AdvTotalAmount</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AdvanceReceiptNumber") { Response.Write("<td align='left'><b>AdvanceReceiptNumber</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Receiptdate") { Response.Write("<td align='left'><b>Receiptdate</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "trackid") { Response.Write("<td align='left'><b>trackid</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "GuestName") { Response.Write("<td align='left'><b>GuestName</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "GuestMobile") { Response.Write("<td align='left'><b>GuestMobile</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "GuestEmailID") { Response.Write("<td align='left'><b>GuestEmailID</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ClientCoName") { Response.Write("<td align='left'><b>ClientCoName</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ServiceType") { Response.Write("<td align='left'><b>ServiceType</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "InvoiceNo") { Response.Write("<td align='left'><b>InvoiceNo</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "StateName") { Response.Write("<td align='left'><b>StateName</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingType") { Response.Write("<td align='left'><b>BookingType</b></td>"); }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalCost") { Response.Write("<td align='left'><b>TotalCost</b></td>"); }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");

            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Unblocking UB = new Unblocking();
        try
        {
            if (txtBookingID.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Please enter bookingid.');</script>");
                txtBookingID.Focus();
                return;
            }

            string BookingID;
            BookingID = txtBookingID.Text;
            BookingID = BookingID.Replace("\r\n", ",");
            BookingID = BookingID.Replace(",,", ",");
            if (BookingID.EndsWith(","))
            {
                int LengthRequired = BookingID.LastIndexOf(",");
                if (LengthRequired >= 0)
                    BookingID = BookingID.Substring(0, LengthRequired);
            }


            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=AdvanceReport.xls"); //excel

            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel"; //excel

            this.EnableViewState = false;
            Response.Write("\r\n");

            DataTable dtEx = new DataTable();
            DataSet excel = new DataSet();
            excel = objMonthlyRevenue.GetAdvanceReport(BookingID);

            dtEx = excel.Tables[0];

            DataSet ExtraAmt = new DataSet();
            if (dtEx.Rows.Count > 0)
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                int[] iColumns = { 0, 1, 2, 3 };
                for (int i = 0; i < dtEx.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "bookingid") { Response.Write("<td align='left'><b>Booking ID</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "invoiceno") { Response.Write("<td align='left'><b>invoiceno</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingStatus") { Response.Write("<td align='left'><b>BookingStatus</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "accountingdate") { Response.Write("<td align='left'><b>accountingdate</b></td>"); }
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                    }
                    Response.Write("</tr>");
                }
                Response.Write("</table>");

                Response.End();
            }
            else
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();
            }
        }
        catch (Exception ex)
        {
        }
    }
}