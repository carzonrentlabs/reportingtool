// JavaScript Document
function onlyNumbers(evt)
{    
	var e = event || evt; // for trans-browser compatibility    
	var charCode = e.which || e.keyCode;    
	if (charCode > 31 && (charCode < 48 || charCode > 57))       
		return false;    
	return true;
}

function chkExpiry(objDate, objMonths)
{
	var Expired=0;	//0 for Expired and 1 for NonExpired
	//Creating Date Six Months in Advance
	var curDate = new Date();
	var futureDate = new Date();
	futureDate.setMonth(curDate.getMonth() + objMonths); //Adding Months to Curdate
	var FDate = futureDate.getTime(); // convert date in milliseconds	
	
	//Converting objDate in Date format
	var objDate = new Date(objDate);
	var ODate = objDate.getTime(); // convert date in milliseconds	
	
	if (ODate < FDate)
	{
		Expired=0;
	}
	else
	{
		Expired=1;
	}
	return Expired;
}
