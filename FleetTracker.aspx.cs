using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;

public partial class FeetTracker : System.Web.UI.Page
{
    #region VariableDeclaration
    clsAutomation objFleetTracker = new clsAutomation();
    int monthName;
    private const int carIdIndex = 0;
    private const int RegnNo = 1;
    DataTable fleetTracker;
    bool showbutton;
    #endregion

    #region Event Function
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                BindMonth();
                BindUnit();
            }
        }
        catch (Exception EX)
        {

            lblMessage.Visible = true;
            lblMessage.Text = EX.Message;
        }


    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        if (ddMonth.SelectedIndex == 0 || ddlUnit.SelectedIndex == 0)
        {
            return;
        }
        try
        {
            DataTable dtTracker = new DataTable();
            DataTable numberOfDays = new DataTable();
            //DataTable fleetTracker = new DataTable();
            fleetTracker = new DataTable();
            monthName = Convert.ToInt32(ddMonth.SelectedValue);
            numberOfDays = objFleetTracker.GetTotalNumberofDays(monthName);
            ViewState["totalNumberofdays"] = Convert.ToInt32(numberOfDays.Rows[0][0]);
            ViewState["unitId"] = Convert.ToInt32(ddlUnit.SelectedValue);
            fleetTracker = objFleetTracker.GetPresentFleetTracker(Convert.ToInt32(Session["UserID"]), Convert.ToInt32(ViewState["unitId"]), monthName);
            if (fleetTracker.Rows.Count > 0)
            {
                grvFleetTracker.DataSource = fleetTracker;
                grvFleetTracker.DataBind();
                //For Column hide and show.
                if (Convert.ToInt32(ViewState["totalNumberofdays"]) < 31)
                {
                    try
                    {
                        for (int i = Convert.ToInt32(ViewState["totalNumberofdays"]); i < 31; i++)
                        {

                            if (i == 28 || i == 29 || i == 30 || i == 31)
                            {
                                grvFleetTracker.Columns[i + 4].Visible = false;
                            }
                            if (i == 31)
                            {
                                grvFleetTracker.Columns[i + 4].Visible = true;
                            }

                        }
                    }
                    catch (Exception Ex)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = Ex.Message;
                    }

                }
                else
                {
                    for (int i = 0; i < 31; i++)
                    {
                        grvFleetTracker.Columns[i + 4].Visible = true;
                    }

                }
                //end of Show hide column

            }
            else
            {
                dtTracker = objFleetTracker.GetFleetTracker(Convert.ToInt32(ViewState["unitId"]));
                if (dtTracker.Rows.Count > 0)
                {
                    if (Convert.ToInt32(ViewState["totalNumberofdays"]) < 31)
                    {
                        try
                        {
                            for (int i = Convert.ToInt32(ViewState["totalNumberofdays"]); i < 31; i++)
                            {

                                if (i == 28 || i == 29 || i == 30 || i == 31)
                                {
                                    grvFleetTracker.Columns[i + 4].Visible = false;
                                }
                                if (i == 31)
                                {
                                    grvFleetTracker.Columns[i + 4].Visible = true;
                                }

                            }
                        }
                        catch (Exception Ex)
                        {
                            lblMessage.Visible = true;
                            lblMessage.Text = Ex.Message;
                        }

                    }
                    else
                    {
                        for (int i = 0; i < 31; i++)
                        {
                            grvFleetTracker.Columns[i + 4].Visible = true;
                        }

                    }
                    grvFleetTracker.DataSource = dtTracker;
                    grvFleetTracker.DataBind();
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Text = "Record is not available.";
                    bntSaveAll.Visible = false;
                    showbutton = true;
                }
            }
            lblConfirmMessage.Visible = false;
            if (Convert.ToInt32(ddMonth.SelectedValue) != Convert.ToInt32(DateTime.Today.Month))
            {
                bntSaveAll.Visible = false;
            }
            else
            {
                if (showbutton == true)
                {
                    bntSaveAll.Visible = false;
                }
                else
                {
                    bntSaveAll.Visible = true;
                }
               
            }

        }
        catch (Exception EX)
        {

            lblMessage.Visible = true;
            lblMessage.Text = EX.Message;
        }

    }
    protected void dd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddCase = (DropDownList)sender;
        if (Convert.ToInt32(ddCase.SelectedValue) == 6)
        {
            //GridView gridTracker = (GridView)ddCase.Parent.Parent;
            GridViewRow grvRow = (GridViewRow)ddCase.Parent.Parent;
            TextBox txtRemark = (TextBox)grvFleetTracker.Rows[grvRow.RowIndex].FindControl("txtRemark");
            txtRemark.Visible = true;

        }

    }
    protected void bntSaveAll_Click(object sender, EventArgs e)
    {
        DateTime tDay = DateTime.Today;
        DateTime presentDate;
        int toDays = tDay.Day;
        int dtFleet = 0;
        try
        {
            if (toDays >= 3 && toDays <= Convert.ToInt32(ViewState["totalNumberofdays"]))
            {
                int j = 2;
                while (j >= 0)
                {
                    presentDate = DateTime.Today.AddDays(-j);
                    for (int i = 0; i < grvFleetTracker.Rows.Count; i++)
                    {
                        int carId = Convert.ToInt32(grvFleetTracker.DataKeys[i].Values[carIdIndex]);
                         int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                        int monthPresent = Convert.ToInt32(ddMonth.SelectedValue);
                        int userId = Convert.ToInt32(Session["UserID"]);
                        //presentDate = DateTime.Today;
                        TextBox txtReason = (TextBox)grvFleetTracker.Rows[i].FindControl("txtAttendance" + Convert.ToInt16(toDays - j));
                        string reason = txtReason.Text.ToString();
                        HtmlTextArea txtRemark = (HtmlTextArea)grvFleetTracker.Rows[i].FindControl("txtRemark" + Convert.ToInt16(toDays - j));
                        string remark = Convert.ToString(txtRemark.Value);
                        dtFleet = objFleetTracker.InsertFleetTracker(carId, reason, presentDate, remark, userId, monthPresent, unitId);

                    }
                    j--;
                }

            }
            else if (toDays == 2)
            {
                int j = 1;
                while (j >= 0)
                {
                    presentDate = DateTime.Today.AddDays(-j);
                    for (int i = 0; i < grvFleetTracker.Rows.Count; i++)
                    {
                        int carId = Convert.ToInt32(grvFleetTracker.DataKeys[i].Values[carIdIndex]);
                        int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                        int monthPresent = Convert.ToInt32(ddMonth.SelectedValue);
                        int userId = Convert.ToInt32(Session["UserID"]);
                        //presentDate = DateTime.Today;
                        TextBox txtReason = (TextBox)grvFleetTracker.Rows[i].FindControl("txtAttendance" + Convert.ToInt16(toDays - j));
                        string reason = txtReason.Text.ToString();
                        HtmlTextArea txtRemark = (HtmlTextArea)grvFleetTracker.Rows[i].FindControl("txtRemark" + Convert.ToInt16(toDays - j));
                        string remark = Convert.ToString(txtRemark.Value);
                        dtFleet = objFleetTracker.InsertFleetTracker(carId, reason, presentDate, remark, userId, monthPresent, unitId);

                    }
                    j--;
                }

            }
            else if (toDays == 1)
            {
                presentDate = DateTime.Today;
                for (int i = 0; i < grvFleetTracker.Rows.Count; i++)
                {
                    int carId = Convert.ToInt32(grvFleetTracker.DataKeys[i].Values[carIdIndex]);
                    int unitId = Convert.ToInt32(ddlUnit.SelectedValue);
                    int monthPresent = Convert.ToInt32(ddMonth.SelectedValue);
                    int userId = Convert.ToInt32(Session["UserID"]);
                    //presentDate = DateTime.Today;
                    TextBox txtReason = (TextBox)grvFleetTracker.Rows[i].FindControl("txtAttendance" + toDays);
                    string reason = txtReason.Text.ToString();
                    HtmlTextArea txtRemark = (HtmlTextArea)grvFleetTracker.Rows[i].FindControl("txtRemark" + toDays);
                    string remark = Convert.ToString(txtRemark.Value);
                    dtFleet = objFleetTracker.InsertFleetTracker(carId, reason, presentDate, remark, userId, monthPresent, unitId);

                }
            }
            if (dtFleet > 0)
            {
                lblConfirmMessage.Visible = true;
                lblConfirmMessage.Text = "Record Save Successfully.";
                lblConfirmMessage.ForeColor = Color.Red;
            }


        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }


    }
    protected void grvFleetTracker_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            try
            {
                DateTime tDay = DateTime.Today;
                int toDays = tDay.Day;
                DataTable dtReason = new DataTable();
                // dtReason = objFleetTracker.GetPresentFleetTracker(Convert.ToInt32(Session["UserID"]), Convert.ToInt32(ViewState["unitId"]), monthName);
                // DataTable dtCheckStatus = new DataTable();
                if (fleetTracker.Rows.Count > 0)  // Get Record when Entered Record in the table
                {
                    for (int i = 1; i <= Convert.ToInt32(ViewState["totalNumberofdays"]); i++)
                    {
                        TextBox txtAttandence = (TextBox)e.Row.FindControl("txtAttendance" + i);
                        if (!string.IsNullOrEmpty(fleetTracker.Rows[e.Row.RowIndex]["day" + i].ToString()))
                        {
                            txtAttandence.Text = fleetTracker.Rows[e.Row.RowIndex]["day" + i].ToString();
                        }

                    }
                    //TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance" + toDays);
                    if (Convert.ToInt32(ddMonth.SelectedValue) != Convert.ToInt32(DateTime.Today.Month))
                    {
                        //ddP.Enabled = false;
                    }

                    else if (toDays >= 3 && toDays <= Convert.ToInt32(ViewState["totalNumberofdays"]))
                    {
                        int j = 2;
                        while (j >= 0)
                        {

                            TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance" + Convert.ToInt16(toDays - j));
                            if (string.IsNullOrEmpty(fleetTracker.Rows[e.Row.RowIndex]["day" + Convert.ToInt16(toDays - j)].ToString()))
                            {
                                txtAttendane.Enabled = true;
                            }
                            else
                            {

                                txtAttendane.Enabled = false;
                            }
                            j--;
                        }
                    }
                    else if (toDays == 2)
                    {
                        int j = 1;
                        while (j >= 0)
                        {
                            TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance" + Convert.ToInt16(toDays - j));
                            if (string.IsNullOrEmpty(fleetTracker.Rows[e.Row.RowIndex]["day" + Convert.ToInt16(toDays - j)].ToString()))
                            {
                                txtAttendane.Enabled = true;
                            }
                            else
                            {
                                txtAttendane.Enabled = false;
                            }
                            j--;
                        }
                    }
                    else if (toDays == 1)
                    {
                        TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance1");
                        if (string.IsNullOrEmpty(fleetTracker.Rows[e.Row.RowIndex]["day" + Convert.ToInt16(toDays)].ToString()))
                        {
                            txtAttendane.Enabled = true;

                        }
                        else
                        {
                            txtAttendane.Enabled = false;
                        }

                    }


                }
                else  //// Starting of the month
                {
                    //TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance" + toDays);
                    if (Convert.ToInt32(ddMonth.SelectedValue) != Convert.ToInt32(DateTime.Today.Month))
                    {
                        //ddP.Enabled = false;
                    }
                    else if (toDays >= 3 && toDays <= Convert.ToInt32(ViewState["totalNumberofdays"]))
                    {
                        int j = 2;
                        while (j >= 0)
                        {
                            TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance" + Convert.ToInt16(toDays - j));
                            txtAttendane.Enabled = true;
                            j--;
                        }
                    }
                    else if (toDays == 2)
                    {
                        int j = 1;
                        while (j >= 0)
                        {
                            TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance" + Convert.ToInt16(toDays - j));
                            txtAttendane.Enabled = true;
                            j--;
                        }
                    }
                    else if (toDays == 1)
                    {

                        TextBox txtAttendane = (TextBox)e.Row.FindControl("txtAttendance1");
                        txtAttendane.Enabled = true;
                    }
                }
            }
            catch (Exception Ex)
            {

                lblMessage.Visible = true;
                lblMessage.Text = Ex.Message;
            }
        }

    }

    #endregion

    #region Helper Function
    private void BindMonth()
    {
        try
        {
            DataTable dtMonth = new DataTable();
            dtMonth = objFleetTracker.GetMonthName();
            ddMonth.DataTextField = "monthName";
            ddMonth.DataValueField = "number";
            ddMonth.DataSource = dtMonth;
            ddMonth.DataBind();
            ddMonth.Items.Insert(0, "--Select--");

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }


    }
    private void BindUnit()
    {
        try
        {
            DataTable dtUnit = new DataTable();
            dtUnit = objFleetTracker.GetUnitName(Convert.ToInt32(Session["UserID"]));
            ddlUnit.DataTextField = "UnitName";
            ddlUnit.DataValueField = "UnitId";
            ddlUnit.DataSource = dtUnit;
            ddlUnit.DataBind();
            ddlUnit.Items.Insert(0, "--Select--");

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }

    #endregion



}

