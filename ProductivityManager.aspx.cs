﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportingTool;
using System.Data;

public partial class ProductivityManager : System.Web.UI.Page
{

    private clsReport objReport = null;
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindClientName();
            BindUserName();
            BindCityName();
            BindYear(ddlYear);
            BindMonth(ddlMonth);
            BindClientDetails();

        }
        lblMessage.Visible = false;
        btnSumbit.Attributes.Add("onclick", "return validate();");
        btnMonthlyentry.Attributes.Add("onclick", "return validateMonthData();");
    }

    private void BindCityName()
    {

        try
        {
            DataSet ds = objReport.GetLocations_UserAccessWiseBK(Convert.ToInt32(Session["UserID"]));
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCityName.DataTextField = "CityName";
                ddlCityName.DataValueField = "CityID";
                ddlCityName.DataSource = ds.Tables[0];
                ddlCityName.DataBind();
                ddlCityName.Items.Insert(0, new ListItem("--Select--", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlCityName.Items.RemoveAt(1);
                }
                
                
            }
            else
            {
                ddlCityName.DataSource = null;
                ddlCityName.DataBind();
            }
        }
        catch (Exception)
        {

            throw new Exception();
        }
    }

    private void BindUserName()
    {
        objReport = new clsReport();
        try
        {
            DataSet ds = objReport.GetProductivityManagerList(Convert.ToInt32(Session["UserID"]));
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlUserName.DataTextField = "LoginID";
                ddlUserName.DataValueField = "sysuserid";
                ddlUserName.DataSource = ds.Tables[0];
                ddlUserName.DataBind();
                ddlUserName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlUserName.DataSource = null;
                ddlUserName.DataBind();
            }
        }
        catch (Exception)
        {

            throw new Exception();
        }
    }

    private void BindClientName()
    {
        objReport = new clsReport();
        try
        {
            DataSet ds = objReport.GetClientName();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlClientName.DataTextField = "clientConame";
                ddlClientName.DataValueField = "clientCoId";
                ddlClientName.DataSource = ds.Tables[0];
                ddlClientName.DataBind();
                ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));

            }
            else
            {

                ddlClientName.DataSource = null;
                ddlClientName.DataBind();
            }
        }
        catch (Exception)
        {

            throw new Exception();
        }
    }
    protected void btnSumbit_Click(object sender, EventArgs e)
    {
        objReport = new clsReport();
        int status;
        try
        {
            if (ddlCityName.SelectedValue == "0" || ddlClientName.SelectedValue == "0" || ddlUserName.SelectedValue == "0")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please select all fields')", true);
                return;
            }
            else
            {
                status = objReport.InsetProductivityManager(Convert.ToInt32(ddlUserName.SelectedValue), Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToInt32(Session["UserID"]));
                if (status > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Successfully inserted productivity manager";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                else if (status < 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Productivity manager entry already exists";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                else if (status == 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Getting some issues";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                BindClientDetails();
            }

        }
        catch (Exception)
        {
            throw new Exception();
        }



    }

    private void BindMonth(DropDownList ddlMonth)
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlMonth.SelectedValue = System.DateTime.Now.Month.ToString();
        }
        catch (Exception)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2015; i <= 2016; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlYear.SelectedValue = System.DateTime.Now.Year.ToString();
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }
    private void BindClientDetails()
    {
       objReport =new clsReport ();
        try
        {
            DataSet ds = objReport.GetClientDetails(Convert.ToInt32(Session["UserID"]));
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlPMClient.DataTextField = "PManager";
                ddlPMClient.DataValueField = "ProductivityID";
                ddlPMClient.DataSource = ds.Tables[0];
                ddlPMClient.DataBind();
                ddlPMClient.Items.Insert(0,new ListItem("--Select--","0"));
            }
            else
            {
                ddlPMClient.DataSource = null;
                ddlPMClient.DataBind();
            }
        }
        catch (Exception)
        {
            
            throw new Exception ();
        }
    }
    protected void btnMonthlyentry_Click(object sender, EventArgs e)
    {
        objReport = new clsReport();
        int status;
        try
        {
            if (ddlPMClient.SelectedIndex == 0 || ddlMonth.SelectedIndex == 0 || ddlYear.SelectedIndex == 0 || string.IsNullOrEmpty(txtBookingTarget.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please select all fields')", true);
                return;
            }
            else
            {
                status = objReport.InsetInsertProductivityMonth(Convert.ToInt32(ddlPMClient.SelectedValue), Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue),Convert.ToInt32(txtBookingTarget.Text), Convert.ToInt32(Session["UserID"]));
                if (status > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Successfully inserted productivity monthly.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                else if (status < 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Productivity monthly entry already exists";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                else if (status == 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Getting some issues";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            
            }
        }
        catch (Exception)
        {
            
            throw new Exception ();
        }
    }
    protected void btnGetIt_Click(object sender, EventArgs e)
    {
        objReport =new clsReport ();
        if (ddlPMClient.SelectedIndex == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please client details')", true);
            return;
        }
        else
        {
            DataSet ds = objReport.GetClientMonthDetails(Convert.ToInt32(ddlPMClient.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                grvMonthDetails.DataSource = ds.Tables[0];
                grvMonthDetails.DataBind();
            }
            else {
                grvMonthDetails.DataSource = null;
                grvMonthDetails.DataBind();
            }
        }

    }
}