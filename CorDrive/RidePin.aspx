﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RidePin.aspx.cs" Inherits="CorDrive_RidePin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtBookingId.ClientID%>").keyup(function () {
                var strPass = $("#<%=txtBookingId.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtBookingId.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            $("#<%=bntGet.ClientID%>").click(function () {
                if ($("#<%=txtBookingId.ClientID %>").val() == "") {
                    alert("Enter booking id");
                    return false;
                }
                else if (isNaN($("#<%=txtBookingId.ClientID %>").val())) {
                    alert("Booking id should only numeric value.")
                }
                else {
                    return true;
                }

            });
        });

        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
    </script>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 20%">
            <tr>
                <td colspan="1" align="center">
                    <b>Ride-Pin</b>
                </td>
            </tr>
            <tr>
                <td colspan="1">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="1" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="1" align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <b>Booking Id </b>&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtBookingId" runat="server"
                        Width="100" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div>
            <asp:GridView ID="grvRidePin" runat="server" AutoGenerateColumns="false" BackColor="#DEBA84"
                BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2"
                OnRowDataBound="grvRidePin_RowDataBound" 
                onrowcommand="grvRidePin_RowCommand">
                <Columns>
                    <asp:BoundField HeaderText="BookingId" DataField="BookingID" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Company Name" DataField="ClientCoName" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Pickup City" DataField="CityName" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Guest Name" DataField="GuestName" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Guest Mobile No" DataField="GuestMobileNo" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Pickup Address" DataField="PickUpAdd" HeaderStyle-Wrap="true">
                        <HeaderStyle Wrap="True"></HeaderStyle>
                    </asp:BoundField>
                     <asp:TemplateField HeaderText="Duty End Ride Pin" HeaderStyle-Wrap="true">
                        <ItemTemplate>
                            <asp:Label ID="lblRideNo" runat="server" Text='<%#Eval("RipNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Duty Start Ride Pin" HeaderStyle-Wrap="true">
                        <ItemTemplate>
                            <asp:Label ID="lblstartRideNo" runat="server" Text='<%#Eval("duty_start_otp") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                   <%-- <asp:TemplateField HeaderText="Send Ride Pin">
                        <ItemTemplate>
                            <asp:Button ID="bntSendRide" runat="server" Text="Send Ride Pin" CommandName="rideSend"
                                CommandArgument='<%#Eval("BookingId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            </asp:GridView>
        </div>
    </center>
</asp:Content>
