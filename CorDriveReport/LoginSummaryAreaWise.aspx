<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginSummaryAreaWise.aspx.cs" Inherits="CorDriveReport_LoginSummaryAreaWise" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 <title>Login Summary Area Wise</title>
 <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?sensor=true&client=gme-carzonrentindiapvt&v=3.20&libraries=places'></script>
<script src="//code.jquery.com/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="../JqueryNew/bootstrap.js" type="text/javascript"></script>
<script src="../JqueryNew/GoogleMapAPIWrapper.js" type="text/javascript"></script>
<%--<link href="../cssNew/bootstrap.min.css" rel="Stylesheet" type="text/css" />--%>
<script type='text/javascript'>
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                                            sURLVariables = sPageURL.split('&'),
                                            sParameterName,
                                            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
   

    var office_location_prediction_autocomplete = null;
    function officeLocationAutoCompleteHandler() {
        if (office_location_prediction_autocomplete != null) {
            var place = office_location_prediction_autocomplete.getPlace();
            // place.geometry.location.lat(), place.geometry.location.lng();
            // map.setCenter(place.geometry.location);
            // map.setZoom(12);
            $("#city_location_lat").val(place.geometry.location.lat());
            $("#city_location_long").val(place.geometry.location.lng());
//            $("#city_location_textdata").val(place.Name);
           // alert($("#city_location_textdata").val(place.Name);
           // alert($("#city_location_text").val())
            $('#<%=lblLocation.ClientID%>').text($("#city_location_text").val());
            $("#<%=txtloation.ClientID%>").val($("#city_location_text").val());          
           //  $("#city_location_text").val(place.geometry.location.text());
        }
    }
// $("#<%=btnSearch.ClientID%>").click(function () {
//                 $('#<%=lblLocation.ClientID%>').text($("#city_location_text").val());
//            });
    function initializePlaceAutoComplete(element_id, callback_function, element_obj) {
        var input = document.getElementById(element_id);
        if (element_obj != null) {
            input = element_obj;
        }
        if (input != null) {
            var options = {};
        }
        options['componentRestrictions'] = { country: 'in' };
        if (google == null || google.maps == null) {
            loading();
            alert("OOPS. There was a network error in loading this page. Please reload");
            return;
        }
        office_location_prediction_autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(office_location_prediction_autocomplete, "place_changed", callback_function, false);

    }

    $(function () {
        initializePlaceAutoComplete("city_location_text", officeLocationAutoCompleteHandler);
        // getCities();
    });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <%--<div>--%>
    <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
            <tr>
                <td colspan="5" align="center" style="background-color: #ffccff">
                    <b>Login Summary Area Wise</b>
                     <br />
                     
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    
                </td>
            </tr>
             <tr>
                <td colspan="5" align="center">
                                    
                    <asp:RadioButtonList ID="rdbcityArea" runat="server" OnSelectedIndexChanged="rdbcityArea_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true" >
                         <asp:ListItem Selected="True">Search By Area</asp:ListItem>
                        <asp:ListItem>Search By City</asp:ListItem>
                      
                    </asp:RadioButtonList>
                  <br />  
                 </td>
                   
            </tr>
            <tr id="trCity" runat="server" visible="false">
                
                <td colspan="5" align="center">
                 <b>Select City</b>&nbsp;&nbsp;
                     <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                    <asp:ListItem Value="0" Text="Select City"></asp:ListItem>
                    <asp:ListItem Value="1" Text="All"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Delhi"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Ahmedabad"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Bangalore"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Chennai"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Hyderabad"></asp:ListItem>
                    <asp:ListItem Value="7" Text="Mumbai"></asp:ListItem>
                    <asp:ListItem Value="8" Text="Chennai"></asp:ListItem>
                    <asp:ListItem Value="9" Text="Pune"></asp:ListItem>
                     <asp:ListItem Value="10" Text="Calcutta"></asp:ListItem>
                </asp:DropDownList>
                
                 <asp:Button ID="bntGet" runat="Server" Text="Export To Excel" OnClick="bntGet_Click" />
                </td>
               <%-- <td align="left">
                  
                </td>
                <td >
                </td>
                <td>
                   
                    
                </td>
                <td >
                   
                </td>--%>
            </tr>
            
             <tr id="trArea" runat="server" visible="false">
                <td style="height: 72px" align="right">
                <b>Enter Area</b> 
                </td>
                <%--<td style="height: 72px">
                  
                    
                </td>--%>
                <td style="height: 72px">
                  <div>
                        
                        <input class="input-large" type="text" id="city_location_text" placeholder = "Type Area...."/>
                         <asp:TextBox  ID="txtloation" runat="server"></asp:TextBox>
                        <br />
                        <asp:Label ID="lblLocation" runat="server" />
                       
                        <div style="display: none">
                            <input id="city_location_lat"  runat ="server"/>
                            <input id="city_location_long"  runat ="server" />
                             <input id="city_location_textdata"  runat ="server" />
                            </div>
                        
                    </div>
                </td>
                  <td style="height: 72px">
                 <b>Category</b>&nbsp;&nbsp;
                   <asp:DropDownList ID="ddlCategory" runat="server">
                   <asp:ListItem Value="0" Text="ALL"></asp:ListItem>
                   <asp:ListItem Value="1" Text="Economy"></asp:ListItem>
                   <asp:ListItem Value="2" Text="Compact"></asp:ListItem>
                   <asp:ListItem Value="3" Text="Intermediate"></asp:ListItem>
                   <asp:ListItem Value="4" Text="Standard"></asp:ListItem>
                   <asp:ListItem Value="5" Text="Superior"></asp:ListItem>
                   <asp:ListItem Value="6" Text="Premium"></asp:ListItem>
                    
                </asp:DropDownList>
                </td>
                
                <td style="height: 72px">
                 <b>Cab Location</b>&nbsp;&nbsp;
                   <asp:DropDownList ID="ddlCabLocation" runat="server">
                    <asp:ListItem Value="2" Text="Cab Current" Selected="True"></asp:ListItem>
                   <asp:ListItem Value="1" Text="Home Location"></asp:ListItem>
                  
                    
                </asp:DropDownList>
                </td>
                <td style="height: 72px">
                 <b>Radius</b>&nbsp;&nbsp;
                   <asp:DropDownList ID="ddlRadius" runat="server">
                    <asp:ListItem Value="1" Text="5"></asp:ListItem>
                    <asp:ListItem Value="2" Text="10"></asp:ListItem>
                    <asp:ListItem Value="3" Text="15"></asp:ListItem>
                    <asp:ListItem Value="4" Text="20"></asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnSearch" runat="Server" Text="Search" OnClick="btnSearch_Click"/>
                </td>
                
               <%-- <td align="left" style="height: 72px" >
                   
                </td>--%>
            </tr>
            
        </table>
   <%-- </div>--%>
     <br />
        <div>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdLoginSummaryCityWise" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor Drive" DataField="total_cor_drive" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 2 Hrs" DataField="active_2_hrs" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 12 Hrs" DataField="active_12_hrs" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 1 Day" DataField="active_1_day" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 2 Days" DataField="active_2_days" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 3 Days" DataField="active_3_days" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 7 Days" DataField="active_7_days" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Inactive Last 7 Days" DataField="inactive_last_7_days"
                                    HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                        <br />
                    </td>
                </tr>
                 <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdRptAreaVersionSummary" runat="server" AutoGenerateColumns="False">
                           																				
                                 <Columns>
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Ahmedabad" DataField="ahmedabad" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Bangalore" DataField="bangalore" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Chennai" DataField="chennai" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Delhi" DataField="delhi" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Hyderabad" DataField="hyderabad" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Kolkata" DataField="kolkata" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Mumbai" DataField="mumbai" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Pune" DataField="pune" >
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                  
                            </Columns>
                        </asp:GridView>
                         <br />
                    </td>
                </tr>
                
            </table>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdActivityCityWise" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="Chauffeur Name" DataField="chaufName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Chauffeur Mobile" DataField="chaufMobile" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Regn No" DataField="RegnNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Category" DataField="category" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Home Loc" DataField="home_loc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest Heartbeat" DataField="latest_heartbeat" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Login Status" DataField="LoginStatus" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Login Date" DataField="LoginDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Logout Date" DataField="LogoutDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Logout Date" DataField="LogoutDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Last Active" DataField="last_active" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest_Duty" DataField="latest_duty" HeaderStyle-Wrap="false" />
                               <%-- <asp:BoundField HeaderText="Chauffeur Name" DataField="chaufName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Chauffeur Mobile" DataField="chaufMobile" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Regn No" DataField="RegnNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Modify Date" DataField="ModifyDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Lat" DataField="Lat" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Lon" DataField="Lon" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Last Active" DataField="last_active" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest_Duty" DataField="latest_duty" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Allocation" DataField="total_alloc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Accepted" DataField="total_accepted" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor Drive Allocation" DataField="total_cor_drive_alloc"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total_Cor Drive Accepted" DataField="total_cor_drive_accepted"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor Drive In Progress" DataField="total_cor_drive_in_progress"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor_Drive Complete" DataField="total_cor_drive_complete"
                                    HeaderStyle-Wrap="false" />--%>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        
        
         <div>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdAreaWiseHome" runat="server" AutoGenerateColumns="false">
                           <Columns>
                            	
                                <asp:BoundField HeaderText="Chauffeur Name" DataField="chaufName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Chauffeur Mobile" DataField="chaufMobile" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Regn No" DataField="RegnNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Category" DataField="category" HeaderStyle-Wrap="false" />
                                 <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Home Loc" DataField="home_loc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest Heartbeat" DataField="latest_heartbeat" HeaderStyle-Wrap="false" />
                                 <asp:BoundField HeaderText="Login Status" DataField="LoginStatus" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Login Date" DataField="LoginDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="LogoutDate" DataField="LogoutDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest_Duty" DataField="latest_duty" HeaderStyle-Wrap="false" />
                                            
                           </Columns>
                        </asp:GridView>
                         <br />
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0">
                          
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdAreaWiseCurrent" runat="server" AutoGenerateColumns="false">
                            <Columns>
                            	
                                <asp:BoundField HeaderText="Chauffeur Name" DataField="chaufName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Chauffeur Mobile" DataField="chaufMobile" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Regn No" DataField="RegnNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Category" DataField="category" HeaderStyle-Wrap="false" />
                                 <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Home Loc" DataField="home_loc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest Heartbeat" DataField="latest_heartbeat" HeaderStyle-Wrap="false" />
                                 <asp:BoundField HeaderText="Login Status" DataField="LoginStatus" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Login Date" DataField="LoginDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="LogoutDate" DataField="LogoutDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest_Duty" DataField="latest_duty" HeaderStyle-Wrap="false" />
                                                      
                                   
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        
    </form>
</body>
</html>
