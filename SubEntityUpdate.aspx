<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubEntityUpdate.aspx.cs"
    Inherits="SubEntityUpdate" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script language="Javascript" src="App_Themes/CommonScript.js" type="text/jscript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/jscript"></script>
    <table width="80%" border="0" align="center">
        <tr>
            <td align="center">
                <strong><u>Enter Booking Id</u></strong>&nbsp;&nbsp;
                <asp:TextBox ID="txtBookingId" runat="server" MaxLength="10"></asp:TextBox>&nbsp;&nbsp;
                <asp:Button ID="bntGet" runat="server" Text="Get It" OnClick="bntGet_Click" />&nbsp;&nbsp;<b>OR</b>&nbsp;&nbsp;
                <asp:FileUpload ID="UpdateSubID" runat="server" />&nbsp;&nbsp;
                <asp:Button ID="btnUpload" runat="server" Text="Update Bulk Subsidiary" OnClick="btnUpload_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left">
                <b><u>Below is sample Excel File Format and file name should be (Subsidiary.xls or Subsidiary.xlsx) and sheet
                    name should be (SubsidiaryID)</u></b>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table border="1">
                    <tr style="background-color: Yellow;">
                        <td>
                            BookingID
                        </td>
                        <td>
                            SubID
                        </td>
                    </tr>
                    <tr>
                        <td>
                            1051508
                        </td>
                        <td>
                            1
                        </td>
                    </tr>
                    <tr>
                        <td>
                            1166112
                        </td>
                        <td>
                            2
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <center>
        <div>
            <asp:GridView ID="grvBookingDetails" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataKeyNames="BookingId"
                ForeColor="Black" GridLines="None" OnRowDataBound="grvBookingDetails_RowDataBound"
                OnRowCommand="grvBookingDetails_RowCommand" OnRowEditing="grvBookingDetails_RowEditing"
                OnRowUpdating="grvBookingDetails_RowUpdating">
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                <Columns>
                    <asp:TemplateField HeaderText="BookingID">
                        <ItemTemplate>
                            <asp:Label ID="lblBookingID" runat="server" Text='<%#Eval("BookingID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ClientId">
                        <ItemTemplate>
                            <asp:Label ID="lblClientID" runat="server" Text='<%#Eval("ClientCoId")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CompanyName">
                        <ItemTemplate>
                            <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("companyName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Guest Name">
                        <ItemTemplate>
                            <asp:Label ID="lblGuestName" runat="server" Text='<%#Eval("GuestName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sub Entity">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlEntity" runat="server">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnSubsidiaryID" Value='<%#Eval("SubsidiaryID")%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--
                    <asp:TemplateField HeaderText="Brand">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlBrand" runat="server">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlLocation" runat="server">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Update">
                        <ItemTemplate>
                            <asp:Button ID="btnApprove" runat="server" CommandArgument="<%#((GridViewRow)Container).RowIndex %>"
                                CommandName="Update" Text="Update" ValidationGroup="PopUp" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="Tan" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            </asp:GridView>
        </div>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    </center>
</asp:Content>
