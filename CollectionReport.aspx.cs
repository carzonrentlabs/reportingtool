using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CollectionReport : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SysUserId"] != null && Convert.ToInt32(Session["SysUserId"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
            }
        }
        else
        {
            Response.Redirect("../logout.aspx");
        }
    }
    protected void BindCity()
    {
        try
        {
            DataTable DT = new DataTable();
            ddlcity.Items.Clear();
            ddlcity.Items.Add(new ListItem(" -- ALL -- ", "0"));
            DT = ReportingTool.Reports.GetCity();
            if (DT.Rows.Count > 0)
            {
                ddlcity.AppendDataBoundItems = true;
                ddlcity.DataSource = DT;
                ddlcity.DataTextField = DT.Columns["CityName"].ToString();
                ddlcity.DataValueField = DT.Columns["CityID"].ToString();
                ddlcity.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect(ex.Message);
        }
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        string Fromdate, Todate;
        int cityid;
        if (txt_FromDate.Text == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "alert('Please Select From Date.')", true);
            txt_FromDate.Focus();
            return;
        }
        if (txt_ToDate.Text == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "alert('Please Select To Date.')", true);
            txt_ToDate.Focus();
            return;
        }
        Fromdate = txt_FromDate.Text;
        Todate = txt_ToDate.Text;
        cityid = Convert.ToInt32(ddlcity.SelectedValue);
        DataTable dtExcel = new DataTable();
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=CollectionReport.xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        this.EnableViewState = false;
        Response.Write("\r\n");
        dtExcel = ReportingTool.Reports.CollectionReport_Myles(Convert.ToDateTime(Fromdate), Convert.ToDateTime(Todate), cityid);
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16 };
        for (int i = 0; i < dtExcel.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    if (dtExcel.Columns[iColumns[j]].Caption.ToString() == "DateClose")
                    {
                        Response.Write("<td align='center'><b>Date of Duty close</b></td>");
                    }
                    else
                    {
                        Response.Write("<td align='center'><b>" + dtExcel.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                    }
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                Response.Write("<td align='center'>" + dtExcel.Rows[i][iColumns[j]].ToString() + "</td>");
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }
    protected void grv_CollectionReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grv_CollectionReport.PageIndex = e.NewPageIndex;
        FillGrid();
    }
    void FillGrid()
    {
        try
        {
            string Fromdate, Todate;
            int cityid;
            if (txt_FromDate.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "alert('Please Select From Date.')", true);
                txt_FromDate.Focus();
                return;

            }
            if (txt_ToDate.Text == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "alert('Please Select To Date.')", true);
                txt_ToDate.Focus();
                return;
            }
            Fromdate = txt_FromDate.Text;
            Todate = txt_ToDate.Text;
            cityid = Convert.ToInt32(ddlcity.SelectedValue);
            DataTable GetCollectionDetail = new DataTable();
            GetCollectionDetail = ReportingTool.Reports.CollectionReport_Myles(Convert.ToDateTime(Fromdate), Convert.ToDateTime(Todate), cityid);
            if (GetCollectionDetail.Rows.Count > 0)
            {
                grv_CollectionReport.DataSource = GetCollectionDetail;
                grv_CollectionReport.DataBind();
            }
            else
            {
                grv_CollectionReport.DataSource = GetCollectionDetail;
                grv_CollectionReport.DataBind();
            }
        }
        catch(Exception ex)
        {
            Response.Redirect(ex.Message);
        }
    }

}
