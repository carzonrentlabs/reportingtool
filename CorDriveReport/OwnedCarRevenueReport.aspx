﻿<%@ Page Title="Owned Car Revenue Report" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="OwnedCarRevenueReport.aspx.cs" Inherits="CorDriveReport_OwnedCarRevenueReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <link href="../greybox/gb_styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <center>
        <div align="center">
            <b>Owned Car Revenue Report</b>
        </div>
        <br />
        <div>
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblBranch" runat="server">Branch</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlBranch" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Label ID="lblToDate" runat="server">To Date</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Label ID="lblProduct" runat="server">Product</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:DropDownList ID="ddlProduct" runat="server">
                            <asp:ListItem Value="0">All</asp:ListItem>
                            <asp:ListItem Value="1">CRD</asp:ListItem>
                            <asp:ListItem Value="2">Limo</asp:ListItem>
                            <asp:ListItem Value="3">Hotel</asp:ListItem>
                            <asp:ListItem Value="4">LTR</asp:ListItem>
                            <asp:ListItem Value="5">Myles</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="Repeater">
            <asp:Repeater ID="rptOwnedCarDetailedReport" runat="server">
                <HeaderTemplate>
                    <table id="sort_table" width="100%">
                        <thead>
                            <tr>
                                <th align="center">
                                    S.No
                                </th>
                                <th align="center">
                                    Car Number
                                </th>
                                <th align="center">
                                    Chauffeur Name
                                </th>
                                <th align="center">
                                    Chauffeur Contact
                                </th>
                                <th align="center">
                                    Category
                                </th>
                                <th align="center">
                                    Model
                                </th>
                                <th align="center">
                                    Date of Activation
                                </th>
                                <th align="center">
                                    Branch
                                </th>
                                <th align="center">
                                    Prodcut
                                </th>
                                <th align="center">
                                    KM Run MTD
                                </th>
                                <th align="center">
                                    Billed KM MTD
                                </th>
                                <th align="center">
                                    Billed Hours MTD
                                </th>
                                <th align="center">
                                    Duties Done MTD
                                </th>
                                <th align="center">
                                    Revenue Done MTD
                                </th>
                                <th align="center">
                                    Open DS MTD
                                </th>
                                <th align="center">
                                    No of Days with Zero Dispatch
                                </th>
                                <th align="center">
                                    Compact Duties done
                                </th>
                                <th align="center">
                                    Economy Duties done
                                </th>
                                <th align="center">
                                    Intermediate Duties done
                                </th>
                                <th align="center">
                                    Standard Duties done
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="row_<%#Container.ItemIndex+1  %>" style="font-family: Arial, Verdana, Sans-Serif;">
                        <td style="text-align: center;">
                            <%# Container.ItemIndex + 1 %>
                        </td>
                        <td style="text-align: left; white-space :nowrap ;">
                            <%#Eval("CarNumber")%>
                        </td>
                        <td style="text-align: left;  white-space :nowrap ">
                            <%#Eval("ChauffeurName")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("ChauffeurContractNo")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Category")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Model")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DateofActivation","{0:dd/MM/yyyy}")%>
                        </td>
                        <td>
                            <%#Eval("Branch")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Product")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("KMRunMTD")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("BilledKMMTD")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("BilledHoursMTD")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DutiesDoneMTD")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("RevenueDoneMTD")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("OPenDSMTD")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("NoofDayswithZeroDispatch")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("CompactDutiesdone")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("EconomyDutiesdone")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("IntermediateDutiesdone")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("StandardDutiesdone")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
            <div align="center" id="dvPaging" runat="server" visible="false">
                <asp:LinkButton ID="lnkPrev" runat="server" Text="Prev" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkPrev_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkNext" runat="server" Text="Next" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkNext_Click"></asp:LinkButton>
            </div>
        </div>
    </center>
</asp:Content>
