<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LoginSummaryCityWise.aspx.cs" Inherits="CorDriveReport_LoginSummaryCityWise" Title="Login Summary City Wise" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

         $(document).ready(function () {
           
            $("#<%=ddlCity.ClientID%>").change(function () {
                if (document.getElementById('<%=ddlCity.ClientID%>').value == "") {
                    alert("Please Select the City");

                    return false;
                }
               
                else {
                    ShowProgress();
                }
            });
       });
       
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() 
        {  
            setTimeout(function () 
            {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            },200);
        }

    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Login Summary City Wise</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                </td>
                <td>
                   
                    
                </td>
                <td >
                  
                </td>
                <td>
                 <b>Select City</b>&nbsp;&nbsp;
                     <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                    <asp:ListItem Value="0" Text="Select City"></asp:ListItem>
                     <asp:ListItem Value="1" Text="All"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Delhi"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Ahmedabad"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Bangalore"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Chennai"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Hyderabad"></asp:ListItem>
                    <asp:ListItem Value="7" Text="Mumbai"></asp:ListItem>
                    <asp:ListItem Value="8" Text="Chennai"></asp:ListItem>
                    <asp:ListItem Value="9" Text="Pune"></asp:ListItem>
                     <asp:ListItem Value="10" Text="Calcutta"></asp:ListItem>
                </asp:DropDownList>
                </td>
                <td>
                   <asp:Button ID="bntGet" runat="Server" Text="Export To Excel" OnClick="bntGet_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div class="Repeater" style="width: 1350px; height: 400px; overflow: auto">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdLoginSummaryCityWise" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor Drive" DataField="total_cor_drive" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 2 Hrs" DataField="active_2_hrs" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 12 Hrs" DataField="active_12_hrs" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 1 Day" DataField="active_1_day" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 2 Days" DataField="active_2_days" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 3 Days" DataField="active_3_days" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Active 7 Days" DataField="active_7_days" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Inactive Last 7 Days" DataField="inactive_last_7_days"
                                    HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdActivityCityWise" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="Chauffeur Name" DataField="chaufName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Chauffeur Mobile" DataField="chaufMobile" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Regn No" DataField="RegnNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Modify Date" DataField="ModifyDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Lat" DataField="Lat" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Lon" DataField="Lon" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Last Active" DataField="last_active" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Latest_Duty" DataField="latest_duty" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Allocation" DataField="total_alloc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Accepted" DataField="total_accepted" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor Drive Allocation" DataField="total_cor_drive_alloc"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total_Cor Drive Accepted" DataField="total_cor_drive_accepted"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor Drive In Progress" DataField="total_cor_drive_in_progress"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Cor_Drive Complete" DataField="total_cor_drive_complete"
                                    HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>

