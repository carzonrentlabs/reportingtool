﻿<%@ Page Title="FuelManagement" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="FuelManagement.aspx.cs" Inherits="FuelManagement_FuelManagement" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script src="../JQuery/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("#<%=bntSubmit.ClientID%>").click(function () {
                if (document.getElementById("<%=ddlCityName.ClientID%>").selectedIndex == 0) {
                    alert("Select city name");
                    document.getElementById("<%=ddlCityName.ClientID%>").focus();
                    return true;
                }
                else if (document.getElementById("<%=ddlRegNo.ClientID%>").selectedIndex == 0) {
                    alert("Select registration")
                    document.getElementById("<%=ddlRegNo.ClientID%>").focus();
                    return true;
                }
                else if (document.getElementById("<%=txtFuelType.ClientID%>").value == "") {
                    alert("Enter fuel type")
                    document.getElementById("<%=txtFuelType.ClientID%>").focus();
                    return false;
                }
                else if (document.getElementById("<%=txtRate.ClientID%>").value == "") {
                    alert("Enter rate of fuel type")
                    document.getElementById("<%=txtRate.ClientID%>").focus();
                    return false;
                }
                else if (document.getElementById("<%=txtQutliters.ClientID%>").value == "") {
                    alert("Enter quantity of fuel")
                    document.getElementById("<%=txtQutliters.ClientID%>").focus();
                    return true;
                }
                else if (document.getElementById("<%=txtTotalAmount.ClientID%>").value == "") {
                    alert("Enter total amount of fuel")
                    document.getElementById("<%=txtTotalAmount.ClientID%>").focus();
                    return false;
                }
                else if (document.getElementById("<%=txtOdometerreading.ClientID%>").value == "") {
                    alert("Enter odometer reading");
                    document.getElementById("<%=txtOdometerreading.ClientID%>").focus();
                    return false;
                }
            });

            $("#<%=txtTotalAmount.ClientID%>,#<%=txtOdometerreading.ClientID%>,#<%=txtQutliters.ClientID%>").keyup(function () {
                var temp = $("#" + this.id).val();
                var strPass = temp;
                var strLength = strPass.length;
                var lchar = temp.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = temp.substring(0, (strLength) - 1);
                    $("#" + this.id).val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

        }
        function PrintVoucher() {
            var voucher = document.getElementById("<%=hdVoucherId.ClientID%>").value;
            //alert(voucher)
            url = encodeURI("FuelManagementPrint.aspx?voucherid=" + voucher)
            window.open(url, "VoucherPrint", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=50, width=500, height=500");
        }
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
           
    </script>
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <table border="0" align="center" cellpadding="2" cellspacing="2">
                <tr>
                    <td align="center">
                        <strong><font size="2">Fuel Management</font> </strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Select City</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCityName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCityName_SelectedIndexChanged"
                            TabIndex="30">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Reg.No</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlRegNo" runat="server" OnSelectedIndexChanged="ddlRegNo_SelectedIndexChanged"
                            AutoPostBack="true" TabIndex="31">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvddlRegNo" ControlToValidate="ddlRegNo"
                            ErrorMessage="*" ForeColor="Red" Display="Static"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Fuel Type</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFuelType" runat="server" Width="100px" ReadOnly="true"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvtxtFuleType" ControlToValidate="txtFuelType"
                            ErrorMessage="*" ForeColor="Red" Display="Static"></asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hdFuelType" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Rate/Liter</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRate" runat="server" Width="100px" ReadOnly="true"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvtxtRate" ControlToValidate="txtRate"
                            ErrorMessage="*" ForeColor="Red" Display="Static"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Quantity (In liters)</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtQutliters" runat="server" Width="100px" OnTextChanged="txtQutliters_TextChanged"
                            TabIndex="32" AutoPostBack="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtQutliters" runat="server" ErrorMessage="*"
                            ForeColor="Red" Display="Static" ControlToValidate="txtQutliters"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Amount (In Rs.)</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTotalAmount" runat="server" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvtxtTotalAmount" ControlToValidate="txtTotalAmount"
                            ErrorMessage="*" ForeColor="Red" Display="Static"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Odometer reading</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtOdometerreading" runat="server" Width="100px" TabIndex="33"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvtxtOdometerreading" ControlToValidate="txtOdometerreading"
                            ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Remarks</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" Columns="20"
                            TabIndex="34"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="bntSubmit" runat="server" Text="Submit" OnClick="bntSubmit_Click"
                            TabIndex="35" />
                        <asp:HiddenField ID="hdVoucherId" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
