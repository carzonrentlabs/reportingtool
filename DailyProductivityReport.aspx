﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DailyProductivityReport.aspx.cs" Inherits="DailyProductivityReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <div>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Daily Prodcutivity Report</b>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;<asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblCityName" runat="server">City Name</asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlCity" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Label ID="lblToDate" runat="server">To date</asp:Label>&nbsp;&nbsp;
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                    <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <center>
        <div>
            <asp:Literal ID="ltlSummary" runat ="server" ></asp:Literal>
        </div>
    </center>
</asp:Content>
