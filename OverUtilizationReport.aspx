<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OverUtilizationReport.aspx.cs" Inherits="OverUtilizationReport" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    
<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}
</script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="7">
                    <strong><u>Over Utilization Report</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 590px">Date</td>
                <td align="left" style="width: 11px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 105px"> City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="7">
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            
        <tr>
        <td align="center" colspan="7">
        <asp:GridView runat="server" ID="GridView1" PageSize="20" EmptyDataText="No Record Found" AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="GridView1_PageIndexChanging">
        <Columns>
        
        <asp:TemplateField HeaderText="Location">
        <ItemTemplate>
        <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Car #">
        <ItemTemplate>
        <asp:Label ID="lblVehicleNo" Text='<% #Bind("VehicleNo") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Car Type">
        <ItemTemplate>
        <asp:Label ID="lblOwnership" Text='<% #Bind("Ownership") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Category">
        <ItemTemplate>
        <asp:Label ID="lblcategory" Text='<% #Bind("category") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="No of DS">
        <ItemTemplate>
        <asp:Label ID="lblDuties" Text='<% #Bind("Duties") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        <asp:TemplateField HeaderText="No. of Auto allocation DS">
        <ItemTemplate>
        <asp:Label ID="lblAutoAllocated" Text='<% #Bind("AutoAllocated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="No. of DS Manually allotted">
        <ItemTemplate>
        <asp:Label ID="lblManuallyAllocated" Text='<% #Bind("ManuallyAllocated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="No. of Outstation Duties done">
        <ItemTemplate>
        <asp:Label ID="lblOutstation" Text='<% #Bind("Outstation") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Std Amount">
        <ItemTemplate>
        <asp:Label ID="lblStandardAmt" Text='<% #Bind("StandardAmt") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Actual Amount">
        <ItemTemplate>
        <asp:Label ID="lblActualAmt" Text='<% #Bind("ActualAmt") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
      
        </Columns>
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        </td>
        </tr>
        
        </tbody>
    </table>
    
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
        top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
        scrolling="no" height="182"></iframe>
    </div>

</asp:Content>