<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VDPReportDayWise.aspx.cs" Inherits="VDPReportDayWise" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

   

    <script language="javascript" type="text/javascript">
window.history.forward(1);
    </script>

    <table width="802" border="0" align="center" id="TABLE1">
        <tbody>
            <tr>
                <td align="center" colspan="8" style="height: 18px">
                    <strong><u>VDP Wise Day Wise Summary</u></strong>&nbsp;</td>
            </tr>
            <tr>
                <td align="right" style="width: 544px">
                    Select Days</td>
                <td width="300px" align="left" colspan="3">
                    <asp:DropDownList ID="ddldays" runat="server">
                    <asp:ListItem Text="15 Days" Value="15" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="30 Days" Value="30"></asp:ListItem>
                    </asp:DropDownList></td>
                <td align="right" style="width: 200px">
                    City Name:</td>
                <td style="width: 210px">
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="center" colspan="8" style="height: 22px">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Get >>" OnClick="btnSubmit_Click" />
                    &nbsp;
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
