using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;

public partial class PenaltyDetails : System.Web.UI.Page
{
    Vendor objVendor = new Vendor();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCityDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");

    }
    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objVendor.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation.Tables[0];
                    ddlCity.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();

    }
    private void BindGrid()
    {
        DataSet ds = new DataSet();
        lblMessage.Visible = false;
        ds = objVendor.GetPenaltyDetails(Convert.ToInt32(ddlCity.SelectedValue.ToString()), Convert.ToDateTime(txtFromDate.Text.ToString()), Convert.ToDateTime(txtToDate.Text.ToString()));
        if (ds.Tables[0].Rows.Count > 0)
        {
            grvValidation.DataSource = ds.Tables[0];
            grvValidation.DataBind();
        }
        else
        {
            grvValidation.DataSource = null;
            grvValidation.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = "Record not available";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        DataSet dsInvoice = new DataSet();
        DataTable dt = new DataTable();
        int counter = 0;

        dsInvoice = objVendor.GetPenaltyDetails(Convert.ToInt32(ddlCity.SelectedValue.ToString()), Convert.ToDateTime(txtFromDate.Text.ToString()), Convert.ToDateTime(txtToDate.Text.ToString()));

        if (dsInvoice != null)
        {
            if (dsInvoice.Tables.Count > 0)
            {
                dt = dsInvoice.Tables[0];
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    //Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=PenaltyReport.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "BookingID")
                                {
                                    Response.Write("<td align='left'><b>BookingID</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                {
                                    Response.Write("<td align='left'><b>City Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "ClientCoName")
                                {
                                    Response.Write("<td align='left'><b>Company Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "GuestName")
                                {
                                    Response.Write("<td align='left'><b>Guest Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "Phone1")
                                {
                                    Response.Write("<td align='left'><b>Guest Mobile</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarModelName")
                                {
                                    Response.Write("<td align='left'><b>Model Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "flightNumber")
                                {
                                    Response.Write("<td align='left'><b>Flight No</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "PickUpDate")
                                {
                                    Response.Write("<td align='left'><b>PickUp Date</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "PickUpTime")
                                {
                                    Response.Write("<td align='left'><b>PickUp Time</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "PickUpAdd")
                                {
                                    Response.Write("<td align='left'><b>PickUp Address</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "OutstationYN")
                                {
                                    Response.Write("<td align='left'><b>OutstationYN</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "Service")
                                {
                                    Response.Write("<td align='left'><b>Service</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "PenalityAmount")
                                {
                                    Response.Write("<td align='left'><b>PenalityAmount</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "VehicleNo")
                                {
                                    Response.Write("<td align='left'><b>Vehicle No</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "ChauffeurName")
                                {
                                    Response.Write("<td align='left'><b>Chauffeur Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                                {
                                    Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DeallocationRemarks")
                                {
                                    Response.Write("<td align='left'><b>Deallocation Remarks</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "deallocationCategory")
                                {
                                    Response.Write("<td align='left'><b>Deallocation Category</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "PenaltyType")
                                {
                                    Response.Write("<td align='left'><b>Penalty Type</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "PenaltyRemarks")
                                {
                                    Response.Write("<td align='left'><b>Penalty Remarks</b></td>");
                                }
                            }
                            Response.Write("</tr>");
                        }
                        Response.Write("<tr>");
                        //Response.Write("<td align='left'>" + counter + "</td>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                        }

                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");

                    Response.End();
                }
                else
                {

                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();

                }
            }
        }
    }

    protected void grvValidation_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvValidation.PageIndex = e.NewPageIndex;
        //this.BindGrid();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}