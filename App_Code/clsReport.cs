using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;

/// <summary>
/// Summary description for clsReport
/// </summary>
namespace ReportingTool
{
    public class clsReport
    {
        public clsReport()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    public DataSet GetCollection1(string strFrom, string strTo, string strCompany, string strCity, string strMode, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strFrom;

            ObjparamUser[1] = new SqlParameter("@to", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strTo;

            ObjparamUser[2] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strCompany;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[4] = new SqlParameter("@mode", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = strMode;

            ObjparamUser[5] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = currpage;

            ObjparamUser[6] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Pagesize;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_RPTCollection_Test", ObjparamUser);


        }
        public DataSet GetCollection(string strFrom, string strTo, string strCompany, string strCity, string strMode, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strFrom;

            ObjparamUser[1] = new SqlParameter("@to", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strTo;

            ObjparamUser[2] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strCompany;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[4] = new SqlParameter("@mode", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = strMode;

            ObjparamUser[5] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = currpage;

            ObjparamUser[6] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Pagesize;


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptcollection", ObjparamUser);


        }
        public DataSet GetCustomerDSO(string strFrom, string strCompany, string strCity, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strFrom;

            ObjparamUser[2] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strCompany;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[4] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = currpage;

            ObjparamUser[5] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptCustomerDSO", ObjparamUser);


        }
        public DataSet GetAgeingofCollection(string strFrom, string strTo, string strCompany, string strCity, string strDate, string strOne, string strTwo, string strThree, string strFour, string strFive, string strSix, string strSeven, string strEight, string strNine, string strTen, string strEleven, string strTwelve, string strThirteen, string strFourteen, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[22];

            ObjparamUser[0] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompany;

            ObjparamUser[1] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strFrom;

            ObjparamUser[2] = new SqlParameter("@to", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strTo;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[5] = new SqlParameter("@date", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = strDate;

            ObjparamUser[6] = new SqlParameter("@flexiBlock_1", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = strOne;

            ObjparamUser[7] = new SqlParameter("@flexiBlock_2", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = strTwo;

            ObjparamUser[8] = new SqlParameter("@flexiBlock_3", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = strThree;

            ObjparamUser[9] = new SqlParameter("@flexiBlock_4", SqlDbType.NVarChar);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            ObjparamUser[9].Value = strFour;

            ObjparamUser[10] = new SqlParameter("@flexiBlock_5", SqlDbType.NVarChar);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = strFive;

            ObjparamUser[11] = new SqlParameter("@flexiBlock_6", SqlDbType.NVarChar);
            ObjparamUser[11].Direction = ParameterDirection.Input;
            ObjparamUser[11].Value = strSix;

            ObjparamUser[12] = new SqlParameter("@flexiBlock_7", SqlDbType.NVarChar);
            ObjparamUser[12].Direction = ParameterDirection.Input;
            ObjparamUser[12].Value = strSeven;

            ObjparamUser[13] = new SqlParameter("@flexiBlock_8", SqlDbType.NVarChar);
            ObjparamUser[13].Direction = ParameterDirection.Input;
            ObjparamUser[13].Value = strEight;

            ObjparamUser[14] = new SqlParameter("@flexiBlock_9", SqlDbType.NVarChar);
            ObjparamUser[14].Direction = ParameterDirection.Input;
            ObjparamUser[14].Value = strNine;

            ObjparamUser[15] = new SqlParameter("@flexiBlock_10", SqlDbType.NVarChar);
            ObjparamUser[15].Direction = ParameterDirection.Input;
            ObjparamUser[15].Value = strTen;

            ObjparamUser[16] = new SqlParameter("@flexiBlock_11", SqlDbType.NVarChar);
            ObjparamUser[16].Direction = ParameterDirection.Input;
            ObjparamUser[16].Value = strEleven;

            ObjparamUser[17] = new SqlParameter("@flexiBlock_12", SqlDbType.NVarChar);
            ObjparamUser[17].Direction = ParameterDirection.Input;
            ObjparamUser[17].Value = strTwelve;


            ObjparamUser[18] = new SqlParameter("@flexiBlock_13", SqlDbType.NVarChar);
            ObjparamUser[18].Direction = ParameterDirection.Input;
            ObjparamUser[18].Value = strThirteen;

            ObjparamUser[19] = new SqlParameter("@flexiBlock_14", SqlDbType.NVarChar);
            ObjparamUser[19].Direction = ParameterDirection.Input;
            ObjparamUser[19].Value = strFourteen;

            ObjparamUser[20] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[20].Direction = ParameterDirection.Input;
            ObjparamUser[20].Value = currpage;

            ObjparamUser[21] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[21].Direction = ParameterDirection.Input;
            ObjparamUser[21].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptAgeingofCollection", ObjparamUser);


        }
        public DataSet GetAgeingofOutstanding(string strCompany, string strCity, string strDate, string strOne, string strTwo, string strThree, string strFour, string strFive, string strSix, string strSeven, string strEight, string strNine, string strTen, string strEleven, string strTwelve, string strThirteen, string strFourteen, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[20];

            ObjparamUser[0] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompany;

            ObjparamUser[1] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strCity;

            ObjparamUser[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strDate;

            ObjparamUser[3] = new SqlParameter("@flexiBlock_1", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strOne;

            ObjparamUser[4] = new SqlParameter("@flexiBlock_2", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = strTwo;

            ObjparamUser[5] = new SqlParameter("@flexiBlock_3", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = strThree;

            ObjparamUser[6] = new SqlParameter("@flexiBlock_4", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = strFour;

            ObjparamUser[7] = new SqlParameter("@flexiBlock_5", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = strFive;

            ObjparamUser[8] = new SqlParameter("@flexiBlock_6", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = strSix;

            ObjparamUser[9] = new SqlParameter("@flexiBlock_7", SqlDbType.NVarChar);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            ObjparamUser[9].Value = strSeven;

            ObjparamUser[10] = new SqlParameter("@flexiBlock_8", SqlDbType.NVarChar);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = strEight;

            ObjparamUser[11] = new SqlParameter("@flexiBlock_9", SqlDbType.NVarChar);
            ObjparamUser[11].Direction = ParameterDirection.Input;
            ObjparamUser[11].Value = strNine;

            ObjparamUser[12] = new SqlParameter("@flexiBlock_10", SqlDbType.NVarChar);
            ObjparamUser[12].Direction = ParameterDirection.Input;
            ObjparamUser[12].Value = strTen;

            ObjparamUser[13] = new SqlParameter("@flexiBlock_11", SqlDbType.NVarChar);
            ObjparamUser[13].Direction = ParameterDirection.Input;
            ObjparamUser[13].Value = strEleven;

            ObjparamUser[14] = new SqlParameter("@flexiBlock_12", SqlDbType.NVarChar);
            ObjparamUser[14].Direction = ParameterDirection.Input;
            ObjparamUser[14].Value = strTwelve;


            ObjparamUser[15] = new SqlParameter("@flexiBlock_13", SqlDbType.NVarChar);
            ObjparamUser[15].Direction = ParameterDirection.Input;
            ObjparamUser[15].Value = strThirteen;

            ObjparamUser[16] = new SqlParameter("@flexiBlock_14", SqlDbType.NVarChar);
            ObjparamUser[16].Direction = ParameterDirection.Input;
            ObjparamUser[16].Value = strFourteen;

            ObjparamUser[17] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[17].Direction = ParameterDirection.Input;
            ObjparamUser[17].Value = currpage;

            ObjparamUser[18] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[18].Direction = ParameterDirection.Input;
            ObjparamUser[18].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptAgeingofOutstanding", ObjparamUser);


        }
        public DataSet GetAgeingofUnappliedCredit(string strCompany, string strCity, string strDate, string strOne, string strTwo, string strThree, string strFour, string strFive, string strSix, string strSeven, string strEight, string strNine, string strTen, string strEleven, string strTwelve, string strThirteen, string strFourteen, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[20];

            ObjparamUser[0] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompany;

            ObjparamUser[1] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strCity;

            ObjparamUser[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strDate;

            ObjparamUser[3] = new SqlParameter("@flexiBlock_1", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strOne;

            ObjparamUser[4] = new SqlParameter("@flexiBlock_2", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = strTwo;

            ObjparamUser[5] = new SqlParameter("@flexiBlock_3", SqlDbType.NVarChar);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = strThree;

            ObjparamUser[6] = new SqlParameter("@flexiBlock_4", SqlDbType.NVarChar);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = strFour;

            ObjparamUser[7] = new SqlParameter("@flexiBlock_5", SqlDbType.NVarChar);
            ObjparamUser[7].Direction = ParameterDirection.Input;
            ObjparamUser[7].Value = strFive;

            ObjparamUser[8] = new SqlParameter("@flexiBlock_6", SqlDbType.NVarChar);
            ObjparamUser[8].Direction = ParameterDirection.Input;
            ObjparamUser[8].Value = strSix;

            ObjparamUser[9] = new SqlParameter("@flexiBlock_7", SqlDbType.NVarChar);
            ObjparamUser[9].Direction = ParameterDirection.Input;
            ObjparamUser[9].Value = strSeven;

            ObjparamUser[10] = new SqlParameter("@flexiBlock_8", SqlDbType.NVarChar);
            ObjparamUser[10].Direction = ParameterDirection.Input;
            ObjparamUser[10].Value = strEight;

            ObjparamUser[11] = new SqlParameter("@flexiBlock_9", SqlDbType.NVarChar);
            ObjparamUser[11].Direction = ParameterDirection.Input;
            ObjparamUser[11].Value = strNine;

            ObjparamUser[12] = new SqlParameter("@flexiBlock_10", SqlDbType.NVarChar);
            ObjparamUser[12].Direction = ParameterDirection.Input;
            ObjparamUser[12].Value = strTen;

            ObjparamUser[13] = new SqlParameter("@flexiBlock_11", SqlDbType.NVarChar);
            ObjparamUser[13].Direction = ParameterDirection.Input;
            ObjparamUser[13].Value = strEleven;

            ObjparamUser[14] = new SqlParameter("@flexiBlock_12", SqlDbType.NVarChar);
            ObjparamUser[14].Direction = ParameterDirection.Input;
            ObjparamUser[14].Value = strTwelve;


            ObjparamUser[15] = new SqlParameter("@flexiBlock_13", SqlDbType.NVarChar);
            ObjparamUser[15].Direction = ParameterDirection.Input;
            ObjparamUser[15].Value = strThirteen;

            ObjparamUser[16] = new SqlParameter("@flexiBlock_14", SqlDbType.NVarChar);
            ObjparamUser[16].Direction = ParameterDirection.Input;
            ObjparamUser[16].Value = strFourteen;

            ObjparamUser[17] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[17].Direction = ParameterDirection.Input;
            ObjparamUser[17].Value = currpage;

            ObjparamUser[18] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[18].Direction = ParameterDirection.Input;
            ObjparamUser[18].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptAgeingofUnappliedCredit", ObjparamUser);


        }
        public DataSet GetBillingNoCollection(string strCompany, string strFrom, string strTo, string strCity, string strDate, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompany;

            ObjparamUser[1] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strFrom;

            ObjparamUser[2] = new SqlParameter("@to", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strTo;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[4] = new SqlParameter("@date", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = strDate;

            ObjparamUser[5] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = currpage;

            ObjparamUser[6] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptbillingnocollection", ObjparamUser);


        }
        public DataSet GetDueDateCollection(string strCompany, string strFrom, string strTo, string strCity, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strCompany;

            ObjparamUser[1] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strFrom;

            ObjparamUser[2] = new SqlParameter("@to", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strTo;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[4] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = currpage;

            ObjparamUser[5] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_RPTDueDateCollection", ObjparamUser);


        }
        public DataSet GetNoBilling(string strFrom, string strTo, string strCompany, string strCity, string strDate, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[8];

            ObjparamUser[0] = new SqlParameter("@from", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strFrom;

            ObjparamUser[1] = new SqlParameter("@to", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strTo;

            ObjparamUser[2] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strCompany;

            ObjparamUser[3] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = strCity;

            ObjparamUser[4] = new SqlParameter("@date", SqlDbType.NVarChar);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = strDate;

            ObjparamUser[5] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[5].Direction = ParameterDirection.Input;
            ObjparamUser[5].Value = currpage;

            ObjparamUser[6] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[6].Direction = ParameterDirection.Input;
            ObjparamUser[6].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptNoBilling", ObjparamUser);


        }

        public DataSet GetExceededCreditLimit(string strFrom, string strCompany, string strCity, int currpage, int Pagesize)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];

            ObjparamUser[0] = new SqlParameter("@date", SqlDbType.NVarChar);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = strFrom;

            ObjparamUser[1] = new SqlParameter("@company", SqlDbType.NVarChar);
            ObjparamUser[1].Direction = ParameterDirection.Input;
            ObjparamUser[1].Value = strCompany;

            ObjparamUser[2] = new SqlParameter("@city", SqlDbType.NVarChar);
            ObjparamUser[2].Direction = ParameterDirection.Input;
            ObjparamUser[2].Value = strCity;

            ObjparamUser[3] = new SqlParameter("@CurrentPage", SqlDbType.Int);
            ObjparamUser[3].Direction = ParameterDirection.Input;
            ObjparamUser[3].Value = currpage;

            ObjparamUser[4] = new SqlParameter("@PageSize", SqlDbType.Int);
            ObjparamUser[4].Direction = ParameterDirection.Input;
            ObjparamUser[4].Value = Pagesize;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_rptExceededCreditLimit", ObjparamUser);


        }
        public DataTable GetClientWisedutyReport()
        {
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_CompanyWiseMTDReport");
        }

        public DataTable MicroSoftPaymentStatus(DateTime FromDate, DateTime ToDate)
        {
            SqlParameter[] param= new SqlParameter[2];

            param[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            param[0].Direction = ParameterDirection.Input;
            param[0].Value = FromDate;

            param[1] = new SqlParameter("@Todate", SqlDbType.DateTime);
            param[1].Direction = ParameterDirection.Input;
            param[1].Value = ToDate;
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_MicrosoftPaymentStatus", param);
        }
        public DataSet GetBookingModifyReport(DateTime _ModifyDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@ModifyDate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _ModifyDate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ExceptionModify", ObjParam);
        }

        public DataSet GetDispatchModifyReport(DateTime _ModifyDate)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@ModifyDate", SqlDbType.DateTime);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = _ModifyDate;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ExceptionModifyDS", ObjParam);
        }

        public DataSet GetRetnalPlan(int month, int Year)
        {
            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Months", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = month;
            ObjParam[1] = new SqlParameter("@Years", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = Year;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_RentalPlan", ObjParam);
        }

        public DataSet GetRetnalPlan(int month, int Year, int cityid)
        {

            SqlParameter[] ObjParam = new SqlParameter[3];
            ObjParam[0] = new SqlParameter("@Months", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = month;
            ObjParam[1] = new SqlParameter("@Years", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = Year;
            ObjParam[2] = new SqlParameter("@CityId", SqlDbType.Int);
            ObjParam[2].Direction = ParameterDirection.Input;
            ObjParam[2].Value = cityid;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_RentalPlan", ObjParam);
        }

        public DataSet GetMonthName(int month, int Year)
        {

            SqlParameter[] ObjParam = new SqlParameter[2];
            ObjParam[0] = new SqlParameter("@Month", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = month;
            ObjParam[1] = new SqlParameter("@Year", SqlDbType.Int);
            ObjParam[1].Direction = ParameterDirection.Input;
            ObjParam[1].Value = Year;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetWEEKDAY", ObjParam);
        }
        public DataSet GetClientName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetClientList");
        }
        public DataSet GetLocations_UserAccessWiseBK(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_BK", ObjParam);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise", ObjParam);
        }
        public DataSet GetProductivityManagerList(int UserID)
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_getUserList");
        }
        public int InsetProductivityManager(int ManagerId, int ClientCoId, int cityId, int CreatedBy)
        {
            try
            {
                SqlParameter[] ObjParam = new SqlParameter[4];
                ObjParam[0] = new SqlParameter("@ManagerId", SqlDbType.Int);
                ObjParam[0].Direction = ParameterDirection.Input;
                ObjParam[0].Value = ManagerId;
                ObjParam[1] = new SqlParameter("@ClientCoId", SqlDbType.Int);
                ObjParam[1].Direction = ParameterDirection.Input;
                ObjParam[1].Value = ClientCoId;
                ObjParam[2] = new SqlParameter("@CityId", SqlDbType.Int);
                ObjParam[2].Direction = ParameterDirection.Input;
                ObjParam[2].Value = cityId;
                ObjParam[3] = new SqlParameter("@SysUserId", SqlDbType.Int);
                ObjParam[3].Direction = ParameterDirection.Input;
                ObjParam[3].Value = CreatedBy;

                object obj;
                obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "InsertProductivityManager", ObjParam);
                return Convert.ToInt32(obj);
            }
            catch (Exception)
            {

                throw new Exception();
            }

        }

        public DataSet GetClientDetails(int SysUserId)
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetProductivityManagerDetails");
        }

        public int InsetInsertProductivityMonth(int ProductivityID, int Months, int Years, int BookingTarget, int CreatedBy)
        {
            try
            {
                SqlParameter[] ObjParam = new SqlParameter[5];
                ObjParam[0] = new SqlParameter("@ProductivityID", SqlDbType.Int);
                ObjParam[0].Direction = ParameterDirection.Input;
                ObjParam[0].Value = ProductivityID;
                ObjParam[1] = new SqlParameter("@Months", SqlDbType.Int);
                ObjParam[1].Direction = ParameterDirection.Input;
                ObjParam[1].Value = Months;
                ObjParam[2] = new SqlParameter("@Years", SqlDbType.Int);
                ObjParam[2].Direction = ParameterDirection.Input;
                ObjParam[2].Value = Years;
                ObjParam[3] = new SqlParameter("@BookingTarget", SqlDbType.Int);
                ObjParam[3].Direction = ParameterDirection.Input;
                ObjParam[3].Value = BookingTarget;
                ObjParam[4] = new SqlParameter("@SysUserId", SqlDbType.Int);
                ObjParam[4].Direction = ParameterDirection.Input;
                ObjParam[4].Value = CreatedBy;
                object obj;
                obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_InsertProductivityMonth", ObjParam);
                return Convert.ToInt32(obj);
            }
            catch (Exception)
            {

                throw new Exception();
            }

        }
        public DataSet GetClientMonthDetails(int ProductivityId)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@ProductivityId", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = ProductivityId;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetProductivityMonthDetails", ObjParam);
        }
    }
}
