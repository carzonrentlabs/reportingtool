using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;
public partial class CorDriveReport_DispatchSummary : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    public void BindGrid()
    {
        objCordrive = new CorDrive();
        DataSet dsDispatchSummary = new DataSet();
        try
        {
            dsDispatchSummary = objCordrive.GetDispatchSummaryReport();
            if (dsDispatchSummary.Tables[0].Rows.Count > 0)
            {
                grdDispatchSummary.DataSource = dsDispatchSummary.Tables[0];
                grdDispatchSummary.DataBind();
                grdDispatchDetails.DataSource = dsDispatchSummary.Tables[1];
                grdDispatchDetails.DataBind();
            }
            else
            {
                grdDispatchSummary.DataSource = null;
                grdDispatchSummary.DataBind();

                grdDispatchDetails.DataSource = null;
                grdDispatchDetails.DataBind();

                lblMessate.Text = "Record not available.";
                lblMessate.Visible = true;
                lblMessate.ForeColor = Color.Red;
            }
          
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }
}
