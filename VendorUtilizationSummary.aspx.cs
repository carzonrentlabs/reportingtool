using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
public partial class VendorUtilizationSummary : clsPageAuthorization
//public partial class VendorUtilizationSummary : System.Web.UI.Page 
{

    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["Userid"] = 173;
        if (!Page.IsPostBack)
        {
            BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string strDateto;
        strDateto = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        Response.Redirect("VendorUtilizationSummaryCrystal.aspx?val=" + strDateto + "," + cityid);
    }

    //protected bool Checkdate()
    //{
    //    try
    //    {
    //        string strMandatoryField = "";

    //        if ((txtFrom.Value.ToString() == "") || (txtTo.Value.ToString() == ""))
    //        {
    //            strMandatoryField = "F";
    //        }
    //        else
    //        {

    //            if ((txtFrom.Value.ToString()) == (txtTo.Value.ToString()))
    //            {
    //                strMandatoryField = "F";
    //            }

    //            else if (Convert.ToDateTime(txtFrom.Value.ToString()) < Convert.ToDateTime(txtTo.Value.ToString()))
    //            {
    //                strMandatoryField = "F";
    //            }
    //            else if (Convert.ToDateTime((txtFrom.Value)) > Convert.ToDateTime((txtTo.Value)))
    //            {
    //                strMandatoryField = "T";
    //            }
    //        }
    //        if (strMandatoryField == "T")
    //        {
    //            string strAlert = "<script>alert('From date should be less than to date')</script>";
    //            if (!ClientScript.IsStartupScriptRegistered("clientScript"))
    //            {
    //                Page.RegisterStartupScript("clientScript", strAlert);
    //            }
    //            strMandatoryField = "";
    //            return false;
    //        }
    //        else
    //        {
    //            return true;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        return false; // throw ex;;
    //    }
    //}
}