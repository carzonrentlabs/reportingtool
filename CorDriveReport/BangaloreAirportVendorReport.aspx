<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BangaloreAirportVendorReport.aspx.cs" Inherits="CorDriveReport_BangaloreAirportVendorReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bangalore Airport Vendor Report</title>
    <link href="../App_Themes/stylesheet/HertzInt.css" rel="stylesheet" />
    <link href="../DatePickerCSS/ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../Css/jquery.gritter.css" rel="stylesheet" type="text/css" />
    <link href="../Css/RepeaterStyle.css" rel="stylesheet" />
    <link href="../greybox/gb_styles.css" rel="stylesheet" />
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>,#<%=btnExport.ClientID%> ").click(function () {
                var id = this.id;
               if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                   alert("Select create from date ");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Select create to date");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    if (id == "bntGet") {
                        ShowProgress();
                    }
                }
            });
       });

        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Bangalore Airport Vendor Report</b>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>From Date :</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                 
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <b>To Date :</b>&nbsp;&nbsp;
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
        </table>
        <br />
        <div style="text-align: center; border: 0" class="Repeater">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdBangaloreAirport" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="BookingId" DataField="BookingId" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Pickup Date" DataField="PickupDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Car RegnNo" DataField="CarRegnNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Car Model Name" DataField="CarModelName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Car Cat Name" DataField="CarCatName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Package" DataField="Package" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Pkg Rate" DataField="PkgRate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Recommended Rate" DataField="RecommendedRate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Vendor Rate" DataField="VendorRate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Actual Paid" DataField="ActualPaid" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Variance TotalInvoice Recommended Rate" DataField="Variance_TotalInvoice_RecommendedRate" HeaderStyle-Wrap="false" />
                           </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
             
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
    </div>
    </form>
</body>
</html>
