﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VVIPTracking.aspx.cs" Inherits="VVIPTracking" %>


<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {});

        function gridviewScroll() {
            $('#<%=gvClientCoDetails.ClientID%>').gridviewScroll({
                width: 1200,
                height: 500
            });
        }



        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        $('form').live("submit", function () {
            ShowProgress();
        });


    </script>


    <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
    
       .FixedHeader 
       {
            position: absolute;
            font-weight: bold;
        }
        
        .RowStyle {
          height: 50px;
        }
        .AlternateRowStyle {
          height: 50px;
        }
        
</style>


        <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width:1200px">
            <tr>
                <td colspan="6" align="center" > <%--bgcolor="#FF6600"--%>
                   <b>VVIP Tracking</b> </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 &nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap; text-align: right;" align="left">
                    <strong>Client Name :</strong></td>
                 <td style="white-space: nowrap; text-align: left;" align="right">
                     <asp:DropDownList ID="ddlClientCoName" runat="server" AutoPostBack="true"
                         onselectedindexchanged="ddlClientCoName_SelectedIndexChanged">
        </asp:DropDownList>
                 
                   </td>
                 <td style="white-space: nowrap; text-align: left;" align="right">
                     &nbsp;&nbsp;
                 
                   <asp:Button ID="btnSearcheVip" runat="Server" 
                        Text="Search VVIP" onclick="btnSearcheVip_Click"/>
                    &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" 
                         onclick="btnReset_Click" />
                 
                   </td>
                <td style="white-space: nowrap;" align="left">
                    &nbsp;&nbsp;</td>
                
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
             <tr>
                <td colspan="6">
                 <div style="width: 100%; height: 400px; overflow: auto;">
                    <asp:GridView ID="gvClientCoDetails" runat="server" AutoGenerateColumns="False" 
            BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" 
            EnableModelValidation="True" ForeColor="#333333"  Width="1280px"
                     onrowdatabound="gvClientCoDetails_RowDataBound" style="height:400px; overflow:auto" 
CssClass="gridViewHeader" HeaderStyle-BackColor="YellowGreen" >
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField HeaderText="Sr.No">
                    <ItemTemplate>
                        <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Client Name" Visible="false">
                    <ItemTemplate>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GuestName">
                    <ItemTemplate>
                        <asp:Label ID="lblGuestName" runat="server" Text='<%# Bind("GuestName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Emailid">
                    <ItemTemplate>
                        <asp:Label ID="lblEmailid" runat="server" Text='<%# Bind("Emailid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PhoneNo">
                    <ItemTemplate>
                        <asp:Label ID="lblPhone1" runat="server" Text='<%# Bind("Phone1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Designation">
                    <ItemTemplate>
                        <asp:Label ID="lblDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                             <asp:TemplateField HeaderText="ISVVIP">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkRow" runat="server"/>
                          <asp:HiddenField ID="hdIsVVIP" runat="server" Value='<%# Bind("IsVVIP") %>' />
                          <asp:HiddenField ID="hdClientCoId" runat="server" Value='<%# Bind("Clientcoid") %>' />
                         <asp:HiddenField ID="hdClientCoIndivId" runat="server" Value='<%# Bind("Clientcoindivid") %>' />
                         <asp:Label ID="lblC1" runat="server" Text='<%# Bind("Clientcoid") %>' Visible="false"></asp:Label>
                         <asp:Label ID="lblC2" runat="server" Text='<%# Bind("Clientcoindivid") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <%--<HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Height="20px" />--%>
             <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" Height="40px" />
        </asp:GridView> 

                   </div> 
                </td>
            </tr>
           
        </table>
            <br />
            <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" Visible="false"/>


        <div class="loading" align="center">
            Please wait...<br />
            <br />
            <img src="images/loader.gif" alt="" />
        </div>
    </center>




</asp:Content>

    
