<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CarDetail_RC.aspx.cs" Inherits="CarDetail_RC" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    
<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}
</script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Car Wise Detail</u></strong>
                </td>
            </tr>
            <tr>
                <td nowrap ="nowrap" align ="right"><asp:CheckBox ID="chkAll" runat ="server"/>  All Car</td>
                <td align="right" style="width: 100Px">Date</td>
                <td align="left" style="width: 11px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 105px"> City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>
                
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="7">
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>
            
        <tr>
        <td align="center" colspan="7">
        <asp:GridView runat="server" ID="GridView1" PageSize="20" EmptyDataText="No Record Found" AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="GridView1_PageIndexChanging">
        <Columns>
        
        <asp:TemplateField HeaderText="Location">
        <ItemTemplate>
        <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Regn No.">
        <ItemTemplate>
        <asp:Label ID="lblregnno" Text='<% #Bind("VehicleNo") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Car Model">
        <ItemTemplate>
        <asp:Label ID="lblmodel" Text='<% #Bind("carmodel") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>   
        
        <asp:TemplateField HeaderText="Car Category">
        <ItemTemplate>
        <asp:Label ID="lblmodel" Text='<% #Bind("CarCatName") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>       
        
        <asp:TemplateField HeaderText="RC Date">
        <ItemTemplate>
        <asp:Label ID="lblrcdate" Text='<% #Bind("ManufacturingDate") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Vendor Name">
        <ItemTemplate>
        <asp:Label ID="lblVendorName" Text='<% #Bind("VendorName") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        <asp:TemplateField HeaderText="Vendor Contact No.">
        <ItemTemplate>
        <asp:Label ID="lblcontact" Text='<% #Bind("VendorContact") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Vendor Address">
        <ItemTemplate>
        <asp:Label ID="lbladdress" Text='<% #Bind("CarVendorAddress") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Aging">
        <ItemTemplate>
        <asp:Label ID="lbltype" Text='<% #Bind("datediff") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        </Columns>
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        </td>
        </tr>
        
        </tbody>
    </table>
    
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
        top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
        scrolling="no" height="182"></iframe>
    </div>

</asp:Content>