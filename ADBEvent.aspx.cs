using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
using System.Drawing;

public partial class ADB_Event : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                btnSubmit.Attributes.Add("onclick", "return validate();");
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        DataTable EventReport = new DataTable();
        EventReport = objAdmin.ADBEventReport(FromDate, Todate);

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=ADBEventReport.xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        this.EnableViewState = false;
        Response.Write("\r\n");
        if (EventReport.Rows.Count > 0)
        {
            Response.Write("<table border = '1' align = 'center'> ");
            int[] iColumns = { 0, 1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
            for (int i = 0; i < EventReport.Rows.Count; i++)
            {
                if (i == 0)
                {
                    //Response.Write("<tr>");
                    //Response.Write("<td align='left'><b></b></td>");
                    //Response.Write("<td align='center' colspan='5'><b>Duty Slip Created Post Dispatch</b></td>");
                    //Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (EventReport.Columns[iColumns[j]].Caption.ToString() == "CarNo")
                        {
                            Response.Write("<td align='left'><b>Car No.</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "carmodel")
                        {
                            Response.Write("<td align='left'><b>Car Model</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "carcategory")
                        {
                            Response.Write("<td align='left'><b>Car Category</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "PickUpDate")
                        {
                            Response.Write("<td align='left'><b>Usage Date</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "bookingId")
                        {
                            Response.Write("<td align='left'><b>Booking ID</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "UserName")
                        {
                            Response.Write("<td align='left'><b>User Name</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "DateOut")
                        {
                            Response.Write("<td align='left'><b>Date Out</b></td>");

                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "DateIn")
                        {
                            Response.Write("<td align='left'><b>Date In</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "KMOut")
                        {
                            Response.Write("<td align='left'><b>Op. KM.</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "KMIn")
                        {
                            Response.Write("<td align='left'><b>Cl. KM.</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "TimeOut")
                        {
                            Response.Write("<td align='left'><b>Op.Time</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "TimeIn")
                        {
                            Response.Write("<td align='left'><b>Cl. Time</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "TotalKm")
                        {
                            Response.Write("<td align='left'><b>Total KM</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "TotalHrs")
                        {
                            Response.Write("<td align='left'><b>Total Hrs.</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "PkgRate")
                        {
                            Response.Write("<td align='left'><b>Package Rate</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "ExtraKMCost")
                        {
                            Response.Write("<td align='left'><b>Extra Km Amt</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "ExtraHrCost")
                        {
                            Response.Write("<td align='left'><b>Extra Hr Amt</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "Revenue")
                        {
                            Response.Write("<td align='left'><b>Total Revenue</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "ParkTollChages")
                        {
                            Response.Write("<td align='left'><b>Parking</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "serviceTax")
                        {
                            Response.Write("<td align='left'><b>Service Tax</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "TotalCost")
                        {
                            Response.Write("<td align='left'><b>Total Billing</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                        {
                            Response.Write("<td align='left'><b>Vendor Name</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "VendorPackage")
                        {
                            Response.Write("<td align='left'><b>Vendor Package</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "VendorRate")
                        {
                            Response.Write("<td align='left'><b>Vendor Rate</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "ExtraKMRate")
                        {
                            Response.Write("<td align='left'><b>Extra KM Rate</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "ExtraHrsRate")
                        {
                            Response.Write("<td align='left'><b>Extra Hrs Rate</b></td>");
                        }
                        else if (EventReport.Columns[iColumns[j]].Caption.ToString() == "VendorShare")
                        {
                            Response.Write("<td align='left'><b>Vendor Amount</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + EventReport.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' width = '100%'>");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }

    }
    void FillGrid()
    {
         
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        DataTable EventReport = new DataTable();
        EventReport = objAdmin.ADBEventReport(FromDate, Todate);
        if (EventReport.Rows.Count > 0)
        {
            txtMessage.Visible = false;
            GrvADBEventReport.DataSource = EventReport;
            GrvADBEventReport.DataBind();
        }
        else
        {
            GrvADBEventReport.DataSource = EventReport;
            GrvADBEventReport.DataBind();
            txtMessage.Visible = true;
            txtMessage.Text = "Record is not available.";
            txtMessage.ForeColor = Color.Red;
           
        }
    }
}
