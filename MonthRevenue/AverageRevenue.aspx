﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AverageRevenue.aspx.cs" Inherits="AverageRevenue" %>

<asp:Content ID="cphAverageRevenue" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=bntGet.ClientID%>,#<%=bntExprot.ClientID%>").click(function () {

                if (document.getElementById("<%=ddFromMonth.ClientID%>").selectedIndex == 0) {
                    alert("Select from month")
                    document.getElementById("<%=ddFromMonth.ClientID%>").focus();
                    return false;
                }
                else if (document.getElementById("<%=ddlFromYear.ClientID%>").selectedIndex == 0) {
                    alert("Select form year")
                    document.getElementById("<%=ddlFromYear.ClientID%>").focus();
                    return false;
                }
                else if (document.getElementById("<%=ddToMonth.ClientID%>").selectedIndex == 0) {
                    alert("Select from month")
                    document.getElementById("<%=ddToMonth.ClientID%>").focus();
                    return false;
                }
                else if (document.getElementById("<%=ddlToYear.ClientID%>").selectedIndex == 0) {
                    alert("Select from year")
                    document.getElementById("<%=ddlToYear.ClientID%>").focus();
                    return false;
                }
            });
        });
   
    </script>
    <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
                <tr>
                    <td colspan="4" align="center">
                        <b>Pickup City wise Average Revenue/Transaction</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <label id="lblUnit" for="ddlUnit">
                            Select City</label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlCityName" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<label id="lblFromMonth" for="ddFromMonth">
                            From Month</label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddFromMonth" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlFromYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<label id="lblToMonth" for="ddToMonth">
                            To Month</label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddToMonth" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlToYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;
                        <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <b>
                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></b></div>
        <div>
            <br />
            <asp:GridView ID="grvRevenuSummary" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="S.No.">
                        <ItemTemplate>
                            <asp:Label ID="lblSNo" runat="server" Text="<%#Container.DisplayIndex + 1%>">"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Client Name" DataField="ClientName" />
                    <%--<asp:BoundField HeaderText="Relation Manager" DataField="RelationManager" />
                    <asp:BoundField HeaderText="Payment Mode" DataField="paymentTerms" />--%>
                    <asp:BoundField HeaderText="Total Duties" DataField="TotalDuties" />
                    <asp:BoundField HeaderText="Total Revenue" DataField="Revenue" />
                    <asp:BoundField HeaderText="Avg TRX" DataField="AVGTrans" />
                    <asp:BoundField HeaderText="Avg Tkt" DataField="AvgTicket" />
                    <asp:BoundField HeaderText="Avg Rev" DataField="AVGRevenue" />
                </Columns>
            </asp:GridView>
        </div>
    </center>
</asp:Content>
