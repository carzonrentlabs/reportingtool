using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;
public partial class CorDriveReport_IncentiveReport : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsIncentive = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
       lblMessage.Text = string.Empty;
       //Session["UserID"] = 3122;
    }
    protected void ddlEvaluationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEvaluationType.SelectedItem.Text.ToUpper() == "MONTHLY")
        {
            objCordrive = new CorDrive();
            ddlEvalutionDate.DataSource = objCordrive.GetTableMonthly();
            ddlEvalutionDate.DataTextField = "Month";
            ddlEvalutionDate.DataBind();
            ddlEvalutionDate.Items.Insert(0, new ListItem("--Select Month--", "0"));
        }
        if (ddlEvaluationType.SelectedItem.Text.ToUpper() == "QUARTERLY")
        { 
            objCordrive = new CorDrive();
            ddlEvalutionDate.DataSource = objCordrive.GetTableQuartely();
            ddlEvalutionDate.DataTextField = "Quarter";
            ddlEvalutionDate.DataBind();
            ddlEvalutionDate.Items.Insert(0, new ListItem("--Select Quarter--", "0"));
        }
    }
    public string GetQuarterLastDate(string firstDayOfQuarter)
    {
        string lastDayOfQuarter = Convert.ToDateTime(firstDayOfQuarter).AddMonths(3).AddDays(-1).ToShortDateString();
        return lastDayOfQuarter;
    }
    public string GetMonthlyStartEndDate(int month)
    {
        string PrevMonth = DateTime.Now.AddMonths(month).ToShortDateString();
        string[] arPrevMonth = PrevMonth.Split('/');
        string StartDate = arPrevMonth[0].ToString() + "-" + "1" + "-" + arPrevMonth[2].ToString();
        int DaysInMonth = DateTime.DaysInMonth(Convert.ToInt32(arPrevMonth[2]), Convert.ToInt32(arPrevMonth[0]));
        string EndDate = Convert.ToInt32(arPrevMonth[0]) + "-" + DaysInMonth + "-" + arPrevMonth[2].ToString();
        string StarAndEndDate = StartDate + "#" + EndDate;
        return StarAndEndDate;
    }
    protected void btnGet_Click(object sender, EventArgs e)
    {
        try
        {
            if ((ddlEvaluationType.SelectedItem.Value != "0") && (ddlEvalutionDate.SelectedItem.Value != "0"))
            {
                if (ddlEvaluationType.SelectedItem.Text.ToUpper() == "QUARTERLY")
                {
                    GetQuarterlyIncentive();
                }
                else if (ddlEvaluationType.SelectedItem.Text.ToUpper() == "MONTHLY")
                {
                    GetMonthlyIncentive();
                }
            }
            else
            {
                lblMessage.Text = "Please Select Evaluation Type and Date";
                lblMessage.Visible = true;
                
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
            lblMessage.Visible = true;
        }
    }
    public void GetQuarterlyIncentive()
    {
        objCordrive = new CorDrive();
        string firstDayOfQuarter = string.Empty;
        if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q1-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "01" + "-" + "01" + "-" + DateTime.Now.Year.ToString();

        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q2-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "04" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q3-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "07" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q4-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "10" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        string lastDayOfQuarter = GetQuarterLastDate(firstDayOfQuarter);

        dsIncentive = objCordrive.GetIncentive(ddlEvaluationType.SelectedItem.Text.ToString(), Convert.ToDateTime(firstDayOfQuarter), Convert.ToDateTime(lastDayOfQuarter));
        if (dsIncentive != null)
        {
            if (dsIncentive.Tables[0].Rows.Count > 0)
            {
                gvIncentive.DataSource = dsIncentive.Tables[0];
                gvIncentive.DataBind();
            }
            else
            {
                gvIncentive.DataSource = null;
                gvIncentive.DataBind();
            }
        }
    
    }
    public void GetMonthlyIncentive()
    {
        objCordrive = new CorDrive();
        string StartDate = string.Empty;
        string EndDate = string.Empty;
        int Month = 0;
        if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Jan-" + DateTime.Now.Year.ToString())
        {
            Month = 0;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Feb-" + DateTime.Now.Year.ToString())
        {
            Month = -1;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Mar-" + DateTime.Now.Year.ToString())
        {
            Month = -2;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Apr-" + DateTime.Now.Year.ToString())
        {
            Month = -3;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "May-" + DateTime.Now.Year.ToString())
        {
            Month = -4;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Jun-" + DateTime.Now.Year.ToString())
        {
            Month = -5;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Jul-" + DateTime.Now.Year.ToString())
        {
            Month = -6;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Aug-" + DateTime.Now.Year.ToString())
        {
            Month = -7;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Sep-" + DateTime.Now.Year.ToString())
        {
            Month = -8;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Oct-" + DateTime.Now.Year.ToString())
        {
            Month = -9;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Nov-" + DateTime.Now.Year.ToString())
        {
            Month = -10;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Dec-" + DateTime.Now.Year.ToString())
        {
            Month = -11;
        }
        string MothStartAndEndDate = GetMonthlyStartEndDate(Month);
        string[] StartEnd = MothStartAndEndDate.Split('#');
        StartDate = StartEnd[0].ToString();
        EndDate = StartEnd[1].ToString();
        dsIncentive = objCordrive.GetIncentive(ddlEvaluationType.SelectedItem.Text.ToString(), Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate));
        if (dsIncentive.Tables[0].Rows.Count > 0)
        {
            gvIncentive.DataSource = dsIncentive.Tables[0];
            gvIncentive.DataBind();
        }
        else
        {
            gvIncentive.DataSource = null;
            gvIncentive.DataBind();
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if((ddlEvaluationType.SelectedItem.Value != "0") && (ddlEvalutionDate.SelectedItem.Value != "0"))
        {
            if (ddlEvaluationType.SelectedItem.Text.ToUpper() == "QUARTERLY")
            {
                GetQuarterlyIncentiveExportToExcel();
            }
            else if (ddlEvaluationType.SelectedItem.Text.ToUpper() == "MONTHLY")
            {
                GetMonthlyIncentiveExportToExcel();
            }
        }
        else
        {
            lblMessage.Text = "Please Select Evaluation Type and Date";
            lblMessage.Visible = true;

        }
    }

    public void GetQuarterlyIncentiveExportToExcel()
    {
        objCordrive = new CorDrive();
        string firstDayOfQuarter = string.Empty;
        if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q1-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "01" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q2-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "04" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q3-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "07" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Q4-" + DateTime.Now.Year.ToString())
        {
            firstDayOfQuarter = "10" + "-" + "01" + "-" + DateTime.Now.Year.ToString();
        }
        string lastDayOfQuarter = GetQuarterLastDate(firstDayOfQuarter);

        dsIncentive = objCordrive.GetIncentive(ddlEvaluationType.SelectedItem.Text.ToString(), Convert.ToDateTime(firstDayOfQuarter), Convert.ToDateTime(lastDayOfQuarter));
        WorkbookEngine.ExportDataSetToExcel(dsIncentive, "IncentiveReport.xls");
       
    }
    public void GetMonthlyIncentiveExportToExcel()
    {
        objCordrive = new CorDrive();
        string StartDate = string.Empty;
        string EndDate = string.Empty;
        int Month = 0;
        if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Jan-" + DateTime.Now.Year.ToString())
        {
            Month = 0;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Feb-" + DateTime.Now.Year.ToString())
        {
            Month = -1;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Mar-" + DateTime.Now.Year.ToString())
        {
            Month = -2;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Apr-" + DateTime.Now.Year.ToString())
        {
            Month = -3;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "May-" + DateTime.Now.Year.ToString())
        {
            Month = -4;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Jun-" + DateTime.Now.Year.ToString())
        {
            Month = -5;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Jul-" + DateTime.Now.Year.ToString())
        {
            Month = -6;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Aug-" + DateTime.Now.Year.ToString())
        {
            Month = -7;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Sep-" + DateTime.Now.Year.ToString())
        {
            Month = -8;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Oct-" + DateTime.Now.Year.ToString())
        {
            Month = -9;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Nov-" + DateTime.Now.Year.ToString())
        {
            Month = -10;
        }
        else if (ddlEvalutionDate.SelectedItem.Text.ToString() == "Dec-" + DateTime.Now.Year.ToString())
        {
            Month = -11;
        }
        string MothStartAndEndDate = GetMonthlyStartEndDate(Month);
        string[] StartEnd = MothStartAndEndDate.Split('#');
        StartDate = StartEnd[0].ToString();
        EndDate = StartEnd[1].ToString();
        dsIncentive = objCordrive.GetIncentive(ddlEvaluationType.SelectedItem.Text.ToString(), Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate));
        WorkbookEngine.ExportDataSetToExcel(dsIncentive, "IncentiveReport.xls");
        
    }
}
