using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ExcelUtil;
using ReportingTool;

public partial class AcceptHandOver : System.Web.UI.Page
{
    static DataTable GridViewDT = new DataTable();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
        if (!Page.IsPostBack)
        {
            BindGrid();
        }
    }

    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }

    private void BindGrid()
    {
        DataSet ds = new DataSet();
        objCordrive = new CorDrive();
        ds = objCordrive.GetHardCopyHandoverDetails(Convert.ToInt32(Session["UserID"]), false);
        GridViewDT = ds.Tables[0];
        if (ds.Tables[0].Rows.Count > 0)
        {
            grdAcceptHandover.DataSource = ds.Tables[0];
            grdAcceptHandover.DataBind();
            btnSaveAcceptHandover.Visible = true;
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Bookings not available for Accepting HandOver.";
            grdAcceptHandover.DataSource = null;
            grdAcceptHandover.DataBind();
            btnSaveAcceptHandover.Visible = false;
        }
    }

    protected void grdHandover_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "RemoveEntry")
            {
                foreach (DataRow dtRow in GridViewDT.Rows)
                {
                    if (dtRow["BookingID"].ToString().Trim() == e.CommandArgument.ToString().Trim())
                    {
                        GridViewDT.Rows.Remove(dtRow);
                        grdAcceptHandover.DataSource = GridViewDT;
                        grdAcceptHandover.DataBind();
                        break;
                    }
                }
                if (grdAcceptHandover.Rows.Count <= 0)
                {
                    btnSaveAcceptHandover.Visible = false;
                    lblMsg.Visible = true;
                    lblMsg.Text = "No Record available for Accepting Handover.";
                }
                else
                {
                    btnSaveAcceptHandover.Visible = true;
                    lblMsg.Visible = false;
                    lblMsg.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
            //throw ex;
        }
    }
    protected void btnSaveAcceptHandover_Click(object sender, EventArgs e)
    {
        if (grdAcceptHandover.Rows.Count > 0)
        {
            int k = 0;
            foreach (GridViewRow dtRow in grdAcceptHandover.Rows)
            {
                CheckBox chkHandover = (CheckBox)dtRow.FindControl("chkHandover");
                Label lblBookingId = (Label)dtRow.FindControl("lblBookingId");
                if (chkHandover.Checked)
                {
                    k += 1;

                    //update handover status
                    objCordrive = new CorDrive();
                    objCordrive.GetHardCopyHandover(Convert.ToInt32(lblBookingId.Text), Convert.ToInt32(Session["UserID"]), 0);
                    BindGrid();

                    lblMsg.Visible = true;
                    lblMsg.Text = "Handover Accepted Successfully.";
                }
            }
            if (k == 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Please select at least 1 booking for Handover.";
            }
            //else
            //{
            //    lblMsg.Visible = false;
            //    lblMsg.Text = "";
            //}
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "No Record available for Handover.";
        }
    }
}