using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;


/// <summary>
/// Summary description for clsIssue
/// </summary>
namespace ReportingTool
{
    public class clsIssue
    {
	    public clsIssue()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }

        public DataSet GetIssueCatagory()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetIssueCatagory");
        }
        public DataSet GetAllCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAllCompanyName");
        }
        public DataSet GetUserName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetUserName");
        }
        public DataSet GetBatchID(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetBatchID", objParams);
            return dsFillData;
        }
        public DataSet GetInvoiceID(int BatchID)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@BatchID", BatchID);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetInvoiceID", objParams);
            return dsFillData;
        }
        public DataSet GetPaymentID(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetPaymentID", objParams);
            return dsFillData;
        }
        public int AddNewIssue(string Issue, string Title, string IssueDesc, string Company, string ContactPerson, string Phone, string Email, int BatchID, int InvoiceID, int PaymentID, int User, string Priority, DateTime TargetDate,int UID)
        {
            SqlParameter[] objParams = new SqlParameter[15];
            objParams[0] = new SqlParameter("@Issue", Issue);
            objParams[1] = new SqlParameter("@Title", Title);
            objParams[2] = new SqlParameter("@IssueDesc", IssueDesc);
            objParams[3] = new SqlParameter("@Company", Company);
            objParams[4] = new SqlParameter("@ContactPerson", ContactPerson);
            objParams[5] = new SqlParameter("@Phone", Phone);
            objParams[6] = new SqlParameter("@Email", Email);
            objParams[7] = new SqlParameter("@BatchID", BatchID);
            objParams[8] = new SqlParameter("@InvoiceID", InvoiceID);
            objParams[9] = new SqlParameter("@PaymentID", PaymentID);
            objParams[10] = new SqlParameter("@User", User);
            objParams[11] = new SqlParameter("@Priority", Priority);
            objParams[12] = new SqlParameter("@TargetDate", SqlDbType.DateTime);
            objParams[12].Value = TargetDate;
            objParams[13] = new SqlParameter("@CreatedBy", UID);
            int result1;
            result1 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspAddNewIssue", objParams);
            return result1;
        }
        public DataSet GetIssueData(int CrteateBy)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@AllocatedID", CrteateBy);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetIssueData", objParams);
        }
        public DataSet GetIssueDataByUser(int User)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@User", User);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetIssueDataByUser",objParams);
        }
        public DataSet GetIssueDataByStatus(string Status)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@Status", Status);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetIssueDataByStatus", objParams);
        }
        public DataSet GetUserData(int IssueID)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@IssueID", IssueID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetUserData", objParams);
        }
        public int AddActionIssue(int IssueID, DateTime FollowUpDate, string ActionDesc, int User, 
            string FollowUpDesc, string Status, int UID)
        {
            SqlParameter[] objParams = new SqlParameter[15];
            objParams[0] = new SqlParameter("@IssueID", IssueID);
            objParams[1] = new SqlParameter("@FollowUpDate", SqlDbType.DateTime);
            objParams[1].Value = FollowUpDate;
            objParams[2] = new SqlParameter("@ActionDesc", ActionDesc);
            objParams[3] = new SqlParameter("@User", User);
            objParams[4] = new SqlParameter("@FollowUpDesc", FollowUpDesc);
            objParams[5] = new SqlParameter("@Status", Status);
            objParams[6] = new SqlParameter("@CreatedBy", UID);
            objParams[7] = new SqlParameter("@s", SqlDbType.Int);
            objParams[7].Direction = ParameterDirection.ReturnValue;
            int result1;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspAddActionIssue", objParams);
             result1 = Convert.ToInt32(objParams[7].Value.ToString());
            return result1;
        }
        public DataSet GetActionData(int IssueID)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@IssueID", IssueID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetActionData", objParams);
        }
        public string GetMaxIssueID()
        {
            //return Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UspGetMaxIssueID"));
            return Convert.ToString(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UspGetMaxIssueID"));
        }
        public DataSet GetQuickSearchData(int Selected, double ModID, string Guest, string InvDate,
            string UsageDate, string InsNo, double InsAmount, string InsDate,
            string PeriodF, string PeriodT, int ActionBy, int Status,int currentpage,int pagesize)
        {
            SqlParameter[] objParams = new SqlParameter[15];
            objParams[0] = new SqlParameter("@Module", Selected);
            objParams[1] = new SqlParameter("@ModuleID", ModID);
            objParams[2] = new SqlParameter("@GuestName", Guest);
            objParams[3] = new SqlParameter("@Invoicedate", InvDate);
            objParams[4] = new SqlParameter("@usagecarDate", UsageDate);
            objParams[5] = new SqlParameter("@InstrumentNo", InsNo);
            objParams[6] = new SqlParameter("@Instrumentamt", InsAmount);
            objParams[8] = new SqlParameter("@InstrumentDate", InsDate);
           
            objParams[9] = new SqlParameter("@PeriodFrom", PeriodF);
            objParams[10] = new SqlParameter("@PeriodTo", PeriodT);
            objParams[11] = new SqlParameter("@ActionBy", ActionBy);
            objParams[12] = new SqlParameter("@Status", Status);
            objParams[13] = new SqlParameter("@currentpage", currentpage);
            objParams[14] = new SqlParameter("@pagesize", pagesize);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspQuickSearch1", objParams);
        }
        public DataSet GetUnappliedCreditData()
        {
            SqlParameter[] objParams = new SqlParameter[2];
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetUnappliedCredit", objParams);
        }

                
        public DataSet GetUnappliedCreditDataByPay(int PaymentID)
        {
            SqlParameter[] objParams = new SqlParameter[2];
            objParams[0] = new SqlParameter("@PaymentID", PaymentID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetUnappliedCreditByPay", objParams);
        }
        public DataSet GetIssueDataBySearch(int AllocatedTo,int AllocatedBy,int CreatedBy,string Status,string Priority)
        {
            SqlParameter[] objParams = new SqlParameter[5];
            objParams[0] = new SqlParameter("@Allocatedto", AllocatedTo);
            objParams[1] = new SqlParameter("@Allocatedby", AllocatedBy);
            objParams[2] = new SqlParameter("@Createdby", CreatedBy);
            objParams[3] = new SqlParameter("@Status", Status);
            objParams[4] = new SqlParameter("@Priority", Priority);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspIssueSearch", objParams);
        }
        public DataSet GetEmailMsg()
        {
            SqlParameter[] objParams = new SqlParameter[2];
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspGetEmailMsg", objParams);
        }
        public int SendMail(string Msg,string Subject,string AcessType,string CC)
        {
            SqlParameter[] objParams = new SqlParameter[4];
            objParams[0] = new SqlParameter("@msg", Msg);
            objParams[1] = new SqlParameter("@sub", Subject);
            objParams[2] = new SqlParameter("@AcessType", AcessType);
            objParams[3] = new SqlParameter("@CC", CC);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "dbo.Send_Mail", objParams);
        }
    }
}