<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ADBEvent.aspx.cs" Inherits="ADB_Event"
    MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
    window.history.forward(1);   
        function validate()
        {
            if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
            {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }
            if(document.getElementById('<%=txtTo.ClientID %>').value == "")
            {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>

    <table width="50%" border="0" align="center" id="TABLE1">
        <tbody>
            <tr>
                <td align="center" colspan="9" style="height: 18px">
                    <strong><u>ADB Events</u></strong>&nbsp;</td>
            </tr>
            <tr>
                <td align="left" style ="white-space :nowrap">
                    From Date</td>
                <td style="white-space: nowrap">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style ="white-space :nowrap">
                    To Date</td>
                <td style="white-space: nowrap">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="center" colspan="8" style="height: 22px">
                    <asp:Button ID="btnSubmit" runat="server" Text="Get >>" OnClick="btnSubmit_Click" />
                </td>
                <td>
                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" OnClick="btnExportToExcel_Click" /></td>
            </tr>
            <tr>
                <td colspan="6" align ="center"><asp:Label ID="txtMessage" runat ="server" Visible ="false"></asp:Label></td>
                </tr>
        </tbody>
    </table>
    <table border="0" align="center" >
        <tbody><tr><td>
            <asp:GridView ID="GrvADBEventReport" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both">
                <Columns>
                    <asp:BoundField DataField="CarNo" HeaderText="Car No." ReadOnly="True" SortExpression="CarID"
                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="carmodel" HeaderText="Car Model" ReadOnly="True" SortExpression="CarModelName"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="carcategory" HeaderText="Car Category" ReadOnly="True"
                        SortExpression="BookedCategory" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="PickUpDate" HeaderText="Usage Date" ReadOnly="True" SortExpression="PickUpDate"
                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="BookingID" HeaderText="Booking Id" ReadOnly="True" SortExpression="BookingID"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="UserName" HeaderText="User Name" ReadOnly="True" SortExpression="UserName"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="DateOut" HeaderText="Date Out" ReadOnly="True" SortExpression="DateOut"
                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="DateIn" HeaderText="Date In" ReadOnly="True" SortExpression="DateIn"
                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="KMOut" HeaderText="Op. Km." ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="KMIn" HeaderText="Cl. Km." ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="TimeOut" HeaderText="Op. Time" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="TimeIn" HeaderText="Cl. Time" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="TotalKM" HeaderText="Total Km." ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="TotalHrs" HeaderText="Total Hrs" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="PkgRate" HeaderText="Package Rate" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="ExtraKMCost" HeaderText="Extra Km Amt." ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="ExtraHrCost" HeaderText="Extra Hr Amt." ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="revenue" HeaderText="Total Revenue" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="ParkTollChages" HeaderText="Toll/Parking" ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="serviceTax" HeaderText="Ser Tax" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="TotalCost" HeaderText="Total Billing" ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="VendorName" HeaderText="Vendor Name" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="VendorPackage" HeaderText="Vendor Package" ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="VendorRate" HeaderText="Vendor Rate" ReadOnly="True" HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="ExtraKMRate" HeaderText="Extra Km Rate" ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="ExtraHrsRate" HeaderText="Extra Hr Rate" ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                    <asp:BoundField DataField="VendorShare" HeaderText="Vendor Amount" ReadOnly="True"
                        HeaderStyle-Wrap="false" />
                </Columns>
                <FooterStyle BackColor="Tan" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
            </asp:GridView>
            </td></tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
