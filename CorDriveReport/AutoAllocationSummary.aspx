<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AutoAllocationSummary.aspx.cs" Inherits="CorDriveReport_AutoAllocationSummary"
    Title="Auto Allocation Summary" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       $(document).ready(function () 
       {
            $("#<%=txtFromDate.ClientID%>").datepicker();
       
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select the From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
               
                else {
                    ShowProgress();
                }
            });
       });

        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() 
        {  
            setTimeout(function () 
            {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            },200);
        }

    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Cor Drive Auto Allocation Summary</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup From Date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                </td>
            </tr>
        </table>
        <br />
        <div style="text-align: center; border: 0" class="Repeater">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdAutoAllocation" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Pending Or Timeout" DataField="pending_or_timeout" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Awarded" DataField="awarded" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="No Car Accepted" DataField="no_car_accepted" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="No Car Available" DataField="no_car_available" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total" DataField="total" HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="Repeater" style="width: 1350px; height: 400px; overflow: auto">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdAutoAllocationDetails" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField HeaderText="Booking Id" DataField="BookingId" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Number Of Bid Attempts" DataField="number_of_bid_attempts"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Auto Allocation Bid Status" DataField="AutoAllocationBidStatus"
                                    HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Awarded Car" DataField="awardedCar" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
