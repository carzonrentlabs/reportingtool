﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class CorDriveReport_WaitingForPayment : System.Web.UI.Page
{
    private CallTaker objCallTaker = null;
    DataSet dsWaitingForPayment = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
        //Session["UserID"] = 3122;
        if (!IsPostBack)
        {
            BindAllClient();
            BindAllCity();
            txtCreatePickUpDate.Text = System.DateTime.Now.ToShortDateString();
           // Convert.ToDateTime(txtPickUpDate.Text).ToString("MM-dd-yyyy");
        }
    }
    private void BindAllCity()
    {
        try
        {
            objCallTaker = new CallTaker();
            DataSet dsLocation = new DataSet();

            dsLocation = objCallTaker.GetAllCityName();
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {

                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                    ddlCity.Items.Insert(0, new ListItem("--All--", "0"));

                   
                }
            }

        }
        catch (Exception Ex)
        {

            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
    private void BindAllClient()
    {
        try
        {
            objCallTaker = new CallTaker();
            DataSet dsClient= new DataSet();
            dsClient = objCallTaker.GetAllClientName();
            if (dsClient != null)
            {
                if (dsClient.Tables.Count > 0)
                {
                    ddlClient.DataTextField = "ClientCoName";
                    ddlClient.DataValueField = "ClientCoID";
                    ddlClient.DataSource = dsClient;
                    ddlClient.DataBind();
                    ddlClient.Items.Insert(0, new ListItem("--All--", "0"));
                }
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCallTaker = new CallTaker();
        try
        {
            
            dsWaitingForPayment = objCallTaker.GetWaitingForPaymentDetails(Convert.ToDateTime(txtCreatePickUpDate.Text), Convert.ToInt32(ddlClient.SelectedValue), Convert.ToInt32(ddlCity.SelectedValue),Convert.ToInt32(rdbCreatePickupDate.SelectedValue));
            if (dsWaitingForPayment.Tables[0].Rows.Count > 0)
            {
                gvWaitingForPayment.DataSource = dsWaitingForPayment.Tables[0];
                gvWaitingForPayment.DataBind();

            }
            else
            {
                gvWaitingForPayment.DataSource = null;
                gvWaitingForPayment.DataBind();
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
   
    protected void gvWaitingForPayment_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvWaitingForPayment.Rows[index];
                int BookingId = Convert.ToInt32(row.Cells[0].Text);
                SMSServices.SendSMS objSMS = new SMSServices.SendSMS();
                string MailMsg = objSMS.PendingforPaymentMail(BookingId, 1);
                if (MailMsg == "Success")
                {
                    ShowMessage("Link Sent Successfully");
                    lblMsg.Visible = true;
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                    lblMsg.Text = "Link Sent Successfully";
                }
                else
                {
                    ShowMessage("Link Not Sent Successfully");
                }
              
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }

    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }
}