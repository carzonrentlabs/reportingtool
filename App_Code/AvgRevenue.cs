﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AverageRevenue
/// </summary>
namespace ReportingTool
{
    public class AvgRevenue
    {
        public AvgRevenue()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable GetAverageRevenue(int fromYear, int fromMonth, int toYear, int toMonth, int cityID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];
            ObjparamUser[0] = new SqlParameter("@FromYear", fromYear);
            ObjparamUser[1] = new SqlParameter("@ToYear", toYear);
            ObjparamUser[2] = new SqlParameter("@FromMonth", fromMonth);
            ObjparamUser[3] = new SqlParameter("@ToMonth", toMonth);
            ObjparamUser[4] = new SqlParameter("@CityId", cityID);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_PickupCitywiseAverageRevenue", ObjparamUser);
        }
       
    }
}