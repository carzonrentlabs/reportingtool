<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BulkUpblocking.aspx.cs" Inherits="BulkUpblocking" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <table align="center">
        <tr>
            <td style="height: 30px; font-size: medium;" valign="middle">
                &nbsp;<b><p>
                    Bulk UnLocking (Max 50 Bookings)</p>
                </b>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;" valign="top">
                <p>
                    <b>
                        <asp:Label ID="lbllabel" Text="BookingID" runat="server"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtBookingID" runat="server" TextMode="MultiLine" Height="100px"
                        Width="253px"></asp:TextBox></p>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;" align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="UnLock" OnClick="btnSubmit_Click" />&nbsp;&nbsp;
                <asp:Label ID="lblmessage" runat="server" Text="" color="red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;">
            </td>
        </tr>
    </table>
</asp:Content>
