<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SSCTrackingReport_Summary.aspx.cs" Inherits="SSCTrackingReport_Summary" Title="" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById("<%= txtFrom.ClientID %>").value == "")
    {
        alert("Please Enter From Date.");
        return false;
    }
    
    if(document.getElementById("<%= txtTo.ClientID %>").value == "")
    {
        alert("Please Enter To Date.");
        return false;
    }
}
</script>
<table width = "902" border = "0" align = "center">
    <tbody>
        <tr>
            <td align="center" colspan="7">
                <strong><u>SSC Tracking Summary Report</u></strong>
            </td>
        </tr>
       <tr>
            <td align="right" style="width: 605px; height: 25px;">
                <asp:Label ID="lblOption" runat="server">Accounting Date</asp:Label></td>
            <td align="right" style="width: 36px; height: 25px;">
                From :</td>
            <td align="left" style="width: 47px; height: 25px;">
                <input id="txtFrom" runat="server" type="text" readonly="readOnly" /></td>
            <td width="26" align="left" style="height: 25px">
                <img alt="Click here to open the calendar and select the date corresponding to 'From Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" /></td>
            <td width="17" align="left" style="height: 25px">
                To :</td>
            <td width="52" align="left" style="height: 25px">
                <input id="txtTo" runat="server" type="text" readonly="readOnly" /></td>
            <td align="left" style="height: 25px; width: 464px;">
                <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />&nbsp;
            </td>
        </tr>
        
        <tr>
            <td align="center" colspan="7" style="height: 22px">
             &nbsp;&nbsp;
             <asp:Button ID="ExportToExcel" runat="server" OnClick="ExportToExcel_Click" Text="Export To Excel" />
             <strong>Pivot Report</strong>&nbsp;<asp:DropDownList ID="ddlPivot" runat="server"
              Font-Names="Verdana" Font-Size="X-Small">
              <asp:ListItem Text="Pivot 1" Value="Pivot 1"></asp:ListItem>
              <asp:ListItem Text="Pivot 2" Value="Pivot 2"></asp:ListItem>
             </asp:DropDownList>
             <strong>Export In </strong>&nbsp;<asp:DropDownList ID="ExportOption" runat="server"
              Font-Names="Verdana" Font-Size="X-Small">
              <asp:ListItem Text="MS Office" Value="MS Office"></asp:ListItem>
              <asp:ListItem Text="Open Office" Value="Open Office"></asp:ListItem>
             </asp:DropDownList>
             &nbsp;<asp:DropDownList ID="ddlPickupRemoveYN" runat="server"
              Font-Names="Verdana" Font-Size="X-Small">
              <asp:ListItem Text="Retain To Date Pickup" Value="Retain"></asp:ListItem>
              <asp:ListItem Text="Remove To Date Pickup" Value="Remove"></asp:ListItem>
              </asp:DropDownList>
             </td>
        </tr>
        
        <%--<tr>
            <td align="center" colspan="7" style="height: 22px">
            <strong>Please make sure to select next month in the To Date</strong>
            </td>
        </tr>--%>
    </tbody>
</table>
<div>
    <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
    top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
    scrolling="no" height="182"></iframe>
</div>
</asp:Content>

