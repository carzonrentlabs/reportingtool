﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UploadParking
/// </summary>
namespace ReportingTool
{
    public class UploadParking
    {
        public UploadParking()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetTollParkDetails(int BookingID)
        {
            SqlParameter[] myParams = new SqlParameter[1];
            myParams[0] = new SqlParameter("@BookingID", SqlDbType.Int);
            myParams[0].Value = BookingID;
            //return ImplantDAL.GetDataSet("dbo.SP_GetTollParkDetails", myParams);
            return SqlHelper.ExecuteDataset("dbo.SP_GetTollParkDetails", myParams);
        }

        public int SaveTollParking(string BookingID, string ReceiptType, double ReceiptAmount, string ReceiptFileName, string CreatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[5];
            ObjparamUser[0] = new SqlParameter("@BookingID", BookingID);
            ObjparamUser[1] = new SqlParameter("@ReceiptType", ReceiptType);
            ObjparamUser[2] = new SqlParameter("@ReceiptAmount", ReceiptAmount);
            ObjparamUser[3] = new SqlParameter("@ReceiptFileName", ReceiptFileName);
            ObjparamUser[4] = new SqlParameter("@CreatedBy", CreatedBy);
            int status = 0;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.SaveTollParkingDocs", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }
    }
}