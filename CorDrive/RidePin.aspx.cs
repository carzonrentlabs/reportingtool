﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;


public partial class CorDrive_RidePin : System.Web.UI.Page
{
    private CorDriveDAL objCorDriveDL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessate.Visible = false;

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCorDriveDL = new CorDriveDAL();
        DataSet ds = new DataSet();
        try
        {
            int number;
            int status = 0;
            bool result = Int32.TryParse(txtBookingId.Text.ToString(), out number);
            if (result == false)
            {
                lblMessate.Visible = true;
                lblMessate.Text = "Booking Id should only numeric value";
                lblMessate.ForeColor = Color.Red;
            }
            else
            {
				status = objCorDriveDL.UpdateManualSendRidePin(Convert.ToInt32(Session["UserID"]), Convert.ToInt32(txtBookingId.Text));
				
                ds = objCorDriveDL.GetRidePin(Convert.ToInt32(txtBookingId.Text.ToString()));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grvRidePin.DataSource = ds.Tables[0];
                    grvRidePin.DataBind();
                    
                }
                else
                {
                    grvRidePin.DataSource = null;
                    grvRidePin.DataBind();
                    lblMessate.Visible = true;
                    lblMessate.Text = "Booking id does not exists";
                    lblMessate.ForeColor = Color.Red;
                }
            }


        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.Text = Ex.Message;
            lblMessate.ForeColor = Color.Red;
        }

    }

    protected void grvRidePin_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        /*
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblRideNo = (Label)e.Row.FindControl("lblRideNo");
            Button rideButton = (Button)e.Row.FindControl("bntSendRide");
            if (!string.IsNullOrEmpty(lblRideNo.Text.ToString()))
            {
                rideButton.Visible = false;
            }
            else
            {
                rideButton.Visible = true;
            }
        }*/
    }
    protected void grvRidePin_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objCorDriveDL = new CorDriveDAL();
        int status = 0;
        try
        {
            if (e.CommandName == "rideSend")
            {
                int bookingId = Convert.ToInt32(e.CommandArgument);
                status = objCorDriveDL.UpdateManualSendRidePin(Convert.ToInt32(Session["UserID"]), Convert.ToInt32(txtBookingId.Text));
                if (status >= 1)
                {
                    lblMessate.Visible = true;
                    lblMessate.Text = "Send by ride pin updated successfully";
                    lblMessate.ForeColor = Color.Red;
                }
                else
                {
                    lblMessate.Visible = true;
                    lblMessate.Text = "Getting Error to update";
                    lblMessate.ForeColor = Color.Red;
                }
            }

        }
        catch (Exception Ex)
        {

            lblMessate.Visible = true;
            lblMessate.Text = Ex.Message;
            lblMessate.ForeColor = Color.Red;
        }



    }
}