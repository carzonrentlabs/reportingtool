﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportingTool;
using System.Data;
using System.Data.SqlClient;

public partial class FuelManagement_FuelManagement : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    //CorDrive objAdmin = new CorDrive();
    private clsFuelManagement clsObjFuel;
    private ObjFuelManagement objFuel;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindUnit();
        }

    }

    protected void ddlCityName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtRegNo = new DataTable();
            objFuel = new ObjFuelManagement();
            clsObjFuel = new clsFuelManagement();
            if (ddlCityName.SelectedValue != "0")
            {
                objFuel.CityId = Convert.ToInt32(ddlCityName.SelectedValue);
                dtRegNo = clsObjFuel.GetCarRegistration(objFuel.CityId);
                if (dtRegNo.Rows.Count > 0)
                {
                    ddlRegNo.DataSource = dtRegNo;
                    ddlRegNo.DataTextField = "RegnNo";
                    ddlRegNo.DataValueField = "CarID";
                    ddlRegNo.DataBind();
                    txtFuelType.Text = string.Empty;
                    hdFuelType.Value = string.Empty;
                    txtRate.Text = string.Empty;
                    txtTotalAmount.Text = string.Empty;
                    txtQutliters.Text = string.Empty;
                    txtOdometerreading.Text = string.Empty;
                    
                }
                else
                {
                    ddlRegNo.Items.Clear();
                }

            }

        }
        catch (Exception)
        {

            throw;
        }


    }

    protected void ddlRegNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        objFuel = new ObjFuelManagement();
        clsObjFuel = new clsFuelManagement();
        DataTable dtFuelType = new DataTable();
        DataTable dtFuelRate = new DataTable();
        try
        {
            if (ddlCityName.SelectedIndex > 0 && ddlRegNo.SelectedIndex > 0)
            {
                dtFuelType = clsObjFuel.GetCarFuelType(Convert.ToInt32(ddlRegNo.SelectedValue.ToString()));
                if (dtFuelType.Rows.Count > 0)
                {
                    txtFuelType.Text = dtFuelType.Rows[0]["FuelTypeName"].ToString();
                    hdFuelType.Value = dtFuelType.Rows[0]["FuelTypeID"].ToString();
                    if (ddlCityName.SelectedIndex > 0 && hdFuelType.Value != "")
                    {
                        dtFuelType = clsObjFuel.GetFuelRate(Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToInt32(hdFuelType.Value));
                        if (dtFuelType.Rows.Count > 0)
                        {
                            txtRate.Text = dtFuelType.Rows[0]["CityFuelRate"].ToString();
                        }
                        else
                        {
                            txtRate.Text = string.Empty;
                        }

                    }

                }
                else
                {
                    txtFuelType.Text = string.Empty;
                    hdFuelType.Value = string.Empty;
                    txtRate.Text = string.Empty;
                    txtTotalAmount.Text = string.Empty;
                    txtQutliters.Text = string.Empty;
                    txtOdometerreading.Text = string.Empty;
                }

            }


        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }

    }
    protected void txtQutliters_TextChanged(object sender, EventArgs e)
    {
        double totalAmount;
        try
        {
            if (ddlCityName.SelectedIndex > 0 && ddlRegNo.SelectedIndex > 0 && txtFuelType.Text.ToString() != "" && txtRate.Text.ToString() != "" && txtQutliters.Text.ToString() != "")
            {
                totalAmount = Convert.ToInt32(txtQutliters.Text.ToString()) * Convert.ToDouble(txtRate.Text.ToString());
                txtTotalAmount.Text = totalAmount.ToString();
            }
            else
            {
                txtTotalAmount.Text = string.Empty;
            }


        }
        catch (Exception Ex)
        {

            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }

    }

    protected void bntSubmit_Click(object sender, EventArgs e)
    {
        objFuel = new ObjFuelManagement();
        clsObjFuel = new clsFuelManagement();
        try
        {

            if (ddlCityName.SelectedIndex > 0 && ddlRegNo.SelectedIndex > 0 && txtFuelType.Text.ToString() != "" && txtRate.Text.ToString() != "" && txtQutliters.Text.ToString() != "")
            {
                objFuel.CarId = Convert.ToInt32(ddlRegNo.SelectedValue);
                objFuel.FuelTypeId = Convert.ToInt32(hdFuelType.Value);
                objFuel.FuelRate = Convert.ToDouble(txtRate.Text);
                objFuel.Quantity = Convert.ToInt32(txtQutliters.Text);
                objFuel.TotalAmount = Convert.ToDouble(txtTotalAmount.Text);
                objFuel.OdoMeter = Convert.ToInt32(txtOdometerreading.Text);
                objFuel.SysUserId = Convert.ToInt32(Session["UserID"]);
                objFuel.Remarks = Convert.ToString(txtRemarks.Text);
                objFuel.RegnNo = ddlRegNo.SelectedItem.ToString();
                objFuel.CityName = ddlCityName.SelectedItem.ToString();
                int invoiceId = clsObjFuel.InsertFuelManagement(objFuel);
                if (invoiceId > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Your invoice id " + invoiceId;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    hdVoucherId.Value = invoiceId.ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Script", "javascript:PrintVoucher();", true);
                    InitializeControls(upPanel);
                }
                else
                {
                    lblMessage.Visible = true;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }

    }


    #region Helper Methods
    private void BindUnit()
    {
        try
        {
            DataTable dtUnit = new DataTable();
            DataSet dsLocation = new DataSet();
            dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
            //dsLocation = objAdmin.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            ddlCityName.DataSource = dsLocation;
            ddlCityName.DataTextField = "CityName";
            ddlCityName.DataValueField = "CityID";
            //ddlCityName.DataTextField = "UnitName";
            //ddlCityName.DataValueField = "UnitID";
            ddlCityName.DataBind();

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }
    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }
            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }
        }
    }

    #endregion



}