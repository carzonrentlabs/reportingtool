﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;

public partial class CorDriveReport_CurrentLocationofVehicles : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!IsPostBack)
        {
            BindCityDropDownlist();
            
        }
    }
    #region Helper Function
    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    } 
    #endregion
    protected void bntGet_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        ifViewMap.Visible = false;
        if (ddlCity.SelectedIndex > 0)
        {

            string url = "http://insta.carzonrent.com/CorMeterGoogleAPI/MapWithCity.aspx?cityId=" + Convert.ToString(ddlCity.SelectedValue);
            ifViewMap.Visible = true;
            ifViewMap.Attributes.Add("src", url);

        }
        else
        { 
            ifViewMap.Visible = false;
            lblMessage.Visible = true;
            lblMessage.Text = "Record not available.";
            lblMessage.ForeColor = Color.Red;
        }


    }
}