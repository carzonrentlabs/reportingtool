﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ModiFyCarGroupDetails.aspx.cs" Inherits="ModiFyCarGroupDetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <%--<script type="text/javascript">
    $("#btnSave").click(function () {
        if (document.getElementById('<%=ddlGroup.ClientID%>').value == "" || document.getElementById('<%=ddlGroup.ClientID%>').value == "UNSELECTED") {
            alert("Please Select Group");
            return false;
        }
        if (document.getElementById('<%=ddlValidTill.ClientID%>').value == "" || document.getElementById('<%=ddlValidTill.ClientID%>').value == "UNSELECTED") {
            alert("Please Select Valid Till");
            return false;
        }
        if (document.getElementById('<%=txtRemarks.ClientID%>').text == "") {
            alert("Please enter remarks.");
            return false;
        }     
    });
    $("#bntGet").click(function () {

        if (document.getElementById('<%=txtRegNo.ClientID%>').text == "") {
            alert("Please Enter Vehicle Regn. No.");
            return false;
        }
    });
</script>--%>
    <table cellpadding="0" cellspacing="0" border="0" style="width: 610px" align="center">
        <tr>
            <td colspan="2" align="center">
                <b>Modify Car Group</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                Vehicle Regn. No.:
                </td>
            <td>
                <asp:TextBox ID="txtRegNo" Width="200px" runat="server"></asp:TextBox>
                <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
            </td>
        </tr>
        <tr runat="server" id="td_details" visible="false">
            <td  align="left">
                Car Group :
                <asp:Label ID="lblGroup" runat="server"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;
               
                </td>
            <td>
             Rating Points :
                <asp:Label ID="lblRatingPoints" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="tbl_ModifyCar" visible="false" runat="server" cellpadding="0" cellspacing="0"
        border="0" style="width: 610px" align="center">
        <tr>
            <td >
            </td>
            <td style="height: 20px; white-space: nowrap;">
            </td>
        </tr>
        <tr>
            <td >
            </td>
            <td style="height: 20px; white-space: nowrap;">
            </td>
        </tr>
        <tr>
            <td style="height: 20px; white-space: nowrap;">
                <span style="color: red">*</span>Group:
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlGroup" Width="100px" runat="server">
                    <asp:ListItem Value="UNSELECTED" Selected>Select</asp:ListItem>                 
                    <asp:ListItem Value="Group B">Group B</asp:ListItem>
                    <asp:ListItem Value="Group C">Group C</asp:ListItem>
                  
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="height: 20px; white-space: nowrap;">
                <span style="color: red">*</span> Valid Till
            </td>
            <td>
                <asp:DropDownList ID="ddlValidTill" Width="100px" runat="server">
                    <asp:ListItem Value="UNSELECTED" Selected>Select</asp:ListItem>
                    <asp:ListItem Value="7">Next 7 days</asp:ListItem>
                    <asp:ListItem Value="14">Next 14 days</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <span style="color: red">*</span> Comments:
            </td>
            <td style="height: 20px; white-space: nowrap;">
                <asp:TextBox ID="txtRemarks" runat="server" Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="height: 20px; white-space: nowrap;">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
