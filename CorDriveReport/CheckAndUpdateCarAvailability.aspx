<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CheckAndUpdateCarAvailability.aspx.cs" Inherits="CorDriveReport_CheckAndUpdateCarAvailability_" Title="Driver Calling" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script src="http://code.jquery.com/jquery-1.8.2.js" type="text/javascript"></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/moment.js" type="text/javascript"></script>
   <script src="../JQuery/ui.core.js" type="text/javascript"></script>
   <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

 <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
<script src="Jquery/jquery.tooltip.min.js" type="text/javascript"></script>
 <link href="../CSSReveal/reveal.css" rel="stylesheet" type="text/css" />
    
<%-- <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/moment.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>--%>
  <script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script>
  <script language="javascript" type="text/javascript">
        function pageLoad(sender, args) {

            $("#<%=txtPickUpDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=ddlCity.ClientID%>').value == "0") {
                    alert("Please Select the City");
                    return false;
                }
                if (document.getElementById('<%=txtPickUpDate.ClientID%>').value == "") {
                    alert("Please Enter PickUp Date");
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            function ShowProgress() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
            function ShowProgressKill() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.removeClass("modal");
                    $('body').remove(modal);
                    loading.hide();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }

            $('a[data-reveal-id]').live('click', function (e) {
                e.preventDefault();
                var id = $(this).attr("id")
             var DutySlip= $(this).attr("PendingDutySlip")
             var Complaints=$(this).attr("NoOfComplaints")
             $("#<%=hid_PendingDutySlip.ClientID%>").val(DutySlip);
             $("#<%=hid_TotalComplaints.ClientID%>").val(Complaints);
                var CarId = $("#" + id).text()
                $("#<%=txtCarId.ClientID%>").val(CarId);
                    $.ajax({
                    type: "GET",
                    cache: false,
                    url: "CarAvailability.ashx",
                    data: { cabId: CarId},
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (response) {
                     
//                           $("#<%=lblToday.ClientID%>").val(SelectedDate);
//                           $("#<%=lblTomorrow.ClientID%>").val(new Date(SelectedDate).add(1,'days').format('L'))
                        if (response != "Record Not Found") {
                            var ReturnString = response.split("#");
                            $("#<%=ddlAvailabilityToday.ClientID%>").val(ReturnString[1]);
                            $("#<%=hid_FOldDutyDate.ClientID%>").val(ReturnString[2]);
                            $("#<%=ddlFromToday.ClientID%>").val(ReturnString[3]);
                            $("#<%=ddlToToday.ClientID%>").val(ReturnString[4]);
                            $("#<%=txtTodayRemarks.ClientID%>").val(ReturnString[5]);
                            $("#<%=txtCreatedBy.ClientID%>").val(ReturnString[6]);
                            $("#<%=txtModifyBy.ClientID%>").val(ReturnString[7]);
                            $("#<%=hid_FAutoId.ClientID%>").val(ReturnString[8]);
                            
                            
                            $("#<%=ddlAvailabilityTomorrow.ClientID%>").val(ReturnString[9]);
                            $("#<%=hid_SOldDutyDate.ClientID%>").val(ReturnString[10]);
                            $("#<%=ddlFromTomorrow.ClientID%>").val(ReturnString[11]);
                            $("#<%=ddlToTomorrow.ClientID%>").val(ReturnString[12]);
                            $("#<%=txtTomorrowRemarks.ClientID%>").val(ReturnString[13]);
                            $("#<%=hid_SAutoId.ClientID%>").val(ReturnString[14]);

//                            var SelectedText = $("#ddlAvailabilityToday option:selected").val();
//                            if(SelectedText=='Available Partly')
                            if(ReturnString[1]=="Available Partly")
                            {
                                 $('#divFrom1').css('display','block');
                                 $('#divTo1').css('display','block');
                                 $('#divddlFromToday').css('display','block');
                                 $('#divddlToToday').css('display','block');
                            }
                            else
                            {
                                    
                                  $('#divFrom1').css('display','none');
                                 $('#divTo1').css('display','none');
                                 $('#divddlFromToday').css('display','none');
                                 $('#divddlToToday').css('display','none');
                            }
                           if(ReturnString[9]=="Available Partly")
                               {
                                    $('#divFrom2').css('display','block');
                                    $('#divTo2').css('display','block');
                                    $('#divddlFromTomorrow').css('display','block');
                                    $('#divddlToTomorrow').css('display','block');
                                }  
                            else
                            {
                                   
                                $('#divFrom2').css('display','none');
                                $('#divTo2').css('display','none');
                                $('#divddlFromTomorrow').css('display','none');
                                $('#divddlToTomorrow').css('display','none');
                            }             
                            
                        }
                        else {
                                 $("#<%=hid_FAutoId.ClientID%>").val("");
                                 $("#<%=hid_FOldDutyDate.ClientID%>").val("");
                                 $("#<%=hid_SAutoId.ClientID%>").val("");
                                 $("#<%=hid_SOldDutyDate.ClientID%>").val("");
                                                 
                                 $("#<%=txtCreatedBy.ClientID%>").val("");
                                 $("#<%=txtModifyBy.ClientID%>").val("");
                            
                                 $("#<%=ddlAvailabilityToday.ClientID%>").val("Not Available");
                                 $("#<%=txtTodayRemarks.ClientID%>").val("")
                                 
                                 $("#<%=ddlAvailabilityTomorrow.ClientID%>").val("Not Available");
                                 $("#<%=txtTomorrowRemarks.ClientID%>").val("")
                                 
                                 $("#<%=ddlFromToday.ClientID%>").val("0");
                                 $("#<%=ddlToToday.ClientID%>").val("0");
                                    
                                 $("#<%=ddlFromTomorrow.ClientID%>").val("0");
                                 $("#<%=ddlToTomorrow.ClientID%>").val("0");
                                 
                                 $('#divFrom1').css('display','none');
                                 $('#divTo1').css('display','none');
                                 $('#divddlFromToday').css('display','none');
                                 $('#divddlToToday').css('display','none');
                                 
                                $('#divFrom2').css('display','none');
                                $('#divTo2').css('display','none');
                                $('#divddlFromTomorrow').css('display','none');
                                $('#divddlToTomorrow').css('display','none');
                         
                        }
                    },
                    error: function (msg) {
                        alert(msg.responseText);
                    }
                });
                var modalLocation = $(this).attr('data-reveal-id');
                $('#' + modalLocation).reveal($(this).data());
            });
             $("#bntCancel").click(function () {
                $(".close-reveal-modal").trigger("click");
                 $("#<%=txtCreatedBy.ClientID%>").val("");
                 $("#<%=txtModifyBy.ClientID%>").val("");
                            
                 $("#<%=ddlAvailabilityToday.ClientID%>").val("Not Available");
                 $("#<%=txtTodayRemarks.ClientID%>").val("")
                 
                 $("#<%=ddlAvailabilityTomorrow.ClientID%>").val("Not Available");
                 $("#<%=txtTomorrowRemarks.ClientID%>").val("")
                 
                 $("#<%=ddlFromToday.ClientID%>").val("0");
                 $("#<%=ddlToToday.ClientID%>").val("0");
                    
                 $("#<%=ddlFromTomorrow.ClientID%>").val("0");
                 $("#<%=ddlToTomorrow.ClientID%>").val("0");
              
            });
            
            $('#<%=ddlAvailabilityToday.ClientID %>').change(function () {
                    if(this.value == 'Available Partly') {
                        $('#divFrom1').css('display','block');
                        $('#divTo1').css('display','block');
                        $('#divddlFromToday').css('display','block');
                        $('#divddlToToday').css('display','block');
                      
                    }
                else {
                        $('#divFrom1').css('display','none');
                        $('#divTo1').css('display','none'); 
                        $('#divddlFromToday').css('display','none');
                        $('#divddlToToday').css('display','none');
                       
                   
                }
                });
                
                   $('#<%=ddlAvailabilityTomorrow.ClientID %>').change(function () {
                    if(this.value == 'Available Partly') {
                        
                        $('#divFrom2').css('display','block');
                        $('#divTo2').css('display','block');
                        $('#divddlFromTomorrow').css('display','block');
                        $('#divddlToTomorrow').css('display','block');
                      
                    }
                else {
                       
                        $('#divFrom2').css('display','none');
                        $('#divTo2').css('display','none');
                        $('#divddlFromTomorrow').css('display','none');
                        $('#divddlToTomorrow').css('display','none');
                   
                }
                });

        }
    </script>
   <script type="text/javascript">
        function InitializeToolTip() {
            $(".gridViewToolTip").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {
                    return $($(this).next().html());
                },
                showURL: false
            });
        }
    </script>
<script type="text/javascript">
        $(function () {
            InitializeToolTip();
        })
    </script>
<style type="text/css">
        #tooltip    {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #C2E0FF;
            padding: 5px;
            opacity: 0.85;    }
        #tooltip h3, #tooltip div
        {  margin: 0;  }
    </style>


    <center>
        <asp:HiddenField ID="hid_PendingDutySlip" runat="server" />
         <asp:HiddenField ID="hid_TotalComplaints" runat="server" />
         <asp:HiddenField ID="hid_FAutoId" runat="server" />
         <asp:HiddenField ID="hid_SAutoId" runat="server" />
         <asp:HiddenField ID="hid_FOldDutyDate" runat="server" />
         <asp:HiddenField ID="hid_SOldDutyDate" runat="server" />
        <table cellpadding="0" cellspacing="0" border="1" style="width: 100%">
             <tr>
                <td colspan="3" align="center" bgcolor="#FF6600">
                   <b>Driver Calling</b> 
                 </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="False" Font-Bold="True" ForeColor="Red"></asp:Label>
                     <br />
                </td>
            </tr>
             <tr>
             <td  colspan="3" align="center">
            
                <table cellpadding="0" cellspacing="0" border="1" style="width: 80%">
                     <tr>
                <td style="white-space: nowrap; height: 18px;" align="center" colspan="1">
                    <b>Choose City </b>
                 </td>
                 <td style="white-space: nowrap; height: 18px;" align="center"  colspan="1">
                    <b>Choose Date </b>
                 </td>
                  <td style="white-space: nowrap; height: 18px;" align="center"  colspan="1">
                    <b>Purpose Of Call</b>
                 </td>
                   <td style="white-space: nowrap; height: 18px;" align="center"  colspan="1">
                    <b>All/Free Cab</b>
                 </td>
                
             </tr>
            
             <tr>
                <td style="white-space: nowrap;" align="center" colspan="1">
                  <asp:DropDownList ID="ddlCity" runat="server" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" AutoPostBack="True"/>
                </td>
                <td style="white-space: nowrap;" align="center" colspan="1">
                   <asp:TextBox ID="txtPickUpDate" runat="server"></asp:TextBox>
                 </td>
                <td style="white-space: nowrap;" align="center" colspan="1">
                  <asp:DropDownList ID="ddlPurposeOfCall" runat="server">
                  <asp:ListItem Value="0">All</asp:ListItem>
                      <asp:ListItem Value="Duty allocation">Duty allocation</asp:ListItem>
                      <asp:ListItem Value="Duty Dispatch">Duty Dispatch</asp:ListItem>
                      <asp:ListItem Value="Duty follow up">Duty follow up</asp:ListItem>
                      <asp:ListItem Value="Tracking of assigned duty">Tracking of assigned duty</asp:ListItem>
                      <asp:ListItem Value="New updates">New updates</asp:ListItem>
                      <asp:ListItem Value="Important updates">Important updates</asp:ListItem>
                      <asp:ListItem Value="Cabs availability">Cabs availability</asp:ListItem>
                      <asp:ListItem Value="For Complaint(By Customer)">For Complaint(By Customer)</asp:ListItem>
                      <asp:ListItem Value="Duty Confirmation Call">Duty Confirmation Call</asp:ListItem>
                      <asp:ListItem Value="For complaint (by Chauffeur)">For complaint (by Chauffeur)</asp:ListItem>
                      <asp:ListItem Value="Emergency Help(Accidental&Legal Matter)">Emergency Help(Accidental&Legal Matter)</asp:ListItem>
                  </asp:DropDownList>
                </td>
                 <td style="white-space: nowrap;" align="center" colspan="1">
                  <asp:DropDownList ID="ddlCarStatus" runat="server">
                  <asp:ListItem Text="All" Value="0">All</asp:ListItem>
                  <asp:ListItem Text="Free" Value="1">Free</asp:ListItem>
                   </asp:DropDownList>
                </td>
             </tr>
             
             <tr>
            
                <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Choose Vendor </b>
                 </td>
                 <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Car Category</b>
                 </td>
                  <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>CorDrive Status </b>
                 </td>
                <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                   
                 </td>
                <%-- <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                   
                 </td>--%>
             </tr>
              <tr>
                <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlVendor" runat="server">
                     <asp:ListItem>--Select Vendor--</asp:ListItem>
                  </asp:DropDownList>
                </td>
                <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlCarCat" runat="server">
                     <asp:ListItem>--Select Car Category--</asp:ListItem>
                  </asp:DropDownList>
                 </td>
               <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlCorDriveStatus" runat="server">
                      <asp:ListItem>--Select CorDrive Status--</asp:ListItem>
                  </asp:DropDownList>
                </td>
                   <td style="white-space: nowrap;" align="center">
                   <asp:Button ID="bntGet" runat="server" Text="Search" Font-Bold="True" OnClick="bntGet_Click"/>
                 </td>
              <%--  <td style="white-space: nowrap;" align="center" colspan="1">
                 
                 </td>--%>
             </tr>
             <tr>
                <td colspan="4">
                 

            <div style="width: 100%; height:100%; overflow: scroll">
                      <asp:GridView ID="gvCheckCarAvailability" runat="server" AutoGenerateColumns="false" Width="100%" OnRowEditing="gvCheckCarAvailability_RowEditing"> 
                                 <Columns> 
    								    
                                         <asp:TemplateField HeaderText="Update">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkBGetBookingId" runat="server" class="big-link" data-reveal-id="myModal" NoOfComplaints='<%#Eval("TotalNoOfComplaints")%>' PendingDutySlip='<%#Eval("NoOfPendingDutySlip")%>'
                                                Text='<%#Eval("CarId") %>' ForeColor="Red" CommandName="Edit" CommandArgument='<%#Eval("CarId")%>'></asp:LinkButton>
                                              
                                        </ItemTemplate>
                                    </asp:TemplateField>                        
                                         <asp:BoundField HeaderText="RegnNo" DataField="RegnNo" /> 
                                        <asp:BoundField HeaderText="Chauffeur Name" DataField="ChauffeurName" /> 
                                        <asp:BoundField HeaderText="CarVendor Name" DataField="CarVendorName" /> 
                                        <asp:BoundField HeaderText="Model" DataField="Model" /> 
                                        <asp:BoundField HeaderText="Availability Status" DataField="AvailabilityStatus"/> 
                                      <%--  <asp:BoundField HeaderText="Current Location" DataField="CarCurrentLocation" /> --%>
                                        <asp:TemplateField HeaderText="Current Location">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="Label1" runat="server" Text='Current Address' tooltip='<%# Eval("CarCurrentLocation") %>' ></asp:Label>--%>
                                              <%-- Text='<%# Left(Eval("Description"),20) %>'--%>
                                             <a href="#" class="gridViewToolTip" style="text-decoration: none">Details
                                                 </a>
                                                <div id="tooltip" style="display: none;">
                                                    <table>
                                                        <tr> <td style="white-space: nowrap;"><b>Name:</b>&nbsp;</td>
                                                            <td><%# Eval("CarCurrentLocation")%></td></tr>
                                                        
                                                    </table>
                                                </div>

                                        </ItemTemplate>
                                         </asp:TemplateField>
                                        <asp:BoundField HeaderText="CarLocationNameUpdateDate" DataField="CarLocationNameUpdateDate" /> 
                                        <asp:BoundField HeaderText="Home Location" DataField="HomeLocation" /> 
                                        <asp:BoundField HeaderText="Duties ForDay" DataField="DutiesForDay" /> 
                                        <asp:BoundField HeaderText="Duties For Tomorrow" DataField="DutiesForTomorrow" /> 
                                         
                                        <asp:BoundField HeaderText="Open Complaints" DataField="TotalNoOfComplaints" /> 
                                        <asp:BoundField HeaderText="Pending DutySlip" DataField="NoOfPendingDutySlip" />                                  
                                      
                                    </Columns>
                                 
                                                                   
                     </asp:GridView>
                   </div>
                </td>
            </tr>
             </table>
             </td>
             </tr>
         
        </table>
    <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>     
   <div id="myModal" class="reveal-modal">
            <fieldset style="border-color: Green">
                <legend><b>Update Duty Availbility Status</b></legend>
                <div>
                    <table>
                   
                        <tr>
                            <td align="right">
                                CabId:
                            </td>
                            <td align="left" colspan="2">
                                <input type="text" name="txtCarId" id="txtCarId" runat="server" readonly="readonly" />
                            </td>
                        </tr>
                        
                     <tr>
                            <td align="right">
                                Created By:
                            </td>
                            <td align="left" colspan="2">
                                <input type="text" name="txtCreatedBy" id="txtCreatedBy" runat="server" readonly="readonly" />
                            </td>
                        </tr>
                       <tr>
                            <td align="right">
                                Modify By:
                            </td>
                            <td align="left" colspan="2">
                                <input type="text" name="txtModifyBy" id="txtModifyBy" runat="server" readonly="readonly" />
                            </td>
                        </tr>
                    <tr>
                        <td align="left" style="white-space: nowrap; height: 18px;" bgcolor="#FF6600">
                                <b>Today:</b>
                            </td>
                            <td  colspan="2" style="white-space: nowrap; height: 18px;" bgcolor="#FF6600">
                               <%-- <asp:Label ID="lblToday" runat="server"  Font-Bold="True"></asp:Label>--%>
                                <input type="text" name="lblToday" id="lblToday" runat="server" readonly="readonly" style="font-weight:bold" />
                            </td>
                        </tr>
                        <tr>
                         <td align="right">
                                Availability Status:
                            </td>
                            <td align="left">
                            <div id="divFrom1" style="display: none; position: relative;">
                                From:
                                </div>
                            </td>
                           <%-- <td align="left">
                             <div id="divTo1" style="display: none; position: relative;">
                                To:
                                </div>
                            </td>--%>
                                    
                            
                        </tr>
                        <tr>
                        <td>
                                <asp:DropDownList ID="ddlAvailabilityToday" runat="server" >
                                    <asp:ListItem Text="Not Available Full Day" Value="Not Available Full Day"></asp:ListItem>
                                    <asp:ListItem Text="Available Full Day" Value="Available Full Day"></asp:ListItem>
                                    <asp:ListItem Text="Available Partly" Value="Available Partly"></asp:ListItem>
                                   
                                </asp:DropDownList>
                            
                        </td>
                      
                        <td>
                       
                          <div id="divddlFromToday" style="display: none; position: relative;">
                        <asp:DropDownList 
                                ID="ddlFromToday" runat="server" 
                                CssClass="textfield" Width="80px">
                            </asp:DropDownList> 
                            </div>
                        </td>
                        <td>
                         <div id="divddlToToday" style="display: none; position: relative;">
                         <asp:DropDownList 
                                ID="ddlToToday" runat="server" 
                                Width="80px" TabIndex="8" >
                            </asp:DropDownList> 
                            </div>
                        </td>
                        
                        </tr>
                       <tr>
                         <td align="center" style="height: 23px">
                                Remarks:
                            </td>
                            <td align="left" colspan="2" style="height: 23px">
                               
                               <asp:TextBox ID="txtTodayRemarks" runat="server" Height="50px" TextMode="MultiLine" Width="200px" />
                              <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Remarks" ControlToValidate="txtTodayRemarks"></asp:RequiredFieldValidator>--%>
                            </td>
                       </tr>
                       
                       <tr>
                        <td align="left" style="white-space: nowrap; height: 10px;" bgcolor="#FF6600" >
                               <b>Tomorrow:</b> 
                            </td>
                            <td  colspan="2" style="white-space: nowrap; height: 18px;" bgcolor="#FF6600">
                                
                                  <input type="text" name="lblTomorrow" id="lblTomorrow" runat="server" readonly="readonly" style="font-weight:bold" />
                            </td>
                        </tr>
                        <tr>
                         <td align="right">
                                Availability Status:
                            </td>
                            <td align="left">
                             <div id="divFrom2" style="display: none; position: relative;">
                                From:
                                </div>
                            </td>
                            <td align="left">
                             <div id="divTo2" style="display: none; position: relative;">
                                To:
                                </div>
                            </td>
                                    
                            
                        </tr>
                        <tr>
                        <td>
                                <asp:DropDownList ID="ddlAvailabilityTomorrow" runat="server" ValidationGroup="FS">
                                    <asp:ListItem Text="Not Available Full Day" Value="Not Available Full Day"></asp:ListItem>
                                    <asp:ListItem Text="Available Full Day" Value="Available Full Day"></asp:ListItem>
                                    <asp:ListItem Text="Available Partly" Value="Available Partly"></asp:ListItem>
                                    
                                </asp:DropDownList>
                            
                        </td>
                        <td style="width: 142px">
                        <div id="divddlFromTomorrow" style="display: none; position: relative;">
                         <asp:DropDownList 
                                ID="ddlFromTomorrow" runat="server" 
                                CssClass="textfield" Width="80px">
                            </asp:DropDownList> 
                            </div>
                        </td>
                        <td>
                        <div id="divddlToTomorrow" style="display: none; position: relative;">
                        <asp:DropDownList 
                                ID="ddlToTomorrow" runat="server" 
                                CssClass="textfield" Width="80px">
                            </asp:DropDownList> 
                            </div>
                        </td>
                        </tr>
                       <tr>
                         <td align="center" style="height: 23px">
                                Remarks:
                            </td>
                            <td align="left" colspan="2" style="height: 23px">
                               
                               <asp:TextBox ID="txtTomorrowRemarks" runat="server" Height="50px" TextMode="MultiLine" Width="200px" />
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Remarks" ControlToValidate="txtTomorrowRemarks"></asp:RequiredFieldValidator>--%>
                            </td>
                       </tr>
                       
                        <tr>
                            <td align="center" colspan="2" style="height: 22px">
                                <input type="button" id="bntCancel" value="Cancel" class="button_quote" style="font-weight: bold" />
                                &nbsp; &nbsp; &nbsp;
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Font-Bold="True"/>
                            </td>
                        </tr>
                    </table>
                </div>  
                 </fieldset>
            <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>    
            </div>     
    </center>
</asp:Content>

