using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Reports
/// </summary>
/// 
namespace ReportingTool
{
    public class Reports
    {
        public Reports()
        {

        }
        public static DataTable GetCity()
        {
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "proc_getCityName_SelfDrive");
        }
        public static DataTable CollectionReport_Myles(DateTime FromDate, DateTime ToDate, int city)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@FromDate", FromDate);
            param[1] = new SqlParameter("@ToDate", ToDate);
            param[2] = new SqlParameter("@CityID", city);
            return SqlHelper.ExecuteDatatable("Prc_CollectionReport_Myles", param); //now containing only cor retail bookings
        }
        //public static DataTable SelfDriveVehicleReading()
        //{
        //    SqlParameter[] param = new SqlParameter[0];
        //    return SqlUtil.SqlHelper.ExecuteDatatable("prc_SelfDriveVehicleLastKmReading", param);
        //}
    }
}