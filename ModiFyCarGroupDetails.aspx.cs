﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ModiFyCarGroupDetails : System.Web.UI.Page
{
    clsModifyCar objclsModifyCar = new clsModifyCar(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
        }
        lblMsg.Text = "";
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtRegNo.Text.Trim() != "")
            {

                DataSet ds = new DataSet();
                ds = objclsModifyCar.GetModifycarGroupDetails(txtRegNo.Text.Trim());
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        td_details.Visible = true;
                        if ((ds.Tables[0].Rows[0]["CarGroup"].ToString() == "Group D") || (ds.Tables[0].Rows[0]["CarGroup"].ToString() == "Group C"))
                        {
                            tbl_ModifyCar.Visible = true;

                        }
                        else
                        {
                            tbl_ModifyCar.Visible = false;
                            lblMsg.Text = "Car Group cannot be modified.";

                        }
                        lblGroup.Text = ds.Tables[0].Rows[0]["CarGroup"].ToString();
                        lblRatingPoints.Text = ds.Tables[0].Rows[0]["RatingPoints"].ToString();
                    }
                    else
                    {
                        td_details.Visible = false;
                        tbl_ModifyCar.Visible = false;
                        lblMsg.Text = "Car or Rating not found";
                    }
                }

            }


            else
            {
                lblMsg.Text = "Please enter vehicle regn no.";
            }
        }
        catch (Exception EX)
        {

            lblMsg.Visible = true;
            lblMsg.Text = EX.Message;
        }


    }
    protected void btnSave_Click(object sender, EventArgs e)
    {      

        if (txtRegNo.Text.Trim()=="")
        {
            lblMsg.Text = "Please enter vehicle regn no.";
        }
      
        else if (ddlGroup.SelectedValue == "UNSELECTED")
        {
            lblMsg.Text = "Please select group.";
        }
        else if (ddlValidTill.SelectedValue == "UNSELECTED")
        {
            lblMsg.Text = "Please select Valid Till.";
        }
        else if (txtRemarks.Text.Trim() == "")
        {
            lblMsg.Text = "Please enter comments.";
        }
        else
        {
            int rslt = objclsModifyCar.ModifyCarGroup(Convert.ToString(txtRegNo.Text.Trim()), ddlGroup.SelectedItem.Text.ToString(), lblGroup.Text, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(ddlValidTill.SelectedValue), Convert.ToString(txtRemarks.Text));
            if (rslt > 0)
            {

                lblMsg.Text = "Car group modified successfully";
            }
        }
    }
}