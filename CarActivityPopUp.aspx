<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CarActivityPopUp.aspx.cs" Inherits="CarActivityPopUp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Add Remarks</title>
     <link href="DatePickerCSS/ui.all.css" rel="stylesheet" type="text/css" />
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Bksharma(brachid)
        {
        fromdate ='<%=fromdate%>'
        todate ='<%=todate%>'
       // alert(fromdate)
         window.close();
         window.opener.location.reload();
    //     location.href ="VendorCarActivityRpt.aspx?BranchId="+brachid+"&FromDate="+fromdate+"&ToDate="+todate+"&bk=1";
        
        }
               
        
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });
//        
// window.onunload = refreshParent;
//        function refreshParent() {
//            window.opener.location.reload();
//        function validate() {
//            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
//                alert("Please Select the From Date");
//                document.getElementById('<%=txtFromDate.ClientID %>').focus();
//                return false;
//            }
//            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
//                alert("Please Select the To Date");
//                document.getElementById('<%=txtToDate.ClientID %>').focus();
//                return false;
//            }
//            if (document.getElementById('<%=txtRemark.ClientID %>').value == "") {
//                alert("Please Enter Remarks");
//                document.getElementById('<%=txtRemark.ClientID %>').focus();
//                return false;
//            }
//        }
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    
    <table class="Table" style="border-right: #cccc99 thin solid; border-top: #cccc99 thin solid; border-left: #cccc99 thin solid; width: 505px; border-bottom: #cccc99 thin solid; background-color: #f7f7de" align="center">
  <tr>
        <td class="TableTD" style="width: 503px; height: 303px" >
  
 <table >
       <tr>
        <td align="center" colspan="2" class="TableTD" valign="top"><b>Add Not Available Details</b>
        <br />
        <br />
        </td>
        </tr>
                  <tr>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: right; width:40%" align="right">
                         Cab No :</td>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: left; width:42%">
                         &nbsp;<asp:Label ID="lblCabId" runat="server" />
                        </td>
                  </tr>             
                 <tr>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: right; width:40%">
                         Reason Type:</td>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: left; width:72%">
                         &nbsp;<asp:DropDownList id="ddlReasonType" runat="server" ValidationGroup="FS">
                                     <asp:ListItem>AVAILABLE for Duty</asp:ListItem>
                                     <asp:ListItem>NOT INTERESTED</asp:ListItem>
                                     <asp:ListItem>Driver not available</asp:ListItem>
                                     <asp:ListItem>On personal duty</asp:ListItem>
                                     <asp:ListItem>At Home town</asp:ListItem>
                                     <asp:ListItem>Car in workshop</asp:ListItem>
                                     <asp:ListItem>Not interested</asp:ListItem>
                                     <asp:ListItem>Payment issue</asp:ListItem>
                         </asp:DropDownList>
                               
                         </td>
                    
                 </tr> 
                  <tr>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: right; width:40%">
                         From Date :</td>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: left; width:42%">
                         &nbsp;<asp:TextBox ID="txtFromDate" runat="server" ValidationGroup="FS"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="rvFromDate" SetFocusOnError="True" ValidationGroup="FS" runat="server" ErrorMessage="Please Enter From Date" ControlToValidate="txtFromDate" Display="None"></asp:RequiredFieldValidator>
                     </td>
                    
                 </tr> 
                  <tr>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: right; width:40%">
                         To Date :</td>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: left; width:42%">
                         &nbsp;<asp:TextBox ID="txtToDate" runat="server" ValidationGroup="FS"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="rvToDate" ValidationGroup="FS" runat="server" ErrorMessage="Please Enter To Date" ControlToValidate="txtToDate" Display="None"></asp:RequiredFieldValidator>
                          
                    
                     </td>
                    
                 </tr> 
                  
                 <tr>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: right; width:40%">
                         Remarks :</td>
                     <td class="TableTD" style="font-family: Tahoma; font-size: 10pt; text-align: left; width:42%">
                         &nbsp;<asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" ValidationGroup="FS"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="rvtxtRemark" ValidationGroup="FS" runat="server" ErrorMessage="Please Enter Remarks" ControlToValidate="txtRemark" Display="None"></asp:RequiredFieldValidator>
                        </td>
                  </tr> 
                 
                 <tr>
                     <td class="TableTD" style="font-size: 10pt; width: 40%; font-family: Tahoma; text-align: center " colspan="2">
                 
                             <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="True"
                             ShowSummary="False" ValidationGroup="FS" />
                     </td>
                     
                 </tr>
                  <tr>
                     <td class="TableTD" style="font-size: 10pt; width: 40%; font-family: Tahoma; text-align: center " colspan="2">
                        <asp:Label ID="lblMsg" runat="server" Font-Size="Small" Font-Names="Verdana" Font-Bold="True" ForeColor="Red"/>
                     </td>
                     
                 </tr>
                                                           
                 <tr>
                     <td style="font-family: Tahoma; font-size: 10pt; text-align: center;" colspan="2"><asp:Button ID="btnSubmitRemarks" runat="server" Text="Submit" ValidationGroup="FS" OnClick="btnSubmitRemarks_Click"/></td>
                                     
                 </tr>        
                
            </table>
            </td>
            </tr>
  </table>
   
    </form>
</body>
</html>
