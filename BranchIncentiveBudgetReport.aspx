<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BranchIncentiveBudgetReport.aspx.cs"
    Inherits="BranchIncentiveBudgetReport" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
    function validate()
        {
            if(document.getElementById('<%=ddlFromMonth.ClientID %>').selectedIndex == 0)
            {
                alert("Select From Month");
                document.getElementById('<%=ddlFromMonth.ClientID %>').focus();
                return false;
            }
             else if (document.getElementById('<%=ddlFromYear.ClientID %>').selectedIndex == 0)
            {
                alert("Select From Year.");
                document.getElementById('<%=ddlFromYear.ClientID %>').focus();
            }
             else if (document.getElementById('<%=ddlToMonth.ClientID %>').selectedIndex == 0)
            {
                 alert("Select To Month.");
                document.getElementById('<%=ddlToMonth.ClientID %>').focus();
            
            }
             else if (document.getElementById('<%=ddlToYear.ClientID %>').selectedIndex == 0)
            {
              alert("Select To Year.");
                document.getElementById('<%=ddlToYear.ClientID %>').focus();
            }
        }
    
    </script>

    <table width="80%" border="0" align="center" id="table1">
        <tbody>
            <tr>
                <td colspan="10" align="center">
                    <strong><u>Budget</u></strong>&nbsp;</td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    From Month</td>
                <td>
                    <asp:DropDownList ID="ddlFromMonth" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    From Year</td>
                <td>
                    <asp:DropDownList ID="ddlFromYear" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    To Month</td>
                <td>
                    <asp:DropDownList ID="ddlToMonth" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    To Year</td>
                <td>
                    <asp:DropDownList ID="ddlToYear" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="bntGet" runat="server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td>
                    <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="7" style="text-align: center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
            </tr>
        </tbody>
    </table>
    <center>
        <div>
            <asp:GridView ID="grvBranchIncentiveReport" runat="server" AutoGenerateColumns="true"
                BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                ForeColor="Black" GridLines="Both">
                <FooterStyle BackColor="Tan" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" />
            </asp:GridView>
        </div>
    </center>
</asp:Content>
