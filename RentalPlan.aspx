﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RentalPlan.aspx.cs" Inherits="RentalPlan" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" type="text/javascript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=ddlMonth.ClientID %>').selectedIndex == 0) {
                alert("Select From Month");
                document.getElementById('<%=ddlMonth.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlYear.ClientID %>').selectedIndex == 0) {
                alert("Select From Year.");
                document.getElementById('<%=ddlYear.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlCityName.ClientID %>').selectedIndex == 0) {
                alert("Select city name.");
                document.getElementById('<%=ddlCityName.ClientID %>').focus();
                return false;
            }

    }

    </script>
    <center>
        <div>
            <table width="80%" border="0" align="center" id="table1">
                <tbody>
                    <tr>
                        <td colspan="2" align="center">
                            <strong><u>Rental Plan</u></strong>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td style="padding-left:150px;"></td>
                        <td>Month&nbsp;
                            <asp:DropDownList ID="ddlMonth" runat="server">
                            </asp:DropDownList>
                        &nbsp;Year&nbsp;
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        &nbsp;PM Registered City&nbsp;
                            <asp:DropDownList ID="ddlCityName" runat="server" ></asp:DropDownList>
                              &nbsp;
                            <asp:Button ID="bntGet" runat="server" Text="Get It" OnClick="bntGet_Click" />
                        &nbsp;&nbsp;
                            <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br />
        <div>
            <asp:Literal ID="ltlSummary" runat="server"></asp:Literal>
        </div>
    </center>
</asp:Content>
