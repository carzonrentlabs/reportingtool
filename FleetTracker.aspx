<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FleetTracker.aspx.cs" Inherits="FeetTracker"
    MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="feelTracker" ContentPlaceHolderID="cphPage" runat="server">

    <script type="text/javascript">
     $(document).ready(function()
     {
        $('input:text').keypress(function(e) 
        { 
         var check=$(this).attr("id")          
         if (e.which ==54)
         {
          //$("#txtRemark1").css("display","block")
          //$("#txtRemark1").css("display","block")
         // document.getElementById ("txtRemark1").style.display ="block"
         //var checkBoxSelector = '#<%=grvFleetTracker.ClientID%> input[id*="txtRemark1"]';
         check="#"+check.replace ("Attendance","Remark");
         //alert(checkBoxSelector)
         // $("#ctl00_cphPage_grvFleetTracker_ctl02_txtRemark1").css("display","block")   
          $(check).css("display","block")           
         }  
         else if (e.which >=55 && e.which <=57)
         {
           alert("Enter number only between 1 and 6")
           return false;         
         }
         else if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
         {
            alert("Only digit enter");
            return false;
         } 
         else 
         {
             check="#"+check.replace ("Attendance","Remark");
             if($(check).css("display")== "block")            
              {              
                $(check).css("display","none")         
              }
         }
        });
        $.gritter.add({
				// (string | mandatory) the heading of the notification
				title: 'Use codes!',
				// (string | mandatory) the text inside the notification
				text: '1 For on road <br> 2 For accidental <br> 3 For servicing <br> 4 For RTO work <br> 5 For driver issue <br> 6 For  others (Pls specify)',
				// (string | optional) the image to display on the left
				//image: 'http://a0.twimg.com/profile_images/59268975/jquery_avatar_bigger.png',
				// (bool | optional) if you want it to fade out on its own or just sit there
				sticky: false,
				// (int | optional) the time you want it to be alive for before fading out
				time: '40000000'
			});
     });
      function ValidationCheck()
     {
         if (document.getElementById ('<%=ddMonth.ClientID%>').selectedIndex==0) 
         {
            alert("Select month.");
            document.getElementById ('<%=ddMonth.ClientID%>').focus();
            return false;
         }
         else if (document.getElementById ('<%=ddlUnit.ClientID%>').selectedIndex==0) 
         {
            alert("Select unit.");
            document.getElementById ('<%=ddlUnit.ClientID%>').focus();
            return false;
         }     
     }
    </script>

    <div style="width: 60%; margin-left: 40%">
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="4" align="center">
                    <b>Fleet Tracker</b></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <label id="lblMonth" for="ddMonth">
                        Select Month</label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddMonth" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <label id="lblUnit" for="ddlUnit">
                        Select Unit</label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlUnit" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click"
                        OnClientClick="javascript:ValidationCheck();" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <asp:Label ID="lblConfirmMessage" runat="Server" Visible="false"></asp:Label>
    <br />
    <div id="divGrid">
        <asp:GridView ID="grvFleetTracker" runat="server" DataKeyNames="CarId,RegnNo" BackColor="LightGoldenrodYellow"
            BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
            AutoGenerateColumns="false" OnRowDataBound="grvFleetTracker_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="S.No." ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                    <ItemTemplate>
                        <asp:Label ID="lblSNo" runat="server" Text='<%#Container.DisplayIndex + 1%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vehicle Reg No.">
                    <ItemTemplate>
                        <asp:Label ID="lblRegNo" runat="server" Text='<%#Eval("RegnNo")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Car Make">
                    <ItemTemplate>
                        <asp:Label ID="lblCarMake" runat="server" Text='<%#Eval("CarCompName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Car Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCarCate" runat="server" Text='<%#Eval("CarCatName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="1">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance1" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark1" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark1" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="2">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance2" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark2" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark2" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="3">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance3" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark3" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark3" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="4">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance4" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark4" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark4" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="5">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance5" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark5" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark5" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="6">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance6" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark6" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark6" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="7">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance7" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark7" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark7" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="8">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance8" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark8" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark8" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="9">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance9" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark9" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark9" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="10">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance10" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark10" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark10" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="11">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance11" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark11" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark11" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="12">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance12" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark12" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark12" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="13">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance13" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark13" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark13" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="14">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance14" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark14" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark14" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="15">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance15" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--  <asp:TextBox ID="txtRemark15" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark15" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="16">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance16" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--  <asp:TextBox ID="txtRemark16" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark16" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="17">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance17" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark17" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark17" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="18">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance18" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark18" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark18" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="19">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance19" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark19" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark19" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="20">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance20" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark20" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark20" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="21">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance21" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark21" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark21" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="22">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance22" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark22" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark22" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="23">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance23" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%-- <asp:TextBox ID="txtRemark23" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark23" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="24">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance24" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark24" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark24" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="25">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance25" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark25" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark25" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="26">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance26" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark26" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark26" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="27">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance27" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark27" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark27" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="28">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance28" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark28" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark28" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="29">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance29" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark29" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark29" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="30">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance30" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark30" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark30" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="31">
                    <ItemTemplate>
                        <asp:TextBox ID="txtAttendance31" runat="server" MaxLength="1" Width="15" Enabled="false"></asp:TextBox>
                        <%--<asp:TextBox ID="txtRemark31" runat="server" TextMode="MultiLine" Rows="3" Columns="10"
                            Visible="false"></asp:TextBox>--%>
                        <textarea id="txtRemark31" cols="10" rows="2" runat="server" style="display: none"></textarea>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="Tan" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
    </div>
    <br />
    <div>
        <div style="text-align: center">
            <asp:Button ID="bntSaveAll" Text="Save" runat="server" ToolTip="Click to save." OnClick="bntSaveAll_Click"
                Visible="false" />
        </div>
    </div>
    <asp:Label ID="lblMessage" runat="Server" Visible="false"></asp:Label>
</asp:Content>
