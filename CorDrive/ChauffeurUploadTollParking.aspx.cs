﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ExcelUtil;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ReportingTool;

public partial class CorDrive_ChauffeurUploadTollParking : System.Web.UI.Page
{
    UploadParking uploadparking = new UploadParking();
    string msgComp = string.Empty;
    DataSet ds = new DataSet();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //if (Session["UserID"] == null)
            //{
            //    Response.Redirect("~/Login.aspx");
            //    return;
            //}

            UploadToll.Attributes.Add("onchange", "return checkFileExtension(this);");
        }
    }

    private void gridbind()
    {
        DataSet DsTollPark = new DataSet();
        DsTollPark = uploadparking.GetTollParkDetails(Convert.ToInt32(txtBookingID.Text));
        if (DsTollPark.Tables[0].Rows.Count > 0)
        {
            gvTollPark.DataSource = DsTollPark.Tables[0];
            gvTollPark.DataBind();
        }
        else
        {
            gvTollPark.DataSource = null;
            gvTollPark.DataBind();
        }
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        try
        {

            if (txtBookingID.Text == "")
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please enter BookingID !');</script>");
                txtBookingID.Focus();
                return;
            }


            gridbind();

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }
    protected void gvTollPark_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblMsg.Text = "";
        if (e.CommandName == "select")
        {
            int ownerid = Convert.ToInt32(e.CommandArgument);
            GridViewRow gvRow = (GridViewRow)(((Button)e.CommandSource).Parent).Parent;
            lblSerialNo.Text = ownerid.ToString();

            lblBookingID.Text = ((Label)gvRow.FindControl("lblBookingID")).Text;
            //lblReceiptID.Text = ((Label)gvRow.FindControl("lblReceiptID")).Text;
            //lblImg.Text = ((HiddenField)gvRow.FindControl("hdnImage")).Value;

            txtAmount.Text = "";
            ddlImageType.SelectedIndex = 0;
            lblErrMsg.Text = "";

            pnlpopup.Visible = true;
            this.mdPopUp.Enabled = true;
            this.mdPopUp.Show();


            // this.pnlpopupF.Visible = true;

        }

        //if (e.CommandName == "dfsdfsdffsdfsdf")
        //{
        //    int ownerid = Convert.ToInt32(e.CommandArgument);
        //    GridViewRow gvRow = (GridViewRow)(((Button)e.CommandSource).Parent).Parent;
        //    lblSerialNo.Text = ownerid.ToString();

        //    lblBookingID.Text = ((Label)gvRow.FindControl("lblBookingID")).Text;
        //    //lblReceiptID.Text = ((Label)gvRow.FindControl("lblReceiptID")).Text;
        //    //lblImg.Text = ((HiddenField)gvRow.FindControl("hdnImage")).Value;
        //    string hdnImage = ((HiddenField)gvRow.FindControl("hdnImage")).Value;
        //    HyperLink hprLnk = (HyperLink)gvRow.FindControl("hprLnkView");
        //    hprLnk.NavigateUrl = hdnImage;
        //    Response.Redirect(hprLnk.NavigateUrl);
        //    // this.pnlpopupF.Visible = true;
        //}
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtAmount.Text = "";
        lblSerialNo.Text = "";
        lblErrMsg.Text = "";
        pnlpopup.Visible = false;
        this.mdPopUp.Hide();
        mdPopUp.Enabled = false;
    }
    protected void ImgClose_Click(object sender, ImageClickEventArgs e)
    {
        txtAmount.Text = "";
        lblSerialNo.Text = "";
        lblErrMsg.Text = "";
        pnlpopup.Visible = false;
        this.mdPopUp.Hide();
        mdPopUp.Enabled = false;
    }

    public string FileType(string FileName, string InfoType)
    {
        if (FileName != "")
        {
            string ft = FileName;
            string[] arr1 = ft.Split('\\');
            int fname = arr1.Length;
            string newfname = arr1[fname - 1].ToString();
            string[] arr2 = ft.Split('.');
            int fleextnname = arr2.Length;

            if (InfoType == "0")
                return arr2[fleextnname - 1].ToString();
            else
                return newfname;
        }
        else
            return "* Not a valid file type";
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        //UploadToll.Attributes.Add("onchange", "return checkFileExtension(this);");
        if (UploadToll.HasFile)
        {
            //HttpPostedFile file = (HttpPostedFile)(UploadToll.PostedFile);
            //int iFileSize = file.ContentLength;
            if (UploadToll.PostedFile.ContentLength > 5000000)  // 5MB approx (actually less though)
            {
                // File is too big so do something here
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('File size should be less then 5 MB !');</script>");
                return;
            }

            int amount;
            string type = "";
            string filename = "";

            amount = Convert.ToInt32(txtAmount.Text);
            type = ddlImageType.SelectedItem.Text;
            filename = UploadToll.FileName;

            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            numIterations = rand.Next(10, 99);
            //Response.Write(numIterations.ToString());

            DataSet DsTollPark = new DataSet();
            DsTollPark = uploadparking.GetTollParkDetails(Convert.ToInt32(txtBookingID.Text));
            int BookingID;
            string ReceiptType = "";
            int amt;
            if (DsTollPark.Tables[0].Rows.Count == 0)
            {
                BookingID = Convert.ToInt32(txtBookingID.Text);
                ReceiptType = ddlImageType.SelectedItem.Text;
                amt = Convert.ToInt32(txtAmount.Text);
            }
            else
            {
                BookingID = Convert.ToInt32(DsTollPark.Tables[0].Rows[0]["BookingID"].ToString());
                ReceiptType = ddlImageType.SelectedItem.Text;
                amt = Convert.ToInt32(txtAmount.Text);
            }
            //string FileName = String.Empty;
            //string filePath = "";

            //filePath= System.IO.Directory.CreateDirectory(Server.MapPath("C:/CorInt/Rentmeter_New/receipts/")).ToString();
            //filePath= "@C:\\CorInt/Rentmeter_New\\receipts\\".ToString();

            //// Create directory if not exists.
            //System.IO.FileInfo fileInfo = new System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(filePath)); //when it gets to this line the error is caught
            //if (!fileInfo.Directory.Exists)
            //{
            //    fileInfo.Directory.Create();
            //}

            ////string Filepath = Server.MapPath("C:\\CorInt\\Rentmeter_New\\receipts" + ReceiptID + "\\" + ReceiptType.ToString() + "\\" + numIterations + "\\" + amt + "\\" + FileType(UploadToll.PostedFile.FileName, "1"));
            ////UploadToll.PostedFile.SaveAs(Filepath);

            string Msg = string.Empty, value = "";
            if (UploadToll.PostedFile.ContentLength > 0)
            {
                try
                {
                    string filePath = UploadToll.PostedFile.FileName;          // getting the file path of uploaded file
                    ViewState["URL"] = filePath;

                    string filename1 = Path.GetFileName(filePath);               // getting the file name of uploaded file
                    string ext = Path.GetExtension(filename1);                      // getting the file extension of uploaded file
                    if (ext.ToLower() == ".png" || ext.ToLower() == ".PNG")
                    {
                        //string Filepath = (Server.MapPath("~/DOC//") + BookingID + "-" + ReceiptType.ToString() + "-" + numIterations + "-" + amt + "-" + filename1);
                        //value = "~/DOC//" + BookingID + "-" + ReceiptType.ToString() + "-" + numIterations + "-" + amt + "-" + filename1;

                        string Filepath = ("C://CorInt//Rentmeter_New//receipts//") + BookingID + "_" + ReceiptType.ToString() + "_" + numIterations + "_" + amt + ".png";
                        value = "C://CorInt//Rentmeter_New//receipts//" + BookingID + "_" + ReceiptType.ToString() + "_" + numIterations + "_" + amt + ".png";
                        string ReceiptFileName = BookingID + "_" + ReceiptType.ToString() + "_" + numIterations + "_" + amt;

                        //System.IO.Directory.CreateDirectory(Server.MapPath("C://CorInt//Rentmeter_New//receipts//") + "//VID_" + ReceiptFileName.ToString());

                        UploadToll.PostedFile.SaveAs(Filepath);
                        ViewState["FileName"] = value;


                        int rslt = 0;
                        rslt = uploadparking.SaveTollParking(lblBookingID.Text.Trim(), ddlImageType.SelectedItem.Text, Convert.ToInt32(txtAmount.Text), ReceiptFileName, Convert.ToInt32(Session["UserID"]).ToString());
                        if (rslt > 0)
                        {
                            gridbind();
                            lblMessage.Text = "Submitted Successfully";
                            lblMessage.Visible = true;
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                            //ClearControl();
                        }
                        else if (rslt == -1)
                        {
                            lblMessage.Text = "Record already Present !";
                            lblMessage.Visible = true;
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            lblMessage.Text = "Not Submitted Successfully";
                            lblMessage.Visible = true;
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                        }

                        //Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('File Upload successfully !');</script>");

                    }
                    else
                    {
                        Msg = "Please select PNG file";
                        return;
                    }
                }
                catch (Exception exFleUpload)
                {
                    Msg = exFleUpload.Message;
                }
            }


        }
        else
        {
            pnlpopup.Visible = true;
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please select file !');</script>");
            return;
        }
    }

    protected void gvTollPark_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        GridViewRow gvRow = e.Row;
        if (gvRow.RowType == DataControlRowType.DataRow)
        {
            Button btnSearch = (Button)gvRow.FindControl("btnSave");
            //Image bksharma = new Image();
            string hdnDutyStatus = ((HiddenField)gvRow.FindControl("hdnDutyStatus")).Value;
            //string hdnImage = ((HiddenField)gvRow.FindControl("hdnImage")).Value;

            HyperLink hprLnk = (HyperLink)e.Row.FindControl("hprLnkView");
            //if (hprLnk.NavigateUrl != "")
            //{
            //    hprLnk.Enabled = true;
            //    hprLnk.NavigateUrl =  hdnImage;
            //    //hprLnk.NavigateUrl = Server.MapPath("~/DOC//")+hdnImage;
            //}

            //else
            //{
            //    hprLnk.Enabled = false;
            //}


            if (hdnDutyStatus == "O" || hdnDutyStatus == "P")
            {
                btnSearch.Enabled = true;
                btnSearch.ForeColor = System.Drawing.Color.Green;

                btnopenPopup.Enabled = true;
                btnopenPopup.Visible = true;
            }
            else
            {
                btnSearch.Enabled = false;
                btnSearch.BackColor = System.Drawing.Color.Red;
                btnSearch.ForeColor = System.Drawing.Color.Yellow;
                btnSearch.Text = "Duty Not Open !";

                btnopenPopup.Enabled = false;
                btnopenPopup.Visible = false;
                //btnSearch.BackColor = System.Drawing.Color.Red;
                //btnopenPopup.ForeColor = System.Drawing.Color.Red;

            }
        }



    }


    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }

            //if ((control.GetType() == typeof(Label)))
            //{
            //    ((Label)(control)).Text = "";
            //}

            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }


            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }
            lblMessage.Text = "";

        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        InitializeControls(Form);
    }
    protected void btnopenPopup_Click(object sender, EventArgs e)
    {
        if (txtBookingID.Text == "")
        {
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please Enter BookingID !');</script>");
            return;
        }

        lblBookingID.Text = txtBookingID.Text;
        txtAmount.Text = "";
        ddlImageType.SelectedIndex = 0;


        pnlpopup.Visible = true;
        this.mdPopUp.Enabled = true;
        this.mdPopUp.Show();
    }

}