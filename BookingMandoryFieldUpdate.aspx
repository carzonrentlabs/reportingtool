﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingMandoryFieldUpdate.aspx.cs"
    Inherits="BookingMandoryFieldUpdate" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js" type="text/jscript"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/jscript"></script>

    <script src="JQuery/jquery.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(function () {
             $("#<%=bntGet.ClientID%>").click(function()
             {
               if ($("#<%=txtBookingId.ClientID%>").val()=="")
               {
                    alert("Enter booking Id");
                    return false;
               }
             });
             
            $("#<%=txtBookingId.ClientID%>").keyup(function () {
            var bookingID = this.id;
            var strPass = $("#" + bookingID).val();
            var strLength = strPass.length;
            var lchar = strPass.charAt((strLength) - 1);
            var cCode = CalcKeyCode(lchar);
            if (cCode < 46 || cCode > 57 || cCode == 47) {
                var myNumber = strPass.substring(0, (strLength) - 1);
                $("#" + bookingID).val(myNumber);
                alert("Enter only numeric value.")
            }
        });
            $("#<%=bntSubmit.ClientID%>").click(function()
            {
              
               if ($("#<%=lblMisc1.ClientID%>").html()!="")
               {
                    if ($("#<%=txtMisc1.ClientID%>").css("display")=="block" && $("#<%=txtMisc1.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 1");
                        return false;
                    }
                    else if($("#<%=ddlMisc1.ClientID%>").css("display")=="block" && $("#<%=ddlMisc1.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 1");
                        return false;
                    }
                    
               }
               if ($("#<%=lblMisc2.ClientID%>").html()!="")
               {
                    if ($("#<%=txtMisc2.ClientID%>").css("display")=="block" && $("#<%=txtMisc2.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 2");
                        return false;
                    }
                    else if($("#<%=ddlMisc2.ClientID%>").css("display")=="block" && $("#<%=ddlMisc2.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 2");
                        return false;
                    }
               }
               if ($("#<%=lblMisc3.ClientID%>").html()!="")
               {
                    if ($("#<%=txtMisc3.ClientID%>").css("display")=="block" && $("#<%=txtMisc3.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 3");
                        return false;
                    }
                    else if($("#<%=ddlMisc3.ClientID%>").css("display")=="block" && $("#<%=ddlMisc3.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 3");
                        return false;
                    }
               }
               if ($("#<%=lblMisc4.ClientID%>").html()!="")
               {
                   if ($("#<%=txtMisc4.ClientID%>").css("display")=="block" && $("#<%=txtMisc4.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 4");
                        return false;
                    }
                    else if($("#<%=ddlMisc4.ClientID%>").css("display")=="block" && $("#<%=ddlMisc4.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 4");
                        return false;
                    }
               }
               if ($("#<%=lblMisc5.ClientID%>").html()!="")
               {
                   if ($("#<%=txtMisc5.ClientID%>").css("display")=="block" && $("#<%=txtMisc5.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 5");
                        return false;
                    }
                    else if($("#<%=ddlMisc5.ClientID%>").css("display")=="block" && $("#<%=ddlMisc5.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 5");
                        return false;
                    }
               }
               if ($("#<%=lblMisc6.ClientID%>").html()!="")
               {
                    if ($("#<%=txtMisc6.ClientID%>").css("display")=="block" && $("#<%=txtMisc6.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 6");
                        return false;
                    }
                    else if($("#<%=ddlMisc6.ClientID%>").css("display")=="block" && $("#<%=ddlMisc6.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 6");
                        return false;
                    }
               }
               if ($("#<%=lblMisc7.ClientID%>").html()!="" )
               {
                    if ($("#<%=txtMisc7.ClientID%>").css("display")=="block" && $("#<%=txtMisc7.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 7");
                        return false;
                    }
                    else if($("#<%=ddlMisc7.ClientID%>").css("display")=="block" && $("#<%=ddlMisc7.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 7");
                        return false;
                    }
               }
               if ($("#<%=lblMisc8.ClientID%>").html()!="" )
               {
                    if ($("#<%=txtMisc8.ClientID%>").css("display")=="block" && $("#<%=txtMisc8.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 8");
                        return false;
                    }
                    else if($("#<%=ddlMisc8.ClientID%>").css("display")=="block" && $("#<%=ddlMisc8.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 8");
                        return false;
                    }
               }
               if ($("#<%=lblMisc9.ClientID%>").html()!="" )
               {
                    if ($("#<%=txtMisc9.ClientID%>").css("display")=="block" && $("#<%=txtMisc9.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 9");
                        return false;
                    }
                    else if($("#<%=ddlMisc9.ClientID%>").css("display")=="block" && $("#<%=ddlMisc9.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 9");
                        return false;
                    }
               }
               if ($("#<%=lblMisc10.ClientID%>").html()!="")
               {
                    if ($("#<%=txtMisc10.ClientID%>").css("display")=="block" && $("#<%=txtMisc10.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 10");
                        return false;
                    }
                    else if($("#<%=ddlMisc10.ClientID%>").css("display")=="block" && $("#<%=ddlMisc10.ClientID%>").val()=="")
                    {
                        alert("enter Miscellaneous 10");
                        return false;
                    }
               }
               
                ShowProgress();
                
            });
             
             
        });
    function CalcKeyCode(aChar) {
        var character = aChar.substring(0, 1);
        var code = aChar.charCodeAt(0);
        return code;
    }
    function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
       
    </script>

 <style type="text/css">
    div.ex1 {
    float: left;
    width: 200px;
    height: 30px;
    margin: 10px;
    /*border: 3px solid #73AD21;*/
    white-space:nowrap;
    text-align: left;
    
}

div.ex2 {
    float: right;
    width: 200px;
    height: 30px;
    margin: 10px;
    /*border: 3px solid #73AD21;*/
    white-space:nowrap;
    text-align: left;

}
    </style>
    <table width="80%" border="0" align="center">
        <tr>
            <td colspan="5" align="center">
                <b>Update Mandory Flield</b>
            </td>
        </tr>
        <tr>
            <td align="center">
                <strong><u>Enter Booking Id</u></strong>&nbsp;&nbsp;
                <asp:TextBox ID="txtBookingId" runat="server" MaxLength="10"></asp:TextBox>&nbsp;&nbsp;
                <asp:Button ID="bntGet" runat="server" Text="Get It" OnClick="bntGet_Click" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <center>
        <div style="width: 40%">
            <div runat="server" id="tr1" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous1" runat="server" Text="Miscellaneous1"></asp:Label>&nbsp;--%>
                    <b>Miscellaneous1</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc1" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc1" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc1" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr2" style="display: none;">
                <div class="ex1">
                   <%-- <asp:Label ID="lblMiscellaneous2" runat="server" Text="Miscellaneous2"></asp:Label>&nbsp;--%>
                   <b>Miscellaneous2</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc2" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc2" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc2" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <div runat="server" id="tr3" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous3" runat="server" Text="Miscellaneous3"></asp:Label>--%>
                    <b>Miscellaneous3</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc3" runat="server"></asp:Label>&nbsp;
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc3" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc3" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr4" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous4" runat="server" Text="Miscellaneous4"></asp:Label>--%>
                    <b>Miscellaneous4</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc4" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc4" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc4" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr5" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous5" runat="server" Text="Miscellaneous5"></asp:Label>--%>
                    <b>Miscellaneous5</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc5" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc5" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc5" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr6" style="display: none;">
                <div class="ex1">
                   <%-- <asp:Label ID="lblMiscellaneous6" runat="server" Text="Miscellaneous6"></asp:Label>--%>
                    <b>Miscellaneous6</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc6" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc6" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc6" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr7" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous7" runat="server" Text="Miscellaneous7"></asp:Label>--%>
                     <b>Miscellaneous7</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc7" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc7" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc7" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr8" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous8" runat="server" Text="Miscellaneous8"></asp:Label>--%>
                    <b>Miscellaneous8</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc8" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc8" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc8" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr9" style="display: none;">
                <div class="ex1">
                   <%-- <asp:Label ID="lblMiscellaneous9" runat="server" Text="Miscellaneous9"></asp:Label>--%>
                   <b>Miscellaneous9</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc9" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc9" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc9" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <div runat="server" id="tr10" style="display: none;">
                <div class="ex1">
                    <%--<asp:Label ID="lblMiscellaneous10" runat="server" Text="Miscellaneous10"></asp:Label>--%>
                    <b>Miscellaneous10</b>&nbsp;&nbsp;
                    <asp:Label ID="lblMisc10" runat="server"></asp:Label>
                </div>
                <div class="ex2">
                    <asp:TextBox ID="txtMisc10" runat="server" Style="display: none;"></asp:TextBox>
                    <asp:DropDownList ID="ddlMisc10" runat="server" Style="display: none;">
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div runat="server" id="trSubmit" style="display: none;">
                <div style="text-align: center;">
                    <asp:Button ID="bntSubmit" runat="Server" Text="Update" OnClick="bntSubmit_Click" />
                </div>
            </div>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
