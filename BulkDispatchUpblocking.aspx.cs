using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;

public partial class BulkDispatchUpblocking : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblmessage.Text = "";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Unblocking UB = new Unblocking();
        try
        {
            if (txtBookingID.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Please enter bookingid.');</script>");
                txtBookingID.Focus();
                return;
            }

            string BookingID;
            BookingID = txtBookingID.Text;
            BookingID = BookingID.Replace("\r\n", ",");
            BookingID = BookingID.Replace(",,", ",");
            if (BookingID.EndsWith(","))
            {
                int LengthRequired = BookingID.LastIndexOf(",");
                if (LengthRequired >= 0)
                    BookingID = BookingID.Substring(0, LengthRequired);
            }

            UB.UnblockDispatch(BookingID, Convert.ToInt32(Session["UserID"]));

            lblmessage.Text = "Unlocking done Successfully.";
        }
        catch (Exception ex)
        {
            lblmessage.Text = ex.Message.ToString();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }
}