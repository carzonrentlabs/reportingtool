﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductivityManager.aspx.cs" Inherits="ProductivityManager" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=ddlClientName.ClientID %>').selectedIndex == 0) {
                alert("Select Client Name");
                document.getElementById('<%=ddlClientName.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlUserName.ClientID %>').selectedIndex == 0) {
                alert("Select User Name.");
                document.getElementById('<%=ddlUserName.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlCityName.ClientID %>').selectedIndex == 0) {
                alert("Select City Name.");
                document.getElementById('<%=ddlCityName.ClientID %>').focus();
                return false;
            }
}
function validateMonthData() {
    if (document.getElementById('<%=ddlMonth.ClientID %>').selectedIndex == 0) {
        alert("Select From Month");
        document.getElementById('<%=ddlMonth.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlYear.ClientID %>').selectedIndex == 0) {
        alert("Select From Year.");
        document.getElementById('<%=ddlYear.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=ddlPMClient.ClientID %>').selectedIndex == 0) {
                    alert("Select client details.");
                    document.getElementById('<%=ddlPMClient.ClientID %>').focus();
                return false;
                }
                else if (document.getElementById('<%=txtBookingTarget.ClientID %>').value=="")
                {
                    alert("Enter target booking.");
                    document.getElementById('<%=txtBookingTarget.ClientID %>').focus();
                     return false;
                }
            else if (isNaN(document.getElementById('<%=txtBookingTarget.ClientID %>').value)) {
                alert("Enter only numeric value.");
                document.getElementById('<%=txtBookingTarget.ClientID %>').focus();
                return false;
            }

}
$(function () {
    $("#<%=txtBookingTarget.ClientID%>").keydown(function (event) {
        // Allow only backspace and delete
        //var Moblength = $("#txtMobileNO").length;
        if (event.keyCode == 46 || event.keyCode == 8) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57 && event.keyCode < 96 || event.keyCode > 105) {
                event.preventDefault();
            }
        }

    });
    $("#<%=btnGetIt.ClientID%>").click(function () {
        if ($("#<%=ddlPMClient.ClientID %>")[0].selectedIndex == 0) {
            alert("Select client details.")
            $("#<%=ddlPMClient.ClientID%>").focus();
            return false;
        }
    });
});

    </script>
    <center>

        <table width="100%" border="0" align="center">
            <tbody>
                <tr>
                    <td colspan="2" align="center">
                        <strong><u>Productivity Manager Entry</u></strong>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td style="white-space:nowrap;"><b>Client Name</b>&nbsp;
                            <asp:DropDownList ID="ddlClientName" runat="server">
                            </asp:DropDownList>
                        &nbsp;<b>User Name</b>&nbsp;
                            <asp:DropDownList ID="ddlUserName" runat="server">
                            </asp:DropDownList>
                        &nbsp;<b>City Name</b>&nbsp;
                           <asp:DropDownList ID="ddlCityName" runat="server">
                           </asp:DropDownList>
                        &nbsp;&nbsp;
                            <asp:Button ID="btnSumbit" runat="server" Text="Manager Entry" OnClick="btnSumbit_Click" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" border="0" align="center">
            <tbody>
                <tr>
                    <td colspan="2" align="center">
                        <strong><u>Monthly Booking Target</u></strong>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td style="white-space:nowrap;"><b>Client Details</b>&nbsp;
                            <asp:DropDownList ID="ddlPMClient" runat="server">
                            </asp:DropDownList>
                        &nbsp;<b>Month</b>&nbsp;
                            <asp:DropDownList ID="ddlMonth" runat="server">
                            </asp:DropDownList>
                        &nbsp;<b>Year</b>&nbsp;
                           <asp:DropDownList ID="ddlYear" runat="server">
                           </asp:DropDownList>
                        &nbsp;&nbsp;<b>BookingTarget</b>&nbsp;
                    <asp:TextBox ID="txtBookingTarget" runat="server" MaxLength="3" Width="5%"></asp:TextBox>
                        &nbsp;&nbsp;
                            <asp:Button ID="btnMonthlyentry" runat="server" Text="Monthly Details Entry" OnClick="btnMonthlyentry_Click" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnGetIt" runat="server" Text="Get It" OnClick="btnGetIt_Click" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:GridView ID="grvMonthDetails" runat="server" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="Both">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField HeaderText="Client Name" DataField="PManager" />
                                <asp:BoundField HeaderText="City Name" DataField="cityName" />
                                <asp:BoundField HeaderText="Month" DataField="monthname" />
                                <asp:BoundField HeaderText="Year" DataField="Years" />
                                <asp:BoundField HeaderText="Booking Target" DataField="BookingTarget" />
                            </Columns>

                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        </asp:GridView>

                    </td>

                </tr>
            </tbody>
        </table>
    </center>
</asp:Content>
