using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ExcelUtil;
using ReportingTool;

public partial class CorDriveReport_DutySlipCollection : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
        if (!Page.IsPostBack)
        {
            getRelationShipManagers();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        try
        {
            //if (string.IsNullOrEmpty(txtBookingId.Text))
            //{
            //    ShowMessage("Please Enter Booking Id");
            //    return;
            //}
            SqlDataReader UserReader = objCordrive.ShowBookingDetailsIdWise(Convert.ToInt32(txtBookingId.Text));
            if (UserReader.HasRows == true)
            {
                while (UserReader.Read())
                {
                    lblBookingStatus.Text = UserReader["BookingStatus"].ToString();
                    lblPickUpCity.Text = UserReader["PickupcityName"].ToString();
                    lblPickUpDateTime.Text = UserReader["PickupDateTime"].ToString();

                    lblDispatchedCar.Text = UserReader["CabID"].ToString();
                    lblDispatchedChauffeur.Text = UserReader["ChauffeurName"].ToString();
                    lblCorDriveStatus.Text = UserReader["CorDriveStatus"].ToString();
                    lblClientName.Text = UserReader["ClientCoName"].ToString();
                    lblGuestName.Text = UserReader["GuestName"].ToString();
                    lblClosedBy.Text = UserReader["ClosedBy"].ToString();

                    if (UserReader["BookingStatus"].ToString() == "Closed" || UserReader["BookingStatus"].ToString() == "No Show")
                    {
                        btnSave.Enabled = false;
                        lblmessage.Text = "Cannot Save as Booking is already Closed.";
                    }
                    else
                    {
                        btnSave.Enabled = true;
                        lblmessage.Text = "";
                    }
                }
                if (UserReader.NextResult())
                {
                    if (UserReader.HasRows == true)
                    {
                        while (UserReader.Read())
                        {
                            txtParkingReceipts.Text = UserReader["ParkingReceipts"].ToString();
                            txtTollReceipts.Text = UserReader["TollReceipts"].ToString();
                            txtInterStateReceipts.Text = UserReader["InterStateReceipts"].ToString();
                            chkPaperDutySlip.Checked = Convert.ToBoolean(UserReader["PaperDutySlip"]);
                            lblCreatedBy.Text = UserReader["CreatedBy"].ToString();
                            lblCreatedDate.Text = UserReader["CreatedDate"].ToString();
                            lblModifyBy.Text = UserReader["ModifyBy"].ToString();
                            lblModifyDate.Text = UserReader["ModifyDate"].ToString();
                            //txtParkingReceipts.Enabled = false;
                            //txtTollReceipts.Enabled = false;
                            //txtInterStateReceipts.Enabled = false;
                        }
                    }
                    else
                    {
                        txtParkingReceipts.Text = string.Empty;
                        txtTollReceipts.Text = string.Empty;
                        txtInterStateReceipts.Text = string.Empty;
                        chkPaperDutySlip.Checked = false;
                        lblCreatedBy.Text = string.Empty;
                        lblCreatedDate.Text = string.Empty;
                        lblModifyBy.Text = string.Empty;
                        lblModifyDate.Text = string.Empty;
                    }
                }
            }
            else
            {
                lblBookingStatus.Text = string.Empty;
                lblPickUpCity.Text = string.Empty;
                lblPickUpDateTime.Text = string.Empty;

                lblDispatchedCar.Text = string.Empty;
                lblDispatchedChauffeur.Text = string.Empty;
                lblCorDriveStatus.Text = string.Empty;

                lblCreatedBy.Text = string.Empty;
                lblCreatedDate.Text = string.Empty;
                lblModifyBy.Text = string.Empty;
                lblModifyDate.Text = string.Empty;

                txtBookingId.Text = string.Empty;
                txtParkingReceipts.Text = string.Empty;
                txtTollReceipts.Text = string.Empty;
                txtInterStateReceipts.Text = string.Empty;

                chkInterStateReceipts.Checked = false;
                chkParkingReceipts.Checked = false;
                chkTollReceipts.Checked = false;
                chkPaperDutySlip.Checked = false;

            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        try
        {
            if (chkParkingReceipts.Checked == true)
            {
                txtParkingReceipts.Enabled = true;
                if ((string.IsNullOrEmpty(txtParkingReceipts.Text) ? 0 : Convert.ToInt32(txtParkingReceipts.Text)) <= 0)
                {
                    ShowMessage("Please Enter Valid Parking Amount");
                    return;
                }
            }
            if (chkTollReceipts.Checked == true)
            {
                if ((string.IsNullOrEmpty(txtTollReceipts.Text) ? 0 : Convert.ToInt32(txtTollReceipts.Text)) <= 0)
                {
                    ShowMessage("Please Enter Valid Toll Amount");
                    return;
                }
            }

            if (chkInterStateReceipts.Checked == true)
            {
                if ((string.IsNullOrEmpty(txtInterStateReceipts.Text) ? 0 : Convert.ToInt32(txtInterStateReceipts.Text)) <= 0)
                {
                    ShowMessage("Please Enter Valid InterState Amount");
                    return;
                }
            }

            int i = objCordrive.SaveDutySlipDetails(Convert.ToInt32(txtBookingId.Text), string.IsNullOrEmpty(txtParkingReceipts.Text) ? 0 : Convert.ToDouble(txtParkingReceipts.Text), string.IsNullOrEmpty(txtTollReceipts.Text) ? 0 : Convert.ToDouble(txtTollReceipts.Text), string.IsNullOrEmpty(txtInterStateReceipts.Text) ? 0 : Convert.ToDouble(txtInterStateReceipts.Text), chkPaperDutySlip.Checked, Convert.ToInt32(Session["UserID"]));
            if (i > 0)
            {
                lblMsg.Text = "Record Added Successfully";
                lblMsg.Visible = true;
                lblBookingStatus.Text = string.Empty;
                lblPickUpCity.Text = string.Empty;
                lblPickUpDateTime.Text = string.Empty;

                lblDispatchedCar.Text = string.Empty;
                lblDispatchedChauffeur.Text = string.Empty;
                lblCorDriveStatus.Text = string.Empty;

                txtBookingId.Text = string.Empty;
                txtParkingReceipts.Text = string.Empty;
                txtTollReceipts.Text = string.Empty;
                txtInterStateReceipts.Text = string.Empty;

                chkInterStateReceipts.Checked = false;
                chkParkingReceipts.Checked = false;
                chkTollReceipts.Checked = false;
                chkPaperDutySlip.Checked = false;
                lblClientName.Text = string.Empty;
                lblGuestName.Text = string.Empty;
                lblClosedBy.Text = string.Empty;
            }
            else
            {
                lblMsg.Text = "Record Not Added Successfully";
                lblMsg.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }

    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }

    protected void bntViewDetails_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        objCordrive = new CorDrive();
        lblMsg.Visible = false;
        ds = objCordrive.GetHardCopyDetails(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), Convert.ToInt32(ddlUserName.SelectedValue));
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvShowDetails.DataSource = ds.Tables[0];
            gvShowDetails.DataBind();
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Record not available.";
            gvShowDetails.DataSource = null;
            gvShowDetails.DataBind();

        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    private void BindGrid()
    {
        DataSet ds = new DataSet();
        objCordrive = new CorDrive();
        ds = objCordrive.GetHardCopyDetails(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), Convert.ToInt32(ddlUserName.SelectedValue));
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvShowDetails.DataSource = ds.Tables[0];
            gvShowDetails.DataBind();
            WorkbookEngine.ExportDataSetToExcel(ds, "HardCopyDetails.xls");
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Record not available.";
            gvShowDetails.DataSource = null;
            gvShowDetails.DataBind();
        }
    }

    protected void getRelationShipManagers()
    {
        clsAdmin objAdmin = new clsAdmin();
        ddlUserName.DataTextField = "UserName";
        ddlUserName.DataValueField = "SysUserID";
        DataSet dsusername = new DataSet();
        dsusername = objAdmin.getUserList();
        ddlUserName.DataSource = dsusername;
        ddlUserName.DataBind();
        ddlUserName.Items.Insert(0, new ListItem("All", "0"));
    }
}