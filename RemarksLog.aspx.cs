using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class RemarksLog : System.Web.UI.Page
{
    int CarId;
    private CorDrive objCordrive = null;
    DataSet dsRemarksLog = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        if ((Request.QueryString["CarId"] != null))
        {
            CarId = Convert.ToInt32(Request.QueryString["CarId"]);
            dsRemarksLog = objCordrive.GetRemarksLog(CarId);
            if (dsRemarksLog.Tables[0].Rows.Count > 0)
            {
                grdRemarksLog.DataSource = dsRemarksLog.Tables[0];
                grdRemarksLog.DataBind();
            }
            else
            {
                grdRemarksLog.DataSource = null;
                grdRemarksLog.DataBind();
            }
        }
    }
}
