﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
public partial class CorDriveReport_BookingMix : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }

    private void BindBrachDropDownlist()
    {
        objCordrive = new CorDrive();
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "UnitName";
                    ddlBranch.DataValueField = "UnitId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    protected void bntGet_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(txtFromDate.Text) || string.IsNullOrEmpty(txtToDate.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date and to date should not be blank')", true);
            return;
        }
        else
        {

            StringBuilder strData = new StringBuilder();
            strData = BindExcelData();
            ltlSummary.Text = strData.ToString();
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "BookingMixReport";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();

    }

    private StringBuilder BindExcelData()
    {
        StringBuilder strData = new StringBuilder();
        DateTime fromDate = Convert.ToDateTime(txtFromDate.Text.ToString());
        DateTime toDate = Convert.ToDateTime(txtToDate.Text.ToString());
        DateTime startDate;
        objCordrive = new CorDrive();
        DataSet dsDetails = objCordrive.GetMixBooking(Convert.ToInt16(ddlBranch.SelectedValue), fromDate, toDate);
        try
        {

            strData.Append(" <html xmlns:o='urn:schemas-microsoft-com:office:office' ");
            strData.Append(" xmlns:x='urn:schemas-microsoft-com:office:excel' ");
            strData.Append(" xmlns='http://www.w3.org/TR/REC-html40'> ");
            strData.Append(" <head> ");
            strData.Append(" <meta http-equiv=Content-Type content='text/html; charset=windows-1252'> ");
            strData.Append(" <meta name=ProgId content=Excel.Sheet> ");
            strData.Append(" <meta name=Generator content='Microsoft Excel 14'> ");
            strData.Append(" <link rel=File-List ");
            strData.Append(" href='New%20Microsoft%20Excel%20Worksheet%20(2)_files/filelist.xml'> ");
            strData.Append(" <style id='New Microsoft Excel Worksheet (2)_14377_Styles'> ");
            strData.Append(" <!--table ");
            strData.Append(" {mso-displayed-decimal-separator:; ");
            strData.Append(" mso-displayed-thousand-separator:;} ");
            strData.Append(" .xl1514377 ");
            strData.Append(" {padding-top:1px; ");
            strData.Append(" padding-right:1px; ");
            strData.Append(" padding-left:1px; ");
            strData.Append(" mso-ignore:padding; ");
            strData.Append(" color:black; ");
            strData.Append(" font-size:11.0pt; ");
            strData.Append(" font-weight:400; ");
            strData.Append(" font-style:normal; ");
            strData.Append(" text-decoration:none; ");
            strData.Append(" font-family:Calibri, sans-serif; ");
            strData.Append(" mso-font-charset:0; ");
            strData.Append(" mso-number-format:General; ");
            strData.Append(" text-align:general; ");
            strData.Append(" vertical-align:bottom; ");
            strData.Append(" mso-background-source:auto; ");
            strData.Append(" mso-pattern:auto; ");
            strData.Append(" white-space:nowrap;} ");
            strData.Append(" .xl6314377 ");
            strData.Append(" {padding-top:1px; ");
            strData.Append(" padding-right:1px; ");
            strData.Append(" padding-left:1px; ");
            strData.Append(" mso-ignore:padding; ");
            strData.Append(" color:black; ");
            strData.Append(" font-size:11.0pt; ");
            strData.Append(" font-weight:700; ");
            strData.Append(" font-style:normal; ");
            strData.Append(" text-decoration:none; ");
            strData.Append(" font-family:Calibri, sans-serif; ");
            strData.Append(" mso-font-charset:0; ");
            strData.Append(" mso-number-format:General; ");
            strData.Append(" text-align:center; ");
            strData.Append(" vertical-align:middle; ");
            strData.Append(" border:1.0pt solid windowtext; ");
            strData.Append(" background:yellow; ");
            strData.Append(" mso-pattern:black none; ");
            strData.Append(" white-space:nowrap;} ");
            strData.Append(" .xl6414377 ");
            strData.Append(" {padding-top:1px; ");
            strData.Append(" padding-right:1px; ");
            strData.Append(" padding-left:1px; ");
            strData.Append(" mso-ignore:padding; ");
            strData.Append(" color:black; ");
            strData.Append(" font-size:11.0pt; ");
            strData.Append(" font-weight:700; ");
            strData.Append(" font-style:normal; ");
            strData.Append(" text-decoration:none; ");
            strData.Append(" font-family:Calibri, sans-serif; ");
            strData.Append(" mso-font-charset:0; ");
            strData.Append(" mso-number-format:General; ");
            strData.Append(" text-align:center; ");
            strData.Append(" vertical-align:middle; ");
            strData.Append(" border:1.0pt solid windowtext; ");
            strData.Append(" background:yellow; ");
            strData.Append(" mso-pattern:black none; ");
            strData.Append(" white-space:normal;} ");

            strData.Append(" .xl6514377 ");
            strData.Append(" {padding-top:1px; ");
            strData.Append(" padding-right:1px; ");
            strData.Append(" padding-left:1px; ");
            strData.Append(" mso-ignore:padding; ");
            strData.Append(" color:black; ");
            strData.Append(" font-size:11.0pt; ");
            strData.Append(" font-weight:400; ");
            strData.Append(" font-style:normal; ");
            strData.Append(" text-decoration:none; ");
            strData.Append(" font-family:Calibri, sans-serif; ");
            strData.Append(" mso-font-charset:0; ");
            strData.Append(" mso-number-format:General; ");
            strData.Append(" text-align:center; ");
            strData.Append(" vertical-align:middle; ");
            strData.Append(" border:1.0pt solid windowtext; ");
            strData.Append(" mso-background-source:auto; ");
            strData.Append(" mso-pattern:auto; ");
            strData.Append(" white-space:normal;} ");

            strData.Append(" .xl6614377 ");
            strData.Append(" {padding-top:1px; ");
            strData.Append(" padding-right:1px; ");
            strData.Append(" padding-left:1px; ");
            strData.Append(" mso-ignore:padding; ");
            strData.Append(" color:black; ");
            strData.Append(" font-size:11.0pt; ");
            strData.Append(" font-weight:400; ");
            strData.Append(" font-style:normal; ");
            strData.Append(" text-decoration:none; ");
            strData.Append(" font-family:Calibri, sans-serif; ");
            strData.Append(" mso-font-charset:0; ");
            strData.Append(" mso-number-format:General; ");
            strData.Append(" text-align:center; ");
            strData.Append(" vertical-align:middle; ");
            strData.Append(" border:1.0pt solid windowtext; ");
            strData.Append(" background:yellow; ");
            strData.Append(" mso-pattern:black none; ");
            strData.Append(" white-space:normal;} ");
            strData.Append(" --> ");
            strData.Append(" </style> ");
            strData.Append(" </head> ");
            strData.Append(" <body> ");
            strData.Append(" <!--[if !excel]>&nbsp;&nbsp;<![endif]--> ");
            strData.Append(" <!--The following information was generated by Microsoft Excel's Publish as Web ");
            strData.Append(" Page wizard.--> ");
            strData.Append(" <!--If the same item is republished from Excel, all information between the DIV ");
            strData.Append(" tags will be replaced.--> ");
            strData.Append(" <!-----------------------------> ");
            strData.Append(" <!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --> ");
            strData.Append(" <!-----------------------------> ");
            strData.Append(" <div id='New Microsoft Excel Worksheet (2)_14377' align=center ");
            strData.Append(" x:publishsource='Excel'> ");
            strData.Append(" <table border=1 cellpadding=0 cellspacing=0 width=1799 style='border-collapse: ");
            strData.Append(" collapse;table-layout:fixed;width:1351pt'> ");
            strData.Append(" <col width=85 style='mso-width-source:userset;mso-width-alt:3108;width:64pt'> ");
            strData.Append(" <col width=133 style='mso-width-source:userset;mso-width-alt:4864;width:100pt'> ");
            strData.Append(" <col width=108 style='mso-width-source:userset;mso-width-alt:3949;width:81pt'> ");
            strData.Append(" <col width=138 style='mso-width-source:userset;mso-width-alt:5046;width:104pt'> ");
            strData.Append(" <col width=112 style='mso-width-source:userset;mso-width-alt:4096;width:84pt'> ");
            strData.Append(" <col width=133 style='mso-width-source:userset;mso-width-alt:4864;width:100pt'> ");
            strData.Append(" <col width=115 style='mso-width-source:userset;mso-width-alt:4205;width:86pt'> ");
            strData.Append(" <col width=135 style='mso-width-source:userset;mso-width-alt:4937;width:101pt'> ");
            strData.Append(" <col width=107 style='mso-width-source:userset;mso-width-alt:3913;width:80pt'> ");
            strData.Append(" <col width=132 style='mso-width-source:userset;mso-width-alt:4827;width:99pt'> ");
            strData.Append(" <col width=114 style='mso-width-source:userset;mso-width-alt:4169;width:86pt'> ");
            strData.Append(" <col width=136 style='mso-width-source:userset;mso-width-alt:4973;width:102pt'> ");
            strData.Append(" <col width=106 style='mso-width-source:userset;mso-width-alt:3876;width:80pt'> ");
            strData.Append(" <col width=132 style='mso-width-source:userset;mso-width-alt:4827;width:99pt'> ");
            strData.Append(" <col width=113 style='mso-width-source:userset;mso-width-alt:4132;width:85pt'> ");
            strData.Append(" <tr height=20 style='height:15.0pt'> ");
            strData.Append(" <td rowspan=2 height=80 class=xl6314377 width=85 style='height:60.0pt; ");
            strData.Append(" width:64pt'>Car Category</td> ");

            startDate = fromDate;
            string daysName = string.Empty;
            string showDate = string.Empty;

            while (startDate < toDate)
            {
                daysName = startDate.DayOfWeek.ToString();
                strData.Append(" <td colspan=2 class=xl6314377 width=241 style='border-left:none;width:181pt'>" + daysName.ToString() + "</td> ");
                startDate = startDate.AddDays(1);

            }
            strData.Append(" <td colspan=2 class=xl6314377 width=245 style='border-left:none;width:184pt'>Daily ");
            strData.Append(" Average</td> ");
            strData.Append(" </tr> ");

            strData.Append(" <tr height=60 style='height:45.0pt'> ");

            startDate = fromDate;
            while (startDate < toDate)
            {
                strData.Append(" <td height=60 class=xl6414377 width=133 style='height:45.0pt;border-top:none; ");
                strData.Append(" border-left:none;width:100pt'>Number of Bookings</td> ");
                strData.Append(" <td class=xl6414377 width=108 style='border-top:none;border-left:none; ");
                strData.Append(" width:81pt'>Revenue Earned</td> ");
                startDate = startDate.AddDays(1);
            }
            strData.Append(" <td height=60 class=xl6414377 width=133 style='height:45.0pt;border-top:none; ");
            strData.Append(" border-left:none;width:100pt'>Number of Bookings</td> ");
            strData.Append(" <td class=xl6414377 width=108 style='border-top:none;border-left:none; ");
            strData.Append(" width:81pt'>Revenue Earned</td> ");

            strData.Append(" </tr> ");

            string noofbooking = string.Empty;
            string revenue = string.Empty;

            foreach (DataRow dr in dsDetails.Tables[0].Rows)
            {
                if (dr["CarCatName"].ToString() != "zzzzzz")
                {
                    strData.Append(" <tr height=20 style='height:15.0pt'> ");
                    strData.Append(" <td height=20 class=xl6514377 width=85 style='height:15.0pt;border-top: ");
                    strData.Append(" none;width:64pt'>" + dr["CarCatName"].ToString() + "<span ");
                    strData.Append(" style='mso-spacerun:yes'> </span></td> ");

                    startDate = fromDate;
                    while (startDate < toDate)
                    {
                        daysName = startDate.ToString("MMMM") + "_" + startDate.Day + "_" + startDate.Year;
                        noofbooking = "NoBookings_" + daysName;
                        revenue = "Revenue_" + daysName;
                        if (!string.IsNullOrEmpty(dr[noofbooking].ToString()))
                        {
                            strData.Append(" <td class=xl6514377 width=133 style='border-top:none;border-left:none; ");
                            strData.Append(" width:100pt'>" + Math.Round(Convert.ToDecimal(dr[noofbooking]), 2) + "</td> ");
                        }
                        else
                        {
                            strData.Append(" <td class=xl6514377 width=133 style='border-top:none;border-left:none; ");
                            strData.Append(" width:100pt'>0</td> ");
                        }

                        if (!string.IsNullOrEmpty(dr[revenue].ToString()))
                        {

                            strData.Append(" <td class=xl6514377 width=108 style='border-top:none;border-left:none; ");
                            strData.Append(" width:81pt'>" + Math.Round(Convert.ToDecimal(dr[revenue]), 2) + " ");
                            strData.Append("</td> ");
                        }
                        else
                        {
                            strData.Append(" <td class=xl6514377 width=133 style='border-top:none;border-left:none; ");
                            strData.Append(" width:100pt'>0</td> ");
                        }

                        startDate = startDate.AddDays(1);

                    }

                    strData.Append(" <td class=xl6514377 width=133 style='border-top:none;border-left:none; ");
                    strData.Append(" width:100pt'>" + Math.Round(Convert.ToDecimal(dr["DailyNumberOfBooking"]), 2) + "</td> ");
                    strData.Append(" <td class=xl6514377 width=108 style='border-top:none;border-left:none; ");
                    strData.Append(" width:81pt'>" + Math.Round(Convert.ToDecimal(dr["DailyRevenueEarned"]), 2) + " ");
                    strData.Append(" </tr> ");
                }
                else
                {

                    strData.Append(" <tr height=20 style='height:15.0pt'> ");
                    startDate = fromDate;
                    strData.Append(" <td height=20 class=xl6414377 width=85 style='height:15.0pt;border-top:none; ");
                    strData.Append(" width:64pt'>Total</td> ");

                    while (startDate < toDate)
                    {
                        daysName = startDate.ToString("MMMM") + "_" + startDate.Day + "_" + startDate.Year;
                        noofbooking = "NoBookings_" + daysName;
                        revenue = "Revenue_" + daysName;

                        if (!string.IsNullOrEmpty(dr[noofbooking].ToString()))
                        {

                            strData.Append(" <td class=xl6614377 width=133 style='border-top:none;border-left:none; ");
                            strData.Append(" width:100pt'>" + Math.Round(Convert.ToDecimal(dr[noofbooking]), 2) + "</td> ");

                        }
                        else
                        {
                            strData.Append(" <td class=xl6614377 width=133 style='border-top:none;border-left:none; ");
                            strData.Append(" width:100pt'>0</td> ");
                        }

                        if (!string.IsNullOrEmpty(dr[revenue].ToString()))
                        {
                            strData.Append(" <td class=xl6614377 width=108 style='border-top:none;border-left:none; ");
                            strData.Append(" width:81pt'>" + Math.Round(Convert.ToDecimal(dr[revenue]), 2) + "</td> ");

                        }
                        else
                        {
                            strData.Append(" <td class=xl6614377 width=108 style='border-top:none;border-left:none; ");
                            strData.Append(" width:81pt'>0</td> ");
                        }

                        startDate = startDate.AddDays(1);
                    }

                    if (!string.IsNullOrEmpty(dr["DailyRevenueEarned"].ToString()))
                    {
                        strData.Append(" <td class=xl6614377 width=132 style='border-top:none;border-left:none; ");
                        strData.Append(" width:99pt'>" + Math.Round(Convert.ToDecimal(dr["DailyRevenueEarned"]), 2) + "</td> ");
                    }
                    else
                    {
                        strData.Append(" <td class=xl6614377 width=132 style='border-top:none;border-left:none; ");
                        strData.Append(" width:99pt'>0</td> ");
                    }

                    if (!string.IsNullOrEmpty(dr["DailyRevenueEarned"].ToString()))
                    {
                        strData.Append(" <td class=xl6614377 width=113 style='border-top:none;border-left:none; ");
                        strData.Append(" width:85pt'>" + Math.Round(Convert.ToDecimal(dr["DailyRevenueEarned"]), 2) + "</td> ");
                    }
                    else
                    {
                        strData.Append(" <td class=xl6614377 width=113 style='border-top:none;border-left:none; ");
                        strData.Append(" width:85pt'>0</td> ");
                    }

                    strData.Append(" </tr> ");
                }
            }


            strData.Append(" <![if supportMisalignedColumns]> ");
            strData.Append(" <tr height=0 style='display:none'> ");
            strData.Append(" <td width=85 style='width:64pt'></td> ");
            strData.Append(" <td width=133 style='width:100pt'></td> ");
            strData.Append(" <td width=108 style='width:81pt'></td> ");
            strData.Append(" <td width=138 style='width:104pt'></td> ");
            strData.Append(" <td width=112 style='width:84pt'></td> ");
            strData.Append(" <td width=133 style='width:100pt'></td> ");
            strData.Append(" <td width=115 style='width:86pt'></td> ");
            strData.Append(" <td width=135 style='width:101pt'></td> ");
            strData.Append(" <td width=107 style='width:80pt'></td> ");
            strData.Append(" <td width=132 style='width:99pt'></td> ");
            strData.Append(" <td width=114 style='width:86pt'></td> ");
            strData.Append(" <td width=136 style='width:102pt'></td> ");
            strData.Append(" <td width=106 style='width:80pt'></td> ");
            strData.Append(" <td width=132 style='width:99pt'></td> ");
            strData.Append(" <td width=113 style='width:85pt'></td> ");
            strData.Append(" </tr> ");
            strData.Append(" <![endif]> ");
            strData.Append(" </table> ");
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return strData;
    }

}