﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using ReportingTool;
using System;

/// <summary>
/// Summary description for CorDrive
/// </summary>
public class CorDrive
{
    public CorDrive()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataSet GetLocations_UserAccessWise_Unit(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_Unit", ObjParam);
    }

    public DataSet GetActiveCarModel()
    {
        SqlParameter[] ObjParam = new SqlParameter[0];

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetActiveCarModel", ObjParam);
    }
    public DataSet GetCarCategory()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarCategory");
    }

    public DataSet GetLoginHoursDetail(DateTime _FromDate, DateTime _ToDate, int _CityId, int LoginStatus)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = _FromDate;

        ObjParam[1] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = _ToDate;

        ObjParam[2] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = _CityId;

        ObjParam[3] = new SqlParameter("@logStatus", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = LoginStatus;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ChauffuerLoginDetail_NewReport", ObjParam);
    }

    public DataSet GetVenderUsgaeReport(int _BranchId, DateTime _FromDate, DateTime _ToDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];

        ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = _BranchId;

        ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = _FromDate;

        ObjParam[2] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = _ToDate;


        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_OnCallVendorUsageReport", ObjParam);
    }

    public DataSet GetPKPMData(int _CityId, DateTime _FromDate, DateTime _ToDate, int _VenDorId, int _ModelId, int VdpYN)
    {
        SqlParameter[] ObjParam = new SqlParameter[6];

        ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = _CityId;

        ObjParam[1] = new SqlParameter("@PickupDateFrom", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = _FromDate;

        ObjParam[2] = new SqlParameter("@PickupDateTo", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = _ToDate;

        ObjParam[3] = new SqlParameter("@ModelId", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = _ModelId;

        ObjParam[4] = new SqlParameter("@VendorID", SqlDbType.Int);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = _VenDorId;

        ObjParam[5] = new SqlParameter("@VDPYN", SqlDbType.Int);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = VdpYN;
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_PKPMReport_New", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_PKPMReport_New_13_May", ObjParam);
    }

    public DataSet BrachChauffuerDetails(int cityId, int status, int modelId, int vDPYN, int chauffuerSource, int carVendorId)
    {
        SqlParameter[] ObjParam = new SqlParameter[6];

        ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = cityId;

        ObjParam[1] = new SqlParameter("@Status", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = status;

        ObjParam[2] = new SqlParameter("@ModelId", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = modelId;

        ObjParam[3] = new SqlParameter("@VDPYN", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = vDPYN;

        ObjParam[4] = new SqlParameter("@ChauffuerSource", SqlDbType.Int);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = chauffuerSource;

        ObjParam[5] = new SqlParameter("@CarVendorId", SqlDbType.Int);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = carVendorId;


        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchChauffuerList", ObjParam);
    }

    public DataSet GetCorDriveTrackingReport(int _BranchId, DateTime _FromDate, DateTime _ToDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];

        ObjParam[0] = new SqlParameter("@BranchID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = _BranchId;

        ObjParam[1] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = _FromDate;

        ObjParam[2] = new SqlParameter("@DropoffDate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = _ToDate;


        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CORDriveTracking", ObjParam);
    }

    public DataSet GetNextDutySlip(int _BookingId, int _CarId, DateTime _FromDate, DateTime _ToDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];

        ObjParam[0] = new SqlParameter("@CurrentBookingId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = _BookingId;

        ObjParam[1] = new SqlParameter("@CarId", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = _CarId;

        ObjParam[2] = new SqlParameter("@PickupFromdate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = _FromDate;

        ObjParam[3] = new SqlParameter("@PickupTodate", SqlDbType.DateTime);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = _ToDate;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetNextDuty", ObjParam);
    }
    public DataSet GetCarInInventryList(int CityId, int Status, int ModelId, int VDPYN, int CarAge)
    {
        SqlParameter[] ObjParam = new SqlParameter[5];

        ObjParam[0] = new SqlParameter("@cityID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityId;

        ObjParam[1] = new SqlParameter("@Status", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = Status;

        ObjParam[2] = new SqlParameter("@ModelId", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = ModelId;

        ObjParam[3] = new SqlParameter("@VDPYN", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = VDPYN;

        ObjParam[4] = new SqlParameter("@AgeOfCar", SqlDbType.Int);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = CarAge;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CarsinInventoryList", ObjParam);
    }

    public DataSet GetCorDrivInvoiceReport(int CityId, DateTime PickupDate, DateTime DropOffDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];

        ObjParam[0] = new SqlParameter("@BranchID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityId;

        ObjParam[1] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = PickupDate;

        ObjParam[2] = new SqlParameter("@DropoffDate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = DropOffDate;


        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CORDriveInvoiceReport", ObjParam);
    }
    public DataSet DriverEndedReport(int CityId, DateTime PickupDate, DateTime DropOffDate, string Status)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];

        ObjParam[0] = new SqlParameter("@BranchID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityId;

        ObjParam[1] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = PickupDate;

        ObjParam[2] = new SqlParameter("@DropoffDate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = DropOffDate;

        ObjParam[3] = new SqlParameter("@Status", SqlDbType.VarChar);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = Status;


        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_DriverEndedReport", ObjParam);
    }
    public DataSet GetCorDriveBranchInventryReport(int CityId, int CarCatID, int AttachmentPackge, int CarID, DateTime FromDate, DateTime ToDate, int Status)
    {
        SqlParameter[] ObjParam = new SqlParameter[7];

        ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityId;

        ObjParam[1] = new SqlParameter("@CarCarId", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = CarCatID;

        ObjParam[2] = new SqlParameter("@AttachmentPackage", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = AttachmentPackge;

        ObjParam[3] = new SqlParameter("@CarSource", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = CarID;

        ObjParam[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = FromDate;

        ObjParam[5] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = ToDate;

        ObjParam[6] = new SqlParameter("@Status", SqlDbType.Int);
        ObjParam[6].Direction = ParameterDirection.Input;
        ObjParam[6].Value = Status;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchInventory", ObjParam);
    }
    public DataSet GetOwnedCarReportData(int UnitId, DateTime FromDate, DateTime ToDate, int ProductId)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];

        ObjParam[0] = new SqlParameter("@BranchId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UnitId;

        ObjParam[1] = new SqlParameter("@PickupDateFrom", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = FromDate;

        ObjParam[2] = new SqlParameter("@PickupDateTo", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = ToDate;

        ObjParam[3] = new SqlParameter("@ProductId", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = ProductId;


        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_Prc_OwnedCarReport_08_May_2015", ObjParam);
    }
    public DataSet GetLoginHoursSummary(int cityId, DateTime fromDate, DateTime toDate, int modelId, int vdpYN)
    {
        SqlParameter[] ObjParam = new SqlParameter[6];

        ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = cityId;

        ObjParam[1] = new SqlParameter("@Fromdate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = fromDate;

        ObjParam[2] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = toDate;

        ObjParam[3] = new SqlParameter("@ModelId", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = modelId;

        ObjParam[4] = new SqlParameter("@VDPYN", SqlDbType.Int);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = vdpYN;

        ObjParam[5] = new SqlParameter("@VendorCarYN", SqlDbType.Int);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = 0;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CORDriveLoginHoursSummary", ObjParam);

    }

    public DataSet GetDayName(DateTime fromDate, DateTime todate)
    {

        SqlParameter[] ObjParam = new SqlParameter[2];

        ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = fromDate;

        ObjParam[1] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = todate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Fn_GetWEEKDAY", ObjParam);
    }
    public DataSet GetMixBooking(int branchId, DateTime fromDate, DateTime todate)
    {

        SqlParameter[] ObjParam = new SqlParameter[3];

        ObjParam[0] = new SqlParameter("@UnitId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = branchId;

        ObjParam[1] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = fromDate;

        ObjParam[2] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = todate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BookingMix", ObjParam);

    }

    public DataSet GetCategoryWiseRevenue(int branchId, DateTime fromDate, DateTime todate)
    {

        SqlParameter[] ObjParam = new SqlParameter[3];

        ObjParam[0] = new SqlParameter("@BrachId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = branchId;

        ObjParam[1] = new SqlParameter("@PickupdateFrom", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = fromDate;

        ObjParam[2] = new SqlParameter("@Pickupdateto", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = todate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CategoryWiseRevenueReport", ObjParam);

    }

    public DataSet GetSoldOutReport(int branchId, DateTime fromDate, DateTime todate)
    {

        SqlParameter[] ObjParam = new SqlParameter[3];

        ObjParam[0] = new SqlParameter("@UnitId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = branchId;

        ObjParam[1] = new SqlParameter("@DateFrom", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = fromDate;

        ObjParam[2] = new SqlParameter("@DateTo", SqlDbType.DateTime);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = todate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SoldOutReport", ObjParam);

    }

    public DataSet GetCurrentDateLoginCar(int cityId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@cityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = cityId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetAllCurrentdateLogin", ObjParam);

    }

    public DataSet GetAutAllocationReports(string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@FromDate", SqlDbType.VarChar);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = fromdate;

        ObjParam[1] = new SqlParameter("@Todate", SqlDbType.VarChar);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = todate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prec_CorDriveBranchWiseAllocationDeallocationSummary", ObjParam);

    }
    public DataSet GetLoginSummaryReport(DateTime PickupDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = PickupDate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_Loginummary", ObjParam);
    }
    public DataSet GetAllocationReport(DateTime PickupDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@Pickupdate", SqlDbType.DateTime);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = PickupDate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_AutoAllocationSummary", ObjParam);
    }

    public DataSet GetLoginSummaryReportCityWise(string CityName)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityName", SqlDbType.VarChar);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityName;
        // return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryCityWiseNew", ObjParam);
        // return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryCityWiseLatest", ObjParam); // commented by bk sharma on 27 nov 2015
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryCityWiseLatest1", ObjParam);
    }
    public DataSet GetLoginSummaryExportToExcel(string CityName)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityName", SqlDbType.VarChar);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityName;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_LoginSummaryExportToExcel", ObjParam);
    }

    public DataSet GetAreaWiseHome(string Latitude, string Longitude, int Radius, int Catid)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@Latitude", SqlDbType.Float);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Latitude;

        ObjParam[1] = new SqlParameter("@Longitude", SqlDbType.Float);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = Longitude;

        ObjParam[2] = new SqlParameter("@Radius", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = Radius;

        ObjParam[3] = new SqlParameter("@Catid", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = Catid;
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseHomeNew", ObjParam); //commented by bk sharma on 27 nov 2015
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseHomeNew1", ObjParam);
    }
    public DataSet GetAreaWiseCurrent(string Latitude, string Longitude, int Radius, int Catid)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@Latitude", SqlDbType.Float);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Latitude;

        ObjParam[1] = new SqlParameter("@Longitude", SqlDbType.Float);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = Longitude;

        ObjParam[2] = new SqlParameter("@Radius", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = Radius;

        ObjParam[3] = new SqlParameter("@Catid", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = Catid;
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseCurrentNew", ObjParam); //Commened By Bk on date 27 Nov 2015
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ReportAreaWiseCurrentNew1", ObjParam);
    }

    public DataSet GetAreaVersion()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "sp_ReportAreaVersionWise");
    }
    public DataSet GetSummaryReportCityWise()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "sp_ReportSummaryCurrent");
    }
    public DataSet GetDispatchSummaryReport()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_RptDispatch");
    }
    public DataSet VendorCarActivityReport(int CityId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_Revenue_Report", ObjParam);
    }

    public DataSet GetLocations_UserAccessWise_SummaryCityWise(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_SummaryCityWise", ObjParam);
    }
    public int SubmitCarRemarks(int CabId, string ReasonType, DateTime FromDate, DateTime ToDate, string Remarks)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[5];
        ObjparamUser[0] = new SqlParameter("@CabId", CabId);
        ObjparamUser[1] = new SqlParameter("@ReasonType", ReasonType);
        ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveNotAvailableReason", ObjparamUser);
    }

    public DataSet CaractivityReport(int CityId, int CategoryID, DateTime Date)
    {

        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@CityID", CityId);
        ObjparamUser[1] = new SqlParameter("@CategoryID", CategoryID);
        ObjparamUser[2] = new SqlParameter("@Date", Date);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CarAvailabilityReportNew", ObjparamUser);
   }

    public DataSet VendorCarActivityReportDateWise(int ServiceType, int CityId, DateTime FromDate, DateTime ToDate, int Status, int sysUserID, int VendorID, string SharePercentage,int SubUnitID)
    {

        SqlParameter[] ObjparamUser = new SqlParameter[9];
        ObjparamUser[0] = new SqlParameter("@ServiceType", ServiceType);
        ObjparamUser[1] = new SqlParameter("@CityId", CityId);
        ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[4] = new SqlParameter("@Status", Status);
        ObjparamUser[5] = new SqlParameter("@SysUserID", sysUserID);
        ObjparamUser[6] = new SqlParameter("@VendorId", VendorID);
        if (SharePercentage == "All")
        {
            ObjparamUser[7] = new SqlParameter("@SharePercentage", DBNull.Value);
        }
        else
        {
            ObjparamUser[7] = new SqlParameter("@SharePercentage", SharePercentage);
        }

        //if(VendorID==0)
        //{
        //    ObjparamUser[6] = new SqlParameter("@VendorId", DBNull.Value);
        //}
        //else
        //{
        //    ObjparamUser[6] = new SqlParameter("@VendorId", VendorID);
        //}
        ObjparamUser[8] = new SqlParameter("@SubLocationAccess", SubUnitID);

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueNew_09", ObjparamUser);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueNew", ObjparamUser);
    }

    public DataSet VendorCarActivityReportDateWiseWithReason(int ServiceType, int CityId, DateTime FromDate, DateTime ToDate, string Reason, int Status, int sysUserID, int VendorID, string SharePercentage,int SubloctionID )
    {
        SqlParameter[] ObjparamUser = new SqlParameter[10];
        ObjparamUser[0] = new SqlParameter("@ServiceType", ServiceType);
        ObjparamUser[1] = new SqlParameter("@CityId", CityId);
        ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[4] = new SqlParameter("@Reason", Reason);
        ObjparamUser[5] = new SqlParameter("@Status", Status);
        ObjparamUser[6] = new SqlParameter("@SysUserID", sysUserID);
        ObjparamUser[7] = new SqlParameter("@VendorId", VendorID);
       
        if (SharePercentage == "All")
        {
            ObjparamUser[8] = new SqlParameter("@SharePercentage", DBNull.Value);
        }
        else
        {
            ObjparamUser[8] = new SqlParameter("@SharePercentage", SharePercentage);
        }
        ObjparamUser[9] = new SqlParameter("@SubLocationAccess", SubloctionID);

        
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueReasonWise_09bkp05", ObjparamUser);
    }
    public DataSet SubmitPauseRideBookingid(int BookingId, int UserId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@BookingId", BookingId);
        ObjparamUser[1] = new SqlParameter("@UserId", UserId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Sp_UpdatePauseRideBookingId", ObjparamUser);
    }

    public DataSet GetNotAvailableDetails(int CabId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@CabId", CabId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_getNotAvailableDetails", ObjparamUser);
    }
    public int SubmitCarRemarks(int CabId, string ReasonType, DateTime FromDate, DateTime ToDate, string Remarks, int User)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[6];
        ObjparamUser[0] = new SqlParameter("@CabId", CabId);
        ObjparamUser[1] = new SqlParameter("@ReasonType", ReasonType);
        ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
        ObjparamUser[5] = new SqlParameter("@User", User);
        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveNotAvailableReason", ObjparamUser);
    }
    public int UpdateCarRemarks(int CabId, string ReasonType, DateTime FromDate, DateTime ToDate, string Remarks, int User)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[6];
        ObjparamUser[0] = new SqlParameter("@CabId", CabId);
        ObjparamUser[1] = new SqlParameter("@ReasonType", ReasonType);
        ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
        ObjparamUser[5] = new SqlParameter("@User", User);
        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_UpdateNotAvailableReason", ObjparamUser);
    }

    public DataSet GetRemarksLog(int CabId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@CabId", CabId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetAllRemarksLog", ObjparamUser);
    }

    public bool IsUserAuthorized_VendorCarActivityRpt(int Userid)
    {
        DataSet dsCheckAccess = new DataSet();
        bool answer = false;
        //int[] UserList = { 1911, 2760, 1471, 444, 49, 920, 2419, 3016, 1139, 2902, 292, 2416, 2591, 164 };
        //for (int i = 0; i < UserList.Length; i++)
        //{
        //    if (UserList[i] == Userid)
        //    {
        //        answer = true;
        //        break;
        //    }
        //}
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@SysUserId", Userid);
        dsCheckAccess = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetCorIntForcedoffroadAccess", ObjparamUser);
        if (dsCheckAccess.Tables[0].Rows.Count > 0)
        {
            answer = true;
        }
        else
        {
            answer = false;
        }
        return answer;
    }

    public DataSet GetCustName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUpClient");
    }
    public DataSet GetActionManagerName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUpUser");
    }
    public int SubmitSalesFollowUp(DateTime DateOfMeeting, int CusId, string MeetingDesc, string ActionItem
        , DateTime ActionByDate, int ActionMgrId, DateTime NextMeetingSchedule, int User
        , string MeetingPersonName, string MeetingPersonMobile, string ClientCoName, string CustomerType
        , string OutcomeofMetting, string ModeofConnect)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[14];
        ObjparamUser[0] = new SqlParameter("@DateOfMeeting", DateOfMeeting);
        ObjparamUser[1] = new SqlParameter("@CusId", CusId);
        ObjparamUser[2] = new SqlParameter("@MeetingDesc", MeetingDesc);
        ObjparamUser[3] = new SqlParameter("@ActionItem", ActionItem);
        ObjparamUser[4] = new SqlParameter("@ActionByDate", ActionByDate);
        ObjparamUser[5] = new SqlParameter("@ActionMgrId", ActionMgrId);
        ObjparamUser[6] = new SqlParameter("@NextMeetingSchedule", NextMeetingSchedule);
        ObjparamUser[7] = new SqlParameter("@User", User);
        ObjparamUser[8] = new SqlParameter("@MeetingPersonName", MeetingPersonName);
        ObjparamUser[9] = new SqlParameter("@MeetingPersonMobile", MeetingPersonMobile);
        ObjparamUser[10] = new SqlParameter("@ClientCoName", ClientCoName);
        ObjparamUser[11] = new SqlParameter("@CustomerType", CustomerType);
        ObjparamUser[12] = new SqlParameter("@OutcomeofMetting", OutcomeofMetting);
        ObjparamUser[13] = new SqlParameter("@ModeOfConnect", ModeofConnect);
        //return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp", ObjparamUser);
        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Sp_SaveSalesFollowUp_New", ObjparamUser);
    }

    public DataSet GetSalesFollowUpReport(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_SalesFollowUp", ObjparamUser);
    }

    public DataSet GetUserName(int UserID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@UserID", UserID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetUserName", ObjparamUser);
    }
    public DataSet GetCityServiceTypeWise(int ServiceType)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@ServiceType", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = ServiceType;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "BindCityName", ObjParam);
    }
    public DataSet GetSubLocation(int SysUserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@SysUserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = SysUserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetSubLocationOnAccessBasis", ObjParam);
    }
    public DataSet GetLocations_UserAccessWise_VendorActivity(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_VendorActivity", ObjParam);
    }
    public DataSet CheckForcedoffRoad(int VendorCarId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@VendorCarId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = VendorCarId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CheckForceOffRoad", ObjParam);
    }
    public DataSet GetVendorPerformence(int CityId, DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@CityId", CityId);
        ObjparamUser[1] = new SqlParameter("@DateFrom", FromDate);
        ObjparamUser[2] = new SqlParameter("@DateTo", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "RelationShipManagerReprotDetails_BKSharma", ObjparamUser);
    }
    public DataSet GetDeallocationReportExportToExcel(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_DeallocationReport", ObjparamUser);
    }
    public DataSet GetBidSummaryReport(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_Call_AutoAllocationBidSummary", ObjparamUser);
    }
    public DataSet GetBidDetailsReport(string CityName, DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@CityName", CityName);
        ObjParam[1] = new SqlParameter("@FromDate", FromDate);
        ObjParam[2] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_AutoAllocationCarBidDetails", ObjParam);
    }
    public DataSet GetCarWiseBidDetailsExportToExcel(DateTime FromDate, DateTime ToDate, int CarId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[2] = new SqlParameter("@CarId", CarId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_ExportBidDetailsCarWise", ObjparamUser);
    }
    public DataSet GetYatraMIS(DateTime FromDate, DateTime ToDate, string OriginCode)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[2] = new SqlParameter("@OriginCode", OriginCode);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_YatraBookingMIS", ObjparamUser);
    }
    public DataSet GetBangaloreAirportVendorReport(int BranchId, DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@BranchId", BranchId);
        ObjparamUser[1] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[2] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_AirportRecommendedVendorReport_ClosedDuties", ObjparamUser);
    }
    public DataSet GetGeolocationReportExportToExcel(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GeolocationReport", ObjparamUser);
    }
    public DataSet GetBookingDetailsByTransaction(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@TranFromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@TranToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_BookingDetailsByTransactionDate", ObjparamUser);
    }
    public DataSet GetVendorDocDetials(int CityId, int Status)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@CityId", CityId);
        ObjparamUser[1] = new SqlParameter("@Status", Status);
        if (CityId == 0)
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetVendorDocReportAllCity", ObjparamUser);
        }
        else
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetVendorDocReport", ObjparamUser);
        }

    }

    public DataSet GetLocations_UserAccessWise_SummaryCityWiseNew(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_Unit", ObjParam);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_SummaryCityWisenew", ObjParam);
    }
    public DataSet GetCorDriveInovieCloseDetails(int cityId, DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@CityId", cityId);
        ObjparamUser[1] = new SqlParameter("@FromDate", ToDate);
        ObjparamUser[2] = new SqlParameter("@ToDate", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetCorDriveCloseDetails", ObjparamUser);

    }

    public DataSet GetEasycabsCorpBokingDetails(DateTime FromDate, DateTime ToDate, int CityId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];

        ObjparamUser[0] = new SqlParameter("@PickUpFrom ", FromDate);
        ObjparamUser[1] = new SqlParameter("@PickUpTo", ToDate);
        ObjparamUser[2] = new SqlParameter("@CityId", CityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetEasycabsCorpBooking", ObjparamUser);
    }

    public DataSet GetRefundBookingDetails(string BookingId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BookingId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = BookingId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_GetRefundDetails", ObjParam);

    }

    public DataSet GetRefundedAmountDetails(string BookingId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BookingId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = BookingId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetRefundDetails", ObjParam);

    }

    public DataSet GetEasycabsDutyRegisterCab(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@PickUpFrom ", FromDate);
        ObjparamUser[1] = new SqlParameter("@PickUpTo", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_RptDutyRegisterCab", ObjparamUser);
    }
    public DataSet GetDaywiseReportforEasycabs(DateTime PickupDate, int CityId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];

        ObjparamUser[0] = new SqlParameter("@PickupDate ", PickupDate);
        ObjparamUser[1] = new SqlParameter("@PickupCity", CityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_DaywiseReportforEasycabs", ObjparamUser);

    }

    public DataSet GetEasycabscarwiserevenueDetails(DateTime FromDate, DateTime ToDate, int CityId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];

        ObjparamUser[0] = new SqlParameter("@fromdate ", FromDate);
        ObjparamUser[1] = new SqlParameter("@todate", ToDate);
        ObjparamUser[2] = new SqlParameter("@CityId", CityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CabWiseRevReport", ObjparamUser);
    }
    public int ApprovingDelayedStartInCORDrive(int SysUserId, int BookingId, string Remarks)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@SysUserId", SysUserId);
        ObjparamUser[1] = new SqlParameter("@BookingId", BookingId);
        ObjparamUser[2] = new SqlParameter("@Remarks", Remarks);
        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ApproveDelayInCorDrive", ObjparamUser);
    }
    public SqlDataReader ShowBookingDetailsIdWise(int BookingId)
    {
        SqlParameter[] sqlParam = new SqlParameter[1];
        sqlParam[0] = new SqlParameter("@BookingId", SqlDbType.Int);
        sqlParam[0].Value = BookingId;
        return SqlHelper.ExecuteReader("SP_GetBookingDetails", sqlParam);
    }

    public int SaveDutySlipDetails(int BookingId, double Parking, double Toll, double Inrerstate, bool PaperDutySlip, int CreatedBy)
    {
        string Output = string.Empty;
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@BookingId", SqlDbType.Int);
        param[0].Value = BookingId;
        param[1] = new SqlParameter("@Parking", SqlDbType.Decimal);
        param[1].Value = Parking;
        param[2] = new SqlParameter("@Toll", SqlDbType.Decimal);
        param[2].Value = Toll;
        param[3] = new SqlParameter("@Inrerstate", SqlDbType.Decimal);
        param[3].Value = Inrerstate;
        param[4] = new SqlParameter("@PaperDutySlip", SqlDbType.Bit);
        param[4].Value = PaperDutySlip;
        param[5] = new SqlParameter("@CreatedBy", SqlDbType.Int);
        param[5].Value = CreatedBy;
        return SqlHelper.ExecuteNonQuery("SP_SaveDutyWiseHardCopySubmission", param);
    }

    public DataSet GetCarRentalcarwiserevenueDetails(DateTime FromDate, DateTime ToDate, int CityId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];

        ObjparamUser[0] = new SqlParameter("@fromdate ", FromDate);
        ObjparamUser[1] = new SqlParameter("@todate", ToDate);
        ObjparamUser[2] = new SqlParameter("@CityId", CityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_CabWiseRevReport_CarRental", ObjparamUser);
    }

    public DataSet GetHardCopyDetails(DateTime FromDate, DateTime ToDate, int UserID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];

        ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[2] = new SqlParameter("@UserID", UserID);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_HardCopyDetails", ObjparamUser);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_HardCopyDetails_New", ObjparamUser);
    }

    public DataSet GetVendorCarCat(int CityId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CityId;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetVendorCarCat", ObjParam);
    }
    public DataSet GetCarAvailabilityDetails(int CityId, DateTime PickUpDate, int VendorId, int CarCatId, string CorDriveStatus, string PurposeOfCall, int CarStatus)
    {
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@CityId", SqlDbType.Int);
        param[0].Value = CityId;
        param[1] = new SqlParameter("@PickUpDate", SqlDbType.Date);
        param[1].Value = PickUpDate;
        param[2] = new SqlParameter("@VendorId", SqlDbType.Int);
        param[2].Value = VendorId;
        param[3] = new SqlParameter("@CarCatId", SqlDbType.Int);
        param[3].Value = CarCatId;
        param[4] = new SqlParameter("@CorDriveStatus", SqlDbType.VarChar);
        param[4].Value = CorDriveStatus;
        param[5] = new SqlParameter("@PurposeOfCall", SqlDbType.VarChar);
        param[5].Value = PurposeOfCall;
        param[6] = new SqlParameter("@FreeCab", SqlDbType.Int);
        param[6].Value = CarStatus;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_CheckAdUpdateCarAvailability", param);
        // return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_CheckAdUpdateCarAvailabilityTest", param);
    }
    public DataSet GetCarAvailabilityDetails(int CarId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[1];
        ObjparamUser[0] = new SqlParameter("@CarId", CarId);
        //ObjparamUser[1] = new SqlParameter("@FromDutyDate",FromDutyDate);
        //ObjparamUser[2] = new SqlParameter("@ToDutyDate",ToDutyDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetCarWiseDailyAvailability", ObjparamUser);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetCarWiseDailyAvailabilityTest",ObjparamUser);

    }
    public ArrayList LoadMinute()
    {
        ArrayList AllMinute = new ArrayList();
        int i = 0;
        AllMinute.Add("0000");
        AllMinute.Add("0030");
        AllMinute.Add("0100");
        AllMinute.Add("0130");

        AllMinute.Add("0200");
        AllMinute.Add("0230");
        AllMinute.Add("0300");
        AllMinute.Add("0330");

        AllMinute.Add("0400");
        AllMinute.Add("0430");
        AllMinute.Add("0500");
        AllMinute.Add("0530");
        AllMinute.Add("0600");
        AllMinute.Add("0630");
        AllMinute.Add("0700");
        AllMinute.Add("0730");

        AllMinute.Add("0800");
        AllMinute.Add("0830");
        AllMinute.Add("0900");
        AllMinute.Add("0930");

        AllMinute.Add("1000");
        AllMinute.Add("1030");
        AllMinute.Add("1100");
        AllMinute.Add("1130");

        AllMinute.Add("1200");
        AllMinute.Add("1230");
        AllMinute.Add("1300");
        AllMinute.Add("1330");

        AllMinute.Add("1400");
        AllMinute.Add("1430");
        AllMinute.Add("1500");
        AllMinute.Add("1530");

        AllMinute.Add("1600");
        AllMinute.Add("1630");
        AllMinute.Add("1700");
        AllMinute.Add("1730");

        AllMinute.Add("1800");
        AllMinute.Add("1830");
        AllMinute.Add("1900");
        AllMinute.Add("1930");
        AllMinute.Add("2000");
        AllMinute.Add("2030");
        AllMinute.Add("2100");
        AllMinute.Add("2130");

        AllMinute.Add("2200");
        AllMinute.Add("2230");
        AllMinute.Add("2300");
        AllMinute.Add("2330");

        return AllMinute;
    }

    public int SaveUpdateCarAvailability(int AutoId, int CarId, DateTime DutyDate, DateTime OldDutyDate, string AvailabilityStatus, string AvailableFromTime, string AvailableToTime, string Comments, int NoOfComplaints, int NoOfPendingDutySlip, string PurposeOfCall, int CreatedBy)
    {
        string Output = string.Empty;
        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@AutoId", SqlDbType.Int);
        param[0].Value = AutoId;
        param[1] = new SqlParameter("@CarId", SqlDbType.Int);
        param[1].Value = CarId;
        param[2] = new SqlParameter("@NewDutyDate", SqlDbType.Date);
        param[2].Value = DutyDate;
        param[3] = new SqlParameter("@OldDutyDate", SqlDbType.Date);
        param[3].Value = OldDutyDate;
        param[4] = new SqlParameter("@AvailabilityStatus", SqlDbType.VarChar, 30);
        param[4].Value = AvailabilityStatus;
        param[5] = new SqlParameter("@AvailableFromTime", SqlDbType.VarChar);
        param[5].Value = AvailableFromTime;
        param[6] = new SqlParameter("@AvailableToTime", SqlDbType.VarChar);
        param[6].Value = AvailableToTime;
        param[7] = new SqlParameter("@Comments", SqlDbType.VarChar);
        param[7].Value = Comments;
        param[8] = new SqlParameter("@NoOfComplaints", SqlDbType.Int);
        param[8].Value = NoOfComplaints;
        param[9] = new SqlParameter("@NoOfPendingDutySlip", SqlDbType.Int);
        param[9].Value = NoOfPendingDutySlip;
        param[10] = new SqlParameter("@PurposeOfCall", SqlDbType.VarChar);
        param[10].Value = PurposeOfCall;
        param[11] = new SqlParameter("@CreatedBy", SqlDbType.Int);
        param[11].Value = CreatedBy;
        return SqlHelper.ExecuteNonQuery("SP_SaveUpdateCarAvailabilityStatus", param);
        // return SqlHelper.ExecuteNonQuery("SP_SaveUpdateCarAvailabilityStatusTest", param);

    }
    public DataTable GetTableMonthly()
    {
        // Here we create a DataTable with four columns.
        DataTable table = new DataTable();
        table.Columns.Add("Month", typeof(string));


        // Here we add five DataRows.
        string Month1 = System.DateTime.Now.ToString("MMM") + "-" + System.DateTime.Now.Year.ToString();

        string Month2 = DateTime.Now.AddMonths(-1).ToString("MMM") + "-" + DateTime.Now.AddMonths(-1).ToString("Y");
        Month2 = Month2.Remove(Month2.IndexOf("-") + 1, Month2.IndexOf(",") - 2);

        string Month3 = DateTime.Now.AddMonths(-2).ToString("MMM") + "-" + DateTime.Now.AddMonths(-2).ToString("Y");
        Month3 = Month3.Remove(Month3.IndexOf("-") + 1, Month3.IndexOf(",") - 2);


        string Month4 = DateTime.Now.AddMonths(-3).ToString("MMM") + "-" + DateTime.Now.AddMonths(-3).ToString("Y");
        Month4 = Month4.Remove(Month4.IndexOf("-") + 1, Month4.IndexOf(",") - 2);

        string Month5 = DateTime.Now.AddMonths(-4).ToString("MMM") + "-" + DateTime.Now.AddMonths(-4).ToString("Y");
        Month5 = Month5.Remove(Month5.IndexOf("-") + 1, Month5.IndexOf(",") - 2);
        string PrevMonth5 = DateTime.Now.AddMonths(-4).ToShortDateString();

        string Month6 = DateTime.Now.AddMonths(-5).ToString("MMM") + "-" + DateTime.Now.AddMonths(-5).ToString("Y");
        Month6 = Month6.Remove(Month6.IndexOf("-") + 1, Month6.IndexOf(",") - 2);

        string Month7 = DateTime.Now.AddMonths(-6).ToString("MMM") + "-" + DateTime.Now.AddMonths(-6).ToString("Y");
        Month7 = Month7.Remove(Month7.IndexOf("-") + 1, Month7.IndexOf(",") - 2);

        string Month8 = DateTime.Now.AddMonths(-7).ToString("MMM") + "-" + DateTime.Now.AddMonths(-7).ToString("Y");
        Month8 = Month8.Remove(Month8.IndexOf("-") + 1, Month8.IndexOf(",") - 2);

        string Month9 = DateTime.Now.AddMonths(-8).ToString("MMM") + "-" + DateTime.Now.AddMonths(-8).ToString("Y");
        Month9 = Month9.Remove(Month9.IndexOf("-") + 1, Month9.IndexOf(",") - 2);

        string Month10 = DateTime.Now.AddMonths(-9).ToString("MMM") + "-" + DateTime.Now.AddMonths(-9).ToString("Y");
        Month10 = Month10.Remove(Month10.IndexOf("-") + 1, Month10.IndexOf(",") - 2);

        string Month11 = DateTime.Now.AddMonths(-10).ToString("MMM") + "-" + DateTime.Now.AddMonths(-10).ToString("Y");
        Month11 = Month11.Remove(Month11.IndexOf("-") + 1, Month11.IndexOf(",") - 2);

        string Month12 = DateTime.Now.AddMonths(-11).ToString("MMM") + "-" + DateTime.Now.AddMonths(-11).ToString("Y");
        Month12 = Month12.Remove(Month12.IndexOf("-") + 1, Month12.IndexOf(",") - 2);

        table.Rows.Add(Month1);
        table.Rows.Add(Month2);
        table.Rows.Add(Month3);
        table.Rows.Add(Month4);
        table.Rows.Add(Month5);
        table.Rows.Add(Month6);

        table.Rows.Add(Month7);
        table.Rows.Add(Month8);
        table.Rows.Add(Month9);
        table.Rows.Add(Month10);
        table.Rows.Add(Month11);
        table.Rows.Add(Month12);
        return table;
    }
    public DataSet GetIncentive(string EvaluationType, DateTime EvaluationStartDate, DateTime EvaluationEndDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@EvaluationType", EvaluationType);
        ObjparamUser[1] = new SqlParameter("@EvaluationStartDate", EvaluationStartDate);
        ObjparamUser[2] = new SqlParameter("@EvaluationEndDate", EvaluationEndDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetIncentive", ObjparamUser);
    }
    public DataTable GetTableQuartely()
    {
        string Q1 = "Q1-" + DateTime.Now.Year.ToString();
        string Q2 = "Q2-" + DateTime.Now.Year.ToString();
        string Q3 = "Q3-" + DateTime.Now.Year.ToString();
        string Q4 = "Q4-" + DateTime.Now.Year.ToString();

        DataTable table = new DataTable();
        table.Columns.Add("Quarter", typeof(string));
        table.Rows.Add(Q1);
        table.Rows.Add(Q2);
        table.Rows.Add(Q3);
        table.Rows.Add(Q4);
        return table;
    }

    public DataSet GetcarCategoryList()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetcarCategoryList");
    }
    public DataSet VendorCarActivityReportDateWise_New(int ServiceType, int CityId, DateTime FromDate, DateTime ToDate, int CarVendorId, int carcatid, string LoginStatus, string ReasonType)
    {

        SqlParameter[] ObjparamUser = new SqlParameter[8];
        ObjparamUser[0] = new SqlParameter("@ServiceType", ServiceType);
        ObjparamUser[1] = new SqlParameter("@CityId", CityId);
        ObjparamUser[2] = new SqlParameter("@FromDate", FromDate);
        ObjparamUser[3] = new SqlParameter("@ToDate", ToDate);
        ObjparamUser[4] = new SqlParameter("@carcatid", carcatid);
        ObjparamUser[5] = new SqlParameter("@CarVendorId", CarVendorId);
        ObjparamUser[6] = new SqlParameter("@LoginStatus", LoginStatus);
        ObjparamUser[7] = new SqlParameter("@ReasonType", ReasonType);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "VendorCarWiseDetails", ObjparamUser);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueNew_09", ObjparamUser);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCarWorkingDetails_VDP_PKPM_RevenueNew", ObjparamUser);
    }
    public DataSet GetVenorNameList()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "BindVenorNameList");
    }

    public DataSet GetDispositionCallList(DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@FromDt", FromDate);
        ObjparamUser[1] = new SqlParameter("@ToDt", ToDate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_RemarksDtls", ObjparamUser);
    }


    public DataSet GetCityName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetCityDetails");
    }
    public DataSet GetServiceTypeName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetBookingServiceType");
    }

    public DataSet GetClientName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetClientDetails");
    }

    public DataSet GetAddMappedCars(int CityID, int ClientCoId, int Active)
    {
        SqlParameter[] myParams = new SqlParameter[3];
        myParams[0] = new SqlParameter("@CityID", SqlDbType.Int);
        myParams[0].Value = CityID;

        myParams[1] = new SqlParameter("@ClientCoId", SqlDbType.Int);
        myParams[1].Value = ClientCoId;

        myParams[2] = new SqlParameter("@Active", SqlDbType.Int);
        myParams[2].Value = Active;

        //return ImplantDAL.GetDataSet("dbo.SP_GetTollParkDetails", myParams);
        return SqlHelper.ExecuteDataset("dbo.SP_GetMappedCarsDetails", myParams);
    }

    public int SaveMappedCar(int CityID, int ClientID, int @VendorType, string RegnNo, string Remarks, string CreatedBy,int SeriveTypeId)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[7];
        ObjparamUser[0] = new SqlParameter("@CityID", CityID);
        ObjparamUser[1] = new SqlParameter("@ClientID", ClientID);
        ObjparamUser[2] = new SqlParameter("@VendorType", VendorType);
        ObjparamUser[3] = new SqlParameter("@RegnNo", RegnNo);
        ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
        ObjparamUser[5] = new SqlParameter("@CreatedBy", CreatedBy);
        ObjparamUser[6] = new SqlParameter("@ServiceTypeId", SeriveTypeId);
        int status = 0;
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Bl_Sp_SaveMappedCar", ObjparamUser);
        if (ds.Tables[0].Rows.Count > 0)
        {
            status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
        }
        else
        {
            status = -1;
        }

        return status;
    }

    public DataSet UpdateMappedCars(int CityID, int ClientID, int CarID, string Remarks, int Active, string ModifiedBy)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[6];
        ObjparamUser[0] = new SqlParameter("@CityID", CityID);
        ObjparamUser[1] = new SqlParameter("@ClientID", ClientID);
        ObjparamUser[2] = new SqlParameter("@CarID", CarID);
        ObjparamUser[3] = new SqlParameter("@Remarks", Remarks);
        ObjparamUser[4] = new SqlParameter("@Active", Active);
        ObjparamUser[5] = new SqlParameter("@ModifiedBy", ModifiedBy);

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.bl_sp_UpdateMappedCar", ObjparamUser);
    }

    public DataSet DeactiveMappedCars(int CityID, int ClientID, int CarID, string Remarks, int Active, string ModifiedBy)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[6];
        ObjparamUser[0] = new SqlParameter("@CityID", CityID);
        ObjparamUser[1] = new SqlParameter("@ClientID", ClientID);
        ObjparamUser[2] = new SqlParameter("@CarID", CarID);
        ObjparamUser[3] = new SqlParameter("@Remarks", Remarks);
        ObjparamUser[4] = new SqlParameter("@Active", Active);
        ObjparamUser[5] = new SqlParameter("@ModifiedBy", ModifiedBy);

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.bl_sp_UpdateMappedCar", ObjparamUser);
    }

    public DataSet GetVVIPClientName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Bl_Sp_GetIsVVIpClientName");
    }

    public DataSet GetVVIPClientDetails(int @ClientCoID)
    {
        SqlParameter[] myParams = new SqlParameter[1];
        myParams[0] = new SqlParameter("ClientCoId", SqlDbType.Int);
        myParams[0].Value = ClientCoID;

        return SqlHelper.ExecuteDataset("dbo.Bl_Sp_Get_IsVVIpClientDetails", myParams);
    }

    public DataSet UpDateClientcoIndivVIPStaus(int ClientCoID, int ClientCoIndivID, int ISVVIP, string VVIPUpdatedBy)
    {

        SqlParameter[] myParams = new SqlParameter[4];

        myParams[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        myParams[0].Value = ClientCoID;

        myParams[1] = new SqlParameter("@ClientCoIndivID", SqlDbType.Int);
        myParams[1].Value = ClientCoIndivID;

        myParams[2] = new SqlParameter("@ISVVIP", SqlDbType.Int);
        myParams[2].Value = ISVVIP;

        myParams[3] = new SqlParameter("@VVIPUpdatedBy", SqlDbType.VarChar);
        myParams[3].Value = VVIPUpdatedBy;

        return SqlHelper.ExecuteDataset("dbo.BL_SP_UpDateClientcoIndivVIPStaus", myParams);

    }

    public DataSet GetVVIPClientStatusDetails(int @ClientCoID, int ClientCoIndivID, int ISVVIP)
    {
        SqlParameter[] myParams = new SqlParameter[3];
        myParams[0] = new SqlParameter("ClientCoId", SqlDbType.Int);
        myParams[0].Value = ClientCoID;

        myParams[1] = new SqlParameter("@ClientCoIndivID", SqlDbType.Int);
        myParams[1].Value = ClientCoIndivID;

        myParams[2] = new SqlParameter("@ISVVIP", SqlDbType.Int);
        myParams[2].Value = ISVVIP;

        return SqlHelper.ExecuteDataset("dbo.Bl_Sp_GetIsVVIpStatusClientCoIndivWise", myParams);
    }
    public DataSet GetAllCityName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetAllCityDetails");
    }

    public DataSet GetAllClientName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetAllClientDetails");
    }
    public DataSet GetWaitingForPaymentDetails(DateTime CreatePickUpDate, int ClientId, int CityId, int CreateOrPickupDate)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CreatePickUpDate", SqlDbType.Date);
        param[0].Value = CreatePickUpDate;
        param[1] = new SqlParameter("@ClientId", SqlDbType.Int);
        param[1].Value = ClientId;
        param[2] = new SqlParameter("@CityId", SqlDbType.Int);
        param[2].Value = CityId;
        param[3] = new SqlParameter("@CreateOrPickupDate", SqlDbType.Int);
        param[3].Value = CreateOrPickupDate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetWaitingForPaymentDetails", param);

    }

    public DataSet GetHardCopyHandoverDetails(int UserID, bool HandOverYN)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];

        ObjparamUser[0] = new SqlParameter("@UserID", UserID);
        ObjparamUser[1] = new SqlParameter("@HandOverYN", HandOverYN);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_HardCopyDetails_Handover", ObjparamUser);
    }

    public void GetHardCopyHandover(int BookingID, int UserID, int HandOverTo)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];

        ObjparamUser[0] = new SqlParameter("@BookingID", BookingID);
        ObjparamUser[1] = new SqlParameter("@UserID", UserID);
        ObjparamUser[2] = new SqlParameter("@HandedOverTo", HandOverTo);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_HardCopy_Handover", ObjparamUser);
    }

    public DataSet MonthWiseBranchWiseRevenue(int Month, int Year)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@Month", Month);
        ObjParam[1] = new SqlParameter("@Year", Year);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchWiseMonthWisereport", ObjParam);
    }

    public DataSet MonthWiseBranchWiseRevenue_CorRetail(int Month, int Year)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@Month", Month);
        ObjParam[1] = new SqlParameter("@Year", Year);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchWiseMonthWisereport_CorRetail", ObjParam);
    }
    public DataSet GetVendorDetailsByCityID_CorRetail(int CityId)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityId", CityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetVendorListByCity", ObjParam);
    }

    public DataSet GetVendorDetailsByCategoryID()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_CategoryList");
    }

    public DataSet GetVendorDetailsByCityID()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_BindCityName");
    }

    public DataSet GetAllSharePercentages_CorRetail()
    {
        string query = "select distinct RevenueSharePC from CORIntVendorRevenueSharing order by RevenueSharePC ";
        return SqlHelper.ExecuteFunction(query);
    }
}