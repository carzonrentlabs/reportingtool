﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Collections;

/// <summary>
/// Summary description for SMSProvider
/// </summary>
/// 
namespace ReportingTool
{
    public class SMSProvider
    {
        public SMSProvider()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable GetSMSProviderName()
        {
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_SMSProviderName");
        }
        public DataTable GetProviderDetails(int ProviderId)
        {
            SqlParameter[] objParamUser = new SqlParameter[1];
            objParamUser[0] = new SqlParameter("@Id", ProviderId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_SMSProviderDetails", objParamUser);
        }
        public int EditProviderDetails(int ProviderId, string proiderUrl, string userName, string password, string genderGSM, int active, int priority, int sysUserId)
        {
            SqlParameter[] objParamUser = new SqlParameter[8];
            objParamUser[0] = new SqlParameter("@Id", ProviderId);
            objParamUser[1] = new SqlParameter("@ProviderUrl", proiderUrl);
            objParamUser[2] = new SqlParameter("@UserName", userName);
            objParamUser[3] = new SqlParameter("@Password", password);
            objParamUser[4] = new SqlParameter("@SenderGSM", genderGSM);
            objParamUser[5] = new SqlParameter("@Active", active);
            objParamUser[6] = new SqlParameter("@Priority", priority);
            objParamUser[7] = new SqlParameter("@SysUserId", sysUserId);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_EditSMSProvider", objParamUser);

        }
        public int InsertProviderDetails(string ProviderName, string proiderUrl, string userName, string password, string genderGSM, int active, int priority, int sysUserId)
        {
            SqlParameter[] objParamUser = new SqlParameter[8];
            objParamUser[0] = new SqlParameter("@ProviderName", ProviderName);
            objParamUser[1] = new SqlParameter("@ProviderUrl", proiderUrl);
            objParamUser[2] = new SqlParameter("@UserName", userName);
            objParamUser[3] = new SqlParameter("@Password", password);
            objParamUser[4] = new SqlParameter("@SenderGSM", genderGSM);
            objParamUser[5] = new SqlParameter("@Active", active);
            objParamUser[6] = new SqlParameter("@Priority", priority);
            objParamUser[7] = new SqlParameter("@SysUserId", sysUserId);
            object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Prc_InsetSMSProvider", objParamUser);
            int status = Convert.ToInt32(obj);
            return status;
        }

        public DataTable CheckPriorityOfSMS(int Priority)
        {
            SqlParameter[] objParamUser = new SqlParameter[1];
            objParamUser[0] = new SqlParameter("@Priority", Priority);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_CheckPriorityOfSMSProvider", objParamUser);

        }
        public DataTable CheckPriorityOfSMS(int Priority,int ProviderId)
        {
            SqlParameter[] objParamUser = new SqlParameter[2];
            objParamUser[0] = new SqlParameter("@Priority", Priority);
            objParamUser[1] = new SqlParameter("@ProviderId", ProviderId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_CheckPriorityOfSMSProviderForEdit", objParamUser);

        }
    }
}