using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
using System.IO;
using System.Drawing;
public partial class ClientWiseDutyReport : System.Web.UI.Page
{
    clsReport objReports = new clsReport();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        BindGrid();

    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        try
        {
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                DataTable dtSummary = new DataTable();
                dtSummary = objReports.GetClientWisedutyReport();
                if (dtSummary.Rows.Count > 0)
                {

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=DailyTracking.xls");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    //To Export all pages
                    grv_DutySummary.AllowPaging = false;
                    this.BindGrid();
                    grv_DutySummary.HeaderRow.BackColor = Color.White;
                    foreach (TableCell cell in grv_DutySummary.HeaderRow.Cells)
                    {
                        cell.BackColor = grv_DutySummary.HeaderStyle.BackColor;
                    }
                    foreach (GridViewRow row in grv_DutySummary.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = grv_DutySummary.AlternatingRowStyle.BackColor;
                            }
                            else
                            {
                                cell.BackColor = grv_DutySummary.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";
                        }
                    }

                    grv_DutySummary.RenderControl(hw);

                    //style to format numbers to string
                    string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }

            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }
    private void BindGrid()
    {
        try
        {
            DataTable dtSummary = new DataTable();
            dtSummary = objReports.GetClientWisedutyReport();
            if (dtSummary.Rows.Count > 0)
            {
                grv_DutySummary.DataSource = dtSummary;
                grv_DutySummary.DataBind();
            }
            else
            {

                grv_DutySummary.DataSource = null;
                grv_DutySummary.DataBind();
            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
            throw ex;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }
    protected void grv_DutySummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}
