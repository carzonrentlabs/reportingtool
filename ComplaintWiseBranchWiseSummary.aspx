<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ComplaintWiseBranchWiseSummary.aspx.cs" Inherits="ComplaintWiseBranchWiseSummary" Title="CarzonRent :: Internal software"%>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    
<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
    {
        alert("Please Select the From Date");
        document.getElementById('<%=txtFrom.ClientID %>').focus();
        return false;
    }
}

</script>
<script language="javascript" type="text/javascript">
window.history.forward(1);
    </script>
        <table width="802" border="0" align="center" id="TABLE1">
            <tbody>
                <tr>
                    <td align="center" colspan="8" style="height: 18px">
                        <strong><u>Complaint Wise Branch Wise Summary</u></strong>&nbsp;</td>
                </tr>
                <tr>
                   <td align="right" style="width: 2244px">
                        Complaint Date</td>
                    <td width="52" align="left">
                        <input id="txtFrom" runat="server" type="text" readonly="readOnly" /></td>
                    <td align="left" style="width: 39px">
                        <img alt="Click here to open the calendar and select the date corresponding to 'From Date'"
                            onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                            src="App_Themes/images/calender.gif" style="cursor: hand" /></td>
                
                    <%--<td align="right" style="width: 782px">
                        To Date</td>
                    <td width="52" align="left">
                        <input id="txtTo" runat="server" type="text" readonly="readOnly" /></td>
                    <td align="left">
                        <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                            onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                            src="App_Themes/images/calender.gif" style="cursor: hand" /></td>--%>
                            
                                <td align="right" style="width: 700px"> City Name:</td>
                            <td style="width: 210px">
          <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList></td>
 
                </tr>
                <tr>
                    <td align="center" colspan="8" style="height: 22px">
                        
                        &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Get >>" OnClick="btnSubmit_Click" />
                        &nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
        <div>
            <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
                top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
                scrolling="no" height="182"></iframe>
        </div>
</asp:Content>