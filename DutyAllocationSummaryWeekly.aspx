<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DutyAllocationSummaryWeekly.aspx.cs" Inherits="DutyAllocationSummaryWeekly" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    
<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
    {
        alert("Please Select the From Date");
        document.getElementById('<%=txtFrom.ClientID %>').focus();
        return false;
    }
}
</script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Duty Allocation Summary - Weekly</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 529px">Date</td>
                <td align="left" style="width: 38px">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>            
                
                <td align="right" style="width: 105px"> City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>

        <tr>
       
        <td align="center" colspan="8">
        <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found" AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="grvMergeHeader_RowCreated">
        <Columns>
        
        <asp:TemplateField HeaderText="Branches">
        <ItemTemplate>
        <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        
        
        <asp:TemplateField HeaderText="Target">
        <ItemTemplate>
        <asp:Label ID="lblt1" Text='<% #Bind("Target1") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Achieved">
        <ItemTemplate>
        <asp:Label ID="lblA1" Text='<% #Bind("Implementation1") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Target">
        <ItemTemplate>
        <asp:Label ID="lblt2" Text='<% #Bind("Target2") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Achieved">
        <ItemTemplate>
        <asp:Label ID="lblA2" Text='<% #Bind("Implementation2") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        <asp:TemplateField HeaderText="Target">
        <ItemTemplate>
        <asp:Label ID="lblt3" Text='<% #Bind("Target3") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Achieved">
        <ItemTemplate>
        <asp:Label ID="lblA3" Text='<% #Bind("Implementation3") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>   
        
        <asp:TemplateField HeaderText="Target">
        <ItemTemplate>
        <asp:Label ID="lblt4" Text='<% #Bind("Target4") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Achieved">
        <ItemTemplate>
        <asp:Label ID="lblA4" Text='<% #Bind("Implementation4") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>                   
        
        </Columns>
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        </td>
        </tr>
        </tbody>
    </table>
    
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
        top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
        scrolling="no" height="182"></iframe>
    </div>

</asp:Content>