using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
using System.Drawing;
using System.IO;

public partial class MicrosoftPaymentStatus : System.Web.UI.Page
{
    clsReport objReport = new clsReport();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnExportToExcel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

               // DataTable dtSummary = new DataTable();
                 DataTable dt = new DataTable();
                 if (txtFrom.Value.ToString() != "" && txtTo.Value.ToString() != "")
                 {
                     dt = objReport.MicroSoftPaymentStatus(Convert.ToDateTime(txtFrom.Value.ToString()), Convert.ToDateTime(txtTo.Value.ToString()));
                     if (dt.Rows.Count > 0)
                     {
                         Response.Clear();
                         Response.Buffer = true;
                         Response.AddHeader("content-disposition", "attachment;filename=MicrosoftPaymentStatus.xls");
                         Response.Charset = "";
                         Response.ContentType = "application/vnd.ms-excel";
                         //To Export all pages
                         grvPaymentStatus.AllowPaging = false;
                         this.BindGrid();
                         grvPaymentStatus.HeaderRow.BackColor = Color.White;
                         foreach (TableCell cell in grvPaymentStatus.HeaderRow.Cells)
                         {
                             cell.BackColor = grvPaymentStatus.HeaderStyle.BackColor;
                         }
                         foreach (GridViewRow row in grvPaymentStatus.Rows)
                         {
                             row.BackColor = Color.White;
                             foreach (TableCell cell in row.Cells)
                             {
                                 if (row.RowIndex % 2 == 0)
                                 {
                                     cell.BackColor = grvPaymentStatus.AlternatingRowStyle.BackColor;
                                 }
                                 else
                                 {
                                     cell.BackColor = grvPaymentStatus.RowStyle.BackColor;
                                 }
                                 cell.CssClass = "textmode";
                             }
                         }
                         grvPaymentStatus.RenderControl(hw);
                         //style to format numbers to string
                         string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                         Response.Write(style);
                         Response.Output.Write(sw.ToString());
                         Response.Flush();
                         Response.End();
                     }
                 }
                 else
                 {
                     ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please select from date and to date.');", true);
                     return;
                 }
            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }      
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            if (txtFrom.Value.ToString() != "" && txtTo.Value.ToString() != "")
            {
                dt = objReport.MicroSoftPaymentStatus(Convert.ToDateTime(txtFrom.Value.ToString()), Convert.ToDateTime(txtTo.Value.ToString()));
                if (dt.Rows.Count > 0)
                {
                    grvPaymentStatus.DataSource = dt;
                    grvPaymentStatus.DataBind();
                }
                else
                {
                    grvPaymentStatus.DataSource = null;
                    grvPaymentStatus.DataBind();
                    lblMessage.Visible = true;
                    lblMessage.Text = "Record not Available";
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please select from date and to date.');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //base.VerifyRenderingInServerForm(control);
    }
}
