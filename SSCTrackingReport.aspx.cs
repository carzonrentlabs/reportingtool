using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CCS;
//using ExcelUtil;

public partial class SSCTrackingReport : clsPageAuthorization
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            ddlDSStatus2.Style.Add(HtmlTextWriterStyle.Display, "none");
            Bindcompany();
            BingCity();
        }
    }

    protected void Bindcompany()
    {
        clsAdmin ObjAdmin = new clsAdmin();
        ddlCompName.DataTextField = "ClientCoName";
        ddlCompName.DataValueField = "ClientCoID";
        ddlCompName.DataSource = ObjAdmin.getCorpCompanyNames();
        ddlCompName.DataBind();
        ddlCompName.Items.Insert(0, "All");
        ddlCompName.Items[0].Value = "0";
        ddlCompName.Items[0].Selected = true;
    }

    protected void BingCity()
    {
        clsAdmin ObjAdmin = new clsAdmin();
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        ddlCityName.DataSource = ObjAdmin.GetCityName();
        ddlCityName.DataBind();
        ddlCityName.Items.Insert(0, "All");
        ddlCityName.Items[0].Value = "0";
        ddlCityName.Items[0].Selected = true;
    }

    protected void rdoOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdoOption.SelectedValue == "0")
        {
            lblOption.Text = "DS Created";
            txtBatchNo.Enabled = false;
            txtBatchNo.Text = "0";
        }
        else if (rdoOption.SelectedValue == "1")
        {
            lblOption.Text = "Usage Date";
            txtBatchNo.Enabled = false;
            txtBatchNo.Text = "0";
        }
        else if (rdoOption.SelectedValue == "2")
        {
            lblOption.Text = "SSC Approval Date";
            txtBatchNo.Enabled = false;
            txtBatchNo.Text = "0";
        }
        else if (rdoOption.SelectedValue == "3")
        {
            lblOption.Text = "SSC Batch No";
            txtBatchNo.Enabled = true;
            txtBatchNo.Text = "";
        }
        else if (rdoOption.SelectedValue == "4")
        {
            lblOption.Text = "CCS Create Date";
            txtBatchNo.Enabled = false;
            txtBatchNo.Text = "0";
        }
        else if (rdoOption.SelectedValue == "5")
        {
            lblOption.Text = "CCS Batch NO";
            txtBatchNo.Enabled = true;
            txtBatchNo.Text = "";
        }
        else if (rdoOption.SelectedValue == "6")
        {
            lblOption.Text = "Accounting Date";
            txtBatchNo.Enabled = false;
            txtBatchNo.Text = "0";
        }
    }

    protected void ExportToExcel_Click(object sender, EventArgs e)
    {
        int ClientCoId, CityID, Status1, tmpRdoOption, tmpRdoOption1, SentToSSC, CCSBatch, BatchNo;
        string DateFrom, DateTo, Status2;
        ClientCoId = Convert.ToInt32(ddlCompName.SelectedValue);
        CityID = Convert.ToInt32(ddlCityName.SelectedValue);
        DateFrom = txtFrom.Value;
        DateTo = txtTo.Value;
        Status1 = Convert.ToInt32(ddlDSStatus.SelectedValue);

        Status2 = ddlDSStatus2.SelectedValue;
        SentToSSC = Convert.ToInt32(ddlSSCStatus.SelectedValue);
        CCSBatch = Convert.ToInt32(ddlBatchStatus.SelectedValue);

        tmpRdoOption = Convert.ToInt32(rdoOption.SelectedValue);
        tmpRdoOption1 = Convert.ToInt32(rdoOption1.SelectedValue);

        BatchNo = Convert.ToInt32(txtBatchNo.Text);

        //string html = "";
        clsAdmin objadmin = new clsAdmin();
        DataTable dtEx = new DataTable();
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=SSCTranckingReport.ods");
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=SSCTranckingReport.xls");
            Response.ContentType = "application/vnd.ms-excel";
        }
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //if (ExportOption.SelectedItem.ToString() == "Open Office")
        //{
        //    Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        //}
        //else
        //{
        //    Response.ContentType = "application/vnd.ms-excel";
        //}

        this.EnableViewState = false;
        Response.Write("\r\n");

        dsExcel = objadmin.GetDataForExcelSSC(ClientCoId, DateFrom, DateTo, Status1, Status2, tmpRdoOption, SentToSSC, CCSBatch, BatchNo, CityID,ddlSubmissionType.SelectedValue);

        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());

        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");

        int[] iColumns = new int[50];

        if (tmpRdoOption1 == 0) //DS Tracking
        {
            iColumns = new int[46] { 6, 7, 5, 0, 9, 1, 3,44, 4, 8, 11, 34, 12, 13,46, 14, 18, 19, 20, 21, 26, 27, 28, 29, 30, 31, 32, 33, 35, 36, 47, 37, 38, 39,45, 40,41,42,43,48,49,50,51,52,53,54};//2015-11-19
        }

        if (tmpRdoOption1 == 1) //Scanning
        {
            iColumns = new int[32] { 6, 7, 5, 0, 9, 3, 44, 4, 8, 11, 34, 12, 13, 46, 14, 20, 21, 15, 35, 36, 47, 37, 38, 39, 45, 40, 41, 42, 43,48,49,54 };//2015-11-19
        }

        if (tmpRdoOption1 == 2) //Comprehensive
        {
            iColumns = new int[50] { 6, 7, 5, 0, 9, 1, 2, 3, 44, 4, 8, 11, 34, 12, 13, 46, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 35, 36, 47, 37, 38, 39, 45, 40, 41, 42, 43,48,49,54 };//2015-11-19
        }

        if (tmpRdoOption1 == 3) //Email
        {
            iColumns = new int[28] { 6, 7, 5, 0, 9, 4, 8, 11, 34, 12, 13, 16, 17, 26, 35, 36, 47, 37, 38, 49, 45, 40, 41, 42, 43,48,49,54};//2015-11-19
        }

        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                Response.Write("<td>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
}