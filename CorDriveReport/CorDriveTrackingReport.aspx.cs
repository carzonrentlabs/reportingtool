﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class CorDriveReport_CorDriveTrackingReport : System.Web.UI.Page
{
    CorDrive objCorDrive = new CorDrive();
    RentMeterService.wsRentmeter _objService = new RentMeterService.wsRentmeter();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }

    #region "Private Void BindReport"
    
    private void BindBrachDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCorDrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "Cityname";
                    ddlBranch.DataValueField = "CityId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    private void BindCorDriveTrackingReportGrid()
    {
        try
        {
            int _cityId = 0;
            DateTime _fromdate, _todate;
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            DataSet _objDs = new DataSet();
            _objDs = objCorDrive.GetCorDriveTrackingReport(_cityId, _fromdate, _todate);
            if (_objDs != null)
            {
                if (_objDs.Tables[0].Rows.Count > 0)
                {
                    gvTrackingReport.DataSource = _objDs.Tables[0];
                    gvTrackingReport.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
 
    #endregion

    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindCorDriveTrackingReportGrid();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            //string _current_Location = "";
            DataSet _ObjNext = new DataSet();
            _objService = new RentMeterService.wsRentmeter();
            DataTable dt = new DataTable();
            int _cityId = 0, counter = 0, bookingId = 0, carId = 0;
            DateTime _fromdate, _todate;
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            DataSet _objDs = new DataSet();
            _objDs = objCorDrive.GetCorDriveTrackingReport(_cityId, _fromdate, _todate);
            if (_objDs != null)
            {
                if (_objDs.Tables[0].Rows.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 32, 22, 23, 24, 43, 40 };
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            carId = Convert.ToInt32(dt.Rows[i]["CarID"].ToString());
                            bookingId = Convert.ToInt32(dt.Rows[i]["BookingID"].ToString());
                            if (carId != 0 && bookingId != 0)
                            {
                                _ObjNext = objCorDrive.GetNextDutySlip(bookingId, carId, Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                            }
                            else
                            {
                                _ObjNext = null;
                            }

                            double _lat = Convert.ToDouble(dt.Rows[i]["Lat"].ToString());
                            double _lon = Convert.ToDouble(dt.Rows[i]["Lon"].ToString());

                            //if (dt.Rows[i]["CurrentLocation"] == "")
                            //{
                            //    if (_lat != 0 && _lon != 0)
                            //    {
                            dt.Rows[i]["CurrentLocation"] = "";// _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());
                                                               //    }
                                                               //}

                            //Driver Location start
                            // _lat = Convert.ToDouble(dt.Rows[i]["DriverStartLat"].ToString());
                            //_lon = Convert.ToDouble(dt.Rows[i]["DriverStartLon"].ToString());
                            //if (dt.Rows[i]["DriverStartLocation"].ToString() == "")
                            //{
                            //    if (_lat != 0 && _lon != 0)
                            //    {
                            dt.Rows[i]["DriverStartLocation"] = "";// _objService.UpdateAddress(_lat.ToString(), _lon.ToString(), bookingId.ToString(), "DS");
                            //    }
                            //}
                            //Driver Location end

                            //Passenger end Location start
                            _lat = Convert.ToDouble(dt.Rows[i]["PassengerEndLat"].ToString());
                            _lon = Convert.ToDouble(dt.Rows[i]["PassengerEndLon"].ToString());

                            if (dt.Rows[i]["PassengerDropoffLocation"].ToString() == "")
                            {
                                if (_lat != 0 && _lon != 0)
                                {
                                    dt.Rows[i]["PassengerDropoffLocation"] = _objService.UpdateAddress(_lat.ToString(), _lon.ToString(), bookingId.ToString(), "PE");
                                }
                            }
                            //Passenger end Location end

                            //for (int j = 0; j < dt.Rows.Count; j++)
                            //{
                            if (_ObjNext.Tables[0].Rows.Count > 0)
                            {
                                dt.Rows[i]["NextPickUpAddress"] = _ObjNext.Tables[0].Rows[0]["PickUpaddress"].ToString();
                                dt.Rows[i]["NextPickupDate"] = _ObjNext.Tables[0].Rows[0]["NextPickupDate"].ToString();
                                dt.Rows[i]["NextPickupTime"] = _ObjNext.Tables[0].Rows[0]["NextPickupTime"].ToString();
                                dt.Rows[i]["NextBookingID"] = _ObjNext.Tables[0].Rows[0]["BookingID"].ToString();

                            }
                            else
                            {
                                dt.Rows[i]["NextPickUpAddress"] = "";
                                dt.Rows[i]["NextPickupDate"] = "";
                                dt.Rows[i]["NextPickupTime"] = "";
                                dt.Rows[i]["NextBookingID"] = "";
                            }
                            //}
                        }
                        Response.ClearContent();
                        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                        Response.Clear();
                        Response.AppendHeader("content-disposition", "attachment;filename=CORDriveTrackingReport.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "application/vnd.ms-excel";
                        this.EnableViewState = false;
                        Response.Write("\r\n");
                        Response.Write("<table border = '1' align = 'center'> ");
                        // int[] iColumns = {0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,23};
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            counter = counter + 1;
                            if (i == 0)
                            {
                                Response.Write("<tr>");
                                Response.Write("<td align='left'><b>S.No.</b></td>");
                                for (int j = 0; j < iColumns.Length; j++)
                                {
                                    if (dt.Columns[iColumns[j]].Caption.ToString() == "BookingID")
                                    {
                                        Response.Write("<td align='left'><b>Booking ID</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ClientCoName")
                                    {
                                        Response.Write("<td align='left'><b>Company Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "GuestName")
                                    {
                                        Response.Write("<td align='left'><b>Guest Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "GuestMobileNo")
                                    {
                                        Response.Write("<td align='left'><b>Guest Contact</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarRegNo")
                                    {
                                        Response.Write("<td align='left'><b>Car Number</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverName")
                                    {
                                        Response.Write("<td align='left'><b>Driver Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverContract")
                                    {
                                        Response.Write("<td align='left'><b>Driver Contact</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                    {
                                        Response.Write("<td align='left'><b>City</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverStartDate")
                                    {
                                        Response.Write("<td align='left'><b>Driver Start Date</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverStartTime")
                                    {
                                        Response.Write("<td align='left'><b>Driver Start Time</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverStartLocation")
                                    {
                                        Response.Write("<td align='left'><b>Driver Start Location</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ActualPickupDate")
                                    {
                                        Response.Write("<td align='left'><b>Actual Pickup Date</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ActualPickupTime")
                                    {
                                        Response.Write("<td align='left'><b>Actual Pickup  Time</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ActualPickupLocation")
                                    {
                                        Response.Write("<td align='left'><b>Actual Pickup  Location</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "RequestedPickupdate")
                                    {
                                        Response.Write("<td align='left'><b>Requested Pickup  Date</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "RequestedPickupTime")
                                    {
                                        Response.Write("<td align='left'><b>Requested Pickup Time</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "RequestedPickLocation")
                                    {
                                        Response.Write("<td align='left'><b>Requested Pickup Location</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerEndtDate")
                                    {
                                        Response.Write("<td align='left'><b>Passenger End Date</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerEndTime")
                                    {
                                        Response.Write("<td align='left'><b>Passenger End Time</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "PassengerDropoffLocation")
                                    {
                                        Response.Write("<td align='left'><b>Passenger End Location</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "dutyslipStatus")
                                    {
                                        Response.Write("<td align='left'><b>DS Status</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "RatingPoint")
                                    {
                                        Response.Write("<td align='left'><b>Feedback Rating</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "NextPickUpAddress")
                                    {
                                        Response.Write("<td align='left'><b>Next Pick Up Address</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "NextPickupDate")
                                    {
                                        Response.Write("<td align='left'><b>Next Pick-up Date</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "NextPickupTime")
                                    {
                                        Response.Write("<td align='left'><b>Next Pick-up Time</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CorDriveVersion")
                                    {
                                        Response.Write("<td align='left'><b>CorDriveVersion</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "NextBookingid")
                                    {
                                        Response.Write("<td align='left'><b>Next Duty Booking Id</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarLocationName")
                                    {
                                        Response.Write("<td align='left'><b>Current Location</b></td>");
                                    }
                                    //else
                                    //{
                                    //    Response.Write("<td align='left'><b>Current Location</b></td>");
                                    //}
                                }
                                Response.Write("</tr>");
                            }
                            Response.Write("<tr>");
                            Response.Write("<td align='left'>" + counter + "</td>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                Response.Write("<td align='left'>" + Convert.ToString(dt.Rows[i][iColumns[j]]) + "</td>");
                            }
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");
                    Response.End();
                }
                else
                {
                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void gvTrackingReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvTrackingReport.PageIndex = e.NewPageIndex;
            BindCorDriveTrackingReportGrid();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void gvTrackingReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            DataSet _obj = new DataSet();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _lbl_BookingId = (Label)e.Row.FindControl("lbl_BookingId");
                HiddenField _hdCarid = (HiddenField)e.Row.FindControl("hdCarid");
                Label _nextPickupAddress = (Label)e.Row.FindControl("lbl_NextPickupAddress");
                Label _NextPickupDate = (Label)e.Row.FindControl("lbl_NextPickupDate");
                Label _lbl_NextPickupTime = (Label)e.Row.FindControl("lbl_NextPickupTime");
                HiddenField _lat = (HiddenField)e.Row.FindControl("hd_lat");
                HiddenField _lon = (HiddenField)e.Row.FindControl("hd_Lon");
                LinkButton _lnkbtn = (LinkButton)e.Row.FindControl("lnkView");
                Label _NextBookingId = (Label)e.Row.FindControl("lbl_NextBookingId");

                HiddenField _hdDutyStatus = (HiddenField)e.Row.FindControl("hd_DutyStatus");
                Label _actualPickupTime = (Label)e.Row.FindControl("lbl_ActualPickupTime");

                Label _driverStartLocation = (Label)e.Row.FindControl("lbl_DriverStartLocation");
                Label _passengerDropoffLocation = (Label)e.Row.FindControl("lbl_PassengerDropoffLocation");

                HiddenField hd_driverStartLocation = (HiddenField)e.Row.FindControl("hd_driverStartLocation");
                HiddenField hd_passengerDropoffLocation = (HiddenField)e.Row.FindControl("hd_passengerDropoffLocation");

                HiddenField _Dlat = (HiddenField)e.Row.FindControl("hd_Dlat");
                HiddenField _DLon = (HiddenField)e.Row.FindControl("hd_DLon");
                HiddenField _Plat = (HiddenField)e.Row.FindControl("hd_Plat");
                HiddenField _PLon = (HiddenField)e.Row.FindControl("hd_PLon");


                if (_lat != null && _lon != null)
                {
                    if (Convert.ToDouble(_lat.Value) != 0 || Convert.ToDouble(_lon.Value) != 0)
                    {
                        _lnkbtn.Enabled = true;
                        _lnkbtn.Text = "View";
                    }
                    else
                    {
                        _lnkbtn.Enabled = false;
                        _lnkbtn.Text = "";
                    }
                }
                if (_lbl_BookingId.Text != null && _hdCarid.Value != null)
                {
                    _obj = objCorDrive.GetNextDutySlip(Convert.ToInt32(_lbl_BookingId.Text), Convert.ToInt32(_hdCarid.Value), Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                    if (_obj != null)
                    {
                        if (_obj.Tables[0].Rows.Count > 0)
                        {
                            _nextPickupAddress.Text = _obj.Tables[0].Rows[0]["PickUpaddress"].ToString();
                            _NextPickupDate.Text = _obj.Tables[0].Rows[0]["NextPickupDate"].ToString();
                            _lbl_NextPickupTime.Text = _obj.Tables[0].Rows[0]["NextPickupTime"].ToString();
                            _NextBookingId.Text = _obj.Tables[0].Rows[0]["BookingId"].ToString();
                        }
                        else
                        {
                            _nextPickupAddress.Text = "";
                            _NextPickupDate.Text = "";
                            _lbl_NextPickupTime.Text = "";
                            _NextBookingId.Text = "";
                        }
                    }
                }

                //***************************************************************************************
                //For the Cell Color To Be Red 
                int _result = GetTimeDifference(_actualPickupTime.Text, _lbl_NextPickupTime.Text);

                if (_result > 60)
                {
                    if (_hdDutyStatus.Value.ToString() == "C")
                    {
                    }
                    else
                    {
                        _lbl_BookingId.BackColor = System.Drawing.Color.Red;
                        _nextPickupAddress.BackColor = System.Drawing.Color.Red;
                        _NextPickupDate.BackColor = System.Drawing.Color.Red;
                    }
                }

                //*******************************************************************************************

                //double _Clat = Convert.ToDouble(_lat.Value);
                //double _Clon = Convert.ToDouble(_lon.Value);

                //if (_Clat != 0 && _Clon != 0)
                //{
                //    dt.Rows[i]["CurrentLocation"] = _objService.GetCurrentLocation_LatLon(_Clat.ToString(), _Clon.ToString());
                //}
                //else
                //{
                //    dt.Rows[i]["CurrentLocation"] = "";
                //}

                //Driver Location start
                double _Clat = Convert.ToDouble(_Dlat.Value);
                double _Clon = Convert.ToDouble(_DLon.Value);

                if (hd_driverStartLocation.Value.ToString() == "")
                {
                    if (_Clat != 0 && _Clon != 0)
                    {
                        _driverStartLocation.Text = _objService.UpdateAddress(_Clat.ToString(), _Clon.ToString(), _lbl_BookingId.Text, "DS");
                    }
                }
                //Driver Location end

                //Passenger end Location start
                _Clat = Convert.ToDouble(_Plat.Value);
                _Clon = Convert.ToDouble(_PLon.Value);

                if (hd_passengerDropoffLocation.Value.ToString() == "")
                {
                    if (_Clat != 0 && _Clon != 0)
                    {
                        _passengerDropoffLocation.Text = _objService.UpdateAddress(_Clat.ToString(), _Clon.ToString(), _lbl_BookingId.Text, "PE");
                    }
                }                //Passenger end Location end
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }

    private int GetTimeDifference(string sDateFrom, string sDateTo)
    {
        int _ressult = 0;
        try
        {
            string _sFrom = "", _sTo1 = "", _sTo2 = "", _sto = ""; ;
            DateTime dFrom;
            DateTime dTo;
            string _sFrom1 = sDateFrom.Substring(0, 2);
            string _sFrom2 = sDateFrom.Substring(2, 2);
            _sTo1 = sDateTo.Substring(0, 2);
            _sTo2 = sDateTo.Substring(2, 2);

            _sFrom = _sFrom1 + ":" + _sFrom2;
            _sto = _sTo1 + ":" + _sTo2;
            if (DateTime.TryParse(_sFrom, out dFrom) && DateTime.TryParse(_sto, out dTo))
            {
                TimeSpan TS = dTo - dFrom;
                int hour = TS.Hours;
                int mins = TS.Minutes;
                int _Minut = 0;
                if (hour > 1)
                {
                    _Minut = 60 * hour;
                }

                _ressult = Convert.ToInt32(_Minut + mins);
            }
            return _ressult;
        }
        catch (Exception ex)
        {
            _ressult = 0;
            ex.Message.ToString();
        }
        return _ressult;
    }
}