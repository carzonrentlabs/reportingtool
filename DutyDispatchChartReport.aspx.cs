using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;

public partial class DutyDispatchChartReport : clsPageAuthorization
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
                btnSubmit.Attributes.Add("onclick", "return validate();");
                btnExport.Attributes.Add("onclick", "return validate();");
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=DutyDispatchReport.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetDutyDispatchChart(FromDate, Todate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            //int[] iColumns = { 0, 6, 1, 2, 3, 4, 7, 5 };
            int[] iColumns = { 0, 6, 1, 2, 3, 4, 5 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>Branch</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == ">120")
                        {
                            Response.Write("<td align='left'><b>2 - 4 Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == ">240")
                        {
                            Response.Write("<td align='left'><b>> 4 Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "1-2Hrs")
                        {
                            Response.Write("<td align='left'><b>1-2 Hr</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "1 Hrs")
                        {
                            Response.Write("<td align='left'><b>1 Hr</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "<30 Min")
                        {
                            Response.Write("<td align='left'><b>< 30 Min</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Booking< 2Hrs")
                        {
                            Response.Write("<td align='left'><b>Booking< 2Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Total")
                        {
                            Response.Write("<td align='left'><b>Total</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' width = '100%'>");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void grvDutyDispatch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvDutyDispatch.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetDutyDispatchChart(FromDate, Todate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            grvDutyDispatch.DataSource = GetBatchDetail;
            grvDutyDispatch.DataBind();
        }
        else
        {
            grvDutyDispatch.DataSource = GetBatchDetail;
            grvDutyDispatch.DataBind();
        }
    }
}