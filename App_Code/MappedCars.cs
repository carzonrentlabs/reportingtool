﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for MappedCars
/// </summary>
/// 
namespace ReportingTool
{
    public class MappedCars
    {
        public MappedCars()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetCityName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetCityDetails");
        }

        public DataSet GetClientName()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetClientDetails");
        }

        public DataSet GetAddMappedCars(int CityID, int ClientCoId, int Active)
        {
            SqlParameter[] myParams = new SqlParameter[3];
            myParams[0] = new SqlParameter("@CityID", SqlDbType.Int);
            myParams[0].Value = CityID;

            myParams[1] = new SqlParameter("@ClientCoId", SqlDbType.Int);
            myParams[1].Value = ClientCoId;

            myParams[2] = new SqlParameter("@Active", SqlDbType.Int);
            myParams[2].Value = Active;

            //return ImplantDAL.GetDataSet("dbo.SP_GetTollParkDetails", myParams);
            return SqlHelper.ExecuteDataset("dbo.SP_GetMappedCarsDetails", myParams);
        }

        public int SaveMappedCar(int CityID, int ClientID, int @VendorType, string RegnNo, string Remarks, string CreatedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[6];
            ObjparamUser[0] = new SqlParameter("@CityID", CityID);
            ObjparamUser[1] = new SqlParameter("@ClientID", ClientID);
            ObjparamUser[2] = new SqlParameter("@VendorType", VendorType);
            ObjparamUser[3] = new SqlParameter("@RegnNo", RegnNo);
            ObjparamUser[4] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[5] = new SqlParameter("@CreatedBy", CreatedBy);
            int status = 0;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.Bl_Sp_SaveMappedCar", ObjparamUser);
            if (ds.Tables[0].Rows.Count > 0)
            {
                status = Convert.ToInt32(ds.Tables[0].Rows[0]["ReturnID"]);
            }
            else
            {
                status = -1;
            }

            return status;
        }

        public DataSet UpdateMappedCars(int CarID, string Remarks, int Active, string ModifiedBy)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];
            ObjparamUser[0] = new SqlParameter("@CarID", CarID);
            ObjparamUser[1] = new SqlParameter("@Remarks", Remarks);
            ObjparamUser[2] = new SqlParameter("@Active", Active);
            ObjparamUser[3] = new SqlParameter("@ModifiedBy", ModifiedBy);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "dbo.bl_sp_UpdateMappedCar", ObjparamUser);
        }
    }
}