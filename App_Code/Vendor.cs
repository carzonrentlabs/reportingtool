﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using ReportingTool;
using System;

/// <summary>
/// Summary description for CorDrive
/// </summary>
public class Vendor
{
    public Vendor()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataSet GetLocations_UserAccessWise_Unit(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_Unit", ObjParam);
    }

    public DataSet GetPenaltyDetails(int cityId, DateTime FromDate, DateTime ToDate)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[3];
        ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
        ObjparamUser[1] = new SqlParameter("@Todate", ToDate);
        ObjparamUser[2] = new SqlParameter("@CitYID", cityId);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_PenaltyDetails", ObjparamUser);

    }

    public DataSet VendorDetails(int CityID,DateTime DateOn)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CityId", CityID);
        param[1] = new SqlParameter("@SelectedDate", DateOn);
        //return SqlHelper.ExecuteDataset("Prc_GetVendorCityWise_NexDay", param);
        //return SqlHelper.ExecuteDataset("Prc_GetVendorCityWise_DutyRoster", param);
        return SqlHelper.ExecuteDataset("Prc_GetVendorCityWise_DutyRoster_Month", param);
    }
    public string GetTotalWorkingCarOnDate(string dateOn, int cityId)
    {
        DataSet dsWorkingCar = new DataSet();
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@DateOn", dateOn);
        param[1] = new SqlParameter("@CityId", cityId);
        dsWorkingCar = SqlHelper.ExecuteDataset("GetWorkingVehicleOnDate", param);
        if (dsWorkingCar.Tables[0].Rows.Count > 0)
        {
            return Convert.ToString(dsWorkingCar.Tables[0].Rows[0][0]);
        }
        else
        {
            return "0";
        }
    }
}