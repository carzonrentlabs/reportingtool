<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DispositionReport.aspx.cs" 
Inherits="CorDriveReport_DispositionReport" 
Title="Disposition Call Report" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 
   <%-- <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>--%>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       $(document).ready(function () 
       {
           
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
           
            $("#<%=btnExport.ClientID%>").click(function () {
             
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            gridviewScroll();
            
            
            $("#<%=bntGet.ClientID%>").click(function () {
             
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            gridviewScroll();
       });
         
     function gridviewScroll() {
        $('#<%=gvDispositionCall.ClientID%>').gridviewScroll({
            width: 1200,
            height: 450
        });
        
//        function DateValidation() {
//        if(document.getElementById('<%=txtFromDate.ClientID%>').value <= document.getElementById('<%=txtToDate.ClientID%>').value)
//         alert("From Date Cannot Be Less Then Todate !");
//        return false;
//        });
    } 

       
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width:1200px">
            <tr>
                <td colspan="7" align="center" bgcolor="#FF6600">
                   <b>Disposition Call Report</b> </td>
            </tr>
            <tr>
                <td colspan="7" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 &nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap; text-align: right;" align="left">
                    <strong>From Date :</strong><asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                 
                </td>
                 <td style="white-space: nowrap; text-align: left;" align="right">
                 <b> To Date :</b>&nbsp;&nbsp;
                 
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox></td>
                <td style="white-space: nowrap;" align="left">
                    &nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click"/>
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click" Width="103px" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" /></td>
                
               <%-- <td style="white-space: nowrap;" align="right">
                 <b> City :</b>&nbsp;&nbsp;
                </td>--%>
               <%-- <td style="white-space: nowrap;" align="left">--%>
                 <%--
                   <asp:DropDownList ID="ddlCity" runat ="server"> 
                   </asp:DropDownList>--%>
               <%-- </td>--%>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
             <tr>
                <td colspan="5">
                 
                      <asp:GridView ID="gvDispositionCall" runat="server" AutoGenerateColumns="false" Width="100%"> 
                                 <Columns> 
                                 						

                                 		<asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                       
                                        <asp:BoundField HeaderText="DispositionName" DataField="DispositionName" /> 
                                        <asp:BoundField HeaderText="SubDispositionName" DataField="SubDispositionName" /> 
                                        <asp:BoundField HeaderText="Remarks" DataField="Remarks" /> 
                                        <asp:BoundField HeaderText="PhoneNo" DataField="PhoneNo" /> 
                                        <asp:BoundField HeaderText="ProcessType" DataField="ProcessType" />
                                         <asp:BoundField HeaderText="CreatedBy" DataField="CreatedBy" /> 
                                        <asp:BoundField HeaderText="CreateOn" DataField="CreateOn" />
                                        
                                        
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                   
                </td>
            </tr>
           
        </table>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>



