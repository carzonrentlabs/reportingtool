using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;


public partial class BookingModifyReport : System.Web.UI.Page
{
    clsReport objReports = new clsReport();


    protected void Page_Load(object sender, EventArgs e)
    {
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }

    #region ReportBind_Method
    private void BindUsageReport()
    {
        try
        {
            int _cityId = 0, _count = 0, _v_count = 0;
            DateTime _modifydate;
            _modifydate = Convert.ToDateTime(txtFromDate.Text);

            DataSet _objDs = new DataSet();
            _objDs = objReports.GetBookingModifyReport(_modifydate);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dvPaging.Visible = true;
                    _count = _objDs.Tables.Count;
                    PagedDataSource objPage_data = new PagedDataSource();
                    objPage_data.DataSource = _objDs.Tables[0].DefaultView;
                    objPage_data.AllowPaging = true;
                    objPage_data.PageSize = 100;
                    objPage_data.CurrentPageIndex = _PgNum;

                    _v_count = _count / objPage_data.PageSize;
                    if (_PgNum < 1)
                    {
                        lnkPrev.Visible = false;
                    }
                    else if (_PgNum > 0)
                    {
                        lnkPrev.Visible = true;
                    }
                    else if (_PgNum < _v_count)
                    {
                        lnkNext.Visible = true;
                    }
                    rptVendorUsageReport.DataSource = objPage_data;
                    rptVendorUsageReport.DataBind();
                }
                else
                {
                    dvPaging.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    #endregion
    protected void lnkPrev_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum -= 1;
            BindUsageReport();

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum += 1;
            BindUsageReport();

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    public int _PgNum
    {
        get
        {
            if (ViewState["PageNum"] != null)
            {
                return Convert.ToInt32(ViewState["PageNum"].ToString());
            }
            else
            {
                return 0;
            }
        }
        set
        {
            ViewState["PageNum"] = value;
        }
    }
    #region Button_Click_Code
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindUsageReport();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    #endregion


    #region Excel_Report
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime _Modifydate;
            DataTable dt = new DataTable();
            int firstBookingID = 0, secondBooking = 0;
            string colorSet1 = "FFFF99", colorSet2 = "CCFFFF", colorSet3 = string.Empty;
            _Modifydate = Convert.ToDateTime(txtFromDate.Text);
            DataSet _objDs = new DataSet();
            _objDs = objReports.GetBookingModifyReport(_Modifydate);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
            }
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=BookingModifyReport.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "BookingID")
                                {
                                    Response.Write("<td align='left'><b>Booking ID</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                {
                                    Response.Write("<td align='left'><b>City Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CarRequested")
                                {
                                    Response.Write("<td align='left'><b>Car Requested</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "PickUpDate")
                                {
                                    Response.Write("<td align='left'><b>PickUp Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "PickUpTime")
                                {
                                    Response.Write("<td align='left'><b>PickUp Time</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "DropOffDate")
                                {
                                    Response.Write("<td align='left'><b>DropOff Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "DropOffTime")
                                {
                                    Response.Write("<td align='left'><b>DropOff Time</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "PickUpAdd")
                                {
                                    Response.Write("<td align='left'><b>PickUp Add</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "FlgtNo")
                                {
                                    Response.Write("<td align='left'><b>Flgt No</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ReportContact")
                                {
                                    Response.Write("<td align='left'><b>Report Contact</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CancelReason")
                                {
                                    Response.Write("<td align='left'><b>Cancel Reason</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "Remarks")
                                {
                                    Response.Write("<td align='left'><b>Remarks</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "VisitedCities")
                                {
                                    Response.Write("<td align='left'><b>Visited Cities</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "trackId")
                                {
                                    Response.Write("<td align='left'><b>Track Id</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CreatedBy")
                                {
                                    Response.Write("<td align='left'><b>Created By</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "Createdate")
                                {
                                    Response.Write("<td align='left'><b>Create Date</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ModifyedName")
                                {
                                    Response.Write("<td align='left'><b>Modified Name</b></td>");
                                }
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ModifyDate")
                                {
                                    Response.Write("<td align='left'><b>Modify Date</b></td>");
                                }
                            }
                            Response.Write("</tr>");
                        }
                        #region Excel_ColorCode
                        if (i == 0)
                        {
                            firstBookingID = int.Parse(dt.Rows[i]["BookingID"].ToString());
                        }
                        else
                        {
                            secondBooking = firstBookingID;
                            firstBookingID = int.Parse(dt.Rows[i]["BookingID"].ToString());
                        }
                        if (i == 0 && secondBooking == 0)
                        {
                            Response.Write("<tr bgcolor='" + colorSet1 + "'>");
                        }
                        else if (firstBookingID == secondBooking)
                        {
                            Response.Write("<tr bgcolor='" + colorSet1 + "'>");
                        }
                        else
                        {
                            Response.Write("<tr bgcolor='" + colorSet2 + "'>");
                            colorSet3 = colorSet1;
                            colorSet1 = colorSet2;
                            colorSet2 = colorSet3;
                        }
                        #endregion

                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");
                    Response.End();
                }
                else
                {
                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    #endregion
}