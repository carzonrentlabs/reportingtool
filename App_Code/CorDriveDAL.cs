﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using ReportingTool;

/// <summary>
/// Summary description for CorDriveDAL
/// </summary>
public class CorDriveDAL
{
	public CorDriveDAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet GetBookingWithoutLatLon( DateTime _FromDate, DateTime _ToDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];

        ObjParam[0] = new SqlParameter("@Fromdate", SqlDbType.DateTime);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = _FromDate;

        ObjParam[1] = new SqlParameter("@Todate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = _ToDate;

        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetBookingWithoutLatLon", ObjParam);
    }

    public DataSet GetRidePin(int BookingID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];

        ObjParam[0] = new SqlParameter("@BookingId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = BookingID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetRidePinDetails", ObjParam);
    }
    public int UpdateManualSendRidePin(int sysUserId,int bookingId)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];

        ObjParam[0] = new SqlParameter("@SysUserId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = sysUserId;

        ObjParam[1] = new SqlParameter("@BookingId", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = bookingId;
        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateRideSendBy", ObjParam);
    }

    public int UpdatePlatPlon(int bookingId, float plat, float plon, int sysUserId,string bookingLocation)
    {
        SqlParameter[] ObjParam = new SqlParameter[5];

        ObjParam[0] = new SqlParameter("@BookingId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = bookingId;

        ObjParam[1] = new SqlParameter("@Plat", SqlDbType.Float);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = plat;

        ObjParam[2] = new SqlParameter("@Plon", SqlDbType.Float);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = plon;

        ObjParam[3] = new SqlParameter("@SysuserId", SqlDbType.Int);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = sysUserId;

        ObjParam[4] = new SqlParameter("@bookingLocation", SqlDbType.VarChar);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = bookingLocation;

        return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_UpdateLatLonBookingID", ObjParam);
    
    }
}