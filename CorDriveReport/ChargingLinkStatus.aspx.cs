﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class CorDriveReport_ChargingLinkStatus : System.Web.UI.Page
{
    private CallTaker objCallTaker = null;
    DataSet dsPendingForApproval = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
        Session["UserID"] = 3122;
        if (!IsPostBack)
        {
            BindAllCity();
            GetPendingForApprovalDetails();
            //txtLinkSendDate.Text = System.DateTime.Now.ToShortDateString();
        }
    }
    private void BindAllCity()
    {
        try
        {
            objCallTaker = new CallTaker();
            DataSet dsLocation = new DataSet();
            dsLocation = objCallTaker.GetAllCityName();
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                    ddlCity.Items.Insert(0, new ListItem("--All--", "0"));
                }
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCallTaker = new CallTaker();
        try
        {

            dsPendingForApproval = objCallTaker.GetPendingForApprovalDetailsCritereaWise(Convert.ToDateTime(txtLinkSendDate.Text), Convert.ToInt32(ddlCity.SelectedValue), Convert.ToInt32(ddlStatus.SelectedValue));
            if (dsPendingForApproval.Tables[0].Rows.Count > 0)
            {
                gvPendingForApproval.DataSource = dsPendingForApproval.Tables[0];
                gvPendingForApproval.DataBind();

            }
            else
            {
                gvPendingForApproval.DataSource = null;
                gvPendingForApproval.DataBind();
                lblMsg.Visible = true;
                lblMsg.ForeColor = System.Drawing.Color.Red;
                lblMsg.Text = "No Data Found";
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
    public void GetPendingForApprovalDetails()
    {
        objCallTaker = new CallTaker();
        try
        {

            dsPendingForApproval = objCallTaker.GetPendingForApprovalDetails();
            if (dsPendingForApproval.Tables[0].Rows.Count > 0)
            {
                gvPendingForApproval.DataSource = dsPendingForApproval.Tables[0];
                gvPendingForApproval.DataBind();

            }
            else
            {
                gvPendingForApproval.DataSource = null;
                gvPendingForApproval.DataBind();
                lblMsg.Visible = true;
                lblMsg.ForeColor = System.Drawing.Color.Red;
                lblMsg.Text = "No Data Found";
            }
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
    protected void rdbCreatePickupDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(rdbCreatePickupDate.SelectedValue=="0")
        {
            GetPendingForApprovalDetails();
            trAll.Visible = false;
            trCriteria.Visible = false;
        }
        else if(rdbCreatePickupDate.SelectedValue=="1")
        {
            trAll.Visible = true;
            trCriteria.Visible = true;
            gvPendingForApproval.DataSource = null;
            gvPendingForApproval.DataBind();
        }
        
    }
}