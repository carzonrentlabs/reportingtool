﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CORDriveLoginHoursSummary.aspx.cs" Inherits="CorDriveReport_CORDriveLoginHoursSummary" %>

<asp:Content ID="HoursSummary" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <link href="../greybox/gb_styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() 
        {
            var dateFrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
            var dateTo = document.getElementById('<%=txtToDate.ClientID %>').value;
            //alert(dateFrom)
           // alert(dateTo)
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }
           else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
            else if (parseInt(DateDiff(dateFrom, dateTo)) > 31)
            {
              alert("Date difference should not be greater than 31 days");
              return false;
            }

        }
        function DateDiff(date1, date2) {
           
//            var day = date1.getDate();
//            var month = date1.getMonth() + 1;
//            var year = date1.getFullYear();
//              var firstDate = day + "/" + month + "/" + year ;
            var firstDate = new Date(date1)

//            var day2 = date2.getDate();
//            var month2 = date2.getMonth() + 1;
//            var year2 = date2.getFullYear();
//            var secondDate = day2 + "/" + month2 + "/" + year2;
            var secondDate = new Date(date2)

            var datediff = secondDate.getTime() - firstDate.getTime();
            return (datediff / (24 * 60 * 60 * 1000));
        }
    </script>
    <div>
        <table cellpadding="0" cellspacing="0" border="0" align="center">
            <tr>
                <td colspan="12" align="center">
                    <b>COR Drive Login Hours Summary</b>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>City Name</b>&nbsp;
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlCity" runat="server">
                    </asp:DropDownList>
                    &nbsp;
                </td>
                <td align="left">
                    <b>From Date</b>&nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="txtFromDate" Width="100Px" runat="server"></asp:TextBox>
                    &nbsp;
                </td>
                <td align="left">
                    <b>To Date</b>&nbsp;
                </td>
                <td align="left">
                    <asp:TextBox ID="txtToDate" Width="100Px" runat="server"></asp:TextBox>
                    &nbsp;
                </td>
                <td align="left">
                    &nbsp;<b>Attachment Model</b>&nbsp;
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlCarModel" runat="server">
                    </asp:DropDownList>
                    &nbsp;
                </td>
                <td align="left">
                    <b>VDP (Y/N)</b>&nbsp;
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlVdpYN" runat="server">
                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                        <asp:ListItem Value="2" Text="No"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                </td>
                <td align="left">
                    <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                </td>
                <td align="left">
                    <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="12">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <center>
        <div style="width: 1300Px; height: 380Px; overflow: auto;">
            <asp:Literal ID="ltlSummary" runat="server"></asp:Literal>
        </div>
    </center>
</asp:Content>
