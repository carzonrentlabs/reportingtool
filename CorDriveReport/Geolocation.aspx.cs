using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;
public partial class CorDriveReport_Geolocation_ : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsGeoExportToExcel = new DataSet();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsGeoExportToExcel = objCordrive.GetGeolocationReportExportToExcel(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
            WorkbookEngine.ExportDataSetToExcel(dsGeoExportToExcel, "GeolocationReport.xls");
        }
    }
}
