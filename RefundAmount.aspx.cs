﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class RefundAmount : System.Web.UI.Page
{
    DataSet ds = new DataSet();
    CorDrive objCoreDrive = new CorDrive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lblMessage.Text = "";
            btnRefund.Enabled = false;
        }
        lblMessage.Text = "";
    }

    protected void btnGetDetails_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtBookingId.Text != "")
            {
                ds = objCoreDrive.GetRefundedAmountDetails(txtBookingId.Text);
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                    grdRefundAmountDetails.DataSource = ds;
                    grdRefundAmountDetails.DataBind();
                //    lblCompanyName.Text = "";
                //    lblGueastName.Text = "";
                //    lblPickupdate.Text = "";
                //    lblPickupTime.Text = "";
                //    lblServiceTax.Text = "";
                //    lblBasic.Text = "";
                //    lblTotalCost.Text = "";
                //}
                //else
                //{
                //    grdRefundAmountDetails.DataSource = null;
                //    grdRefundAmountDetails.DataBind();
                    ds = objCoreDrive.GetRefundBookingDetails(txtBookingId.Text);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        btnRefund.Enabled = true;

                        lblCompanyName.Text = ds.Tables[0].Rows[0]["ClientCoName"].ToString();
                        lblGueastName.Text = ds.Tables[0].Rows[0]["GuestName"].ToString();
                        lblPickupdate.Text = ds.Tables[0].Rows[0]["Pickupdate"].ToString();
                        lblPickupTime.Text = ds.Tables[0].Rows[0]["PickUpTime"].ToString();
                        lblServiceTax.Text = ds.Tables[0].Rows[0]["ServiceTaxAmt"].ToString();
                        lblBasic.Text = ds.Tables[0].Rows[0]["Basic"].ToString();
                        txtTotalCost.Text = ds.Tables[0].Rows[0]["TotalCost"].ToString();
                    }
                    else
                    {
                        lblMessage.Text = "No record found";
                        lblCompanyName.Text = "";
                        lblGueastName.Text = "";
                        lblPickupdate.Text = "";
                        lblPickupTime.Text = "";
                        lblServiceTax.Text = "";
                        lblBasic.Text = "";
                        txtTotalCost.Text = "";
                    }
                //}
            }

            else
            {
                lblMessage.Text = "Please enter booking id";
            }
        }

        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
        }
    }

    protected void btnRefund_Click(object sender, EventArgs e)
    {
        try
        {
            //ChargingWebService.Service obj = new ChargingWebService.Service();
            NewChargingWebService.Service obj = new NewChargingWebService.Service();

            lblMessage.Text = obj.RefundProcess(Convert.ToInt32(txtBookingId.Text), Convert.ToDouble(txtRefundAmount.Text)
                , Convert.ToDouble(txtTotalCost.Text));
        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;

        }
    }
}