﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ReportingTool;

public partial class AverageRevenue : System.Web.UI.Page
{
    #region VariableDeclaration
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    clsAdmin objAdmin = new clsAdmin();
    AvgRevenue objAverageRevenue = new AvgRevenue();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        try
        {
            if (!Page.IsPostBack)
            {
                BindUnit();
                BindFromMonth();
                BindToMonth();
                BindYearFrom();
                BindYearTo();

            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;

        }

    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        DataTable dtRevenueDetail = new DataTable();
        lblMessage.Visible = false;
        try
        {
            dtRevenueDetail = objAverageRevenue.GetAverageRevenue(Convert.ToInt32(ddlFromYear.SelectedValue.ToString()), Convert.ToInt32(ddFromMonth.SelectedValue.ToString()), Convert.ToInt32(ddlToYear.SelectedValue.ToString()), Convert.ToInt32(ddToMonth.SelectedValue.ToString()), Convert.ToInt32(ddlCityName.SelectedValue.ToString()));
            if (dtRevenueDetail.Rows.Count > 0)
            {
                grvRevenuSummary.DataSource = dtRevenueDetail;
                grvRevenuSummary.DataBind();
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {

        DataTable dtRevenueDetail = new DataTable();
        dtRevenueDetail = objAverageRevenue.GetAverageRevenue(Convert.ToInt32(ddlFromYear.SelectedValue.ToString()), Convert.ToInt32(ddFromMonth.SelectedValue.ToString()), Convert.ToInt32(ddlToYear.SelectedValue.ToString()), Convert.ToInt32(ddToMonth.SelectedValue.ToString()), Convert.ToInt32(ddlCityName.SelectedValue.ToString()));
        //-------------------
        string attach = "attachment;filename=Report.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/ms-excel";
        HttpContext context = HttpContext.Current;
        context.Response.Clear();
        if (dtRevenueDetail != null)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<tr>");
            foreach (DataColumn dc in dtRevenueDetail.Columns)
            {
                if (dc.ColumnName == "ID")
                {
                    Response.Write("<td><b>S.No.</b></td>");
                }
                else if (dc.ColumnName == "ClientName")
                {
                    Response.Write("<td><b>Client Name</b></td>");
                }
                else if (dc.ColumnName == "RelationManager")
                {
                    Response.Write("<td><b>Relation Manager</b></td>");
                }
                else if (dc.ColumnName == "paymentTerms")
                {
                    Response.Write("<td><b>Payment Mode</b></td>");
                }
                else if (dc.ColumnName == "TotalDuties")
                {
                    Response.Write("<td><b>Total Duties</b></td>");
                }
                else if (dc.ColumnName == "Revenue")
                {
                    Response.Write("<td><b>Total Revenue</b></td>");
                }
                else if (dc.ColumnName == "AVGTrans")
                {
                    Response.Write("<td><b>Avg TRX</b></td>");
                }
                else if (dc.ColumnName == "AvgTicket")
                {
                    Response.Write("<td><b>Avg Tkt</b></td>");
                }
                else if (dc.ColumnName == "AVGRevenue")
                {
                    Response.Write("<td><b>Avg Rev</b></td>");
                }
                else
                {
                    Response.Write("<td><b>" + dc.ColumnName + "</b></td>");
                }
            }
            Response.Write("</tr>");
            foreach (DataRow dr in dtRevenueDetail.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtRevenueDetail.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table> ");
            Response.End();
        }

    }


    #region Helper Function
    private void BindUnit()
    {
        try
        {
            DataTable dtUnit = new DataTable();
            DataSet dsLocation = new DataSet();
            dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
            ddlCityName.DataSource = dsLocation;
            ddlCityName.DataTextField = "CityName";
            ddlCityName.DataValueField = "CityID";
            ddlCityName.DataBind();

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }
    private void BindFromMonth()
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddFromMonth.DataTextField = "monthName";
            ddFromMonth.DataValueField = "number";
            ddFromMonth.DataSource = dtFromMonth;
            ddFromMonth.DataBind();
            ddFromMonth.Items.Insert(0, "--Select--");
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }


    }
    private void BindToMonth()
    {
        try
        {
            DataTable dtToMonth = new DataTable();
            dtToMonth = objFleetTrackerSummary.GetMonthName();
            ddToMonth.DataTextField = "monthName";
            ddToMonth.DataValueField = "number";
            ddToMonth.DataSource = dtToMonth;
            ddToMonth.DataBind();
            ddToMonth.Items.Insert(0, "--Select--");
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }

    }
    private void BindYearFrom()
    {
        Dictionary<int, int> fromYear = new Dictionary<int, int>();
        try
        {
            fromYear = BindYear();
            if (fromYear != null)
            {
                ddlFromYear.DataSource = fromYear;
                ddlFromYear.DataTextField = "Value";
                ddlFromYear.DataValueField = "Key";
                ddlFromYear.DataBind();
                ddlFromYear.Items.Insert(0, "--Select--");
            }


        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }
    private void BindYearTo()
    {
        Dictionary<int, int> toYear = new Dictionary<int, int>();
        try
        {
            toYear = BindYear();
            if (toYear != null)
            {

                ddlToYear.DataSource = toYear;
                ddlToYear.DataTextField = "Value";
                ddlToYear.DataValueField = "Key";
                ddlToYear.DataBind();
                ddlToYear.Items.Insert(0, "--Select--");
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
    }

    private Dictionary<int, int> BindYear()
    {
        Dictionary<int, int> ddlYear = new Dictionary<int, int>();
        try
        {

            int monthStartYear = DateTime.Now.Year;
            for (int years = Convert.ToInt32(monthStartYear) - 1; years <= Convert.ToInt32(monthStartYear) + 1; years++)
            {
                ddlYear.Add(years, years);
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return ddlYear;
    }

    #endregion


}