﻿<%@ Page Title="Pending For Payment" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WaitingForPayment.aspx.cs" Inherits="CorDriveReport_WaitingForPayment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $("#<%=txtCreatePickUpDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtCreatePickUpDate.ClientID%>').value == "") {
                    alert("Please Select the Create/Pickup Date");
                    document.getElementById('<%=txtCreatePickUpDate.ClientID%>').focus();
                    return false;
                }

//                else {
//                    ShowProgress();
//                }
            });
        });
//        function ShowProgress() {
//            setTimeout(function () {
//                var modal = $('<div />');
//                modal.addClass("modal");
//                $('body').append(modal);
//                var loading = $(".loading");
//                loading.show();
//                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
//                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
//                loading.css({ top: top, left: left });
//            }, 200);
//        }
//        function ShowProgressKill() {
//            setTimeout(function () {
//                var modal = $('<div />');
//                modal.removeClass("modal");
//                $('body').remove(modal);
//                loading.hide();
//                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
//                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
//                loading.css({ top: top, left: left });
//            }, 200);
//        }

    </script>
    <script language="javascript">
        function confirmResend() {
            if (confirm("Are you sure want to Resend Link?") == true)
                return true;
            else
                return false;
        }
    </script>
 <table cellpadding="0" cellspacing="0" border="1" style="width: 100%">
             <tr>
                <td colspan="3" align="center" bgcolor="#FF6600">
                   <b>Track bookings Waiting for Payment</b> 
                 </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="False" Font-Bold="True" ForeColor="Red"></asp:Label>
                     <br />
                </td>
            </tr>
             <tr>
                <td colspan="3" align="center">
                                    
                    <asp:RadioButtonList ID="rdbCreatePickupDate" runat="server"  RepeatDirection="Horizontal">
                         <asp:ListItem  Value="0" Selected="True">Create Date</asp:ListItem>
                        <asp:ListItem  Value="1">PickUp Date</asp:ListItem>
                      
                    </asp:RadioButtonList>
                  <br />  
                 </td>
                   
            </tr>
             <tr>
             <td  colspan="3" align="center">
            
                <table cellpadding="0" cellspacing="0" border="1" style="width: 100%">
                                  
             <tr>
            
                <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Create/Pickup Date </b>
                 </td>
                 <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Choose Client</b>
                 </td>
                  <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Choose City </b>
                 </td>
                <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                   
                 </td>
                <%-- <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                   
                 </td>--%>
             </tr>
              <tr>
                <td style="white-space: nowrap;" align="center">
                   <asp:TextBox ID="txtCreatePickUpDate" runat="server"></asp:TextBox>
                </td>
                <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlClient" runat="server">
                    <%-- <asp:ListItem>--Select Car Category--</asp:ListItem>--%>
                  </asp:DropDownList>
                 </td>
               <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlCity" runat="server">
                    <%--  <asp:ListItem>--Select CorDrive Status--</asp:ListItem>--%>
                  </asp:DropDownList>
                </td>
                   <td style="white-space: nowrap;" align="center">
                   <asp:Button ID="bntGet" runat="server" Text="Search" Font-Bold="True" OnClick="bntGet_Click"/>
                 </td>
              <%--  <td style="white-space: nowrap;" align="center" colspan="1">
                 
                 </td>--%>
             </tr>
             <tr>
                <td colspan="4">
                 

            <div style="width: 100%; height:100%; overflow: scroll">
                      <asp:GridView ID="gvWaitingForPayment" runat="server" 
                          AutoGenerateColumns="false" Width="80%" 
                          onrowcommand="gvWaitingForPayment_RowCommand"> 
                                 <Columns> 
    																	
	                                                            
                                        <asp:BoundField HeaderText="BookingId" DataField="BookingId" /> 
                                        <asp:BoundField HeaderText="CityName" DataField="CityName" /> 
                                        <asp:BoundField HeaderText="PickUpDateTime" DataField="PickUpDateTime" /> 

                                        <asp:BoundField HeaderText="CarType" DataField="CarType" /> 
                                        <asp:BoundField HeaderText="Service" DataField="Service" /> 
                                        <asp:BoundField HeaderText="Client" DataField="Client"/> 
                                                                           
                                        <asp:BoundField HeaderText="GuestDetails" DataField="GuestDetails"/> 


                                        <asp:BoundField HeaderText="FacilitatorDetails" DataField="FacilitatorDetails" /> 
                                        <asp:BoundField HeaderText="EstimatedPrice" DataField="EstimatedPrice" /> 
                                        <asp:BoundField HeaderText="CreatedAt" DataField="CreatedAt" /> 
                                         
                                        <asp:BoundField HeaderText="CreatedBY" DataField="CreatedBY" /> 
                                        <asp:BoundField HeaderText="OriginCode" DataField="OriginCode" />  
                                                                        
                                      <asp:TemplateField HeaderText="Resend Payment Link"  >
                    <ItemTemplate >
                    <table border ="0" cellpadding ="0" cellspacing ="0" width ="100%" >  
                    <tr>
                     <td class="content" style="width: 11%; height: 34px" valign="middle" >
                     <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="<%#Container.DataItemIndex %>"  commandname="Select" OnClientClick="return confirm('Are you sure you want to Resend Link?');">Resend </asp:LinkButton>     
                       <%--  <asp:ImageButton ID="btnDelete" runat="server" CssClass="btnStyle" Height="13" ImageUrl="images/remove.gif" />--%>
                          <%--<asp:LinkButton ID="lnkDelete" runat="server"  CommandArgument='<%# Eval("ID") %>'  CommandName="Delete" CssClass="content">Delete</asp:LinkButton>--%>
                          <%--<asp:LinkButton ID="lnkDelete" runat="server"  CommandArgument='<%# Eval("BookingId") %>'  CommandName="Delete" CssClass="content">Resend</asp:LinkButton>--%>
                         <%-- <asp:LinkButton ID="LinkButton1" runat="server"  CommandArgument='<%# Eval("enrollment") %>'  CommandName="Delete" CssClass="content">Delete</asp:LinkButton>--%>
                            <%--  <asp:buttonfield buttontype="Button" 
            commandname="Select"
            headertext="Select Customer" 
            text="Select"/>--%>
                          </td></tr>
                        </table>  
                    </ItemTemplate>
                    </asp:TemplateField>
                                    </Columns>
                                 
                                                                   
                     </asp:GridView>
                   </div>
                </td>
            </tr>
             </table>
             </td>
             </tr>
         
        </table>
</asp:Content>

