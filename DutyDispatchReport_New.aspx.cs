using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;

public partial class DutyDispatchReport_New : clsPageAuthorization
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
                btnSubmit.Attributes.Add("onclick", "return validate();");
                btnExport.Attributes.Add("onclick", "return validate();");
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=ManualDutyslipReport.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetDutyDispatchNew(FromDate, Todate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = '1' align = 'center'> ");
            int[] iColumns = { 0, 1, 2, 3, 4, 5 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    Response.Write("<td align='left'><b></b></td>");
                    Response.Write("<td align='center' colspan='5'><b>Duty Slip Created Post Dispatch</b></td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>Branch</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "24Hrs")
                        {
                            Response.Write("<td align='left'><b>&lt; 24 Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "48Hrs")
                        {
                            Response.Write("<td align='left'><b>&lt; 48 Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "96Hrs")
                        {
                            Response.Write("<td align='left'><b>&lt; 96 Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == ">96Hrs")
                        {
                            Response.Write("<td align='left'><b>&gt; 96 Hrs</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Total")
                        {
                            Response.Write("<td align='left'><b>Total</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' width = '100%'>");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void grvDutyDispatch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvDutyDispatch.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetDutyDispatchNew(FromDate, Todate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            grvDutyDispatch.DataSource = GetBatchDetail;
            grvDutyDispatch.DataBind();
        }
        else
        {
            grvDutyDispatch.DataSource = GetBatchDetail;
            grvDutyDispatch.DataBind();
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();

            HeaderCell.ColumnSpan = 1;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 6;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "Duty Slip Created Post Dispatch";
            HeaderGridRow.Cells.Add(HeaderCell);
            grvDutyDispatch.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string temp = grvDutyDispatch.DataKeys[e.Row.RowIndex].Values["cityName"].ToString();
            if (temp.ToString().Trim() == "Grand Total")
            {
                Button bntHide = (Button)e.Row.FindControl("bntExport");
                bntHide.Visible = false;

            }
        }
    }

    protected void grvDutyDispatch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Export")
        {
            string fromdate = txtFrom.Value.ToString();
            string todate = txtTo.Value.ToString();
            Button bntExport = (Button)e.CommandSource;
            GridViewRow rv = (GridViewRow)bntExport.NamingContainer;
            string location = grvDutyDispatch.DataKeys[rv.RowIndex].Values["cityId"].ToString();
            DataTable dtExportDetail = new DataTable();
            dtExportDetail = objAdmin.ManualDutyDispatchDetails(fromdate, todate, location);
            if (fromdate != "" && todate != "")
            {
                DutyDispatchDetails(dtExportDetail);
            }
            else
            {
                //txtMessage.Visible = true;
                //txtMessage.Text = "Select date";
                //txtMessage.ForeColor = Color.Red;
            }
        }

    }
    private void DutyDispatchDetails(DataTable dtExportDetail)
    {

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=ManualDispatch.xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        this.EnableViewState = false;
        Response.Write("\r\n");
        if (dtExportDetail.Rows.Count > 0)
        {
            Response.Write("<table border = '1' align = 'center'> ");
            int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
            for (int i = 0; i < dtExportDetail.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "cityname")
                        {
                            Response.Write("<td align='left'><b>Location</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "bookingid")
                        {
                            Response.Write("<td align='left'><b>Booking Id</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "ClientCoName")
                        {
                            Response.Write("<td align='left'><b>Client Name</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "Guestname")
                        {
                            Response.Write("<td align='left'><b>Guest Date</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "CarNo")
                        {
                            Response.Write("<td align='left'><b>Car No.</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                        {
                            Response.Write("<td align='left'><b>Vendor Name</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "createdate")
                        {
                            Response.Write("<td align='left'><b>Creation Date</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "TimeOut")
                        {
                            Response.Write("<td align='left'><b>Time Out</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "CreatedBy")
                        {
                            Response.Write("<td align='left'><b>Created By</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "DtDiff")
                        {
                            Response.Write("<td align='left'><b>Time Span</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "OutStation")
                        {
                            Response.Write("<td align='left'><b>OutStation Y/N</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "ContinuousBooking")
                        {
                            Response.Write("<td align='left'><b>Continuous Y/N</b></td>");
                        }

                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtExportDetail.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' width = '100%'>");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }

    }
    protected void bntGetAll_Click(object sender, EventArgs e)
    {
        string fromdate = txtFrom.Value.ToString();
        string todate = txtTo.Value.ToString();
        string location = ddlCityName.SelectedValue.ToString();
        DataTable dtExportDetail = new DataTable();
        dtExportDetail = objAdmin.ManualDutyDispatchDetails(fromdate, todate,location);
        if (fromdate != "" && todate != "")
        {
            DutyDispatchDetails(dtExportDetail);
        }
    }
}