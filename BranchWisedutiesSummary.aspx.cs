using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class BranchWisedutiesSummary : clsPageAuthorization
//public partial class BranchWisedutiesSummary : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=BranchWiseDutiesSummary.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetBranchWiseDutiesSummary(FromDate, Todate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 0, 1, 2, 3, 7, 4, 5, 6, 8, 9, 5, 12, 14, 15, 13, 17, 18, 16 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    Response.Write("<td align='left'><b></b></td>");
                    Response.Write("<td align='center' colspan='4'><b>Direct</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>BTC older than 48 hours</b></td>");
                    Response.Write("<td align='center' colspan='2'><b>BTC older than 96 hours</b></td>");
                    //Response.Write("<td align='center' colspan='2'><b>Branch</b></td>");
                    Response.Write("<td align='center' colspan='3'><b>SSC</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>Branch</b></td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>City Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DirectOpenDuties")
                        {
                            Response.Write("<td align='left'><b>Open Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DirectPendingDuties")
                        {
                            Response.Write("<td align='left'><b>Pending Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DirectClosedDuties")
                        {
                            Response.Write("<td align='left'><b>Closed Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToOpenDuties")
                        {
                            Response.Write("<td align='left'><b>Open Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToPendingDuties")
                        {
                            Response.Write("<td align='left'><b>Pending Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToClosedDuties")
                        {
                            Response.Write("<td align='left'><b>Closed Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DirectTotal")
                        {
                            Response.Write("<td align='left'><b>Total</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToTotal")
                        {
                            Response.Write("<td align='left'><b>Total</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToOpenDuties4")
                        {
                            Response.Write("<td align='left'><b>Open Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BillToTotal4")
                        {
                            Response.Write("<td align='left'><b>Total</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SSCBatch")
                        {
                            Response.Write("<td align='left'><b>DS Sent To SSC</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSubmittedtoSSC")
                        {
                            Response.Write("<td align='left'><b>DS submitted directly by branches</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PrevMonth")
                        {
                            Response.Write("<td align='left'><b>DS Not Sent To SSC (Prior to Current Month)</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CCSBatch")
                        {
                            Response.Write("<td align='left'><b>DS Submitted</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DSNotSubmitted")
                        {
                            Response.Write("<td align='left'><b>DS Not Submitted</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CCSBatchCreated_CurrMonth")
                        {
                            Response.Write("<td align='left'><b>CCS Batch created by Branches</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CCSBatchNotCreated_CurrMonth")
                        {
                            Response.Write("<td align='left'><b>CCS Batch Not created by branches</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetBranchWiseDutiesSummary(FromDate, Todate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();

            HeaderCell.ColumnSpan = 1;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "Direct";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "BTC older than 48 hours";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "BTC older than 96 hours";
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "SSC";
            HeaderCell.ColumnSpan = 3;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Branch";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string RowIndex, CityName;
        int CityID;
        string Todate, FromDate;

        RowIndex = e.NewEditIndex.ToString();
        GridViewRow row = GridView1.Rows[e.NewEditIndex];
        CityName = ((Label)(row.FindControl("lblCityName"))).Text.ToString();
        CityID = Convert.ToInt32(((Label)(row.FindControl("lbl_CityID"))).Text);

        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "window.open('ClientWiseSubmissionSummary.aspx?CityName=" + CityName + "&CityID=" + CityID + "&FromDate=" + FromDate + "&Todate=" + Todate + "','window','HEIGHT=500,WIDTH=800,top=50,left=50,toolbar=no,scrollbars=yes,resizable=no')", true);
    }
}