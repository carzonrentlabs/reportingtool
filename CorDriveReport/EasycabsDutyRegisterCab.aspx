<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EasycabsDutyRegisterCab.aspx.cs" Inherits="CorDriveReport_EasycabsDutyRegisterCab" Title="Easycabs Duty Register Cab" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 
   <%-- <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>--%>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       $(document).ready(function () 
       {
           
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
           
            $("#<%=btnExport.ClientID%>").click(function () {
             
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            gridviewScroll();
       });
         
     function gridviewScroll() {
        $('#<%=gvEasycabsDutyRegister.ClientID%>').gridviewScroll({
            width: 1200,
            height: 450
        });
    } 

       
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width:1200px">
            <tr>
                <td colspan="8" align="center" bgcolor="#FF6600">
                   <b>Easycabs Duty Register Cab</b> </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b> From Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                 
                </td>
                 <td style="white-space: nowrap;" align="right">
                 <b> To Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                 
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </td>
                
               <%-- <td style="white-space: nowrap;" align="right">
                 <b> City :</b>&nbsp;&nbsp;
                </td>--%>
               <%-- <td style="white-space: nowrap;" align="left">--%>
                 <%--
                   <asp:DropDownList ID="ddlCity" runat ="server"> 
                   </asp:DropDownList>--%>
               <%-- </td>--%>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
             <tr>
                <td colspan="6">
                 
                      <asp:GridView ID="gvEasycabsDutyRegister" runat="server" AutoGenerateColumns="false" Width="100%"> 
                                 <Columns> 
                                 						

                                 		<asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                       
                                        <asp:BoundField HeaderText="EasycabsBookingId" DataField="EasyCabsBookingId" /> 
                                        <asp:BoundField HeaderText="PickUpDateTime" DataField="PickUpDateTime" /> 
                                        <asp:BoundField HeaderText="DriverName" DataField="DriverName" /> 
                                        <asp:BoundField HeaderText="BidPassNo" DataField="BidPassNo" /> 
                                        <asp:BoundField HeaderText="BidAwardRule" DataField="BidAwardRule" /> 
                                        <asp:BoundField HeaderText="CallTaker" DataField="CallTaker" /> 
                                        <asp:BoundField HeaderText="CarPref" DataField="CarPref" />
                                        <asp:BoundField HeaderText="GuestName" DataField="GuestName" />
                                        <asp:BoundField HeaderText="Phone1" DataField="Phone1" />
                                        <asp:BoundField HeaderText="EmailID" DataField="EmailID" />
                                        <asp:BoundField HeaderText="PickupGeolocation" DataField="PickupGeolocation" />
                                        <asp:BoundField HeaderText="DropoffGeolocation" DataField="DropoffGeolocation" />
                                        <asp:BoundField HeaderText="TravelKM" DataField="TravelKM" />
                                        <asp:BoundField HeaderText="DeadKM" DataField="DeadKM" /> 
                                        <asp:BoundField HeaderText="StartKM" DataField="StartKM" /> 
                                        <asp:BoundField HeaderText="EndKM" DataField="EndKM" /> 
                                        
                                        <asp:BoundField HeaderText="StartTime" DataField="StartTime" />
                                        <asp:BoundField HeaderText="EndTime" DataField="EndTime" />
                                        <asp:BoundField HeaderText="TravelTime" DataField="TravelTime" /> 
                                        <asp:BoundField HeaderText="Basic" DataField="Basic" /> 
                                        <asp:BoundField HeaderText="ServiceTaxPercent" DataField="ServiceTaxPercent" /> 
                                        
                                         <asp:BoundField HeaderText="TotalCost" DataField="TotalCost" />
                                        <asp:BoundField HeaderText="TripStatus" DataField="TripStatus" />
                                        <asp:BoundField HeaderText="Remarks" DataField="Remarks" /> 
                                        <asp:BoundField HeaderText="CancelReason" DataField="CancelReason" /> 
                                        <asp:BoundField HeaderText="SrCitizenTrip" DataField="SrCitizenTrip" /> 
                                        
                                         <asp:BoundField HeaderText="AlternateMobileNo" DataField="AlternateMobileNo" /> 
                                        <asp:BoundField HeaderText="BookingTakenAt" DataField="BookingTakenAt" /> 
                                        <asp:BoundField HeaderText="CabAssignedAt" DataField="CabAssignedAt" /> 
                                        
                                       
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                   
                </td>
            </tr>
           
        </table>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>

