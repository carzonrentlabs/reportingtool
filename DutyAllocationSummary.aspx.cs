using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class DutyAllocationSummary : clsPageAuthorization
//public partial class DutyAllocationSummary : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
        }

        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Text.ToString();
        Todate = txtTo.Text.ToString();
        int cityid, Type;

        cityid = Convert.ToInt32(ddlCityName.SelectedValue);
        Type = Convert.ToInt32(ddltype.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=DutyAllocationsummary.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetDutiesAllocationSummary(FromDate, Todate, cityid, Type);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = new int[30];
            if (Convert.ToInt32(Type) == 1)
            {
                iColumns = new int[6] { 0, 1, 2, 3, 5, 5 };
            }
            else
            {
                iColumns = new int[7] { 0, 1, 2, 3, 4, 5, 6 };
            }
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>City Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDuties")
                        {
                            Response.Write("<td align='left'><b>Total Auto Allocated Duty</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AutoDutyAllocated")
                        {
                            Response.Write("<td align='left'><b>Auto Duty Allocated</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DutyDeallocated")
                        {
                            Response.Write("<td align='left'><b>No. of Duties De-allocated</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Implementation")
                        {
                            Response.Write("<td align='left'><b>% Implementation</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DutiesPerformed")
                        {
                            Response.Write("<td align='left'><b>Total Duty</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PickupDate")
                        {
                            Response.Write("<td align='left'><b>Date</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridView1.PageIndex = e.NewPageIndex;
        
    //}

    void FillGrid()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
        //GridView1.DataBinding = null;

        string Todate, FromDate;
        FromDate = txtFrom.Text.ToString();
        Todate = txtTo.Text.ToString();
        int cityid, Type;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);
        Type = Convert.ToInt32(ddltype.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetDutiesAllocationSummary(FromDate, Todate, cityid, Type);

        GridView1.Columns.Clear();

        //DataColumn col = new DataColumn();

        foreach (DataColumn col in GetBatchDetail.Columns)
        //foreach (col in GetBatchDetail.Columns)
        {
           
            BoundField bField = new BoundField();
            bField.DataField = col.ColumnName;
            if (col.ColumnName == "CityName")
            {
                bField.HeaderText = "City Name";
            }
            else if (col.ColumnName == "TotalDuties")
            {
                bField.HeaderText = "Total Auto Allocated Duty";
            }
            else if (col.ColumnName == "AutoDutyAllocated")
            {
                bField.HeaderText = "Auto Duty Allocated";
            }
            else if (col.ColumnName == "DutyDeallocated")
            {
                bField.HeaderText = "No. of Duties De-allocated";
            }
            else if (col.ColumnName == "Implementation")
            {
                bField.HeaderText = "% Implementation";
            }
            else if (col.ColumnName == "DutiesPerformed")
            {
                bField.HeaderText = "Total Duty";
            }
            else if (col.ColumnName == "PickupDate")
            {
                bField.HeaderText = "Date";
            }
            else
            {
                bField.HeaderText = col.ColumnName;
            }
            GridView1.Columns.Add(bField);
        }
        GridView1.DataSource = GetBatchDetail;
        //Bind the datatable with the GridView.
        GridView1.DataBind();
       
        //if (GetBatchDetail.Rows.Count > 0)
        //{
        //    GridView1.DataSource = GetBatchDetail;
        //    GridView1.DataBind();
        //}
        //else
        //{
        //    GridView1.DataSource = GetBatchDetail;
        //    GridView1.DataBind();
        //}
    }


    //private void loadDynamicGrid()
    //{
    //    #region Code for preparing the DataTable

    //    //Create an instance of DataTable
    //    DataTable dt = new DataTable();

    //    //Create an ID column for adding to the Datatable
    //    DataColumn dcol = new DataColumn(ID, typeof(System.Int32));
    //    dcol.AutoIncrement = true;
    //    dt.Columns.Add(dcol);

    //    //Create an ID column for adding to the Datatable
    //    dcol = new DataColumn(NAME, typeof(System.String));
    //    dt.Columns.Add(dcol);

    //    //Now add data for dynamic columns
    //    //As the first column is auto-increment, we do not have to add any thing.
    //    //Let's add some data to the second column.
    //    for (int nIndex = 0; nIndex < 10; nIndex++)
    //    {
    //        //Create a new row
    //        DataRow drow = dt.NewRow();

    //        //Initialize the row data.
    //        drow[NAME] = "Row-" + Convert.ToString((nIndex + 1));

    //        //Add the row to the datatable.
    //        dt.Rows.Add(drow);
    //    }
    //    #endregion

    //    //Iterate through the columns of the datatable to set the data bound field dynamically.
    //    foreach (DataColumn col in dt.Columns)
    //    {
    //        //Declare the bound field and allocate memory for the bound field.
    //        BoundField bfield = new BoundField();

    //        //Initalize the DataField value.
    //        bfield.DataField = col.ColumnName;

    //        //Initialize the HeaderText field value.
    //        bfield.HeaderText = col.ColumnName;

    //        //Add the newly created bound field to the GridView.
    //        GrdDynamic.Columns.Add(bfield);
    //    }

    //    //Initialize the DataSource
    //    GrdDynamic.DataSource = dt;

    //    //Bind the datatable with the GridView.
    //    GrdDynamic.DataBind();
    //}

}