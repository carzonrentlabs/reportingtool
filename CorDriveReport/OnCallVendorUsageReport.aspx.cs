﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_OnCallVendorUsage_Report : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    #region "Dropdownlist Bind Function"
    private void BindBrachDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "Cityname";
                    ddlBranch.DataValueField = "CityId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    private void BindUsageReport()
    {
        try
        {
            int _cityId = 0, _count = 0, _v_count = 0;
            DateTime _fromdate, _todate;
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            DataSet _objDs = new DataSet();
            _objDs = objCordrive.GetVenderUsgaeReport(_cityId, _fromdate, _todate);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dvPaging.Visible = true;
                    _count = _objDs.Tables.Count;
                    PagedDataSource objPage_data = new PagedDataSource();
                    objPage_data.DataSource = _objDs.Tables[0].DefaultView;
                    objPage_data.AllowPaging = true;
                    objPage_data.PageSize = 100;
                    objPage_data.CurrentPageIndex = _PgNum;

                    _v_count = _count / objPage_data.PageSize;
                    if (_PgNum < 1)
                    {
                        lnkPrev.Visible = false;
                    }
                    else if (_PgNum > 0)
                    {
                        lnkPrev.Visible = true;
                    }
                    else if (_PgNum < _v_count)
                    {
                        lnkNext.Visible = true;
                    }
                    rptVendorUsageReport.DataSource = objPage_data;
                    rptVendorUsageReport.DataBind();
                }
                else
                {
                    dvPaging.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    #endregion

    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindUsageReport();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {

            int _cityId = 0;
            DateTime _fromdate, _todate;
            DataTable dt = new DataTable();

            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            DataSet _objDs = new DataSet();
            _objDs = objCordrive.GetVenderUsgaeReport(_cityId, _fromdate, _todate);

            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
            }


            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=OnCallVendorUsageReport.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10 };
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "ID")
                                {
                                    Response.Write("<td align='left'><b>S.No</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarNumber")
                                {
                                    Response.Write("<td align='left'><b>Car Number</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarCategory")
                                {
                                    Response.Write("<td align='left'><b>Car Category</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarModel")
                                {
                                    Response.Write("<td align='left'><b>Car Model</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                                {
                                    Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "BranchName")
                                {
                                    Response.Write("<td align='left'><b>Branch / Unit</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "NoOfBooking")
                                {
                                    Response.Write("<td align='left'><b>Number Of Bookings</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "Revenue")
                                {
                                    Response.Write("<td align='left'><b>Revenue</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorShare")
                                {
                                    Response.Write("<td align='left'><b>Vendor Share</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CorShare")
                                {
                                    Response.Write("<td align='left'><b>COR Share</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "RevenueSharePC")
                                {
                                    Response.Write("<td align='left'><b>Revenue sharing PC</b></td>");
                                }
                            }
                            Response.Write("</tr>");
                        }
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");
                    Response.End();


                }
                else
                {

                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();

                }


            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void lnkPrev_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum -= 1;
            BindUsageReport();

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum += 1;
            BindUsageReport();

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }

    public int _PgNum
    {
        get
        {
            if (ViewState["PageNum"] != null)
            {
                return Convert.ToInt32(ViewState["PageNum"].ToString());
            }
            else
            {
                return 0;
            }
        }
        set
        {
            ViewState["PageNum"] = value;
        }
    }
}