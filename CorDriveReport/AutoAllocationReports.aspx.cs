﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

public partial class CorDriveReport_AutoAllocationReports : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        objCordrive = new CorDrive();
        DataSet ds = new DataSet();
        if (!string.IsNullOrEmpty(txtFromDate.Text.ToString()) && !string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ds = objCordrive.GetAutAllocationReports(txtFromDate.Text.ToString(), txtToDate.Text.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                grvAutoAlloction.DataSource = ds.Tables[0];
                grvAutoAlloction.DataBind();
            }
        }
        else
        {
            lblMessage.Visible = true;
            lblMessage.Text = "From date and to date should not be blank.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }




    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        objCordrive = new CorDrive();
        DataSet ds = new DataSet();
        DataTable dtEx = new DataTable();
        StringBuilder strAttachment;
        if (!string.IsNullOrEmpty(txtFromDate.Text.ToString()) && !string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ds = objCordrive.GetAutAllocationReports(txtFromDate.Text.ToString(), txtToDate.Text.ToString());
            dtEx = ds.Tables[0];
            if (ds.Tables[0].Rows.Count > 0)
            {
                strAttachment = new StringBuilder("<table border = 1 cellspaning='2' cellpadding='2'  width = 100%> ");
                for (int i = 0; i < dtEx.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        strAttachment.Append("<tr>");
                        for (int j = 0; j < dtEx.Columns.Count; j++)
                        {
                            strAttachment.Append("<td align='center' bgcolor='#FFE4B5' ><b>" + dtEx.Columns[j].Caption.ToString() + "</b></td>");
                        }
                        strAttachment.Append("</tr>");
                    }
                    if (i == Convert.ToInt16(dtEx.Rows.Count) - 1)
                    {
                        strAttachment.Append("<tr bgcolor='#FFE4B5'>");
                    }
                    else
                    {
                        strAttachment.Append("<tr>");
                    }
                    int s;
                    for (int j = 0; j < dtEx.Columns.Count; j++)
                    {
                        if (int.TryParse(dtEx.Rows[i][j].ToString(), out s))
                        {
                            strAttachment.Append("<td align='center'>" + dtEx.Rows[i][j].ToString() + "</td>");
                        }
                        else if (dtEx.Rows[i][j].ToString() == "zzTotal")
                        {
                            strAttachment.Append("<td align='center'>Total</td>");
                        }
                        else
                        {
                            strAttachment.Append("<td align='left'>" + dtEx.Rows[i][j].ToString() + "</td>");
                        }
                    }
                    strAttachment.Append("</tr>");
                }
                strAttachment.Append("</table>");

                Response.ClearContent();
                Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                Response.Clear();
                Response.AppendHeader("content-disposition", "attachment;filename=BranchChauffeurList.xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.ms-excel";
                this.EnableViewState = false;
                Response.Write("\r\n");
                Response.Write(strAttachment);
                Response.End();

            }
        }
        else
        {
            lblMessage.Visible = true;
            lblMessage.Text = "From date and to date should not be blank.";
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void grvAutoAlloction_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblCityName = (Label)e.Row.FindControl("lblCityName");
            if (lblCityName.Text == "zzTotal")
            {
                lblCityName.Text = "Total";
            }
        
        }
    }
}

