﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;

public partial class CorDriveReport_CORDriveLoginHoursSummary : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            BindCityDropDownlist();
            BindCarModelDropDowmnList();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    private void BindCarModelDropDowmnList()
    {
        try
        {
            DataSet _objDScarModel = new DataSet();
            _objDScarModel = objCordrive.GetActiveCarModel();
            if (_objDScarModel != null)
            {
                if (_objDScarModel.Tables.Count > 0)
                {
                    ddlCarModel.DataTextField = "ModelName";
                    ddlCarModel.DataValueField = "ModelId";
                    ddlCarModel.DataSource = _objDScarModel.Tables[0];
                    ddlCarModel.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        StringBuilder strData = BindSummaryData();
        ltlSummary.Text = strData.ToString();

        // ltlSummary.Visible = false; 
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = BindSummaryData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "CORDriveLoginHoursSummary";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();
        // ltlSummary.Visible = false; 
    }

    private StringBuilder BindSummaryData()
    {
        StringBuilder strData = new StringBuilder();
        DateTime fromDate = Convert.ToDateTime(txtFromDate.Text.ToString());
        DateTime toDate = Convert.ToDateTime(txtToDate.Text.ToString());
        DateTime startDate;
        List<string> dateName;

        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alter('From date and todate should not be empty.')", true);
        }

        try
        {

            DataSet dsSummary = new DataSet();
            dsSummary = objCordrive.GetLoginHoursSummary(Convert.ToInt16(ddlCity.SelectedValue), fromDate, toDate, Convert.ToInt16(ddlCarModel.SelectedValue), Convert.ToInt16(ddlVdpYN.SelectedValue));
            if (dsSummary.Tables[0].Rows.Count > 0)
            {

                strData.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office'");
                strData.Append("xmlns:x='urn:schemas-microsoft-com:office:excel'");
                strData.Append("xmlns='http://www.w3.org/TR/REC-html40'>");
                strData.Append("<head>");
                strData.Append("<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>");
                strData.Append("<meta name=ProgId content=Excel.Sheet>");
                strData.Append("<meta name=Generator content='Microsoft Excel 14'>");
                strData.Append("<link rel=File-List href='Summary_files/filelist.xml'>");
                strData.Append("<style id='Summary_2336_Styles'>");
                strData.Append("<!--table");
                strData.Append("{mso-displayed-decimal-separator:'\';");
                strData.Append("mso-displayed-thousand-separator:'\';}");
                strData.Append(".xl632336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:400;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:general;");
                strData.Append("vertical-align:middel;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl642336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:400;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:center;");
                strData.Append("vertical-align:middle;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl652336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:400;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:general;");
                strData.Append("vertical-align:bottom;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:normal;}");
                strData.Append(".xl662336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:400;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:center;");
                strData.Append("vertical-align:middle;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl672336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:400;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:general;");
                strData.Append("vertical-align:middle;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl682336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:9.0pt;");
                strData.Append("font-weight:700;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Arial, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:center;");
                strData.Append("vertical-align:middle;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl692336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:700;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:general;");
                strData.Append("vertical-align:middle;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl702336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:700;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:center;");
                strData.Append("vertical-align:middle;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append(".xl712336");
                strData.Append("{padding-top:1px;");
                strData.Append("padding-right:1px;");
                strData.Append("padding-left:1px;");
                strData.Append("mso-ignore:padding;");
                strData.Append("color:black;");
                strData.Append("font-size:11.0pt;");
                strData.Append("font-weight:700;");
                strData.Append("font-style:normal;");
                strData.Append("text-decoration:none;");
                strData.Append("font-family:Calibri, sans-serif;");
                strData.Append("mso-font-charset:0;");
                strData.Append("mso-number-format:General;");
                strData.Append("text-align:general;");
                strData.Append("vertical-align:bottom;");
                strData.Append("border:1.0pt solid windowtext;");
                strData.Append("mso-background-source:auto;");
                strData.Append("mso-pattern:auto;");
                strData.Append("white-space:nowrap;}");
                strData.Append("-->");
                strData.Append("</style>");
                strData.Append("</head>");
                strData.Append("<body>");
                strData.Append("<!--[if !excel]>&nbsp;&nbsp;<![endif]-->");
                strData.Append("<!--The following information was generated by Microsoft Excel's Publish as Web");
                strData.Append("Page wizard.-->");
                strData.Append("<!--If the same item is republished from Excel, all information between the DIV");
                strData.Append("tags will be replaced.-->");
                strData.Append("<!----------------------------->");
                strData.Append("<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->");
                strData.Append("<!----------------------------->");
                strData.Append("<div id='Summary_2336' align=center x:publishsource='Excel'>");
                strData.Append("<table border=0 cellpadding=0 cellspacing=0 width=2619 class=xl655352336");
                strData.Append("style='border-collapse:collapse;table-layout:fixed;width:1965pt'>");
                strData.Append("<col class=xl655352336 width=36 style='mso-width-source:userset;mso-width-alt:");
                strData.Append("1316;width:27pt'>");
                strData.Append("<col class=xl655352336 width=64 style='mso-width-source:userset;mso-width-alt:");
                strData.Append("2340;width:48pt'>");
                strData.Append("<col class=xl655352336 width=74 style='mso-width-source:userset;mso-width-alt:");
                strData.Append("2706;width:56pt'>");
                strData.Append("<col class=xl655352336 width=64 span=2 style='width:48pt'>");
                strData.Append("<col class=xl655352336 width=83 style='mso-width-source:userset;mso-width-alt:");
                strData.Append("3035;width:62pt'>");
                strData.Append("<col class=xl655352336 width=126 style='mso-width-source:userset;mso-width-alt:");
                strData.Append("4608;width:95pt'>");
                strData.Append("<col class=xl655352336 width=60 style='mso-width-source:userset;mso-width-alt:");
                strData.Append("2194;width:45pt'>");
                strData.Append("<col class=xl655352336 width=64 span=32 style='width:48pt'>");
                strData.Append("<tr height=20 style='height:15.0pt'>");
                strData.Append("<td height=20 class=xl712336 width=36 style='height:15.0pt;width:27pt'>S.No</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>Car No</td>");
                strData.Append("<td class=xl682336 width=74 style='border-left:none;width:56pt'>City Name</td>");
                strData.Append("<td class=xl692336 width=64 style='border-left:none;width:48pt'>Car Categ<span");
                strData.Append("style='display:none'>ory</span></td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>Model Nam<span");
                strData.Append("style='display:none'>e</span></td>");
                strData.Append("<td class=xl682336 width=83 style='border-left:none;width:62pt'>Vendor name</td>");
                strData.Append("<td class=xl702336 width=126 style='border-left:none;width:95pt'>Attachment");
                strData.Append("Model</td>");
                strData.Append("<td class=xl682336 width=60 style='border-left:none;width:45pt'>VDP (Y/N)</td>");
                //DataSet dsMonthName = new DataSet();
                // dsMonthName = objCordrive.GetDayName(fromDate,toDate);
                startDate = fromDate;
                dateName = new List<string>();
                string daysName = string.Empty;
                string showDate = string.Empty;
                while (startDate < toDate)
                {
                    startDate = startDate.AddDays(1);
                    daysName = startDate.ToString("MMMM") + "_" + startDate.Day + "_" + startDate.Year;
                    showDate = startDate.Day + "D/" + startDate.Month + "M/" + startDate.Year+"Y";
                    dateName.Add(daysName);
                    strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>" + showDate.ToString() + "</td>");
                }
                /*
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>DD/MM/YY</td>");
                */
                int counter = 0;
                strData.Append("<td class=xl682336 width=64 style='border-left:none;width:48pt'>MTD Total</td>");
                strData.Append("</tr>");
                foreach (DataRow dr in dsSummary.Tables[0].Rows)
                {
                    counter = counter + 1;
                    strData.Append("<tr height=60 style='height:45.0pt'>");
                    strData.Append("<td height=60 class=xl632336 style='height:45.0pt;border-top:none'>" + counter.ToString() + "</td>");
                    strData.Append("<td class=xl642336 style='border-top:none;border-left:none'>" + dr["Regnno"] + "</td>");
                    strData.Append("<td class=xl642336 style='border-top:none;border-left:none'>" + dr["CityName"] + "</td>");
                    strData.Append("<td class=xl642336 style='border-top:none;border-left:none'>" + dr["CarCategory"] + "</td>");
                    strData.Append("<td class=xl642336 style='border-top:none;border-left:none'>" + dr["ModelName"] + "</td>");
                    strData.Append("<td class=xl632336 style='border-top:none;border-left:none'>" + dr["VendorName"] + "</td>");
                    strData.Append("<td class=xl632336 style='border-top:none;border-left:none'>" + dr["AttachmentModel"] + "</td>");
                    strData.Append("<td class=xl672336 style='border-top:none;border-left:none'>" + dr["VDPYN"] + "</td>");

                    foreach (string daymonthName in dateName)
                    {
                        strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                        strData.Append("width:48pt'>" + dr[daymonthName] + "&nbsp;</td>");
                    }
                    
                    /*strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>Total Login Hours</td>");
                     */
                    strData.Append("<td class=xl652336 width=64 style='border-top:none;border-left:none;");
                    strData.Append("width:48pt'>" + dr["MTDTotal"] + "</td>");
                    
                    strData.Append("</tr>");
                }
                strData.Append("<tr height=20 style='height:15.0pt'>");
                strData.Append("<td height=20 class=xl655352336 style='height:15.0pt'></td>");
                strData.Append("<td class=xl662336></td>");
                strData.Append("<td class=xl662336></td>");
                strData.Append("<td class=xl662336></td>");
                strData.Append("<td class=xl662336></td>");
                strData.Append("<td class=xl662336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                /*strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");
                strData.Append("<td class=xl655352336></td>");*/
                strData.Append("</tr>");
                strData.Append("<![if supportMisalignedColumns]>");
                strData.Append("<tr height=0 style='display:none'>");
                strData.Append("<td width=36 style='width:27pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=74 style='width:56pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=83 style='width:62pt'></td>");
                strData.Append("<td width=126 style='width:95pt'></td>");
                strData.Append("<td width=60 style='width:45pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                /*
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");
                strData.Append("<td width=64 style='width:48pt'></td>");*/
                strData.Append("</tr>");
                strData.Append("<![endif]>");
                strData.Append("</table>");
                strData.Append("</div>");
                strData.Append("<!----------------------------->");
                strData.Append("<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->");
                strData.Append("<!----------------------------->");
                strData.Append("</body>");
                strData.Append("</html>");
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
        return strData;
    }


}
