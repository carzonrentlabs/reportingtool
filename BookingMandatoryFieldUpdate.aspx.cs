using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;
using ClosedXML.Excel;
using System.Linq;

public partial class BookingMandatoryFieldUpdate : System.Web.UI.Page
{
    clsAutomation objMandate = new clsAutomation();
    Miscellaneous objMesceOL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            lblMessage.Text = "";
            BindClient();
        }
    }
    private void BindClient()
    {

        DataSet ds = objMandate.FillClient();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            ddlClientName.DataSource = ds.Tables[0];
            ddlClientName.DataTextField = "ClientCoName";
            ddlClientName.DataValueField = "ClientCoID";
            ddlClientName.DataBind();
        }
        else
        {
            ddlClientName.DataSource = null;
            ddlClientName.DataBind();
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        DataTable dtBookingIdDetails = new DataTable();
        DataTable dtMapField = new DataTable();
        lblMessage.Visible = false;
        int clientcoId;
        pnlMandatory.Visible = false;
        pnlUploadDOC.Visible = false;
        lnkViewAddEmail.Visible = false;
        lnkAddVRF.Visible = false;
        try
        {
            if (Convert.ToInt16(rdupdate.SelectedValue) == 1)
            {
                dtBookingIdDetails = objMandate.CheckUploadEmailVRFRequired(int.Parse(txtBookingId.Text));
                if (dtBookingIdDetails.Rows.Count >= 1)
                {
                    pnlUploadDOC.Visible = true;
                    pnlMandatory.Visible = false;
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0][0])))
                    {
                        ImageEmailPreview.ImageUrl = "~/images/TickMark.png";
                        lnkViewEmail.Visible = true;
                    }
                    else
                    {
                        ImageEmailPreview.ImageUrl = "~/images/cross.png";
                        lnkViewEmail.Visible = false;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0][1])))
                    {
                        ImageVRFPreview.ImageUrl = "~/images/TickMark.png";
                        lnkVRF.Visible = true;
                    }
                    else
                    {
                        ImageVRFPreview.ImageUrl = "~/images/cross.png";
                        lnkVRF.Visible = false;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["AddEmailFile"])))
                    {
                        lnkViewAddEmail.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["AddVRFFile"])))
                    {
                        lnkAddVRF.Visible = true;
                    }

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Email and VRF uploading is not required.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }

            }
            else
            {
                pnlMandatory.Visible = true;
                pnlUploadDOC.Visible = false;
                InitializeControls(Form, txtBookingId);
                HideAllcontrol();

                if (txtBookingId.Text.Trim() == "")
                {
                    return;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Enter booking id');", true);
                }
                else
                {

                    dtBookingIdDetails = objMandate.GetMandatoryFields(int.Parse(txtBookingId.Text));

                    if (dtBookingIdDetails.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc1"])))
                        {
                            tr1.Style.Add("display", "block");
                            lblMisc1.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc1"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype1"]) == "1")
                            {
                                ddlMisc1.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID1"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc1");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc1.DataSource = dtMapField;
                                    ddlMisc1.DataValueField = "MapFieldName";
                                    ddlMisc1.DataTextField = "MapFieldName";
                                    ddlMisc1.DataBind();
                                    ddlMisc1.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous1"]);
                                }
                            }
                            else
                            {
                                txtMisc1.Style.Add("display", "block");
                                txtMisc1.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous1"]);
                            }
                        }
                        else
                        {
                            ddlMisc1.Style.Add("display", "none");
                            txtMisc1.Style.Add("display", "none");
                            tr1.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc2"])))
                        {
                            tr2.Style.Add("display", "block");
                            lblMisc2.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc2"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype2"]) == "1")
                            {
                                ddlMisc2.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID2"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc2");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc2.DataSource = dtMapField;
                                    ddlMisc2.DataValueField = "MapFieldName";
                                    ddlMisc2.DataTextField = "MapFieldName";
                                    ddlMisc2.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous2"].ToString()))
                                    {
                                        ddlMisc2.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous2"]);
                                    }
                                }

                            }
                            else
                            {
                                txtMisc2.Style.Add("display", "block");
                                txtMisc2.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous2"]);
                            }
                        }
                        else
                        {
                            ddlMisc2.Style.Add("display", "none");
                            txtMisc2.Style.Add("display", "none");
                            tr2.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc3"])))
                        {
                            tr3.Style.Add("display", "block");
                            lblMisc3.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc3"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype3"]) == "1")
                            {
                                ddlMisc3.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID3"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc3");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc3.DataSource = dtMapField;
                                    ddlMisc3.DataValueField = "MapFieldName";
                                    ddlMisc3.DataTextField = "MapFieldName";
                                    ddlMisc3.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous3"].ToString()))
                                    {
                                        ddlMisc3.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous3"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc3.Style.Add("display", "block");
                                txtMisc3.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous3"]);
                            }
                        }
                        else
                        {
                            ddlMisc3.Style.Add("display", "none");
                            txtMisc3.Style.Add("display", "none");
                            tr3.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc4"])))
                        {
                            tr4.Style.Add("display", "block");
                            lblMisc4.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc4"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                            {
                                ddlMisc4.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID4"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc4");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc4.DataSource = dtMapField;
                                    ddlMisc4.DataValueField = "MapFieldName";
                                    ddlMisc4.DataTextField = "MapFieldName";
                                    ddlMisc4.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous4"].ToString()))
                                    {
                                        ddlMisc4.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous4"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc4.Style.Add("display", "block");
                                txtMisc4.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous4"]);
                            }
                        }
                        else
                        {
                            ddlMisc4.Style.Add("display", "none");
                            txtMisc4.Style.Add("display", "none");
                            tr4.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc5"])))
                        {
                            tr5.Style.Add("display", "block");
                            lblMisc5.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc5"]);

                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype5"]) == "1")
                            {
                                ddlMisc5.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID5"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc5");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc5.DataSource = dtMapField;
                                    ddlMisc5.DataValueField = "MapFieldName";
                                    ddlMisc5.DataTextField = "MapFieldName";
                                    ddlMisc5.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous5"].ToString()))
                                    {
                                        ddlMisc5.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous5"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc5.Style.Add("display", "block");
                                txtMisc5.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous5"]);
                            }
                        }
                        else
                        {
                            ddlMisc5.Style.Add("display", "none");
                            txtMisc5.Style.Add("display", "none");
                            tr5.Style.Add("display", "none");
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc6"])))
                        {
                            tr6.Style.Add("display", "block");
                            lblMisc6.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc6"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype6"]) == "1")
                            {
                                ddlMisc6.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID6"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc6");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc6.DataSource = dtMapField;
                                    ddlMisc6.DataValueField = "MapFieldName";
                                    ddlMisc6.DataTextField = "MapFieldName";
                                    ddlMisc6.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous6"].ToString()))
                                    {
                                        ddlMisc6.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous6"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc6.Style.Add("display", "block");
                                txtMisc6.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous6"]);
                            }
                        }
                        else
                        {
                            ddlMisc6.Style.Add("display", "none");
                            txtMisc6.Style.Add("display", "none");
                            tr6.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc7"])))
                        {
                            tr7.Style.Add("display", "block");
                            lblMisc7.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc7"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype7"]) == "1")
                            {
                                ddlMisc7.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID7"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc7");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc7.DataSource = dtMapField;
                                    ddlMisc7.DataValueField = "MapFieldName";
                                    ddlMisc7.DataTextField = "MapFieldName";
                                    ddlMisc7.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous7"].ToString()))
                                    {
                                        ddlMisc7.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous7"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc7.Style.Add("display", "block");
                                txtMisc7.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous7"]);
                            }
                        }
                        else
                        {
                            ddlMisc7.Style.Add("display", "none");
                            txtMisc7.Style.Add("display", "none");
                            tr7.Style.Add("display", "none");
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc8"])))
                        {
                            tr8.Style.Add("display", "block");
                            lblMisc8.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc8"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype8"]) == "1")
                            {
                                ddlMisc8.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID8"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc8");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc8.DataSource = dtMapField;
                                    ddlMisc8.DataValueField = "MapFieldName";
                                    ddlMisc8.DataTextField = "MapFieldName";
                                    ddlMisc8.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous8"].ToString()))
                                    {
                                        ddlMisc8.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous8"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc8.Style.Add("display", "block");
                                txtMisc8.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous8"]);
                            }
                        }
                        else
                        {
                            ddlMisc8.Style.Add("display", "none");
                            txtMisc8.Style.Add("display", "none");
                            tr8.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc9"])))
                        {
                            tr9.Style.Add("display", "block");
                            lblMisc9.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc9"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype9"]) == "1")
                            {
                                ddlMisc9.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID9"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc9");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc9.DataSource = dtMapField;
                                    ddlMisc9.DataValueField = "MapFieldName";
                                    ddlMisc9.DataTextField = "MapFieldName";
                                    ddlMisc9.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous9"].ToString()))
                                    {
                                        ddlMisc9.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous9"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc9.Style.Add("display", "block");
                                txtMisc9.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous9"]);
                            }
                        }
                        else
                        {
                            ddlMisc9.Style.Add("display", "none");
                            txtMisc9.Style.Add("display", "none");
                            tr9.Style.Add("display", "none");
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc10"])))
                        {
                            tr10.Style.Add("display", "block");
                            lblMisc10.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc10"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype10"]) == "1")
                            {
                                ddlMisc10.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID10"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc10");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc10.DataSource = dtMapField;
                                    ddlMisc10.DataValueField = "MapFieldName";
                                    ddlMisc10.DataTextField = "MapFieldName";
                                    ddlMisc10.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous10"].ToString()))
                                    {
                                        ddlMisc10.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous10"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc10.Style.Add("display", "block");
                                txtMisc10.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous10"]);
                            }
                        }
                        else
                        {
                            ddlMisc10.Style.Add("display", "none");
                            txtMisc10.Style.Add("display", "none");
                            tr10.Style.Add("display", "none");
                        }

                        // Amazon city
                        clientcoId = Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc11"])))
                        {
                            tr11.Style.Add("display", "block");
                            lblMisc11.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc11"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype11"]) == "1")
                            {
                                ddlMisc11.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID11"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc11");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc11.DataSource = dtMapField;
                                    ddlMisc11.DataValueField = "MapFieldName";
                                    ddlMisc11.DataTextField = "MapFieldName";
                                    ddlMisc11.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous11"].ToString()))
                                    {
                                        ddlMisc11.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous11"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc11.Style.Add("display", "block");
                                txtMisc11.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous11"]);
                            }

                        }
                        else
                        {
                            ddlMisc11.Style.Add("display", "none");
                            txtMisc11.Style.Add("display", "none");
                            tr11.Style.Add("display", "none");
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc12"])))
                        {
                            tr12.Style.Add("display", "block");
                            lblMisc12.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc12"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype12"]) == "1")
                            {
                                ddlMisc12.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID12"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc12");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc12.DataSource = dtMapField;
                                    ddlMisc12.DataValueField = "MapFieldName";
                                    ddlMisc12.DataTextField = "MapFieldName";
                                    ddlMisc12.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous12"].ToString()))
                                    {
                                        ddlMisc12.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous12"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc12.Style.Add("display", "block");
                                txtMisc12.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous12"]);
                            }

                        }
                        else
                        {
                            ddlMisc12.Style.Add("display", "none");
                            txtMisc12.Style.Add("display", "none");
                            tr12.Style.Add("display", "none");
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc13"])))
                        {
                            tr13.Style.Add("display", "block");
                            lblMisc13.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc13"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype13"]) == "1")
                            {
                                ddlMisc13.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID13"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc13");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc13.DataSource = dtMapField;
                                    ddlMisc13.DataValueField = "MapFieldName";
                                    ddlMisc13.DataTextField = "MapFieldName";
                                    ddlMisc13.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous13"].ToString()))
                                    {
                                        ddlMisc13.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous13"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc13.Style.Add("display", "block");
                                txtMisc13.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous13"]);
                            }

                        }
                        else
                        {
                            ddlMisc13.Style.Add("display", "none");
                            txtMisc13.Style.Add("display", "none");
                            tr13.Style.Add("display", "none");
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc14"])))
                        {
                            tr14.Style.Add("display", "block");
                            lblMisc14.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc14"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype13"]) == "1")
                            {
                                ddlMisc14.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID14"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc14");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc14.DataSource = dtMapField;
                                    ddlMisc14.DataValueField = "MapFieldName";
                                    ddlMisc14.DataTextField = "MapFieldName";
                                    ddlMisc14.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous14"].ToString()))
                                    {
                                        ddlMisc14.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous14"]);
                                    }
                                }
                            }
                            else
                            {

                                txtMisc14.Style.Add("display", "block");
                                txtMisc14.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous14"]);
                            }

                        }
                        else
                        {
                            ddlMisc14.Style.Add("display", "none");
                            txtMisc14.Style.Add("display", "none");
                            tr14.Style.Add("display", "none");
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc15"])))
                        {
                            tr15.Style.Add("display", "block");
                            lblMisc15.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc15"]);
                            if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype13"]) == "1")
                            {
                                ddlMisc15.Style.Add("display", "block");
                                dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID15"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc15");
                                if (dtMapField.Rows.Count > 0)
                                {
                                    ddlMisc15.DataSource = dtMapField;
                                    ddlMisc15.DataValueField = "MapFieldName";
                                    ddlMisc15.DataTextField = "MapFieldName";
                                    ddlMisc15.DataBind();
                                    if (!string.IsNullOrEmpty(dtBookingIdDetails.Rows[0]["Miscellaneous15"].ToString()))
                                    {
                                        ddlMisc15.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous15"]);
                                    }
                                }
                            }
                            else
                            {
                                txtMisc15.Style.Add("display", "block");
                                txtMisc15.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous15"]);
                            }

                        }
                        else
                        {
                            ddlMisc15.Style.Add("display", "none");
                            txtMisc15.Style.Add("display", "none");
                            tr15.Style.Add("display", "none");
                        }
                        //End of Amazon City

                        trSubmit.Style.Add("display", "block");
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Record not available";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }

                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void bntSubmit_Click(object sender, EventArgs e)
    {
        Miscellaneous objMis = new Miscellaneous();
        int status = 0;
        lblMessage.Visible = false;
        try
        {

            if (txtBookingId.Text.Trim() == "")
            {
                return;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Enter booking id');", true);
            }
            else
            {

                objMis.BookingID = int.Parse(txtBookingId.Text);
                objMis.SysUserId = Convert.ToInt32(Session["UserID"]);
                if (txtMisc1.Style["display"] == "block")
                {
                    objMis.Miscellaneous1 = Convert.ToString(txtMisc1.Text);
                }
                else if (ddlMisc1.Style["display"] == "block")
                {
                    objMis.Miscellaneous1 = Convert.ToString(ddlMisc1.SelectedValue);
                }
                if (txtMisc2.Style["display"] == "block")
                {
                    objMis.Miscellaneous2 = Convert.ToString(txtMisc2.Text);
                }
                else if (ddlMisc2.Style["display"] == "block")
                {
                    objMis.Miscellaneous2 = Convert.ToString(ddlMisc2.SelectedValue);
                }
                if (txtMisc3.Style["display"] == "block")
                {
                    objMis.Miscellaneous3 = Convert.ToString(txtMisc3.Text);
                }
                else if (ddlMisc3.Style["display"] == "block")
                {
                    objMis.Miscellaneous3 = Convert.ToString(ddlMisc3.SelectedValue);
                }
                if (txtMisc4.Style["display"] == "block")
                {
                    objMis.Miscellaneous4 = Convert.ToString(txtMisc4.Text);
                }
                else if (ddlMisc4.Style["display"] == "block")
                {
                    objMis.Miscellaneous4 = Convert.ToString(ddlMisc4.SelectedValue);
                }
                if (txtMisc5.Style["display"] == "block")
                {
                    objMis.Miscellaneous5 = Convert.ToString(txtMisc5.Text);
                }
                else if (ddlMisc5.Style["display"] == "block")
                {
                    objMis.Miscellaneous5 = Convert.ToString(ddlMisc5.SelectedValue);
                }
                if (txtMisc6.Style["display"] == "block")
                {
                    objMis.Miscellaneous6 = Convert.ToString(txtMisc6.Text);
                }
                else if (ddlMisc6.Style["display"] == "block")
                {
                    objMis.Miscellaneous6 = Convert.ToString(ddlMisc6.SelectedValue);
                }
                if (txtMisc7.Style["display"] == "block")
                {
                    objMis.Miscellaneous7 = Convert.ToString(txtMisc7.Text);
                }
                else if (ddlMisc7.Style["display"] == "block")
                {
                    objMis.Miscellaneous7 = Convert.ToString(ddlMisc7.SelectedValue);
                }
                if (txtMisc8.Style["display"] == "block")
                {
                    objMis.Miscellaneous8 = Convert.ToString(txtMisc8.Text);
                }
                else if (ddlMisc8.Style["display"] == "block")
                {
                    objMis.Miscellaneous8 = Convert.ToString(ddlMisc8.SelectedValue);
                }
                if (txtMisc9.Style["display"] == "block")
                {
                    objMis.Miscellaneous9 = Convert.ToString(txtMisc9.Text);
                }
                else if (ddlMisc9.Style["display"] == "block")
                {
                    objMis.Miscellaneous9 = Convert.ToString(ddlMisc9.SelectedValue);
                }
                if (txtMisc10.Style["display"] == "block")
                {
                    objMis.Miscellaneous10 = Convert.ToString(txtMisc10.Text);
                }
                else if (ddlMisc10.Style["display"] == "block")
                {
                    objMis.Miscellaneous10 = Convert.ToString(ddlMisc10.SelectedValue);
                }

                if (txtMisc11.Style["display"] == "block")
                {
                    objMis.Miscellaneous11 = Convert.ToString(txtMisc11.Text);
                }
                else if (ddlMisc11.Style["display"] == "block")
                {
                    objMis.Miscellaneous11 = Convert.ToString(ddlMisc11.SelectedValue);
                }

                if (txtMisc12.Style["display"] == "block")
                {
                    objMis.Miscellaneous12 = Convert.ToString(txtMisc12.Text);
                }
                else if (ddlMisc12.Style["display"] == "block")
                {
                    objMis.Miscellaneous12 = Convert.ToString(ddlMisc12.SelectedValue);
                }

                if (txtMisc13.Style["display"] == "block")
                {
                    objMis.Miscellaneous13 = Convert.ToString(txtMisc13.Text);
                }
                else if (ddlMisc13.Style["display"] == "block")
                {
                    objMis.Miscellaneous13 = Convert.ToString(ddlMisc13.SelectedValue);
                }

                if (txtMisc14.Style["display"] == "block")
                {
                    objMis.Miscellaneous14 = Convert.ToString(txtMisc14.Text);
                }
                else if (ddlMisc14.Style["display"] == "block")
                {
                    objMis.Miscellaneous14 = Convert.ToString(ddlMisc14.SelectedValue);
                }

                if (txtMisc15.Style["display"] == "block")
                {
                    objMis.Miscellaneous15 = Convert.ToString(txtMisc15.Text);
                }
                else if (ddlMisc15.Style["display"] == "block")
                {
                    objMis.Miscellaneous15 = Convert.ToString(ddlMisc15.SelectedValue);
                }

                status = objMandate.UpdateMiscellaneousField(objMis);
                if (status > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Successfully updated.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Getting error while updating...";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }

    }

    public void InitializeControls(Control objcontrol, TextBox bookingId)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)) && bookingId.ID != "txtBookingId")
            {
                ((TextBox)(control)).Text = "";
            }
            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control, bookingId);
            }
        }
    }

    public void HideAllcontrol()
    {

        //lblMisc1.Style.Add("display", "none");
        ddlMisc1.Style.Add("display", "none");
        txtMisc1.Style.Add("display", "none");
        tr1.Style.Add("display", "none");

        // lblMisc2.Style.Add("display", "none");
        ddlMisc2.Style.Add("display", "none");
        txtMisc2.Style.Add("display", "none");
        tr2.Style.Add("display", "none");

        // lblMisc3.Style.Add("display", "none");
        ddlMisc3.Style.Add("display", "none");
        txtMisc3.Style.Add("display", "none");
        tr3.Style.Add("display", "none");

        // lblMisc4.Style.Add("display", "none");
        ddlMisc4.Style.Add("display", "none");
        txtMisc4.Style.Add("display", "none");
        tr4.Style.Add("display", "none");

        // lblMisc5.Style.Add("display", "none");
        ddlMisc5.Style.Add("display", "none");
        txtMisc5.Style.Add("display", "none");
        tr5.Style.Add("display", "none");

        //lblMisc6.Style.Add("display", "none");
        ddlMisc6.Style.Add("display", "none");
        txtMisc6.Style.Add("display", "none");
        tr6.Style.Add("display", "none");

        // lblMisc7.Style.Add("display", "none");
        ddlMisc7.Style.Add("display", "none");
        txtMisc7.Style.Add("display", "none");
        tr7.Style.Add("display", "none");

        // lblMisc8.Style.Add("display", "none");
        ddlMisc8.Style.Add("display", "none");
        txtMisc8.Style.Add("display", "none");
        tr8.Style.Add("display", "none");

        // lblMisc9.Style.Add("display", "none");
        ddlMisc9.Style.Add("display", "none");
        txtMisc9.Style.Add("display", "none");
        tr9.Style.Add("display", "none");

        //lblMisc10.Style.Add("display", "none");
        ddlMisc10.Style.Add("display", "none");
        txtMisc10.Style.Add("display", "none");
        tr10.Style.Add("display", "none");

        // lblMisc11.Style.Add("display", "none");
        ddlMisc11.Style.Add("display", "none");
        txtMisc11.Style.Add("display", "none");
        tr11.Style.Add("display", "none");

        // lblMisc12.Style.Add("display", "none");
        ddlMisc12.Style.Add("display", "none");
        txtMisc12.Style.Add("display", "none");
        tr12.Style.Add("display", "none");

        //lblMisc13.Style.Add("display", "none");
        ddlMisc13.Style.Add("display", "none");
        txtMisc13.Style.Add("display", "none");
        tr13.Style.Add("display", "none");

        // lblMisc14.Style.Add("display", "none");
        ddlMisc14.Style.Add("display", "none");
        txtMisc14.Style.Add("display", "none");
        tr14.Style.Add("display", "none");

        //lblMisc15.Style.Add("display", "none");
        ddlMisc15.Style.Add("display", "none");
        txtMisc15.Style.Add("display", "none");
        tr15.Style.Add("display", "none");
    }

    protected void btnUploadDOC_Click(object sender, EventArgs e)
    {
        try
        {
            string filePath = ConfigurationManager.AppSettings["EmailUplod"].ToString();
            string filePathBackUp= ConfigurationManager.AppSettings["EmailUplodBackUp"].ToString();
            string bookingID = txtBookingId.Text;
            string emailExt = string.Empty;
            string vrfExt = string.Empty;
            string fileName = string.Empty;
            int emailstatus = 0, status = 0;
            int vRFstatus = 0;

            emailExt = Path.GetExtension(flEmailUpload.FileName);
            vrfExt = Path.GetExtension(flVRFUpload.FileName);

            if ((!string.IsNullOrEmpty(flEmailUpload.FileName) && emailExt.ToLower() != ".pdf") || (
                !string.IsNullOrEmpty(flVRFUpload.FileName) && vrfExt.ToLower() != ".pdf"))
            {

                lblMessage.Visible = true;
                lblMessage.Text = "Only pdf file will be upload.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }


            if (string.IsNullOrEmpty(bookingID))
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Enter booking Id";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else if (!flEmailUpload.HasFile && !flVRFUpload.HasFile)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Choose file to upload";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else if (flEmailUpload.HasFile && flEmailUpload.PostedFile.ContentLength > 1597152)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Files size should not greather than 2 MB";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else if (flVRFUpload.HasFile && flVRFUpload.PostedFile.ContentLength > 1597152)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Files size should not greather than 2 MB";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else
            {
                if (!Directory.Exists(filePathBackUp))
                {
                    Directory.CreateDirectory(filePathBackUp);
                }

                if (flEmailUpload.HasFile)
                {
                    //int fileIndex = objMandate.GetFileIndex(int.Parse(txtBookingId.Text), 1);
                    long FileNameUnique = DateTime.Now.Ticks;
                    // 1 for add and 2 for replace
                    if (rblEditaddReplace.SelectedValue == "1")
                    {

                        if (File.Exists(filePath + bookingID + "_ad.pdf"))
                        {                            
                            File.Move(filePath + bookingID + "_ad.pdf", filePathBackUp + bookingID + "_" + FileNameUnique + "_ad.pdf");
                        }
                        fileName = bookingID + "_ad.pdf";
                        flEmailUpload.SaveAs(filePath + fileName);
                        emailstatus = UpdateFileStatus(Convert.ToInt32(bookingID), fileName, "Email", 1);
                    }
                    else
                    {
                        if (File.Exists(filePath + bookingID + ".pdf"))
                        {
                            File.Move(filePath + bookingID + ".pdf", filePathBackUp + bookingID + "_" + FileNameUnique + ".pdf");
                            //File.Delete(filePath + bookingID + ".pdf");
                        }
                        fileName = bookingID + ".pdf";
                        flEmailUpload.SaveAs(filePath + fileName);
                        emailstatus = UpdateFileStatus(Convert.ToInt32(bookingID), fileName, "Email", 2);

                    }


                }
                if (flVRFUpload.HasFile)
                {
                    // int fileIndex = objMandate.GetFileIndex(int.Parse(txtBookingId.Text), 2);
                    long FileNameUnique = DateTime.Now.Ticks;
                    // 1 for add and 2 for replace
                    if (rblVRFaddReplace.SelectedValue == "1")
                    {
                        if (File.Exists(filePath + bookingID + "_a_ad.pdf"))
                        {  
                            File.Move(filePath + bookingID + "_a_ad.pdf", filePathBackUp + bookingID + "_" + FileNameUnique + "_a_ad.pdf");
                        }
                        fileName = bookingID + "_a_ad.pdf";
                        flVRFUpload.SaveAs(filePath + fileName);
                        vRFstatus = UpdateFileStatus(Convert.ToInt32(bookingID), fileName, "VRF", 1);

                    }
                    else
                    {
                        if (File.Exists(filePath + bookingID + "_a.pdf"))
                        {
                            File.Move(filePath + bookingID + "_a.pdf", filePathBackUp + bookingID + "_" + FileNameUnique + "_a.pdf");
                            //File.Delete(filePath + bookingID + "_a.pdf");
                        }
                        fileName = bookingID + "_a.pdf";
                        flVRFUpload.SaveAs(filePath + fileName);
                        vRFstatus = UpdateFileStatus(Convert.ToInt32(bookingID), fileName, "VRF", 2);
                    }
                }
                if (emailstatus > 0 || vRFstatus > 0)
                {
                    status = objMandate.UpdateEmailVRFDocUPdate(Convert.ToInt32(bookingID));
                }
                if (emailstatus > 0 && vRFstatus > 0 && status > 0)
                {
                    lblMessage.Text = "Both file upload successfully.";
                }
                else if (emailstatus > 0 && status > 0)
                {
                    lblMessage.Text = "Email file upload successfully.";
                }
                else if (vRFstatus > 0 && status > 0)
                {
                    lblMessage.Text = "VRF file upload successfully.";
                }
                else
                {
                    lblMessage.Text = "Getting error.";
                }
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                pnlUploadDOC.Visible = false;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Text = Ex.Message;
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }


    }
    private int UpdateFileStatus(int bookingId, string fileName, string fileType, int actId)
    {
        int status = 0;
        try
        {
            status = objMandate.AddReplaceEmailVRFFile(bookingId, fileName, Convert.ToInt32(Session["UserID"]), fileType, actId);
        }
        catch (Exception Ex)
        {
            return status;
        }
        return status;

    }

    protected void ImgFormat_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
           // fldUploadMandate.Visible = false;
           // btnUPloadExcel.Visible = false;
            DataTable dt = objMandate.GetClientMandatoryFields(Convert.ToInt32(ddlClientName.SelectedValue));
            DataTable dtm = new DataTable("Sheet1");
            dtm.Columns.Add("BookingId", typeof(string));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    dtm.Columns.Add(Convert.ToString(row["DisplayName"]), typeof(string));
                }

                /*foreach (DataColumn col in dt.Columns)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        dtm.Columns.Add(Convert.ToString(row["FieldName"]), typeof(string));
                    }
                }*/

            }
            if (dtm.Columns.Count > 0 && dt.Rows.Count > 0 && dt != null)
            {
                fldUploadMandate.Visible = true;
                btnUPloadExcel.Visible = true;

                using (XLWorkbook wb = new XLWorkbook())
                {

                    wb.Worksheets.Add(dtm);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=MandatoryFields.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            else
            {
                lblMessage.Text = "Client has no mandatory fields.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception Ex)
        {

            throw new Exception(Ex.Message);
        }

    }

    protected void btnUPloadExcel_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        try
        {


            if (fldUploadMandate.HasFile)
            {
                string[] validFileTypes = { ".xls", ".xlsx" };
                string extension = Path.GetExtension(fldUploadMandate.FileName).ToLower();
                string path = string.Format("{0}/{1}", Server.MapPath("~/ExcelUpload"), fldUploadMandate.FileName);
                string connString = string.Empty;
                DataTable dt = new DataTable();
                DataTable dts = new DataTable("UpdateStatus");
                dts.Columns.Add("BookingId", typeof(string));
                dts.Columns.Add("Status", typeof(string));

                //To check upload client fields is correct or not 
                DataTable dtm = objMandate.GetClientMandatoryFields(Convert.ToInt32(ddlClientName.SelectedValue));

                if (validFileTypes.Contains(extension) && dtm != null && dtm.Rows.Count > 0)
                {
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    fldUploadMandate.SaveAs(path);

                    if (extension.Trim() == ".xls")
                    {
                        connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        dt = GeneralUtility.ConvertXSLXtoDataTable(path, connString);

                    }
                    else if (extension.Trim() == ".xlsx")
                    {
                        connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        dt = GeneralUtility.ConvertXSLXtoDataTable(path, connString);

                    }
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        int matched = 0;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dc.ColumnName.ToLower() != "bookingid")
                            {
                                matched = 1;
                                foreach (DataRow dr in dtm.Rows)
                                {
                                    if (dc.ColumnName == Convert.ToString(dr["DisplayName"]))
                                    {
                                        matched = 0;
                                        break;
                                    }
                                }
                            }
                            if (matched == 1)
                            {
                                lblMessage.Visible = true;
                                lblMessage.Text = "Mandatory fields is not correct for selected client.";
                                lblMessage.ForeColor = System.Drawing.Color.Red;
                                return;
                            }

                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            int success = 1;
                            int status = 0;
                            string ResponseStatus = string.Empty;
                            objMesceOL = new Miscellaneous();
                            try
                            {

                                if (string.IsNullOrEmpty(Convert.ToString(dr["bookingID"])))
                                {
                                    success = 0;
                                    ResponseStatus = "BookingID should not empty.";
                                }
                                else if (!GeneralUtility.IsNumeric(Convert.ToString(dr["bookingID"])))
                                {
                                    success = 0;
                                    ResponseStatus = "Booking ID should only numeric value";
                                }
                                else
                                {
                                    objMesceOL.BookingID = Convert.ToInt32(dr["bookingID"]);
                                    objMesceOL.SysUserId = Convert.ToInt32(Session["UserID"]);
                                    foreach (DataColumn dc in dt.Columns)
                                    {
                                        if (dc.ColumnName.ToLower() != "bookingid")
                                        {
                                            foreach (DataRow drm in dtm.Rows)
                                            {
                                                if (dc.ColumnName == Convert.ToString(drm["DisplayName"]))
                                                {
                                                    if (Convert.ToString(drm["FieldName"]).Contains("Misc1"))
                                                    {
                                                        objMesceOL.Miscellaneous1 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc2"))
                                                    {
                                                        objMesceOL.Miscellaneous2 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc3"))
                                                    {
                                                        objMesceOL.Miscellaneous3 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc4"))
                                                    {
                                                        objMesceOL.Miscellaneous4 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc5"))
                                                    {
                                                        objMesceOL.Miscellaneous5 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc6"))
                                                    {
                                                        objMesceOL.Miscellaneous6 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc7"))
                                                    {
                                                        objMesceOL.Miscellaneous7 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc8"))
                                                    {
                                                        objMesceOL.Miscellaneous8 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc9"))
                                                    {
                                                        objMesceOL.Miscellaneous9 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc10"))
                                                    {
                                                        objMesceOL.Miscellaneous10 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc11"))
                                                    {
                                                        objMesceOL.Miscellaneous11 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc12"))
                                                    {
                                                        objMesceOL.Miscellaneous12 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc13"))
                                                    {
                                                        objMesceOL.Miscellaneous13 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc14"))
                                                    {
                                                        objMesceOL.Miscellaneous14 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    else if (Convert.ToString(drm["FieldName"]).Contains("Misc15"))
                                                    {
                                                        objMesceOL.Miscellaneous15 = Convert.ToString(dr[dc.ColumnName]);
                                                    }
                                                    break;
                                                }
                                            }
                                        }

                                    }

                                }
                                if (objMesceOL != null && success == 1)
                                {
                                    status = objMandate.UpdateMiscellaneousField(objMesceOL); ;
                                    if (status > 0)
                                    {
                                        ResponseStatus = "Upload Successfully";
                                    }
                                    else
                                    {
                                        ResponseStatus = "Getting updating error.";
                                    }

                                }
                            }

                            catch (Exception Ex)
                            {
                                ResponseStatus = Ex.Message;
                            }
                            finally
                            {
                                dts.Rows.Add(Convert.ToString(dr["bookingID"]), ResponseStatus);
                            }
                        }
                        DataSet set = new DataSet();
                        set.Tables.Add(dts);
                        if (set.Tables.Count > 0)
                        {
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                foreach (DataTable DT in set.Tables)
                                {
                                    wb.Worksheets.Add(DT);
                                }
                                Response.Clear();
                                Response.Buffer = true;
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("content-disposition", "attachment;filename=MandatoryFieldsUpdateStatus.xlsx");
                                using (MemoryStream MyMemoryStream = new MemoryStream())
                                {
                                    wb.SaveAs(MyMemoryStream);
                                    MyMemoryStream.WriteTo(Response.OutputStream);
                                    Response.Flush();
                                    Response.End();
                                }
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Getting functionality error.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Only excel file will be upload.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;

                }

            }
            else
            {
                lblMessage.Text = "Choose file to upload.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

}
