using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
public partial class PauseRidePopup : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["UserID"] = 377;
        lblMsg.Text = string.Empty;
        if (Request.QueryString["BookingId"] != null && Request.QueryString["Id"] != null && Convert.ToInt32(Request.QueryString["Id"])!= 0)
        {
            txtBookingId.Text = Request.QueryString["BookingId"].ToString();
            Session["UserID"] = Request.QueryString["Id"].ToString();
        }
        else
        {
            lblMsg.Text = "Booking Id Not Found";
            lblMsg.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnPauseDuty_Click(object sender, EventArgs e)
    {
        try
        {
                DataSet dsPauseRide = new DataSet();                           
                objCordrive = new CorDrive();
                dsPauseRide = objCordrive.SubmitPauseRideBookingid(Convert.ToInt32(txtBookingId.Text), Convert.ToInt32(Session["UserID"]));
                string Output = dsPauseRide.Tables[0].Rows[0]["MSG"].ToString();
                if (Output=="Updated Successfully")
                {
                 
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "close", "<script language=javascript>self.close();</script>");
                }
                else
                {
                    lblMsg.Text = Output.ToString();
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
           
           
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.ForeColor = System.Drawing.Color.Red;
        }
    }
}
