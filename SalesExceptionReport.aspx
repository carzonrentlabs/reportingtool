<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalesExceptionReport.aspx.cs"
    MasterPageFile="~/MasterPage.master" Inherits="SalesExceptionReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" type="text/javascript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{        
    if (document.getElementById('<%=ddlMonth.ClientID%>').value == "0")
    {
        alert("Please select Month.");
        document.getElementById('<%=ddlMonth.ClientID%>').focus();
        return false;
    }

    if (document.getElementById('<%=ddlYear.ClientID%>').value =="--Select--")
    {
        alert("Please select Year.");
        document.getElementById('<%=ddlYear.ClientID%>').focus();
        return false;
    }
}
   </script> 

    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Sales Exceptional Report</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 287px; height: 21px;">
                    Month</td>
                <td align="left" style="width: 28px; height: 21px;">
                    <asp:DropDownList ID="ddlMonth" runat="server" Width="174px">
                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                    <asp:ListItem Text="January" Value="1"></asp:ListItem>
                    <asp:ListItem Text="February" Value="2"></asp:ListItem>
                    <asp:ListItem Text="March" Value="3"></asp:ListItem>
                    <asp:ListItem Text="April" Value="4"></asp:ListItem>
                    <asp:ListItem Text="May" Value="5"></asp:ListItem>
                    <asp:ListItem Text="June" Value="6"></asp:ListItem>
                    <asp:ListItem Text="July" Value="7"></asp:ListItem>
                    <asp:ListItem Text="August" Value="8"></asp:ListItem>
                    <asp:ListItem Text="September" Value="9"></asp:ListItem>
                    <asp:ListItem Text="October" Value="10"></asp:ListItem>
                    <asp:ListItem Text="November" Value="11"></asp:ListItem>
                    <asp:ListItem Text="December" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="right" style="width: 57px; height: 21px;">
                    Year</td>
                <td align="left" style="width: 20px; height: 21px;">
                
                    <asp:DropDownList ID="ddlYear" runat="server" Width="174px">
                    </asp:DropDownList>
                   
                </td>
                <td align="right" style="width: 105px; height: 21px;">
                    City Name:</td>
                <td style="width: 183px; height: 21px;" colspan="2">
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;</td>
                
            </tr>
            <tr>
                <td align="center" colspan="8">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
                        OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Top 10 Clients">
                                <ItemTemplate>
                                    <asp:Label ID="lblClientconame" Text='<% #Bind("Clientconame") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrevPrevDuties" Text='<% #Bind("PrevPrevDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amt">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrePrevAmount" Text='<% #Bind("PrePrevAmount") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrevDutiesDuties" Text='<% #Bind("PrevDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amt">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrevAmount" Text='<% #Bind("PrevAmount") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstWeekDuties" Text='<% #Bind("FirstWeekDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblSecondWeekDuties" Text='<% #Bind("SecondWeekDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblThirdWeekDuties" Text='<% #Bind("ThirdWeekDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lbForthWeekDuties" Text='<% #Bind("ForthWeekDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total of DS of the current Month">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToTotal" Text='<% #Bind("TotalDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
