<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientWiseSubmissionSummary.aspx.cs" Inherits="ClientWiseSubmissionSummary" Title="CarzonRent :: Internal software" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
<script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>
<script language="javascript" type="text/javascript">
function goBack()
  {
  window.history.go(-1)
  }
</script>
<link href="App_Themes/Stylesheet/HertzInt.css" type="text/css" rel="stylesheet" />
</head>
<body>
<%--<link href="App_Themes/images/logo_carzonret.gif" type="text/css" rel="stylesheet" />--%>

<form id="form1" runat="server">

<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <table width="802" border="0" align="center">
        <tbody>
            <tr>
            <td>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/images/logo_carzonret.gif" />
            </td>
            </tr>
            
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Client Wise Submission Summary</u></strong>
                </td>
            </tr>
           
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                &nbsp;&nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>

        <tr>
       
        <td align="center" colspan="8">
        <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found" AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="grvMergeHeader_RowCreated">
        <Columns>
        
        <asp:TemplateField HeaderText="Client Name">
        <ItemTemplate>
        <asp:Label ID="lblClientName" Text='<% #Bind("ClientName") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
     
        <asp:TemplateField HeaderText="Submission Type">
        <ItemTemplate>
        <asp:Label ID="lblSubmissionType" Text='<% #Bind("SubmissionType") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Submitted">
        <ItemTemplate>
        <asp:Label ID="lblSubmited" Text='<% #Bind("Submited") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        </asp:TemplateField> 
        
        <asp:TemplateField HeaderText="Not Submitted">
        <ItemTemplate>
        <asp:Label ID="lblNotSubmited" Text='<% #Bind("NotSubmited") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>                 
        
        </Columns>
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        </td>
        </tr>
        </tbody>
    </table>
    
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
        top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
        scrolling="no" height="182"></iframe>
    </div>

</form>
</body>
</html>