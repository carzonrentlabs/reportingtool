﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RefundAmount.aspx.cs" Inherits="RefundAmount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="JQuery/moment.js" type="text/javascript"></script>

    <script src="JQuery/ui.core.js" type="text/javascript"></script>

    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script src="ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>

    <center>
        <table cellpadding="0" width="700px" cellspacing="0" border="0">
            <tr>
                <td colspan="5" align="center" style="background-color: #ccffff; height: 30px;">
                    <b>Refund Amount
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 25px; white-space: nowrap;" a>
                    <b>Booking ID</b>
                </td>
                <td style="height: 25px; white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtBookingId" runat="server"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnGetDetails" runat="Server" Text="Get It" OnClick="btnGetDetails_Click" />
                </td>
                <td style="height: 25px; white-space: nowrap;" align="left">
                </td>
                <td style="height: 25px; white-space: nowrap;">
                    &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Company Name:</b></td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label></td>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Gueast Name:</b></td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblGueastName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Pickup Date: </b>
                </td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblPickupdate" runat="server"></asp:Label></td>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Pickup Time:</b></td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblPickupTime" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Service:</b>
                </td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblService" runat="server"></asp:Label></td>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Total Cost:</b></td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:TextBox ID="txtTotalCost" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Basic: </b>
                </td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblBasic" runat="server"></asp:Label></td>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Service Tax:</b>
                </td>
                <td style="height: 25px; white-space: nowrap;">
                    <asp:Label ID="lblServiceTax" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 25px; white-space: nowrap;">
                    <b>Refund Amount: </b>
                </td>
                <td style="height: 25px; white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtRefundAmount" runat="server"></asp:TextBox>
                    &nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btnRefund" runat="server" Text="Refund" OnClick="btnRefund_Click" /></td>
                <td style="height: 25px; white-space: nowrap;" align="left">
                </td>
                <td style="height: 25px; white-space: nowrap;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div>
                        <asp:GridView ID="grdRefundAmountDetails" runat="server">
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
