using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;
public partial class CorDriveReport_AutoAllocationSummary : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(5000);
        DataSet dsAllocationSummary = new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
                return;
            }
            else
            {
                //dsAllocationSummary = objCordrive.GetAllocationReport(Convert.ToDateTime("2015-10-19"));
                dsAllocationSummary = objCordrive.GetAllocationReport(Convert.ToDateTime(txtFromDate.Text));
                if (dsAllocationSummary.Tables[0].Rows.Count > 0)
                {
                    
                    grdAutoAllocation.DataSource = dsAllocationSummary.Tables[0];
                    grdAutoAllocation.DataBind();

                    grdAutoAllocationDetails.DataSource = dsAllocationSummary.Tables[1];
                    grdAutoAllocationDetails.DataBind();
                }
                else
                {
                    grdAutoAllocation.DataSource = null;
                    grdAutoAllocation.DataBind();

                    grdAutoAllocationDetails.DataSource = null;
                    grdAutoAllocationDetails.DataBind();

                    lblMessate.Text = "Record not available.";
                    lblMessate.Visible = true;
                    lblMessate.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }
}
