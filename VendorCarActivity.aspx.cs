﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ExcelUtil;
using System.Globalization;

public partial class VendorCarActivityUpendra : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsVendorCarDateWise = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindBrachDropDownlist();
            BindCategoryDropDownlist();
        }
    }

    protected void bntGetDateWise_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        try
        {
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblMessage.Text = "Please Select Date.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
            }
            else
            {
                dsVendorCarDateWise = objCordrive.CaractivityReport(Convert.ToInt16(ddlBranch.SelectedValue), Convert.ToInt16(ddlCategory.SelectedValue), Convert.ToDateTime(txtDate.Text));

                if (dsVendorCarDateWise.Tables[0].Rows.Count > 0)
                {
                    grdVendorCarActivity.DataSource = dsVendorCarDateWise.Tables[0];
                    grdVendorCarActivity.DataBind();
                    lblMessage.Text = "";
                    lblMessage.Visible = false;
                }
                else
                {
                    grdVendorCarActivity.DataSource = null;
                    grdVendorCarActivity.DataBind();
                    lblMessage.Text = "Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
        }
    }

    protected void bntDateWiseExprot_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtDate.Text))
        {
            lblMessage.Text = "Please Select Date.";
            lblMessage.Visible = true;
            lblMessage.ForeColor = Color.Red;
        }
        else
        {
            lblMessage.Text = "";
            lblMessage.Visible = false;

            DataSet dsVendorCarExportToExcel = new DataSet();
            objCordrive = new CorDrive();

            dsVendorCarExportToExcel = objCordrive.CaractivityReport(Convert.ToInt16(ddlBranch.SelectedValue), Convert.ToInt16(ddlCategory.SelectedValue), Convert.ToDateTime(txtDate.Text));

            WorkbookEngine.ExportDataSetToExcel(dsVendorCarExportToExcel, "VendorCarActivityReport.xls");
        }
    }

    public void BindBrachDropDownlist()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetVendorDetailsByCityID();
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "CityName";
                    ddlBranch.DataValueField = "CityID";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                    ddlBranch.Items.Insert(0, new ListItem("--All--", "0"));
                }
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }

    public void BindCategoryDropDownlist()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsCategory = new DataSet();

            dsCategory = objCordrive.GetVendorDetailsByCategoryID();

            if (dsCategory != null)
            {
                if (dsCategory.Tables.Count > 0)
                {
                    ddlCategory.DataTextField = "CarCatName";
                    ddlCategory.DataValueField = "CarCatID";
                    ddlCategory.DataSource = dsCategory;
                    ddlCategory.DataBind();
                    ddlCategory.Items.Insert(0, new ListItem("--All--", "0"));
                }
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
}