﻿<%@ Page Title="Vendor Usage Report" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="OnCallVendorUsageReport.aspx.cs" Inherits="CorDriveReport_OnCallVendorUsage_Report" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
                <tr>
                    <td colspan="5" align="center">
                        <b>On Call Vendor Usage Report</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblBranch" runat="server">Branch</asp:Label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlBranch" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblToDate" runat="server">To date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;
                        <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="Repeater">
            <asp:Repeater ID="rptVendorUsageReport" runat="server">
                <HeaderTemplate>
                    <table id="sort_table" width="100%">
                        <thead>
                            <tr>
                                    <th align="center">
                                    Sr.No
                                    </th>
                                    <th align="center">
                                        Car Number
                                    </th>
                                    <th align="center">
                                        Car Category
                                    </th>
                                    <th align="center">
                                        Car Model
                                    </th>
                                    <th align="center">
                                        Vendor Name
                                    </th>
                                    <th align="center">
                                        Branch / Unit
                                    </th>
                                    <th align="center">
                                        Number Of Bookings
                                    </th>
                                    <th align="center">
                                        Revenue
                                    </th>
                                    <th align="center">
                                        Vendor Share
                                    </th>
                                    <th align="center">
                                        COR Share
                                    </th>
                                    <th align="center">
                                        Revenue sharing PC
                                    </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="row_<%#Eval("Id")  %>" style="font-family: Arial, Verdana, Sans-Serif;">
                        <td style="text-align: center;">&nbsp;&nbsp;
                            <%#Eval("Id")%>
                        </td>
                        <td style="text-align:left;">&nbsp;&nbsp;
                            <%#Eval("CarNumber")%>
                        </td>
                        <td style="text-align: left;">&nbsp;&nbsp;
                            <%#Eval("CarCategory")%>
                        </td>
                        <td style="text-align: left;">&nbsp;&nbsp;
                            <%#Eval("CarModel")%>
                        </td>
                        <td style="text-align: left;">&nbsp;&nbsp;
                            <%#Eval("VendorName")%>
                        </td>
                        <td style="text-align: left;">&nbsp;&nbsp;
                            <%#Eval("BranchName")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("NoOfBooking")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Revenue")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("VendorShare")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("CorShare")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("RevenueSharePC")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
            <div align="center" id="dvPaging" runat="server" visible="false">
                <asp:LinkButton ID="lnkPrev" runat="server" Text="Prev" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkPrev_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkNext" runat="server" Text="Next" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkNext_Click"></asp:LinkButton>
            </div>
        </div>
    </center>
</asp:Content>
