using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;
public partial class CorDriveReport_DispositionReport : System.Web.UI.Page
{
    DataSet dsgvDispositionCall = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["Userid"] = 3122;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        if (txtFromDate.Text == "")
        {
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please enter FromDate !');</script>");
            txtFromDate.Focus();
            return;
        }

        if (txtToDate.Text == "")
        {
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please enter ToDate !');</script>");
            txtToDate.Focus();
            return;
        }

        BindGrid();
    }

    public void BindGrid()
    {
        try
        {
            objCordrive = new CorDrive();
            dsgvDispositionCall = objCordrive.GetDispositionCallList(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()));
            if (dsgvDispositionCall.Tables[0].Rows.Count > 0)
            {
                gvDispositionCall.DataSource = dsgvDispositionCall.Tables[0];
                gvDispositionCall.DataBind();
            }
            else
            {
                gvDispositionCall.DataSource = dsgvDispositionCall.Tables[0];
                gvDispositionCall.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }

    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }


            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }


            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }

            lblMsg.Text = "";
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From Date and To Date can not be empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsgvDispositionCall = objCordrive.GetDispositionCallList(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()));
            WorkbookEngine.ExportDataSetToExcel(dsgvDispositionCall, "DispositionCall" + ".xls");
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        InitializeControls(Form);
    }
}
