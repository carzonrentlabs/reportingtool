using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ExcelUtil;
using ReportingTool;

public partial class HandOver : System.Web.UI.Page
{
    static DataTable GridViewDT = new DataTable();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMsg.Text = string.Empty;
        if (!Page.IsPostBack)
        {
            getRelationShipManagers();
            BindGrid();
        }
    }

    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }

    private void BindGrid()
    {
        DataSet ds = new DataSet();
        objCordrive = new CorDrive();
        ds = objCordrive.GetHardCopyHandoverDetails(Convert.ToInt32(Session["UserID"]), true);
        GridViewDT = ds.Tables[0];
        if (ds.Tables[0].Rows.Count > 0)
        {
            grdHandover.DataSource = ds.Tables[0];
            grdHandover.DataBind();
            btnSaveHandover.Visible = true;
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Bookings not available for HandOver.";
            grdHandover.DataSource = null;
            grdHandover.DataBind();
            btnSaveHandover.Visible = false;
        }
    }

    protected void getRelationShipManagers()
    {
        clsAdmin objAdmin = new clsAdmin();
        ddlUserName.DataTextField = "UserName";
        ddlUserName.DataValueField = "SysUserID";
        DataSet dsusername = new DataSet();
        dsusername = objAdmin.getUserListBilling();
        ddlUserName.DataSource = dsusername;
        ddlUserName.DataBind();
        ddlUserName.Items.Insert(0, new ListItem("--Select--", "0"));
    }

    protected void grdHandover_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "RemoveEntry")
            {
                foreach (DataRow dtRow in GridViewDT.Rows)
                {
                    if (dtRow["BookingID"].ToString().Trim() == e.CommandArgument.ToString().Trim())
                    {
                        GridViewDT.Rows.Remove(dtRow);
                        grdHandover.DataSource = GridViewDT;
                        grdHandover.DataBind();
                        break;
                    }
                }
                if (grdHandover.Rows.Count <= 0)
                {
                    btnSaveHandover.Visible = false;
                    lblMsg.Visible = true;
                    lblMsg.Text = "No Record available for Handover.";
                }
                else
                {
                    btnSaveHandover.Visible = true;
                    lblMsg.Visible = false;
                    lblMsg.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
            //throw ex;
        }
    }
    protected void btnSaveHandover_Click(object sender, EventArgs e)
    {
        if (ddlUserName.SelectedValue == "0")
        {
            ShowMessage("Please select the Handover user name.");
            ddlUserName.Focus();
            return;
        }

        if (grdHandover.Rows.Count > 0)
        {
            int k = 0;
            foreach (GridViewRow dtRow in grdHandover.Rows)
            {
                CheckBox chkHandover = (CheckBox)dtRow.FindControl("chkHandover");
                Label lblBookingId = (Label)dtRow.FindControl("lblBookingId");
                if (chkHandover.Checked)
                {
                    k += 1;

                    //update handover status
                    objCordrive = new CorDrive();
                    objCordrive.GetHardCopyHandover(Convert.ToInt32(lblBookingId.Text), Convert.ToInt32(Session["UserID"]), Convert.ToInt32(ddlUserName.SelectedValue));
                    BindGrid();
                    lblMsg.Text = "Handover Successfully.";
                    lblMsg.Visible = true;
                }
            }
            if (k == 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Please select at least 1 booking for Handover.";
            }
            //else
            //{
            //    lblMsg.Visible = false;
            //    lblMsg.Text = "";
            //}
        }
        else
        {
            lblMsg.Visible = true;
            lblMsg.Text = "No Record available for Handover.";
        }
    }
}