﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_BranchChauffeurList : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
            BindCarModelDropDowmnList();
        }
        //bntGet.Attributes.Add("Onclick", "return validate()");
        //bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    #region "Private Void Bind DropDownist Function"
    private void BindBrachDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "Cityname";
                    ddlBranch.DataValueField = "CityId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    private void BindCarModelDropDowmnList()
    {
        try
        {
            DataSet _objDScarModel = new DataSet();
            _objDScarModel = objCordrive.GetActiveCarModel();
            if (_objDScarModel != null)
            {
                if (_objDScarModel.Tables.Count > 0)
                {
                    ddlCarModel.DataTextField = "ModelName";
                    ddlCarModel.DataValueField = "ModelId";
                    ddlCarModel.DataSource = _objDScarModel.Tables[0];
                    ddlCarModel.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    private void BindChaufferListGrid()
    {
        try
        {
            int _cityId = 0, _carModelId = 0, _ddlVdpYN = 0, _chaufer_source = 0, _status = 0;
            DataSet dsChauffeur = new DataSet();
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            _status = Convert.ToInt32(ddlStatus.SelectedValue.ToString() == null || ddlStatus.SelectedValue.ToString() == "" ? "0" : ddlStatus.SelectedValue.ToString());
            _chaufer_source = Convert.ToInt32(ddlChauffeurSource.SelectedValue.ToString() == null || ddlChauffeurSource.SelectedValue.ToString() == "" ? "0" : ddlChauffeurSource.SelectedValue.ToString());
            _carModelId = Convert.ToInt32(ddlCarModel.SelectedValue.ToString() == null || ddlCarModel.SelectedValue.ToString() == "" ? "0" : ddlCarModel.SelectedValue.ToString());
            _ddlVdpYN = Convert.ToInt32(ddlVdpYN.SelectedValue.ToString() == null || ddlVdpYN.SelectedValue.ToString() == "" ? "0" : ddlVdpYN.SelectedValue.ToString());
            _chaufer_source = Convert.ToInt32(ddlChauffeurSource.SelectedValue.ToString() == null || ddlChauffeurSource.SelectedValue.ToString() == "" ? "0" : ddlChauffeurSource.SelectedValue.ToString());

            dsChauffeur = objCordrive.BrachChauffuerDetails(_cityId, _status, _carModelId, _ddlVdpYN, _chaufer_source, 0);
            if (dsChauffeur != null)
            {
                if (dsChauffeur.Tables.Count > 0)
                {
                    gvChauffuer.DataSource = dsChauffeur;
                    gvChauffuer.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    #endregion
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindChaufferListGrid();

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            int _cityId = 0, _carModelId = 0, _ddlVdpYN = 0, _chaufer_source = 0, _status = 0, counter = 0;
            DataSet dsChauffeur = new DataSet();
            DataTable dt = new DataTable();
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            _status = Convert.ToInt32(ddlStatus.SelectedValue.ToString() == null || ddlStatus.SelectedValue.ToString() == "" ? "0" : ddlStatus.SelectedValue.ToString());
            _chaufer_source = Convert.ToInt32(ddlChauffeurSource.SelectedValue.ToString() == null || ddlChauffeurSource.SelectedValue.ToString() == "" ? "0" : ddlChauffeurSource.SelectedValue.ToString());
            _carModelId = Convert.ToInt32(ddlCarModel.SelectedValue.ToString() == null || ddlCarModel.SelectedValue.ToString() == "" ? "0" : ddlCarModel.SelectedValue.ToString());
            _ddlVdpYN = Convert.ToInt32(ddlVdpYN.SelectedValue.ToString() == null || ddlVdpYN.SelectedValue.ToString() == "" ? "0" : ddlVdpYN.SelectedValue.ToString());
            _chaufer_source = Convert.ToInt32(ddlChauffeurSource.SelectedValue.ToString() == null || ddlChauffeurSource.SelectedValue.ToString() == "" ? "0" : ddlChauffeurSource.SelectedValue.ToString());
            //Get Result From Data base
            dsChauffeur = objCordrive.BrachChauffuerDetails(_cityId, _status, _carModelId, _ddlVdpYN, _chaufer_source, 0);
            //For Export To Excel
            if (dsChauffeur != null)
            {
                if (dsChauffeur.Tables.Count > 0)
                {
                    dt = dsChauffeur.Tables[0];
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Response.ClearContent();
                        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                        Response.Clear();
                        Response.AppendHeader("content-disposition", "attachment;filename=BranchChauffeurList.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "application/vnd.ms-excel";
                        this.EnableViewState = false;
                        Response.Write("\r\n");
                        Response.Write("<table border = '1' align = 'center'> ");
                        int[] iColumns = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,25 };
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            counter = counter + 1;
                            if (i == 0)
                            {
                                Response.Write("<tr>");
                                Response.Write("<td align='left'><b>S.No.</b></td>");
                                for (int j = 0; j < iColumns.Length; j++)
                                {
                                    if (dt.Columns[iColumns[j]].Caption.ToString() == "ChauffeurName")
                                    {
                                        Response.Write("<td align='left'><b>Chauffeur Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "MobileNo")
                                    {
                                        Response.Write("<td align='left'><b>Chauffeur Contact</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarVendorName")
                                    {
                                        Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                    }

                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarVendorName")
                                    {
                                        Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorContractNo")
                                    {
                                        Response.Write("<td align='left'><b>Vendor Contact</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ChauffeurSource")
                                    {
                                        Response.Write("<td align='left'><b>Chauffeur Source</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "AssociatedcarNo")
                                    {
                                        Response.Write("<td align='left'><b>Associted Car Numbers</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofActivation")
                                    {
                                        Response.Write("<td align='left'><b>Date of Activation</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DocumentUploadedBy")
                                    {
                                        Response.Write("<td align='left'><b>Doument Uploaded by</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DocumentApprovedBy")
                                    {
                                        Response.Write("<td align='left'><b>Doument Approved by</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofModification")
                                    {
                                        Response.Write("<td align='left'><b>DateofModification</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofDeactivation")
                                    {
                                        Response.Write("<td align='left'><b>Date of Deactivation</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DeactivationBy")
                                    {
                                        Response.Write("<td align='left'><b>Deactivated by</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "chaufStatus")
                                    {
                                        Response.Write("<td align='left'><b>Status</b></td>");
                                    }

                                }
                                Response.Write("</tr>");
                            }
                            Response.Write("<tr>");
                            Response.Write("<td align='left'>" + counter + "</td>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                            }

                            Response.Write("</tr>");
                        }
                        Response.Write("</table>");
                        Response.End();
                    }
                    else
                    {

                        Response.Write("<table border = 1 align = 'center' width = '100%'>");
                        Response.Write("<td align='center'><b>No Record Found</b></td>");
                        Response.Write("</table>");
                        Response.End();

                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }

    }
    protected void rvChauffuer_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvChauffuer.PageIndex = e.NewPageIndex;
            BindChaufferListGrid();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void gvChauffuer_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _chauffer_source = (Label)e.Row.FindControl("lblChaufferSource");
                LinkButton _btnPassport = (LinkButton)e.Row.FindControl("btnPassport");
                LinkButton _btnLicence = (LinkButton)e.Row.FindControl("btnLicence");
                LinkButton _btnPolice = (LinkButton)e.Row.FindControl("btnPolice");
                LinkButton _btnResume = (LinkButton)e.Row.FindControl("btnResume");
                LinkButton _btnPhotograph = (LinkButton)e.Row.FindControl("btnPhotograph");
                HiddenField _hdndocument = (HiddenField)e.Row.FindControl("hdndocument");



                if (_chauffer_source != null)
                {
                    if (_chauffer_source.Text.ToString() == "COR")
                    {
                        _btnPassport.Enabled = false;
                        _btnLicence.Enabled = false;
                        _btnPolice.Enabled = false;
                        _btnResume.Enabled = false;
                        _btnPhotograph.Enabled = false;

                        _btnPassport.Text = "";
                        _btnLicence.Text = "";
                        _btnPolice.Text = "";
                        _btnResume.Text = "";
                        _btnPhotograph.Text = "";

                    }
                    else
                    {
                        if (_hdndocument.Value == "1")
                        {
                            _btnPassport.Enabled = true;
                            _btnLicence.Enabled = true;
                            _btnPolice.Enabled = true;
                            _btnResume.Enabled = true;
                            _btnPhotograph.Enabled = true;

                            _btnPassport.Text = "View";
                            _btnLicence.Text = "View";
                            _btnPolice.Text = "View";
                            _btnResume.Text = "View";
                            _btnPhotograph.Text = "View";
                        }
                        else
                        {
                            _btnPassport.Enabled = false;
                            _btnLicence.Enabled = false;
                            _btnPolice.Enabled = false;
                            _btnResume.Enabled = false;
                            _btnPhotograph.Enabled = false;

                            _btnPassport.Text = "";
                            _btnLicence.Text = "";
                            _btnPolice.Text = "";
                            _btnResume.Text = "";
                            _btnPhotograph.Text = "";
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
}