<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IncentiveReport.aspx.cs" Inherits="CorDriveReport_IncentiveReport" Title="Incentive Report" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/moment.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <%--<link href="../CSSReveal/reveal.css" rel="stylesheet" type="text/css" />--%>
    <%--<cc1:ToolkitScriptManager runat="server">
    </cc1:ToolkitScriptManager>--%>
    <script src="../ScriptsReveal/jquery-1.4.4.min.js" type="text/javascript"></script>
    <%--<script src="ScriptsReveal/ScrollableGridPlugin.js" type="text/javascript"></script>--%>
   <%--  <script language="javascript" type="text/javascript">
        function pageLoad(sender, args)
        {
              $("#<%=btnGet.ClientID%>").click(function () {
               if(document.getElementById('<%=ddlEvaluationType.ClientID%>').value == "0") {
                    alert("Please Evaluation Type");
                    return false;
                }
               else {
                    ShowProgress();
                }
            });
            
          
        }
          function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        function ShowProgressKill() 
        {  
            setTimeout(function () 
            {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            },200);
        }
     
      </script>--%>
    <center>
        <%--<table cellpadding="0" cellspacing="0" border="0" style="width: 60%">--%>
         <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
           <tr>
                <td colspan="3" align="center">
                   <b>Incentive Report</b> 
                 </td>
                 
            </tr>
             <tr>
                <td colspan="3" align="center">
               
                 </td>
                 
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                <b>Select EvaluationType :</b>
                     <asp:DropDownList ID="ddlEvaluationType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEvaluationType_SelectedIndexChanged">
                     <asp:ListItem Value="0">Select Evaluation Type</asp:ListItem>
                     <asp:ListItem Value="1">MONTHLY</asp:ListItem>
                     <asp:ListItem Value="2">QUARTERLY</asp:ListItem>
                     </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;" align="center">
                    <b>Evaluation Date :</b>
                     <asp:DropDownList ID="ddlEvalutionDate" runat="server">
                     <asp:ListItem Value="0">--No Data--</asp:ListItem>
                     </asp:DropDownList>
                </td>
                     <td style="height: 20px; white-space: nowrap;">
                    <asp:Button ID="btnGet" runat="Server" Text="Get It" OnClick="btnGet_Click"/>
                    &nbsp;&nbsp;
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
            <tr>
            <td colspan="3">
            <table cellpadding="0" cellspacing="0" border="1" style="width: 100%">
             <tr>
            <td colspan="3" align="center">
            <asp:GridView ID="gvIncentive" runat="server" BorderColor="Black" BorderStyle="None" BorderWidth="1px"
                                        CellPadding="3" HeaderStyle-Width="70px" EmptyDataText="No Data Found"/>
            </td>
            </tr>
            </table>
            </td>
           
</tr>
        </table>
       
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>

