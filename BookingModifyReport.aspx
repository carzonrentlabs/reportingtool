<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingModifyReport.aspx.cs" Inherits="BookingModifyReport" 
MasterPageFile="~/MasterPage.master" Title="CarzonRent :: Internal software"%>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
   <script src="../JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>.
    
     <script type ="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <%--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">--%>
  <script type ="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script type ="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
           
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }        
        }
    </script>
  <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width:30%">
                <tr>
                    <td colspan="3" align="center">
                        <b>Booking Modify Report</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblFromDate" runat="server">Modify Date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                    </td>
                     <td style="height: 20px;">
                       <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px;">
                         <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div >
            <asp:Repeater ID="rptVendorUsageReport" runat="server">
                <HeaderTemplate>
                    <table id="sort_table" width="100%" cellpadding="0" cellspacing="0" border="1">
                        <thead>
                            <tr>
                                   <%-- <th align="center">
                                    Sr.No
                                    </th>--%>
                                    <th align="center">
                                       Booking Id
                                    </th>
                                    <th align="center">
                                       City Name
                                    </th>
                                    <th align="center">
                                        Car Requested
                                    </th>
                                    <th align="center">
                                       PickUp Date
                                    </th>
                                    <th align="center">
                                        PickUp Time
                                    </th>
                                    <th align="center">
                                        DropOff Date
                                    </th>
                                    <th align="center">
                                        DropOff Time
                                    </th>
                                    <th align="center">
                                        Pickup Add
                                    </th>
                                    <th align="center">
                                        Flgt No
                                    </th>
                                    <th align="center">
                                       Report Contact
                                    </th>
                                    <th align="center">
                                        Cancel Reason
                                    </th>
                                    <th align="center">
                                       Remarks
                                    </th>
                                     <th align="center">
                                       Visited Cities
                                    </th>
                                    <th align="center">
                                        Track Id
                                    </th>
                                    <th align="center">
                                      CreatedBy
                                    </th>
                                     <th align="center">
                                       Create Date
                                    </th>
                                    <th align="center">
                                        Modify Name
                                    </th>
                                    <th align="center">
                                      Modify Date
                                    </th>
                                     
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                       <%-- <td style="text-align: center;">&nbsp;&nbsp;
                            <%#Eval("Id")%>
                        </td>--%>
                        <td style="text-align:left;">
                            <%#Eval("BookingID")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("CityName")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("CarRequested")%>
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("PickUpDate")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("PickUpTime")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DropOffDate")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DropOffTime")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("PickUpAdd")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("FlgtNo")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("ReportContact")%>&nbsp;
                        </td>
                        
                         <td style="text-align: center;">
                            <%#Eval("CancelReason")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Remarks")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("VisitedCities")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("trackId")%>&nbsp;
                        </td>
                        
                          <td style="text-align: center;">
                            <%#Eval("CreatedBy")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Createdate")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("ModifyedName")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("ModifyDate")%>&nbsp;
                        </td>
                     </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
            <div align="center" id="dvPaging" runat="server" visible="false">
                <asp:LinkButton ID="lnkPrev" runat="server" Text="Prev" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkPrev_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkNext" runat="server" Text="Next" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkNext_Click"></asp:LinkButton>
            </div>
        </div>
    </center>
</asp:Content>
