<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PauseRidePopup.aspx.cs" Inherits="PauseRidePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Authorize Pause Duty</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellpadding ="2" cellspacing ="2" border ="0">
                <tr>
                    <td align="center" colspan="2" class="TableTD" valign="top">
                        <b>Authorize Pause Duty</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label ID="lblMsg" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        Booking Id :</td>
                    <td align="left">
                        &nbsp;<asp:TextBox ID="txtBookingId" runat="server" ReadOnly ="true"/>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Tahoma; font-size: 10pt; text-align: center;" colspan="2">
                        <asp:Button ID="btnPauseDuty" runat="server" Text="Submit" OnClick="btnPauseDuty_Click" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
