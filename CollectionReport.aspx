<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CollectionReport.aspx.cs" Inherits="CollectionReport"
    Title="Myles" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentPage" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="javascript" type="text/javascript">
function dateReg(obj)
{
    if(obj.value!="")
    {
        //  alert(obj.value);      
        var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-([1-9][0-9][0-9][0-9]))/ 
        if(reg.test(obj.value))
        {
            //alert('valid');      
        }  
        else
        {
            alert('notvalid');
            obj.value="";
        }
    }     
}

function showDate(sender, args)
{
    if(sender._textbox.get_element().value == "")
    {
        var todayDate = new Date();
        sender._selectedDate = todayDate;
    }
}
    </script>

    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="height: 30px">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <a href="">
                        <img src="../App_Themes/logo_myles.gif" border="0" style="height: 28px; width: 109px"
                            alt="" />
                    </a>
                </td>
            </tr>
            <tr>
                <td style="height: 30px; text-align: center;">
                    <h3>
                        Myles Collection Report</h3>
                </td>
            </tr>
            <tr>
                <td style="height: 30px;" align="center">
                    City Name&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlcity" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 30px;" align="center">
                    From Date&nbsp;&nbsp;
                    <asp:TextBox ID="txt_FromDate" runat="server" onblur="dateReg(this);" MaxLength="10"
                        autocomplete="off" Width="145px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" OnClientShowing="showDate" runat="server"
                        TargetControlID="txt_FromDate" Format="MM-dd-yyyy">
                    </cc1:CalendarExtender>
                    &nbsp;&nbsp; To Date&nbsp;&nbsp;
                    <asp:TextBox ID="txt_ToDate" runat="server" onblur="dateReg(this);" MaxLength="10"
                        autocomplete="off" Width="145px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" OnClientShowing="showDate" runat="server"
                        TargetControlID="txt_ToDate" Format="MM-dd-yyyy">
                    </cc1:CalendarExtender>
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 30px;" align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnExcel" runat="server" Text="Export To Excel" OnClick="btnExcel_Click" />
                </td>
            </tr>
            <tr>
                <td style="height: 30px;">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView runat="server" ID="grv_CollectionReport" PageSize="20" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="White"
                        BorderColor="#CC9966" BorderWidth="1px" CellPadding="4"
                        OnPageIndexChanging="grv_CollectionReport_PageIndexChanging" BorderStyle="Solid">
                        <Columns>
                            <asp:TemplateField HeaderText="Collection Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblCollectionDate" Text='<% #Bind("CollectionDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingID">
                                <ItemTemplate>
                                    <asp:Label ID="lblbooking" Text='<% #Bind("BookingID") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="trackid">
                                <ItemTemplate>
                                    <asp:Label ID="lbltrackid" Text='<% #Bind("trackid") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ReceiptID">
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptID" Text='<% #Bind("ReceiptID") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblLocation" Text='<% #Bind("Location") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Receipt Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptAmount" Text='<% #Bind("ReceiptAmount") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Advance Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdvanceAmt" Text='<% #Bind("AdvanceAmt") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mode of Receipt">
                                <ItemTemplate>
                                    <asp:Label ID="ddlModeofReceipt" Text='<% #Bind("ModeofReceipt") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Batch Value">
                                <ItemTemplate>
                                    <asp:Label ID="lbltbv" runat="server" Text='<% #Bind("Total_Batch_Value") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duty Status">
                                <ItemTemplate>
                                    <asp:Label ID="txtDutyStatus" Text='<% #Bind("DutyStatus") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Accounting Date">
                                <ItemTemplate>
                                    <asp:Label ID="txtAccountingDate" Text='<% #Bind("AccountingDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date of Duty close">
                                <ItemTemplate>
                                    <asp:Label ID="txtDutyClose" Text='<% #Bind("DateClose") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total cost - billing value">
                                <ItemTemplate>
                                    <asp:Label ID="lblToatalCost" Text='<% #Bind("TotalCost") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Deposit Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepositeDate" Text='<% #Bind("DepositeDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Deposit Bank name">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" Text='<% #Bind("BankName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblClient" Text='<% #Bind("ClientCoName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                        <SelectedRowStyle BackColor="#FFCC66" ForeColor="#663399" Font-Bold="True" />
                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                        <RowStyle BackColor="White" ForeColor="#330099" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
