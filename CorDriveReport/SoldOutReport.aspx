﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SoldOutReport.aspx.cs" Inherits="CorDriveReport_SoldOutReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            var dateFrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
            var dateTo = document.getElementById('<%=txtToDate.ClientID %>').value;
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
            else if (parseInt(DateDiff(dateFrom, dateTo)) > 31) {
                alert("Date difference should not be greater than 31 days");
                return false;
            }
        }
        function DateDiff(date1, date2) {

            var firstDate = new Date(date1)
            var secondDate = new Date(date2)
            var datediff = secondDate.getTime() - firstDate.getTime();
            return (datediff / (24 * 60 * 60 * 1000));
        }
    </script>
    <div align="center">
        <b>Sold Out Report</b>
    </div>
    <br />
    <div>
        <table cellpadding="0" cellspacing="0" border="0" align="center">
            <tr>
                <td align="left">
                    <asp:Label ID="lblBranch" runat="server">Branch</asp:Label>&nbsp;&nbsp;
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlBranch" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Label ID="lblToDate" runat="server">To Date</asp:Label>&nbsp;&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                </td>
                <td align="left">
                    &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <asp:Literal ID="ltlSummary" runat="server"></asp:Literal>
    </div>
</asp:Content>
