<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BranchWisedutiesSummary_New.aspx.cs" Inherits="BranchWisedutiesSummary"
    Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script language="Javascript" src="App_Themes/CommonScript.js"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Branch Wise Duties Summary</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 287px">
                    From Date
                </td>
                <td align="left">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 57px">
                    To Date
                </td>
                <td align="left" style="width: 6px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 20px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 105px">
                    City Name:
                </td>
                <td style="width: 183px">
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
                        OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="grvMergeHeader_RowCreated"
                        OnRowEditing="GridView1_RowEditing">
                        <Columns>
                            <asp:TemplateField HeaderText="City Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="">
                                        <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server"></asp:Label>
                                        <asp:Label Visible="false" ID="lbl_CityID" runat="server" Text='<%#Eval("CityID") %>'></asp:Label>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToOpenDuties" Text='<% #Bind("BillToOpenDuties48") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pending Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToPendingDuties" Text='<% #Bind("BillToPendingDuties48") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closed Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToClosedDuties" Text='<% #Bind("BillToClosedDuties48") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToOpenDuties4" Text='<% #Bind("OpenDuties4896Hr") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pending Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToPendingDuties1" Text='<% #Bind("PendingDuties4896Hr") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closed Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToPendingDuties1" Text='<% #Bind("ClosedDuties4896Hr") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblSSCBatch" Text='<% #Bind("OpenDutiesGreater96Hr") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pending Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblCCSBatch" Text='<% #Bind("PendingDutiesGreater96Hr") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closed Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblDSNotSubmitted" Text='<% #Bind("ClosedDutiesGreater96Hr") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotSubmittedtoSSC" Text='<% #Bind("GrandTotal") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblDirectOpenDuties" Text='<% #Bind("DirectOpenDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pending Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblDirectPendingDuties" Text='<% #Bind("DirectPendingDuties") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closed Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblDirectClosedDuties" Text='<% #Bind("DirectClosedDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=" BTC Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblDirectTotal" Text='<% #Bind("DirectTotal") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
