using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class ClientWisedetails : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCity();
            BindCustomerName();
            getRelationShipManagers();
            getNationalAccManagers();
            txtImplant.Visible = false;
        }

    }
    protected void BindCity()
    {
        ddllocation.DataTextField = "CityName";
        ddllocation.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        ddllocation.DataSource = dsLocation;
        ddllocation.DataBind();
    }
    protected void BindCustomerName()
    {
        ddlCustomerName.DataTextField = "ClientCoName";
        ddlCustomerName.DataValueField = "ClientCoID";
        DataSet dsCustName = new DataSet();
        dsCustName = objAdmin.getCorpCompanyNames();
        ddlCustomerName.DataSource = dsCustName;
        ddlCustomerName.DataBind();
        ddlCustomerName.Items.Insert(0, "--Select--");
    }

    protected void getRelationShipManagers()
    {
        ddlrmgr.DataTextField = "RelationManager";
        ddlrmgr.DataValueField = "SysUserID";
        DataSet dsRMngr = new DataSet();
        dsRMngr = objAdmin.getRelationShipManager();
        ddlrmgr.DataSource = dsRMngr;
        ddlrmgr.DataBind();
        ddlrmgr.Items.Insert(0, "--Select--");
    }

    protected void getNationalAccManagers()
    {
        ddlNAccMngr.DataTextField = "RelationManager";
        ddlNAccMngr.DataValueField = "SysUserID";
        DataSet dsAMngr = new DataSet();
        dsAMngr = objAdmin.getNationalAccManager();
        ddlNAccMngr.DataSource = dsAMngr;
        ddlNAccMngr.DataBind();
        ddlNAccMngr.Items.Insert(0, "--Select--");
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        int ChkValue;
        string Implant;
        int customerName = Convert.ToInt32(ddlCustomerName.SelectedValue);
        int Location = Convert.ToInt32(ddllocation.SelectedValue);
        if (chkImplant.Checked)
        {
            ChkValue = 1;
            if (txtImplant.Text == "")
            {
                ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert('Please Enter Implant')</script>");
            }
            Implant = txtImplant.Text;

        }
        else
        {
            ChkValue = 0;
            Implant = "";
        }

        int RelMangr = Convert.ToInt32(ddlrmgr.SelectedValue);
        string Competitors = txtcompetitors.Text;
        string Potential = txtPotential.Text;
        int NatAccMangr = Convert.ToInt32(ddlNAccMngr.SelectedValue);
         int CreatedBy =Convert.ToInt32( Session["UserID"]);
         int rslt = objAdmin.SubmitDetail(customerName, Location, ChkValue, Implant, RelMangr, Competitors, Potential, NatAccMangr, CreatedBy);
        if (rslt > 0)
        {
            ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert('Record Submitted')</script>");
            reset();
        }

    }
    protected void chkImplant_CheckedChanged(object sender, EventArgs e)
    {
        if (chkImplant.Checked)
        {
            txtImplant.Visible = true;
        }
        else
        {
            txtImplant.Text = "";
            txtImplant.Visible = false;
        }
    }

    public void reset()
    {
        ddlCustomerName.SelectedIndex = 0;
        ddllocation.SelectedIndex = 0;
        ddlNAccMngr.SelectedIndex = 0;
        ddlrmgr.SelectedIndex = 0;
        txtcompetitors.Text = "";
        txtImplant.Text = "";
        txtPotential.Text = "";
        chkImplant.Checked = false;
        txtImplant.Visible = false;

    }
}
