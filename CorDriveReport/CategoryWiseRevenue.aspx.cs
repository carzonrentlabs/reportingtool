﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
public partial class CorDriveReport_CategoryWiseRevenue : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }

    private void BindBrachDropDownlist()
    {
        objCordrive = new CorDrive();
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "UnitName";
                    ddlBranch.DataValueField = "UnitId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    protected void bntGet_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(txtFromDate.Text) || string.IsNullOrEmpty(txtToDate.Text))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date and to date should not be blank')", true);
            return;
        }
        else
        {

            StringBuilder strData = new StringBuilder();
            strData = BindExcelData();
            ltlSummary.Text = strData.ToString();
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "categoryRevenueReport";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();

    }

    private StringBuilder BindExcelData()
    {
        StringBuilder strData = new StringBuilder();
        DateTime fromDate = Convert.ToDateTime(txtFromDate.Text.ToString());
        DateTime toDate = Convert.ToDateTime(txtToDate.Text.ToString());
        objCordrive = new CorDrive();
        DataSet dsDetails = objCordrive.GetCategoryWiseRevenue(Convert.ToInt16(ddlBranch.SelectedValue), fromDate, toDate);
        DataView dv = new DataView(dsDetails.Tables[0]);
        try
        {

            if (dsDetails.Tables[0].Rows.Count > 0)
            {
                strData.Append(" <html xmlns:o='urn:schemas-microsoft-com:office:office' ");
                strData.Append(" xmlns:x='urn:schemas-microsoft-com:office:excel' ");
                strData.Append(" xmlns='http://www.w3.org/TR/REC-html40'> ");
                strData.Append(" <head> ");
                strData.Append(" <meta http-equiv=Content-Type content='text/html; charset=windows-1252'> ");
                strData.Append(" <meta name=ProgId content=Excel.Sheet> ");
                strData.Append(" <meta name=Generator content='Microsoft Excel 14'> ");
                strData.Append(" <link rel=File-List href='Category_files/filelist.xml'> ");
                strData.Append(" <style id='Category_25550_Styles'> ");
                strData.Append(" <!--table ");
                strData.Append(" {mso-displayed-decimal-separator:; ");
                strData.Append(" mso-displayed-thousand-separator:;} ");
                strData.Append(" .xl1525550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:general; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6325550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6425550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6525550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:right; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6625550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" text-align:right; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6725550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" text-align:right; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" background:#F2F2F2; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6825550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6925550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:1.0pt solid black; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7025550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7125550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7225550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:1.0pt solid black; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7325550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:1.0pt solid black; ");
                strData.Append(" background:#F2F2F2; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7425550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:1.0pt solid black; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:#F2F2F2; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" --> ");
                strData.Append(" </style> ");
                strData.Append(" </head> ");
                strData.Append(" <body> ");
                strData.Append(" <!--[if !excel]>&nbsp;&nbsp;<![endif]--> ");
                strData.Append(" <!--The following information was generated by Microsoft Excel's Publish as Web ");
                strData.Append(" Page wizard.--> ");
                strData.Append(" <!--If the same item is republished from Excel, all information between the DIV ");
                strData.Append(" tags will be replaced.--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <div id='Category_25550' align=center x:publishsource='Excel'> ");
                strData.Append(" <table border=1 cellpadding=0 cellspacing=0 width=748 style='border-collapse: ");
                strData.Append(" collapse;table-layout:fixed;width:561pt'> ");
                strData.Append(" <col width=215 style='mso-width-source:userset;mso-width-alt:7862;width:161pt'> ");
                strData.Append(" <col width=95 style='mso-width-source:userset;mso-width-alt:3474;width:71pt'> ");
                strData.Append(" <col width=75 style='mso-width-source:userset;mso-width-alt:2742;width:56pt'> ");
                strData.Append(" <col width=64 style='width:48pt'> ");
                strData.Append(" <col width=56 style='mso-width-source:userset;mso-width-alt:2048;width:42pt'> ");
                strData.Append(" <col width=81 span=3 style='mso-width-source:userset;mso-width-alt:2962; ");
                strData.Append(" width:61pt'> ");
                strData.Append(" <tr height=20 style='height:15.0pt'> ");
                strData.Append(" <td colspan=2 height=20 class=xl6825550 width=310 style='border-right:1.0pt solid black; ");
                strData.Append(" height:15.0pt;width:232pt'>&nbsp;</td> ");
                strData.Append(" <td colspan=3 class=xl7025550 width=195 style='border-right:1.0pt solid black; ");
                strData.Append(" border-left:none;width:146pt'>No of Duty</td> ");
                strData.Append(" <td colspan=3 class=xl7025550 width=243 style='border-right:1.0pt solid black; ");
                strData.Append(" border-left:none;width:183pt'>Revenue</td> ");
                strData.Append(" </tr> ");
                strData.Append(" <tr height=20 style='height:15.0pt'> ");
                strData.Append(" <td height=20 class=xl6325550 style='height:15.0pt;border-top:none'>Branch</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Category</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Owned Car</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Hired Car</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Total</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Owned Car</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Hired Car</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Total</td> ");
                strData.Append(" </tr> ");


                foreach (DataRow dr in dsDetails.Tables[0].Rows)
                {

                    if (dr["BranchName"].ToString().Contains("Total"))
                    {
                        //string ttt = dr["BranchName"].ToString() + " Total";
                       // string expression = "BranchName =" + ttt;
                       // DataRow[] totalRow;
                        //totalRow = dsDetails.Tables[0].Select(expression);
                        
                            strData.Append(" <tr height=20 style='height:15.0pt'> ");
                            strData.Append(" <td colspan=2 height=20 class=xl7325550 style='border-right:1.0pt solid black; ");
                            strData.Append(" height:15.0pt'>Total</td> ");
                            strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                            strData.Append(" style='mso-spacerun:yes'></span>" + dr["NoOfDutyOwnedCar"].ToString() + "</td> ");
                            strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                            strData.Append(" style='mso-spacerun:yes'></span>" + dr["NoOfDutyHiredCar"].ToString() + "</td> ");
                            strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                            strData.Append(" style='mso-spacerun:yes'></span>" + dr["TotalDuties"].ToString() + "</td> ");
                            strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                            strData.Append(" style='mso-spacerun:yes'></span>" + dr["RevenueOwnedCar"].ToString() + "</td> ");
                            strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                            strData.Append(" style='mso-spacerun:yes'></span>" + dr["RevenueHiredCar"].ToString() + "</td> ");
                            strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                            strData.Append(" style='mso-spacerun:yes'></span>" + dr["TotalRevenue"].ToString() + "</td> ");
                            strData.Append(" </tr> ");
                   


                    }
                    else
                    {
                        strData.Append(" <tr height=20 style='height:15.0pt'> ");
                        strData.Append(" <td height=20 class=xl6425550 style='height:15.0pt;border-top:none'>" + dr["BranchName"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6525550 style='border-top:none;border-left:none'>" + dr["CategroyName"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["NoOfDutyOwnedCar"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["NoOfDutyHiredCar"].ToString() + "<span ");
                        strData.Append(" style='mso-spacerun:yes'></span></td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["TotalDuties"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["RevenueOwnedCar"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["RevenueHiredCar"].ToString() + "<span ");
                        strData.Append(" style='mso-spacerun:yes'></span></td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["TotalRevenue"].ToString() + "</td> ");
                        strData.Append(" </tr> ");

                    }
                }
                strData.Append(" <![if supportMisalignedColumns]> ");
                strData.Append(" <tr height=0 style='display:none'> ");
                strData.Append(" <td width=215 style='width:161pt'></td> ");
                strData.Append(" <td width=95 style='width:71pt'></td> ");
                strData.Append(" <td width=75 style='width:56pt'></td> ");
                strData.Append(" <td width=64 style='width:48pt'></td> ");
                strData.Append(" <td width=56 style='width:42pt'></td> ");
                strData.Append(" <td width=81 style='width:61pt'></td> ");
                strData.Append(" <td width=81 style='width:61pt'></td> ");
                strData.Append(" <td width=81 style='width:61pt'></td> ");
                strData.Append(" </tr> ");
                strData.Append(" <![endif]> ");
                strData.Append(" </table> ");
                strData.Append(" </div> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" </body> ");
                strData.Append(" </html> ");
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return strData;
    }

}