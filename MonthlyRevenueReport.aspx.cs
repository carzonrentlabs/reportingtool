using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using ReportingTool;
using ExcelUtil;

public partial class MonthlyRevenueReport : System.Web.UI.Page
{
    #region Variable Declaration
    string dateFrom, dateTo;
    int cityId;
    Decimal totalCost = 0;
    clsAutomation objMonthlyRevenue = new clsAutomation();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["Userid"] = 4;
        if (!Page.IsPostBack)
        {
            BindCity();
            BindClient();
            bntNextImport.Attributes.Add("onclick"
                , "return ValidationCheck();");
        }
    }

    protected void BindClient()
    {
        DataSet dsClientName = new DataSet();
        dsClientName = objMonthlyRevenue.FillClient();
        ddlClientName.DataTextField = "ClientCoName";
        ddlClientName.DataValueField = "ClientCoID";
        ddlClientName.DataSource = dsClientName;

        ddlClientName.DataBind();
        ddlClientName.Items.Insert(0, new ListItem("--Select--", "0"));
    }

    protected void BindCity()
    {
        DataSet dsCityName = new DataSet();
        dsCityName = objMonthlyRevenue.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        ddlCityName.DataSource = dsCityName;
        ddlCityName.DataBind();
    }

    protected void bntNextImport_Click(object sender, EventArgs e)
    {
        dateFrom = txtFrom.Text.ToString();
        dateTo = txtTo.Text.ToString();
        cityId = Convert.ToInt32(ddlCityName.SelectedValue); ;


        int CheckBoxSelect = 0, GSTBulkReport = 0, GSTManualReport = 0;
        if (GstBulk.Checked)
        {
            GSTBulkReport = 1;
        }
        if (GSTManual.Checked)
        {
            GSTManualReport = 1;
        }

        if ((ddlbusinessType.SelectedValue == "2" && CheckID.Checked) || ddlbusinessType.SelectedValue == "3")
        {
            if (CheckID.Checked)
            {
                CheckBoxSelect = 1;
            }


            string ExcelName = "";
            if (ddlbusinessType.SelectedValue == "2")
            {
                ExcelName = "EasyCabNewReport.xls";
            }
            else
            {
                ExcelName = "MegaCabNewReport.xls";
            }

            DataSet ds = new DataSet();
            //DataTable dt = new DataTable();
            ds = objMonthlyRevenue.GetMonthlyRevenueReport(
                txtFrom.Text.ToString()
                , txtTo.Text.ToString(), cityId
            , Convert.ToInt32(ddlClientName.SelectedValue)
            , Convert.ToInt32(ddlbusinessType.SelectedItem.Value)
            , CheckBoxSelect, GSTBulkReport, GSTManualReport);
            if (ds.Tables[0].Rows.Count > 0)
            {
                WorkbookEngine.ExportDataSetToExcel(ds, ExcelName);
            }
        }
        else
        {

            DataTable dtEx = new DataTable();

            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=MonthlyReport.xls"); //excel
            //Response.AppendHeader("content-disposition", "attachment;filename=MonthlyReport.csv"); //csv

            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";

            this.EnableViewState = false;
            Response.Write("\r\n");
            DataSet excel = new DataSet();
            excel = objMonthlyRevenue.GetMonthlyRevenueReport(
                dateFrom.ToString()
                , dateTo.ToString(), cityId
                , Convert.ToInt32(ddlClientName.SelectedValue)
                , Convert.ToInt32(ddlbusinessType.SelectedItem.Value)
                , CheckBoxSelect, GSTBulkReport, GSTManualReport);

            dtEx = excel.Tables[0];

            DataSet ExtraAmt = new DataSet();
            if (dtEx.Rows.Count > 0)
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                int[] iColumns = { 1, 33, 29, 2, 3, 4, 9, 5, 6, 7, 8, 10, 30, 11, 12, 13, 14, 16, 17, 15, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 32, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 67, 63, 64, 65, 66, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82 };
                for (int i = 0; i < dtEx.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingID") { Response.Write("<td align='left'><b>Booking ID</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "LimoBookingID") { Response.Write("<td align='left'><b>New Booking ID (limo)</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OldBookingID") { Response.Write("<td align='left'><b>Old Booking ID</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Customer") { Response.Write("<td align='left'><b>Customer</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Company") { Response.Write("<td align='left'><b>Company</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName") { Response.Write("<td align='left'><b>City Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "accountingdate") { Response.Write("<td align='left'><b>Accounting Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "dateout") { Response.Write("<td align='left'><b>Date Out</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "timeout") { Response.Write("<td align='left'><b>Time Out</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "datein") { Response.Write("<td align='left'><b>Date In</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "timein") { Response.Write("<td align='left'><b>Time In</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "carcatname") { Response.Write("<td align='left'><b>Booked Category</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Allotedcarcatname") { Response.Write("<td align='left'><b>Alloted Category</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "unitName") { Response.Write("<td align='left'><b>Unit Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "VendorName") { Response.Write("<td align='left'><b>Vendor Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "VehicleNo") { Response.Write("<td align='left'><b>Vehicle No</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalHrsUsed") { Response.Write("<td align='left'><b>Total Hrs.</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "KMIn") { Response.Write("<td align='left'><b>KMIn</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "KMOut") { Response.Write("<td align='left'><b>KMOut</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "totalKM") { Response.Write("<td align='left'><b>Total KM</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Revenue") { Response.Write("<td align='left'><b>Revenue</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DamageCharges") { Response.Write("<td align='left'><b>Damage Charges</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ServiceTaxAmt") { Response.Write("<td align='left'><b>Service Tax</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "EduTaxAmt") { Response.Write("<td align='left'><b>Edu Tax</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "HduTaxAmt") { Response.Write("<td align='left'><b>Higer Edu Tax</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Taxes") { Response.Write("<td align='left'><b>Taxes</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ParkTollChages") { Response.Write("<td align='left'><b>Toll Charges</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalCost") { Response.Write("<td align='left'><b>Adjusted Amt</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalCost") { Response.Write("<td align='left'><b>Total Billing</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DiscPC") { Response.Write("<td align='left'><b>DiscPC</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "FuelSurcharge") { Response.Write("<td align='left'><b>FuelSurcharge</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "vouchervalue") { Response.Write("<td align='left'><b>Voucher Value</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CorTax") { Response.Write("<td align='left'><b>Cor Tax</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OutstationYN") { Response.Write("<td align='left'><b>Oustation YN</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "RevenueType") { Response.Write("<td align='left'><b>Revenue Type</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "FullServiceTax") { Response.Write("<td align='left'><b>FullServiceTax</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OracleLocation") { Response.Write("<td align='left'><b>Oracle Location</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DiscountValue") { Response.Write("<td align='left'><b>Discount Value</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OracleCarRegNo") { Response.Write("<td align='left'><b>Oracle Car RegNo</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OracleCarCode") { Response.Write("<td align='left'><b>Oracle Car Code</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SwachhBharatTaxPercent") { Response.Write("<td align='left'><b>Swachh Bharat Cess</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SwachhBharatTaxAmt") { Response.Write("<td align='left'><b>Swachh Bharat Cess Amt</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "KrishiKalyanTaxPercent") { Response.Write("<td align='left'><b>Krishi Kalyan Cess</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "KrishiKalyanTaxAmt") { Response.Write("<td align='left'><b>Krishi Kalyan Cess Amt</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BusinessType") { Response.Write("<td align='left'><b>Business Type</b></td>"); }

                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CGSTTaxAmt") { Response.Write("<td align='left'><b>CGST Tax Amt</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SGSTTaxAmt") { Response.Write("<td align='left'><b>SGST Tax Amt</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "IGSTTaxAmt") { Response.Write("<td align='left'><b>IGST Tax Amt</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "InvoiceNo1") { Response.Write("<td align='left'><b>Invoice No</b></td>"); }

                            if (ddlbusinessType.SelectedValue == "2")
                            {
                                if (dtEx.Columns[iColumns[j]].Caption.ToString() == "callcenter") { Response.Write("<td align='left'><b>Call center convenience charges</b></td>"); }
                                if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Airport") { Response.Write("<td align='left'><b>Airport Parking</b></td>"); }
                                if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NCR") { Response.Write("<td align='left'><b>NCR Entry Charges</b></td>"); }
                            }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "StateName") { Response.Write("<td align='left'><b>StateName</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "GSTIN") { Response.Write("<td align='left'><b>GSTIN</b></td>"); }

                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarzonrentState") { Response.Write("<td align='left'><b>Carzonrent StateName</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarzonrentGSTIN") { Response.Write("<td align='left'><b>Carzonrent GSTIN</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SubName") { Response.Write("<td align='left'><b>Subsidiary Client Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DoNotApplyServiceTaxYN") { Response.Write("<td align='left'><b>Subsidiary Tax Excempted</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PaymentMode") { Response.Write("<td align='left'><b>Payment Mode</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BulkInvoiceNo") { Response.Write("<td align='left'><b>Bulk InvoiceNo</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "oldInvoiceNo") { Response.Write("<td align='left'><b>Old Bulk InvoiceNo</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CreateDate") { Response.Write("<td align='left'><b>Old Bulk Create Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SEZClientYN") { Response.Write("<td align='left'><b>SEZ ClientYN</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PkgName") { Response.Write("<td align='left'><b>Pkg Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ServiceName") { Response.Write("<td align='left'><b>Service Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ServiceType") { Response.Write("<td align='left'><b>Service Type</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SanatizationCharges") { Response.Write("<td align='left'><b>Sanatization Charges</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CompanyLegalName") { Response.Write("<td align='left'><b>Legal Name</b></td>"); }

                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CentralizedBilling") { Response.Write("<td align='left'><b>Centralized Billing</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CentralizedBillingEffectivedate") { Response.Write("<td align='left'><b>Centralized Billing Effective Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "FuelTypeName") { Response.Write("<td align='left'><b>Fuel Type Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CreditCardPayment") { Response.Write("<td align='left'><b>Credit Card YN</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingPerformedBy") { Response.Write("<td align='left'><b>Bookings Performed</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "EditStatus") { Response.Write("<td align='left'><b>EditStatus</b></td>"); }

                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "RentalType") { Response.Write("<td align='left'><b>RentalType</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ExtraHrsAmount") { Response.Write("<td align='left'><b>ExtraHrsAmount</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ExtraKmAmount") { Response.Write("<td align='left'><b>ExtraKmAmount</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PkgRate") { Response.Write("<td align='left'><b>PkgRate</b></td>"); }
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalCost")
                        {
                            if (dtEx.Rows[i][iColumns[j]] is DBNull)
                            {
                                Response.Write("<td align='left'>0</td>");
                            }
                            else
                            {
                                Response.Write("<td align='left'>" + Convert.ToInt64(dtEx.Rows[i][iColumns[j]]).ToString() + "</td>");
                            }
                            if (dtEx.Rows[i][iColumns[j]] is DBNull)
                            {
                                Response.Write("<td align='left'>0</td>");
                            }
                            else
                            {
                                Response.Write("<td align='left'>" + Convert.ToInt64(dtEx.Rows[i][iColumns[j]]).ToString() + "</td>");
                            }
                        }
                        else
                        {
                            if (ddlbusinessType.SelectedValue == "2")
                            {
                                if (dtEx.Columns[iColumns[j]].Caption.ToString() == "callcenter" || dtEx.Columns[iColumns[j]].Caption.ToString() == "Airport"
                                    || dtEx.Columns[iColumns[j]].Caption.ToString() == "NCR")
                                {
                                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                                }
                                if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OutstationYN")
                                {
                                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                                }
                            }
                            else
                            {
                                if (dtEx.Columns[iColumns[j]].Caption.ToString() != "callcenter" && dtEx.Columns[iColumns[j]].Caption.ToString() != "Airport"
                                    && dtEx.Columns[iColumns[j]].Caption.ToString() != "NCR")
                                {
                                    if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Revenue")
                                    {
                                        Response.Write("<td align='left'>" + Convert.ToInt64(dtEx.Rows[i][iColumns[j]]).ToString() + "</td>");
                                    }

                                    else
                                    {
                                        Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                                    }
                                }
                            }
                        }
                    }
                    Response.Write("</tr>");
                    totalCost = totalCost + Convert.ToDecimal(dtEx.Rows[i][18]);
                }
                Response.Write("<tr><td colspan='20' align='right'>Total Cost</td><td align='right' >" + totalCost + "</td><td colspan='19'>&nbsp;</td></tr>");
                Response.Write("</table>");

                Response.End();
            }
            else
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();
            }
        }
    }
    protected void ddlbusinessType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlbusinessType.SelectedValue == "1")
        {
            CheckID.Checked = false;
        }
        else if (ddlbusinessType.SelectedValue == "2")
        {
            CheckID.Checked = true;
            GstBulk.Checked = false;
        }
        else if (ddlbusinessType.SelectedValue == "3")
        {
            CheckID.Checked = false;
            GstBulk.Checked = false;
        }
    }
}