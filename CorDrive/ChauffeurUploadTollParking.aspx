﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChauffeurUploadTollParking.aspx.cs" Inherits="CorDrive_ChauffeurUploadTollParking" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../JQuery/jquery.reveal.js"></script>
    <link href="../CSSReveal/reveal.css" rel="stylesheet" />

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/moment.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
<style type="text/css">
    .hideGridColumn
    {
        display:none;
    }
    
    
    .modalBackground 
{
    height:100%;
    background-color:#EBEBEB;
    filter:alpha(opacity=60);
    opacity:0.6;
}
.body 
{
background-color:#000000; 
}
 </style>

 <script type="text/javascript">
     function ValidateControl() {
         var txt1 = document.getElementById('txtAmount');
         var txt2 = document.getElementById('ddlImageType');
         var uploadfile = document.getElementById('UploadToll');

         if (txt1.value == '') {
             alert('Please enter amount');
             return false;
         }

         if (txt2.value == '0') {
             alert('Please select Upload Type !');
             return false;
         }

         if (UploadToll.value == '') {
             alert('Please upload the attachment');
             return false;
         }
     }


     //Function to allow only numbers to textbox
     function validate(key) 
     {
         //getting key code of pressed key
         var keycode = (key.which) ? key.which : key.keyCode;
         var phn = document.getElementById('txtPhn');
         //comparing pressed keycodes
         if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
             return false;
         }
         else {
             //Condition to check textbox contains ten numbers or not
             if (phn.value.length < 10) {
                 return true;
             }
             else {
                 return false;
             }
         }
     }

//     $(document).ready(function () {
//         $('#btnSave').click(function () {
//             if ($('#UploadToll').val() == "") {
//                 alert('no file selected');
//                 return false;
//             }
//         });
//     });

     function checkFileExtension(elem) {
         var filePath = elem.value;

         if (filePath.indexOf('.') == -1)
             return false;

         var validExtensions = new Array();
         var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

         validExtensions[0] = 'png';
         validExtensions[1] = 'PNG';

         for (var i = 0; i < validExtensions.length; i++) {
             if (ext == validExtensions[i])
                 return true;
         }

         alert('The file extension ' + ext.toUpperCase() + ' is not allowed!');
         return false;
     }

//     $(function () {

//         $("a[id*='dfsdfsdffsdfsdf']").click(function () {
//             alert("teter");
//             var tt = $(this).attr("href");
//             var sdfsdf = $(this).attr("bksharma");
//             alert(tt);
//             window.open("C:\\CorInt\\Rentmeter_New\\receipts\\8018806_Toll_1_30.png", "_blank");
//         });

//     });
            </script>




<fieldset class="FieldsetClassic" style="text-align: center; width: 90%;"> 
    <center>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    <legend class="LegendClassic"> 
    <span style="font-size: 10pt; font-family: Verdana">.::Toll/Parking Image Upload::.</span>. </legend>  &nbsp;

    <asp:Panel ID="Panel1" runat="server" BorderColor="Black" BorderWidth="1px" Width="250px" Visible="true">
        <table>
            <tr>
                <td style="width: 1px">
                    <span style="font-size: 8pt; font-family: Verdana">
                    BookingID </span>
                </td>
                <td style="width: 1px">
                    <asp:TextBox ID="txtBookingID" runat="server" Width="100px" onkeypress="return validate(event)"></asp:TextBox>

                </td>

                <td style="width: 1px">
                  
                       <asp:RequiredFieldValidator ID="rfvtxtBookingID" runat="server" 
                                            ControlToValidate="txtBookingID" Enabled="true" ErrorMessage="*" 
                                            SetFocusOnError="True" ValidationGroup="Booking">
                                        </asp:RequiredFieldValidator>
                </td>
            </tr>

              <%--<tr>
                <td colspan="4">
                    <asp:Button ID="btnShow" runat="server" Text="Search" Font-Names="Verdana" 
                        Font-Size="8pt" onclick="btnShow_Click" ValidationGroup="Booking"/>
                    &nbsp;<asp:Button ID="btnReset" runat="server" Font-Names="Verdana" Font-Size="8pt" 
                        Text="Reset" />
                </td>

            </tr>--%>
        </table>
        <asp:Panel ID="Panel2" runat="server" BorderColor="Black" BorderWidth="0px" Width="250px" Visible="true">
        <table>
              <tr>
                <td>
                    <asp:Button ID="btnShow" runat="server" Text="Search" Font-Names="Verdana" 
                        Font-Size="8pt" onclick="btnShow_Click" ValidationGroup="Booking"/>
                    &nbsp;<asp:Button ID="btnReset" runat="server" Font-Names="Verdana" Font-Size="8pt" 
                        Text="Reset" onclick="btnReset_Click" />
                         &nbsp;<asp:Button ID="btnopenPopup" runat="server" Font-Names="Verdana" Font-Size="8pt" 
                        Text="UploadFile" onclick="btnopenPopup_Click" ValidationGroup="Booking" />
                    &nbsp;</td>

            </tr>
        </table>
    </asp:Panel>
    </asp:Panel>
     

    <table cellpadding="0" cellspacing="0" style="width: 90%">
     <tr>
     <td>
     
         
    
     </td>
     </tr>
        <tr>
            <td style="height: 168px">
                <asp:GridView ID="gvTollPark" runat="server" SkinID="SandAndSkywithoutwidth1" EmptyDataText="--No Record Found--"
                    Width="90%" Font-Names="Verdana" Font-Overline="False" Font-Size="7pt" 
                    onrowcommand="gvTollPark_RowCommand" AutoGenerateColumns="False" 
                    onrowdatabound="gvTollPark_RowDataBound">
                    <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100%" />
                    <Columns>
                       
                        <asp:TemplateField HeaderText="SNO" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblSerialNo" runat="server" Text='<%# Container.DataItemIndex+1 %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        
                         <asp:TemplateField HeaderText="BookingID" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblBookingID" runat="server" Text='<%# Bind("BookingID") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="ReceiptID" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblReceiptID" runat="server" Text='<%# Bind("ReceiptID") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ReceiptType" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblReceiptType" runat="server" Text='<%# Bind("ReceiptType") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ReceiptAmount" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblReceiptAmount" runat="server" Text='<%# Bind("ReceiptAmount") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="CreatedBy" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Bind("CreatedBy") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="CreatedAt" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedAt" runat="server" Text='<%# Bind("CreatedAt") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="ApprovedYN" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblApprovedYN" runat="server" Text='<%# Bind("ApprovedYN") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="ValidatedHardCopyYN" Visible ="true" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblValidatedHardCopyYN" runat="server" Text='<%# Bind("ValidatedHardCopyYN") %>' Visible="true"></asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="DutyStatus" Visible ="false" >                        
                            <ItemTemplate>
                                <asp:Label ID="lblDutyStatus" runat="server" Text='<%# Bind("Status") %>' Visible="true"></asp:Label> 
                                <asp:HiddenField ID="hdnDutyStatus" runat="server"  Value='<%# Bind("Status") %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField Visible="true"  >
                                <ItemTemplate>
                             <asp:HyperLink ID="hprLnkView" runat="server" Font-Bold="True" ForeColor="Red" NavigateUrl='<%#Eval("ReceiptFileName") %>'
                                                                Target="_blank">View</asp:HyperLink>
                                <br />
                                 </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                          

                        <asp:TemplateField Visible ="false" >
                                <ItemTemplate>
                                    <asp:Button ID="btnSave" Width="111px" Text="Submit" runat="server" CommandName="select" Visible="true" 
                                        CommandArgument='<%#Bind("BookingID")%>'/>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField> 
                                                   
                    </Columns>
                </asp:GridView>                
                <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal>
                </td>
                 
        </tr>
        <tr>
            <td style="height: 50px">
            <asp:Panel id="pnlpopup" runat="server" Width="35%" BorderWidth="2px" BorderStyle="Solid" BorderColor="Black" Visible="false">
                     <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        <TABLE style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; FONT-SIZE: 9pt;BORDER-LEFT: black 1px solid; 
                            WIDTH: 100%; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Tahoma; BACKGROUND-COLOR: #EBEBEB; TEXT-ALIGN: left">
                            <TBODY>
                                <TR style="HEIGHT: 10px">
                                    <TD colSpan=3 style="color: #ff3333; font-family: Tahoma; text-align: right;">
                                        <asp:ImageButton ID="ImgClose"
                                            runat="server" Height="20px" ImageUrl="~/images/Close.jpg" 
                                            onclick="ImgClose_Click" />
                                    </TD>
                                </TR>

                                   <tr style="HEIGHT: 10px">
                                    <td colSpan="3" style="color: #ff3333; font-family: Tahoma">
                                        <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                    </td>
                                </tr>

                                <tr id="trTDSBookingiD" runat="server">
                                    <td align="left">
                                        BookingiD                                    
                                    </td>
                                    <td>:</td>
                                    <td align="left">
                                        <asp:Label ID="lblSerialNo" runat="server" Visible="false"></asp:Label>
                                        
                                        <asp:Label ID="lblBookingID" runat="server"></asp:Label>
                                        
                                    </td>
                                </tr>


                                  <tr runat="server" id="trCreditTo" visible="true">
                                <td align="left">Upload Type</td>
                                 
                                <td><strong>:</strong></td>
                                <td>
                                    <asp:DropDownList ID="ddlImageType" runat="server" Visible="true">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Toll" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Parking" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Others" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfv_ddlImageType" runat="server" 
                                        ControlToValidate="ddlImageType" Enabled="true" 
                                        ErrorMessage="Please select type !" InitialValue="0" ValidationGroup="save">
                                    </asp:RequiredFieldValidator>
                                    &nbsp;
                                </td>
                            </tr>
                                <tr id="trRemarks" runat="server" visible="true">
                                    <td align="left" >
                                        Amount</td>
                                    <td>:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAmount" runat="server" MaxLength="10" 
                                            onkeypress="return validate(event)" TextMode="SingleLine" Width="75px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtAmount" runat="server" ControlToValidate="txtAmount" 
                                            ErrorMessage="Please enter amount !" ValidationGroup="save" Enabled="true" 
                                            SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                 <tr id="tr1" runat="server" visible="true">
                                    <td align="left" >
                                        Upload Image</td>
                                    <td>:</td>
                                    <td align="left">
                                        <asp:FileUpload ID="UploadToll" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvUploadToll" runat="server" 
                                            ControlToValidate="UploadToll" Enabled="true" 
                                            ErrorMessage="please select file !" ValidationGroup="save">
                                        </asp:RequiredFieldValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvUploadToll"
                                        runat="server" ErrorMessage="Please select file !" 
                                        ControlToValidate="UploadToll" ValidationGroup="save" Enabled="true" Display="None"
                                        ></asp:RequiredFieldValidator>--%>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks" 
                                            ErrorMessage="*" ValidationGroup="save" SetFocusOnError="True" Enabled="true">
                                        </asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>

                                <TR>
                                    <TD align="center" colspan="3">
                                        &nbsp;</TD>
                                </TR>
                                
                                
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" text="Save" 
                                            validationgroup="save" />
                                        &nbsp;
                                        <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                                            text="Cancel" />
                                    </td>
                                </tr>
                                
                                
                            </TBODY>
                        </TABLE>
                        </ContentTemplate>
                        <triggers>
                            <asp:PostBackTrigger ControlID="btnSave" ></asp:PostBackTrigger>
                            <%--<asp:PostBackTrigger ControlID = "UploadToll" ></asp:PostBackTrigger>
                            <asp:AsyncPostBackTrigger ControlID="txtAmount" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>--%>
                           <%--<asp:AsyncPostBackTrigger ControlID = "btnAsyncUpload" EventName = "Click" />--%>
                        </triggers>
                        </asp:UpdatePanel>
                    </asp:Panel> 
                <asp:Label ID="lblMsg" runat="server"></asp:Label></td>
            <td colspan="1" style="height: 1px">
            </td>
        </tr>
        <tr>
            <td style="height: 10px">
                &nbsp;</td>
            <td colspan="1" style="height: 1px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 100px">
             <asp:Label style="DISPLAY: none" id="lblPopupTargetID" runat="server"></asp:Label> 
                    <cc1:ModalPopupExtender 
                        id="mdPopUp" 
                        runat="server" 
                        TargetControlID="lblPopupTargetID" 
                        PopupControlID="pnlpopup"                   
                        BackgroundCssClass="modalBackground"
                        BehaviorID="mdlPopup"
                        CancelControlID="btnCancel"
                         DropShadow="false"
                        RepositionMode ="RepositionOnWindowResizeAndScroll" >
                    </cc1:ModalPopupExtender>
              


            </td>
            <td colspan="1" style="height: 1px">
            </td>
        </tr>

        <%--<tr><td>
        <image id="bkshrma" src="C:\CorInt\Rentmeter_New\receipts\8018806_Toll_1_30.png" alt="Bksharma"></image>
        </td></tr> 
       <tr><td>
            <iframe id="bkshrma" src="C:\CorInt\Rentmeter_New\receipts\8018806_Toll_1_30.png" alt="Bksharma"></iframe>
           
        </td></tr>--%> 
 
</table> 
</center>        
</fieldset>
        </div>
    </form>
</body>
</html>
