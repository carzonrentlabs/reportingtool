﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using ReportingTool;
using System.IO;
using System.Text;


public partial class TrackingDutiesClosureBranches : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
             btnExport.Attributes.Add ("Onclick","return validate()");
        }
        btnExport.Attributes.Add("Onclick", "return validate()");
    }

    protected void BindCity()
    {
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        ddlCityName.DataBind();
    }
    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    string fromDate = txtFrom.Value;
    //    string toDate = txtTo.Value;
    //    if (fromDate != "" && toDate != "")
    //    {
    //        DataTable dtEx = new DataTable();

    //        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
    //        Response.Clear();

    //        Response.AppendHeader("content-disposition", "attachment;filename=TracingDutiesClosureBranches.xls");

    //        Response.Charset = "";
    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //        Response.ContentType = "application/vnd.ms-excel";

    //        this.EnableViewState = false;
    //        Response.Write("\r\n");

    //        dtEx = objAdmin.TrackingDutiesClosureBranches(fromDate, toDate);

    //        if (dtEx.Rows.Count > 0)
    //        {
    //            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
    //            int[] iColumns = {0,1,2,3,4,5};
    //            for (int i = 0; i < dtEx.Rows.Count; i++)
    //            {
    //                if (i == 0)
    //                {
    //                    Response.Write("<tr>");
    //                    for (int j = 0; j < iColumns.Length; j++)
    //                    {
    //                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BranchName")
    //                        {
    //                            Response.Write("<td align='left'><b>Branch Name</b></td>");
    //                        }
    //                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ExecutiveName")
    //                        {
    //                            Response.Write("<td align='left'><b>Executive Name</b></td>");
    //                        }
    //                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingID")
    //                        {
    //                            Response.Write("<td align='left'><b>Invoice No.</b></td>");
    //                        }
    //                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ClosureDate")
    //                        {
    //                            Response.Write("<td align='left'><b>ClosureDate</b></td>");
    //                        }
    //                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ClosureTime")
    //                        {
    //                            Response.Write("<td align='left'><b>ClosureTime</b></td>");
    //                        }
    //                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalHours")
    //                        {
    //                            Response.Write("<td align='left'><b>Closure TAT</b></td>");
    //                        }
    //                    }
    //                    Response.Write("</tr>");
    //                }
    //                Response.Write("<tr>");
    //                for (int j = 0; j < iColumns.Length; j++)
    //                {
    //                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
    //                }
    //                Response.Write("</tr>");
    //            }
    //            Response.Write("</table>");
    //            Response.End();
    //        }
    //        else
    //        {
    //            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
    //            Response.Write("<td align='center'><b>No Record Found</b></td>");
    //            Response.Write("</table>");
    //            Response.End();
    //        }        
    //    }

    //}

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string fromDate = txtFrom.Value;
        string toDate = txtTo.Value;
        string cityId = ddlCityName.SelectedValue.ToString();
        if (fromDate != "" && toDate != "" && cityId!="")
        {
            DataTable dtEx = new DataTable();
            dtEx = objAdmin.TrackingDutiesClosureBranches(fromDate, toDate,cityId);

            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office'");
            str.Append("xmlns:x='urn:schemas-microsoft-com:office:excel'");
            str.Append("xmlns='http://www.w3.org/TR/REC-html40'>");
            str.Append("<head>");
            str.Append("<meta http-equiv=Content-Type content='text/html; charset=windows-1252'>");
            str.Append("<meta name=ProgId content=Excel.Sheet>");
            str.Append("<meta name=Generator content='Microsoft Excel 14'>");
            str.Append("<link rel=File-List href='Arpita_206_files/filelist.xml'>");
            str.Append("<style id='Arpita_206_27870_Styles'>");
            str.Append("<!--table");
            str.Append("{mso-displayed-decimal-separator:'\';");
            str.Append("mso-displayed-thousand-separator:'\';}");
            str.Append(".xl1527870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:general;");
            str.Append("vertical-align:bottom;");
            str.Append("mso-background-source:auto;");
            str.Append("mso-pattern:auto;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6327870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:general;");
            str.Append("vertical-align:bottom;");
            str.Append("background:white;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6427870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:general;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("mso-background-source:auto;");
            str.Append("mso-pattern:auto;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6527870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border-top:.5pt solid windowtext;");
            str.Append("border-right:.5pt solid windowtext;");
            str.Append("border-bottom:.5pt solid windowtext;");
            str.Append("border-left:none;");
            str.Append("background:yellow;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6627870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:yellow;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6727870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:#C5D9F1;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6827870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:#F2DCDB;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl6927870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:'Short Date';");
            str.Append("text-align:general;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("mso-background-source:auto;");
            str.Append("mso-pattern:auto;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl7027870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("background:#92D050;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl7127870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:middle;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:#D9D9D9;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl7227870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:red;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:#F2DCDB;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl7327870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:red;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:#C5D9F1;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl7427870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:400;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:bottom;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:yellow;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append(".xl7527870");
            str.Append("{padding-top:1px;");
            str.Append("padding-right:1px;");
            str.Append("padding-left:1px;");
            str.Append("mso-ignore:padding;");
            str.Append("color:black;");
            str.Append("font-size:11.0pt;");
            str.Append("font-weight:700;");
            str.Append("font-style:normal;");
            str.Append("text-decoration:none;");
            str.Append("font-family:Calibri, sans-serif;");
            str.Append("mso-font-charset:0;");
            str.Append("mso-number-format:General;");
            str.Append("text-align:center;");
            str.Append("vertical-align:middle;");
            str.Append("border:.5pt solid windowtext;");
            str.Append("background:white;");
            str.Append("mso-pattern:black none;");
            str.Append("white-space:nowrap;}");
            str.Append("-->");
            str.Append("</style>");
            str.Append("</head>");
            str.Append("<body>");
            str.Append("<!--[if !excel]>&nbsp;&nbsp;<![endif]-->");
            str.Append("<!--The following information was generated by Microsoft Excel's Publish as Web");
            str.Append("Page wizard.-->");
            str.Append("<!--If the same item is republished from Excel, all information between the DIV");
            str.Append("tags will be replaced.-->");
            str.Append("<!----------------------------->");
            str.Append("<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->");
            str.Append("<!----------------------------->");
            str.Append("<div id='Arpita_206_27870' align=center x:publishsource='Excel'>");
            str.Append("<table border=0 cellpadding=0 cellspacing=0 width=2266 style='border-collapse:");
            str.Append(" collapse;table-layout:fixed;width:1695pt'>");
            str.Append("<col width=64 style='width:48pt'>");
            str.Append("<col width=108 style='mso-width-source:userset;mso-width-alt:3949;width:81pt'>");
            str.Append("<col width=112 style='mso-width-source:userset;mso-width-alt:4096;width:84pt'>");
            str.Append("<col width=86 style='mso-width-source:userset;mso-width-alt:3145;width:65pt'>");
            str.Append("<col width=46 style='mso-width-source:userset;mso-width-alt:1682;width:35pt'>");
            str.Append("<col width=45 style='mso-width-source:userset;mso-width-alt:1645;width:34pt'>");
            str.Append("<col width=47 span=6 style='mso-width-source:userset;mso-width-alt:1718;");
            str.Append(" width:35pt'>");
            str.Append("<col width=39 style='mso-width-source:userset;mso-width-alt:1426;width:29pt'>");
            str.Append("<col width=47 span=14 style='mso-width-source:userset;mso-width-alt:1718;");
            str.Append(" width:35pt'>");
            str.Append("<col width=39 style='mso-width-source:userset;mso-width-alt:1426;width:29pt'>");
            str.Append("<col width=83 style='mso-width-source:userset;mso-width-alt:3035;width:62pt'>");
            str.Append("<col width=64 span=11 style='width:48pt'>");
            str.Append("<tr height=20 style='height:15.0pt'>");
            str.Append("<td height=20 class=xl6327870 width=64 style='height:15.0pt;width:48pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=108 style='width:81pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=112 style='width:84pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=86 style='width:65pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=46 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=45 style='width:34pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td colspan=2 class=xl7027870 width=86 style='width:64pt'>Overlap</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td colspan=2 class=xl7027870 width=94 style='width:70pt'>Overlap</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=47 style='width:35pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=39 style='width:29pt'>&nbsp;</td>");
            str.Append("<td class=xl6327870 width=83 style='width:62pt'>&nbsp;</td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("<td class=xl1527870 width=64 style='width:48pt'></td>");
            str.Append("</tr>");
            str.Append("<tr height=20 style='height:15.0pt'>");
            str.Append("<td rowspan=2 height=40 class=xl7127870 style='height:30.0pt'>Branch</td>");
            str.Append("<td rowspan=2 class=xl7127870>Executive Name</td>");
            str.Append("<td rowspan=2 class=xl7127870>Department</td>");
            str.Append("<td rowspan=2 class=xl7127870>Date</td>");
            str.Append("<td colspan=8 class=xl7427870 style='border-left:none'>NIGHT</td>");
            str.Append("<td colspan=8 class=xl7327870 style='border-left:none'>MORNING</td>");
            str.Append("<td colspan=8 class=xl7227870 style='border-left:none'>Evening</td>");
            str.Append("<td rowspan=2 class=xl7527870>Total Closed</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("</tr>");
            str.Append("<tr height=20 style='height:15.0pt'>");
            str.Append("<td height=20 class=xl6527870 style='height:15.0pt;border-top:none'>23</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>" + 24 + "</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>" + 24 + "</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>" + 24 + "</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>" + 24 + "</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>" + 24 + "4</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>" + 24 + "</td>");
            str.Append("<td class=xl6627870 style='border-top:none;border-left:none'>6</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>7</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>8</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>9</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>10</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>11</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>12</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>13</td>");
            str.Append("<td class=xl6727870 style='border-top:none;border-left:none'>14</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>15</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>16</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>17</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>18</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>19</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>20</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>21</td>");
            str.Append("<td class=xl6827870 style='border-top:none;border-left:none'>22</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("<td class=xl6327870>&nbsp;</td>");
            str.Append("</tr>");

            if (dtEx.Rows.Count > 0)
            {
                foreach (DataRow row in dtEx.Rows)
                {
                    str.Append("<tr height=20 style='height:15.0pt'>");
                    str.Append("<td height=20 class=xl6427870 style='height:15.0pt;border-top:none'>" + row["BranchName"] + "</td>");
                    str.Append("<td class=xl6427870 style='border-top:none;border-left:none'>" + row["ExecutiveName"] + "</td>");
                    str.Append("<td class=xl6427870 style='border-top:none;border-left:none'>" + row["Department"] + "</td>");
                    DateTime strDate = Convert.ToDateTime(row["ClosingDate"]);
                    str.Append("<td class=xl6927870 align=right style='border-top:none;border-left:none'>" + strDate.ToString("MM-dd-yyyy") + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime23"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime24"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime1"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime2"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime3"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime4"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime5"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime6"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime7"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime8"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime9"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime10"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime11"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime12"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime13"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime14"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime15"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime16"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime17"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime18"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime19"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime20"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime21"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["BookingTime22"] + "</td>");
                    str.Append("<td class=xl6427870 align=right style='border-top:none;border-left:none'>" + row["total"] + "</td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("<td class=xl1527870></td>");
                    str.Append("</tr>");
                }
            }
            
            str.Append("<![if supportMisalignedColumns]>");
            str.Append("<tr height=0 style='display:none'>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=108 style='width:81pt'></td>");
            str.Append("<td width=112 style='width:84pt'></td>");
            str.Append("<td width=86 style='width:65pt'></td>");
            str.Append("<td width=46 style='width:35pt'></td>");
            str.Append("<td width=45 style='width:34pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=39 style='width:29pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=47 style='width:35pt'></td>");
            str.Append("<td width=39 style='width:29pt'></td>");
            str.Append("<td width=83 style='width:62pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("<td width=64 style='width:48pt'></td>");
            str.Append("</tr>");
            str.Append("<![endif]>");
            str.Append("</table>");
            str.Append("</div>");
            str.Append("<!----------------------------->");
            str.Append("<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->");
            str.Append("<!----------------------------->");
            str.Append("</body>");
            str.Append("</html>");
            ltrStudentDetails.Text = str.ToString();

            Response.Clear();
            Response.Buffer = true;
            string strFileName = "TrackingDutiesClouserBranch";
            strFileName = strFileName.Replace("(", "");
            strFileName = strFileName.Replace(")", "");
            Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            ltrStudentDetails.RenderControl(hw);
            Response.Output.Write(sw.ToString());
            Response.End();
            ltrStudentDetails.Visible = false;

            
        }

    }
}