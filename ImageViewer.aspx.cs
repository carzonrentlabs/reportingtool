﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Text;
using System.Data;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using ReportingTool;
public partial class ImageViewer : System.Web.UI.Page
{
    clsAutomation objMandate = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {
        // byte[] bytes;        
        //idImage.Attributes["src"] = "D:\\Upload\\DutySlips\\8357779.pdf";
        //FileStream obj = new FileStream("D:\\Upload\\DutySlips\\8357779.pdf",FileMode.Open);
        //StreamReader obj = new StreamReader("D:\\Upload\\DutySlips\\8357779.pdf");
        try
        {
            if (Request.QueryString.HasKeys() && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                if (Convert.ToInt32(Request.QueryString["bookingId"]) > 0)
                {
                    //byte[] bytes = System.IO.File.ReadAllBytes("D:\\Upload\\Mails7132064.pdf");
                    byte[] bytes;
                    string FilePath = string.Empty;
                    string[] Allfiles = new string[2];
                    //List<byte[]> temp = new List<byte[]>();
                    //byte[] allfiles;
                    string bookingId = Request.QueryString["bookingId"].ToString();
                    string filetype= Request.QueryString["type"].ToString();

                    if (Request.QueryString["MailYN"] == "0")
                    {
                        FilePath = @"F:\\Upload\\DutySlips\\";
                        if (File.Exists(FilePath + bookingId + ".pdf"))
                        {
                            bytes = System.IO.File.ReadAllBytes(FilePath + bookingId + ".pdf");
                        }
                        else
                        {
                            bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/images/FileNA.pdf"));
                        }
                    }
                    else
                    {
                        FilePath = @"F:\\Upload\\Mails\\";
                        //if (File.Exists(FilePath + bookingId + "_ALL_a" + ".pdf"))
                        //{
                        //    bytes = System.IO.File.ReadAllBytes(FilePath + bookingId + "_ALL_a" + ".pdf");
                        //}
                        //else 
                        DataTable dtBookingIdDetails = objMandate.CheckUploadEmailVRFRequired(Convert.ToInt32(bookingId));
                        if (dtBookingIdDetails != null && dtBookingIdDetails.Rows.Count > 0)
                        {

                            if (File.Exists(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["File1"])) && filetype == "1")
                            {
                                bytes = System.IO.File.ReadAllBytes(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["File1"]));
                            }
                            else if (File.Exists(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["File2"])) && filetype == "2")
                            {
                                bytes = System.IO.File.ReadAllBytes(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["File2"]));
                            }
                            else if (File.Exists(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["AddEmailFile"])) && filetype == "3")
                            {
                                bytes = System.IO.File.ReadAllBytes(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["AddEmailFile"]));
                            }
                            else if (File.Exists(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["AddVRFFile"])) && filetype == "4")
                            {
                                bytes = System.IO.File.ReadAllBytes(FilePath + Convert.ToString(dtBookingIdDetails.Rows[0]["AddVRFFile"]));
                            }
                            else
                            {
                                bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/images/FileNA.pdf"));
                            }
                        }
                        else
                        {
                            bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/images/FileNA.pdf"));
                        }
                    }

                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }

    public void MergeFiles(string destinationFile, string[] sourceFiles)
    {
        if (System.IO.File.Exists(destinationFile))
            System.IO.File.Delete(destinationFile);
        string[] sSrcFile;
        sSrcFile = new string[sourceFiles.Length];

        string[] arr = new string[sourceFiles.Length];
        for (int i = 0; i <= sourceFiles.Length - 1; i++)
        {
            if (sourceFiles[i] != null)
            {
                if (sourceFiles[i].Trim() != "")
                    arr[i] = sourceFiles[i].ToString();
            }
        }

        if (arr != null)
        {
            sSrcFile = new string[sourceFiles.Length];

            for (int ic = 0; ic <= arr.Length - 1; ic++)
            {
                sSrcFile[ic] = arr[ic].ToString();
            }
        }
        try
        {
            int f = 0;

            PdfReader reader = new PdfReader(sSrcFile[f]);
            int n = reader.NumberOfPages;

            Document document = new Document(PageSize.A4);

            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));

            document.Open();
            PdfContentByte cb = writer.DirectContent;
            PdfImportedPage page;

            int rotation;
            while (f < sSrcFile.Length)
            {
                int i = 0;
                while (i < n)
                {
                    i++;

                    document.SetPageSize(PageSize.A4);
                    document.NewPage();
                    page = writer.GetImportedPage(reader, i);

                    rotation = reader.GetPageRotation(i);
                    if (rotation == 90 || rotation == 270)
                    {
                        cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                    }
                    else
                    {
                        cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                    }

                }

                f++;
                if (f < sSrcFile.Length)
                {
                    reader = new PdfReader(sSrcFile[f]);
                    n = reader.NumberOfPages;

                }
            }

            document.Close();
        }
        catch (Exception e)
        {

        }
    }



    public byte[] addByteToArray(byte[] bArray, byte[] newByte)
    {
        return null;
        //return bArray.Concat(newByte).ToArray();
    }
    public static byte[] CombineOld(byte[] first, byte[] second)
    {

        byte[] bytes = new byte[first.Length + second.Length];
        System.Buffer.BlockCopy(first, 0, bytes, 0, first.Length);
        System.Buffer.BlockCopy(second, 0, bytes, first.Length, second.Length);
        return bytes;
    }
    byte[] Combine(byte[] a1, byte[] a2)
    {
        byte[] ret = new byte[a1.Length + a2.Length];
        Array.Copy(a1, 0, ret, 0, a1.Length);
        Array.Copy(a2, 0, ret, a1.Length, a2.Length);
        // Array.Copy(a3, 0, ret, a1.Length + a2.Length, a3.Length);
        return ret;
    }
}