using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using ReportingTool;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using ExcelUtil;

public partial class SubEntityUpdate : System.Web.UI.Page
{
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lblMessage.Text = "";
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtBookingId.Text.Trim() == "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Enter booking id');", true);
                txtBookingId.Focus();
                return;
            }
            else
            {
                FillGrid();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }

    private void FillGrid()
    {
        DataTable dtBookingIdDetails = new DataTable();
        dtBookingIdDetails = objFleetTrackerSummary.GetBookingDetails(int.Parse(txtBookingId.Text));
        if (dtBookingIdDetails.Rows.Count > 0)
        {
            grvBookingDetails.DataSource = dtBookingIdDetails;
            grvBookingDetails.DataBind();
        }
        else
        {
            grvBookingDetails.DataSource = null;
            grvBookingDetails.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = "Record is not available.";
            lblMessage.ForeColor = Color.Red;
        }
    }

    protected void grvBookingDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DropDownList ddlBrand = (DropDownList)e.Row.FindControl("ddlBrand");
            //DropDownList ddlLocation = (DropDownList)e.Row.FindControl("ddlLocation");
            DropDownList ddlEntity = (DropDownList)e.Row.FindControl("ddlEntity");
            Label lblClientID = (Label)e.Row.FindControl("lblClientID");
            int ClientCoId = Convert.ToInt32(lblClientID.Text);
            HiddenField hdnSubsidiaryID = (HiddenField)e.Row.FindControl("hdnSubsidiaryID");
            //DataTable dtBrand = new DataTable();
            //dtBrand = objFleetTrackerSummary.GetBrand(ClientCoId);
            //if (dtBrand.Rows.Count > 0)
            //{
            //    ddlBrand.DataSource = dtBrand;
            //    ddlBrand.DataTextField = "MapFieldName";
            //    ddlBrand.DataBind();
            //    ddlBrand.Items.Insert(0, new ListItem("--Select Brand--", "0"));

            //}
            DataTable dtEntity = new DataTable();
            dtEntity = objFleetTrackerSummary.GetDivision(ClientCoId);
            if (dtEntity.Rows.Count > 0)
            {
                ddlEntity.DataSource = dtEntity;
                ddlEntity.DataTextField = "SubName";
                ddlEntity.DataValueField = "SubID";
                ddlEntity.DataBind();
                ddlEntity.Items.Insert(0, new ListItem("--Select Entity--", "0"));
            }

            ddlEntity.SelectedValue = hdnSubsidiaryID.Value;

            lblMessage.Text = "";
            //DataTable dtLocation = new DataTable();
            //dtLocation = objFleetTrackerSummary.GetLocation(ClientCoId);

            //if (dtLocation.Rows.Count > 0)
            //{
            //    ddlLocation.DataSource = dtLocation;
            //    ddlLocation.DataTextField = "Location";
            //    ddlLocation.DataBind();
            //    ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "0"));

            //}
        }
    }
    protected void grvBookingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Update")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = grvBookingDetails.Rows[index];
        }
    }
    protected void grvBookingDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //DropDownList ddlBrand = (DropDownList)grvBookingDetails.Rows[e.RowIndex].FindControl("ddlBrand");
        //DropDownList ddlLocation = (DropDownList)grvBookingDetails.Rows[e.RowIndex].FindControl("ddlLocation");
        DropDownList ddlEntity = (DropDownList)grvBookingDetails.Rows[e.RowIndex].FindControl("ddlEntity");
        Label BookingID = (Label)grvBookingDetails.Rows[e.RowIndex].FindControl("lblBookingID");
        int i = objFleetTrackerSummary.UpdateDetails(Convert.ToInt32(BookingID.Text), Convert.ToInt32(ddlEntity.SelectedValue), Convert.ToInt32(Session["UserID"]));
        if (i > 0)
        {
            txtBookingId.Text = "0";
        }
        grvBookingDetails.EditIndex = -1;
        FillGrid();
        lblMessage.Text = "Updated successfully";

    }
    protected void grvBookingDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grvBookingDetails.EditIndex = e.NewEditIndex;
        FillGrid();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (UpdateSubID.PostedFile.FileName == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Please select an excel file.');", true);
            return;
        }

        if (UpdateSubID.PostedFile.ContentLength <= 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Invalid File.');", true);
            return;
        }

        string FileName = "";
        string filePath = "";
        filePath = "D:/Upload/SubSidiary/";

        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        FileName = Path.GetFileName(UpdateSubID.FileName);

        if (FileName != "Subsidiary.xlsx" && FileName != "Subsidiary.xls")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('The file name should be Subsidiary.xlsx');", true);
            return;
        }

        string[] strArray = FileName.Split('.');

        string Newfilename = "";
        Newfilename = System.DateTime.Now.Year.ToString() + "_" + System.DateTime.Now.Month.ToString()
            + "_" + System.DateTime.Now.Day.ToString() + "_" + System.DateTime.Now.Hour.ToString()
            + "_" + System.DateTime.Now.Minute.ToString() + "_" + System.DateTime.Now.Second.ToString() + "." + strArray[1].ToString();

        UpdateSubID.SaveAs(filePath + Newfilename);

        DataTable dt = exceldata(filePath + Newfilename);
        if (dt.Rows.Count > 0)
        {
            string SubID = "", BookingID = "";

            string ExcelName = "";
            ExcelName = "SubEntity.xls";
            int status = 0;
            int subStatus= 0;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();


            DataTable dtable = new DataTable("MyTable");
            dtable.Columns.Add(new DataColumn("BookingID", typeof(string)));
            dtable.Columns.Add(new DataColumn("SubID", typeof(string)));
            dtable.Columns.Add(new DataColumn("Status", typeof(string)));

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                SubID = Convert.ToString(dt.Rows[i]["SubID"]);
                BookingID = Convert.ToString(dt.Rows[i]["BookingID"]);

                //update data start

                //subStatus = objFleetTrackerSummary.GetSubStaus(Convert.ToInt32(BookingID), Convert.ToInt32(SubID));
                ds1 = objFleetTrackerSummary.GetSubStaus(Convert.ToInt32(BookingID), Convert.ToInt32(SubID));

                //if (subStatus.Equals(0))
                //{
                //    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Subsidiary and client not matched for'" +BookingID+"' );", true);
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Subsidiary and client not matched for' );", true);



                //    return;
                //}
                DataRow dr = dtable.NewRow();
                if (Convert.ToString(ds1.Tables[0].Rows[0][0])=="1")
                {
                    status = objFleetTrackerSummary.UpdateDetails(Convert.ToInt32(BookingID), Convert.ToInt32(SubID), Convert.ToInt32(Session["UserID"]));

                }
                    
                    dr["SubID"] = SubID;
                    dr["BookingID"] = BookingID;
                    if (status > 0)
                    {
                        dr["Status"] = "Success";
                    }
                    else
                    {
                    dr["Status"] = ds1.Tables[0].Rows[0][1];
                                            
                    }

                
                    
                
                dtable.Rows.Add(dr);
                status = 0;
                //update data end
            }
            ds.Tables.Add(dtable);
           
            WorkbookEngine.ExportDataSetToExcel(ds, ExcelName);
        }
    }

    public static DataTable exceldata(string filePath)
    {
        DataTable dtexcel = new DataTable();
        bool hasHeaders = false;
        string HDR = hasHeaders ? "Yes" : "No";
        string strConn;
        if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
        else
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
        OleDbConnection conn = new OleDbConnection(strConn);
        conn.Open();
        DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

        DataRow schemaRow = schemaTable.Rows[0];
        string sheet = schemaRow["TABLE_NAME"].ToString();
        if (!sheet.EndsWith("_"))
        {
            string query = "SELECT * FROM [SubsidiaryID$]";
            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
            dtexcel.Locale = CultureInfo.CurrentCulture;
            daexcel.Fill(dtexcel);
        }

        conn.Close();
        return dtexcel;
    }
}