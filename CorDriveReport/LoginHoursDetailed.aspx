﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LoginHoursDetailed.aspx.cs" Inherits="CorDriveReport_LoginHoursDetailed" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <link href="../greybox/gb_styles.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }

        $(function () {
           // $("#<%=rptLoginHoursDetail.ClientID%> a[id*='lnkView']").click(function ()
           $("a").click (function()
            {
                //debugger;
                var caption = "Map View";
                var linkId = this.id
                var hdImageID = linkId.replace("lnkView", "hdlblView")
                var hdImageID = $("#" + hdImageID).val();
                var url = "http://insta.carzonrent.com/CorMeterGoogleAPI/MapWithCarNO.aspx?RegnNo=" + hdImageID
                //alert(url);
                //return GB_showFullScreen(caption, url)
                return GB_showCenter(caption, url, 400, 800)
            });
        });
   
    </script>
    <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
                <tr>
                    <td colspan="4" align="center">
                        <b>Log In Hours Detailed</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblBranch" runat="server">Branch</asp:Label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlBranch" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblToDate" runat="server">To date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblLoginLogout" runat="server">COR Drive</asp:Label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlLoginLogout" runat="server">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Logged In"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Logged Out"></asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;
                        <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <%--<div class="Repeater">--%>
        <div>
            <asp:Repeater ID="rptLoginHoursDetail" runat="server" OnItemDataBound="rptLoginHoursDetail_ItemDataBound"
                EnableTheming="True">
                <HeaderTemplate>
                    <table id="sort_table" width="100%" border="1">
                        <thead>
                            <tr>
                                <th align="center">
                                    S No.
                                </th>
                                <th align="center">
                                    Registration No.
                                </th>
                                <th align="center">
                                    Login Date
                                </th>
                                <th align="center">
                                    Logout Date
                                </th>
                                <th align="center">
                                    City Name
                                </th>
                                <th align="center">
                                    Chauffeur Name
                                </th>
                                <th align="center">
                                    Chauffeur Contact
                                </th>
                                <th align="center">
                                    Current Status
                                </th>
                                <th align="center">
                                    Hours Logged in
                                </th>
                                <th align="center">
                                    Duty Status
                                </th>
                                <th align="center">
                                    Current Location
                                </th>
                                <th align="center">
                                    BookingID
                                </th>
                                <th align="center">
                                    Version No
                                </th>
                                <th align="center">
                                    View current location
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="row_<%#Eval("Id") %>" style="font-family: Arial, Verdana, Sans-Serif;">
                        <td style="text-align: center;">
                            <%#Eval("Id") %>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DeviceId")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("Login_Date")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("LogOut_date")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("cityName")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("chauffuername")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("MobileNo")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("LoginStatus")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("LoginTime")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DutySlipStatus")%>
                            <asp:HiddenField ID="hdLat" runat="server" Value='<%#Eval("Lat")%>' />
                            <asp:HiddenField ID="hdLon" runat="server" Value='<%#Eval("Lon")%>' />
                        </td>
                        <td style="text-align: center;">
                            <asp:Label ID="lbl_CurrntLocation" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("bookingid")%>
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("cordriveversion")%>
                        </td>
                        <td style="text-align: center;">
                            <asp:LinkButton ID="lnkView" runat="server" Text=''></asp:LinkButton>
                            <asp:HiddenField ID="hdlblView" runat="server" Value='<%#Eval("DeviceId")%>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
            <div align="center" id="dvPaging" runat="server" visible="false">
                <asp:LinkButton ID="lnkPrev" runat="server" Text="Prev" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkPrev_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkNext" runat="server" Text="Next" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkNext_Click"></asp:LinkButton>
            </div>
        </div>
    </center>
</asp:Content>
