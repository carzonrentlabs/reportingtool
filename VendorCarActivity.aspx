﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorCarActivity.aspx.cs" Inherits="VendorCarActivityUpendra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script src="JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="JQuery/moment.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <link href="CSSReveal/reveal.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function pageLoad(sender, args) {

            $("#<%=txtDate.ClientID%>").datepicker();

            $("#<%=bntGetDateWise.ClientID%>").click(function () {
                if (document.getElementById('<%=ddlBranch.ClientID%>').value == "") {
                    alert("Please Select the City");
                    return false;
                }
                else {
                    ShowProgress();
                }
            });

            function ShowProgress() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
            function ShowProgressKill() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.removeClass("modal");
                    $('body').remove(modal);
                    loading.hide();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
        }
    </script>
    <center>
        <br />
        <br />
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td colspan="5" align="center" style="height: 30px;">
                    <b>Vendor Car Activity Report</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <b>City</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlBranch" runat="server"></asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Category</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlCategory" runat="server" Width="100px">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;" colspan="2">
                    <asp:Button ID="bntGetDateWise" runat="Server" Text="Get It" OnClick="bntGetDateWise_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="bntDateWiseExprot" runat="server" Text="Export To Excel" OnClick="bntDateWiseExprot_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdVendorCarActivity" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
