using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ReportingTool;

public partial class BookingMandoryFieldUpdate : System.Web.UI.Page
{
    clsAutomation objMandate = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            lblMessage.Text = "";
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        DataTable dtBookingIdDetails = new DataTable();
        DataTable dtMapField = new DataTable();
        lblMessage.Visible = false;
        try
        {
            if (txtBookingId.Text.Trim() == "")
            {
                return;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Enter booking id');", true);
            }
            else
            {
                dtBookingIdDetails = objMandate.GetMandatoryFields(int.Parse(txtBookingId.Text));
                if (dtBookingIdDetails.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc1"])))
                    {
                        tr1.Style.Add("display", "block");
                        lblMisc1.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc1"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype1"]) == "1")
                        {
                            ddlMisc1.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID1"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc1");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc1.DataSource = dtMapField;
                                ddlMisc1.DataValueField = "MapFieldName";
                                ddlMisc1.DataTextField = "MapFieldName";
                                ddlMisc1.DataBind();
                                ddlMisc1.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous1"]);
                            }
                        }
                        else
                        {
                            txtMisc1.Style.Add("display", "block");
                            txtMisc1.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous1"]);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc2"])))
                    {
                        tr2.Style.Add("display", "block");
                        lblMisc2.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc2"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype2"]) == "1")
                        {
                            ddlMisc2.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID2"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc2");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc2.DataSource = dtMapField;
                                ddlMisc2.DataValueField = "MapFieldName";
                                ddlMisc2.DataTextField = "MapFieldName";
                                ddlMisc2.DataBind();
                                ddlMisc2.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous2"]);
                            }

                        }
                        else
                        {
                            txtMisc2.Style.Add("display", "block");
                            txtMisc2.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous2"]);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc3"])))
                    {
                        tr3.Style.Add("display", "block");
                        lblMisc3.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc3"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype3"]) == "1")
                        {
                            ddlMisc3.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID3"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc3");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc3.DataSource = dtMapField;
                                ddlMisc3.DataValueField = "MapFieldName";
                                ddlMisc3.DataTextField = "MapFieldName";
                                ddlMisc3.DataBind();
                                ddlMisc3.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous3"]);
                            }
                        }
                        else
                        {
                            txtMisc3.Style.Add("display", "block");
                            txtMisc3.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous3"]);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc4"])))
                    {
                        tr4.Style.Add("display", "block");
                        lblMisc4.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc4"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc4.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID2"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc4");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc4.DataSource = dtMapField;
                                ddlMisc4.DataValueField = "MapFieldName";
                                ddlMisc4.DataTextField = "MapFieldName";
                                ddlMisc4.DataBind();
                                ddlMisc4.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous4"]);
                            }
                        }
                        else
                        {
                            txtMisc4.Style.Add("display", "block");
                            txtMisc4.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous4"]);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc5"])))
                    {
                        tr5.Style.Add("display", "block");
                        lblMisc5.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc5"]);

                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc5.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID2"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc5");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc5.DataSource = dtMapField;
                                ddlMisc5.DataValueField = "MapFieldName";
                                ddlMisc5.DataTextField = "MapFieldName";
                                ddlMisc5.DataBind();
                                ddlMisc5.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous5"]);
                            }
                        }
                        else
                        {
                            txtMisc5.Style.Add("display", "block");
                            txtMisc5.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous5"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc6"])))
                    {
                        tr6.Style.Add("display", "block");
                        lblMisc6.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc6"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc6.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID2"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc6");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc6.DataSource = dtMapField;
                                ddlMisc6.DataValueField = "MapFieldName";
                                ddlMisc6.DataTextField = "MapFieldName";
                                ddlMisc6.DataBind();
                                ddlMisc6.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous6"]);
                            }
                        }
                        else
                        {
                            txtMisc6.Style.Add("display", "block");
                            txtMisc6.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous6"]);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc7"])))
                    {
                        tr7.Style.Add("display", "block");
                        lblMisc7.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc7"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc7.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID2"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc7");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc7.DataSource = dtMapField;
                                ddlMisc7.DataValueField = "MapFieldName";
                                ddlMisc7.DataTextField = "MapFieldName";
                                ddlMisc7.DataBind();
                                ddlMisc7.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous7"]);
                            }
                        }
                        else
                        {
                            txtMisc7.Style.Add("display", "block");
                            txtMisc7.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous7"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc8"])))
                    {
                        tr8.Style.Add("display", "block");
                        lblMisc8.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc8"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc8.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID8"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc8");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc8.DataSource = dtMapField;
                                ddlMisc8.DataValueField = "MapFieldName";
                                ddlMisc8.DataTextField = "MapFieldName";
                                ddlMisc8.DataBind();
                                ddlMisc8.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous8"]);
                            }
                        }
                        else
                        {
                            txtMisc8.Style.Add("display", "block");
                            txtMisc8.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous8"]);
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc9"])))
                    {
                        tr9.Style.Add("display", "block");
                        lblMisc9.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc9"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc9.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID9"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc9");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc9.DataSource = dtMapField;
                                ddlMisc9.DataValueField = "MapFieldName";
                                ddlMisc9.DataTextField = "MapFieldName";
                                ddlMisc9.DataBind();
                                ddlMisc9.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous9"]);
                            }
                        }
                        else
                        {
                            txtMisc9.Style.Add("display", "block");
                            txtMisc9.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous9"]);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(dtBookingIdDetails.Rows[0]["Misc10"])))
                    {
                        tr10.Style.Add("display", "block");
                        lblMisc10.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Misc10"]);
                        if (Convert.ToString(dtBookingIdDetails.Rows[0]["controltype4"]) == "1")
                        {
                            ddlMisc10.Style.Add("display", "block");
                            dtMapField = objMandate.GetMandatoryFields(Convert.ToInt32(dtBookingIdDetails.Rows[0]["clientcoid"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["ID10"]), Convert.ToInt32(dtBookingIdDetails.Rows[0]["PickupcityID"]), "Misc10");
                            if (dtMapField.Rows.Count > 0)
                            {
                                ddlMisc10.DataSource = dtMapField;
                                ddlMisc10.DataValueField = "MapFieldName";
                                ddlMisc10.DataTextField = "MapFieldName";
                                ddlMisc10.DataBind();
                                ddlMisc10.SelectedValue = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous10"]);
                            }
                        }
                        else
                        {
                            txtMisc10.Style.Add("display", "block");
                            txtMisc10.Text = Convert.ToString(dtBookingIdDetails.Rows[0]["Miscellaneous10"]);
                        }
                    }

                    trSubmit.Style.Add("display", "block");
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Record not available";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }

            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void bntSubmit_Click(object sender, EventArgs e)
    {
        Miscellaneous objMis = new Miscellaneous();
        int status = 0;
        lblMessage.Visible = false;
        try
        {

            if (txtBookingId.Text.Trim() == "")
            {
                return;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Enter booking id');", true);
            }
            else
            {

                objMis.BookingID = int.Parse(txtBookingId.Text);
                objMis.SysUserId = Convert.ToInt32(Session["UserID"]);
                if (txtMisc1.Style["display"] == "block")
                {
                    objMis.Miscellaneous1 = Convert.ToString(txtMisc1.Text);
                }
                else if (ddlMisc1.Style["display"] == "block")
                {
                    objMis.Miscellaneous1 = Convert.ToString(ddlMisc1.SelectedValue);
                }
                if (txtMisc2.Style["display"] == "block")
                {
                    objMis.Miscellaneous2 = Convert.ToString(txtMisc2.Text);
                }
                else if (ddlMisc2.Style["display"] == "block")
                {
                    objMis.Miscellaneous2 = Convert.ToString(ddlMisc2.SelectedValue);
                }
                if (txtMisc3.Style["display"] == "block")
                {
                    objMis.Miscellaneous3 = Convert.ToString(txtMisc3.Text);
                }
                else if (ddlMisc3.Style["display"] == "block")
                {
                    objMis.Miscellaneous3 = Convert.ToString(ddlMisc3.SelectedValue);
                }
                if (txtMisc4.Style["display"] == "block")
                {
                    objMis.Miscellaneous4 = Convert.ToString(txtMisc4.Text);
                }
                else if (ddlMisc4.Style["display"] == "block")
                {
                    objMis.Miscellaneous4 = Convert.ToString(ddlMisc4.SelectedValue);
                }
                if (txtMisc5.Style["display"] == "block")
                {
                    objMis.Miscellaneous5 = Convert.ToString(txtMisc5.Text);
                }
                else if (ddlMisc5.Style["display"] == "block")
                {
                    objMis.Miscellaneous5 = Convert.ToString(ddlMisc5.SelectedValue);
                }
                if (txtMisc6.Style["display"] == "block")
                {
                    objMis.Miscellaneous6 = Convert.ToString(txtMisc6.Text);
                }
                else if (ddlMisc6.Style["display"] == "block")
                {
                    objMis.Miscellaneous6 = Convert.ToString(ddlMisc6.SelectedValue);
                }
                if (txtMisc7.Style["display"] == "block")
                {
                    objMis.Miscellaneous7 = Convert.ToString(txtMisc7.Text);
                }
                else if (ddlMisc7.Style["display"] == "block")
                {
                    objMis.Miscellaneous7 = Convert.ToString(ddlMisc7.SelectedValue);
                }
                if (txtMisc8.Style["display"] == "block")
                {
                    objMis.Miscellaneous8 = Convert.ToString(txtMisc8.Text);
                }
                else if (ddlMisc8.Style["display"] == "block")
                {
                    objMis.Miscellaneous8 = Convert.ToString(ddlMisc8.SelectedValue);
                }
                if (txtMisc9.Style["display"] == "block")
                {
                    objMis.Miscellaneous9 = Convert.ToString(txtMisc9.Text);
                }
                else if (ddlMisc9.Style["display"] == "block")
                {
                    objMis.Miscellaneous9 = Convert.ToString(ddlMisc9.SelectedValue);
                }
                if (txtMisc10.Style["display"] == "block")
                {
                    objMis.Miscellaneous10 = Convert.ToString(txtMisc10.Text);
                }
                else if (ddlMisc10.Style["display"] == "block")
                {
                    objMis.Miscellaneous10 = Convert.ToString(ddlMisc10.SelectedValue);
                }
                status = objMandate.UpdateMiscellaneousField(objMis);
                if (status > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Successfully updated.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Getting error while updating...";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }

    }
}
