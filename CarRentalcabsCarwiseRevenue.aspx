<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CarRentalcabsCarwiseRevenue.aspx.cs" Inherits="CarRentalcabsCarwiseRevenue" Title="Easycabs Car Wise Revenue Report" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       $(document).ready(function () 
       {
           
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
           
            $("#<%=btnExport.ClientID%>").click(function () {
             
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            gridviewScroll();
       });
         
     function gridviewScroll() {
        $('#<%=gvEasycabsCorpBooking.ClientID%>').gridviewScroll({
            width: 1200,
            height: 450
        });
    } 

       
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width: 1200px">
            <tr>
                <td colspan="8" align="center" bgcolor="#FF6600" style="height: 17px">
                    <strong>CarRental Car Wise Revenue Report</strong>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;" align="right">
                    <b>From Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                </td>
                <td style="white-space: nowrap;" align="right">
                    <b>To Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </td>
                <td style="white-space: nowrap;" align="right">
                    <b>City :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                    <asp:DropDownList ID="ddlCity" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;
                    <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    <asp:GridView ID="gvEasycabsCorpBooking" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Sr. No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Taxi No" DataField="Taxino" />
                            <asp:BoundField HeaderText="Total Login Duration" DataField="Loginduration" />
                            <asp:BoundField HeaderText="Total Bids Sent" DataField="totalbidssend" />
                            <asp:BoundField HeaderText="Bids Reached" DataField="bidsReached" />
                            <asp:BoundField HeaderText="Bids Accepted" DataField="bidsaccepted" />
                            <asp:BoundField HeaderText="Last Calculation Reset At" DataField="calcResetAt" />
                            <asp:BoundField HeaderText="First Bid Accept At" DataField="firstbidacceptat" />
                            <asp:BoundField HeaderText="Last Bid Accept At" DataField="lastbidacceptat" />
                            <asp:BoundField HeaderText="Auto Bid Duty" DataField="autobidduty" />
                            <asp:BoundField HeaderText="Auto Bid Revenue" DataField="autobidrevenue" />
                            <asp:BoundField HeaderText="Manual Duty" DataField="manualduty" />
                            <asp:BoundField HeaderText="Manual Revenue" DataField="manualrevenue" />
                            <asp:BoundField HeaderText="Kurb Duty" DataField="kurbduty" />
                            <asp:BoundField HeaderText="Kurb Revenue" DataField="kurbrevenue" />
                            <asp:BoundField HeaderText="First Duty Pickup At" DataField="firstdutypickupat" />
                            <asp:BoundField HeaderText="Last Duty Pickup At" DataField="lastdutypickupat" />
                            <asp:BoundField HeaderText="Total Duty" DataField="totalduty" />
                            <asp:BoundField HeaderText="Total Revenue" DataField="totalrevenue" />
                            <asp:BoundField HeaderText="Revenue KM" DataField="revenuekm" />
                            <asp:BoundField HeaderText="Non Revenue KM" DataField="nonrevenuekm" />
                            <asp:BoundField HeaderText="Approx. Call Center Duty KM" DataField="approxccdutykm" />
                            <asp:BoundField HeaderText="Vehicle No" DataField="regnno" />
                        </Columns>
                        <HeaderStyle CssClass="GridviewScrollHeader" />
                        <RowStyle CssClass="GridviewScrollItem" />
                        <PagerStyle CssClass="GridviewScrollPager" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
