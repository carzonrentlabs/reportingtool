﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VendorCarActivityUpendra.aspx.cs" Inherits="VendorCarActivityUpendra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script src="JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="JQuery/moment.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <link href="CSSReveal/reveal.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function pageLoad(sender, args) {

            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();

            $("#<%=txtRMFromDate.ClientID%>").datepicker();
            $("#<%=txtRMToDate.ClientID%>").datepicker();

            $("#<%=bntGetDateWise.ClientID%>").click(function () {
                if (document.getElementById('<%=ddlBranch.ClientID%>').value == "") {
                    alert("Please Select the City");
                    return false;
                }
                else {
                    ShowProgress();
                }
            });

            $("#<%=btnGetRMPerformence.ClientID%>").click(function () {
                if (document.getElementById('<%=ddlRMBranch.ClientID%>').value == "") {
                    alert("Please Select the City");
                    return false;
                }
                else {
                    ShowProgress();
                }
            });


            function ShowProgress() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
            function ShowProgressKill() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.removeClass("modal");
                    $('body').remove(modal);
                    loading.hide();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }

            $('a[data-reveal-id]').live('click', function (e) {
                e.preventDefault();
                var id = $(this).attr("id")
                var CarId = $("#" + id).text()
                $("#<%=txtCabId.ClientID%>").val(CarId);
                $("#txtCabId").val("");
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: "Handler.ashx",
                    data: { cabId: CarId },
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    success: function (response) {
                        if (response != "Record Not Found") {
                            var ReturnString = response.split("#")
                            $("#<%=ddlReasonType.ClientID%>").val(ReturnString[1]);
                            $("#<%=txtFromDateReason.ClientID%>").val(ReturnString[2]);
                            $("#<%=txtToDateReason.ClientID%>").val(ReturnString[3]);
                            $("#<%=txtRemarks.ClientID%>").val(ReturnString[4]);
                            $('#trFromDate').css('display', 'block');
                            $('#trToDate').css('display', 'block');
                            $('#trRemarks').css('display', 'block');
                            $("#<%=hid_Fromdate.ClientID%>").val(ReturnString[2]);
                            $("#<%=hid_Todate.ClientID%>").val(ReturnString[3]);
                            $("#<%=hid_CheckData.ClientID%>").val("OldData");

                            // $("#<%=txtFromDateReason.ClientID%>").attr('readonly', 'true'); 
                            // $('#billAccountNumber').attr('readonly', 'true'); 

                        }
                        else {
                            $("#<%=txtFromDateReason.ClientID%>").val(moment().format('L'));
                            $("#<%=txtToDateReason.ClientID%>").val(moment().format('L'));
                            $("#<%=txtRemarks.ClientID%>").val("")
                            //                         $("#<%=hid_Fromdate.ClientID%>").val(moment().format('L'));
                            //                        $("#<%=hid_Todate.ClientID%>").val(moment().format('L'));
                            $("#<%=hid_CheckData.ClientID%>").val("NewData");
                            // $("#<%=ddlReasonType.ClientID%>").val("Select Reason");
                            // $("#<%=txtFromDateReason.ClientID%>").attr('readonly', 'false'); 
                        }
                    },
                    error: function (msg) {
                        alert(msg.responseText);
                    }
                });
                var modalLocation = $(this).attr('data-reveal-id');
                $('#' + modalLocation).reveal($(this).data());
            });

            $("#bntCancel").click(function () {
                $(".close-reveal-modal").trigger("click");
                $("#<%=ddlReasonType.ClientID%>").selectedIndex = "0";
                $('#trFromDate').css('display', 'none');
                $('#trToDate').css('display', 'none');
                $('#trRemarks').css('display', 'none');
            });

            $('#<%=ddlReasonType.ClientID %>').change
            (function () {
                if (this.value == 'Available For Duty') {
                    $('#trFromDate').css('display', 'none');
                    $('#trToDate').css('display', 'none');
                    $('#trRemarks').css('display', 'none');
                }
                else {
                    $('#trFromDate').css('display', 'block');
                    $('#trToDate').css('display', 'block');
                    $('#trRemarks').css('display', 'block');
                }
            });
        }
    </script>
    <center>
        <asp:HiddenField ID="hid_Fromdate" runat="server" />
        <asp:HiddenField ID="hid_Todate" runat="server" />
        <asp:HiddenField ID="hid_CheckData" runat="server" />
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td colspan="5" align="center" style="background-color: #ccffff; height: 30px;">
                    <b>RM Performance</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Type</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlServiceTypeRm" runat="server">
                        <asp:ListItem Value="C">Limo</asp:ListItem>
                        <asp:ListItem Value="0">CRD</asp:ListItem>
                        <asp:ListItem Value="A">AirPort</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Branch</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlRMBranch" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup From Date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtRMFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup To date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtRMToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Button ID="btnGetRMPerformence" runat="Server" Text="Get It" OnClick="btnGetRMPerformence_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnExportRMPerformence" runat="server" Text="Export To Excel" OnClick="btnExportRMPerformence_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td colspan="5">
                                    <asp:GridView ID="grdRM" runat="server" BorderColor="Black" BorderStyle="None" BorderWidth="1px"
                                        CellPadding="3" HeaderStyle-Width="70px" AutoGenerateColumns="false" OnRowDataBound="grdRM_RowDataBound"
                                        ShowFooter="true">
                                        <Columns>
                                            <asp:BoundField HeaderText="Relationship Manager" ItemStyle-Width="70px" DataField="ManagerName" />
                                            <asp:BoundField HeaderText="Active Cars" ItemStyle-Width="70px" DataField="ActiveCars"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <%--<asp:BoundField HeaderText="COR Drive Cars" ItemStyle-Width="70px" DataField="CORDriveCars" ItemStyle-HorizontalAlign="Center" />--%>
                                            <asp:TemplateField HeaderText="COR Drive Cars" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCORDriveCars" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CORDriveCars") %>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblCORDriveCarsTotal" runat="server" />
                                                    <hr />
                                                    <font style="font-size: 8px">(Target-100 each) (KPI Rating-20%)</font>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:BoundField HeaderText="Logged in Cars before 10 am" ItemStyle-Width="70px" DataField="LoggedinCarsbefore" ItemStyle-HorizontalAlign="Center"/>--%>
                                            <asp:TemplateField HeaderText="Logged in Cars before 10 am (AVG)" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoggedin" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"LoggedinCarsbefore") %>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lbl10amTotal" runat="server" />
                                                    <hr />
                                                    <font style="font-size: 8px">(Target 90 Each) (KPI Rating-20%)</font></td>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <%--  <asp:BoundField HeaderText="No of Cars accepted BIDS" ItemStyle-Width="70px" DataField="NoOfCarsacceptedBIDS" ItemStyle-HorizontalAlign="Center" />--%>
                                            <asp:TemplateField HeaderText="No of Cars accepted BIDS (AVG)" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNoOfCarsacceptedBIDS" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NoOfCarsacceptedBIDS") %>' />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblNoOfCarsacceptedBIDSTotal" runat="server" />
                                                    <hr />
                                                    <font style="font-size: 8px">(Target 80 Each) (Rating-30%)</font>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="No of Duties Done" ItemStyle-Width="70px" DataField="NoOfDutiesDone"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="No of duties on COR Drive" ItemStyle-Width="70px" DataField="NoOfDutiesonCORDrive"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <%--<asp:BoundField HeaderText="No of Chauffeur Complaint" ItemStyle-Width="70px" DataField="NoOfChauffeurComplaint" ItemStyle-HorizontalAlign="Center"/>--%>
                                            <asp:TemplateField HeaderText="No of Chauffeur Complaint" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNoofChauffeurComplaint" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NoOfChauffeurComplaint") %>' />
                                                </ItemTemplate>
                                                <%--<FooterTemplate>
                                     <asp:Label ID="lblNoofChauffeurComplaintTotal" runat="server"/>
                                       <br />
                                    <asp:Label ID="lblNoofChauffeurComplaints" runat="server" Text="KPI Rating-15%" />
                                    </FooterTemplate>--%>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblNoofChauffeurComplaintTotal" runat="server" />
                                                    <hr />
                                                    <font style="font-size: 8px">KPI Rating-15%</font>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:BoundField HeaderText="No of Customer complaint" ItemStyle-Width="70px" DataField="NoofCustomercomplaint" ItemStyle-HorizontalAlign="Center"/>--%>
                                            <asp:TemplateField HeaderText="No of Customer complaint" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNoofCustomercomplaint" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NoofCustomercomplaint") %>' />
                                                </ItemTemplate>
                                                <%-- <FooterTemplate>
                                     
                                       <br />
                                    
                                    </FooterTemplate>--%>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblNoofCustomercomplaintTotal" runat="server" />
                                                    <hr />
                                                    <font style="font-size: 8px">KPI Rating-15%</font>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField HeaderText="Rating" ItemStyle-Width="70px" DataField="Rating" ItemStyle-BackColor="BlueViolet" ItemStyle-HorizontalAlign="Center"/>--%>
                                            <asp:TemplateField HeaderText="Rating" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRating" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Rating") %>'
                                                        DataFormatString="{0:2}" />
                                                </ItemTemplate>
                                                <%--<FooterTemplate>
                                     
                                       <br />
                                   
                                    </FooterTemplate>--%>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblRatingTotal" runat="server" Visible="false" />
                                                    <font style="font-size: 8px">Rating (Max -5)</font>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="0-5 duties" ItemStyle-Width="70px" DataField="duties_0_5"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="5-10 duties" ItemStyle-Width="70px" DataField="duties_5_10"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="10-15 duties" ItemStyle-Width="70px" DataField="duties_10_15"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="15-20 duties" ItemStyle-Width="70px" DataField="duties_15_20"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="20 > duties" ItemStyle-Width="70px" DataField="duties_Greater_20"
                                                ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <FooterStyle BackColor="#006699" ForeColor="#000066" HorizontalAlign="Center" />
                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#006699" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td colspan="5" align="center" style="background-color: #ccffff; height: 30px;">
                    <b>Vendor Car Activity Report</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Type</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlServiceTypevendor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlServiceTypevendor_SelectedIndexChanged">
                        <asp:ListItem Value="1">Limo</asp:ListItem>
                        <asp:ListItem Value="3">CRD</asp:ListItem>
                        <asp:ListItem Value="2">AirPort</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Branch</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                 <td style="height: 20px; white-space: nowrap;">
                    <b>Sub Locaion</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlSubLocation" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Vendor</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlvendors" runat="server" Width="100px">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                 <td style="height: 20px; white-space: nowrap;">
                    <b>Share Percentage</b>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlsharepercent" runat="server" Width="100px">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup From Date</b>&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Pickup To date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:DropDownList ID="ddlReason" runat="server" ValidationGroup="FS">
                        <asp:ListItem Text="Select Reason" Value="Select Reason" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Available For Duty" Value="Available For Duty"></asp:ListItem>
                        <asp:ListItem Text="Not Interested" Value="Not Interested"></asp:ListItem>
                        <asp:ListItem Text="Driver Not Available" Value="Driver Not Available"></asp:ListItem>
                        <asp:ListItem Text="On Personal Duty" Value="On Personal Duty"></asp:ListItem>
                        <asp:ListItem Text="At Home Town" Value="At Home Town"></asp:ListItem>
                        <asp:ListItem Text="Car In Workshop" Value="Car In Workshop"></asp:ListItem>
                        <asp:ListItem Text="Payment Issue" Value="Payment Issue"></asp:ListItem>
                        <asp:ListItem Text="Forced off road" Value="Forced off road"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                
                <td style="height: 20px; white-space: nowrap;">
                    <asp:DropDownList ID="ddlStatusANI" runat="server" AutoPostBack="True" ValidationGroup="FS">
                        <%--<asp:ListItem Text="Select Status" Value="2" Selected="True"></asp:ListItem>--%>
                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                        <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <asp:Button ID="bntGetDateWise" runat="Server" Text="Get It" OnClick="bntGetDateWise_Click1" />
                    &nbsp;&nbsp;
                    <asp:Button ID="bntDateWiseExprot" runat="server" Text="Export To Excel" OnClick="bntDateWiseExprot_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="grdVendorCarActivity" runat="server" OnRowCommand="grdVendorCarActivity_RowCommand"
                            OnRowCreated="grdVendorCarActivity_RowCreated" OnRowDataBound="grdVendorCarActivity_RowDataBound"
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                            CellPadding="3" OnRowEditing="grdVendorCarActivity_RowEditing">
                            <Columns>
                                <asp:TemplateField HeaderText="Enter Not Available Reason">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkBGetBookingId" runat="server" class="big-link" data-reveal-id="myModal"
                                            Text='<%#Eval("Carid") %>' ForeColor="Red" CommandName="Edit" CommandArgument='<%#Eval("Carid")%>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButtonView" runat="server" CommandName="View" CommandArgument='<%#Eval("Carid") %>'>View History</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="images/loader.gif" alt="" />
        </div>
        <div id="myModal" class="reveal-modal">
            <fieldset style="border-color: Green">
                <legend><b>Add Not Available Details</b></legend>
                <div>
                    <table>
                        <tr>
                            <td align="right">
                                CabId:
                            </td>
                            <td align="left">
                                <input type="text" name="txtCabId" id="txtCabId" runat="server" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblMesageRoadOff" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Reason:
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlReasonType" runat="server" ValidationGroup="FS">
                                    <asp:ListItem Text="Available For Duty" Value="Available For Duty"></asp:ListItem>
                                    <asp:ListItem Text="Not Interested" Value="Not Interested"></asp:ListItem>
                                    <asp:ListItem Text="Driver Not Available" Value="Driver Not Available"></asp:ListItem>
                                    <asp:ListItem Text="On Personal Duty" Value="On Personal Duty"></asp:ListItem>
                                    <asp:ListItem Text="At Home Town" Value="At Home Town"></asp:ListItem>
                                    <asp:ListItem Text="Car In Workshop" Value="Car In Workshop"></asp:ListItem>
                                    <asp:ListItem Text="Payment Issue" Value="Payment Issue"></asp:ListItem>
                                    <asp:ListItem Text="Forced off road" Value="Forced off road"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblTest" runat="server" />
                            </td>
                        </tr>
                        <tr id="trFromDate" style="display: none; position: relative;">
                            <td align="right">
                                From Date:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFromDateReason" runat="server" ValidationGroup="FS"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" PopupPosition="TopLeft"
                                    TargetControlID="txtFromDateReason">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rvFromDateReason" SetFocusOnError="True" ValidationGroup="FS"
                                    runat="server" ErrorMessage="Please Enter From Date" ControlToValidate="txtFromDate"
                                    Display="None"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="trToDate" style="display: none; position: relative;">
                            <td align="right" style="height: 30px">
                                To Date:
                            </td>
                            <td align="left" style="height: 30px">
                                <asp:TextBox ID="txtToDateReason" runat="server" ValidationGroup="FS"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" PopupPosition="TopLeft"
                                    TargetControlID="txtToDateReason">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rvToDateReason" SetFocusOnError="True" ValidationGroup="FS"
                                    runat="server" ErrorMessage="Please Enter From Date" ControlToValidate="txtFromDate"
                                    Display="None"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="trRemarks" style="display: none; position: relative;">
                            <td align="right" style="height: 20px">
                                Remarks:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRemarks" runat="server" ValidationGroup="FS"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvtxtReason" SetFocusOnError="True" ValidationGroup="FS"
                                    runat="server" ErrorMessage="Please Enter From Date" ControlToValidate="txtFromDate"
                                    Display="None"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="bntSubmit" runat="server" Text="Submit" OnClick="bntSubmit_Click" />
                                &nbsp; &nbsp; &nbsp;
                                <input type="button" id="bntCancel" value="Cancel" class="button_quote" />
                                &nbsp; &nbsp; &nbsp;
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>
        </div>
    </center>
</asp:Content>
