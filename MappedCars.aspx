<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MappedCars.aspx.cs" Inherits="MappedCars" Title="Mapped Cars" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="cntPlaceholder" ContentPlaceHolderID="cphPage" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../JQuery/jquery.reveal.js"></script>
    <link href="../CSSReveal/reveal.css" rel="stylesheet" />
    
    <script src="../JQuery/moment.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function () 
        {
            $("[id*=CheckBox_Header]").live("click", function () {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function () {
                    if (chkHeader.is(":checked")) {
                        $(this).attr("checked", "checked");
                        $("td", $(this).closest("tr")).addClass("selected");
                    } else {
                        $(this).removeAttr("checked");
                        $("td", $(this).closest("tr")).removeClass("selected");
                    }
                });

            });

            $("[id*=btnDeactive]").click(function () {
                var chkboxrowcount = $("#<%=gvMappedCars.ClientID%> input[id*='CheckBox_row']:checkbox:checked").size();
                var totalRecord = $("table[id*=gvMappedCars]").find("TR").length - 1;               
                if (chkboxrowcount == 0) {
                    alert("Please select at least one booking id  for updating remarks.");
                    return false;
                }
               
                if ($("[id*=txtBulkDeactiveReason]").val() == "")
                {                    
                    alert("Enter decativation reason.");
                    $("[id*=txtBulkDeactiveReason]").focus();
                    return false;
                }
                
            });
            $("[id*=btnSave]").live("click", function () {

                if ($("[id*=ddlVendorType]")[0].selectedIndex == 0) {
                    alert("Select vendor type.")
                    $("[id*=ddlVendorType]").focus();
                    return false;
                }
                else if ($("[id*=ddlServiceType]")[0].selectedIndex == 0) {
                    alert("Select service type.")
                    $("[id*=ddlServiceType]").focus();
                    return false;
                }
                else if ($("[id*=txtRegNo]").val() == "") {
                    alert("Ender vehicle registration number.")
                    $("[id*=txtRegNo]").focus();
                    return false;
                }

            });

        })
    </script>


    <style type="text/css">
        .hideGridColumn {
            display: none;
        }


        .modalBackground {
            height: 100%;
            background-color: #EBEBEB;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }

        .body {
            background-color: #000000;
        }

        .style2 {
            color: #000000;
            height: 2px;
            font-size: medium;
        }

        .style3 {
            font-size: medium;
            color: #000000;
            text-align: center;
        }

        .style5 {
            font-family: Verdana;
            font-size: medium;
        }
    </style>




    <fieldset class="FieldsetClassic" style="text-align: center; width: 95%;">
        <center>
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            <legend class="style5">
                <strong>Mapped Cars</strong></legend>&nbsp;
        </center>

        <center>
            <table cellpadding="0" cellspacing="0" style="width: 95%">
                <tr>
                    <td>
                        <asp:Panel ID="Panel1" runat="server" BorderColor="Black" BorderWidth="1px"
                            Style="width: 90%" Visible="true">
                            <table>
                                <tr>
                                    <td>
                                        <span style="font-size: 8pt; font-family: Verdana"><strong>Status</strong>
                                        </span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlApprovedStatus" runat="server" Width="174px">
                                            <asp:ListItem Text="All" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <span style="font-size: 8pt; font-family: Verdana"><strong>City</strong> </span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <strong>Client Name</strong>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlClientName" runat="server" Width="174px">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                            OnClick="btnSearch_Click" Text="Search" ValidationGroup="Booking" />
                                    </td>
                                    <td>
                                        <asp:Button ID="bnAdd" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                            OnClick="bnAdd_Click" Text="Add" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnResetAll" runat="server" Font-Names="Verdana"
                                            Font-Size="8pt" OnClick="btnResetAll_Click" Text="Reset" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnDeactive" runat="server" Text="DeActivate" Visible="false" OnClick="btnDeactive_Click" /></td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblDeActivateMessage" runat="server" Visible="false" Text="De-Active Reason"></asp:Label>
                        <asp:TextBox ID="txtBulkDeactiveReason" runat="server" TextMode="MultiLine" Rows="2" Columns="10" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 168px">
                        <asp:GridView ID="gvMappedCars" runat="server" SkinID="SandAndSkywithoutwidth1" EmptyDataText="--No Record Found--"
                            Width="90%" Font-Names="Verdana" Font-Overline="False" Font-Size="7pt"
                            AutoGenerateColumns="False" OnRowDataBound="gvMappedCars_RowDataBound"
                            CellPadding="4" EnableModelValidation="True" ForeColor="#333333"
                            BorderStyle="Solid" BorderWidth="1px"
                            OnRowCommand="gvMappedCars_RowCommand">
                            <EditRowStyle BackColor="#2461BF" />
                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100%" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>

                                <asp:TemplateField HeaderText="SNO" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSerialNo" runat="server" Text='<%# Container.DataItemIndex+1 %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="CarID" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCarID" runat="server" Text='<%# Bind("CarID") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="RegNo" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegNo" runat="server" Text='<%# Bind("RegnNo") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Vendor/Own" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVendor" runat="server" Text='<%# Bind("OwnOrVendor") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ModelCategory" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblModelCategory" runat="server" Text='<%# Bind("carmodelName") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Remarks" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("Remarks") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActive" runat="server" Text='<%# Bind("GrpActive") %>' Visible="true"></asp:Label>
                                        <asp:Label ID="lblActiveValue" runat="server" Text='<%# Bind("ActiveValue") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="CreatedBy" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Bind("LoginId") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CreatedOn" Visible="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedAt" runat="server" Text='<%# Bind("CreateDate") %>' Visible="true"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink ID="hprLnkView" runat="server" Font-Bold="True" ForeColor="Red" NavigateUrl='<%#Eval("ReceiptFileName") %>'
                                                                Target="_blank">View</asp:HyperLink>--%>
                                        <br />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>


                                <asp:TemplateField Visible="True">
                                    <ItemTemplate>
                                        <asp:Button ID="btnEdit" Width="111px" Text="Edit" runat="server" CommandName="select" Visible="true"
                                            CommandArgument='<%#Bind("RegnNo")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CheckBox_Header" AutoPostBack="false" runat="server" />
                                        Deactivate
                                       
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox_row" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                        <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal>
                    </td>

                </tr>
                <tr>
                    <td style="height: 50px">
                        <asp:Panel ID="pnlpopup" runat="server" Width="45%" BorderWidth="2px" BorderStyle="Solid" BorderColor="Black" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <table style="border-right: black 1px solid; border-top: black 1px solid; font-size: 9pt; border-left: black 1px solid; width: 100%; border-bottom: black 1px solid; font-family: Tahoma; background-color: #EBEBEB; text-align: left">
                                        <tbody>
                                            <tr style="height: 10px">
                                                <td colspan="3" style="color: #ff3333; font-family: Tahoma; text-align: right;">
                                                    <asp:ImageButton ID="ImgClose"
                                                        runat="server" Height="20px" ImageUrl="~/images/Close.jpg" OnClick="ImgClose_Click" />
                                                </td>
                                            </tr>

                                            <tr style="height: 10px">
                                                <td colspan="3" style="font-family: Tahoma" class="style3">
                                                    <strong>Add New Mapped Car </strong>
                                                </td>
                                            </tr>

                                            <tr style="height: 10px">
                                                <td colspan="3" style="color: #ff3333; font-family: Tahoma">
                                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                                </td>
                                            </tr>


                                            <tr runat="server" id="trTDSBookingiD">
                                                <td align="left"></td>
                                                <td></td>
                                                <td align="left">
                                                    <asp:Label ID="lblSerialNo" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRegnNo" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCarID" runat="server"></asp:Label>
                                                </td>
                                            </tr>

                                            <tr id="trCreditTo" runat="server" visible="true">
                                                <td align="left">VendorType</td>
                                                <td><strong>:</strong></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlVendorType" runat="server" Visible="true">
                                                        <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                                                        <asp:ListItem Text="Vendor" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Own" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfv_ddlVendorType" runat="server" ControlToValidate="ddlVendorType"
                                                        ErrorMessage="Please select VendorType !" ValidationGroup="save"
                                                        Enabled="true" InitialValue="-1">
                                                    </asp:RequiredFieldValidator>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="trSeviceType" runat="server" visible="true">
                                                <td align="left">ServiceType</td>
                                                <td><strong>:</strong></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlServiceType" runat="server" Visible="true">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfv_ServiceTypeValidation" runat="server" ControlToValidate="ddlServiceType"
                                                        ErrorMessage="Select service Type !" ValidationGroup="save"
                                                        Enabled="true" InitialValue="0">
                                                    </asp:RequiredFieldValidator>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="tr4" runat="server" visible="true">
                                                <td align="left">RegNo</td>
                                                <td>:</td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRegNo" runat="server"
                                                        TextMode="SingleLine" Width="100px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv_txtRegNo" runat="server" ControlToValidate="txtRegNo"
                                                        ErrorMessage="Please enter RegNo !" ValidationGroup="save" Enabled="true"
                                                        SetFocusOnError="True">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>

                                            <tr id="trRemarks" runat="server" visible="true">
                                                <td align="left">Remarks</td>
                                                <td>:</td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="275px" Height="50px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvtxtRemarks" runat="server"
                                                        ControlToValidate="txtRemarks" Enabled="true"
                                                        ErrorMessage="Please enter remarks !" SetFocusOnError="True"
                                                        ValidationGroup="save">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td align="center" colspan="3">&nbsp;</td>
                                            </tr>


                                            <tr>
                                                <td align="center" colspan="3">
                                                    <asp:Button ID="btnSave" runat="server" Text="Add New Mapped Car"
                                                        ValidationGroup="save" OnClick="btnSave_Click" />
                                                    &nbsp;
                                        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            Text="Cancel" />
                                                </td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <%--<asp:PostBackTrigger ControlID="btnSave"></asp:PostBackTrigger>--%>
                                    <%--<asp:PostBackTrigger ControlID = "UploadToll" ></asp:PostBackTrigger>
                            <asp:AsyncPostBackTrigger ControlID="txtAmount" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>--%>
                                    <%--<asp:AsyncPostBackTrigger ControlID = "btnAsyncUpload" EventName = "Click" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <asp:Label ID="lblMsg" runat="server"></asp:Label></td>





                    <td colspan="1" style="height: 1px"></td>
                </tr>
                <tr>
                    <td style="height: 10px">
                        <asp:Panel ID="pnlPopUpEdit" runat="server" Width="45%" BorderWidth="2px" BorderStyle="Solid" BorderColor="Black" Visible="false">
                            <asp:UpdatePanel ID="UpdatePanel1Edit" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <table style="border-right: black 1px solid; border-top: black 1px solid; font-size: 9pt; border-left: black 1px solid; width: 100%; border-bottom: black 1px solid; font-family: Tahoma; background-color: #EBEBEB; text-align: left">
                                        <tbody>
                                            <tr style="height: 10px">
                                                <td colspan="3" style="color: #ff3333; font-family: Tahoma; text-align: right;">
                                                    <asp:ImageButton ID="ImgEditClose"
                                                        runat="server" Height="20px" ImageUrl="~/images/Close.jpg" OnClick="ImgEditClose_Click" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="3" style="font-family: Tahoma; text-align: center;" class="style2">
                                                    <strong>Edit Mapped Car </strong>
                                                </td>
                                            </tr>



                                            <tr id="tr1" runat="server">
                                                <td align="left">RegNo</td>
                                                <td>:</td>
                                                <td align="left">
                                                    <asp:Label ID="lblMessageEdit" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCarIDEdit" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblStatusEdit" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblRegNoEdit" runat="server"></asp:Label>
                                                    <asp:Label ID="lblSerialNoEdit" runat="server" Visible="false"></asp:Label>

                                                </td>
                                            </tr>


                                            <tr runat="server" id="tr2" visible="true">
                                                <td align="left">Active/InActive</td>

                                                <td><strong>:</strong></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStatusEdit" runat="server" Visible="true">
                                                        <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                                                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfv_ddlStatusEdit" runat="server"
                                                        ControlToValidate="ddlStatusEdit" Enabled="true"
                                                        ErrorMessage="Please select Status !" InitialValue="-1" ValidationGroup="save">
                                                    </asp:RequiredFieldValidator>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="tr3" runat="server" visible="true">
                                                <td align="left">Remarks</td>
                                                <td>:</td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRemarksEdit" runat="server" MaxLength="10"
                                                        TextMode="MultiLine" Width="275px" Height="50px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfv_txtRemarksEdit" runat="server" ControlToValidate="txtRemarksEdit"
                                                        ErrorMessage="Please enter remarks !" ValidationGroup="save" Enabled="true"
                                                        SetFocusOnError="True">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center" colspan="3">&nbsp;</td>
                                            </tr>


                                            <tr>
                                                <td align="center" colspan="3">
                                                    <asp:Button ID="BtnSaveEdit" runat="server" Text="Modify Mapped Car"
                                                        ValidationGroup="save" OnClick="BtnSaveEdit_Click" />
                                                    &nbsp;
                                        <asp:Button ID="btnCancelEdit" runat="server"
                                            Text="Cancel" OnClick="btnCancelEdit_Click" />
                                                </td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="BtnSaveEdit"></asp:PostBackTrigger>
                                    <%--<asp:PostBackTrigger ControlID = "UploadToll" ></asp:PostBackTrigger>
                            <asp:AsyncPostBackTrigger ControlID="txtAmount" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>--%>
                                    <%--<asp:AsyncPostBackTrigger ControlID = "btnAsyncUpload" EventName = "Click" />--%>
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                    <td colspan="1" style="height: 1px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 100px">
                        <asp:Label Style="display: none" ID="lblPopupTargetID" runat="server"></asp:Label>
                        <cc1:ModalPopupExtender
                            ID="mdPopUp"
                            runat="server"
                            TargetControlID="lblPopupTargetID"
                            PopupControlID="pnlpopup"
                            BackgroundCssClass="modalBackground"
                            BehaviorID="mdlPopup"
                            CancelControlID="btnCancel"
                            DropShadow="false"
                            RepositionMode="RepositionOnWindowResizeAndScroll">
                        </cc1:ModalPopupExtender>
                    </td>
                    <td colspan="1" style="height: 1px"></td>
                </tr>



                <tr>
                    <td style="height: 100px">
                        <asp:Label Style="display: none" ID="lblEditPopUpTagetID" runat="server"></asp:Label>
                        <cc1:ModalPopupExtender
                            ID="mdPopUpEdit"
                            runat="server"
                            TargetControlID="lblEditPopUpTagetID"
                            PopupControlID="pnlPopUpEdit"
                            BackgroundCssClass="modalBackground"
                            BehaviorID="mdPopUpEdit"
                            CancelControlID="btnCancelEdit"
                            DropShadow="false"
                            RepositionMode="RepositionOnWindowResizeAndScroll">
                        </cc1:ModalPopupExtender>
                    </td>
                    <td colspan="1" style="height: 1px"></td>
                </tr>

                <%--<tr><td>
        <image id="bkshrma" src="C:\CorInt\Rentmeter_New\receipts\8018806_Toll_1_30.png" alt="Bksharma"></image>
        </td></tr> 
       <tr><td>
            <iframe id="bkshrma" src="C:\CorInt\Rentmeter_New\receipts\8018806_Toll_1_30.png" alt="Bksharma"></iframe>
           
        </td></tr>--%>
            </table>
        </center>
    </fieldset>


</asp:Content>


