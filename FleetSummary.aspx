<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FleetSummary.aspx.cs" Inherits="FleetSummary"
    MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="feelTrackerSummary" ContentPlaceHolderID="cphPage" runat="server">

    <script type="text/javascript">
     $(document).ready(function()
     {
         $("#<%=bntGet.ClientID%>").click(function()
         {
             if(document.getElementById ("<%=ddlUnit.ClientID%>").selectedIndex==0)
             {              
                alert("Select Unit.")
                return false;
             }
             else if(parseInt((document.getElementById ('<%=ddFromMonth.ClientID%>').value)) > parseInt((document.getElementById ('<%=ddToMonth.ClientID%>').value)))
             {
                alert("From date can't be greater than to date.")
                return false;
             }
         });
     });
//     function checkValidation()
//     {
//         if((document.getElementById ('<%=ddFromMonth.ClientID%>').value)>(document.getElementById ('<%=ddToMonth.ClientID%>').value))
//         {
//            alert("From date can't be greater than to date.")
//         }
//      }
    </script>

    <div style="width: 60%; margin-left: 30%">
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="4" align="center">
                    <b>Fleet Tracker Summary</b></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                    <label id="lblUnit" for="ddlUnit">
                        Select Unit</label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlUnit" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<label id="lblFromMonth" for="ddFromMonth">
                        From Month</label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddFromMonth" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<label id="lblToMonth" for="ddToMonth">
                        To Month</label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddToMonth" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <center>
        <div>
            <b>
                <asp:Label ID="lblHeader" runat="server" Visible="false"></asp:Label></b></div>
        <div>
            <br />
            <asp:GridView ID="grvSummary" runat="server" AutoGenerateColumns="false" >
                <Columns>
                    <asp:TemplateField HeaderText="S.No.">
                        <ItemTemplate>
                            <asp:Label ID="lblSNo" runat="server" Text="<%#Container.DisplayIndex + 1%>">"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Month Name" DataField="Month_Name" />
                    <asp:BoundField HeaderText="Total Car Days" DataField="TotalCarDay" />
                    <asp:BoundField HeaderText="Total Off Road Days" DataField="TotalOffRoad" />
                    <asp:BoundField HeaderText="Total On Road Days " DataField="TotalOnRoad" />
                    <asp:TemplateField HeaderText="% On road Days">
                        <ItemTemplate>
                            <asp:Label ID="lblPer" runat="server" Text='<%#Eval("Per_OnRoad")%>'></asp:Label>%
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Accidental" DataField="Accidental" />
                    <asp:BoundField HeaderText="Servicing" DataField="Servicing" />
                    <asp:BoundField HeaderText="Driver Issues" DataField="DriverIssues" />
                    <asp:BoundField HeaderText="Off road days - Other Reasons" DataField="OtherReason" />
                    <asp:BoundField HeaderText="Consecutive Days - servicing" DataField="ConsecutiveServicing" />
                </Columns>
            </asp:GridView>
        </div>
    </center>
    <asp:Label ID="lblMessage" runat="Server" Visible="false"></asp:Label>
</asp:Content>
