using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;
public partial class CorDriveReport_LoginSummary : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(5000);
        DataSet dsLoginSummary = new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
                return;
            }
            else
            {
                //dsLoginSummary = objCordrive.GetLoginSummaryReport(Convert.ToDateTime("2015-10-19"));
                dsLoginSummary = objCordrive.GetLoginSummaryReport(Convert.ToDateTime(txtFromDate.Text));
                if (dsLoginSummary.Tables[0].Rows.Count > 0)
                {
                    grdLoginSummary.DataSource = dsLoginSummary.Tables[0];
                    grdLoginSummary.DataBind();

                    grdActivity.DataSource = dsLoginSummary.Tables[1];
                    grdActivity.DataBind();
                }
                else
                {
                    grdLoginSummary.DataSource = null;
                    grdLoginSummary.DataBind();

                    grdActivity.DataSource = null;
                    grdActivity.DataBind();

                    lblMessate.Text = "Record not available.";
                    lblMessate.Visible = true;
                    lblMessate.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }
}
