﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CurrentLocationofVehicles.aspx.cs" Inherits="CorDriveReport_CurrentLocationofVehicles" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#<%=bntGet.ClientID%>").click(function () {
                if ($("#<%=ddlCity.ClientID%>").get(0).selectedIndex == 0) {
                    alert("Select location");
                    return false;
                }
            });
        });
    </script>
    <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 40%">
                <tr>
                    <td colspan="3" align="center">
                        <b>Location Wise</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Location</b>&nbsp;
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCity" runat="server">
                        </asp:DropDownList>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        <asp:Button ID="bntGet" runat="server" Text="Get It" OnClick="bntGet_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div style="text-align: center">
            <iframe id="ifViewMap" runat="server" visible="false" height="500px" width="1000px"
                s></iframe>
        </div>
    </center>
</asp:Content>
