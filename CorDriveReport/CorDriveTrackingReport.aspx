﻿<%@ Page Title="COR Drive Tracking Report" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="CorDriveTrackingReport.aspx.cs" Inherits="CorDriveReport_CorDriveTrackingReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>

    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>

    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>

    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=gvTrackingReport.ClientID%> a[id*='lnkView']").click(function () {
                //debugger;
                //alert("Hi All ");
                var caption = "Map View";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("lnkView", "hd_CarId")
                var hdImageID = $("#" + hdImageID).val();
                //alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");
                var url = "http://insta.carzonrent.com/CorMeterGoogleAPI/MapWithCarNO.aspx?RegnNo=" + hdImageID
                //alert(url);
                //return GB_showFullScreen(caption, url)
                return GB_showCenter(caption, url, 400, 800)
            });
        });
    </script>

    <center>
        <div align="center">
            <b>COR Drive Tracking Report</b>
        </div>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblBranch" runat="server">Branch</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlBranch" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Label ID="lblFromDate" runat="server">Pick-up date From</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Label ID="lblToDate" runat="server">Pick-up date To</asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <div>
            <asp:GridView ID="gvTrackingReport" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                ShowFooter="false" PageSize="100" OnPageIndexChanging="gvTrackingReport_PageIndexChanging"
                OnRowDataBound="gvTrackingReport_RowDataBound" GridLines="Both">
                <Columns>
                    <asp:TemplateField HeaderText="Booking ID">
                        <ItemTemplate>
                            <asp:Label ID="lbl_BookingId" runat="server" Text='<% #Eval("BookingID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Company Name">
                        <ItemTemplate>
                            <asp:Label ID="lbl_CompanyName" runat="server" Text='<% #Eval("ClientCoName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Guest Name">
                        <ItemTemplate>
                            <asp:Label ID="lbl_GuestName" runat="server" Text='<% #Eval("GuestName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Guest Contact">
                        <ItemTemplate>
                            <asp:Label ID="lbl_GuestMobileNo" runat="server" Text='<% #Eval("GuestMobileNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Car Number">
                        <ItemTemplate>
                            <asp:Label ID="lbl_CarRegNo" runat="server" Text='<% #Eval("CarRegNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Driver Name">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DriverName" runat="server" Text='<% #Eval("DriverName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Driver Contact">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DriverContract" runat="server" Text='<% #Eval("DriverContract") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City">
                        <ItemTemplate>
                            <asp:Label ID="lbl_CityName" runat="server" Text='<% #Eval("CityName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Driver Start Date" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DriverStartDate" runat="server" Text='<% #Eval("DriverStartDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Driver Start Time">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DriverStartTime" runat="server" Text='<% #Eval("DriverStartTime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Driver Start Location">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DriverStartLocation" runat="server" Text='<% #Eval("DriverStartLocation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Pickup Date" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_ActualPickupDate" runat="server" Text='<%  #Eval("ActualPickupDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Pickup  Time">
                        <ItemTemplate>
                            <asp:Label ID="lbl_ActualPickupTime" runat="server" Text='<% #Eval("ActualPickupTime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Pickup  Location">
                        <ItemTemplate>
                            <asp:Label ID="lbl_ActualPickupLocation" runat="server" Text='<% #Eval("ActualPickupLocation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Pickup Date" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_RequestedPickupdate" runat="server" Text='<%  #Eval("RequestedPickupdate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Pickup Time" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_RequestedPickuptime" runat="server" Text='<%  #Eval("RequestedPickupTime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Pickup  Location">
                        <ItemTemplate>
                            <asp:Label ID="lbl_RequestedPickLocation" runat="server" Text='<% #Eval("RequestedPickLocation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Passanger End Date" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_PassengerEndtDate" runat="server" Text='<% #Eval("PassengerEndtDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Passenger EndT ime">
                        <ItemTemplate>
                            <asp:Label ID="lbl_PassengerEndTime" runat="server" Text='<% #Eval("PassengerEndTime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Passenger End Location">
                        <ItemTemplate>
                            <asp:Label ID="lbl_PassengerDropoffLocation" runat="server" Text='<% #Eval("PassengerDropoffLocation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DS Status">
                        <ItemTemplate>
                            <asp:Label ID="lbl_dutyslipStatus" runat="server" Text='<% #Eval("dutyslipStatus") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Feedback Rating">
                        <ItemTemplate>
                            <asp:Label ID="lbl_RatingPoint" runat="server" Text='<% #Eval("RatingPoint") %>'></asp:Label>
                            <asp:HiddenField ID="hdCarid" runat="server" Value='<% #Eval("CarID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Next Duty Booking Id">
                        <ItemTemplate>
                            <asp:Label ID="lbl_NextBookingId" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Next Pick Up Address">
                        <ItemTemplate>
                            <asp:Label ID="lbl_NextPickupAddress" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Next Pick-up Date" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_NextPickupDate" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Next Pick-up Time">
                        <ItemTemplate>
                            <asp:Label ID="lbl_NextPickupTime" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Location">
                        <ItemTemplate>
                            <%--<asp:LinkButton ID="lnkView" runat="server" Text="View"></asp:LinkButton>--%>
                            <asp:Label ID="lbl_CarLocationName" runat="server" Text='<% #Eval("CarLocationName") %>'></asp:Label>
                            <asp:HiddenField ID="hd_CarId" runat="server" Value='<% #Eval("CarRegNo") %>' />
                            <asp:HiddenField ID="hd_lat" runat="server" Value='<% #Eval("lat") %>' />
                            <asp:HiddenField ID="hd_Lon" runat="server" Value='<% #Eval("Lon") %>' />
                            <asp:HiddenField ID="hd_DutyStatus" runat="server" Value='<% #Eval("DutyStatus") %>' />
                            <asp:HiddenField ID="hd_Dlat" runat="server" Value='<% #Eval("DriverStartLat") %>' />
                            <asp:HiddenField ID="hd_DLon" runat="server" Value='<% #Eval("DriverStartLon") %>' />
                            <asp:HiddenField ID="hd_Plat" runat="server" Value='<% #Eval("PassengerEndLat") %>' />
                            <asp:HiddenField ID="hd_PLon" runat="server" Value='<% #Eval("PassengerEndLon") %>' />
                            <asp:HiddenField ID="hd_driverStartLocation" runat="server" Value='<% #Eval("DriverStartLocation") %>' />
                            <asp:HiddenField ID="hd_passengerstartLocation" runat="server" Value='<% #Eval("PassengerstartLocation") %>' />
                            <asp:HiddenField ID="hd_passengerDropoffLocation" runat="server" Value='<% #Eval("PassengerDropoffLocation") %>' />
                            <asp:HiddenField ID="hd_driverendLocation" runat="server" Value='<% #Eval("DriverEndLocation") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CorDriveVersion">
                        <ItemTemplate>
                            <asp:Label ID="lbl_CorDriveVersion" runat="server" Text='<% #Eval("CorDriveVersion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            </asp:GridView>
        </div>
    </center>
</asp:Content>
