<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DutyDespatchDetail.aspx.cs" Inherits="DutyDespatchDetail" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" src="../JScripts/Datefunc.js"></script>

    <script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
    {
        alert("Please Select the From Date");
        document.getElementById('<%=txtFrom.ClientID %>').focus();
        return false;
    }

    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}
    </script>

    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Duty Despatch Detail</u></strong>
                </td>
            </tr>
            <tr>
                <%--<td nowrap ="nowrap" align ="right"><asp:CheckBox ID="chkAll" runat ="server"/>  All Car</td>--%>
                <td align="center" colspan="8">
                    From Date
                    <%--</td>
                <td align="left" style="width: 11px">--%>
                    &nbsp;
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                    &nbsp;
                    <%--</td>
                <td align="left" style="width: 28px">--%>
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                    &nbsp;&nbsp;
                    <%--<td align="right" style="width: 100Px" colspan="6">--%>
                    To Date &nbsp;
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                    &nbsp;
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <%--<td align="right" style="width: 105px"> City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>--%>
            </tr>
            <tr>
                <td colspan="7">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="7">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="7">
                    <asp:GridView runat="server" ID="GridView1" PageSize="20" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="false" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
                        >
                        <Columns>
                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closed Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToClosedDuties" Text='<% #Bind("BillToClosedDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Open Duties">
                                <ItemTemplate>
                                    <asp:Label ID="lblBillToOpenDuties" Text='<% #Bind("BillToOpenDuties") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No Show Charged">
                                <ItemTemplate>
                                    <asp:Label ID="lblNoShowClosed" Text='<% #Bind("NoShowClosed") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No Show Not Charged">
                                <ItemTemplate>
                                    <asp:Label ID="lblNoShowClosedNotCharged" Text='<% #Bind("NoShowClosedNotCharged") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Less Than 48 Hours">
                                <ItemTemplate>
                                    <asp:Label ID="lblLessThan48" Text='<% #Bind("LessThan48") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Greater Than 48 Hours">
                                <ItemTemplate>
                                    <asp:Label ID="lblHours48To96" Text='<% #Bind("Hours48To96") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Greater Than 96 Hours">
                                <ItemTemplate>
                                    <asp:Label ID="lblGreaterThan96" Text='<% #Bind("GreaterThan96") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>