using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

//public partial class ClientWiseSubmissionSummary : clsPageAuthorization
public partial class ClientWiseSubmissionSummary : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    string FromDate, Todate, CityName;
    int CityID;

    protected void Page_Load(object sender, EventArgs e)
    {
        Todate = Request.QueryString["Todate"];
        CityName = Request.QueryString["CityName"];
        CityID = Convert.ToInt32(Request.QueryString["CityID"]);
        FromDate = Request.QueryString["FromDate"];

        if (!Page.IsPostBack)
        {
            FillGrid();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetClientWiseSubmissionSummary(FromDate, Todate, CityID, CityName);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=ClientWiseSubmissionSummary.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetClientWiseSubmissionSummary(FromDate, Todate, CityID, CityName);        

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 1, 2, 4, 5 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    //Response.Write("<tr>");
                    //Response.Write("<td align='left'><b></b></td>");
                    //Response.Write("<td align='center' colspan='4'><b>Direct</b></td>");
                    //Response.Write("<td align='center' colspan='4'><b>BTC older than 48 hours</b></td>");
                    //Response.Write("<td align='center' colspan='2'><b>BTC older than 96 hours</b></td>");
                    //Response.Write("<td align='center' colspan='3'><b>Branch</b></td>");
                    //Response.Write("<td align='center' colspan='2'><b>SSC</b></td>");
                    //Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ClientName")
                        {
                            Response.Write("<td align='left'><b>Client Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SubmissionType")
                        {
                            Response.Write("<td align='left'><b>Submission Type</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Submited")
                        {
                            Response.Write("<td align='left'><b>Submitted</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSubmited")
                        {
                            Response.Write("<td align='left'><b>Not Submitted</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    GridView HeaderGrid = (GridView)sender;
        //    GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
        //    TableCell HeaderCell = new TableCell();

        //    HeaderCell.ColumnSpan = 1;
        //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        //    HeaderCell.Text = "";
        //    HeaderGridRow.Cells.Add(HeaderCell);

        //    HeaderCell = new TableCell();
        //    HeaderCell.ColumnSpan = 4;
        //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        //    HeaderCell.Text = "Direct";
        //    HeaderGridRow.Cells.Add(HeaderCell);

        //    HeaderCell = new TableCell();
        //    HeaderCell.Text = "BTC older than 48 hours";
        //    HeaderCell.ColumnSpan = 4;
        //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        //    HeaderGridRow.Cells.Add(HeaderCell);

        //    HeaderCell = new TableCell();
        //    HeaderCell.Text = "BTC older than 96 hours";
        //    HeaderCell.ColumnSpan = 2;
        //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        //    HeaderGridRow.Cells.Add(HeaderCell);

        //    HeaderCell = new TableCell();
        //    HeaderCell.Text = "Branch";
        //    HeaderCell.ColumnSpan = 3;
        //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        //    HeaderGridRow.Cells.Add(HeaderCell);

        //    HeaderCell = new TableCell();
        //    HeaderCell.Text = "SSC";
        //    HeaderCell.ColumnSpan = 2;
        //    HeaderCell.HorizontalAlign = HorizontalAlign.Center;
        //    HeaderGridRow.Cells.Add(HeaderCell);

        //    GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);

        //}
    }
}