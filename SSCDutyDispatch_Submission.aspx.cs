using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class SSCDutyDispatch_Submission : clsPageAuthorization
//public partial class SSCDutyDispatch_Submission : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=BranchWiseDutiesSummary.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetSSCDutyDispatch_SubmissionSummary(FromDate, Todate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 0, 2, 4, 10, 5, 6, 7, 3, 8, 9, 1 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    Response.Write("<td align='left'><b></b></td>");
                    Response.Write("<td align='center' colspan='6'><b>BTC DS (Submitted through SSC)</b></td>");
                    Response.Write("<td align='center' colspan='3'><b>BTC DS (Not Submitted through SSC)</b></td>");
                    Response.Write("<td align='left'><b></b></td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Location")
                        {
                            Response.Write("<td align='left'><b>City Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SenttoSSC")
                        {
                            Response.Write("<td align='left'><b>Closed Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "RecdbySSC")
                        {
                            Response.Write("<td align='left'><b>Recd. By SSC</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "InTransit")
                        {
                            Response.Write("<td align='left'><b>In Transit</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotRecdbySSC")
                        {
                            Response.Write("<td align='left'><b>Not Recd. By SSC</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CCSBatchCreated")
                        {
                            Response.Write("<td align='left'><b>CCS Batch Created</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CCSBatchNotCreated")
                        {
                            Response.Write("<td align='left'><b>CCS Batch not Created</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotsenttoSSC")
                        {
                            Response.Write("<td align='left'><b>Closed Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSentToSSC_CCSBatchCreated")
                        {
                            Response.Write("<td align='left'><b>CCS Batch Created</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSentToSSC_CCSBatchNotCreated")
                        {
                            Response.Write("<td align='left'><b>CCS Batch not Created</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDS")
                        {
                            Response.Write("<td align='left'><b>Grand Total</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetSSCDutyDispatch_SubmissionSummary(FromDate, Todate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();

            HeaderCell.ColumnSpan = 1;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 6;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "BTC DS (Submitted through SSC)";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 3;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "BTC DS (Not Submitted Through SSC)";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 1;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
    }
}