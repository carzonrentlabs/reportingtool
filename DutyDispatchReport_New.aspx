<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DutyDispatchReport_New.aspx.cs"
    Inherits="DutyDispatchReport_New" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>
    <script language="JavaScript" type="text/javascript" src="../JScripts/Datefunc.js"></script>
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Manual Duty slip Report</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 287px">
                    From Date
                </td>
                <td align="left">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 57px">
                    To Date
                </td>
                <td align="left" style="width: 6px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 20px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 105px">
                    City Name:
                </td>
                <td style="width: 183px">
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    <asp:GridView runat="server" ID="grvDutyDispatch" PageSize="50" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow"
                        OnRowCreated="grvMergeHeader_RowCreated" BorderColor="Tan" BorderWidth="1px"
                        CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="grvDutyDispatch_PageIndexChanging"
                        DataKeyNames="cityId,cityName" onrowcommand="grvDutyDispatch_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Branch">
                                <ItemTemplate>
                                    <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="< 24 Hrs">
                                <ItemTemplate>
                                    <asp:Label ID="lbllt24" Text='<% #Bind("[24Hrs]") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="< 48 Hrs">
                                <ItemTemplate>
                                    <asp:Label ID="lbllt48" Text='<% #Bind("[48Hrs]") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="< 96 Hrs">
                                <ItemTemplate>
                                    <asp:Label ID="lbl1t96" Text='<% #Bind("[96Hrs]") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="> 96 Hrs">
                                <ItemTemplate>
                                    <asp:Label ID="lblgt96" Text='<% #Bind("[>96Hrs]") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" Text='<% #Bind("[Total]") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText ="Details">
                            <HeaderTemplate >
                               <asp:Button ID="bntGetAll" runat ="server" Text ="Export to Excel All" 
                                onclick="bntGetAll_Click" />
                            </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="bntExport" runat="server" Text="Export to Excel" CommandName="Export"
                                        CommandArgument="<%#((GridViewRow)Container).RowIndex %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
            
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
