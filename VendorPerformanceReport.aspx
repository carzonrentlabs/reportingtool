﻿<%@ Page Title="Vendor Performance Report" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="VendorPerformanceReport.aspx.cs" Inherits="VendorPerformanceReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" type="text/javascript" src="../JScripts/Datefunc.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="JQuery/gridviewScroll.min.js"></script> 
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=ddlMonth.ClientID%>').value == "0") {
                alert("Please select Month.");
                document.getElementById('<%=ddlMonth.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlYear.ClientID%>').value == "--Select--") {
                alert("Please select Year.");
                document.getElementById('<%=ddlYear.ClientID%>').focus();
                return false;
            }
        }
    </script>
<script type="text/javascript">
    $(document).ready(function () {
        gridviewScroll();
    });

    function gridviewScroll() {
        $('#<%=gvVendorPerformance.ClientID%>').gridviewScroll({
            width: 1150,
            height: 430
        });
    } 
</script>
    <table width="100%" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="6">
                    <strong><u>Vendor Performance Report</u></strong>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="6">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Center" colspan="6">
                    Month : 
                    <asp:DropDownList ID="ddlMonth" runat="server" Width="74px">
                    </asp:DropDownList>
                    &nbsp;&nbsp; Year : 
                    <asp:DropDownList ID="ddlYear" runat="server" Width="74px">
                    </asp:DropDownList>
                    &nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btnSubmit" runat="server" Text="Get It" OnClick="btnSubmit_Click" />
                    &nbsp; &nbsp;
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                </td>
            </tr>
             <tr>
                <td colspan="6">
                 				                     
		

                      <asp:GridView ID="gvVendorPerformance" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvVendorPerformance_RowDataBound" > 
                                 <Columns> 
    								    <asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                        <asp:BoundField HeaderText="CityName" DataField="CityName" /> 
                                        <%--<asp:BoundField HeaderText="CityID" DataField="CityID" /> --%>
                                        <asp:BoundField HeaderText="NoOfCarTarget" DataField="NoOfCarTarget" /> 
                                        <asp:BoundField HeaderText="NoOfCarAchivedMonth1" DataField="NoOfCarAchivedMonth1" /> 
                                        <asp:BoundField HeaderText="NoOfCarAchivedMonth2" DataField="NoOfCarAchivedMonth2" /> 
                                        
                                        <asp:BoundField HeaderText="NoOfCarShortfall" DataField="NoOfCarShortfall" /> 
                                        <asp:BoundField HeaderText="AllocationTarget" DataField="AllocationTarget" /> 
                                         <asp:BoundField HeaderText="AllocationAchivedMonth1" DataField="AllocationAchivedMonth1" /> 
                                        <asp:BoundField HeaderText="AllocationAchivedMonth2" DataField="AllocationAchivedMonth2" /> 
                                        <asp:BoundField HeaderText="AllocationShortfall" DataField="AllocationShortfall" /> 
                                        
                                        
                                        <asp:BoundField HeaderText="VendorTarget" DataField="VendorTarget" />
                                         <asp:BoundField HeaderText="VendorAchivedMonth1" DataField="VendorAchivedMonth1" />  
                                        <asp:BoundField HeaderText="VendorAchivedMonth2" DataField="VendorAchivedMonth2" />
                                       
                                        <asp:BoundField HeaderText="VendorShortfall" DataField="VendorShortfall" /> 
                                        <asp:BoundField HeaderText="QualityTarget" DataField="QualityTarget" /> 
                                         <asp:BoundField HeaderText="QualityAchivedMonth1" DataField="QualityAchivedMonth1" /> 
                                        <asp:BoundField HeaderText="QualityAchivedMonth2" DataField="QualityAchivedMonth2" /> 
                                       
                                        <asp:BoundField HeaderText="QualityShortfall" DataField="QualityShortfall" /> 
                                                                                                   
                                       
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                  
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
