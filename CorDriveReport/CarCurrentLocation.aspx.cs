﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class CorDriveReport_CarCurrentLocation : System.Web.UI.Page
{
    RentMeterService.wsRentmeter rentMeterAPI = new RentMeterService.wsRentmeter();
    public string strCarRegNo = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        ifViewMap.Visible = false;
        if (!string.IsNullOrEmpty(txtRegNo.Text))
        {
            strCarRegNo = txtRegNo.Text;
            if (chkHistorical.Checked == true)
            {
                lblCurrentLocation.Visible = false;
                string url = "http://insta.carzonrent.com/CorMeterGoogleAPI/MapWithCarNo_Historical.aspx?RegnNo=" + Convert.ToString(strCarRegNo);
                ifViewMap.Visible = true;
                ifViewMap.Attributes.Add("src", url);
            }
            else
            {
                lblCurrentLocation.Visible = true;
                string location = rentMeterAPI.GetCurrentLocation(txtRegNo.Text.ToString());
                lblCurrentLocation.Text = Convert.ToString(location);
                string url = "http://insta.carzonrent.com/CorMeterGoogleAPI/MapWithCarNO.aspx?RegnNo=" + Convert.ToString(strCarRegNo);
                ifViewMap.Visible = true;
                ifViewMap.Attributes.Add("src", url);
            }


        }
        else
        {
            lblMessage.Visible = true;
            ifViewMap.Visible = false;
            lblMessage.Text = "Enter Registration Number.";
            lblMessage.ForeColor = Color.Red;
        }


    }
    protected void bntBookingId_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        lblCurrentLocation.Visible = false;
        ifViewMap.Visible = false;
        if (!string.IsNullOrEmpty(txtBookingId.Text.ToString()))
        {
            lblMessage.Visible = false;
            lblCurrentLocation.Visible = false;
            string url = "http://insta.carzonrent.com/cormetergoogleAPI/MapWithSatelliteView.aspx?DutySlipNo=" + Convert.ToString(txtBookingId.Text);
            ifViewMap.Visible = true;
            System.Threading.Thread.Sleep(10000);
            ifViewMap.Attributes.Add("src", url);
        }
        else
        {
            lblMessage.Visible = true;
            lblCurrentLocation.Visible = false;
            ifViewMap.Visible = false;
            lblMessage.Text = "Enter booking Id.";
            lblMessage.ForeColor = Color.Red;
        }


    }
}