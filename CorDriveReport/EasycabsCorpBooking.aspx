<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EasycabsCorpBooking.aspx.cs" Inherits="CorDriveReport_EasycabsCorpBooking" Title="Easycabs Corp Booking Report" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 
   <%-- <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>--%>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       $(document).ready(function () 
       {
           
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
           
            $("#<%=btnExport.ClientID%>").click(function () {
             
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
            gridviewScroll();
       });
         
     function gridviewScroll() {
        $('#<%=gvEasycabsCorpBooking.ClientID%>').gridviewScroll({
            width: 1200,
            height: 450
        });
    } 

       
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width:1200px">
            <tr>
                <td colspan="8" align="center" bgcolor="#FF6600">
                    <strong>Easycabs Corp Booking Report</strong>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b> From Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                 
                </td>
                 <td style="white-space: nowrap;" align="right">
                 <b> To Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                 
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </td>
                
                <td style="white-space: nowrap;" align="right">
                 <b> City :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                 
                   <asp:DropDownList ID="ddlCity" runat ="server"> 
                   </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
             <tr>
                <td colspan="8">
                 
                      <asp:GridView ID="gvEasycabsCorpBooking" runat="server" AutoGenerateColumns="false" Width="100%"> 
                                 <Columns> 
                                 		<asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                        <asp:BoundField HeaderText="InstaBookingId" DataField="InstaBookingId" /> 
                                        <asp:BoundField HeaderText="EasycabsBookingId" DataField="EasycabsBookingId" /> 
                                        <asp:BoundField HeaderText="Client Name" DataField="ClientCoName" /> 
                                        <asp:BoundField HeaderText="System" DataField="SystemDuty" /> 
                                        <asp:BoundField HeaderText="DutyType" DataField="DutyType" /> 
                                        <asp:BoundField HeaderText="CreatedBy" DataField="CreatedBy" /> 
                                        <asp:BoundField HeaderText="BookingStatus" DataField="BookingStatus" /> 
                                        <asp:BoundField HeaderText="PickUpDateTime" DataField="PickUpDateTime" /> 
                                        <asp:BoundField HeaderText="CreateDate" DataField="CreateDate" /> 
                                        <asp:BoundField HeaderText="PickupLocation" DataField="PickupGeolocation" /> 
                                        <asp:BoundField HeaderText="DropoffGeolocation" DataField="DropoffGeolocation" /> 
                                        <asp:BoundField HeaderText="Remarks" DataField="Remarks" /> 
                                        <asp:BoundField HeaderText="GuestDetails" DataField="GuestDetails" />
                                        <asp:BoundField HeaderText="Guest1" DataField="Guest1" />
                                        <asp:BoundField HeaderText="Guest2" DataField="Guest2" />
                                        <asp:BoundField HeaderText="Guest3" DataField="Guest3" />
                                        <asp:BoundField HeaderText="Guest4" DataField="Guest4" />
                                        <asp:BoundField HeaderText="CarID" DataField="CarID" />
                                        <asp:BoundField HeaderText="CarNo" DataField="CarNo" />
                                        <asp:BoundField HeaderText="ChauffeurName" DataField="ChauffeurName" /> 
                                        <asp:BoundField HeaderText="ChauffeurContact" DataField="ChauffeurContact" /> 
                                        <asp:BoundField HeaderText="DutySlipStatus" DataField="DutySlipStatus" /> 
                                        
                                       
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                   
                </td>
            </tr>
           
        </table>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>

