﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
public partial class CorDrive_LatLonEntry : System.Web.UI.Page
{
    private CorDriveDAL objCorDriveDAL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessate.Visible = false;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    protected void bntSubmit_Click(object sender, EventArgs e)
    {
        objCorDriveDAL = new CorDriveDAL();
        float plat = 0;
        float plon = 0;
        int status = 0;
        int number;
        try
        {
            bool result = Int32.TryParse(txtBookingId.Value.ToString(), out number);
            string bookingLocation = hdCityLocationText.Value.ToString();

            if (!string.IsNullOrEmpty(txtBookingId.Value.ToString()) && result == true)
            {

                if (string.IsNullOrEmpty(city_location_lat.Value.ToString()) || string.IsNullOrEmpty(city_location_long.Value.ToString()))
                {
                    lblMessate.Visible = true;
                    lblMessate.Text = "Guest location should not be blank";
                    lblMessate.ForeColor = Color.Red;
                }
                else
                {
                    plat = float.Parse(city_location_lat.Value.ToString());
                    plon = float.Parse(city_location_long.Value.ToString());
                    status = objCorDriveDAL.UpdatePlatPlon(Convert.ToInt32(txtBookingId.Value.ToString()), plat, plon, Convert.ToInt32(Session["UserID"]), bookingLocation);
                    if (status >= 1)
                    {
                        city_location_lat.Value = "";
                        city_location_long.Value = "";
                        BindGrid();

                    }
                    else
                    {
                        city_location_lat.Value = "";
                        city_location_long.Value = "";
                        lblMessate.Visible = true;
                        lblMessate.Text = "Get some error";
                        lblMessate.ForeColor = Color.Red;
                    }

                }
            }
            else
            {
                lblMessate.Visible = true;
                lblMessate.Text = "Booking Id should not be blank";
                lblMessate.ForeColor = Color.Red;
            }


        }
        catch (Exception Ex)
        {

            lblMessate.Visible = true;
            lblMessate.Text = Ex.Message;
            lblMessate.ForeColor = Color.Red;
        }


    }
    protected void grvLatLon_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void grvLatLon_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }

    protected void grvLatLon_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvLatLon.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.Text = Ex.Message;
            lblMessate.ForeColor = Color.Red;
        }
    }

    public void BindGrid()
    {
        objCorDriveDAL = new CorDriveDAL();
        DataSet ds = new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                lblMessate.Visible = true;
                lblMessate.Text = "Pickup from date and pickup to date should not be blank";
                lblMessate.ForeColor = Color.Red;
            }
            else
            {
                ds = objCorDriveDAL.GetBookingWithoutLatLon(DateTime.Parse(txtFromDate.Text.ToString()), DateTime.Parse(txtToDate.Text.ToString()));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grvLatLon.DataSource = ds.Tables[0];
                    grvLatLon.DataBind();
                }
                else
                {
                    grvLatLon.DataSource = ds.Tables[0];
                    grvLatLon.DataBind();
                    lblMessate.Visible = true;
                    lblMessate.Text = "Record Not Available";
                    lblMessate.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.Text = Ex.Message;
            lblMessate.ForeColor = Color.Red;
        }

    }

}