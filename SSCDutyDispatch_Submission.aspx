<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SSCDutyDispatch_Submission.aspx.cs" Inherits="SSCDutyDispatch_Submission" Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">

<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=txtFrom.ClientID %>').value == "")
    {
        alert("Please Select the From Date");
        document.getElementById('<%=txtFrom.ClientID %>').focus();
        return false;
    }
            
    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}
</script>
    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>SSC Duty Dispatch and Submission Summary</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" style="width: 287px">From Date</td>
                <td align="left">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 28px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>            
                <td align="right" style="width: 57px">To Date</td>
                <td align="left" style="width: 6px">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                </td>
                <td align="left" style="width: 20px">
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                    onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                    src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right" style="width: 105px"> City Name:</td>
                <td style="width: 183px">
                <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">&nbsp;</td>
            </tr>

        <tr>
       
        <td align="center" colspan="8">
        <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found" AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCreated="grvMergeHeader_RowCreated">
        <Columns>
        
        <asp:TemplateField HeaderText="City Name">
        <ItemTemplate>
        <asp:Label ID="lblCityName" Text='<% #Bind("Location") %>' runat="server" ></asp:Label>
        </ItemTemplate>
        <ItemStyle HorizontalAlign="Left" />
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Closed Duties">
        <ItemTemplate>
        <asp:Label ID="lblClosed" Text='<% #Bind("SenttoSSC") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Recd. By SSC">
        <ItemTemplate>
        <asp:Label ID="lblRecdBySSC" Text='<% #Bind("RecdbySSC") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="In Transit">
        <ItemTemplate>
        <asp:Label ID="lblTransit" Text='<% #Bind("InTransit") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        <asp:TemplateField HeaderText="Not Recd. By SSC">
        <ItemTemplate>
        <asp:Label ID="lblNotRecdBySSC" Text='<% #Bind("NotRecdbySSC") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="CCS Batch Created">
        <ItemTemplate>
        <asp:Label ID="lblCCSBatch" Text='<% #Bind("CCSBatchCreated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="CCS Batch not Created">
        <ItemTemplate>
        <asp:Label ID="lblCCSBatchNotCreated" Text='<% #Bind("CCSBatchNotCreated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        <asp:TemplateField HeaderText="Closed Duties">
        <ItemTemplate>
        <asp:Label ID="lblClosed1" Text='<% #Bind("NotsenttoSSC") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="CCS Batch Created">
        <ItemTemplate>
        <asp:Label ID="lblCCSBatch1" Text='<% #Bind("NotSentToSSC_CCSBatchCreated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="CCS Batch not Created">
        <ItemTemplate>
        <asp:Label ID="lblCCSBatchNotCreated1" Text='<% #Bind("NotSentToSSC_CCSBatchNotCreated") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Grand Total">
        <ItemTemplate>
        <asp:Label ID="lblGrandTotal" Text='<% #Bind("TotalDS") %>' runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>        
        
        </Columns>
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
        </asp:GridView>
        </td>
        </tr>
        </tbody>
    </table>
    
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
        top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
        scrolling="no" height="182"></iframe>
    </div>

</asp:Content>