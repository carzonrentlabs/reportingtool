﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YatraBookingMIS.aspx.cs" Inherits="CorDriveReport_YatraBookingMIS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../App_Themes/stylesheet/HertzInt.css" rel="stylesheet" />
    <link href="../DatePickerCSS/ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../Css/jquery.gritter.css" rel="stylesheet" type="text/css" />
    <link href="../Css/RepeaterStyle.css" rel="stylesheet" />
    <link href="../greybox/gb_styles.css" rel="stylesheet" />
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>,#<%=btnExport.ClientID%> ").click(function () {
                var id = this.id;
               if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                   alert("Select create from date ");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Select create to date");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    if (id == "bntGet") {
                        ShowProgress();
                    }
                }
            });
       });

        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Yatra Booking MIS</b>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b>Create From Date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                 
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <b>Create To Date</b>&nbsp;&nbsp;
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
        </table>
        <br />
        <div style="text-align: center; border: 0" class="Repeater">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdYatraMIS" runat="server" AutoGenerateColumns="false">
                            <Columns>
                            												

                                <asp:BoundField HeaderText="BookingId" DataField="BookingId" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Guest Name" DataField="GuestName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Pickup Date Time" DataField="PickupDateTime" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Booking Create Date" DataField="BookingCreateDate" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Booking Status" DataField="BookingStatus" HeaderStyle-Wrap="false" />
                                
                                <asp:BoundField HeaderText="RentalType Description" DataField="RentalTypeDescription" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Indicated Price" DataField="IndicatedPrice" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Online Payment Amount" DataField="OnlinePaymentAmount" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Payment Status" DataField="PaymentStatus" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Final Amount" DataField="finalAmount" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Tax" DataField="totalTax" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Final Amount WithTax" DataField="finalAmountWithTax" HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
             
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
    </div>
    </form>
</body>
</html>
