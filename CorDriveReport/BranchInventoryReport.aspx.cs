﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

public partial class CorDriveReport_BranchInventoryReport : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindBrachDropDownlist();
            CarCategory();

            //string script = "$(document).ready(function () { $('[id*=bntGet]').click(); });";
            // ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
        }
        //bntGet.Attributes.Add("Onclick", "return validate()");
        //bntExprot.Attributes.Add("Onclick", "return validate()");
        lblMessate.Visible = false;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(5000);
        DataSet dsReport = new DataSet();
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()))
        {
            txtFromDate.Text = "2005-01-01";
        }
        if (string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            txtToDate.Text = DateTime.Today.ToString ();
        }

        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Create from date and create to date should not be blank. ')", true);
                return;
            }
            else
            {

                dsReport = objCordrive.GetCorDriveBranchInventryReport(Convert.ToInt32(ddlBranch.SelectedValue), Convert.ToInt32(ddlCat.SelectedValue), Convert.ToInt32(ddlAttachmentPkg.SelectedValue), Convert.ToInt32(ddlSoruce.SelectedValue), Convert.ToDateTime(txtFromDate.Text.ToString()), Convert.ToDateTime(txtToDate.Text.ToString()), Convert.ToInt32(ddlSoruce.SelectedValue));
                if (dsReport.Tables[0].Rows.Count > 0)
                {
                    grdRport.DataSource = dsReport.Tables[0];
                    grdRport.DataBind();
                }
                else
                {
                    lblMessate.Visible = true;
                    lblMessate.Text = "Record not available.";
                    lblMessate.ForeColor = Color.Red;
                    grdRport.DataSource = null;
                    grdRport.DataBind();

                }

            }
        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        DataSet dsInvoice = new DataSet();
        DataTable dt = new DataTable();
        int counter = 0;
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()))
        {
            txtFromDate.Text = "2005-01-01";
        }
        if (string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            txtToDate.Text = DateTime.Today.ToString();
        }
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Create from date and create to date should not be blank. ')", true);
                return;
            }
            else
            {

                dsInvoice = objCordrive.GetCorDriveBranchInventryReport(Convert.ToInt32(ddlBranch.SelectedValue), Convert.ToInt32(ddlCat.SelectedValue), Convert.ToInt32(ddlAttachmentPkg.SelectedValue), Convert.ToInt32(ddlSoruce.SelectedValue), Convert.ToDateTime(txtFromDate.Text.ToString()), Convert.ToDateTime(txtToDate.Text.ToString()), Convert.ToInt32(ddlSoruce.SelectedValue));

                if (dsInvoice != null)
                {
                    if (dsInvoice.Tables.Count > 0)
                    {
                        dt = dsInvoice.Tables[0];
                    }
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {

                            //Response.ClearContent();
                            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                            Response.Clear();
                            Response.AppendHeader("content-disposition", "attachment;filename=BranchInvoiceReport.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.ContentType = "application/vnd.ms-excel";
                            this.EnableViewState = false;
                            Response.Write("\r\n");
                            Response.Write("<table border = '1' align = 'center'> ");
                            int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                counter = counter + 1;
                                if (i == 0)
                                {
                                    Response.Write("<tr>");
                                    Response.Write("<td align='left'><b>S.No.</b></td>");
                                    for (int j = 0; j < iColumns.Length; j++)
                                    {
                                        if (dt.Columns[iColumns[j]].Caption.ToString() == "CarModelName")
                                        {
                                            Response.Write("<td align='left'><b>Car Model</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarCatName")
                                        {
                                            Response.Write("<td align='left'><b>Car Category</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "RegnNo")
                                        {
                                            Response.Write("<td align='left'><b>Vehicle No</b></td>");
                                        }

                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverName")
                                        {
                                            Response.Write("<td align='left'><b>Driver Name</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "DriverMobileNo")
                                        {
                                            Response.Write("<td align='left'><b>Driver Mobile</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                                        {
                                            Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PanNo")
                                        {
                                            Response.Write("<td align='left'><b>Pan No</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                        {
                                            Response.Write("<td align='left'><b>City Name</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "VDPYN")
                                        {
                                            Response.Write("<td align='left'><b>VDP YN</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CurrentStatus")
                                        {
                                            Response.Write("<td align='left'><b>Status</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "VehicleCreateDate")
                                        {
                                            Response.Write("<td align='left'><b>Vehicle Created Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "VehicleModifedDate")
                                        {
                                            Response.Write("<td align='left'><b>Vehicle Modified Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorCreateDate")
                                        {
                                            Response.Write("<td align='left'><b>Vendor Created Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "AttachmentModel")
                                        {
                                            Response.Write("<td align='left'><b>Attachment Model</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "PaymentOption")
                                        {
                                            Response.Write("<td align='left'><b>Payment Option</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "RevenueSharePC")
                                        {
                                            Response.Write("<td align='left'><b>Revenue Share</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "EffectiveDate")
                                        {
                                            Response.Write("<td align='left'><b>Effective Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "AgeOfCar")
                                        {
                                            Response.Write("<td align='left'><b>Car Age from RC Date</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "Grade")
                                        {
                                            Response.Write("<td align='left'><b>Grade</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "IMEINumber")
                                        {
                                            Response.Write("<td align='left'><b>IMEI Number</b></td>");

                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "SimMobileNumber")
                                        {
                                            Response.Write("<td align='left'><b>SIM Mobile Number</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "ParkingLodation")
                                        {
                                            Response.Write("<td align='left'><b>Parking Location</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "CorDriveStatus")
                                        {
                                            Response.Write("<td align='left'><b>COR Drive Status</b></td>");
                                        }
                                        else if (dt.Columns[iColumns[j]].Caption.ToString() == "GPSStatus")
                                        {
                                            Response.Write("<td align='left'><b>GPS Status</b></td>");
                                        }
                                    }
                                    Response.Write("</tr>");
                                }
                                Response.Write("<tr>");
                                Response.Write("<td align='left'>" + counter + "</td>");
                                for (int j = 0; j < iColumns.Length; j++)
                                {
                                    Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                                }

                                Response.Write("</tr>");
                            }
                            Response.Write("</table>");

                            Response.End();
                        }
                        else
                        {

                            Response.Write("<table border = 1 align = 'center' width = '100%'>");
                            Response.Write("<td align='center'><b>No Record Found</b></td>");
                            Response.Write("</table>");
                            Response.End();

                        }
                    }
                }


            }

        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
        finally
        {

        }
    }

    #region Helping Method
    private void BindBrachDropDownlist()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "Cityname";
                    ddlBranch.DataValueField = "CityId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }


    }
    private void CarCategory()
    {
        objCordrive = new CorDrive();
        DataSet dsCat = new DataSet();
        try
        {
            dsCat = objCordrive.GetCarCategory();
            if (dsCat.Tables[0].Rows.Count > 0)
            {
                ddlCat.DataSource = dsCat.Tables[0];
                ddlCat.DataTextField = "CarCatName";
                ddlCat.DataValueField = "CarCatID";
                ddlCat.DataBind();
                ddlCat.Items.Insert(0, new ListItem("All", "0"));
            }
            else
            {
                ddlCat.DataSource = null;
                ddlCat.DataBind();
            }

        }
        catch (Exception Ex)
        {
            lblMessate.Visible = true;
            lblMessate.ForeColor = System.Drawing.Color.Red;
            lblMessate.Text = Ex.Message;
        }
    }


    #endregion


}