using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
//using ExcelUtil;

public partial class SSCTrackingReport_Summary : clsPageAuthorization
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ExportToExcel.Attributes.Add("onclick", "return validate();");
        }
    }

    protected void ExportToExcel_Click(object sender, EventArgs e)
    {
        string DateFrom, DateTo, ToDateRemoveYN;
        DateFrom = txtFrom.Value;
        DateTo = txtTo.Value;
        ToDateRemoveYN = ddlPickupRemoveYN.SelectedValue.ToString();

        //string html = "";
        clsAdmin objadmin = new clsAdmin();
        DataTable dtEx = new DataTable();
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=SSCTranckingSummaryReport.ods");
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else //if (ExportOption.SelectedItem.ToString() == "MS Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=SSCTranckingSummaryReport.xls");
            Response.ContentType = "application/vnd.ms-excel";
        }
        
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        this.EnableViewState = false;
        Response.Write("\r\n");

        if (ddlPivot.SelectedItem.ToString() == "Pivot 1")
        {
            dsExcel = objadmin.GetDataForExcelSSC_Pivot1(DateFrom, DateTo, ToDateRemoveYN);
        }
        else
        {
            dsExcel = objadmin.GetDataForExcelSSC_Pivot2(DateFrom, DateTo, ToDateRemoveYN);
        }

        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());

        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");

        int[] iColumns = new int[30];

        if (ddlPivot.SelectedItem.ToString() == "Pivot 1")
        {
            iColumns = new int[8] { 0, 1, 2, 3, 4, 5, 6, 7 };
        }
        else
        {
            iColumns = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        }
        

        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                Response.Write("<td>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
}