using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReportingTool;

public partial class VendorUtilizationSummaryCrystal : System.Web.UI.Page
{
    string strTo;
    int strcityid;

    clsAdmin objAdmin = new clsAdmin();

    ReportDocument VendorUtilizationSummary = new ReportDocument();
    
    ParameterFields collectionParam = new ParameterFields();

    ParameterField paramDateTo = new ParameterField();
    ParameterDiscreteValue paramDVDateTo = new ParameterDiscreteValue();

    ParameterField paramcityid = new ParameterField();
    ParameterDiscreteValue paramDVcityid = new ParameterDiscreteValue();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["val"] != null)
        {
            string prms = Request.QueryString["val"];
            string[] prm = prms.Split(',');
            strTo = prm[0];
            strcityid = Convert.ToInt32(prm[1]);
            bindReport();
        }
    }

    private void bindReport()
    {
        paramDateTo.Name = "@todate";
        paramDVDateTo.Value = strTo.ToString();
        paramDateTo.CurrentValues.Add(paramDVDateTo);

        paramcityid.Name = "@cityid";
        paramDVcityid.Value = strcityid.ToString();
        paramcityid.CurrentValues.Add(paramDVcityid);


        collectionParam.Add(paramDateTo);
        collectionParam.Add(paramcityid);

        CrystalReportViewer1.ParameterFieldInfo = collectionParam;
        VendorUtilizationSummary.Load(Server.MapPath("VendorUtilizationSummary_1.rpt"));
        VendorUtilizationSummary.SetDatabaseLogon(objAdmin.dbUser, objAdmin.dbPWD);
        CrystalReportViewer1.ReportSource = VendorUtilizationSummary;
        CrystalReportViewer1.DataBind();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        VendorUtilizationSummary.Close();
        VendorUtilizationSummary.Dispose();
    }
}
