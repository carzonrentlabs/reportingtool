﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ReportingTool;

/// <summary>
/// Summary description for clsFuelManagement
/// </summary>
/// 
public class ObjFuelManagement
{
    private int _carId;
    private string _regnNo;
    private int _fuelTypeId;
    private int _quantity;
    private double _fuelRate;
    private double _totalAmount;
    private int _odoMeter;
    private string _Remarks;
    private int _sysUserId;
    private int _cityId;
    private string _cityName;

    public string CityName
    {
        get { return _cityName; }
        set { _cityName = value; }
    }

    public int CarId
    {
        get { return _carId; }
        set { _carId = value; }
    }

    public string RegnNo
    {
        get { return _regnNo; }
        set { _regnNo = value; }
    }

    public int FuelTypeId
    {
        get { return _fuelTypeId; }
        set { _fuelTypeId = value; }
    }
    public int Quantity
    {
        get { return _quantity; }
        set { _quantity = value; }
    }
    public double FuelRate
    {
        get { return _fuelRate; }
        set { _fuelRate = value; }
    }
    public double TotalAmount
    {
        get { return _totalAmount; }
        set { _totalAmount = value; }
    }
    public int OdoMeter
    {
        get { return _odoMeter; }
        set { _odoMeter = value; }
    }
    public string Remarks
    {
        get { return _Remarks; }
        set { _Remarks = value; }
    }
    public int SysUserId
    {
        get { return _sysUserId; }
        set { _sysUserId = value; }
    }
    public int CityId
    {
        get { return _cityId; }
        set { _cityId = value; }
    }
}

public class clsFuelManagement
{
    public clsFuelManagement()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable GetCarRegistration(int cityId)
    {
        SqlParameter[] objParamUser = new SqlParameter[1];
        objParamUser[0] = new SqlParameter("@CityId", cityId);
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetCarRegistrationNo", objParamUser);
    }
    public DataTable GetCarFuelType(int carId)
    {
        SqlParameter[] objParamUser = new SqlParameter[1];
        objParamUser[0] = new SqlParameter("@CarId", carId);
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetCarFuelType", objParamUser);
    }

    public DataTable GetFuelRate(int cityId,int fuelTypeId)
    {
        SqlParameter[] objParamUser = new SqlParameter[2];
        objParamUser[0] = new SqlParameter("@CityId", cityId);
        objParamUser[1] = new SqlParameter("@FuelTypeId", fuelTypeId);
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetCurrentFuelPrice", objParamUser);
    }

    public Int32 InsertFuelManagement(ObjFuelManagement objFuelManage)
    {
        int invoiceId;
        try
        {
            SqlParameter[] objParamUser = new SqlParameter[9];
            objParamUser[0] = new SqlParameter("@Carid", objFuelManage.CarId);
            objParamUser[1] = new SqlParameter("@FuelTypeId", objFuelManage.FuelTypeId);
            objParamUser[2] = new SqlParameter("@FuelPrice", objFuelManage.FuelRate);
            objParamUser[3] = new SqlParameter("@Quantity", objFuelManage.Quantity);
            objParamUser[4] = new SqlParameter("@totalCost", objFuelManage.TotalAmount);
            objParamUser[5] = new SqlParameter("@Odometer", objFuelManage.OdoMeter);
            objParamUser[6] = new SqlParameter("@SysuserId", objFuelManage.SysUserId);
            objParamUser[7] = new SqlParameter("@Remarks", objFuelManage.Remarks);
            objParamUser[8] = new SqlParameter("@cityName", objFuelManage.CityName);
            Object obj= SqlHelper.ExecuteScalar("Prc_InsertFuelMangement", objParamUser);
            invoiceId = Convert.ToInt32 (obj);
        }
        catch (Exception)
        {
            invoiceId = 0;
        }
       
        return invoiceId;
    }

    public DataTable GetVoucherDetails(int voucherId)
    {
        SqlParameter[] objParamUser = new SqlParameter[1];
        objParamUser[0] = new SqlParameter("@VoucherId", voucherId);
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetvoucherDetails", objParamUser);
    }
}