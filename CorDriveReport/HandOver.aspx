<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HandOver.aspx.cs" Inherits="HandOver" Title="HandOver" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <center>
        <table cellpadding="1" cellspacing="1" border="1" style="width: 90%">
            <tr>
                <td colspan="3" align="center" bgcolor="#FF6600" style="padding-top: 10px; padding-bottom: 10px;">
                    <b>Duty slip/parking HandOver</b>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center" style="padding-top: 10px; padding-bottom: 10px;">
                    Hand Over User:
                    <asp:DropDownList ID="ddlUserName" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;<asp:Button ID="btnSaveHandover" runat="server" Text="HandOver" OnClick="btnSaveHandover_Click" />
                    &nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" Visible="False" Font-Bold="True"
                        ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding-top: 10px; padding-bottom: 10px;">
                    <asp:GridView ID="grdHandover" runat="server" AutoGenerateColumns="False" OnRowCommand="grdHandover_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Sr.No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblSNo" runat="server" Text="<%# Container.DataItemIndex+1 %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingId">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingId" runat="server" Text='<%# Bind("BookingId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Client Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblClientCoName" runat="server" Text='<%# Bind("ClientCoName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Guest Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGuest" runat="server" Text='<%# Bind("Guest") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCityName" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PickupDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickupDate" runat="server" Text='<%# Eval("PickupDate", "{0:dd-MMM-yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parking Receipts">
                                <ItemTemplate>
                                    <asp:Label ID="lblParkingReceipts" runat="server" Text='<%# Bind("ParkingReceipts") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Toll Receipts">
                                <ItemTemplate>
                                    <asp:Label ID="lblTollReceipts" runat="server" Text='<%# Bind("TollReceipts") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="InterState Receipts">
                                <ItemTemplate>
                                    <asp:Label ID="lblInterStateReceipts" runat="server" Text='<%# Bind("InterStateReceipts") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paper DutySlip">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaperDutySlip" runat="server" Text='<%# Bind("PaperDutySlip") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Handover">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkHandover" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remove" ShowHeader="false" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRemove" Text="Remove" CausesValidation="false" runat="server"
                                        CommandName="RemoveEntry" CommandArgument='<%#Bind("BookingID") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
