using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for clsPayment
/// </summary>
namespace ReportingTool
{
    public class clsPayment
    {
        public clsPayment()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet GetCorporateCompanyName()
        {
            try
            {
                SqlParameter[] ObjparamUser = new SqlParameter[2];

                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCorporateCompanyName");
            }
            catch
            {
                return null;
            }
        }
        public DataSet GetIndividualCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetIndividualCompanyName");
        }
        public DataSet GetAllCompanyName()
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAllCompanyName");
        }
	public DataSet GetAllCompanyName1(int type)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@type", type);


            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetAllCompanyName1", objParams);
        }
        public DataSet GetCorporateCompanyData(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetCorporateCompanyData", objParams);
            return dsFillData;
        }
        public DataSet GetCorporateCompanyDataAdd(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetCorporateCompanyDataAdd", objParams);
            return dsFillData;
        }
        public DataSet GetMaxCorporateCompanyDataAdd(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetMaxAdditionalCreditLimitData", objParams);
            return dsFillData;
        }
 public DataSet GetBankName()
        {
     
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetBankname");
            return dsFillData;
        }
        public DataSet GetAdditionalCreditLimitData(string CName)
        {
            SqlParameter[] objParams = new SqlParameter[1];
            objParams[0] = new SqlParameter("@CName", CName);
            DataSet dsFillData = new DataSet();
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetAdditionalCreditLimitData", objParams);
            return dsFillData;
        }
        public int AddCreditLimit(string CoId,string AddLimit,DateTime ValidDate,string Reason,int CreatedBy)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CompID", CoId);
            objParams[1] = new SqlParameter("@ExtCreditLimit", AddLimit);
            objParams[2] = new SqlParameter("@Valid_upto", SqlDbType.DateTime);
            objParams[2].Value = ValidDate;
            //objParams[2] = new SqlParameter("@Valid_upto",SqlDbType.DateTime, ValidDate);
            objParams[3] = new SqlParameter("@ReasonForExtnd", Reason);
            objParams[4] = new SqlParameter("@CreatedBy", CreatedBy);
            objParams[5] = new SqlParameter("@UpdatedBy", CreatedBy);
            int result1;
            result1 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "uspCreditLimit", objParams);
            return result1;
        }
        public DataSet GetCompanyServerDate(string CoId)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CName", CoId);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyName", objParams);
            return dsFillData;
        }
        public DataSet GetCompanyPaymentDetails(string CoId,string CqNo,string BName,int Cityid)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@CName", CoId);
            objParams[1] = new SqlParameter("@CqNo", CqNo);
            objParams[2] = new SqlParameter("@BName", BName);
            objParams[3] = new SqlParameter("@Cityid", Cityid);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyPaymentDetails", objParams);
            return dsFillData;
        }
        public DataSet GetCompanyPaymentDetails1(int PayId)
        {
            DataSet dsFillData = new DataSet();
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@PayId", PayId);
            dsFillData = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UspGetCompanyPaymentDetails1", objParams);
            return dsFillData;
        }
        public int AddPaymentDetails(string CompID,string PAmt,string PMode,string Inumber,double Iamt,string DeBank,string DrBank,string Rname,DateTime Idate,DateTime CRDate,string Branch,string remarks,int UID,int cityid)
        {
            SqlParameter[] objParams = new SqlParameter[20];
            objParams[0] = new SqlParameter("@CompID", CompID);
            objParams[1] = new SqlParameter("@PayAmt", PAmt);
            objParams[2] = new SqlParameter("@PayMode", PMode);
            objParams[3] = new SqlParameter("@INo", Inumber);
            objParams[4] = new SqlParameter("@IAmt", Iamt);
            objParams[5] = new SqlParameter("@DeBank", DeBank);
            objParams[6] = new SqlParameter("@DrBank", DrBank);
            objParams[7] = new SqlParameter("@RName", Rname);
            objParams[8] = new SqlParameter("@IDate", SqlDbType.DateTime);
            objParams[8].Value = Idate;
           // objParams[8] = new SqlParameter("@IDate", Idate);
            objParams[9] = new SqlParameter("@CRDate", SqlDbType.DateTime);
            objParams[9].Value = CRDate;
            //objParams[9] = new SqlParameter("@CRDate", CRDate);
            objParams[10] = new SqlParameter("@DDBranch", Branch);
            objParams[11] = new SqlParameter("@Remarks", remarks);
            objParams[12] = new SqlParameter("@CreatedBy", UID);
            objParams[13] = new SqlParameter("@UpdatedBy", UID);
            objParams[14] = new SqlParameter("@Cityid", cityid);
            int Result2 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspAddPaymentDetails", objParams);
            return Result2;
        }
        public int AddVoidPaymentDetails(string CompID, string PAmt, string PMode, string Inumber, double Iamt, string DeBank, string DrBank, string Rname, DateTime Idate, DateTime CRDate, string Branch, string remarks, int UID, int PayID, int cityid)
        {
            SqlParameter[] objParams = new SqlParameter[20];
            objParams[0] = new SqlParameter("@CompID", CompID);
            objParams[1] = new SqlParameter("@PayAmt", PAmt);
            objParams[2] = new SqlParameter("@PayMode", PMode);
            objParams[3] = new SqlParameter("@INo", Inumber);
            objParams[4] = new SqlParameter("@IAmt", Iamt);
            objParams[5] = new SqlParameter("@DeBank", DeBank);
            objParams[6] = new SqlParameter("@DrBank", DrBank);
            objParams[7] = new SqlParameter("@RName", Rname);
            objParams[8] = new SqlParameter("@IDate", SqlDbType.DateTime);
            objParams[8].Value = Idate;
            // objParams[8] = new SqlParameter("@IDate", Idate);
            objParams[9] = new SqlParameter("@CRDate", SqlDbType.DateTime);
            objParams[9].Value = CRDate;
            //objParams[9] = new SqlParameter("@CRDate", CRDate);
            objParams[10] = new SqlParameter("@DDBranch", Branch);
            objParams[11] = new SqlParameter("@Remarks", remarks);
            objParams[12] = new SqlParameter("@CreatedBy", UID);
            objParams[13] = new SqlParameter("@UpdatedBy", UID);
            objParams[14] = new SqlParameter("@PayID", PayID);
            objParams[15] = new SqlParameter("@Cityid", cityid);
            int Result2 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspAddVoidPaymentDetails", objParams);
            return Result2;
        }
        public int AddVoidDetails(int PayId, string Remark, int CreatedBy)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@PayID", PayId);
            objParams[1] = new SqlParameter("@Remark", Remark);
            objParams[5] = new SqlParameter("@UpdatedBy", CreatedBy);
            int result1;
            result1 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UspEditVoidPayment", objParams);

            return result1;
        }
        public int CheckPayentForVoid(int PayId)
        {
            SqlParameter[] objParams = new SqlParameter[6];
            objParams[0] = new SqlParameter("@PaymentId", SqlDbType.Int);
            objParams[0].Direction = ParameterDirection.Input;
            objParams[0].Value = PayId;

            objParams[1] = new SqlParameter("@a", SqlDbType.Int);
            objParams[1].Direction = ParameterDirection.ReturnValue;
            
           // return  Convert.ToInt32(SqlHelper.ExecuteScalar (CommandType.StoredProcedure, "uspcheckPaymentidstatus_1", objParams));
            SqlHelper.ExecuteNonQuery (CommandType.StoredProcedure, "uspcheckPaymentidstatus_1", objParams);
            int status = Convert.ToInt32(objParams[1].Value);

            return status;

            //int status = Convert.ToInt32(objParams[1].Value);
           
            
        }
        public string GetMaxPaymentID()
        {
            //return Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UspGetMaxPaymentID"));
            return Convert.ToString(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UspGetMaxPaymentID"));
        }

        public DataSet GetCityName(int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@cityId", SqlDbType.Int);
            ObjparamUser[0].Direction = ParameterDirection.Input;
            ObjparamUser[0].Value = cityid;

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "uspcityName", ObjparamUser);
        }

    }
}
