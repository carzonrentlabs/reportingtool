﻿<%@ Page Title="Charging Links Status Report" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChargingLinkStatus.aspx.cs" Inherits="CorDriveReport_ChargingLinkStatus" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
<script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $("#<%=txtLinkSendDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtLinkSendDate.ClientID%>').value == "") {
                    alert("Please Enter Link Send Date");
                    document.getElementById('<%=txtLinkSendDate.ClientID%>').focus();
                    return false;
                }

                //                else {
                //                    ShowProgress();
                //                }
            });
        });
        //        function ShowProgress() {
        //            setTimeout(function () {
        //                var modal = $('<div />');
        //                modal.addClass("modal");
        //                $('body').append(modal);
        //                var loading = $(".loading");
        //                loading.show();
        //                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        //                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        //                loading.css({ top: top, left: left });
        //            }, 200);
        //        }
        //        function ShowProgressKill() {
        //            setTimeout(function () {
        //                var modal = $('<div />');
        //                modal.removeClass("modal");
        //                $('body').remove(modal);
        //                loading.hide();
        //                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        //                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        //                loading.css({ top: top, left: left });
        //            }, 200);
        //        }

    </script>
   
 <table cellpadding="0" cellspacing="0" border="1" style="width: 100%">
             <tr>
                <td colspan="3" align="center" bgcolor="#FF6600">
                   <b>Status Of All Charging Links</b> 
                 </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="False" Font-Bold="True" ForeColor="Red"></asp:Label>
                     <br />
                </td>
            </tr>
             <tr>
                <td colspan="3" align="center">
                                    
                    <asp:RadioButtonList ID="rdbCreatePickupDate" runat="server"  
                        RepeatDirection="Horizontal" 
                        onselectedindexchanged="rdbCreatePickupDate_SelectedIndexChanged" 
                        AutoPostBack="True">
                         <asp:ListItem  Value="0" Selected="True">Display All Pending For Approval</asp:ListItem>
                        <asp:ListItem  Value="1">Filter by Criteria</asp:ListItem>
                      
                    </asp:RadioButtonList>
                  <br />  
                 </td>
                   
            </tr>
             <tr>
             <td  colspan="3" align="center">
                 <table cellpadding="0" cellspacing="0" border="1" style="width: 100%">
             <tr id="trAll" runat="server" visible="false">
            
                <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Link Send Date </b>
                 </td>
                 <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Pickup City Name </b>
                 </td>
                  <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                    <b>Status </b>
                 </td>
                <td style="white-space: nowrap; height: 18px;" align="center" bgcolor="#FF6600">
                   
                 </td>
           
             </tr>
              <tr id="trCriteria" runat="server" visible="false">
                <td style="white-space: nowrap;" align="center">
                   <asp:TextBox ID="txtLinkSendDate" runat="server"></asp:TextBox>
                </td>
                <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlCity" runat="server">
                  
                  </asp:DropDownList>
                 </td>
               <td style="white-space: nowrap;" align="center">
                  <asp:DropDownList ID="ddlStatus" runat="server">
                       <asp:ListItem Value="0">--Select Status--</asp:ListItem>
                       <asp:ListItem Value="1">Pending</asp:ListItem>
                       <asp:ListItem Value="2">Rejected</asp:ListItem>
                       <asp:ListItem Value="3">Charged</asp:ListItem>
                  </asp:DropDownList>
                </td>
                   <td style="white-space: nowrap;" align="center">
                   <asp:Button ID="bntGet" runat="server" Text="Search" Font-Bold="True" OnClick="bntGet_Click"/>
                 </td>
            
             </tr>
             <tr>
                <td colspan="4" align="center">
                 

            <div style="width: 100%; height:100%; overflow: scroll">
                      <asp:GridView ID="gvPendingForApproval" runat="server" 
                          AutoGenerateColumns="false" Width="80%" > 
                               <Columns>
                               		    <asp:BoundField HeaderText="BookingId" DataField="BookingId" /> 
                                        <asp:BoundField HeaderText="GuestDetails" DataField="GuestDetails" /> 
                                        <asp:BoundField HeaderText="ClientDetails" DataField="ClientDetails" /> 
                                        <asp:BoundField HeaderText="PickUpCity" DataField="PickUpCity" /> 
                                        <asp:BoundField HeaderText="PickUpDateTime" DataField="PickUpDateTime" /> 
                                        <asp:BoundField HeaderText="TotalAmount" DataField="TotalAmount"/> 
                                        <asp:BoundField HeaderText="ChargingStatus" DataField="ChargingStatus"/> 
                                        <asp:BoundField HeaderText="CardType" DataField="CardType" /> 
                                                                            
                                        <asp:BoundField HeaderText="Reject Reason" DataField="ComplaintDescription" /> 
                                        <asp:BoundField HeaderText="CRM Ticket Number" DataField="ComplaintId" />  
                                       </Columns>
                                                                
                     </asp:GridView>
                   </div>
                </td>
            </tr>
             </table>
             </td>
             </tr>
         
        </table>
</asp:Content>

