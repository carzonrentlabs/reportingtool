<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VendorUtilizationSummary.aspx.cs" Inherits="VendorUtilizationSummary" Title="CarzonRent :: Internal software"%>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" Runat="Server">
    
<script language="Javascript" src="App_Themes/CommonScript.js"></script>
<script language="JavaScript" src="../JScripts/Datefunc.js"></script>
<script language="javascript" type="text/javascript">
function validate()
{
 
    if(document.getElementById('<%=txtTo.ClientID %>').value == "")
    {
        alert("Please Select the To Date");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }
}

</script>
<script language="javascript" type="text/javascript">
window.history.forward(1);
    </script>
        <table width="802" border="0" align="center" id="TABLE1">
            <tbody>
                <tr>
                    <td align="center" colspan="7" style="height: 18px">
                        <strong><u>Vendor Wise Utilization Summary</u></strong>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="width: 492px">
                        Date</td>
                    <td width="52" align="left">
                        <input id="txtTo" runat="server" type="text" readonly="readOnly" /></td>
                    <td align="left" style="width: 39px">
                        <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                            onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                            src="App_Themes/images/calender.gif" style="cursor: hand" /></td>
                            
                                <td align="right" style="width: 105px"> City Name:</td>
                            <td style="width: 183px">
          <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList></td>
 
                </tr>
                <tr>
                    <td align="center" colspan="7">
                        
                        &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Get >>" OnClick="btnSubmit_Click" />
                        &nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
        <div>
            <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
                top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
                scrolling="no" height="182"></iframe>
        </div>
</asp:Content>