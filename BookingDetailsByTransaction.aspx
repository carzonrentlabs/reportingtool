<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BookingDetailsByTransaction.aspx.cs" Inherits="CorDriveReport_BookingDetailsByTransaction" Title="Booking Details Transaction Report" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
    <script src="JQuery/gridviewScroll.min.js" type="text/javascript"></script>
   <%-- <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>--%>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
     <script type="text/javascript">
        $(document).ready(function () {
         
             $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                    ShowProgress();
                }
            });
               gridviewScroll();
        });
        function gridviewScroll() {
            $('#<%=gvBookingDetails.ClientID%>').gridviewScroll({
                width: 1150,
                height: 430
            });
            
            
        } 
</script>
   

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width: 60%">
            <tr>
                <td colspan="6" align="center" bgcolor="#FF6600">
                    <strong>Booking Details Transaction Report</strong>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b> From Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                 
                </td>
                 <td style="white-space: nowrap;" align="right">
                 <b> To Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                 
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                   <%-- <asp:GridView ID="gvBookingDetails" runat="server" />--%>
                       <asp:GridView ID="gvBookingDetails" runat="server" AutoGenerateColumns="false" Width="100%"> 
                                 <Columns> 
                                        <asp:BoundField HeaderText="S.No." DataField="S.No." /> 
                                        <asp:BoundField HeaderText="BookingID" DataField="BookingID" /> 
                                        <asp:BoundField HeaderText="GuestName" DataField="GuestName" /> 
                                        <asp:BoundField HeaderText="PhoneNo" DataField="PhoneNo" /> 
                                        <asp:BoundField HeaderText="EmailID" DataField="EmailID" /> 
                                        <asp:BoundField HeaderText="Bookingdate" DataField="Bookingdate" /> 
                                        <asp:BoundField HeaderText="PickUpDate" DataField="PickUpDate" /> 
                                        <asp:BoundField HeaderText="BookingStatus" DataField="BookingStatus" /> 
                                        <asp:BoundField HeaderText="CORIcId" DataField="CORIcId" /> 
                                        <asp:BoundField HeaderText="PaymentStatus" DataField="PaymentStatus" /> 
                                        <asp:BoundField HeaderText="OriginCode" DataField="origincode" />
                                        <%--<asp:BoundField HeaderText="PaymentDate" DataField="PaymentDate"/>--%>
                                        <asp:BoundField HeaderText="Gateway" DataField="Gateway" />
                                        <asp:BoundField HeaderText="PaymentTransactionId" DataField="PaymentTransactionId" /> 
                                         <asp:BoundField HeaderText="ActualAmtByGateway" DataField="ActualAmtByGateway" /> 
                                         <asp:BoundField HeaderText="ActualClosingAmtByInsta" DataField="ActualClosingAmtByInsta" /> 
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                </td>
            </tr>
        </table>
         <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>

