<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VendorDocReport.aspx.cs" Inherits="CorDriveReport_VendorDocReport" Title="Vendor Doc Report" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="../JQuery/gridviewScroll.min.js"></script> 

 <%--<script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>--%>
   <%-- <link href="../CSSReveal/reveal.css" rel="stylesheet" type="text/css" />--%>
<script type="text/javascript">
    $(document).ready(function () {
        gridviewScroll();
    });

    function gridviewScroll() {
        $('#<%=gvVendorDocReport.ClientID%>').gridviewScroll({
            width: 1150,
            height: 430
        });
    } 
</script>

     <%--<script language="javascript" type="text/javascript">
         function pageLoad(sender, args) 
         {
                $('a[data-reveal-id]').live('click', function (e) {
                    e.preventDefault();
                    var id = $(this).attr("id")
                    var CompId = $("#" + id).text()
                    $("#<%=txtComplaintId.ClientID%>").val(CompId);
                   var modalLocation = $(this).attr('data-reveal-id');
                $('#' + modalLocation).reveal($(this).data());
                });
               $("#bntCancel").click(function () {
                   $(".close-reveal-modal").trigger("click");
               });
               
               
            }

</script>--%>
   
    <center>
       <%-- <table cellpadding="0" cellspacing="0" border="1" style="width:60%">--%>
        <table cellpadding="0" cellspacing="0" border="1" width="200px">
            <tr>
                <td colspan="6" align="center" bgcolor="#FF6600">
                    <strong>Vendor Doc Report</strong>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b> Select City :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   <asp:DropDownList ID="ddlCity" runat ="server"> 
                   </asp:DropDownList>
                 
                </td>
                 <td style="white-space: nowrap;" align="right">
                 <b> Status :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   <asp:DropDownList ID="ddlStatus" runat ="server"> 
                   <asp:ListItem Value="2" Selected="True">All</asp:ListItem>
                   <asp:ListItem Value="1">Active</asp:ListItem>
                   <asp:ListItem Value="0">Non Active</asp:ListItem>
                   </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;
                      <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                 
                      <asp:GridView ID="gvVendorDocReport" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvVendorDocReport_RowDataBound"> 
                                 <Columns> 
    								    <asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                        <asp:BoundField HeaderText="CityName" DataField="CityName" /> 
                                        <asp:BoundField HeaderText="RMName" DataField="RMName" /> 
                                        <asp:BoundField HeaderText="RMContactNo" DataField="RMContactNo" /> 
                                        <asp:BoundField HeaderText="AllActive" DataField="AllActive" /> 
                                        <asp:BoundField HeaderText="InstaCabId" DataField="InstaCabId" /> 
                                        <asp:BoundField HeaderText="RegnNo" DataField="RegnNo" /> 
                                        <asp:BoundField HeaderText="Model" DataField="Model" /> 
                                        <asp:BoundField HeaderText="CarCatName" DataField="CarCatName" /> 
                                        <asp:BoundField HeaderText="FuelTypeName" DataField="FuelTypeName" /> 
                                        <asp:BoundField HeaderText="CarCreationDate" DataField="CarCreationDate" /> 
                                        <asp:BoundField HeaderText="CarStatus" DataField="CarStatus" />
                                        <asp:BoundField HeaderText="CarAgeInYear" DataField="CarAgeInYear" />
                                        <asp:BoundField HeaderText="RC Issue Date" DataField="RCDate" /> 
                                        <asp:BoundField HeaderText="PermitExpiryDate" DataField="PermitExpiryDate" /> 
                                        <asp:BoundField HeaderText="InsuranceExpiryDate" DataField="InsuranceExpiryDate" /> 
                                        <asp:BoundField HeaderText="FitnessExpiryDate" DataField="FitnessExpiryDate" /> 
                                        <asp:BoundField HeaderText="InstaVendorId" DataField="InstaVendorId" /> 
                                        <asp:BoundField HeaderText="CarVendorName" DataField="CarVendorName" /> 
                                        <asp:BoundField HeaderText="VendorContactNo" DataField="VendorContactNo" /> 
                                        <asp:BoundField HeaderText="PANNo" DataField="PANNo" /> 
                                        <asp:BoundField HeaderText="VendorCreateDate" DataField="VendorCreateDate" /> 
                                        <asp:BoundField HeaderText="VendorStatus" DataField="VedorStatus" /> 
                                        <asp:BoundField HeaderText="AgreementStartDate" DataField="AgreementStartDate" /> 
                                        <asp:BoundField HeaderText="AgreementEndDate" DataField="AgreementEndDate" />
                                        <asp:BoundField HeaderText="RevenueSharePC" DataField="RevenueSharePC" />
                                        <asp:BoundField HeaderText="InstaChauffeurID" DataField="InstaChauffeurID" /> 
                                        <asp:BoundField HeaderText="ChauffeurName" DataField="ChauffeurName" /> 
                                        <asp:BoundField HeaderText="ChauffeurContact" DataField="ChauffeurContact" />
                                        <asp:BoundField HeaderText="ChauffeurCreationDate" DataField="ChauffeurCreationDate" />
                                        <asp:BoundField HeaderText="ChauffeurStatus" DataField="ChauffeurStatus" /> 
                                        <asp:BoundField HeaderText="DLExpiryDate" DataField="DLExpiryDate" />
                                        <asp:BoundField HeaderText="PoliceVerificationDate" DataField="PoliceVerificationDate" />
                                        <asp:BoundField HeaderText="MedicalFitnessDate" DataField="MedicalFitnessDate" /> 
                                        <asp:BoundField HeaderText="CommercialBatchNo" DataField="CommercialBatchNo" />  
                                        <%--<asp:BoundField HeaderText="BookingByCab" DataField="BookingByCab" />                                        --%>
                                       <%-- <asp:TemplateField HeaderText="Modification History">
                                        <ItemTemplate>
                                        
                                            <asp:LinkButton ID="lkDetails" runat="server" class="big-link" data-reveal-id="myModal"
                                            Text='<%#Eval("InstaVendorId") %>' ForeColor="Red"  CommandArgument='<%#Eval("InstaVendorId")%>'></asp:LinkButton>
                                           </ItemTemplate> 
                                       </asp:TemplateField>--%>
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                   
                </td>
            </tr>
           
        </table>
         <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
    <%--    </div>
     <div id="myModal" class="reveal-modal">
        <fieldset style="border-color: Green">
            <legend><b>Complaint Details</b></legend>
            <table class="bksharma">
                <tr>
                    <td align="right" style="white-space:nowrap">Complaint Id: 
                    </td>
                    <td>
                        <input type="text" id="txtComplaintId" runat="server" readonly="readonly" />
                    </td>
                    
                </tr>

               
            </table>
        </fieldset>
        <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>
    </div>--%>
       
     </center>
</asp:Content>
