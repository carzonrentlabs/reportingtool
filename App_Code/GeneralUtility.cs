﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;

/// <summary>
/// Summary description for GeneralUtility
/// </summary>
public class GeneralUtility
{
    public GeneralUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable ConvertXSLXtoDataTable(string strFilePath, string connString)
    {
        OleDbConnection oledbConn = new OleDbConnection(connString);
        DataTable dt = new DataTable();
        try
        {

            oledbConn.Open();
            using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oledbConn))
            {
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                oleda.SelectCommand = cmd;
                DataSet ds = new DataSet();
                oleda.Fill(ds);
                dt = ds.Tables[0];
            }
        }
        catch (Exception ex)
        {
			 throw new Exception(ex.Message);
        }
        finally
        {
            oledbConn.Close();
        }

        return dt;
    }
    public static bool IsNumeric(object obj)
    {
        bool check = true;
        string strNumeric = obj.ToString();
        foreach (char c in strNumeric)
        {
            if (!Char.IsDigit(c) && c != '.')
            {
                check = false;
            }
        }
        return check;
    }
}