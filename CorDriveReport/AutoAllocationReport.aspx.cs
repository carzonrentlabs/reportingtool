using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ExcelUtil;

public partial class CorDriveReport_AutoAllocationReport : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsBidDetailsExprtToExcel = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    DataSet dsBidCarwiseExportToExcel = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindBrachDropDownlist();
        }
    }

    private void BindBrachDropDownlist()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_SummaryCityWise(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        AutoAllocationBidSummary();
        //if (ddlCity.SelectedIndex !=0)
        //{
            AutoAllocationBidDetails();
        //}
    }
    protected void bntExport_Click(object sender, EventArgs e)
    {
        //if (ddlCity.SelectedIndex != 0)
        //{
            objCordrive = new CorDrive();
            dsBidDetailsExprtToExcel = objCordrive.GetBidDetailsReport(ddlCity.SelectedItem.Text, Convert.ToDateTime(txtFromDate.Text),Convert.ToDateTime(txtToDate.Text));
            WorkbookEngine.ExportDataSetToExcel(dsBidDetailsExprtToExcel, "BidDetailsReport.xls");
        //}
    }
    public void AutoAllocationBidSummary()
    {
        objCordrive = new CorDrive();
        DataSet dsBidSummary= new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date or To date can not be empty.')", true);
                return;
            }
            else
            {
               // dsBidSummary = objCordrive.GetBidSummaryReport(Convert.ToDateTime("2015-12-14"));
                dsBidSummary = objCordrive.GetBidSummaryReport(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                if (dsBidSummary.Tables[0].Rows.Count > 0)
                {
                    grdBidSummaryMorning.DataSource = dsBidSummary.Tables[0];
                    grdBidSummaryMorning.DataBind();
                                       
                }
                else
                {
                    grdBidSummaryMorning.DataSource = null;
                    grdBidSummaryMorning.DataBind();
                    lblMessage.Text = "Morning Bid Summary Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
                if (dsBidSummary.Tables[1].Rows.Count > 0)
                {
                    grdBidSummaryInterDay.DataSource = dsBidSummary.Tables[1];
                    grdBidSummaryInterDay.DataBind();
                }
                else
                {
                    grdBidSummaryInterDay.DataSource = null;
                    grdBidSummaryInterDay.DataBind();
                    lblMessage.Text = "InterDay Bid Summary Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }

    public void AutoAllocationBidDetails()
    {
        objCordrive = new CorDrive();
        DataSet dsBidDetails = new DataSet();
        try
        {
            if(string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date or To date can not be empty.')", true);
                return;
            }
            else
            {

                dsBidDetails = objCordrive.GetBidDetailsReport(ddlCity.SelectedItem.Text, Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                //dsBidDetails = objCordrive.GetBidDetailsReport(ddlCity.SelectedItem.Text, Convert.ToDateTime("2015-12-14"));
                if (dsBidDetails.Tables[0].Rows.Count > 0)
                {

                    grdBidDetails.DataSource = dsBidDetails.Tables[0];
                    grdBidDetails.DataBind();
                }
                else
                {
                  
                    grdBidDetails.DataSource = null;
                    grdBidDetails.DataBind();

                    lblMessage.Text = "Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date To date can not be empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsExportToExcel = objCordrive.GetDeallocationReportExportToExcel(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
            WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "DeallocationReport.xls");
        }
    }
    protected void grdBidDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Export")
        {
            LinkButton btndetails = (LinkButton)e.CommandSource;
            GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            int CarId = Convert.ToInt32(HttpUtility.HtmlDecode(gvrow.Cells[1].Text));
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From date or To date can not be empty.')", true);
                return;
            }
            else
            {
                objCordrive = new CorDrive();
                dsBidCarwiseExportToExcel = objCordrive.GetCarWiseBidDetailsExportToExcel(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), CarId);
                WorkbookEngine.ExportDataSetToExcel(dsBidCarwiseExportToExcel, "BidDetailsCarwise.xls");
            }
          
        }
    }
}
