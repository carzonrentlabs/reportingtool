﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using ReportingTool;
using System.Drawing;
public partial class SSCBatchCreationWithin72Hours : System.Web.UI.Page
{
    clsAutomation objSSCBatch = new clsAutomation();
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCityName();
            btnGet.Attributes.Add("onclick", "return validate()");
            btnExportToExcel.Attributes.Add("onclick", "return validate()");
        }
        btnGet.Attributes.Add("onclick", "return validate()");
        btnExportToExcel.Attributes.Add("onclick", "return validate()");
    }

    private void BindCityName()
    {

        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        ddlLocation.DataTextField = "CityName";
        ddlLocation.DataValueField = "CityID";
        ddlLocation.DataSource = dsLocation;
        ddlLocation.DataBind();

    }


    protected void btnGet_Click(object sender, EventArgs e)
    {
        string fromdate = txtFrom.Value.ToString();
        string todate = txtTo.Value.ToString();
        Int16 location = Convert.ToInt16(ddlLocation.SelectedValue);
        DataTable dtSSCBatch = new DataTable();
        if (fromdate != "" && todate != "")
        {
            try
            {
                dtSSCBatch = objSSCBatch.SSCBatchCreationWithinTimeInterval(fromdate, todate, location);
                if (dtSSCBatch.Rows.Count > 0)
                {
                    txtMessage.Visible = false;
                    GrvCCSBatch.DataSource = dtSSCBatch;
                    GrvCCSBatch.DataBind();
                }
                else
                {
                    GrvCCSBatch.DataSource = null;
                    GrvCCSBatch.DataBind();
                    txtMessage.Visible = true;
                    txtMessage.Text = "Record is not available.";
                    txtMessage.ForeColor = Color.Red;
                }

            }
            catch (Exception Ex)
            {

                txtMessage.Visible = true;
                txtMessage.Text = Ex.Message;
                txtMessage.ForeColor = Color.Red;
            }

        }
        else
        {
            txtMessage.Visible = true;
            txtMessage.Text = "Select date";
            txtMessage.ForeColor = Color.Red;
        }
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        string fromdate = txtFrom.Value.ToString();
        string todate = txtTo.Value.ToString();
        Int16 location = Convert.ToInt16(ddlLocation.SelectedValue);
        DataTable dtSSCBatch = new DataTable();
        if (fromdate != "" && todate != "")
        {
            dtSSCBatch = objSSCBatch.SSCBatchCreationWithinTimeInterval(fromdate, todate, location);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
            Response.Clear();
            Response.AppendHeader("content-disposition", "attachment;filename=SSCBatchCreation.xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            this.EnableViewState = false;
            Response.Write("\r\n");
            if (dtSSCBatch.Rows.Count > 0)
            {
                Response.Write("<table border = '1' align = 'center'> ");
                Response.Write("<tr><td align='center' colspan='5'><b>Aging >=72 Hrs</b></td></tr>");
                int[] iColumns = { 0, 1, 2, 3, 4 };
                for (int i = 0; i < dtSSCBatch.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            if (dtSSCBatch.Columns[iColumns[j]].Caption.ToString() == "UnitName")
                            {
                                Response.Write("<td align='left'><b>Location</b></td>");
                            }
                            else if (dtSSCBatch.Columns[iColumns[j]].Caption.ToString() == "Created")
                            {
                                Response.Write("<td align='left'><b>SSC BATCH CREATED</b></td>");
                            }
                            else if (dtSSCBatch.Columns[iColumns[j]].Caption.ToString() == "NotCreated")
                            {
                                Response.Write("<td align='left'><b>SSC BATCH NOT CREATED</b></td>");
                            }
                            else if (dtSSCBatch.Columns[iColumns[j]].Caption.ToString() == "GrandTotal")
                            {
                                Response.Write("<td align='left'><b>Grand Total</b></td>");
                            }
                            else if (dtSSCBatch.Columns[iColumns[j]].Caption.ToString() == "AWBDetails")
                            {
                                Response.Write("<td align='left'><b>AWB Details Not Available</b></td>");
                            }
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        Response.Write("<td align='left'>" + dtSSCBatch.Rows[i][iColumns[j]].ToString() + "</td>");
                    }
                    Response.Write("</tr>");
                }
                Response.Write("</table>");
                Response.End();
            }
            else
            {
                Response.Write("<table border = 1 align = 'center' width = '100%'>");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();
            }
        }
        else
        {
            txtMessage.Visible = true;
            txtMessage.Text = "Select date";
            txtMessage.ForeColor = Color.Red;
        }

    }

    protected void GrvCCSBatch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Export")
        {
            string fromdate = txtFrom.Value.ToString();
            string todate = txtTo.Value.ToString();
            //string Location12 = GrvCCSBatch.DataKeys[GrvCCSBatch.SelectedIndex].Value.ToString();
            Button bntExport = (Button)e.CommandSource;
            GridViewRow rv = (GridViewRow)bntExport.NamingContainer;
            string location = GrvCCSBatch.DataKeys[rv.RowIndex].Value.ToString();
            DataTable dtExportDetail = new DataTable();
            dtExportDetail = objSSCBatch.SSCBatchAWBDetails(fromdate, todate, location);
            if (fromdate != "" && todate != "")
            {
                GotDetails(dtExportDetail);
            }
            else
            {
                txtMessage.Visible = true;
                txtMessage.Text = "Select date";
                txtMessage.ForeColor = Color.Red;
            }
        }
    }
    private void GotDetails(DataTable dtExportDetail)
    {

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        Response.AppendHeader("content-disposition", "attachment;filename=SSCBatchDetails.xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        this.EnableViewState = false;
        Response.Write("\r\n");
        if (dtExportDetail.Rows.Count > 0)
        {
            Response.Write("<table border = '1' align = 'center'> ");
            int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            for (int i = 0; i < dtExportDetail.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "Location")
                        {
                            Response.Write("<td align='left'><b>Location</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "BookingID")
                        {
                            Response.Write("<td align='left'><b>Booking Id</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "CompanyName")
                        {
                            Response.Write("<td align='left'><b>Company Name</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "UsageDate")
                        {
                            Response.Write("<td align='left'><b>Usage Date</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "SSCBtachNo")
                        {
                            Response.Write("<td align='left'><b>SSC Batch No</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "AWBAvailable")
                        {
                            Response.Write("<td align='left'><b>AWB Details Not Available</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "SSCBtachCreatedYN")
                        {
                            Response.Write("<td align='left'><b>SSC Btach Created</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "ScanedBy")
                        {
                            Response.Write("<td align='left'><b>Scaned By</b></td>");
                        }
                        else if (dtExportDetail.Columns[iColumns[j]].Caption.ToString() == "UnitName")
                        {
                            Response.Write("<td align='left'><b>Branch Name</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtExportDetail.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' width = '100%'>");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }

    }
    protected void GrvCCSBatch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string temp = GrvCCSBatch.DataKeys[e.Row.RowIndex].Value.ToString();
            if (temp == "Total")
            {
                Button bntExport = (Button)e.Row.FindControl("bntExport");
                //Label lblSNo=(Label)e.Row .FindControl ("lblSNo");
                // bntExport.Visible = false;  //this line commented on 22 Aug 2014
                //lblSNo.Visible = false;
                bntExport.Text = "Export to Excel Total";
            }
        }

    }
}