using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class VendorUtilizationReport : clsPageAuthorization
//public partial class VendorUtilizationReport : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate;
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=VendorUtilizationReport.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetVendorUtilizationDetail(Todate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 10, 12, 13 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>City Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "carmodel")
                        {
                            Response.Write("<td align='left'><b>Car Model</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "category")
                        {
                            Response.Write("<td align='left'><b>Car Category</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                        {
                            Response.Write("<td align='left'><b>Vendor Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "VehicleNo")
                        {
                            Response.Write("<td align='left'><b>Vehicle No</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ContactNo")
                        {
                            Response.Write("<td align='left'><b>Contact No. of Vendor</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "RevenueSharePC")
                        {
                            Response.Write("<td align='left'><b>Revenue Share</b></td>");
                        }

                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Ownership")
                        {
                            Response.Write("<td align='left'><b>Ownership Status</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "StandardAmt")
                        {
                            Response.Write("<td align='left'><b>Std Amount</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "QualifiedAmt")
                        {
                            Response.Write("<td align='left'><b>Qualified Amount</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ActualAmt")
                        {
                            Response.Write("<td align='left'><b>Actual Amount</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ExpectedAmt")
                        {
                            Response.Write("<td align='left'><b>Expected Amount</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Variace")
                        {
                            Response.Write("<td align='left'><b>Variance</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Utilisation")
                        {
                            Response.Write("<td align='left'><b>Utilisation</b></td>");
                        }

                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate;
        
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetVendorUtilizationDetail(Todate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }
}