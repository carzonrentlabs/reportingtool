﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Text;

public partial class CorDriveReport_DutyRoster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Request.QueryString.HasKeys())
            {
                Session["UserID"] = GetSessionFromToken.GetSession(Request.QueryString["SessionToken"]);
            }
            else
            {
                Session["UserID"] = null;
            }
            BindCityDropDownlist();
        }
        bntExprot.Attributes.Add("Onclick", "return validate()");

    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strAttachment = new StringBuilder();
        int totalPickup = 0;
        string totalWorkingDays = "0";
        string airPort = "0";
        string disPosal = "0";
        string outStation = "0", IndicatedPrice = "0";
        string totalClosedDuties = "0", totalOpenDuties = "0";
        Vendor objVendor = new Vendor();

        try
        {


            StringBuilder vendorBody = new StringBuilder();
            DataSet ds = new DataSet();
            ds = objVendor.VendorDetails(Convert.ToInt32(ddlCity.SelectedValue), DateTime.Parse(txtDate.Text));
            int hour = System.DateTime.Now.Hour;
            DateTime reportdate;
            string columnname = "";
            if (Convert.ToDateTime(txtDate.Text).Month == System.DateTime.Now.Month && Convert.ToDateTime(txtDate.Text).Year == System.DateTime.Now.Year)
            {
                if (hour == 1)
                {
                    columnname = "Current Day-";
                    reportdate = Convert.ToDateTime(System.DateTime.Now.ToString("dd-MMM-yyyy"));
                }
                else if (hour == 9)
                {
                    columnname = "Current Day-";
                    reportdate = Convert.ToDateTime(System.DateTime.Now.ToString("dd-MMM-yyyy"));
                }
                else if (hour == 21)
                {
                    columnname = "Next Day-";
                    reportdate = Convert.ToDateTime(System.DateTime.Now.AddDays(1).ToString("dd-MMM-yyyy"));
                }
                else
                {
                    if (hour <= 15)
                    {
                        columnname = "Current Day-";
                        reportdate = Convert.ToDateTime(System.DateTime.Now.ToString("dd-MMM-yyyy"));
                    }
                    else
                    {
                        columnname = "Next Day-";
                        reportdate = Convert.ToDateTime(System.DateTime.Now.AddDays(1).ToString("dd-MMM-yyyy"));
                    }
                }

            }
            else
            {
                columnname = "Current Day-";
                reportdate = Convert.ToDateTime(Convert.ToDateTime(txtDate.Text).ToString("dd-MMM-yyyy"));
            }
            if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0] != null)
            {

                strAttachment.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
                strAttachment.Append("<body>");

                strAttachment.Append("<table width='100%' border='1' style='font-size:12px;' cellpadding='0' cellspacing='0'>");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i == 0) // && borderStyle == 0)
                    {
                        //borderStyle = 1; //set to single header
                        strAttachment.Append("<tr>");
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            if (ds.Tables[0].Columns[j].Caption.ToString() != "CarId" && ds.Tables[0].Columns[j].Caption.ToString() != "VendorCarYN" && ds.Tables[0].Columns[j].Caption.ToString() != "CarType"
                                && ds.Tables[0].Columns[j].Caption.ToString() != "TotalBookings" && ds.Tables[0].Columns[j].Caption.ToString() != "TotalWorkingDays"
                                && ds.Tables[0].Columns[j].Caption.ToString() != "Airport" && ds.Tables[0].Columns[j].Caption.ToString() != "Disposal" && ds.Tables[0].Columns[j].Caption.ToString() != "Outstation"
                                && ds.Tables[0].Columns[j].Caption.ToString() != "IndicatedPrice"
                                && ds.Tables[0].Columns[j].Caption.ToString() != "TotalColsedDuties" && ds.Tables[0].Columns[j].Caption.ToString() != "TotalOpenDuties")
                            {
                                if (ds.Tables[0].Columns[j].Caption.Contains("_") && ds.Tables[0].Columns[j].Caption.Contains("Duty"))
                                {
                                    string[] headerCaptionDuty = ds.Tables[0].Columns[j].Caption.ToString().Split('#');
                                    string[] headerCaption = headerCaptionDuty[1].ToString().Split('_');

                                    string dd = headerCaption[2] + "/" + headerCaption[0] + "/" + headerCaption[1];
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>" + Convert.ToDateTime(dd).Day.ToString()
                                   + "/" + Convert.ToDateTime(dd).ToString("MMM") + "<br>" + headerCaptionDuty[0].ToString() + "</b></td>");

                                }
                                else if (ds.Tables[0].Columns[j].Caption.ToString() == "ChauffeurName")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>Chauffeur<br/>Name</b></td>");
                                }
                                else if (ds.Tables[0].Columns[j].Caption.ToString() == "VendorName")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>Vendor<br/>Name</b></td>");
                                }
                                else if (ds.Tables[0].Columns[j].Caption.ToString() == "NextDayTotalBooking")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>" + columnname + "<br/>" + reportdate.ToString("dd-MMM") + "</b></td>");
                                }
                                else if (ds.Tables[0].Columns[j].Caption.ToString() == "RevenuesharingType")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>Payment Option</b></td>");
                                }
                                else if (ds.Tables[0].Columns[j].Caption.ToString() == "AvailabilityRemarks")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>Availability Remarks</b></td>");
                                }
                                else if (ds.Tables[0].Columns[j].Caption.ToString() == "CarManufactureYear")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>Manufacture Year</b></td>");
                                }
                                else
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>" + ds.Tables[0].Columns[j].Caption.ToString() + "</b></td>");
                                }
                            }
                        }

                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Total</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Closed Duties</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Open Duties</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Working <br/> Days</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Airport/One Way</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Disposal</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>MTD Outstation</b></td>");
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'><b>Indicate Price</b></td>");
                        strAttachment.Append("</tr>");
                    }
                    strAttachment.Append("<tr>");
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        if (dc.Caption.ToString() != "CarId" && dc.Caption.ToString() != "VendorCarYN" && dc.Caption.ToString() != "CarType"
                           && dc.Caption.ToString() != "TotalWorkingDays" && dc.Caption.ToString() != "Airport" && dc.Caption.ToString() != "Disposal"
                           && dc.Caption.ToString() != "TotalBookings" && dc.Caption.ToString() != "Outstation" && dc.Caption.ToString() != "IndicatedPrice"
                          && dc.Caption.ToString() != "TotalColsedDuties" && dc.Caption.ToString() != "TotalOpenDuties")
                        {
                            if (dc.Caption.ToString() != "TotalBookings" && dc.Caption.ToString() != "TotalRevenue")
                            {
                                if (Convert.ToString(ds.Tables[0].Rows[i][dc]) == "0" || Convert.ToString(ds.Tables[0].Rows[i][dc]).Trim() == "0 (0.00)" || Convert.ToString(ds.Tables[0].Rows[i][dc]).Trim() == "0<br/>(0)")
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:normal;'>&nbsp;</td>");
                                }
                                else if (dc.Caption.ToString() == "CarManufactureYear")
                                {
                                    if (ds.Tables[0].Rows[i][dc] == DBNull.Value)
                                    {
                                        strAttachment.Append("<td style='text-align:center;white-space:normal;'>&nbsp;&nbsp;</td>");
                                    }
                                    else
                                    {
                                        strAttachment.Append("<td style='text-align:center;white-space:normal;'>&nbsp;" + Convert.ToDateTime(ds.Tables[0].Rows[i][dc]).ToString("dd-MM-yyyy") + "&nbsp;</td>");
                                    }

                                }
                                else
                                {
                                    strAttachment.Append("<td style='text-align:center;white-space:normal;'>" + ds.Tables[0].Rows[i][dc] + "</td>");
                                }
                            }

                        }
                        else if (dc.Caption.ToString() == "TotalBookings")
                        {
                            totalPickup = Convert.ToInt32(ds.Tables[0].Rows[i]["TotalBookings"]);
                        }
                        else if (dc.Caption.ToString() == "TotalWorkingDays")
                        {
                            totalWorkingDays = Convert.ToString(ds.Tables[0].Rows[i]["TotalWorkingDays"]);
                        }
                        else if (dc.Caption.ToString() == "Airport")
                        {
                            airPort = Convert.ToString(ds.Tables[0].Rows[i]["Airport"]);
                        }
                        else if (dc.Caption.ToString() == "Outstation")
                        {
                            outStation = Convert.ToString(ds.Tables[0].Rows[i]["outStation"]);
                        }
                        else if (dc.Caption.ToString() == "Disposal")
                        {
                            disPosal = Convert.ToString(ds.Tables[0].Rows[i]["Disposal"]);
                        }
                        else if (dc.Caption.ToString() == "IndicatedPrice")
                        {
                            IndicatedPrice = Convert.ToString(ds.Tables[0].Rows[i]["IndicatedPrice"]);
                        }

                        else if (dc.Caption.ToString() == "TotalColsedDuties")
                        {
                            totalClosedDuties = Convert.ToString(ds.Tables[0].Rows[i]["TotalColsedDuties"]);
                        }
                        else if (dc.Caption.ToString() == "TotalOpenDuties")
                        {
                            totalOpenDuties = Convert.ToString(ds.Tables[0].Rows[i]["TotalOpenDuties"]);
                        }

                    }
                    if (totalPickup == 0)
                    {
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;background-color:Red;'>" + totalPickup + "</td>");
                    }
                    else
                    {
                        strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + totalPickup + "</td>");
                    }
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + totalClosedDuties + "</td>");
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + totalOpenDuties + "</td>");
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + totalWorkingDays + "</td>");
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + airPort + "</td>");
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + disPosal + "</td>");
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + outStation + "</td>");
                    strAttachment.Append("<td style='text-align:center;white-space:nowrap;'>" + IndicatedPrice + "</td>");

                    strAttachment.Append("</tr>");

                }
                strAttachment.Append("</table>");
                strAttachment.Append("</body></html>");
            }

            StringBuilder strData = new StringBuilder();
            strData = strAttachment;
            ltlSummary.Text = strData.ToString();
            Response.Clear();
            Response.Buffer = true;
            string strFileName = "Duty Roster";
            strFileName = strFileName.Replace("(", "");
            strFileName = strFileName.Replace(")", "");
            Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            ltlSummary.RenderControl(hw);
            Response.Output.Write(sw.ToString());
            Response.End();
            //borderStyle = 0; //set to single header
        } //end if //if EmailID Exists


        //End Mercedes

        catch (Exception Ex)
        {

        }
        finally
        {
        }

    }

    private void BindCityDropDownlist()
    {
        Vendor objVendor = new Vendor();
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objVendor.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {

                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation.Tables[0];
                    ddlCity.DataBind();
                    if (Convert.ToString(dsLocation.Tables[0].Rows[0]["CityName"]) == " -- All -- ")
                    {
                        ddlCity.Items.RemoveAt(0);
                    }

                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
}
