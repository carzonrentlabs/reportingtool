using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReportingTool;

public partial class ComplaintWiseBranchWiseCrystal : System.Web.UI.Page
{
    string strFrom;
    int strcityid;

    clsAdmin objAdmin = new clsAdmin();

    ReportDocument VendorUtilizationSummary = new ReportDocument();
    
    ParameterFields collectionParam = new ParameterFields();

    ParameterField paramDateFrom = new ParameterField();
    ParameterDiscreteValue paramDVDateFrom = new ParameterDiscreteValue();

    ParameterField paramcityid = new ParameterField();
    ParameterDiscreteValue paramDVcityid = new ParameterDiscreteValue();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["val"] != null)
        {
            string prms = Request.QueryString["val"];
            string[] prm = prms.Split(',');
            strFrom = prm[0];
            strcityid = Convert.ToInt32(prm[1]);
            bindReport();
        }
    }

    private void bindReport()
    {
        paramDateFrom.Name = "@Date";
        paramDVDateFrom.Value = strFrom.ToString();
        paramDateFrom.CurrentValues.Add(paramDVDateFrom);

        paramcityid.Name = "@Cityid";
        paramDVcityid.Value = strcityid.ToString();
        paramcityid.CurrentValues.Add(paramDVcityid);

        collectionParam.Add(paramDateFrom);
        collectionParam.Add(paramcityid);

        CrystalReportViewer1.ParameterFieldInfo = collectionParam;
        VendorUtilizationSummary.Load(Server.MapPath("BranchWiseComplaintSummary.rpt"));
        VendorUtilizationSummary.SetDatabaseLogon(objAdmin.dbUser, objAdmin.dbPWD);
        CrystalReportViewer1.ReportSource = VendorUtilizationSummary;
        CrystalReportViewer1.DataBind();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        VendorUtilizationSummary.Close();
        VendorUtilizationSummary.Dispose();
    }
}
