using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class DutyAllocationDetail : clsPageAuthorization
//public partial class DutyAllocationDetail : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Text.ToString();
        Todate = txtTo.Text.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=DutyAllocationDetail.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetDutiesAllocationDetail(FromDate, Todate, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 0, 1, 2, 5, 3, 4, 6, 7, 8, 9, 11, 12, 13 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingID")
                        {
                            Response.Write("<td align='left'><b>Booking ID</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PickupDate")
                        {
                            Response.Write("<td align='left'><b>Pickup Date</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeallocatedDate")
                        {
                            Response.Write("<td align='left'><b>De- allocated Date</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeallocatedName")
                        {
                            Response.Write("<td align='left'><b>Executive Name (De-allocated)</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeallocationRemarks")
                        {
                            Response.Write("<td align='left'><b>De-allocation Remarks</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>City Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AllocatedCar")
                        {
                            Response.Write("<td align='left'><b>Allocated Vehicle No.</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarNo")
                        {
                            Response.Write("<td align='left'><b>Reallocated Vehicle  No.</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ChangeReason")
                        {
                            Response.Write("<td align='left'><b>Vehicle Change Reason</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OustationYN")
                        {
                            Response.Write("<td align='left'><b>Outstation</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingCreateDate")
                        {
                            Response.Write("<td align='left'><b>BookingCreateDate</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DutyAllocationDate")
                        {
                            Response.Write("<td align='left'><b>DutyAllocationDate</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AssignBeforeTimeInMins")
                        {
                            Response.Write("<td align='left'><b>AssignBeforeTimeInMins</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Text.ToString();
        Todate = txtTo.Text.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetDutiesAllocationDetail(FromDate, Todate, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }
}