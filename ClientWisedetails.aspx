<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientWisedetails.aspx.cs"
    MasterPageFile="~/MasterPage.master" Inherits="ClientWisedetails" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cphPage">
    <%--   <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>--%>
    <table align="center" cellspacing="0" cellpadding="0" border="1" style="width: 80%">
        <tr>
            <td style="width: 80%" >
                <table align="center" style="width: 70%">
                    <tr>
                        <td colspan="3" align="center" style="height: 8px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%">
                            Customer Name
                        </td>
                        <td  colspan="2" style="width:85%">
                            <asp:DropDownList ID="ddlCustomerName" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvCutomername" runat="server" ControlToValidate="ddlCustomerName"
                                InitialValue="--Select--" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Location
                        </td>
                        <td  colspan="2">
                            <asp:DropDownList ID="ddllocation" runat="server">
                            </asp:DropDownList>
                            <%--<td style="width: 30%" align="left">--%>
                            <asp:RequiredFieldValidator ID="rfvddllocation" runat="server" ControlToValidate="ddllocation"
                                InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Implant
                        </td>
                        <td colspan="2" >
                            <asp:CheckBox ID="chkImplant" runat="server" OnCheckedChanged="chkImplant_CheckedChanged"
                                AutoPostBack="True" />
                            <asp:TextBox ID="txtImplant" Width="150px" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <%-- <tr>
            <td>
                Implant</td>
            <td>
                <asp:TextBox ID="txtImplant" Width="170px" runat="server"></asp:TextBox>
            </td>
        </tr>--%>
                    <tr>
                        <td >
                            Relationship Manager
                        </td>
                        <td colspan="2" >
                            <asp:DropDownList ID="ddlrmgr" Width="175px" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlrmgr" runat="server" ControlToValidate="ddlrmgr"
                                InitialValue="--Select--" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Competitors
                        </td>
                        <td colspan="2" >
                            <asp:TextBox ID="txtcompetitors" Width="170px" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtcompetitors" runat="server" ControlToValidate="txtcompetitors"
                                ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            Potential (pm)
                        </td>
                        <td colspan="2" >
                            <asp:TextBox ID="txtPotential" Width="170px" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtPotential" runat="server" ControlToValidate="txtPotential"
                                ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            National Account Manager
                        </td>
                        <td colspan="2" >
                            <asp:DropDownList ID="ddlNAccMngr" Width="175px" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlNAccMngr" runat="server" ControlToValidate="ddlNAccMngr"
                                InitialValue="--Select--" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" style="height: 22px">
                            <asp:Button ID="btnsubmit" runat="server" Text="Save" OnClick="btnsubmit_Click" CausesValidation="true" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
