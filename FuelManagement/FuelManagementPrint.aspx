﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FuelManagementPrint.aspx.cs"
    Inherits="FuelManagement_FuelManagementPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="5" cellspacing="0" border="1">
            <tr>
                <td>
                    <table cellpadding="5" cellspacing="0" border="0">
                        <tr>
                            <td colspan="2" align="center">
                                <img src="../App_Themes/images/logo_carzonret.gif" alt="" width="100px" height="100px" />
                            </td>
                            <td colspan="2" align="left">
                                <b>Credit Memo</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Voucher No.</b>
                            </td>
                            <td>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Dated</b>
                            </td>
                            <td>
                                <asp:Label ID="lblDated" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>City Name</b>
                            </td>
                            <td>
                                <asp:Label ID="lblCityName" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Reg.No</b>
                            </td>
                            <td>
                                <asp:Label ID="lblRegNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Fuel Type</b>
                            </td>
                            <td>
                                <asp:Label ID="lblFuelType" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Rate/Liter (In Rs.)</b>
                            </td>
                            <td>
                                <asp:Label ID="lblRate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Quantity (In liters)</b>
                            </td>
                            <td>
                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Amount (In Rs.)</b>
                            </td>
                            <td>
                                <asp:Label ID="lblAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Remarks</b>
                            </td>
                            <td>
                                <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="center">
                                <b>Created By</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblCreateDateTime" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
