using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;

public partial class EasycabsCarwiseRevenue : System.Web.UI.Page
{
    DataSet dsEasycabsCorpBooking = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
       // Session["Userid"] = 3122;
        if (!IsPostBack)
        {
            BindCity();
        }
    }
    public void BindCity()
    {
        DataSet dsLocation = new DataSet();
        try
        {
            objCordrive = new CorDrive();
            dsLocation = objCordrive.GetLocations_UserAccessWise_SummaryCityWiseNew(Convert.ToInt32(Session["UserID"]));
            if (dsLocation.Tables.Count > 0)
            {
                ddlCity.DataTextField = "Cityname";
                ddlCity.DataValueField = "CityId";
                ddlCity.DataSource = dsLocation;
                ddlCity.DataBind();
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    public void BindGrid()
    {
        try
        {
            objCordrive = new CorDrive();
            dsEasycabsCorpBooking = objCordrive.GetEasycabscarwiserevenueDetails(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()), Convert.ToInt32(ddlCity.SelectedValue));
            if (dsEasycabsCorpBooking.Tables[0].Rows.Count > 0)
            {
                gvEasycabsCorpBooking.DataSource = dsEasycabsCorpBooking.Tables[0];
                gvEasycabsCorpBooking.DataBind();
            }
            else
            {
                gvEasycabsCorpBooking.DataSource = dsEasycabsCorpBooking.Tables[0];
                gvEasycabsCorpBooking.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From Date and To Date can not be empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            //dsEasycabsCorpBooking = objCordrive.GetEasycabscarwiserevenueDetails(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()), Convert.ToInt32(ddlCity.SelectedValue));
            //WorkbookEngine.ExportDataSetToExcel(dsEasycabsCorpBooking, "EasycabsCorpBooking" + ".xls");

            DataTable dtEx = new DataTable();

            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=EasyCabsCarWiseRevenue.xls");

            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";

            this.EnableViewState = false;
            Response.Write("\r\n");

            dsEasycabsCorpBooking = objCordrive.GetEasycabscarwiserevenueDetails(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()), Convert.ToInt32(ddlCity.SelectedValue));
            dtEx = dsEasycabsCorpBooking.Tables[0];
            if (dtEx.Rows.Count > 0)
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 };
                for (int i = 0; i < dtEx.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Taxino")
                            {
                                Response.Write("<td align='left'><b>S. No.</b></td>");
                                Response.Write("<td align='left'><b>Taxi No</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Loginduration")
                            {
                                Response.Write("<td align='left'><b>Total Login Duration</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "totalbidssend")
                            {
                                Response.Write("<td align='left'><b>Total Bids Sent</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "bidsReached")
                            {
                                Response.Write("<td align='left'><b>Bids Reached</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "bidsaccepted")
                            {
                                Response.Write("<td align='left'><b>Bids Accepted</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "calcResetAt")
                            {
                                Response.Write("<td align='left'><b>Last Calculation Reset At</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "firstbidacceptat")
                            {
                                Response.Write("<td align='left'><b>First Bid Accept At</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "lastbidacceptat")
                            {
                                Response.Write("<td align='left'><b>Last Bid Accept At</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "autobidduty")
                            {
                                Response.Write("<td align='left'><b>Auto Bid Duty</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "autobidrevenue")
                            {
                                Response.Write("<td align='left'><b>Auto Bid Revenue</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "manualduty")
                            {
                                Response.Write("<td align='left'><b>Manual Duty</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "manualrevenue")
                            {
                                Response.Write("<td align='left'><b>Manual Revenue</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "kurbduty")
                            {
                                Response.Write("<td align='left'><b>Kurb Duty</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "kurbrevenue")
                            {
                                Response.Write("<td align='left'><b>Kurb Revenue</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "firstdutypickupat")
                            {
                                Response.Write("<td align='left'><b>First Duty Pickup At</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "lastdutypickupat")
                            {
                                Response.Write("<td align='left'><b>Last Duty Pickup At</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "totalduty")
                            {
                                Response.Write("<td align='left'><b>Total Duty</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "totalrevenue")
                            {
                                Response.Write("<td align='left'><b>Total Revenue</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "revenuekm")
                            {
                                Response.Write("<td align='left'><b>Revenue KM</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "nonrevenuekm")
                            {
                                Response.Write("<td align='left'><b>Non Revenue KM</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "approxccdutykm")
                            {
                                Response.Write("<td align='left'><b>Approx. Call Center Duty KM</b></td>");
                            }
                            else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "regnno")
                            {
                                Response.Write("<td align='left'><b>Vehicle No</b></td>");
                            }
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Taxino")
                        {
                            Response.Write("<td align='left'>" + (i + 1).ToString() + "</td>");
                        }
                        Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                    }
                    Response.Write("</tr>");
                }
                Response.Write("</table>");
                Response.End();
            }
            else
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();
            }
            //***export to excel
        }
    }
}
