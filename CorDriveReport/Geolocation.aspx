<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Geolocation.aspx.cs" Inherits="CorDriveReport_Geolocation_" Title="COR Drive Geolocation Validation Report" %>
<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       $(document).ready(function () 
       {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
            $("#<%=btnExport.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtToDate.ClientID%>').value == "") {
                    alert("Please Select ToDate");
                    document.getElementById('<%=txtToDate.ClientID%>').focus();
                    return false;
                }
                else {
                   // ShowProgress();
                }
            });
       });
         function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() 
        {  
            setTimeout(function () 
            {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            },200);
        }

       
    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="1" style="width: 60%">
            <tr>
                <td colspan="5" align="center" bgcolor="#FF6600">
                    <strong>COR Drive Geolocation Validation Report</strong>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                     <br />
                </td>
            </tr>
           
            <tr>
                <td style="white-space: nowrap;" align="right">
                 <b> From Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                   
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                 
                </td>
                 <td style="white-space: nowrap;" align="right">
                 <b> To Date :</b>&nbsp;&nbsp;
                </td>
                <td style="white-space: nowrap;" align="left">
                 
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </td>
               <%-- <td style="height: 20px; white-space: nowrap;">
                  <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>--%>
                <td style="white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export To Excel" OnClick="btnExport_Click"/>
                </td>
            </tr>
        </table>
       <%--  <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>--%>
    </center>
</asp:Content>

