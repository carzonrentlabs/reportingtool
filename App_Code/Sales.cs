﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using ReportingTool;
using System;

/// <summary>
/// Summary description for Sales
/// </summary>
public class Sales
{
	public Sales()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet GetRetailReportData(int month, int year)
    {

        SqlParameter[] ObjParam = new SqlParameter[2];

        ObjParam[0] = new SqlParameter("@Month", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = month;

        ObjParam[1] = new SqlParameter("@Year", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = year;
        
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_BranchMonthWiseRevenueReport_CorRetail_Total", ObjParam);

    }
}