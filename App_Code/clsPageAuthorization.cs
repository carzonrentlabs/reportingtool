// *******************************************************************************************************************
//*				Project name		: FBC-Intranet																			
//*				Module name			: --
//*				Sub module name		: --
//*				File name			: clsPageAuthorization.cs
//*				Programmer name		: Saurabh Hoonka
//*				Creation date		: 14-02-2008
//*				Version No.			: 
//*				Edited by			: 
//*             Edited on           : 
//*				Reviewed by			: --
//*				Reviewed on			: --
//*				Final review		: --																			
//*				Final review on		: --																				
//*				File Description	: This class checks whether the user requesting a page URL is valid or not.
//*																 			
//*				Class Description	:
//*											Class Name: 
//*                                                 clsPageAuthoriation
//*                                                 
//*											Member Methods:
//*													1) CheckUser
//*													2) clsPageAuthorization 
//*													3) clsPageAuthorization_Load
//*
//*                                         Class Properties:
//*                                                 NA
//********************************************************************************************************************
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace ReportingTool
{
    public class clsPageAuthorization : System.Web.UI.Page
    {
        /// <summary>
        /// Class Constructor - binds the Load event
        /// </summary>
        public clsPageAuthorization()
        {
            base.Load += new EventHandler(clsPageAuthorization_Load);
        }

        /// <summary>
        /// Load Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void clsPageAuthorization_Load(object sender, EventArgs e)
        {
            //CheckLang();
            CheckUser();
            CacheCleanUp();
        }

        /// <summary>
        /// Method to check whether session is valid or not
        /// </summary>
        private void CheckUser()
        {
            if (Session["Userid"] == null)
            {
                //Not a valid user; Redirect user to login page
                Response.Redirect("http://insta.carzonrent.com/");
            }
        }

        /// <summary>
        /// Method to Handle cache memory for the page
        /// </summary>
        private void CacheCleanUp()
        {
           Response.Cache.SetCacheability(HttpCacheability.NoCache); 
        }

        //private void CheckLang()
        //{
        //    if (Session["Language"] == null)
        //    {
        //        //Not a valid user; Redirect user to login page
        //        Response.Redirect("Login.aspx");
        //    }
        //}
    }
}