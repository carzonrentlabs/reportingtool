﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TrackingDutiesClosureBranches.aspx.cs"
    Inherits="TrackingDutiesClosureBranches" MasterPageFile="~/MasterPage.master"
    Title="CarzonRent :: Internal software" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>
    <script language="JavaScript" type="text/javascript" src="../JScripts/Datefunc.js"></script>
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }

            if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <table border="0" align="center" cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td align="center" colspan="5">
                    <strong><u>Tracking of duties closure at Branches</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right" valign="middle">
                    From Date
                </td>
                <td valign="top">
                    <input id="txtFrom" runat="server" type="text" readonly="readOnly" />
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFrom,ctl00$cphPage$txtFrom, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td align="right">
                    To Date
                </td>
                <td valign="top">
                    <input id="txtTo" runat="server" type="text" readonly="readOnly" />
                    <img alt="Click here to open the calendar and select the date corresponding to 'To Date'"
                        onclick="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtTo,ctl00$cphPage$txtTo, 1);return false;"
                        src="App_Themes/images/calender.gif" style="cursor: hand" />
                </td>
                <td>
                    Location
                </td>
                <td style="width: 183px">
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;<%--<asp:Button ID="btnSubmit" runat="server" Text="Go"/>--%>
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:Literal runat="server" ID="ltrStudentDetails"></asp:Literal>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
