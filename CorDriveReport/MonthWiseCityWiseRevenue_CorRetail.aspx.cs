﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using ReportingTool;
public partial class MonthWiseCityWiseRevenue_CorRetail : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindYear(ddlYear);
            BindMonth(ddlMonth);
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }

    private void BindMonth(DropDownList ddlMonth)
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlMonth.SelectedValue = System.DateTime.Now.Month.ToString();
        }
        catch (Exception)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2015; i <= System.DateTime.Now.Year; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlYear.SelectedValue = System.DateTime.Now.Year.ToString();
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {

        if (ddlMonth.SelectedItem.Value == "0" || ddlYear.SelectedItem.Value == "0")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please Select Year and month.')", true);
            return;
        }
        else
        {
            StringBuilder strData = new StringBuilder();
            strData = BindExcelData();
            ltlSummary.Text = strData.ToString();
        }
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "MonthWiseCityWiseReport_CorRetail";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();
    }

    private StringBuilder BindExcelData()
    {
        StringBuilder strData = new StringBuilder();
        objCordrive = new CorDrive();
        DataSet dsDetails = objCordrive.MonthWiseBranchWiseRevenue_CorRetail(Convert.ToInt32(ddlMonth.SelectedItem.Value), Convert.ToInt32(ddlYear.SelectedItem.Value));
        DataView dv = new DataView(dsDetails.Tables[0]);
        try
        {
            if (dsDetails.Tables[0].Rows.Count > 0)
            {
                strData.Append(" <html xmlns:o='urn:schemas-microsoft-com:office:office' ");
                strData.Append(" xmlns:x='urn:schemas-microsoft-com:office:excel' ");
                strData.Append(" xmlns='http://www.w3.org/TR/REC-html40'> ");
                strData.Append(" <head> ");
                strData.Append(" <meta http-equiv=Content-Type content='text/html; charset=windows-1252'> ");
                strData.Append(" <meta name=ProgId content=Excel.Sheet> ");
                strData.Append(" <meta name=Generator content='Microsoft Excel 14'> ");
                strData.Append(" <link rel=File-List href='Category_files/filelist.xml'> ");
                strData.Append(" <style id='Category_25550_Styles'> ");
                strData.Append(" <!--table ");
                strData.Append(" {mso-displayed-decimal-separator:; ");
                strData.Append(" mso-displayed-thousand-separator:;} ");
                strData.Append(" .xl1525550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:general; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6325550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6425550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6525550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:right; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6625550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" text-align:right; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6725550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" text-align:right; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:1.0pt solid black; ");
                strData.Append(" background:#F2F2F2; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6825550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6925550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:1.0pt solid black; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7025550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:1.0pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7125550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7225550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:1.0pt solid black; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7325550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:none; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:1.0pt solid black; ");
                strData.Append(" background:#F2F2F2; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7425550 ");
                strData.Append(" {padding-top:1px; ");
                strData.Append(" padding-right:1px; ");
                strData.Append(" padding-left:1px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border-top:1.0pt solid black; ");
                strData.Append(" border-right:1.0pt solid black; ");
                strData.Append(" border-bottom:1.0pt solid black; ");
                strData.Append(" border-left:none; ");
                strData.Append(" background:#F2F2F2; ");
                strData.Append(" mso-pattern:black none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" --> ");
                strData.Append(" </style> ");
                strData.Append(" </head> ");
                strData.Append(" <body> ");
                strData.Append(" <!--[if !excel]>&nbsp;&nbsp;<![endif]--> ");
                strData.Append(" <!--The following information was generated by Microsoft Excel's Publish as Web ");
                strData.Append(" Page wizard.--> ");
                strData.Append(" <!--If the same item is republished from Excel, all information between the DIV ");
                strData.Append(" tags will be replaced.--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <div id='Category_25550' align=center x:publishsource='Excel'> ");
                strData.Append(" <table border=1 cellpadding=0 cellspacing=0 width=748 style='border-collapse: ");
                strData.Append(" collapse;table-layout:fixed;width:561pt'> ");
                strData.Append(" <col width=215 style='mso-width-source:userset;mso-width-alt:7862;width:161pt'> ");
                strData.Append(" <col width=95 style='mso-width-source:userset;mso-width-alt:3474;width:71pt'> ");
                strData.Append(" <col width=75 style='mso-width-source:userset;mso-width-alt:2742;width:56pt'> ");
                strData.Append(" <col width=64 style='width:48pt'> ");
                strData.Append(" <col width=56 style='mso-width-source:userset;mso-width-alt:2048;width:42pt'> ");
                strData.Append(" <col width=81 span=3 style='mso-width-source:userset;mso-width-alt:2962; ");
                strData.Append(" width:61pt'> ");
                strData.Append(" <tr height=20 style='height:15.0pt'> ");
                strData.Append(" <td colspan=1 height=20 class=xl6825550 width=310 style='border-right:1.0pt solid black; ");
                strData.Append(" height:15.0pt;width:232pt'>&nbsp;</td> ");
                strData.Append(" <td colspan=2 class=xl7025550 width=195 style='border-right:1.0pt solid black; ");
                strData.Append(" border-left:none;width:146pt'>Revenue</td> ");

                strData.Append(" <td colspan=1 class=xl7025550 width=195 style='border-right:1.0pt solid black; ");
                strData.Append(" border-left:none;width:146pt'></td> ");

                strData.Append(" <td colspan=2 class=xl7025550 width=243 style='border-right:1.0pt solid black; ");
                strData.Append(" border-left:none;width:183pt'>Expected</td> ");
                strData.Append(" </tr> ");
                strData.Append(" <tr height=20 style='height:15.0pt'> ");
                strData.Append(" <td height=20 class=xl6325550 style='height:15.0pt;border-top:none'>Branch</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Duty</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Revenue</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Budgeted</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Duty</td> ");
                strData.Append(" <td class=xl6325550 style='border-top:none;border-left:none'>Revenue</td> ");
                strData.Append(" </tr> ");


                foreach (DataRow dr in dsDetails.Tables[0].Rows)
                {

                    if (Convert.ToString(dr["CityID"]) == "0")
                    {
                        strData.Append(" <tr height=20 style='height:15.0pt'> ");
                        strData.Append(" <td colspan=1 height=20 class=xl7325550 style='border-right:1.0pt solid black; ");
                        strData.Append(" height:15.0pt'>Total</td> ");
                        strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["RevTotal"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["Revenue"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>0</td> ");
                        strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["ExpectedDuty"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6725550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["ExpectedRevenue"].ToString() + "</td> ");
                        strData.Append(" </tr> ");
                    }
                    else
                    {
                        strData.Append(" <tr height=20 style='height:15.0pt'> ");
                        strData.Append(" <td height=20 class=xl6425550 style='height:15.0pt;border-top:none'>" + dr["CityName"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6525550 style='border-top:none;border-left:none'>" + dr["RevTotal"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["Revenue"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>0</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["ExpectedDuty"].ToString() + "</td> ");
                        strData.Append(" <td class=xl6625550 style='border-top:none;border-left:none'><span ");
                        strData.Append(" style='mso-spacerun:yes'></span>" + dr["ExpectedRevenue"].ToString() + "</td> ");
                        strData.Append(" </tr> ");

                    }
                }
                strData.Append(" <![if supportMisalignedColumns]> ");
                strData.Append(" <tr height=0 style='display:none'> ");
                strData.Append(" <td width=215 style='width:161pt'></td> ");
                strData.Append(" <td width=95 style='width:71pt'></td> ");
                strData.Append(" <td width=75 style='width:56pt'></td> ");
                strData.Append(" <td width=64 style='width:48pt'></td> ");
                strData.Append(" <td width=56 style='width:42pt'></td> ");
                strData.Append(" <td width=81 style='width:61pt'></td> ");
                //strData.Append(" <td width=81 style='width:61pt'></td> ");
                //strData.Append(" <td width=81 style='width:61pt'></td> ");
                strData.Append(" </tr> ");
                strData.Append(" <![endif]> ");
                strData.Append(" </table> ");
                strData.Append(" </div> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" </body> ");
                strData.Append(" </html> ");
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return strData;
    }
}