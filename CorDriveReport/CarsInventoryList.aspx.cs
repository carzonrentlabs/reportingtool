﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_CarsInventoryList : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCityDropDownlist();
            BindCarModelDropDowmnList();
        }

    }
    #region "Private Bind Function For DropDownlist"
    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    private void BindCarModelDropDowmnList()
    {
        try
        {
            DataSet _objDScarModel = new DataSet();
            _objDScarModel = objCordrive.GetActiveCarModel();
            if (_objDScarModel != null)
            {
                if (_objDScarModel.Tables.Count > 0)
                {
                    ddlCarModel.DataTextField = "ModelName";
                    ddlCarModel.DataValueField = "ModelId";
                    ddlCarModel.DataSource = _objDScarModel.Tables[0];
                    ddlCarModel.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    private void BindCarInventryListGrid()
    {
        try
        {
            DataSet objds = new DataSet();
            int _cityId = Convert.ToInt32(ddlCity.SelectedValue.ToString() == null || ddlCity.SelectedValue.ToString() == "" ? "0" : ddlCity.SelectedValue.ToString());
            int _status = Convert.ToInt32(ddlStatus.SelectedValue.ToString() == null || ddlStatus.SelectedValue.ToString() == "" ? "0" : ddlStatus.SelectedValue.ToString());
            int _model_id = Convert.ToInt32(ddlCarModel.SelectedValue.ToString() == null || ddlCarModel.SelectedValue.ToString() == "" ? "0" : ddlCarModel.SelectedValue.ToString());
            int _vdp_YN = Convert.ToInt32(ddlVdpYN.SelectedValue.ToString() == null || ddlVdpYN.SelectedValue.ToString() == "" ? "0" : ddlVdpYN.SelectedValue.ToString());
            int _car_age = Convert.ToInt32(ddlCarAge.SelectedValue.ToString() == null || ddlCarAge.SelectedValue.ToString() == "" ? "0" : ddlCarAge.SelectedValue.ToString());

            objds = objCordrive.GetCarInInventryList(_cityId, _status, _model_id, _vdp_YN, _car_age);
            if (objds != null)
            {
                if (objds.Tables[0].Rows.Count > 0)
                {
                    gvCarInventryList.DataSource = objds.Tables[0];
                    gvCarInventryList.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    #endregion
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindCarInventryListGrid();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void gvCarInventryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvCarInventryList.PageIndex = e.NewPageIndex;
            BindCarInventryListGrid();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            int counter = 0;
            DataSet objds = new DataSet();
            DataTable dt = new DataTable();
            int _cityId = Convert.ToInt32(ddlCity.SelectedValue.ToString() == null || ddlCity.SelectedValue.ToString() == "" ? "0" : ddlCity.SelectedValue.ToString());
            int _status = Convert.ToInt32(ddlStatus.SelectedValue.ToString() == null || ddlStatus.SelectedValue.ToString() == "" ? "0" : ddlStatus.SelectedValue.ToString());
            int _model_id = Convert.ToInt32(ddlCarModel.SelectedValue.ToString() == null || ddlCarModel.SelectedValue.ToString() == "" ? "0" : ddlCarModel.SelectedValue.ToString());
            int _vdp_YN = Convert.ToInt32(ddlVdpYN.SelectedValue.ToString() == null || ddlVdpYN.SelectedValue.ToString() == "" ? "0" : ddlVdpYN.SelectedValue.ToString());
            int _car_age = Convert.ToInt32(ddlCarAge.SelectedValue.ToString() == null || ddlCarAge.SelectedValue.ToString() == "" ? "0" : ddlCarAge.SelectedValue.ToString());

            objds = objCordrive.GetCarInInventryList(_cityId, _status, _model_id, _vdp_YN, _car_age);
            if (objds != null)
            {
                if (objds.Tables[0].Rows.Count > 0)
                {
                    dt = objds.Tables[0];
                }
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,17,18,19 };

                    Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=CarsinInventoryList.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        counter = counter + 1;
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            Response.Write("<td align='left'><b>S.No.</b></td>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "CarModelName")
                                {
                                    Response.Write("<td align='left'><b>Car Model</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarCatName")
                                {
                                    Response.Write("<td align='left'><b>Car Category</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "RegnNo")
                                {
                                    Response.Write("<td align='left'><b>Vehicle No</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarVendorName")
                                {
                                    Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                {
                                    Response.Write("<td align='left'><b>CityName</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "VDPYN")
                                {
                                    Response.Write("<td align='left'><b>VDP YN</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarStatus")
                                {
                                    Response.Write("<td align='left'><b>Status</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "VehicleCreateDate")
                                {
                                    Response.Write("<td align='left'><b>Vehicle Created Date</b></td>");
                                }

                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofModifcation")
                                {
                                    Response.Write("<td align='left'><b>Vehicle Modified Date</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofVendorCreationDate")
                                {
                                    Response.Write("<td align='left'><b>Vendor Created Date</b></td>");
                                }

                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "AttachmentModel")
                                {
                                    Response.Write("<td align='left'><b>Attachment Model</b></td>");
                                }

                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "Grade")
                                {
                                    Response.Write("<td align='left'><b>Grade</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "ParkingLocation")
                                {
                                    Response.Write("<td align='left'><b>Parking Location</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CorDriveStatus")
                                {
                                    Response.Write("<td align='left'><b>COR Drive Status</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "GPSStatus")
                                {
                                    Response.Write("<td align='left'><b>GPS Status</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateOfActivation")
                                {
                                    Response.Write("<td align='left'><b>Date of Activation</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DocumentUploadedBy")
                                {
                                    Response.Write("<td align='left'><b>Doument Uploaded by</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DocumentApprovedBy")
                                {
                                    Response.Write("<td align='left'><b>Doument Approved by</b></td>");
                                }
                                //else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofModification")
                                //{
                                //    Response.Write("<td align='left'><b>Date of Modification</b></td>");
                                //}
                                //else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofDeactivation")
                                //{
                                //    Response.Write("<td align='left'><b>Date of Deactivation</b></td>");
                                //}
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "deactivatedBy")
                                {
                                    Response.Write("<td align='left'><b>Deactivated by</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "AgeOfCar")
                                {
                                    Response.Write("<td align='left'><b>Age of car as on Date</b></td>");
                                }

                            }
                            Response.Write("</tr>");
                        }
                        Response.Write("<tr>");
                        Response.Write("<td align='left'>" + counter + "</td>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            Response.Write("<td align='left'>" + Convert.ToString(dt.Rows[i][iColumns[j]]) + "</td>");
                        }
                    }


                    Response.Write("</tr>");
                }
                Response.Write("</table>");
                Response.End();
            }
            else
            {

                Response.Write("<table border = 1 align = 'center' width = '100%'>");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();

            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
}