<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DispatchModifyReport.aspx.cs" Inherits="DispatchModifyReport" 
MasterPageFile="~/MasterPage.master" Title="CarzonRent :: Internal software"%>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
   <script src="../JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>.
    
  <script type ="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <%--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">--%>
  <script type ="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script type ="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
           
        });

        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }        
        }
    </script>
  <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 30%">
                <tr>
                    <td colspan="3" align="center">
                        <b>Dispatch Modify Report</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr> 
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblFromDate" runat="server">Modify Date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                    </td>
                     <td style="height: 20px; white-space: nowrap;">
                        <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                         <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div>
            <asp:Repeater ID="rptVendorUsageReport" runat="server">
                <HeaderTemplate>
                    <table id="sort_table" width="100%" border ="1" cellpadding ="0" cellspacing ="0">
                        <thead>
                            <tr>
                                   <%-- <th align="center">
                                    Sr.No
                                    </th>--%>
                                    <th align="center">
                                       Booking Id
                                    </th>
                                    <th align="center">
                                       Km Out
                                    </th>
                                    <th align="center">
                                        Time Out
                                    </th>
                                    <th align="center">
                                      Date Out
                                    </th>
                                    <th align="center">
                                        Km In
                                    </th>
                                    <th align="center">
                                        Time In
                                    </th>
                                    <th align="center">
                                        Date In
                                    </th>
                                    <th align="center">
                                        Point Opening Km
                                    </th>
                                    <th align="center">
                                        Guest Opening Time
                                    </th>
                                    <th align="center">
                                       Guest Opening Date
                                    </th>
                                    <th align="center">
                                        Km Close
                                    </th>
                                    <th align="center">
                                      Guest Closing Time
                                    </th>
                                     <th align="center">
                                       Guest Closing Date
                                    </th>
                                    <th align="center">
                                        Vendor Car
                                    </th>
                                    <th align="center">
                                      Vendor Chauffeur
                                    </th>
                                     <th align="center">
                                       Reg No
                                    </th>
                                    <th align="center">
                                        Chauffeur Name
                                    </th>
                                    <th align="center">
                                      Created Date
                                    </th>
                                     <th align="center">
                                        Modify Date
                                    </th>
                                    <th align="center">
                                      Modify By
                                    </th>
                     </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                       <%-- <td style="text-align: center;">&nbsp;&nbsp;
                            <%#Eval("Id")%>
                        </td>--%>
                        <td style="text-align:left;">
                            <%#Eval("BookingID")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("KMOut")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("TimeOut")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("DateOut")%>&nbsp;
                        </td>
                        <td style="text-align: left;">
                            <%#Eval("KMIn")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("TimeIn")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("DateIn")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("pointOpeniningKm")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("GuestOpTime")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("GuestOpDate")%>&nbsp;
                        </td>
                        
                         <td style="text-align: center;">
                            <%#Eval("KMClose")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("GuestClTime")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("GuestClDate")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("VendorCarYs")%>&nbsp;
                        </td>
                        
                          <td style="text-align: center;">
                            <%#Eval("VendorChauffYN")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("RegnNo")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("chauffeurName")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("CreateDate")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("ModifyDate")%>&nbsp;
                        </td>
                        <td style="text-align: center;">
                            <%#Eval("ModifyBy")%>&nbsp;
                        </td>
                       
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
            <div align="center" id="dvPaging" runat="server" visible="false">
                <asp:LinkButton ID="lnkPrev" runat="server" Text="Prev" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkPrev_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnkNext" runat="server" Text="Next" Font-Bold="true" BackColor="LightBlue"
                    OnClick="lnkNext_Click"></asp:LinkButton>
            </div>
        </div>
    </center>
</asp:Content>
