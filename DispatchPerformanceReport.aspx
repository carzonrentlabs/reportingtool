<%@ Page Title="Vendor Performance Report" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="DispatchPerformanceReport.aspx.cs" Inherits="VendorPerformanceReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="Javascript" type="text/javascript" src="App_Themes/CommonScript.js"></script>

    <script language="JavaScript" type="text/javascript" src="../JScripts/Datefunc.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script> 
<script type="text/javascript" src="JQuery/gridviewScroll.min.js"></script> 
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById('<%=ddlMonth.ClientID%>').value == "0") {
                alert("Please select Month.");
                document.getElementById('<%=ddlMonth.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=ddlYear.ClientID%>').value == "--Select--") {
                alert("Please select Year.");
                document.getElementById('<%=ddlYear.ClientID%>').focus();
                return false;
            }
        }
    </script>
<script type="text/javascript">
    $(document).ready(function () {
        gridviewScroll();
    });

    function gridviewScroll() {
        $('#<%=gvDispatchPerformance.ClientID%>').gridviewScroll({
            width: 1150,
            height: 430
        });
    } 
</script>
    <table width="100%" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="6">
                    <strong><u>Dispatch Performance Report</u></strong>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="6">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Center" colspan="6">
                    Month : 
                    <asp:DropDownList ID="ddlMonth" runat="server" Width="74px">
                    </asp:DropDownList>
                    &nbsp;&nbsp; Year : 
                    <asp:DropDownList ID="ddlYear" runat="server" Width="74px">
                    </asp:DropDownList>
                    &nbsp; &nbsp; &nbsp;
                    <asp:Button ID="btnSubmit" runat="server" Text="Get It" OnClick="btnSubmit_Click" />
                    &nbsp; &nbsp;
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                </td>
            </tr>
             <tr>
                <td colspan="6">
                 										
																			

                      <asp:GridView ID="gvDispatchPerformance" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvDispatchPerformance_RowDataBound"> 
                                 <Columns> 
    								    <asp:TemplateField HeaderText="Sl.No.">                        
                                            <ItemTemplate>
                                                <asp:Label ID="lblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>                                                                 
                                            </ItemTemplate>
                                        </asp:TemplateField>                             
                                        <asp:BoundField HeaderText="CityName" DataField="CityName" /> 
                                       <%-- <asp:BoundField HeaderText="CityID" DataField="CityID" /> --%>
                                        <asp:BoundField HeaderText="AllocationTarget" DataField="AllocationTarget" /> 
                                        <asp:BoundField HeaderText="AllocationAchivedMonth1" DataField="AllocationAchivedMonth1" /> 
                                        <asp:BoundField HeaderText="AllocationAchivedMonth2" DataField="AllocationAchivedMonth2" /> 
                                        <asp:BoundField HeaderText="AllocationShortfall" DataField="AllocationShortfall" /> 
                                                  

                                          <asp:BoundField HeaderText="ManualDSTarget" DataField="ManualDSTarget" /> 
                                           <asp:BoundField HeaderText="ManualDSAchivedMonth1" DataField="ManualDSAchivedMonth1" /> 
                                        <asp:BoundField HeaderText="ManualDSAchivedMonth2" DataField="ManualDSAchivedMonth2" /> 
                                                                              
                                        <asp:BoundField HeaderText="ManualDSShortfall" DataField="ManualDSShortfall" /> 
                                        
                                        
                                        <asp:BoundField HeaderText="ServiceQualityTarget" DataField="ServiceQualityTarget" /> 
                                          <asp:BoundField HeaderText="ServiceQualityAchivedMonth1" DataField="ServiceQualityAchivedMonth1" />
                                           <asp:BoundField HeaderText="ServiceQualityAchivedMonth2" DataField="ServiceQualityAchivedMonth2" /> 
                                                                   
                                        <asp:BoundField HeaderText="ServiceQualityShortfall" DataField="ServiceQualityShortfall" />
                                          <asp:BoundField HeaderText="RefusalTarget" DataField="RefusalTarget" />  
                                           <asp:BoundField HeaderText="RefusalAchivedMonth1" DataField="RefusalAchivedMonth1" /> 
                                                                                      
                                          <asp:BoundField HeaderText="RefusalAchivedMonth2" DataField="RefusalAchivedMonth2" /> 
                                                                                                                              					 			

                                        
                                        
                                         <asp:BoundField HeaderText="RefusalShortfall" DataField="RefusalShortfall" /> 
                                         
                                        
                                        <asp:BoundField HeaderText="CustomerComplaintsTarget" DataField="CustomerComplaintsTarget" /> 
                                         <asp:BoundField HeaderText="CustomerComplaintsAchivedMonth1" DataField="CustomerComplaintsAchivedMonth1" /> 
                                        <asp:BoundField HeaderText="CustomerComplaintsAchivedMonth2" DataField="CustomerComplaintsAchivedMonth2" /> 
                                        
                                       
                                        
                                        <asp:BoundField HeaderText="CustomerComplaintsShortfall" DataField="CustomerComplaintsShortfall" /> 
                                       
                                                            
                                       
                                    </Columns>
                                   <HeaderStyle CssClass="GridviewScrollHeader" /> 
                                   <RowStyle CssClass="GridviewScrollItem" /> 
                                   <PagerStyle CssClass="GridviewScrollPager" /> 
                                                                   
                     </asp:GridView>
                  
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
