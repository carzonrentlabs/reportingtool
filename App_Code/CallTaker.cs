﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using ReportingTool;
using System;

/// <summary>
/// Summary description for CallTaker
/// </summary>
public class CallTaker
{
	public CallTaker()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet GetAllCityName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetAllCityDetails");
    }

    public DataSet GetAllClientName()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Bl_Sp_GetAllClientDetails");
    }
    public DataSet GetWaitingForPaymentDetails(DateTime CreatePickUpDate, int ClientId, int CityId, int CreateOrPickupDate)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CreatePickUpDate", SqlDbType.Date);
        param[0].Value = CreatePickUpDate;
        param[1] = new SqlParameter("@ClientId", SqlDbType.Int);
        param[1].Value = ClientId;
        param[2] = new SqlParameter("@CityId", SqlDbType.Int);
        param[2].Value = CityId;
        param[3] = new SqlParameter("@CreateOrPickupDate", SqlDbType.Int);
        param[3].Value = CreateOrPickupDate;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetWaitingForPaymentDetails", param);

    }
    public DataSet GetPendingForApprovalDetails()
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetStatusOfAllChargingLinks");

    }
    public DataSet GetPendingForApprovalDetailsCritereaWise(DateTime LinkSendDate, int CityId, int ChargingStatus)
    {
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@LinkSendDate", SqlDbType.Date);
        param[0].Value = LinkSendDate;
        param[1] = new SqlParameter("@CityId", SqlDbType.Int);
        param[1].Value = CityId;
        param[2] = new SqlParameter("@ChargingStatus", SqlDbType.Int);
        param[2].Value = ChargingStatus;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SP_GetStatusOfAllChargingLinksCreteriaWise", param);

    }
}