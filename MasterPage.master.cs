using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using ReportingTool;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] == null || Session["Userid"].ToString() == "")
        {
            Response.Redirect("http://insta.carzonrent.com");
        }
        else
        {
            PopulateMenu();
            GetUserName();
        }
    }

    private string GetUserName()
    {
        string UserName = "";

        SqlConnection mycon = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString());
        SqlDataAdapter adusername = new SqlDataAdapter("select  top 1 loginid from dbo.corintsysusersmaster where sysuserid = " + int.Parse(Session["Userid"].ToString()), mycon);
        DataSet dbusername = new DataSet();
        adusername.Fill(dbusername);

        if (dbusername.Tables[0].Rows.Count > 0)
        {
            lblUserID.Text = "Logged In : " + Convert.ToString(dbusername.Tables[0].Rows[0]["loginid"]);
        }

        return UserName;
    }

    private DataSet GetDataSetForMenu()
    {
        SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString());
        SqlDataAdapter adCat = new SqlDataAdapter("exec prc_GetModule_Master " + int.Parse(Session["Userid"].ToString()), myConnection);
        SqlDataAdapter adProd = new SqlDataAdapter("exec prc_GetModule_MenuMaster " + int.Parse(Session["Userid"].ToString()), myConnection);
        DataSet ds = new DataSet();
        adCat.Fill(ds, "CORIntModuleMaster");
        adProd.Fill(ds, "CORIntModuleFunctionMaster");
        ds.Relations.Add("Children",
        ds.Tables["CORIntModuleMaster"].Columns["Moduleid"],
        ds.Tables["CORIntModuleFunctionMaster"].Columns["Moduleid"]);
        return ds;
    }

    private void PopulateMenu()
    {
        DataSet ds = GetDataSetForMenu();
        Menu menu = new Menu();

        foreach (DataRow parentItem in ds.Tables["CORIntModuleMaster"].Rows)
        {
            MenuItem categoryItem = new MenuItem((string)parentItem["ModuleName"]);
            mnuTop.Items.Add(categoryItem);

            //menu.Orientation = "Horizontal";
            foreach (DataRow childItem in parentItem.GetChildRows("Children"))
            {
                MenuItem childrenItem = new MenuItem((string)childItem["FunctionName"]);
                childrenItem.NavigateUrl = childItem["Link"].ToString();
                categoryItem.ChildItems.Add(childrenItem);
            }
        }

        Panel1.Controls.Add(mnuTop);
        Panel1.DataBind();
    }
    protected void back_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Cookies.Clear();
        Response.Redirect("http://insta.carzonrent.com/index/index.asp");
    }
    protected void logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Cookies.Clear();
        Response.Redirect("http://insta.carzonrent.com/");
    }
}
