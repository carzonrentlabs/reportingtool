﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_OwnedCarRevenueReport : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    RentMeterService.wsRentmeter _objService = new RentMeterService.wsRentmeter();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    private void BindBrachDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "UnitName";
                    ddlBranch.DataValueField = "UnitId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }


    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindOwnedCarRepprt();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            int counter = 0;
            DataTable dt = new DataTable();
            int BranchId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            int ProductId = Convert.ToInt32(ddlProduct.SelectedValue.ToString() == null || ddlProduct.SelectedValue.ToString() == "" ? "0" : ddlProduct.SelectedValue.ToString());
            DateTime _Fromdate = Convert.ToDateTime(txtFromDate.Text);
            DateTime _ToDate = Convert.ToDateTime(txtToDate.Text);

            DataSet objds = new DataSet();
            objds = objCordrive.GetOwnedCarReportData(BranchId, _Fromdate, _ToDate, ProductId);
            if (objds != null)
            {
                if (objds.Tables[0].Rows.Count > 0)
                {
                    dt = objds.Tables[0];
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

                        Response.ClearContent();
                        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                        Response.Clear();
                        Response.AppendHeader("content-disposition", "attachment;filename=OwnedCarReport.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "application/vnd.ms-excel";
                        this.EnableViewState = false;
                        Response.Write("\r\n");
                        Response.Write("<table border = '1' align = 'center'> ");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            counter = counter + 1;
                            if (i == 0)
                            {
                                Response.Write("<tr>");
                                Response.Write("<td align='left'><b>S.No.</b></td>");
                                for (int j = 0; j < iColumns.Length; j++)
                                {

                                    if (dt.Columns[iColumns[j]].Caption.ToString() == "CarNumber")
                                    {
                                        Response.Write("<td align='left'><b>Car Number</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ChauffeurName")
                                    {
                                        Response.Write("<td align='left'><b>Chauffeur Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "ChauffeurContractNo")
                                    {
                                        Response.Write("<td align='left'><b>Chauffeur Contact</b></td>");
                                    }

                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "Category")
                                    {
                                        Response.Write("<td align='left'><b>Category</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "Model")
                                    {
                                        Response.Write("<td align='left'><b>Model</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DateofActivation")
                                    {
                                        Response.Write("<td align='left'><b>Date of Activation</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "Branch")
                                    {
                                        Response.Write("<td align='left'><b>Branch</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "Product")
                                    {
                                        Response.Write("<td align='left'><b>Prodcut</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "KMRunMTD")
                                    {
                                        Response.Write("<td align='left'><b>KM Run MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "BilledKMMTD")
                                    {
                                        Response.Write("<td align='left'><b>Billed KM MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "BilledHoursMTD")
                                    {
                                        Response.Write("<td align='left'><b>Billed Hours MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DutiesDoneMTD")
                                    {
                                        Response.Write("<td align='left'><b>Duties Done MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "RevenueDoneMTD")
                                    {
                                        Response.Write("<td align='left'><b>Revenue Done MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "OPenDSMTD")
                                    {
                                        Response.Write("<td align='left'><b>Open DS MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "NoofDayswithZeroDispatch")
                                    {
                                        Response.Write("<td align='left'><b>No of Days with Zero Dispatch</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CompactDutiesdone")
                                    {
                                        Response.Write("<td align='left'><b>Compact Duties done</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "EconomyDutiesdone")
                                    {
                                        Response.Write("<td align='left'><b>Economy Duties done</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "IntermediateDutiesdone")
                                    {
                                        Response.Write("<td align='left'><b>Intermediate Duties done</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "StandardDutiesdone")
                                    {
                                        Response.Write("<td align='left'><b>Standard Duties done</b></td>");
                                    }

                                }
                                Response.Write("</tr>");
                            }

                            Response.Write("<tr>");
                            Response.Write("<td align='left'>" + counter + "</td>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                Response.Write("<td align='left'>" + Convert.ToString(dt.Rows[i][iColumns[j]]) + "</td>");
                            }
                        }
                        Response.Write("</tr>");
                    }

                    Response.Write("</table>");
                    Response.End();
                }
                else
                {

                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();

                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void lnkPrev_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum -= 1;
            BindOwnedCarRepprt();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    private void BindOwnedCarRepprt()
    {
        try
        {
            int _count = 0, _v_count = 0;
            int BranchId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            int ProductId = Convert.ToInt32(ddlProduct.SelectedValue.ToString() == null || ddlProduct.SelectedValue.ToString() == "" ? "0" : ddlProduct.SelectedValue.ToString());
            DateTime _Fromdate = Convert.ToDateTime(txtFromDate.Text);
            DateTime _ToDate = Convert.ToDateTime(txtToDate.Text);

            DataSet objds = new DataSet();
            objds = objCordrive.GetOwnedCarReportData(BranchId, _Fromdate, _ToDate, ProductId);
            if (objds != null)
            {
                if (objds.Tables[0].Rows.Count > 0)
                {
                    dvPaging.Visible = true;
                    _count = objds.Tables[0].Rows.Count;
                    PagedDataSource objPage_data = new PagedDataSource();
                    objPage_data.DataSource = objds.Tables[0].DefaultView;
                    objPage_data.AllowPaging = true;
                    objPage_data.PageSize = 100;
                    objPage_data.CurrentPageIndex = _PgNum;

                    _v_count = _count / objPage_data.PageSize;
                    if (_PgNum < 1)
                    {
                        lnkPrev.Visible = false;
                    }
                    else if (_PgNum > 0)
                    {
                        lnkPrev.Visible = true;
                    }
                    else if (_PgNum < _v_count)
                    {
                        lnkNext.Visible = true;
                    }
                    rptOwnedCarDetailedReport.DataSource = objPage_data;
                    rptOwnedCarDetailedReport.DataBind();
                }

                else
                {
                    dvPaging.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum += 1;
            BindOwnedCarRepprt();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    public int _PgNum
    {
        get
        {
            if (ViewState["PageNum"] != null)
            {
                return Convert.ToInt32(ViewState["PageNum"].ToString());
            }
            else
            {
                return 0;
            }
        }
        set
        {
            ViewState["PageNum"] = value;
        }
    }
}