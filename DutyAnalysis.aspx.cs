using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class DutyAnalysis : clsPageAuthorization
//public partial class DutyAnalysis : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //BindCity();
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }

    //protected void BindCity()
    //{
    //    ddlCityName.DataTextField = "CityName";
    //    ddlCityName.DataValueField = "CityID";
    //    DataSet dsLocation = new DataSet();
    //    dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
    //    //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
    //    ddlCityName.DataSource = dsLocation;
    //    ddlCityName.DataBind();
    //}

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        //int cityid;
        //cityid = 0;
        //cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=DutyAllocationDetail.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetDutiesAnalysis(FromDate, Todate);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 1, 2, 30, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    Response.Write("<td align='left' colspan='5'><b></b></td>");
                    Response.Write("<td align='center' colspan='4'><b>System Allocation count</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>De-allocated count</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>Net duties done against system allocation</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>duties done against any other car allocation</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>Duties done - no system allocation</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>Total duties done</b></td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "RegnNo")
                        {
                            Response.Write("<td align='left'><b>Car No</b></td>");
                        }
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Grade")
                        {
                            Response.Write("<td align='left'><b>Grade</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarCatName")
                        {
                            Response.Write("<td align='left'><b>Category</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName")
                        {
                            Response.Write("<td align='left'><b>Dedicated Car Location</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CurrentStatus")
                        {
                            Response.Write("<td align='left'><b>Current Status</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SystemAllocationD")
                        {
                            Response.Write("<td align='left'><b>Delhi</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SystemAllocationG")
                        {
                            Response.Write("<td align='left'><b>Gurgaon</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SystemAllocationN")
                        {
                            Response.Write("<td align='left'><b>Noida</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SystemAllocationA")
                        {
                            Response.Write("<td align='left'><b>Airport</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeAllocationD")
                        {
                            Response.Write("<td align='left'><b>Delhi</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeAllocationG")
                        {
                            Response.Write("<td align='left'><b>Gurgaon</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeAllocationN")
                        {
                            Response.Write("<td align='left'><b>Noida</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DeAllocationA")
                        {
                            Response.Write("<td align='left'><b>Airport</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NetDutySystemAllocationD")
                        {
                            Response.Write("<td align='left'><b>Delhi</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NetDutySystemAllocationG")
                        {
                            Response.Write("<td align='left'><b>Gurgaon</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NetDutySystemAllocationN")
                        {
                            Response.Write("<td align='left'><b>Noida</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NetDutySystemAllocationA")
                        {
                            Response.Write("<td align='left'><b>Airport</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OtherDutySystemAllocationD")
                        {
                            Response.Write("<td align='left'><b>Delhi</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OtherDutySystemAllocationG")
                        {
                            Response.Write("<td align='left'><b>Gurgaon</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OtherDutySystemAllocationN")
                        {
                            Response.Write("<td align='left'><b>Noida</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OtherDutySystemAllocationA")
                        {
                            Response.Write("<td align='left'><b>Airport</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSystemAllocationD")
                        {
                            Response.Write("<td align='left'><b>Delhi</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSystemAllocationG")
                        {
                            Response.Write("<td align='left'><b>Gurgaon</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSystemAllocationN")
                        {
                            Response.Write("<td align='left'><b>Noida</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NotSystemAllocationA")
                        {
                            Response.Write("<td align='left'><b>Airport</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDutiesD")
                        {
                            Response.Write("<td align='left'><b>Delhi</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDutiesG")
                        {
                            Response.Write("<td align='left'><b>Gurgaon</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDutiesN")
                        {
                            Response.Write("<td align='left'><b>Noida</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDutiesA")
                        {
                            Response.Write("<td align='left'><b>Airport</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        //int cityid;
        //cityid = 0;
        //cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetDutiesAnalysis(FromDate, Todate);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }

    protected void grvMergeHeader_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell HeaderCell = new TableCell();

            HeaderCell.ColumnSpan = 5;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "System Allocation count";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "De-allocated count";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Net duties done against system allocation";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "duties done against any other car allocation";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Duties done - no system allocation";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "Total duties done";
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);
        }
    }
}