﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BranchChauffeurList.aspx.cs" Inherits="CorDriveReport_BranchChauffeurList" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=gvChauffuer.ClientID%> a[id*='btnResume']").click(function () {
                //debugger;
                //alert("Hi All ");
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnResume", "hdResume")
                var hdImageID = $("#" + hdImageID).val();
                // alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/" + hdImageID + "/" + "Resume.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/39575/" + "Resume.pdf";
                    //alert(url)
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });
        $(function () {
            $("#<%=gvChauffuer.ClientID%> a[id*='btnPassport']").click(function () {
                //debugger;
                //alert("passport");
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnPassport", "hdpassport")
                var hdImageID = $("#" + hdImageID).val();
                //alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/" + hdImageID + "/" + "Resume.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/39575/" + "Resume.pdf";
                    //alert(url)
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });
        $(function () {
            $("#<%=gvChauffuer.ClientID%> a[id*='btnLicence']").click(function () {
                //debugger;
                //alert("Hi All ");
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnLicence", "hdLicence")
                var hdImageID = $("#" + hdImageID).val();
                // alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/" + hdImageID + "/" + "ValidCDLWE.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/40245/" + "ValidCDLWE.pdf";
                    //alert(url)
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });

        $(function () {
            $("#<%=gvChauffuer.ClientID%> a[id*='btnPolice']").click(function () {
                //debugger;
                //alert("Hi All ");
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnPolice", "hdPolice")
                var hdImageID = $("#" + hdImageID).val();
                // alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/" + hdImageID + "/" + "PoliceVerif.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/40245/" + "ValidCDLWE.pdf";
                    //alert(url)
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });

        $(function () {
            $("#<%=gvChauffuer.ClientID%> a[id*='btnPhotograph']").click(function () {
                //debugger;
                //alert("Hi All ");
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnPhotograph", "hdPhotograph")
                var hdImageID = $("#" + hdImageID).val();
                // alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/" + hdImageID + "/" + "PoliceVerif.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/40245/" + "ValidCDLWE.pdf";
                    alert("Document Not Exists");
                    return;
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });
    </script>
    <center>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 100%" align="center">
                <tr>
                    <td align="center" colspan ="12">
                        <b>Branch Chauffeur List</b>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblBranch" runat="server">Branch</asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlBranch" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblStatus" runat="server">Status</asp:Label>&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                            <asp:ListItem Value="2" Text="DeActive"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        &nbsp;<asp:Label ID="lblCarModel" runat="server">Model</asp:Label>&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlCarModel" runat="server" Enabled ="false">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        &nbsp;<asp:Label ID="lblVDP" runat="server">VDP (Y/N)</asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlVdpYN" runat="server" Enabled ="false">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="2" Text="No"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblChauffeur_Source" runat="server">Chauffeur Source</asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlChauffeurSource" runat="server">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Vendor"></asp:ListItem>
                            <asp:ListItem Value="2" Text="COR"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                    </td>
                    <td align="left">
                        <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="Repeater" align="center">
            <asp:GridView ID="gvChauffuer" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                PageSize="100" OnPageIndexChanging="rvChauffuer_PageIndexChanging" OnRowDataBound="gvChauffuer_RowDataBound" CellPadding ="2">
                <Columns>
                    <asp:TemplateField HeaderText="Sr.No" HeaderStyle-Width="10px">
                        <ItemTemplate>
                            <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.DataItemIndex+1  %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Chauffeur Name" DataField="ChauffeurName" />
                    <asp:BoundField HeaderText="Chauffeur Contact" DataField="MobileNo" />
                    <asp:BoundField HeaderText="Vendor Name" DataField="CarVendorName" />
                    <asp:BoundField HeaderText="Vendor Contact" DataField="VendorContractNo" />
                    <asp:TemplateField HeaderText="Chauffeur Source">
                        <ItemTemplate>
                            <asp:Label ID="lblChaufferSource" runat="server" Text='<%#Eval("ChauffeurSource")  %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Alternate ID Proof" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnPassport" runat="server" Text="View"></asp:LinkButton>
                            <asp:HiddenField ID="hdpassport" runat="server" Value='<%#Eval("VendorChauffeurID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Drivers Licence" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnLicence" runat="server" Text="View"></asp:LinkButton>
                            <asp:HiddenField ID="hdLicence" runat="server" Value='<%#Eval("VendorChauffeurID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Police Varification" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnPolice" runat="server" Text="View"></asp:LinkButton>
                            <asp:HiddenField ID="hdPolice" runat="server" Value='<%#Eval("VendorChauffeurID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Resume" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnResume" runat="server" Text="View"></asp:LinkButton>
                            <asp:HiddenField ID="hdResume" runat="server" Value='<%#Eval("VendorChauffeurID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Photograph" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnPhotograph" runat="server" Text="View"></asp:LinkButton>
                            <asp:HiddenField ID="hdPhotograph" runat="server" Value='<%#Eval("VendorChauffeurID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Associated CarNo" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_CarNo" runat="server" Text='<%# Eval("AssociatedcarNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Of Activation" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_ActivationDate" runat="server" Text='<%# Eval("DateofActivation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document UploadedBy" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DocumentUploadedBy" runat="server" Text='<%# Eval("DocumentUploadedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Doument Approved by" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DocumentApprovedBy" runat="server" Text='<%# Eval("DocumentApprovedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date of Modification" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_DateofModification" runat="server" Text='<%# Eval("DateofModification") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_chaufStatus" runat="server" Text='<%# Eval("chaufStatus") %>'></asp:Label>
                            <asp:HiddenField ID="hdndocument" runat="server" Value='<%#Eval("DocumentUploadYN")  %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:BoundField DataField ="CityName"  HeaderText ="Location" />

                </Columns>
            </asp:GridView>
        </div>
    </center>
</asp:Content>
