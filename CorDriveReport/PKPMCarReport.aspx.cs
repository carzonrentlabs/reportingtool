﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_PKPMCarReport : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCityDropDownlist();
            BindCarModelDropDowmnList();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    #region "Private Function For DropDownlist"
    private void BindCityDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation.Tables[0];
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    private void BindCarModelDropDowmnList()
    {
        try
        {
            DataSet _objDScarModel = new DataSet();
            _objDScarModel = objCordrive.GetActiveCarModel();
            if (_objDScarModel != null)
            {
                if (_objDScarModel.Tables.Count > 0)
                {
                    ddlCarModel.DataTextField = "ModelName";
                    ddlCarModel.DataValueField = "ModelId";
                    ddlCarModel.DataSource = _objDScarModel.Tables[0];
                    ddlCarModel.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }


    private void BindPKPMCarReport()
    {
        try
        {
            int _carModelId = 0, _cityId = 0, _ddlVdpYN = 0, _count = 0, _v_count = 0;
            DateTime _fromdate, _todate;
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _carModelId = Convert.ToInt32(ddlCarModel.SelectedValue.ToString() == null || ddlCarModel.SelectedValue.ToString() == "" ? "0" : ddlCarModel.SelectedValue.ToString());
            _cityId = Convert.ToInt32(ddlCity.SelectedValue.ToString() == null || ddlCity.SelectedValue.ToString() == "" ? "0" : ddlCity.SelectedValue.ToString());
            _ddlVdpYN = Convert.ToInt32(ddlVdpYN.SelectedValue.ToString() == null || ddlVdpYN.SelectedValue.ToString() == "" ? "0" : ddlVdpYN.SelectedValue.ToString());

            DataSet _objDs = new DataSet();
            _objDs = objCordrive.GetPKPMData(_cityId, _fromdate, _todate, 0, _carModelId, _ddlVdpYN);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dvPaging.Visible = true;
                    _count = _objDs.Tables[0].Rows.Count;
                    PagedDataSource objPage_data = new PagedDataSource();
                    objPage_data.DataSource = _objDs.Tables[0].DefaultView;
                    objPage_data.AllowPaging = true;
                    objPage_data.PageSize = 100;
                    objPage_data.CurrentPageIndex = _PgNum;
                    _v_count = _count / objPage_data.PageSize;
                    if (_PgNum < 1)
                    {
                        lnkPrev.Visible = false;
                    }
                    else if (_PgNum > 0)
                    {
                        lnkPrev.Visible = true;
                    }
                    else if (_PgNum < _v_count)
                    {
                        lnkNext.Visible = true;
                    }

                    rptPKPM.DataSource = objPage_data;
                    rptPKPM.DataBind();
                }
                else
                {
                    Visible = false;
                }

            }
        }
        catch (Exception ex)
        {

            ex.Message.ToString();
        }
    }

    #endregion
    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindPKPMCarReport();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {
            int _carModelId = 0, _cityId = 0, _ddlVdpYN = 0, _count = 0, _v_count = 0;
            DateTime _fromdate, _todate;
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _carModelId = Convert.ToInt32(ddlCarModel.SelectedValue.ToString() == null || ddlCarModel.SelectedValue.ToString() == "" ? "0" : ddlCarModel.SelectedValue.ToString());
            _cityId = Convert.ToInt32(ddlCity.SelectedValue.ToString() == null || ddlCity.SelectedValue.ToString() == "" ? "0" : ddlCity.SelectedValue.ToString());
            _ddlVdpYN = Convert.ToInt32(ddlVdpYN.SelectedValue.ToString() == null || ddlVdpYN.SelectedValue.ToString() == "" ? "0" : ddlVdpYN.SelectedValue.ToString());

            DataSet _objDs = new DataSet();
            DataTable dt = new DataTable();
            _objDs = objCordrive.GetPKPMData(_cityId, _fromdate, _todate, 0, _carModelId, _ddlVdpYN);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Response.ClearContent();
                        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                        Response.Clear();
                        Response.AppendHeader("content-disposition", "attachment;filename=PKPMCarReport.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "application/vnd.ms-excel";
                        this.EnableViewState = false;
                        Response.Write("\r\n");
                        Response.Write("<table border = '1' align = 'center'> ");
                        Response.Write("<tr><td align='center' colspan='5'>PKPM Car Reports<b></b></td></tr>");
                        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                Response.Write("<tr>");
                                for (int j = 0; j < iColumns.Length; j++)
                                {
                                    if (dt.Columns[iColumns[j]].Caption.ToString() == "Id")
                                    {
                                        Response.Write("<td align='left'><b>S.No.</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "RegNo")
                                    {
                                        Response.Write("<td align='left'><b>Car No</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "Location")
                                    {
                                        Response.Write("<td align='left'><b>City Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarCatName")
                                    {
                                        Response.Write("<td align='left'><b>Car Category</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarModelName")
                                    {
                                        Response.Write("<td align='left'><b>Model Name</b></td>");

                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "VendorName")
                                    {
                                        Response.Write("<td align='left'><b>Vendor Name</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "AttachedModel")
                                    {
                                        Response.Write("<td align='left'><b>Attachment Model</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "VDPYN")
                                    {
                                        Response.Write("<td align='left'><b>VDP (Y/N)</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "TotalLoginHr")
                                    {
                                        Response.Write("<td align='left'><b>Total Login Hours</b></td>");
                                    }

                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "TotalKMUsed")
                                    {
                                        Response.Write("<td align='left'><b>Total Billed KM</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "TotalHrUsed")
                                    {
                                        Response.Write("<td align='left'><b>Total Billed Hours</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DutyDoneMTD")
                                    {
                                        Response.Write("<td align='left'><b>Duties Done MTD</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "DutyCompleteCorDrive")
                                    {
                                        Response.Write("<td align='left'><b>Duties Compleated on COR Drive</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "Revenue")
                                    {
                                        Response.Write("<td align='left'><b>Total Billed Revenue</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "OpenDuties")
                                    {
                                        Response.Write("<td align='left'><b>Open Duties</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "TargetRevenue")
                                    {
                                        Response.Write("<td align='left'><b>Target Revenue</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "shortRevenue")
                                    {
                                        Response.Write("<td align='left'><b>Short Fall / Excess</b></td>");
                                    }
                                    else if (dt.Columns[iColumns[j]].Caption.ToString() == "CarStatus")
                                    {
                                        Response.Write("<td align='left'><b>Car Status</b></td>");
                                    }


                                }
                                Response.Write("</tr>");
                            }
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                            }
                            Response.Write("</tr>");
                        }
                        Response.Write("</table>");
                        Response.End();


                    }
                    else
                    {

                        Response.Write("<table border = 1 align = 'center' width = '100%'>");
                        Response.Write("<td align='center'><b>No Record Found</b></td>");
                        Response.Write("</table>");
                        Response.End();



                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    public int _PgNum
    {
        get
        {
            if (ViewState["PageNum"] != null)
            {
                return Convert.ToInt32(ViewState["PageNum"].ToString());
            }
            else
            {
                return 0;
            }
        }
        set
        {
            ViewState["PageNum"] = value;
        }
    }

    protected void lnkPrev_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum -= 1;
            BindPKPMCarReport();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum += 1;
            BindPKPMCarReport();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
}