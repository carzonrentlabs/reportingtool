using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReportingTool;

public partial class VDPReportBranchPerformanceReportCrystal : System.Web.UI.Page
{
    
    int strcityid;
    string fromDate, toDate;
    clsAdmin objAdmin = new clsAdmin();

    ReportDocument VendorUtilizationSummary = new ReportDocument();

    ParameterFields collectionParam = new ParameterFields();

    //ParameterField paramdays = new ParameterField();
    //ParameterDiscreteValue paramDVdays = new ParameterDiscreteValue();

    ParameterField paramFrom = new ParameterField();
    ParameterDiscreteValue paramDVFrom = new ParameterDiscreteValue();

    ParameterField paramTo = new ParameterField();
    ParameterDiscreteValue paramDVTo = new ParameterDiscreteValue();

    ParameterField paramcityid = new ParameterField();
    ParameterDiscreteValue paramDVcityid = new ParameterDiscreteValue();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (Request.QueryString["val"] != null)
            {
                string prms = Request.QueryString["val"];
                string[] prm = prms.Split(',');
                fromDate = Convert.ToString(prm[0]);
                toDate = Convert.ToString(prm[1]);
                strcityid = Convert.ToInt32(prm[2]);
                bindReport();
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
    }

    private void bindReport()
    {


        paramcityid.Name = "@cityid";
        paramDVcityid.Value = strcityid.ToString();
        paramcityid.CurrentValues.Add(paramDVcityid);

        //paramdays.Name = "@NoOfDays";
        //paramDVdays.Value = days;
        //paramdays.CurrentValues.Add(paramDVdays);

        paramFrom.Name = "@fromdate";
        paramDVFrom.Value = fromDate;
        paramFrom.CurrentValues.Add(paramDVFrom);

        paramTo.Name = "@toDate";
        paramDVTo.Value = toDate;
        paramTo.CurrentValues.Add(paramDVTo);

        collectionParam.Add(paramFrom);
        collectionParam.Add(paramTo);
        collectionParam.Add(paramcityid);

        CrystalReportViewer1.ParameterFieldInfo = collectionParam;
        VendorUtilizationSummary.Load(Server.MapPath("VDP-Daywise-Attendance.rpt"));
        VendorUtilizationSummary.SetDatabaseLogon(objAdmin.dbUser, objAdmin.dbPWD);
        CrystalReportViewer1.ReportSource = VendorUtilizationSummary;
        CrystalReportViewer1.DataBind();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        VendorUtilizationSummary.Close();
        VendorUtilizationSummary.Dispose();
    }
}