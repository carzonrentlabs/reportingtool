<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DutyAllocationDetail.aspx.cs" Inherits="DutyAllocationDetail" Title="CarzonRent :: Internal software" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="ajax" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="JQuery/moment.js" type="text/javascript"></script>

    <script src="JQuery/ui.core.js" type="text/javascript"></script>

    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script src="ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $("#<%=txtFrom.ClientID%>").datepicker();
        $("#<%=txtTo.ClientID%>").datepicker();

       });
    </script>

    <table width="802" border="0" align="center">
        <tbody>
            <tr>
                <td align="center" colspan="8">
                    <strong><u>Duties Allocation Detail</u></strong>
                </td>
            </tr>
            <tr>
                <td align="right">
                    From Date&nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="rvFromDate" SetFocusOnError="True" ValidationGroup="FS"
                        runat="server" ErrorMessage="Please Enter From Date" ControlToValidate="txtFrom"
                        Display="None"></asp:RequiredFieldValidator>
                </td>
                <td align="right">
                    To Date&nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="rvToDate" SetFocusOnError="True" ValidationGroup="FS"
                        runat="server" ErrorMessage="Please Enter From Date" ControlToValidate="txtTo"
                        Display="None"></asp:RequiredFieldValidator>
                </td>
                <td align="right">
                    City Name:</td>
                <td>
                    <asp:DropDownList ID="ddlCityName" runat="server" Width="174px">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Go" OnClick="btnSubmit_Click" />
                    &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export To Excel" OnClick="btnExport_Click" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="8">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="8">
                    <asp:GridView runat="server" ID="GridView1" PageSize="50" EmptyDataText="No Record Found"
                        AutoGenerateColumns="False" AllowPaging="True" BackColor="LightGoldenrodYellow"
                        BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="Both"
                        OnPageIndexChanging="GridView1_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Booking ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingID" Text='<% #Bind("BookingID") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pickup Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickupDate" Text='<% #Bind("PickupDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="De- allocated Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeallocatedDate" Text='<% #Bind("DeallocatedDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Executive Name (De-allocated)">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeallocatedName" Text='<% #Bind("DeallocatedName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="De-allocation Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeallocationRemarks" Text='<% #Bind("DeallocationRemarks") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCityName" Text='<% #Bind("CityName") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Allocated Vehicle No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblAllocatedCar" Text='<% #Bind("AllocatedCar") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reallocated Vehicle  No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblCarNo" Text='<% #Bind("CarNo") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Change Reason">
                                <ItemTemplate>
                                    <asp:Label ID="lblChangeReason" Text='<% #Bind("ChangeReason") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Outstation">
                                <ItemTemplate>
                                    <asp:Label ID="lblOutstation" Text='<% #Bind("OustationYN") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingCreateDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingCreateDate" Text='<% #Bind("BookingCreateDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DutyAllocationDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblDutyAllocationDate" Text='<% #Bind("DutyAllocationDate") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AssignBeforeTimeInMins">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssignBeforeTimeInMins" Text='<% #Bind("AssignBeforeTimeInMins") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="Tan" />
                        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Tan" Font-Bold="True" />
                        <AlternatingRowStyle BackColor="PaleGoldenrod" />
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
