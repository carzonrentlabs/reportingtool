using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;
using ExcelUtil;
public partial class CorDriveReport_LoginSummaryAreaWise : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsLoginSummaryCityWise = new DataSet();
    DataSet dsExportToExcel = new DataSet();
    DataSet dsAreaWiseHome = new DataSet();
    DataSet dsAreaWiseCurrent = new DataSet();
    DataSet dsAreaVersionSummary = new DataSet();
    DataSet dsSummaryCityWise = new DataSet();
    public string Location = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (rdbcityArea.SelectedIndex == 0)
        {
            trArea.Visible = true;
        }
        else
        {
            trArea.Visible =false;
        }
        lblMessage.Text = string.Empty;
        //lblLocation.Text = string.Empty;
    }
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(500);
        try
        {
            if (ddlCity.SelectedIndex != 0)
            {
                dsLoginSummaryCityWise = objCordrive.GetLoginSummaryReportCityWise(ddlCity.SelectedItem.Text.ToString());
                if (dsLoginSummaryCityWise.Tables[0].Rows.Count > 0)
                {
                    grdLoginSummaryCityWise.DataSource = dsLoginSummaryCityWise.Tables[0];
                    grdLoginSummaryCityWise.DataBind();

                    grdActivityCityWise.DataSource = dsLoginSummaryCityWise.Tables[1];
                    grdActivityCityWise.DataBind();
                    grdRptAreaVersionSummary.DataSource = dsLoginSummaryCityWise.Tables[2];
                    grdRptAreaVersionSummary.DataBind();
                    
                    grdAreaWiseHome.DataSource = null;
                    grdAreaWiseHome.DataBind();

                    grdAreaWiseCurrent.DataSource = null;
                    grdAreaWiseCurrent.DataBind();
                   // BidAreaVersionSummary();
                }
                else
                {
                    grdLoginSummaryCityWise.DataSource = null;
                    grdLoginSummaryCityWise.DataBind();

                    grdActivityCityWise.DataSource = null;
                    grdActivityCityWise.DataBind();

                    grdAreaWiseHome.DataSource = null;
                    grdAreaWiseHome.DataBind();

                    grdRptAreaVersionSummary.DataSource = null;
                    grdRptAreaVersionSummary.DataBind();

                    grdAreaWiseCurrent.DataSource = null;
                    grdAreaWiseCurrent.DataBind();

                    lblMessage.Text = "Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Please select City.')", true);
                return;
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        if (ddlCity.SelectedIndex != 0)
        {
            objCordrive = new CorDrive();
            //string CityName = ddlCity.SelectedItem.Text.ToString();
            dsExportToExcel = objCordrive.GetLoginSummaryExportToExcel(ddlCity.SelectedItem.Text.ToString());
            WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "LoginActivityReport.xls");
        }
    }
    protected void rdbcityArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdbcityArea.SelectedIndex == 0)
        {
            trArea.Visible = true;
            trCity.Visible = false;
            grdAreaWiseCurrent.DataSource = null;
            grdAreaWiseCurrent.DataBind();

            grdAreaWiseHome.DataSource = null;
            grdAreaWiseHome.DataBind();

            grdLoginSummaryCityWise.DataSource = null;
            grdLoginSummaryCityWise.DataBind();

            grdActivityCityWise.DataSource = null;
            grdActivityCityWise.DataBind();

            grdRptAreaVersionSummary.DataSource = null;
            grdRptAreaVersionSummary.DataBind();

            
        }
        if (rdbcityArea.SelectedIndex == 1)
        {
            trCity.Visible = true;
            trArea.Visible = false;

            grdAreaWiseCurrent.DataSource = null;
            grdAreaWiseCurrent.DataBind();

            grdAreaWiseHome.DataSource = null;
            grdAreaWiseHome.DataBind();

            grdLoginSummaryCityWise.DataSource = null;
            grdLoginSummaryCityWise.DataBind();

            grdActivityCityWise.DataSource = null;
            grdActivityCityWise.DataBind();

            grdRptAreaVersionSummary.DataSource = null;
            grdRptAreaVersionSummary.DataBind();


        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
            objCordrive = new CorDrive();
            string bk = txtloation.Text;
            try
            {
                if(city_location_lat.Value.ToString()!="")
                {
                    if (ddlCabLocation.SelectedItem.Text.ToString() == "Home Location")
                    {
                        BindAreaWiseHome(city_location_lat.Value.ToString(), city_location_long.Value.ToString(), Convert.ToInt32(ddlRadius.SelectedItem.Text), Convert.ToInt32(ddlCategory.SelectedValue));
                    }
                    else
                    {

                        BindAreaWiseCurrent(city_location_lat.Value.ToString(), city_location_long.Value.ToString(), Convert.ToInt32(ddlRadius.SelectedItem.Text), Convert.ToInt32(ddlCategory.SelectedValue));
                    }
                }
               
                Location = bk.ToString();
                lblLocation.Text = Location.ToString();
            

            }
            catch (Exception Ex)
            {
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = Ex.Message;
            }
           
        }
    public void BindAreaWiseCurrent(string Latitude, string Longitude,int Radius,int CategoryId)
    {
        objCordrive = new CorDrive();
        try
        {
            dsAreaWiseCurrent = objCordrive.GetAreaWiseCurrent(Latitude, Longitude, Radius, CategoryId);
            if (dsAreaWiseCurrent.Tables[0].Rows.Count > 0)
            {
               
                grdAreaWiseCurrent.DataSource = dsAreaWiseCurrent.Tables[0];
                grdAreaWiseCurrent.DataBind();

                grdAreaWiseHome.DataSource = null;
                grdAreaWiseHome.DataBind();

                grdLoginSummaryCityWise.DataSource = null;
                grdLoginSummaryCityWise.DataBind();

                grdActivityCityWise.DataSource = null;
                grdActivityCityWise.DataBind();

                grdRptAreaVersionSummary.DataSource = null;
                grdRptAreaVersionSummary.DataBind();

            }
            else
            {
                grdAreaWiseCurrent.DataSource = null;
                grdAreaWiseCurrent.DataBind();

                grdAreaWiseHome.DataSource = null;
                grdAreaWiseHome.DataBind();

                grdLoginSummaryCityWise.DataSource = null;
                grdLoginSummaryCityWise.DataBind();

                grdActivityCityWise.DataSource = null;
                grdActivityCityWise.DataBind();

                grdRptAreaVersionSummary.DataSource = null;
                grdRptAreaVersionSummary.DataBind();

                lblMessage.Text = "Record not available.";
                lblMessage.Visible = true;
                lblMessage.ForeColor = Color.Red;
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }

    }
    public void BindAreaWiseHome(string Latitude, string Longitude, int Radius,int CategoryId)
    {
        objCordrive = new CorDrive();
        string bk = txtloation.Text;
        try
        {
            if (city_location_lat.Value.ToString() != "")
            {
                if (ddlCabLocation.SelectedItem.Text.ToString() == "Home Location")
                {
                    dsAreaWiseHome = objCordrive.GetAreaWiseHome(Latitude, Longitude, Radius, CategoryId);
                    if (dsAreaWiseHome.Tables[0].Rows.Count > 0)
                    {
                        grdAreaWiseHome.DataSource = dsAreaWiseHome.Tables[0];
                        grdAreaWiseHome.DataBind();
                                               

                        grdAreaWiseCurrent.DataSource = null;
                        grdAreaWiseCurrent.DataBind();

                        grdLoginSummaryCityWise.DataSource = null;
                        grdLoginSummaryCityWise.DataBind();

                        grdActivityCityWise.DataSource = null;
                        grdActivityCityWise.DataBind();

                        grdRptAreaVersionSummary.DataSource = null;
                        grdRptAreaVersionSummary.DataBind();

                    }
                    else
                    {
                        grdAreaWiseHome.DataSource = null;
                        grdAreaWiseHome.DataBind();
                        
                        grdAreaWiseCurrent.DataSource = null;
                        grdAreaWiseCurrent.DataBind();

                        grdLoginSummaryCityWise.DataSource = null;
                        grdLoginSummaryCityWise.DataBind();

                        grdActivityCityWise.DataSource = null;
                        grdActivityCityWise.DataBind();

                        grdRptAreaVersionSummary.DataSource = null;
                        grdRptAreaVersionSummary.DataBind();

                        lblMessage.Text = "Record not available.";
                        lblMessage.Visible = true;
                        lblMessage.ForeColor = Color.Red;
                    }
                }
               
            }
            Location = bk.ToString();
            lblLocation.Text = Location.ToString();
          
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    
    }
}
    
   

    

