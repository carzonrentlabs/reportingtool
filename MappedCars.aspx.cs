using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ReportingTool;

public partial class MappedCars : System.Web.UI.Page
{
    CorDrive Crdr = new CorDrive();
    clsAdmin objAdmin = new clsAdmin();
    string msgComp = string.Empty;
    DataSet ds = new DataSet();
    int RegNo_Index, TotalCount_Index, LastUpdated_Index, TotalCount_Value, BranchId_Index, BranchId_Value, CityID_Index, CityID_Value, ClientID_Index, ClientID_Value;
    string RegNo_Value, Msg = String.Empty;
    DateTime LastUpdated_Value;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
            BindClient();
            BindServiceType();
            pnlpopup.Visible = false;
            pnlPopUpEdit.Visible = false;
        }
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = Crdr.GetCityName();
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }
    private void BindServiceType()
    {
        ddlServiceType.DataTextField = "ServiceName";
        ddlServiceType.DataValueField = "serviceTypeId";
        DataSet dsSericeType = new DataSet();
        dsSericeType = Crdr.GetServiceTypeName();
        if (dsSericeType.Tables[0].Rows.Count > 0)
        {
            ddlServiceType.DataSource = dsSericeType;
            ddlServiceType.DataBind();
            ddlServiceType.Items.Insert(0, new ListItem("--select--", "0"));
        }
        else
        {
            ddlServiceType.DataSource = null;
            ddlServiceType.DataBind();
        }

    }

    protected void BindClient()
    {
        ddlClientName.DataTextField = "ClientCoName";
        ddlClientName.DataValueField = "ClientCoID";
        DataSet dsClient = new DataSet();
        dsClient = Crdr.GetClientName();
        ddlClientName.DataSource = dsClient;
        ddlClientName.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Text = "";
            pnlpopup.Visible = false;
            pnlPopUpEdit.Visible = false;
            if (ddlCityName.SelectedIndex == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please select city name !');</script>");
                ddlCityName.Focus();
                return;
            }

            if (ddlClientName.SelectedIndex == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please select client name !');</script>");
                ddlClientName.Focus();
                return;
            }

            gridbind();

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }


    private void gridbind()
    {
        DataSet dsMappedCars = new DataSet();
        dsMappedCars = Crdr.GetAddMappedCars(Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlApprovedStatus.SelectedValue));
        if (dsMappedCars.Tables[0].Rows.Count > 0)
        {
            gvMappedCars.DataSource = dsMappedCars.Tables[0];
            gvMappedCars.DataBind();
            btnDeactive.Visible = true;
            lblDeActivateMessage.Visible = true;
            txtBulkDeactiveReason.Visible = true;
        }
        else
        {
            btnDeactive.Visible = false;
            lblDeActivateMessage.Visible = false;
            txtBulkDeactiveReason.Visible = false;
            gvMappedCars.DataSource = null;
            gvMappedCars.DataBind();
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('No Record Found !');</script>");

            return;
        }
    }
    protected void bnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            pnlpopup.Visible = false;
            pnlPopUpEdit.Visible = false;
            lblMessage.Text = "";
            if (ddlCityName.SelectedIndex == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please select city name !');</script>");
                ddlCityName.Focus();
                return;
            }

            if (ddlClientName.SelectedIndex == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please select client name !');</script>");
                ddlClientName.Focus();
                return;
            }

            pnlpopup.Visible = true;
            this.mdPopUp.Enabled = true;
            this.mdPopUp.Show();

            pnlPopUpEdit.Visible = false;
            this.mdPopUpEdit.Enabled = false;
            this.mdPopUpEdit.Hide();

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
        }
    }

    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }

            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }


            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }
            lblMessage.Text = "";
            // btnopenPopup.Visible = true;

        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        InitializeControls(Form);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

        lblErrMsg.Text = "";
        pnlpopup.Visible = false;
        this.mdPopUp.Hide();
        mdPopUp.Enabled = false;
    }
    protected void ImgClose_Click(object sender, ImageClickEventArgs e)
    {

        lblErrMsg.Text = "";
        pnlpopup.Visible = false;
        this.mdPopUp.Hide();
        mdPopUp.Enabled = false;
    }
    protected void gvMappedCars_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvRow = e.Row;
        if (gvRow.RowType == DataControlRowType.DataRow)
        {
            Button btnEdit = (Button)gvRow.FindControl("btnEdit");

        }
    }
    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        lblErrMsg.Text = "";
        pnlPopUpEdit.Visible = false;
        this.mdPopUpEdit.Hide();
        mdPopUpEdit.Enabled = false;
    }
    protected void gvMappedCars_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblMsg.Text = "";
        if (e.CommandName == "select")
        {
            // int ownerid = Convert.ToInt32(e.CommandArgument);
            GridViewRow gvRow = (GridViewRow)(((Button)e.CommandSource).Parent).Parent;
            lblSerialNoEdit.Text = ((Label)gvRow.FindControl("lblActiveValue")).Text;

            lblRegNoEdit.Text = ((Label)gvRow.FindControl("lblRegNo")).Text;



            Label lblDistrict = (Label)gvRow.FindControl("lblActiveValue");
            if (lblDistrict.Text == "0")
            {
                ddlStatusEdit.SelectedValue = Convert.ToString(lblDistrict.Text);
            }
            if (lblDistrict.Text == "1")
            {
                ddlStatusEdit.SelectedValue = Convert.ToString(lblDistrict.Text);
            }


            //if (lblSerialNoEdit.Text == "False")
            //{
            //    ddlStatusEdit.Items.Insert(0, "InActive");
            //    ddlStatusEdit.Items.Insert(1, "Active");
            //    ddlStatusEdit.SelectedItem.Text = ((Label)gvRow.FindControl("lblActive")).Text;
            //    //ddlStatusEdit.SelectedValue = ((Label)gvRow.FindControl("lblActiveValue")).Text;
            //}

            //if (lblSerialNoEdit.Text == "True")
            //{

            //    ddlStatusEdit.Items.Insert(1, "Active");
            //    ddlStatusEdit.Items.Insert(0, "InActive");
            //    ddlStatusEdit.SelectedItem.Text = ((Label)gvRow.FindControl("lblActive")).Text;
            //    //ddlStatusEdit.SelectedValue = ((Label)gvRow.FindControl("lblActiveValue")).Text;
            //}


            //lblStatusEdit.Text = ((Label)gvRow.FindControl("lblActive")).Text;
            //ddlStatusEdit.SelectedItem.Text = ((Label)gvRow.FindControl("lblActive")).Text;

            lblCarIDEdit.Text = ((Label)gvRow.FindControl("lblCarID")).Text;
            txtRemarksEdit.Text = ((Label)gvRow.FindControl("lblRemarks")).Text;

            lblErrMsg.Text = "";

            pnlPopUpEdit.Visible = true;
            this.mdPopUpEdit.Enabled = true;
            this.mdPopUpEdit.Show();

            pnlpopup.Visible = false;
            this.mdPopUp.Enabled = false;
            this.mdPopUp.Hide();

        }

    }



    // ---- GetCellByName ----------------------------------     
    //     
    // pass in a GridViewRow and a database column name      
    // returns a DataControlFieldCell or null     

    public DataControlFieldCell GetCellByName(GridViewRow Row, String CellName)
    {
        foreach (DataControlFieldCell Cell in Row.Cells)
        {
            if (Cell.ContainingField.ToString() == CellName)
                return Cell;
        }
        return null;
    }

    // ---- GetColumnIndexByHeaderText ----------------------------------     
    //     
    // pass in a GridView and a Column's Header Text     
    // returns index of the column if found      
    // returns -1 if not found      

    public int GetColumnIndexByHeaderText(GridView aGridView, String ColumnText)
    {
        TableCell Cell;
        for (int Index = 0; Index < aGridView.HeaderRow.Cells.Count; Index++)
        {
            Cell = aGridView.HeaderRow.Cells[Index];
            if (Cell.Text.ToString() == ColumnText)
                return Index;
        }
        return -1;
    }

    // ---- GetColumnIndexByDBName ----------------------------------     
    //     
    // pass in a GridView and a database field name     
    // returns index of the bound column if found      
    // returns -1 if not found      

    public int GetColumnIndexByDBName(GridView aGridView, String ColumnText)
    {
        System.Web.UI.WebControls.BoundField DataColumn;

        for (int Index = 0; Index < aGridView.Columns.Count; Index++)
        {
            DataColumn = aGridView.Columns[Index] as System.Web.UI.WebControls.BoundField;

            if (DataColumn != null)
            {
                if (DataColumn.DataField == ColumnText)
                    return Index;
            }
        }
        return -1;
    }
    protected void ImgEditClose_Click(object sender, ImageClickEventArgs e)
    {
        lblErrMsg.Text = "";
        pnlPopUpEdit.Visible = false;
        this.mdPopUpEdit.Hide();
        mdPopUpEdit.Enabled = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Text = "";
            pnlpopup.Visible = false;
            pnlPopUpEdit.Visible = false;
            int rslt = 0;
            if (ddlServiceType.SelectedIndex == 0)
            {
                lblMessage.Text = "Select service type !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else if (ddlVendorType.SelectedIndex == 0)
            {
                lblMessage.Text = "Select vendor type !";
                lblMessage.Visible = true;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }

            else
            {
                rslt = Crdr.SaveMappedCar(Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(ddlVendorType.SelectedValue), txtRegNo.Text, txtRemarks.Text, Session["UserID"].ToString(),Convert.ToInt32(ddlServiceType.SelectedValue));
                if (rslt > 0)
                {
                    gridbind();
                    Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Record submitted successfully !');</script>");
                    lblMessage.Text = "Record submitted successfully !";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    txtRemarks.Text = "";
                    ddlVendorType.SelectedIndex = -1;
                    ddlServiceType.SelectedIndex = 0;
                    txtRegNo.Text = "";
                    //ClearControl();
                }
                else if (rslt == -1)
                {
                    lblMessage.Text = "Cannot add same car again !";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    txtRemarks.Text = "";
                    ddlVendorType.SelectedIndex = -1;
                    ddlServiceType.SelectedIndex = 0;
                    txtRegNo.Text = "";
                }
                else if (rslt == -2)
                {
                    lblMessage.Text = "Car registration number not active or exist !";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    txtRemarks.Text = "";
                    ddlVendorType.SelectedIndex = -1;
                    ddlServiceType.SelectedIndex = 0;
                    txtRegNo.Text = "";
                }

                else if (rslt == -3)
                {
                    lblMessage.Text = "Car registration or vendor or chauffeur not active/approved !";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    txtRemarks.Text = "";
                    ddlVendorType.SelectedIndex = -1;
                    ddlServiceType.SelectedIndex = 0;
                    txtRegNo.Text = "";
                }

                else if (rslt == -4)
                {
                    lblMessage.Text = "Record not saved !";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    txtRemarks.Text = "";
                    ddlVendorType.SelectedIndex = -1;
                    ddlServiceType.SelectedIndex = 0;
                    txtRegNo.Text = "";
                }
                else
                {
                    lblMessage.Text = "Not Submitted Successfully";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    txtRemarks.Text = "";
                    ddlVendorType.SelectedIndex = -1;
                    ddlServiceType.SelectedIndex = 0;
                    txtRegNo.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
            lblMessageEdit.Text = ex.Message.ToString();
        }
    }

    protected void BtnSaveEdit_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Text = "";
            pnlpopup.Visible = false;
            pnlPopUpEdit.Visible = false;
            DataSet dsUpdateMappedCar = new DataSet();
            Crdr = new CorDrive();
            dsUpdateMappedCar = Crdr.UpdateMappedCars(Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(lblCarIDEdit.Text), txtRemarksEdit.Text, Convert.ToInt32(ddlStatusEdit.SelectedValue), Session["UserID"].ToString());
            string Output = dsUpdateMappedCar.Tables[0].Rows[0]["MSG"].ToString();
            if (Output == "Updated Successfully")
            {
                //lblMessage.Text = "Record update successfully !";
                txtRemarksEdit.Text = "";
                ddlStatusEdit.SelectedIndex = -1;
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Record update successfully !');</script>");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "close", "<script language=javascript>self.close();</script>");
            }
            else
            {
                lblMsg.Text = Output.ToString();
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }

        }
        catch (Exception ex)
        {
            lblMessageEdit.Text = ex.Message.ToString();
        }
    }

    protected void btnResetAll_Click(object sender, EventArgs e)
    {
        InitializeControls(Form);
    }
    protected void btnDeactive_Click(object sender, EventArgs e)
    {
        int totalInactivate = 0;
        if (gvMappedCars.Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(txtBulkDeactiveReason.Text))
            {
                foreach (GridViewRow row in gvMappedCars.Rows)
                {
                   
                    Label _CarID = (Label)row.FindControl("lblCarID");
                    CheckBox _isCarChecked = (CheckBox)row.FindControl("CheckBox_row");
                    if (_isCarChecked.Checked == true)
                    {
                        try
                        {   
                            DataSet dsUpdateMappedCar = new DataSet();
                            Crdr = new CorDrive();
                            dsUpdateMappedCar = Crdr.DeactiveMappedCars(Convert.ToInt32(ddlCityName.SelectedValue), Convert.ToInt32(ddlClientName.SelectedValue), Convert.ToInt32(_CarID.Text), txtBulkDeactiveReason.Text, Convert.ToInt16("0"), Session["UserID"].ToString());
                            string Output = dsUpdateMappedCar.Tables[0].Rows[0]["MSG"].ToString();
                            if (Output == "Updated Successfully")
                            {
                                totalInactivate += 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            lblMessageEdit.Text = ex.Message.ToString();
                        }
 
                    }

                }
                if (totalInactivate == gvMappedCars.Rows.Count)
                {
                    Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('ALL Record update successfully !');</script>");
                }
                else
                {
                    Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert(" + totalInactivate + "' Record update successfully !');</script>");
                }
            }
            else
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Enter deactivate reason !');</script>");
                txtBulkDeactiveReason.Focus();
                return;
            }
        
        }


    }
}
