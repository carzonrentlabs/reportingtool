<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ChauffeurRatingReport.aspx.cs" Inherits="ChauffeurRatingReport" Title="Chauffuer Rating" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <script src="JQuery/jquery.min.js" type="text/javascript"></script>

    <script src="JQuery/ui.core.js" type="text/javascript"></script>

    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
       
       function pageLoad(sender, args) {
            $("#<%=txtToDate.ClientID%>").datepicker();
        }
        
        
        function validate() {
            if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>

    <center>
        <asp:UpdatePanel runat="server" ID="upChaufferRating">
            <ContentTemplate>
                <div style="text-align: center">
                    <table cellpadding="0" cellspacing="0" border="0" style="display: inline">
                        <tr>
                            <td colspan="9" align="center">
                                <b>Chauffeur Rating Report</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9" align="center">
                                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;<asp:Label ID="lblCityName" runat="server">City Name</asp:Label>
                                <asp:DropDownList ID="ddlCity" runat="server" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;&nbsp;<asp:Label ID="lblRMName" runat="server">RM Name</asp:Label>
                                <asp:DropDownList ID="ddlRMName" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;&nbsp;<asp:Label ID="lblVendorName" runat="server">Vendor Status</asp:Label>
                                <asp:DropDownList ID="ddlVendorStatus" runat="server">
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Not Active" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;&nbsp;<asp:Label ID="lblChauffeurStatus" runat="server">Chauffeur Status</asp:Label>
                                <asp:DropDownList ID="ddlChauffeurStatus" runat="server">
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Not Active" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;&nbsp;<asp:Label ID="lblCar" runat="server">Car Status</asp:Label>
                                <asp:DropDownList ID="ddlCarStatus" runat="server">
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Not Active" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="lblCarCreation" runat="server">Car Creation Date</asp:Label>&nbsp;&nbsp;
                                <asp:DropDownList ID="ddlCarCreation" runat="server">
                                    <asp:ListItem Text="Within One Month" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="1-3 Months" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="3-6 Months" Value="3"></asp:ListItem>
                                    <asp:ListItem Text=">6 Months" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;&nbsp;<asp:Label ID="lblToDate" runat="server">To date</asp:Label>
                                &nbsp;&nbsp;<asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                            </td>
                            <td>
                                &nbsp;&nbsp;
                                <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9" align="center">
                                <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="upChaufferRating"
                                    DynamicLayout="true">
                                    <ProgressTemplate>
                                        <img src="images/wait.gif" alt="Loading..." />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: center">
                    <asp:GridView ID="grvChauffeur" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" AllowPaging="true"
                        PageSize="100">
                        <RowStyle BackColor="White" ForeColor="#330099" />
                        <Columns>
                            <asp:BoundField HeaderText="City Name" DataField="CityName" />
                            <asp:BoundField HeaderText="Insta Cab ID" DataField="VendorCarID" />
                            <asp:BoundField HeaderText="Car No" DataField="Regnno" />
                            <asp:BoundField HeaderText="RM Name" DataField="RMname" />
                            <asp:BoundField HeaderText="RM Mobile No" DataField="RMMobileNo" />
                            <asp:BoundField HeaderText="Car Category" DataField="CarCatname" />
                            <asp:BoundField HeaderText="Car Model" DataField="CarModelName" />
                            <asp:BoundField HeaderText="Car Registered under Business" DataField="BusinessName" />
                            <asp:BoundField HeaderText="Chauffer Creation Date" DataField="Createdate" />
                            <asp:BoundField HeaderText="Chauffeur Name" DataField="ChauffeurName" />
                            <asp:BoundField HeaderText="Chauffeur Mobile No" DataField="Mobile" />
                            <asp:BoundField HeaderText="Vendor Name" DataField="CarVendorName" />
                            <asp:BoundField HeaderText="Vendor Mobile No" DataField="VendorMobileNo" />
                            <asp:BoundField HeaderText="Chauffeur,Vendor and Car Active" DataField="ChaufeurVendorCarActive" />
                            <%-- <asp:BoundField HeaderText="Deactive Status" DataField="DeactiveStatus" />--%>
                            <asp:TemplateField HeaderText="Deactive Status">
                                <ItemTemplate>
                                    <%#Eval("DeactiveStatus")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField HeaderText="Monthly Duties" DataField="MonthlyTotalBooking" />--%>
                            <asp:TemplateField HeaderText="Monthly Duties">
                                <ItemTemplate>
                                    <%#Eval("TotalComplaints")%>
                                    (<%#Eval("MonthlyDutiesScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Quarterly Duties" DataField="QuaterlyTotalBooking" />--%>
                            <asp:TemplateField HeaderText="Quarterly Duties">
                                <ItemTemplate>
                                    <%#Eval("QuaterlyTotalBooking")%>
                                    (<%#Eval("QuarterlyDutiesScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Chauffeur Active in System since(Date)" DataField="ApprovedDate" />
                            <asp:BoundField HeaderText="Last Booking Performed date on (Date)" DataField="LastWorkingDate" />
                            <%--<asp:BoundField HeaderText="No.of Complaints Received" DataField="TotalComplaints" />--%>
                            <asp:TemplateField HeaderText="No.of Complaints Received">
                                <ItemTemplate>
                                    <%#Eval("TotalComplaints")%>
                                    (<%#Eval("NoOfComplain")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No. of bids received" DataField="NoOfBidRecived" />
                            <asp:BoundField HeaderText="No. of bids rejected" DataField="NoOfBidReject" />
                            <%--<asp:BoundField HeaderText="Cor Drive Version Used" DataField="VersionNo" />--%>
                            <asp:TemplateField HeaderText="Cor Drive Version Used">
                                <ItemTemplate>
                                    <%#Eval("VersionNo")%>
                                    (<%#Eval("VersonScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField HeaderText="Monthly Revenue earned" DataField="MonthlyRevenue" />--%>
                            <asp:TemplateField HeaderText="Monthly Revenue earned">
                                <ItemTemplate>
                                    <%#Eval("MonthlyRevenue")%>
                                    (<%#Eval("MonthlyRevenueScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField HeaderText="Quarterly Revenue earned" DataField="QuaterlyRevenue" />--%>
                            <asp:TemplateField HeaderText="Quarterly Revenue earned">
                                <ItemTemplate>
                                    <%#Eval("QuaterlyRevenue")%>
                                    (<%#Eval("QuarterlyRevenueScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Average Customer Rating" DataField="Average" />--%>
                            <asp:TemplateField HeaderText="Average Customer Rating">
                                <ItemTemplate>
                                    <%#Eval("Average")%>
                                    (<%#Eval("CustomerRatingScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Monthly COR Drive Login Hours" DataField="MonthlyLoginHrMin" />--%>
                            <asp:TemplateField HeaderText="Monthly COR Drive Login Hours">
                                <ItemTemplate>
                                    <%#Eval("MonthlyLoginHrMin")%>
                                    (<%#Eval("MonthlyLoginScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Quarterly COR Drive Login Hours" DataField="QuaterlyLoginHrMin" />--%>
                            <asp:TemplateField HeaderText="Quarterly COR Drive Login Hours">
                                <ItemTemplate>
                                    <%#Eval("QuaterlyLoginHrMin")%>
                                    (<%#Eval("QuarterlyLoginScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="No.of Deallocations by COR Personnel" DataField="TotalDeallocation" />--%>
                            <asp:TemplateField HeaderText="No.of Deallocations by COR Personnel">
                                <ItemTemplate>
                                    <%#Eval("TotalDeallocation")%>
                                    (<%#Eval("DeallocationScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No.of penalties" DataField="Penalities" />
                            <%--<asp:BoundField HeaderText="No.of Expired Documents" DataField="NoOFExpirDoc" />--%>
                            <asp:TemplateField HeaderText="No.of Expired Documents">
                                <ItemTemplate>
                                    <%#Eval("NoOFExpirDoc")%>
                                    (<%#Eval("DocScore")%>)
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Chauffeur Score (out of 100)" DataField="ChauffeurScore" />
                            <asp:BoundField HeaderText="Chauffeur Performance (High/Average/Low)" DataField="ChauffeurPerformance" />
                            <%--<asp:BoundField HeaderText="Vendor Score (out of 100)" DataField="VendorMobileNO" HtmlEncode ="false"/>
                            <asp:BoundField HeaderText="Vendor Performance (High/Average/Low)" DataField="Staus" />
                            <asp:BoundField HeaderText="Car Score(Out of 100)" DataField="PaymentMode" />
                            <asp:BoundField HeaderText="Car Performance (High/Average/Low)" DataField="OutstationYN" />--%>
                        </Columns>
                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="bntExprot" />
            </Triggers>
        </asp:UpdatePanel>
    </center>
</asp:Content>
