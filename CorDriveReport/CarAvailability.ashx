<%@ WebHandler Language="C#" Class="CarAvailability" %>

using System;
using System.Web;
using System.Data;
public class CarAvailability : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {

      
        
        context.Response.ContentType = "text/plain";
        int cabId = Convert.ToInt32(context.Request.QueryString["cabId"].ToString());
        //DateTime FormDutyDate = Convert.ToDateTime(context.Request.QueryString["FDutyDate"]);
        //DateTime ToDutyDate = Convert.ToDateTime(context.Request.QueryString["TDutyDate"]);
        //DateTime ToDutyDate =Convert.ToDateTime(Convert.ToDateTime(FormDutyDate).AddDays(1).ToString("MM-dd-yyyy"));
        
        DataSet dsCarAvailabilityDetails = new DataSet();
        CorDrive objCordrive = new CorDrive();
        //dsCarAvailabilityDetails = objCordrive.GetCarAvailabilityDetails(cabId, FormDutyDate,ToDutyDate);
        dsCarAvailabilityDetails = objCordrive.GetCarAvailabilityDetails(cabId);
        if (dsCarAvailabilityDetails.Tables[0].Rows.Count==2)
        {

            string ConcatenateString =dsCarAvailabilityDetails.Tables[0].Rows[0]["CarId"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["AvailabilityStatus"].ToString().Trim() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["DutyDate"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["AvailableFromTime"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["AvailableToTime"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["Comments"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["CreatedBy"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["ModifyBy"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[0]["AutoId"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[1]["AvailabilityStatus"].ToString().Trim() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[1]["DutyDate"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[1]["AvailableFromTime"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[1]["AvailableToTime"].ToString() + "#" + dsCarAvailabilityDetails.Tables[0].Rows[1]["Comments"].ToString()+"#"+dsCarAvailabilityDetails.Tables[0].Rows[1]["AutoId"].ToString(); 
            context.Response.Write(ConcatenateString);
            context.Response.End();
        }
        else
        {
            context.Response.Write("Record Not Found");
            context.Response.End();
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}