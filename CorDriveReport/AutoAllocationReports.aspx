﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AutoAllocationReports.aspx.cs" Inherits="CorDriveReport_AutoAllocationReports" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });

        function validate() {
            var dateFrom = document.getElementById('<%=txtFromDate.ClientID %>').value;
            var dateTo = document.getElementById('<%=txtToDate.ClientID %>').value;
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
            else if (Date.parse(document.getElementById('<%=txtFromDate.ClientID %>').value) > Date.parse(document.getElementById('<%=txtToDate.ClientID %>').value)) {

                alert("To date should not be greater than from date.");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;

            }
            else if (parseInt(DateDiff(dateFrom, dateTo)) > 31) {
                alert("Date difference should not be greater than 31 days");
                return false;
            }
        }
        function DateDiff(date1, date2) {
            var firstDate = new Date(date1)
            var secondDate = new Date(date2)
            var datediff = secondDate.getTime() - firstDate.getTime();
            return (datediff / (24 * 60 * 60 * 1000));
        }
    </script>
    <center>
        <div align="center">
            <b>Auto Allocation Reports</b>
        </div>
        <br />
        <div>
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td colspan="8">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;&nbsp;<b>From Date</b>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<b>To Date</b>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                    </td>
                    <td align="left">
                        &nbsp;&nbsp;<asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <div align="center">
            <asp:GridView ID="grvAutoAlloction" runat="server" AutoGenerateColumns="False" BackColor="White"
                BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" OnRowDataBound="grvAutoAlloction_RowDataBound"
                FooterStyle-Font-Bold="true">
                <Columns>
                    <asp:TemplateField HeaderText="City Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCityName" runat="server" Text='<%#Eval("CityName") %>'>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Pickup" DataField="Pickup" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="AutoAllocated" DataField="AutoAllocated" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Deallocated" DataField="Deallocated" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CorDriveAllocated" DataField="CorDriveAllocated" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CorDriveDellocated" DataField="CorDriveDellocated" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="TotalLogin" DataField="TotalLogin" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CorDriveStarted" DataField="CorDriveStarted" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CorDriveCompleted" DataField="CorDriveCompleted" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#990000" ForeColor="#FFFFCC" Font-Bold="True" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            </asp:GridView>
        </div>
    </center>
</asp:Content>
