using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;

public partial class ComplaintSummary : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
    }
    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }
    #region Bind ComplaintSummary Details
    public void BindComplaintSummary()
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid = Convert.ToInt32(ddlCityName.SelectedValue);
         
         DataSet ds = new DataSet();
         ds = objAdmin.GetComplaintSummary(FromDate, Todate, cityid);
         if (ds.Tables[0].Rows.Count > 0)
        {
            grvComplaintSummary.DataSource = ds.Tables[0];
            grvComplaintSummary.DataBind();
            int r = grvComplaintSummary.Rows.Count;
            r--;
            //grvComplaintSummary.Rows[r].BackColor = System.Drawing.Color.DarkGoldenrod;
            grvComplaintSummary.Rows[r].Cells[0].Font.Bold=true;
            grvComplaintSummary.Rows[r].Cells[1].Font.Bold = true;
            grvComplaintSummary.Rows[r].Cells[2].Font.Bold = true;
        }
        else
        {
            grvComplaintSummary.DataSource = ds.Tables[0];
            grvComplaintSummary.DataBind();
        }
    }
        
 
    #endregion
   
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        BindComplaintSummary();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        string Todate, FromDate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();
        DataSet ds = new DataSet();
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=CRMComplaintReport.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");
        ds = objAdmin.GetComplaintSummary(FromDate, Todate, cityid);
        dtEx = ds.Tables[0];
        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center' > ");
            int[] iColumns = { 0, 1, 2 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Branch Name")
                        {
                            Response.Write("<td align='left'><b>Branch Name</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Feedback Received")
                        {
                            Response.Write("<td align='left'><b>No.Of Complaints</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Actioned")
                        {
                            Response.Write("<td align='left'><b>Resolved</b></td>");
                        }
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center' >");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }
}
