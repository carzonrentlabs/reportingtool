﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using ReportingTool;
using System;

/// <summary>
/// Summary description for CorDrive
/// </summary>
public class Unblocking
{
    public Unblocking()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    
    public void Unblock(string BookingID, int UserID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@BookingID", BookingID);
        ObjparamUser[1] = new SqlParameter("@Unblockedby", UserID);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_UnblockBookings", ObjparamUser);
    }
    public void UnblockDispatch(string BookingID, int UserID)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[2];
        ObjparamUser[0] = new SqlParameter("@BookingID", BookingID);
        ObjparamUser[1] = new SqlParameter("@Unblockedby", UserID);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_UnblockDispatchBookings", ObjparamUser);
    }
}