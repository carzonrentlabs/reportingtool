﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportingTool;
using System.Data;
using System.Data.SqlClient;

public partial class FuelManagement_FuelManagementPrint : System.Web.UI.Page
{
    private clsFuelManagement clsObjFuel;
    private ObjFuelManagement objFuel;
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dtdetails = new DataTable();
        clsObjFuel = new clsFuelManagement();
        if (!Page.IsPostBack)
        {
            if (Request.QueryString.HasKeys())
            {
                int voucherId = Convert.ToInt32(Request.QueryString["voucherid"].ToString());
                dtdetails = clsObjFuel.GetVoucherDetails(voucherId);
                if (dtdetails.Rows.Count > 0)
                {
                    lblCityName.Text = dtdetails.Rows[0]["CityName"].ToString ();;
                    lblRegNo.Text =dtdetails.Rows[0]["RegnNo"].ToString ();
                    lblFuelType.Text = dtdetails.Rows[0]["FuelTypeName"].ToString();
                    lblRate.Text = dtdetails.Rows[0]["fuelPrice"].ToString();
                    lblQuantity.Text = dtdetails.Rows[0]["quantity"].ToString();
                    lblAmount.Text = dtdetails.Rows[0]["TotalCost"].ToString();
                    lblRemarks.Text = dtdetails.Rows[0]["Remarks"].ToString();
                    lblDated.Text = dtdetails.Rows[0]["GenerateDate"].ToString();
                    lblNo.Text = dtdetails.Rows[0]["VoucherNo"].ToString();
                    lblCreatedBy.Text = dtdetails.Rows[0]["CreatorName"].ToString();
                    lblCreateDateTime.Text = dtdetails.Rows[0]["CreateDate"].ToString();
                }

            }
            
        }

    }
}