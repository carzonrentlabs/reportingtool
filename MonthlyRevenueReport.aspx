<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MonthlyRevenueReport.aspx.cs"
    Inherits="MonthlyRevenueReport" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="ConMonthly" ContentPlaceHolderID="cphPage" runat="server">
    <script src="JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="JQuery/ui.core.js" type="text/javascript"></script>
    <script src="JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="Javascript" src="App_Themes/CommonScript.js" type="text/javascript"></script>
    <script language="JavaScript" src="../JScripts/Datefunc.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#<%=txtFrom.ClientID%>").datepicker();
            $("#<%=txtTo.ClientID%>").datepicker();
        });

        function ValidationCheck() {
            if (document.getElementById('<%=txtFrom.ClientID %>').value == "") {
                alert("Fill from date.");
                document.getElementById('<%=txtFrom.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtTo.ClientID %>').value == "") {
                alert("Fill to date.");
                document.getElementById('<%=txtTo.ClientID %>').focus();
                return false;
            }
    }

    function showDate(sender, args) {
        if (sender._textbox.get_element().value == "") {
            var todayDate = new Date();
            sender._selectedDate = todayDate;
        }
    }

    function dateReg(obj) {
        if (obj.value != "") {
            //alert(obj.value);      
            var reg = /((0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])-([1-9][0-9][0-9][0-9]))/
            if (reg.test(obj.value)) {
                //alert('valid');      
            }
            else {
                alert('notvalid');
                obj.value = "";
            }
        }
    }

    </script>
    <center>
        <div style="width: 85%">
            <span><font size="2"><b>Download Monthly Revenue Report</b></font></span>
            <br />
            <br />
            <br />
            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                    <td align="left" colspan="8">From Date&nbsp;&nbsp;
                        <asp:TextBox ID="txtFrom" runat="server" Width="100px" MaxLength="10">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Font-Bold="true"
                            ControlToValidate="txtFrom" Display="Dynamic" ErrorMessage="*">
                        </asp:RequiredFieldValidator>
                        &nbsp;&nbsp; To Date&nbsp;&nbsp;
                        <asp:TextBox ID="txtTo" runat="server" Width="100px" MaxLength="10">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Font-Bold="true"
                            ControlToValidate="txtTo" Display="Dynamic" ErrorMessage="*">
                        </asp:RequiredFieldValidator>
                        &nbsp;&nbsp; City Name&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlCityName" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp; Business Type &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlbusinessType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlbusinessType_SelectedIndexChanged">
                            <asp:ListItem Text="CarRental" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="EasyCabs" Value="2"></asp:ListItem>
                            <asp:ListItem Text="MegaCabs" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="8">
                        &nbsp;
                        <asp:CheckBox ID="CheckID" Text="EasyCab New Format" runat="server" />
                        &nbsp;
                        <asp:CheckBox ID="GstBulk" Text="GST Bulk Format" runat="server" />
                        &nbsp;
                        <asp:CheckBox ID="GSTManual" Text="GST Manul Format" runat="server" />
                        &nbsp;&nbsp;
                        Client Name&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlClientName" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                        <asp:Button ID="bntNextImport" runat="server" Text="Export to Excel" OnClick="bntNextImport_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute; top: 0px"
                name="CalFrame" src="APP_THEMES/CorCalendar.htm" frameborder="0" width="260"
                scrolling="no" height="182"></iframe>
        </div>
    </center>
</asp:Content>
