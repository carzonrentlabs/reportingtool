<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PenaltyDetails.aspx.cs" Inherits="PenaltyDetails" Title="Penalty Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
        });
        function validate() {
            if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                alert("Please Select the From Date");
                document.getElementById('<%=txtFromDate.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                alert("Please Select the To Date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
            else if (Date.parse(document.getElementById('<%=txtFromDate.ClientID %>').value) > Date.parse(document.getElementById('<%=txtToDate.ClientID %>').value)) {
                alert("From date should not be greater than to date");
                document.getElementById('<%=txtToDate.ClientID %>').focus();
                return false;
            }
        }
    </script>
    <center>
        <div style="text-align: center">
            <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
                <tr>
                    <td colspan="5" align="center">
                        <b>Penalty Details</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center">
                        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblCityName" runat="server">City Name</asp:Label>&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlCity" runat="server">
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblFromDate" runat="server">From Date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        <asp:Label ID="lblToDate" runat="server">To date</asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                    </td>
                    <td style="height: 20px; white-space: nowrap;">
                        &nbsp;&nbsp;
                        <asp:Button ID="bntExprot" runat="server" Text="Export To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="text-align: center">
            <asp:GridView ID="grvValidation" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                PageSize="100" OnPageIndexChanging="grvValidation_PageIndexChanging">
                <Columns>
                    <asp:BoundField HeaderText="BookingID" DataField="BookingID" />
                    <asp:BoundField HeaderText="City Name" DataField="CityName" />
                    <asp:BoundField HeaderText="Client Name" DataField="ClientCoName" />
                    <asp:BoundField HeaderText="Guest Name" DataField="GuestName" />
                    <asp:BoundField HeaderText="Guest Mobile" DataField="Phone1" />
                    <asp:BoundField HeaderText="Model Name" DataField="CarModelName" />
                    <asp:BoundField HeaderText="Flight No" DataField="flightNumber" />
                    <asp:BoundField HeaderText="PickUp Date" DataField="PickUpDate" />
                    <asp:BoundField HeaderText="PickUp Time" DataField="PickUpTime" />
                    <asp:BoundField HeaderText="PickUp Address" DataField="PickUpAdd" />
                    <asp:BoundField HeaderText="OutstationYN" DataField="OutstationYN" />
                    <asp:BoundField HeaderText="Service" DataField="Service" />
                    <asp:BoundField HeaderText="PenalityAmount" DataField="PenalityAmount" />
                    <asp:BoundField HeaderText="Vehicle No" DataField="VehicleNo" />
                    <asp:BoundField HeaderText="Chauffeur Name" DataField="ChauffeurName" />
                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" />
                    <asp:BoundField HeaderText="Deallocation Remarks" DataField="DeallocationRemarks" />
                    <asp:BoundField HeaderText="Deallocation Category" DataField="deallocationCategory" />
                    <asp:BoundField HeaderText="Penalty Type" DataField="PenaltyType" />
                    <asp:BoundField HeaderText="Penalty Remarks" DataField="PenaltyRemarks" />
                </Columns>
            </asp:GridView>
        </div>
    </center>
</asp:Content>
