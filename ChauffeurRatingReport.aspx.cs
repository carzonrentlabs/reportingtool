using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using ReportingTool;
using System.IO;
using System.Drawing;

public partial class ChauffeurRatingReport : System.Web.UI.Page
{
    clsAdmin objAdmin = null;
    CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        lblMessage.Visible = false;
       
        if (!Page.IsPostBack)
        {
            BindCityDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");

    }
    private void BindCityDropDownlist()
    {
        objCordrive = new CorDrive();
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation.Tables[0];
                    ddlCity.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    private void BindGrid()
    {
        objAdmin = new clsAdmin();
        DataSet ds = new DataSet();
        lblMessage.Visible = false;
        try
        {
            ds = objAdmin.GetChauffeurRatingReport(Convert.ToInt32(ddlCity.SelectedValue.ToString()), Convert.ToDateTime(txtToDate.Text.ToString()), Convert.ToInt32(ddlVendorStatus.SelectedValue), Convert.ToInt32(ddlCarStatus.SelectedValue), Convert.ToInt32(ddlChauffeurStatus.SelectedValue), Convert.ToInt32(ddlRMName.SelectedValue), Convert.ToInt32(ddlCarCreation.SelectedValue));
            if (ds.Tables[0].Rows.Count > 0)
            {
                grvChauffeur.DataSource = ds.Tables[0];
                grvChauffeur.DataBind();
            }
            else
            {
                grvChauffeur.DataSource = null;
                grvChauffeur.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception Ex)
        {

            grvChauffeur.DataSource = null;
            grvChauffeur.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
            lblMessage.ForeColor = System.Drawing.Color.Red;
        }
       
    
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=ChauffeurRatingReport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            grvChauffeur.AllowPaging = false;
            this.BindGrid();

            grvChauffeur.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grvChauffeur.HeaderRow.Cells)
            {
                cell.BackColor = grvChauffeur.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grvChauffeur.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grvChauffeur.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grvChauffeur.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grvChauffeur.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        objAdmin = new clsAdmin();
        DataSet ds = new DataSet();
         ds = objAdmin.GetRMName(Convert.ToInt32(ddlCity.SelectedValue.ToString()));
         if (ds.Tables[0].Rows.Count > 0)
         {
             ddlRMName.DataSource = ds.Tables[0];
             ddlRMName.DataValueField = "SysUserId";
             ddlRMName.DataTextField = "RMName";
             ddlRMName.DataBind();
             ddlRMName.Items.Insert(0, new ListItem("All", "0"));
         }
         else {
             ddlRMName.Items.Insert(0, new ListItem("All", "0"));
         }


    }
}
