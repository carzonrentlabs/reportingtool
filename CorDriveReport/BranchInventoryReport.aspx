﻿<%@ Page Title="Branch Inventory Report" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="BranchInventoryReport.aspx.cs" Inherits="CorDriveReport_BranchInventoryReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>").click(function () {
//                if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
//                    alert("Please Select the From Date");
//                    document.getElementById('<%=txtFromDate.ClientID %>').focus();
//                    return false;
//                }
//                else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
//                    alert("Please Select the To Date");
//                    document.getElementById('<%=txtToDate.ClientID %>').focus();
//                    return false;
//                }
//                else {
//                    ShowProgress();
                //                }
                ShowProgress();
            });
            $("#<%=bntExprot.ClientID%>").click(function () {
//                if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
//                    alert("Please Select the From Date");
//                    document.getElementById('<%=txtFromDate.ClientID %>').focus();
//                    return false;
//                }
//                else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
//                    alert("Please Select the To Date");
//                    document.getElementById('<%=txtToDate.ClientID %>').focus();
//                    return false;
//                }
            });
        });
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
    </script>
    <center>
        <table cellpadding="2" cellspacing="5" border="0">
            <tr>
                <td colspan="8" align="center">
                    <b>Cor Branch Inventory Report</b>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <b>Branch</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlBranch" runat="server">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <b>Category</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlCat" runat="server">
                    </asp:DropDownList>
                </td>
                <td style="text-align: left;">
                    <b>Attachment Package</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlAttachmentPkg" runat="server">
                        <asp:ListItem Text="All" Value="0"> </asp:ListItem>
                        <asp:ListItem Text="Revenue Sharing" Value="1"> </asp:ListItem>
                        <asp:ListItem Text="Revenue Sharing Slab Based" Value="2"> </asp:ListItem>
                        <asp:ListItem Text="PKPM" Value="3"> </asp:ListItem>
                        <asp:ListItem Text="LTR" Value="4"> </asp:ListItem>
                        <asp:ListItem Text="On-Call" Value="5"> </asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left;">
                    <b>Car Source</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlSoruce" runat="server">
                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Owned"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Vendor"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <b>Create From Date</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <b>Create To date</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <b>Status</b>&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Value="0" Text="All"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Non active"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left;">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                    &nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" 
                        onclick="bntExprot_Click" />
                </td>
            </tr>
        </table>
        <div class="Repeater" style="width: 1350px; height: 400px; overflow: auto">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdRport" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="SNo." ItemStyle-HorizontalAlign="Center" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.DataItemIndex+1  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Car Model" DataField="CarModelName" />
                                <asp:BoundField HeaderText="Car Category" DataField="CarCatName" />
                                <asp:BoundField HeaderText="Vehicle No" DataField="RegnNo" />
                                <asp:BoundField HeaderText="Driver Name" DataField="DriverName" />
                                <asp:BoundField HeaderText="Driver Mobile" DataField="DriverMobileNo" />
                                <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" />
                                <asp:BoundField HeaderText="Pan No" DataField="PanNo" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" />
                                <asp:BoundField HeaderText="VDP YN" DataField="VDPYN" />
                                <asp:BoundField HeaderText="Status" DataField="CurrentStatus" />
                                <asp:BoundField HeaderText="Vehicle Created Date" DataField="VehicleCreateDate" />
                                <asp:BoundField HeaderText="Vehicle Modified Date" DataField="VehicleModifedDate" />
                                <asp:BoundField HeaderText="Vendor Created Date" DataField="VendorCreateDate" />
                                <asp:BoundField HeaderText="Attachment Model" DataField="AttachmentModel" />
                                <asp:BoundField HeaderText="Payment Option" DataField="PaymentOption" />
                                <asp:BoundField HeaderText="Revenue Share" DataField="RevenueSharePC" />
                                <asp:BoundField HeaderText="Effective Date" DataField="EffectiveDate" />
                                <asp:BoundField HeaderText="Car Age from RC Date" DataField="AgeOfCar" />
                                <asp:BoundField HeaderText="Grade" DataField="Grade" />
                                <asp:BoundField HeaderText="IMEI Number" DataField="IMEINumber" />
                                <asp:BoundField HeaderText="SIM Mobile Number" DataField="SimMobileNumber" />
                                <asp:BoundField HeaderText="Parking Location" DataField="ParkingLodation" />
                                <asp:BoundField HeaderText="COR Drive Status" DataField="CorDriveStatus" />
                                <asp:BoundField HeaderText="GPS Status" DataField="GPSStatus" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
         <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>
