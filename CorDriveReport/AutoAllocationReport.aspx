<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AutoAllocationReport.aspx.cs" Inherits="CorDriveReport_AutoAllocationReport" Title="Auto Allocation Report" %>

<asp:Content ID="cploginHoursDetailed" ContentPlaceHolderID="cphPage" runat="Server">

    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>

    <script src="../JQuery/ui.core.js" type="text/javascript"></script>

    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $("#<%=txtFromDate.ClientID%>").datepicker();
              $("#<%=txtToDate.ClientID%>").datepicker();
            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select the From Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
                if (document.getElementById('<%=txtFromDate.ClientID%>').value == "") {
                    alert("Please Select the To Date");
                    document.getElementById('<%=txtFromDate.ClientID%>').focus();
                    return false;
                }
               
                else {
                    ShowProgress();
                }
            });
       });
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        function ShowProgressKill() 
        {  
            setTimeout(function () 
            {
                var modal = $('<div />');
                modal.removeClass("modal");
                $('body').remove(modal);
                loading.hide();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            },200);
        }

    </script>

    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Auto Allocation Report</b>
                    <br />
                     <br />
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; white-space: nowrap;">
                <b>Select City</b>
                     <asp:DropDownList ID="ddlCity" runat="server">
                   
                </asp:DropDownList>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    <b> From Date</b>
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                </td>
                 <td style="height: 20px; white-space: nowrap;">
                  <b> To Date</b>
                   <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;<asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
                <td style="height: 20px; white-space: nowrap;">
                   &nbsp;&nbsp;
                  <asp:Button ID="bntExport" runat="Server" Text="Export To Excel" OnClick="bntExport_Click"/>
               
                </td>
                <td style="height: 20px; white-space: nowrap;">
                    &nbsp;&nbsp;
                       <asp:Button ID="btnExport" runat="Server" Text="Export Deallocation Report" OnClick="btnExport_Click"/>
                </td>
            </tr>
        </table>
        <br />
        <div >
            <table cellpadding="0" cellspacing="0" border="0">
          
                <tr>
                    <td>
                       <asp:GridView ID="grdBidSummaryMorning" runat="server" AutoGenerateColumns="false" Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0"><tr><td align="center" style="background-color: #9966ff"><b>Morining Duties</b></td></tr></table>'
CaptionAlign="Top">
                            <Columns>
                            	<asp:BoundField HeaderText="City Name" DataField="cityName" HeaderStyle-Wrap="false"/>
                                <asp:BoundField HeaderText="Total" DataField="total" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Eligible" DataField="eligible" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Manual Alloc" DataField="manual_alloc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Manual Ds" DataField="manual_ds" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Auto Alloc Reallocated" DataField="auto_alloc_reallocated" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Awarded" DataField="awarded" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="In Progress" DataField="in_progress" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Unable Deliver Bid" DataField="unable_deliver_bid" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Bid Sent Rejected" DataField="bid_sent_rejected" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Bid Sent No Response" DataField="bid_sent_no_response" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="No Car Nearby" DataField="no_car_nearby" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Auto Alloc" DataField="total_auto_alloc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Number Of Unique Cars" DataField="number_of_unique_cars" HeaderStyle-Wrap="false" />
                                
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                
                 <tr>
                    <td>
                       <asp:GridView ID="grdBidSummaryInterDay" runat="server" AutoGenerateColumns="false" Caption='<table border="1" width="100%" cellpadding="0" cellspacing="0"><tr><td align="center" style="background-color: #9966ff"><b>Intraday Duties</b></td></tr></table>' CaptionAlign="Top">
                            <Columns>
                            	<asp:BoundField HeaderText="City Name" DataField="cityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total" DataField="total" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Eligible" DataField="eligible" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Manual Alloc" DataField="manual_alloc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Manual Ds" DataField="manual_ds" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Auto Alloc Reallocated" DataField="auto_alloc_reallocated" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Awarded" DataField="awarded" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="In Progress" DataField="in_progress" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Unable Deliver Bid" DataField="unable_deliver_bid" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Bid Sent Rejected" DataField="bid_sent_rejected" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Bid Sent No Response" DataField="bid_sent_no_response" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="No Car Nearby" DataField="no_car_nearby" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Auto Alloc" DataField="total_auto_alloc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Number Of Unique Cars" DataField="number_of_unique_cars" HeaderStyle-Wrap="false" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:GridView ID="grdBidDetails" runat="server" AutoGenerateColumns="false" OnRowCommand="grdBidDetails_RowCommand">
                            <Columns>
                              <asp:TemplateField HeaderText="Export">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkBtnExport" runat="server" class="big-link" ForeColor="Red" CommandName="Export" CommandArgument='<%#Eval("VendorCarID")%>'>Export</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="VendorCarID" DataField="VendorCarID" HeaderStyle-Wrap="false" />			
                            	<asp:BoundField HeaderText="Registration No" DataField="RegnNo" HeaderStyle-Wrap="false" />																
                                <asp:BoundField HeaderText="Chauffeur Name" DataField="chaufName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Chauffeur Mobile" DataField="chaufMobile" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Car Vendor Name" DataField="CarVendorName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Contact Ph1" DataField="ContactPh1" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="category" DataField="category" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="City Name" DataField="CityName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Version No" DataField="VersionNo" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Home Loc" DataField="home_loc" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Car ModelName" DataField="CarModelName" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Login Status" DataField="LoginStatus" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="RM" DataField="RM" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Last Active" DataField="last_active" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Bids" DataField="total_bids" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Unique Bids" DataField="unique_bids" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Unique Total Accepted" DataField="unique_total_accepted" HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Rejected" DataField="total_rejected"  HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Bids Sent No Response" DataField="total_bids_sent_no_response"   HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Pending Bids" DataField="total_pending_bids"  HeaderStyle-Wrap="false" />
                                <asp:BoundField HeaderText="Total Unable To Send Bid" DataField="total_unable_to_send_bid"   HeaderStyle-Wrap="false" />
                                  
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
    </center>
</asp:Content>

