using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;

///<summary>
///Summary description for clslAdmin
///</summary>
///
namespace ReportingTool
{
    public class clsAdmin
    {
        public int Retval;
        public string dbUser = "carzonrent";
        public string dbPWD = "CarZ#58B9";
        public clsAdmin()
        {
            // TODO: Add constructor logic here
        }

        public DataSet GetLocations_UserAccessWise(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise", ObjParam);
        }

        public DataSet GetLocations_UserAccessWiseBK(int UserID)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
            ObjParam[0].Direction = ParameterDirection.Input;
            ObjParam[0].Value = UserID;
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getLocations_UserAccessWise_BK", ObjParam);
        }

        public DataTable GetVendorUtilizationDetail(String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@todate", ToDate);
            ObjparamUser[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_VendorCarUtilization_RevenueSharing_CityWise", ObjparamUser);
        }

        public DataTable GetBranchWiseDutiesSummary(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_getOpenDSDetails", ObjparamUser);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_getOpenDSDetails_New", ObjparamUser);
        }

        public DataTable GetDutiesAllocationSummary(String FromDate, String ToDate, int cityid, int Type)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);
            ObjparamUser[3] = new SqlParameter("@Type", Type);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetAllocationSummary_New", ObjparamUser);
        }

        public DataTable GetDutiesAllocationDetail(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetAllocationDetail", ObjparamUser);
        }

        public DataTable GetDutiesAllocationSummary_Weekly(String FromDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@date", FromDate);
            ObjparamUser[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetAllocationSummary_weekly", ObjparamUser);
        }

        public DataSet GetDataForExcelSSC_Pivot1(string fromdate, string todate, string ToDateRemoveYN)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@DateFrom", fromdate);
            ObjparamUser[1] = new SqlParameter("@DateTo", todate);
            ObjparamUser[2] = new SqlParameter("@ToDateRemoveYN", ToDateRemoveYN);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSC_MIS_Pivot", ObjparamUser);
        }

        public DataSet GetDataForExcelSSC_Pivot2(string fromdate, string todate, string ToDateRemoveYN)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@DateFrom", fromdate);
            ObjparamUser[1] = new SqlParameter("@DateTo", todate);
            ObjparamUser[2] = new SqlParameter("@ToDateRemoveYN", ToDateRemoveYN);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SSC_MIS_Pivot_2", ObjparamUser);
        }

        public DataTable GetRC_VehicleDetail(String ToDate, int cityid, bool strAll)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@todate", ToDate);
            ObjparamUser[1] = new SqlParameter("@cityid", cityid);
            ObjparamUser[2] = new SqlParameter("@allVechicle", strAll);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_RC_CarDetail_CityWise", ObjparamUser);
        }

        public DataTable ModelWise_RCSummary(String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@todate", ToDate);
            ObjparamUser[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_RC_CarDetail_CityWise_Summary", ObjparamUser);
        }

        public DataTable GetVendorWiseShiftWiseSummary(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_VendorCarUtilization_RevenueSharing_CityWise_ShiftWise", ObjparamUser);
        }

        public DataTable GetSSCDutyDispatch_SubmissionSummary(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@DateFrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@DateTo", ToDate);
            ObjparamUser[2] = new SqlParameter("@CityID", cityid);

            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_SSC_CCS_DutySummary", ObjparamUser);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_SSC_CCS_DutySummary_New", ObjparamUser);
        }

        public DataTable GetClientWiseSubmissionSummary(String FromDate, String ToDate, int cityid, string cityname)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);
            ObjparamUser[3] = new SqlParameter("@CityName", cityname);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_ClientWiseDutySubmitted", ObjparamUser);
        }

        public DataTable GetDutyDispatchChart(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@datefrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@dateto", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_duty_dispatch_chart_new", ObjparamUser);
        }

        public DataTable GetSalesException(int Month, int Year, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@Month", Month);
            ObjparamUser[1] = new SqlParameter("@Year", Year);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_SalesExceptionReport_New", ObjparamUser);
        }

        public DataTable GetOverUtilizationDetail(String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@todate", ToDate);
            ObjparamUser[1] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_OverUtilizedCarReport_CityWise", ObjparamUser);
        }

        public DataSet getCorpCompanyNames()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getCorpCompanyNames");
            }
            catch
            {
                return null;
            }
        }

        public DataSet getRelationShipManager()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getRelationShipManager");
            }
            catch
            {
                return null;
            }
        }

        public DataSet getNationalAccManager()
        {
            try
            {
                return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getNationalAccManager");
            }
            catch
            {
                return null;
            }
        }

        public int SubmitDetail(int customerName, int LocationId, int ChkValueImp, string Implant, int RelMangrId, string Competitors, string Potential, int NatAccMangr, int createdby)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[9];
            ObjparamUser[0] = new SqlParameter("@ClientId", customerName);
            ObjparamUser[1] = new SqlParameter("@CityId", LocationId);
            ObjparamUser[2] = new SqlParameter("@RelationManagerId", RelMangrId);
            ObjparamUser[3] = new SqlParameter("@Implant", ChkValueImp);
            ObjparamUser[4] = new SqlParameter("@ImplantName", Implant);
            ObjparamUser[5] = new SqlParameter("@Competitors", Competitors);
            ObjparamUser[6] = new SqlParameter("@Potential", Potential);
            ObjparamUser[7] = new SqlParameter("@NationalAccMangrId ", NatAccMangr);
            ObjparamUser[8] = new SqlParameter("@createdby ", createdby);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "InsertCorpRelDetails", ObjparamUser);
        }

        public DataSet GetComplaintSummary(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@dateFrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@dateTo", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);
            return SqlHelper.ExecuteEasyCabsDataset(CommandType.StoredProcedure, "prc_getComplaintSummary_New1", ObjparamUser);
        }

        public DataTable GetDutiesAnalysis(String FromDate, String ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetAllocationAnalysis_New", ObjparamUser);
        }

        public DataTable GetDutyDispatchNew(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@datefrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@dateto", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_duty_dispatch_new", ObjparamUser);
        }

        public DataSet BranchWiseComplaint(String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@Date", ToDate);
            ObjparamUser[1] = new SqlParameter("@Cityid", cityid);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_getBranchWiseComplaint", ObjparamUser);
        }

        public DataSet BranchWiseComplaintQ(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@FromDate", FromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ComplaintQuaterSummary", ObjparamUser);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_ComplaintQuaterSummary_New", ObjparamUser);
        }

        public DataTable GetSSCTrackingReport(string fromdate, string todate, int CityID)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@DateFrom", fromdate);
            ObjparamUser[1] = new SqlParameter("@DateTo", todate);
            ObjparamUser[2] = new SqlParameter("@cityid", CityID);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SSC_MIS_Pivot_New", ObjparamUser);
        }

        public DataTable GetCarAgingSummary(string fromdate, string ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@fromdate", fromdate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_RC_CarDetail_CityWise_Summary_New", ObjparamUser);
        }

        public DataSet GetVendorCarUtilizationSummary(string fromdate, string ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];

            ObjparamUser[0] = new SqlParameter("@fromdate", fromdate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_VendorCarUtilization_RevenueSharing_SummaryCityWise_New", ObjparamUser);
        }
        public DataTable ADBEventReport(String FromDate, String ToDate)
        {
            int vendorId = 0;
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@VendorID", vendorId);
            // return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_ADB_Event", ObjparamUser);
            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_Prc_ADB_Event_New", ObjparamUser);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_Prc_ADB_Event", ObjparamUser);
        }
        public DataTable ClientPerformanceReport(String FromDate, String ToDate, int ServiceUnitId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@DateFrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@DateTo", ToDate);
            ObjparamUser[2] = new SqlParameter("@ServiceUnitId", ServiceUnitId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_ReviewAndClientPerformance_18July2013", ObjparamUser);
        }
        public DataTable ClientPerformanceReportCityBased(String FromDate, String ToDate, int Cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@DateFrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@DateTo", ToDate);
            ObjparamUser[2] = new SqlParameter("@CityId", Cityid);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_ReviewAndClientPerformance_BasedCity", ObjparamUser);
        }
        public DataTable BranchPerformanceDutiesAndRevenue(String FromDate, String ToDate, int ServiceUnitId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@DateFrom", FromDate);
            ObjparamUser[1] = new SqlParameter("@DateTo", ToDate);
            ObjparamUser[2] = new SqlParameter("@ServiceUnitId", ServiceUnitId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "BranchPerformanceDuties_Revenue", ObjparamUser);
        }
        public DataTable ManualDutyDispatchDetails(string fromDate, string toDate, string location)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@DateFrom", fromDate);
            ObjparamUser[1] = new SqlParameter("@DateTo", toDate);
            ObjparamUser[2] = new SqlParameter("@Location", location);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_DetailsManualDispatch", ObjparamUser);

        }

        public DataTable PendingDsDetails(String FromDate, String ToDate)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];

            ObjparamUser[0] = new SqlParameter("@fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);

            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_BillToDSDetails", ObjparamUser);
        }
        public DataTable TrackingDutiesClosureBranches(string fromDate, string toDate, string cityId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@DateFrom", fromDate);
            ObjparamUser[1] = new SqlParameter("@ToDate", toDate);
            ObjparamUser[2] = new SqlParameter("@CityId", cityId);
            //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_TrackingdutiesclosureBranches", ObjparamUser);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_TrackingdutiesclosureBranches_New", ObjparamUser);
        }

        public DataTable GetBranchWiseDutiesSummaryNew(String FromDate, String ToDate, int cityid)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@Fromdate", FromDate);
            ObjparamUser[1] = new SqlParameter("@todate", ToDate);
            ObjparamUser[2] = new SqlParameter("@cityid", cityid);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_getOpenDSDetails_New_New_5May", ObjparamUser);
        }

        public DataTable BookingDetailsRS(string fromDate, string toDate, string cityId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            ObjparamUser[0] = new SqlParameter("@Fromdate", fromDate);
            ObjparamUser[1] = new SqlParameter("@Todate ", toDate);
            ObjparamUser[2] = new SqlParameter("@Location", cityId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_BookingDetailsRSY", ObjparamUser);
        }
        public DataTable BookingDetailsCRD(string fromDate, string toDate, string cityId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            int vendorId = 0;
            ObjparamUser[0] = new SqlParameter("@fromdate", fromDate);
            ObjparamUser[1] = new SqlParameter("@todate", toDate);
            ObjparamUser[2] = new SqlParameter("@VendorID", vendorId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "VendorNCarWiseRevenue_PKG_Summary_212_Rental", ObjparamUser);
        }
        public DataTable BookingDetailsTrips(string fromDate, string toDate, string cityId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[3];
            int vendorId = 0;
            ObjparamUser[0] = new SqlParameter("@fromdate", fromDate);
            ObjparamUser[1] = new SqlParameter("@todate ", toDate);
            ObjparamUser[2] = new SqlParameter("@VendorID", vendorId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "VendorNCarWiseRevenue_Trips", ObjparamUser);
        }
        //*****************************************************************************************************************************
        // For the Client Co Master Activation and De-Activation 
        public DataTable GetExpiredClientContractData(int SysUserId)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[1];
            ObjparamUser[0] = new SqlParameter("@SysUserId", SysUserId);
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "prc_GetExpiredClient ", ObjparamUser);
        }

        public int UpdateClientContractDate(int ClientCoId, DateTime ContractExpireDate, int ApprovedBy, string Remarks)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[4];

            ObjparamUser[0] = new SqlParameter("@ClientCoId", ClientCoId);
            ObjparamUser[1] = new SqlParameter("@ContractExpireDate", ContractExpireDate);
            ObjparamUser[2] = new SqlParameter("@ApprovedBy", ApprovedBy);
            ObjparamUser[3] = new SqlParameter("@Remarks", Remarks);

            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_CreateClientContractExpiryLog", ObjparamUser);
        }
        //****************************************************************************************************************************

        //follow up start 
        public DataSet FollowUpInsert(int ClientID, string MeetingDate, string FromTime, string ToTime
            , string Name, string Agenda, int UserId)
        {
            SqlParameter[] ObjParam = new SqlParameter[7];
            ObjParam[0] = new SqlParameter("@ClientID", ClientID);
            ObjParam[1] = new SqlParameter("@MeetingDate", MeetingDate);
            ObjParam[2] = new SqlParameter("@FromTime", FromTime);
            ObjParam[3] = new SqlParameter("@ToTime", ToTime);
            ObjParam[4] = new SqlParameter("@Name", Name);
            ObjParam[5] = new SqlParameter("@Agenda", Agenda);
            ObjParam[6] = new SqlParameter("@UserId", UserId);

            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_InsertFollowUP", ObjParam);
        }
        //follow up end
        //public DataSet GetChauffeurRatingReport(int cityId, DateTime todate, int vendorStatus, int carStatus, int chauffeurStatus, int rmId, int carCeation)


        public DataSet GetChauffeurRatingReport(int cityId,DateTime todate,int vendorStatus,int carStatus,int chauffeurStatus,int rmId,int carCeation )
        {
            SqlParameter[] ObjParam = new SqlParameter[7];
            ObjParam[0] = new SqlParameter("@CityID", cityId);
            ObjParam[1] = new SqlParameter("@VendorStatus", vendorStatus);
            ObjParam[2] = new SqlParameter("@CarStatus", carStatus);
            ObjParam[3] = new SqlParameter("@ChauffeurStatus", chauffeurStatus);
            ObjParam[4] = new SqlParameter("@Todate", todate);
            ObjParam[5] = new SqlParameter("@RMNameId", rmId);
            ObjParam[6] = new SqlParameter("@CarCreation", carCeation);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetchauffeurPerformanceReport", ObjParam);
        }
        public DataSet GetRMName(int cityId)
        {
            SqlParameter[] ObjParam = new SqlParameter[1];
            ObjParam[0] = new SqlParameter("CityId", cityId);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetRMNameBasisOnCity", ObjParam);

        }

        //Vendor Performance Report
        public DataSet GetVendorPerformance(int Month, int Year)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@Month", Month);
            ObjparamUser[1] = new SqlParameter("@Year", Year);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_VendorDevelopmentPerformance", ObjparamUser);
        }
        
        //Airport Performance Report
        public DataSet GetAirportPerformance(int Month, int Year)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@Month", Month);
            ObjparamUser[1] = new SqlParameter("@Year", Year);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_AirportPerformance", ObjparamUser);
        }

        //Dispatch Performance Report
        public DataSet GetDispatchPerformance(int Month, int Year)
        {
            SqlParameter[] ObjparamUser = new SqlParameter[2];
            ObjparamUser[0] = new SqlParameter("@Month", Month);
            ObjparamUser[1] = new SqlParameter("@Year", Year);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_DispatchPerformance", ObjparamUser);
        }

        public DataSet getUserList()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getUserList");
        }

        public DataSet getUserListBilling()
        {
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getUserList_Billing");
        }
        
    }
}