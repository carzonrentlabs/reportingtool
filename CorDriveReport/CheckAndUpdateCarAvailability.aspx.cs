using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CorDriveReport_CheckAndUpdateCarAvailability_ : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    int TotalComplaints;
    DataSet dsCarAvailDetails = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
      {
          lblMsg.Text = string.Empty;
          //Session["UserID"] = 3122;
         
        if (!Page.IsPostBack)
        {
           
            //lblToday.Value= DateTime.Now.ToString("yyyy-MM-dd");
            //lblTomorrow.Value = System.DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            objCordrive = new CorDrive();
            ddlFromToday.DataSource = objCordrive.LoadMinute();
            ddlFromToday.DataBind();
            ddlToToday.DataSource = objCordrive.LoadMinute();
            ddlToToday.DataBind();

            ddlFromTomorrow.DataSource = objCordrive.LoadMinute();
            ddlFromTomorrow.DataBind();
            ddlToTomorrow.DataSource = objCordrive.LoadMinute();
            ddlToTomorrow.DataBind();
            //objCordrive.LoadMinute()
            BindBrachDropDownlist();
        }
    }

    private void BindBrachDropDownlist()
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_SummaryCityWise(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlCity.DataTextField = "Cityname";
                    ddlCity.DataValueField = "CityId";
                    ddlCity.DataSource = dsLocation;
                    ddlCity.DataBind();
                    ddlCity.Items.Insert(0, new ListItem("--All--", "0"));
                }
            }

        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
  
    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            objCordrive = new CorDrive();
            DataSet dsVendorCarCat = new DataSet();
            dsVendorCarCat = objCordrive.GetVendorCarCat(Convert.ToInt32(ddlCity.SelectedValue));
            if (dsVendorCarCat != null)
            {
                if (dsVendorCarCat.Tables[0].Rows.Count > 0)
                {
                    ddlVendor.DataTextField = "CarVendorName";
                    ddlVendor.DataValueField = "CarVendorID";
                    ddlVendor.DataSource = dsVendorCarCat.Tables[0];
                    ddlVendor.DataBind();
                    ddlVendor.Items.Insert(0, new ListItem("--All--", "0"));
                }
                if (dsVendorCarCat.Tables[1].Rows.Count > 0)
                {
                    ddlCarCat.DataTextField = "CarCatName";
                    ddlCarCat.DataValueField = "CarCatID";
                    ddlCarCat.DataSource = dsVendorCarCat.Tables[1];
                    ddlCarCat.DataBind();
                    ddlCarCat.Items.Insert(0, new ListItem("--All--", "0"));
                }
                if (dsVendorCarCat.Tables[2].Rows.Count > 0)
                {
                    ddlCorDriveStatus.DataTextField = "CorDriveStatus";
                    ddlCorDriveStatus.DataValueField = "CorDriveStatus";
                    ddlCorDriveStatus.DataSource = dsVendorCarCat.Tables[2];
                    ddlCorDriveStatus.DataBind();
                    ddlCorDriveStatus.Items.Insert(0, new ListItem("--All--", "0"));
                }
            }

        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        lblToday.Value = Convert.ToDateTime(txtPickUpDate.Text).ToString("MM-dd-yyyy");
        lblTomorrow.Value = Convert.ToDateTime(txtPickUpDate.Text).AddDays(1).ToString("MM-dd-yyyy");
        objCordrive = new CorDrive();
      
        dsCarAvailDetails = objCordrive.GetCarAvailabilityDetails(Convert.ToInt32(ddlCity.SelectedValue), Convert.ToDateTime(txtPickUpDate.Text),Convert.ToInt32(ddlVendor.SelectedValue),Convert.ToInt32(ddlCarCat.SelectedValue),ddlCorDriveStatus.SelectedValue,ddlPurposeOfCall.SelectedItem.Value.ToString() ,Convert.ToInt32(ddlCarStatus.SelectedItem.Value));
        if (dsCarAvailDetails.Tables[0].Rows.Count > 0)
        {
            gvCheckCarAvailability.DataSource = dsCarAvailDetails.Tables[0];
            gvCheckCarAvailability.DataBind();
            System.IO.StringWriter sw = new System.IO.StringWriter();
            dsCarAvailDetails.WriteXml(sw);
            ViewState["ds"] = sw.ToString();
        }
        else
        {
            gvCheckCarAvailability.DataSource = null;
            gvCheckCarAvailability.DataBind();
        }
    }
 
    protected void gvCheckCarAvailability_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        try
        {
            int TotalComplaints = Convert.ToInt32(hid_TotalComplaints.Value);
            int PendingDutySlip=Convert.ToInt32(hid_PendingDutySlip.Value);
            string PurposeofCall = ddlPurposeOfCall.SelectedItem.Text;
            int FAutoId=string.IsNullOrEmpty(hid_FAutoId.Value.ToString())?0:Convert.ToInt32(hid_FAutoId.Value);
            int SAutoId=string.IsNullOrEmpty(hid_SAutoId.Value.ToString()) ? 0 : Convert.ToInt32(hid_SAutoId.Value);

            DateTime FOldDate=string.IsNullOrEmpty(hid_FOldDutyDate.Value)?Convert.ToDateTime("01-01-1900"): Convert.ToDateTime(Convert.ToDateTime(hid_FOldDutyDate.Value).ToString("MM-dd-yyyy"));
            DateTime SOldDate = string.IsNullOrEmpty(hid_SOldDutyDate.Value) ? Convert.ToDateTime("01-01-2000") : Convert.ToDateTime(Convert.ToDateTime(hid_SOldDutyDate.Value).ToString("MM-dd-yyyy"));
            if (ddlAvailabilityToday.SelectedItem.Text == "Available Partly")
            {
                int FromToday = Convert.ToInt32(ddlFromToday.SelectedItem.Text) == 0000 ? 2359 : Convert.ToInt32(ddlFromToday.SelectedItem.Text);
                int ToToday = Convert.ToInt32(ddlToToday.SelectedItem.Text) == 0000 ? 2359 : Convert.ToInt32(ddlToToday.SelectedItem.Text);
                if (ToToday<=FromToday)
                {
                    ShowMessage("To Time Must be Greater Than From Time");
                    return;
                }
            }
            if (ddlAvailabilityTomorrow.SelectedItem.Text.ToString() == "Available Partly")
            {
                int FromTomorrow = Convert.ToInt32(ddlFromTomorrow.SelectedItem.Text) == 0000 ? 2359 : Convert.ToInt32(ddlFromTomorrow.SelectedItem.Text);
                int ToTomorrow = Convert.ToInt32(ddlToTomorrow.SelectedItem.Text) == 0000 ? 2359 : Convert.ToInt32(ddlToTomorrow.SelectedItem.Text);
                if (ToTomorrow <= FromTomorrow)
                {
                    ShowMessage("To Time Must be Greater Than From Time");
                    return;
                }
            }
                      

            string TodayDate =FAutoId+"#"+ txtCarId.Value + "#" + lblToday.Value + "#" + FOldDate+"#"+ ddlAvailabilityToday.SelectedItem.Text + "#" + ddlFromToday.SelectedItem.Text + "#" + ddlToToday.SelectedItem.Text + "#" + txtTodayRemarks.Text + "#" + Convert.ToInt32(TotalComplaints) + "#" + Convert.ToInt32(PendingDutySlip) + "#" +PurposeofCall+"#"+ Convert.ToString(Session["UserID"]);
            string TomorrowData =SAutoId+"#"+ txtCarId.Value + "#" + lblTomorrow.Value + "#" +SOldDate+"#"+ddlAvailabilityTomorrow.SelectedItem.Text + "#" + ddlFromTomorrow.SelectedItem.Text + "#" + ddlToTomorrow.SelectedItem.Text + "#" + txtTomorrowRemarks.Text + "#" + Convert.ToInt32(TotalComplaints) + "#" + Convert.ToInt32(PendingDutySlip) + "#" + PurposeofCall + "#" + Convert.ToString(Session["UserID"]);
            for (int i = 0; i <= 1; i++)
            {
                string[] SaveUpdate = TodayDate.Split('#');
              
                int result = objCordrive.SaveUpdateCarAvailability(Convert.ToInt32(SaveUpdate[0]), Convert.ToInt32(SaveUpdate[1]), Convert.ToDateTime(SaveUpdate[2]),Convert.ToDateTime(SaveUpdate[3]),SaveUpdate[4].ToString(), SaveUpdate[5].ToString(), SaveUpdate[6].ToString(), SaveUpdate[7].ToString(),Convert.ToInt32(SaveUpdate[8]),Convert.ToInt32(SaveUpdate[9]),SaveUpdate[10],Convert.ToInt32(SaveUpdate[11]));
                if (result > 0)
                {
                    DeleteRow(Convert.ToInt32(txtCarId.Value));
                   // ddlReasonType.SelectedIndex = 0;
                    TodayDate = TomorrowData;
                    lblMsg.Text = "Record Saved Successfully";
                    lblMsg.Visible = true;
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                    
                }
                else
                {
                    lblMsg.Text = "Record Not Saved Successfully";
                    lblMsg.Visible = true;
                    lblMsg.ForeColor = System.Drawing.Color.Red;

                }
               

            }
        }
        catch (Exception ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = ex.Message;
        }
    }
    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }
    public void DeleteRow(int CarId)
    {
        try
        {
            DataSet ds = new DataSet();
            System.IO.StringReader sr =
            new System.IO.StringReader((string)(ViewState["ds"]));
            ds.ReadXml(sr);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToInt32(ds.Tables[0].Rows[i]["CarId"]) == CarId)
                {
                    ds.Tables[0].Rows[i].Delete();
                    ds.Tables[0].AcceptChanges();

                    gvCheckCarAvailability.DataSource = ds.Tables[0];
                    gvCheckCarAvailability.DataBind();
                    break;
                }
            }
            System.IO.StringWriter sw = new System.IO.StringWriter();
            ds.WriteXml(sw);
            ViewState["ds"] = sw.ToString();
        }
        catch (Exception Ex)
        {
            lblMsg.Visible = true;
            lblMsg.ForeColor = System.Drawing.Color.Red;
            lblMsg.Text = Ex.Message;
        }
    }
   
}
