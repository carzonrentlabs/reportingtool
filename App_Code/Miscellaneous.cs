using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Miscellaneous
/// </summary>
public class Miscellaneous
{
    private int sysUserId;

    public int SysUserId
    {
        get { return sysUserId; }
        set { sysUserId = value; }
    }


    private int bookingID;

    public int BookingID
    {
        get { return bookingID; }
        set { bookingID = value; }
    }
    private string miscellaneous1;

    public string Miscellaneous1
    {
        get { return miscellaneous1; }
        set { miscellaneous1 = value; }
    }
    private string miscellaneous2;

    public string Miscellaneous2
    {
        get { return miscellaneous2; }
        set { miscellaneous2 = value; }
    }

    private string miscellaneous3;

    public string Miscellaneous3
    {
        get { return miscellaneous3; }
        set { miscellaneous3 = value; }
    }
    private string miscellaneous4;

    public string Miscellaneous4
    {
        get { return miscellaneous4; }
        set { miscellaneous4 = value; }
    }
    private string miscellaneous5;

    public string Miscellaneous5
    {
        get { return miscellaneous5; }
        set { miscellaneous5 = value; }
    }

    private string miscellaneous6;

    public string Miscellaneous6
    {
        get { return miscellaneous6; }
        set { miscellaneous6 = value; }
    }

    private string miscellaneous7;

    public string Miscellaneous7
    {
        get { return miscellaneous7; }
        set { miscellaneous7 = value; }
    }
    private string miscellaneous8;

    public string Miscellaneous8
    {
        get { return miscellaneous8; }
        set { miscellaneous8 = value; }
    }
    private string miscellaneous9;

    public string Miscellaneous9
    {
        get { return miscellaneous9; }
        set { miscellaneous9 = value; }
    }
    private string miscellaneous10;

    public string Miscellaneous10
    {
        get { return miscellaneous10; }
        set { miscellaneous10 = value; }
    }

    private string miscellaneous11;
    public string Miscellaneous11
    {
        get { return miscellaneous11; }
        set { miscellaneous11 = value; }
    }
    private string miscellaneous12;
    public string Miscellaneous12
    {
        get { return miscellaneous12; }
        set { miscellaneous12 = value; }
    }
    private string miscellaneous13;
    public string Miscellaneous13
    {
        get { return miscellaneous13; }
        set { miscellaneous13 = value; }
    }
    private string miscellaneous14;
    public string Miscellaneous14
    {
        get { return miscellaneous14; }
        set { miscellaneous14 = value; }
    }
    private string miscellaneous15;
    public string Miscellaneous15
    {
        get { return miscellaneous15; }
        set { miscellaneous15 = value; }
    }
}
