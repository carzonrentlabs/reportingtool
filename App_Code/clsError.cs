using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Data.SqlClient;


namespace ReportingTool
{

    public class clsError
    {
        public static void SendMail(Exception exObject, string MailingUserId)
        {

            SmtpClient objsmtp = new SmtpClient();
            MailMessage objmail = new MailMessage();
            if (MailingUserId == "")
            {
                MailingUserId = "UserID not available";
            }
            MailAddress objsendmail = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailFrom"].ToString());
            string ErrorMessage;
            DateTime ObjDate = DateTime.Now;
            try
            {
                SqlHelper.ExecuteNonQuery( "uspInsertError", ObjDate, exObject.Source, exObject.InnerException, exObject.Message, 1, exObject.GetType().ToString(), exObject.StackTrace.ToString());

                ErrorMessage = "Error Encountered in WFP Application On : " + ObjDate.ToString() +
                                          "<br>_____________________________________________________<br>" +
                                          "Source:-- " + exObject.Source + "<br>" +
                                          "<br>InnerException:-- " + exObject.InnerException + "<br>" +
                                          "<br>Message:-- " + exObject.Message.ToString() + "<br>" +
                                          "<br>Type:-- " + exObject.GetType().ToString() + "<br>" +
                                          "<br>StackTrace:-- " + exObject.StackTrace.ToString() + "<br>" +
                                          "<br>Session User Id:--" + MailingUserId;

                objsmtp.Host = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["Host"]);
                //objsmtp.Port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["Port"]);
                objmail.From = objsendmail;
                objmail.To.Add(System.Configuration.ConfigurationSettings.AppSettings["MailTo"].ToString());
                objmail.Subject = "ReportingTool Error";
                objmail.IsBodyHtml = true;
                objmail.Body = ErrorMessage;
                objsmtp.Send(objmail);
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("AppError.aspx");
            }
        }

        public static void SendErrorMessg(Exception exObject, string MailingUserId)
        {

            string ErrorMessage;
            DateTime ObjDate = DateTime.Now;
            try
            {

                string strSqlInsert = "Insert Into LogError(ErrorDateTime,Source,InnerException,Message,UserId,Type,StackTrace)" +
                                      "values('" + ObjDate + "'," +
                                      " '" + exObject.Source + "','" + exObject.InnerException + "','" + exObject.Message.ToString() + "', " +
                                      "'" + MailingUserId + "','" + exObject.GetType().ToString() + "','" + exObject.StackTrace.ToString() + "')";

                SqlHelper.ExecuteNonQuery(CommandType.Text, strSqlInsert);

            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }
    }
}
