using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ReportingTool;
public partial class VDPReportBranchPerformanceReport : clsPageAuthorization
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["Userid"] = 173;
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (!Page.IsPostBack)
            {
                BindCity();
                btnSubmit.Attributes.Add("onclick", "return validate();");
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string FromDate, Todate;
        FromDate = txtFrom.Value.ToString();
        Todate = txtTo.Value.ToString();
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);
        Response.Redirect("VDPReportBranchPerformanceReportCrystal.aspx?val=" + FromDate + "," + Todate + "," + cityid);
    }
}