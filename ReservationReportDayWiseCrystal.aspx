<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="ReservationReportDayWiseCrystal.aspx.cs" Inherits="ReservationReportDayWiseCrystal" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<asp:Content ID="content2" ContentPlaceHolderID="cphPage" runat="server">
    <a href="http://insta.carzonrent.com/Reports-New/ReservationTracker.aspx">Go Back</a>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
        Width="350px" HasCrystalLogo="False" HasToggleGroupTreeButton="False" HasViewList="False"
        OnUnload="CrystalReportViewer1_Unload" />
</asp:Content>
