using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CarActivityPopUp : System.Web.UI.Page
{
    int CabId, BranchId;
    string FromDate, ToDate;
    public DateTime fromdate;
    public DateTime todate;

    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if((Request.QueryString["CabId"] != null))
        {
            CabId = Convert.ToInt32(Request.QueryString["CabId"]);
            BranchId = Convert.ToInt32(Request.QueryString["BranchId"]);
            FromDate = Request.QueryString["FromDate"];
            ToDate = Request.QueryString["ToDate"];
            lblCabId.Text = CabId.ToString();
           // Response.Write("CabId=" + CabId);
        }
    }
      
    protected void btnSubmitRemarks_Click(object sender, EventArgs e)
    {        
        objCordrive = new CorDrive();
        fromdate  = Convert.ToDateTime(txtFromDate.Text);
        todate = Convert.ToDateTime(txtToDate.Text);
        int rslt = objCordrive.SubmitCarRemarks(Convert.ToInt32(lblCabId.Text), ddlReasonType.SelectedItem.Text.ToString(), Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), txtRemark.Text.ToString());
       // int rslt = objCordrive.SubmitCarRemarks(Convert.ToInt32(lblCabId.Text), ddlReasonType.SelectedItem.Text.ToString(), System.DateTime.Now, System.DateTime.Now, txtRemark.Text.ToString());
        if (rslt > 0)
        {
           // ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script> alert('Record Submitted') window.close() </script>");
           // Response.Redirect("VendorCarActivityRpt.aspx?BranchId=" + BranchId + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:Bksharma(" + BranchId+ ")", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "close", "<script language=javascript>window.opener.location.reload(true);self.close();</script>");

        }
    }
}
