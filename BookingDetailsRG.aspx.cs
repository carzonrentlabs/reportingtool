﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using ReportingTool;
using System.IO;
using System.Text;


public partial class BookingDetailsRG : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
            btnExport.Attributes.Add("Onclick", "return validate()");
        }
        btnExport.Attributes.Add("Onclick", "return validate()");

    }
    protected void BindCity()
    {
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        ddlCityName.DataBind();
    }
    private string GetSelectedCityValue()
    {
        string CityListValue = String.Empty;
        int i = 0;
        foreach (ListItem li in ddlCityName.Items)
        {
            if (li.Selected == true)
            {
                if (i == 0)
                {
                    CityListValue = CityListValue + li.Value;
                    i = 1;
                }
                else
                    CityListValue = CityListValue + "," + li.Value;
            }
        }
        return CityListValue;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        string City = GetSelectedCityValue();       
        string fromDate = txtFrom.Value;
        string toDate = txtTo.Value;
        //string cityId = ddlCityName.SelectedValue.ToString();
        string cityId = City;
        if (fromDate != "" && toDate != "" && cityId != "")
        {
            DataTable dtEx = new DataTable();
            DataTable dtCRD = new DataTable();
            DataTable dtTrips = new DataTable();

            dtEx = objAdmin.BookingDetailsRS(fromDate, toDate, cityId);
            dtTrips = objAdmin.BookingDetailsTrips(fromDate, toDate, cityId);
            dtCRD = objAdmin.BookingDetailsCRD(fromDate, toDate, cityId);

            Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=BookingDetails.xls");

            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";

            this.EnableViewState = false;
            Response.Write("\r\n");
            if (dtEx.Rows.Count > 0)
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                int[] iColumns = { 0, 1,50, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 };
                for (int i = 0; i < dtEx.Rows.Count; i++)
                //foreach (DataRow dr in dtEx.Rows)
                {
                    if (i == 0)
                    {
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingID") { Response.Write("<td><b>BookingID</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PickUpDate") { Response.Write("<td><b>PickUpDateTime</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PickTime") { Response.Write("<td><b>PickTime</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarNo") { Response.Write("<td><b>Car No</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarModel") { Response.Write("<td><b>Cab Model</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CarCatName") { Response.Write("<td><b>Category Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "vehicleCreationDate") { Response.Write("<td><b>Vehicle Creation Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "VendorName") { Response.Write("<td><b>Vender Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CountCarModel") { Response.Write("<td><b>No Of car model belong to that booking</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ChaufferName") { Response.Write("<td><b>Driver Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ChaufferContact") { Response.Write("<td><b>Driver Mobile</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ClientName") { Response.Write("<td><b>Client Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "GuestName") { Response.Write("<td><b>Customer Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CustomerContratNo") { Response.Write("<td><b>Customer Contact No</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Service") { Response.Write("<td><b>Service</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PickupAdd") { Response.Write("<td><b>PickupAddress</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CityName") { Response.Write("<td><b>Pickup City</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "dropoffaddress") { Response.Write("<td><b>Drop off addess</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "totalDistance") { Response.Write("<td><b>TotalDistance</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "bookingCreationBy") { Response.Write("<td><b>Booking Maid By Agent Name</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookedBy") { Response.Write("<td><b>Booking through</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingCreationDate") { Response.Write("<td><b>Booking Creation DateTime</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "datetimeout") { Response.Write("<td><b>Date out/ time & date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingStatus") { Response.Write("<td><b>Duty Status</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CancelBy") { Response.Write("<td><b>If CancelledBy</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CancelDate") { Response.Write("<td><b>If CancelledOn</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CancelReason") { Response.Write("<td><b>CancelationReason</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "DSCreatedBy") { Response.Write("<td><b>Duty Slip Created By</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ComplaintDescription") { Response.Write("<td><b>ComplaintType</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "EmailID") { Response.Write("<td><b>eMailID</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalCost") { Response.Write("<td><b>Total Revenue</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Taxes") { Response.Write("<td><b>Taxes</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "companyRevenueShare") { Response.Write("<td><b>Company Revenue share</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "CloseDate") { Response.Write("<td><b>Duty Closed Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PackageDetails") { Response.Write("<td><b>Package Details</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "BookingModifiedBy") { Response.Write("<td><b>Booking Modified By</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ModifyDate") { Response.Write("<td><b>Booking Modify Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ApprovalAmt") { Response.Write("<td><b>Approval Amount</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Outstation") { Response.Write("<td><b>Outstation YN</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PaymentMode") { Response.Write("<td><b>Mode of Payment</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalCarDuties") { Response.Write("<td><b>Used Car Quantity</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "NoOfDutiesInMonths") { Response.Write("<td><b>Days count</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "contractstartdate") { Response.Write("<td><b>Contract Start Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "contractEnddate") { Response.Write("<td><b>Contract End Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "initialcontract_Date") { Response.Write("<td><b>Contract  Date</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "OracleCarRegNo") { Response.Write("<td><b>Oracle Car RegNo</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ORACLECARCODE") { Response.Write("<td><b>Oracle car code</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Manager") { Response.Write("<td><b>Relationship Manager </b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AllocationDate") { Response.Write("<td><b>Allocation Date </b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Gender") { Response.Write("<td><b>Gender</b></td>"); }
                            if (dtEx.Columns[iColumns[j]].Caption.ToString() == "AssignBeforeTimeInMins") { Response.Write("<td><b>AssignBeforeTimeInMins</b></td>"); }
                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("<tr>");


                    string expression;
                    expression = "BookingID='" + dtEx.Rows[i]["BookingID"].ToString() + "'";
                    DataRow[] foundRows;
                    if (dtEx.Rows[i]["ClientName"].ToString().Trim() == "Trips" || dtEx.Rows[i]["ClientName"].ToString().Trim() == "COR-Retail" || dtEx.Rows[i]["ClientName"].ToString().Trim() == "Trips Alliance" || dtEx.Rows[i]["ClientName"].ToString().Trim() == "Cor Retail Alliance")
                    {
                        foundRows = dtTrips.Select(expression);
                    }
                    else
                    {
                        foundRows = dtCRD.Select(expression);
                    }

                    for (int j = 0; j < iColumns.Length; j++)
                    {

                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "companyRevenueShare")
                        {

                            if (foundRows.Length > 0)
                            {
                                Response.Write("<td>" + foundRows[0]["HertzShare"].ToString() + "</td>");
                            }
                            else
                            {
                                Response.Write("<td>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                            }

                        }
                        else
                        {
                            Response.Write("<td>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                        }
                    }
                    Response.Write("</tr>");

                }
                Response.Write("</table>");
                Response.End();
            }
            else
            {
                Response.Write("<table border = 1 align = 'center'  width = 100%> ");
                Response.Write("<td align='center'><b>No Record Found</b></td>");
                Response.Write("</table>");
                Response.End();
            }
        }

    }
}