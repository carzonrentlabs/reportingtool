using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ExcelUtil;

public partial class CorDriveReport_EasycabsDutyRegisterCab : System.Web.UI.Page
{
    DataSet dsEasycabsDutyRegister= new DataSet();
    DataSet dsExportToExcel = new DataSet();
    private CorDrive objCordrive = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["Userid"] = 3122;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

    public void BindGrid()
    {
        try
        {
            objCordrive = new CorDrive();
            dsEasycabsDutyRegister = objCordrive.GetEasycabsDutyRegisterCab(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()));
            if (dsEasycabsDutyRegister.Tables[0].Rows.Count > 0)
            {
                gvEasycabsDutyRegister.DataSource = dsEasycabsDutyRegister.Tables[0];
                gvEasycabsDutyRegister.DataBind();
            }
            else
            {
                gvEasycabsDutyRegister.DataSource = dsEasycabsDutyRegister.Tables[0];
                gvEasycabsDutyRegister.DataBind();
            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message.ToString();
            lblMsg.Visible = true;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtToDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('From Date and To Date can not be empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsEasycabsDutyRegister = objCordrive.GetEasycabsDutyRegisterCab(Convert.ToDateTime(txtFromDate.Text.Trim()), Convert.ToDateTime(txtToDate.Text.Trim()));
            WorkbookEngine.ExportDataSetToExcel(dsEasycabsDutyRegister, "EasycabsDutyRegister" + ".xls");
        }
    }
}
