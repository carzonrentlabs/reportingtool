﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using ReportingTool;

public partial class VVIPTracking : System.Web.UI.Page
{
    CorDrive Crdr = new CorDrive();
    clsAdmin objAdmin = new clsAdmin();
    string msgComp = string.Empty;
    DataSet ds = new DataSet();
    int IClientCoID, IClientCoIndivID;
    string VVIPUpdatedBy, Msg = String.Empty;
    DateTime LastUpdated_Value;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {


            BindClienCoName();

            string script = "$(document).ready(function () { $('[id*=btnSave]').click(); });";
            ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
        }

    }

    protected void BindClienCoName()
    {
        ddlClientCoName.DataTextField = "ClientConame";
        ddlClientCoName.DataValueField = "ClientCoID";
        DataSet dsClient = new DataSet();
        dsClient = Crdr.GetVVIPClientName();
        ddlClientCoName.DataSource = dsClient;
        ddlClientCoName.DataBind();
    }

    private void gridbind()
    {
        DataSet dsClientDetails = new DataSet();
        dsClientDetails = Crdr.GetVVIPClientDetails(Convert.ToInt32(ddlClientCoName.SelectedValue));
        if (dsClientDetails.Tables[0].Rows.Count > 0)
        {
            gvClientCoDetails.DataSource = dsClientDetails.Tables[0];
            gvClientCoDetails.DataBind();
        }
        else
        {
            gvClientCoDetails.DataSource = null;
            gvClientCoDetails.DataBind();
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('No Record Found !');</script>");

            return;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
       
    }


    public void ShowMessage(string message)
    {
        string strerrscript;
        strerrscript = "<script language='javascript'>alert('" + message + "');</script>";
        if ((!ClientScript.IsClientScriptBlockRegistered(strerrscript)))
        {
            Page.ClientScript.RegisterClientScriptBlock(typeof(String), "myScript", strerrscript);
        }
    }
    protected void gvClientCoDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        GridViewRow gvRow = e.Row;
        if (gvRow.RowType == DataControlRowType.DataRow)
        {
            CheckBox ChkRow = (CheckBox)gvRow.FindControl("chkRow");
            string hdnDutyStatus = ((HiddenField)gvRow.FindControl("hdIsVVIP")).Value;

            if (hdnDutyStatus == "True")
            {
               
                ChkRow.Checked = true;
                e.Row.BackColor = System.Drawing.Color.Silver;
               
            }
            else
            {
               

                ChkRow.Checked = false;
                e.Row.BackColor = System.Drawing.Color.LightGoldenrodYellow;
               


            }

          
        }
    }
    protected void btnSearcheVip_Click(object sender, EventArgs e)
    {
        if (ddlClientCoName.SelectedIndex == 0)
        {
            Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('Please select client name !');</script>");
            ddlClientCoName.Focus();
            return;
        }
        btnSave.Visible = true; 
        gridbind();

       
    }

    public void InitializeControls(Control objcontrol)
    {
        foreach (Control control in objcontrol.Controls)
        {
            if ((control.GetType() == typeof(TextBox)))
            {
                ((TextBox)(control)).Text = "";
            }

            if ((control.GetType() == typeof(DropDownList)))
            {
                if (((DropDownList)(control)).Items.Count > 0)
                {
                    ((DropDownList)(control)).ClearSelection();
                    ((DropDownList)(control)).SelectedIndex = 0;
                }
            }
            if ((control.GetType() == typeof(ListBox)))
            {
                if (((ListBox)(control)).Items.Count > 0)
                {
                    ((ListBox)(control)).ClearSelection();
                    ((ListBox)(control)).SelectedIndex = 0;
                }
            }

            if ((control.GetType() == typeof(CheckBox)))
            {
                ((CheckBox)(control)).Checked = false;
            }

            if (control.HasControls() && control.GetType() != typeof(MultiView))
            {
                InitializeControls(control);
            }


            if ((control.GetType() == typeof(GridView)))
            {
                ((GridView)(control)).DataSource = null;
                ((GridView)(control)).DataBind();
            }
            lblMessage.Text = "";
            btnSave.Visible = false;  
            // btnopenPopup.Visible = true;

        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        int Count = 0;
        int DCount = 0;
        try
        {
            if (gvClientCoDetails.Rows.Count == 0)
            {
                Page.RegisterStartupScript("ScriptDescription", "<script type=\"text/javascript\">alert('No data found to store !');</script>");
                return;
            }
            System.Threading.Thread.Sleep(1000);

            foreach (GridViewRow gvRow in gvClientCoDetails.Rows)
            {
                if (gvRow.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkKnockOff = (CheckBox)gvRow.FindControl("chkRow");
                    HiddenField ClientCoID = (HiddenField)gvRow.FindControl("hdClientCoId");
                    HiddenField ClientCoIndivID = (HiddenField)gvRow.FindControl("hdClientCoIndivId");

                    //if (chkKnockOff.Checked == true)
                    //{
                        VVIPUpdatedBy = Session["Userid"].ToString();
                        IClientCoID = Convert.ToInt32(ClientCoID.Value);
                        IClientCoIndivID = Convert.ToInt32(ClientCoIndivID.Value);

                        int active;

                        if (chkKnockOff.Checked & chkKnockOff != null)
                           // str += gvDetails.DataKeys[gvrow.RowIndex].Value.ToString() + ',';
                        { active = 1; }

                        else

                        { active = 0; }
                        DataSet DsChkStatus = new DataSet();
                        DsChkStatus = Crdr.GetVVIPClientStatusDetails(IClientCoID, IClientCoIndivID, active);

                        if (DsChkStatus.Tables[0].Rows[0]["MsgStatus"].ToString().ToUpper() == "OK")
                        {
                            Count = Count + 1;
                            ds = Crdr.UpDateClientcoIndivVIPStaus(IClientCoID, IClientCoIndivID, active, VVIPUpdatedBy);
                        }
                        if (DsChkStatus.Tables[0].Rows[0]["MsgStatus"].ToString().ToUpper() == "ERROR")
                        {
                            DCount = DCount + 1; ;
                        }
                    //}
                }
            }
            if (Count > 0)
            {
                ViewState["chkStatus"] = ds.Tables[0].Rows[0]["MsgStatus"].ToString().ToUpper();

                string msg = "";
                if (ViewState["chkStatus"].ToString() == "OK")
                {
                    msg = "Total " + Count.ToString() + " records updated successfully.";
                    lblMessage.Text = msg;
                    ShowMessage(msg);
                }
                else if (ViewState["chkStatus"].ToString() == "ERROR")
                {
                    msg = "No records Updated !";
                    lblMessage.Text = msg;
                    ShowMessage(msg);
                }
                else
                {
                    msg = "No records Updated !.";
                    lblMessage.Text = msg;
                    ShowMessage(msg);
                }

                gridbind();
            }

            if (Count==0 && DCount == gvClientCoDetails.Rows.Count)
            {
                string msg = "Select atleast one record to process !";
                ShowMessage(msg);
                lblMessage.Text = msg;
            }    
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
    }
    protected void ddlClientCoName_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnSave.Visible = false;
        gvClientCoDetails.DataSource = null;
        gvClientCoDetails.DataBind();

    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        InitializeControls(Form);
    }
}