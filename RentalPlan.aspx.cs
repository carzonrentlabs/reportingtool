﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ReportingTool;
using System.Text;
using System.IO;

public partial class RentalPlan : System.Web.UI.Page
{
    #region VariableDeclaration
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    private clsReport objReport = null;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        lblMessage.Text = "";
        try
        {
            if (!Page.IsPostBack)
            {
                BindYear(ddlYear);
                BindMonth(ddlMonth);
                BindCityName();
            }
        }
        catch (Exception)
        {

            throw;
        }
        bntGet.Attributes.Add("onclick", "return validate();");
        bntExprot.Attributes.Add("onclick", "return validate();");
    }
    private void BindCityName()
    {
        objReport = new clsReport();
        try
        {
            DataSet ds = objReport.GetLocations_UserAccessWiseBK(Convert.ToInt32(Session["UserID"]));
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCityName.DataTextField = "CityName";
                ddlCityName.DataValueField = "CityID";
                ddlCityName.DataSource = ds.Tables[0];
                ddlCityName.DataBind();
                ddlCityName.Items.Insert(0, new ListItem("--Select--", "0"));
                ddlCityName.Items.RemoveAt(1);

            }
            else
            {
                ddlCityName.DataSource = null;
                ddlCityName.DataBind();
            }
        }
        catch (Exception)
        {

            throw new Exception();
        }
    }
    private void BindMonth(DropDownList ddlMonth)
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlMonth.SelectedValue = System.DateTime.Now.Month.ToString();
        }
        catch (Exception)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2015; i <= 2016; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlYear.SelectedValue = System.DateTime.Now.Year.ToString();
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        if (ddlMonth.SelectedIndex <= 0 || ddlYear.SelectedIndex <= 0 || ddlCityName.SelectedIndex <=0 )
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Select months and years.')", true);
            return;
        }
        else
        {

            StringBuilder strData = new StringBuilder();
            strData = BindExcelData();
            ltlSummary.Text = strData.ToString();
        }
    }

    private StringBuilder BindExcelData()
    {
        StringBuilder strData = new StringBuilder();
        int Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
        int Year = Convert.ToInt32(ddlYear.SelectedValue);
        int cityid =Convert.ToInt32(ddlCityName.SelectedValue);

        objReport = new clsReport();
        DataSet dsDetails = objReport.GetRetnalPlan(Month, Year, cityid);
        DataView dv = new DataView(dsDetails.Tables[0]);
        List<string> MonthDays = new List<string>();

        try
        {

            if (dsDetails.Tables[0].Rows.Count > 0)
            {
                lblMessage.Text = "";
                strData.Append(" <html xmlns:v='urn:schemas-microsoft-com:vml' ");
                strData.Append(" xmlns:o='urn:schemas-microsoft-com:office:office' ");
                strData.Append(" xmlns:x='urn:schemas-microsoft-com:office:excel' ");
                strData.Append(" xmlns='http://www.w3.org/TR/REC-html40'> ");
                strData.Append(" <head> ");
                strData.Append(" <meta http-equiv=Content-Type content='text/html; charset=windows-1252'> ");
                strData.Append(" <meta name=ProgId content=Excel.Sheet> ");
                strData.Append(" <meta name=Generator content='Microsoft Excel 12'> ");
                strData.Append(" <link rel=File-List href='RentalPlan_files/filelist.xml'> ");
                strData.Append(" <!--[if !mso]> ");
                strData.Append(" <style> ");
                strData.Append(" v:* {behavior:url(#default#VML);} ");
                strData.Append(" o:* {behavior:url(#default#VML);} ");
                strData.Append(" x:* {behavior:url(#default#VML);} ");
                strData.Append(" .shape {behavior:url(#default#VML);} ");
                strData.Append(" </style> ");
                strData.Append(" <![endif]--> ");
                strData.Append(" <style id='Q1 Rentals Plan April -may - June FY 2016-17.xls_17484_Styles'> ");
                strData.Append(" <!--table ");
                strData.Append(" {mso-displayed-decimal-separator:'.'; ");
                strData.Append(" mso-displayed-thousand-separator:',';} ");
                strData.Append(" .font017484 ");
                strData.Append(" {color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0;} ");
                strData.Append(" .xl1517484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:general; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6317484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#99CC00; ");
                strData.Append(" mso-pattern:#99CC00 none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6417484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:yellow none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6517484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6617484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:underline; ");
                strData.Append(" text-underline-style:single; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6717484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl6817484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#99CC00; ");
                strData.Append(" mso-pattern:#99CC00 none; ");
                strData.Append(" white-space:normal;} ");
                strData.Append(" .xl6917484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:normal;} ");
                strData.Append(" .xl7017484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:normal;} ");
                strData.Append(" .xl7117484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:normal;} ");
                strData.Append(" .xl7217484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7317484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:yellow none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7417484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:Percent; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7517484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#99CC00; ");
                strData.Append(" mso-pattern:#99CC00 none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7617484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:yellow none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7717484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#99CC00; ");
                strData.Append(" mso-pattern:#99CC00 none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7817484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:yellow none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl7917484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:Percent; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:yellow; ");
                strData.Append(" mso-pattern:yellow none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8017484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");

                strData.Append(" .xl8117484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:windowtext; ");
                strData.Append(" font-size:10.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" width: 100%;");
                strData.Append(" white-space:nowrap;} ");

                strData.Append(" .xl8217484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8317484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:white; ");
                strData.Append(" mso-pattern:white none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8417484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8517484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:middle; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8617484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#99CC00; ");
                strData.Append(" mso-pattern:#99CC00 none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8717484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#339966; ");
                strData.Append(" mso-pattern:#339966 none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8817484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:0; ");
                strData.Append(" text-align:left; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" background:#339966; ");
                strData.Append(" mso-pattern:#339966 none; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" .xl8917484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:black; ");
                strData.Append(" font-size:9.0pt; ");
                strData.Append(" font-weight:700; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri; ");
                strData.Append(" mso-generic-font-family:auto; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" border:.5pt solid black; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:normal;} ");
                strData.Append(" .xl9017484 ");
                strData.Append(" {padding:0px; ");
                strData.Append(" mso-ignore:padding; ");
                strData.Append(" color:windowtext; ");
                strData.Append(" font-size:11.0pt; ");
                strData.Append(" font-weight:400; ");
                strData.Append(" font-style:normal; ");
                strData.Append(" text-decoration:none; ");
                strData.Append(" font-family:Calibri, sans-serif; ");
                strData.Append(" mso-font-charset:0; ");
                strData.Append(" mso-number-format:General; ");
                strData.Append(" text-align:center; ");
                strData.Append(" vertical-align:bottom; ");
                strData.Append(" mso-background-source:auto; ");
                strData.Append(" mso-pattern:auto; ");
                strData.Append(" white-space:nowrap;} ");
                strData.Append(" --> ");
                strData.Append(" </style> ");
                strData.Append(" </head> ");
                strData.Append(" <body> ");
                strData.Append(" <!--[if !excel]>&nbsp;&nbsp;<![endif]--> ");
                strData.Append(" <!--The following information was generated by Microsoft Office Excel's Publish ");
                strData.Append(" as Web Page wizard.--> ");
                strData.Append(" <!--If the same item is republished from Excel, all information between the DIV ");
                strData.Append(" tags will be replaced.--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <div id='Q1 Rentals Plan April -may - June FY 2016-17.xls_17484' align=center ");
                strData.Append(" x:publishsource='Excel'> ");
                strData.Append(" <table border=0 cellpadding=0 cellspacing=0 width=1875 style='border-collapse: ");
                strData.Append(" collapse;table-layout:auto;width:1407pt'> ");
                strData.Append(" <col width=213 style='mso-width-source:userset;mso-width-alt:7789;width:160pt'> ");
                strData.Append(" <col width=69 style='mso-width-source:userset;mso-width-alt:2523;width:52pt'> ");
                strData.Append(" <col width=57 style='mso-width-source:userset;mso-width-alt:2084;width:43pt'> ");
                strData.Append(" <col width=64 span=24 style='width:62pt'> ");
                strData.Append(" <tr class=xl1517484 height=37 style='mso-height-source:userset;height:27.75pt'> ");
                strData.Append(" <td height=37 class=xl8917484 width=213 style='height:27.75pt;width:160pt'><span ");
                strData.Append(" style='mso-spacerun:yes'> </span></td> ");
                strData.Append(" <td class=xl6317484 width=69 style='border-left:none;width:52pt'>&nbsp;</td> ");
                strData.Append(" <td class=xl8217484 width=57 style='border-left:none;width:43pt'>&nbsp;</td> ");
                strData.Append(" <td class=xl8217484 width=64 style='border-left:none;width:62pt'>&nbsp;</td> ");
                DataSet dsDays = new DataSet();
                dsDays = objReport.GetMonthName(Month, Year);
                int count = 0;
                if (dsDays.Tables[0].Rows.Count > 0)
                {
                    count = dsDays.Tables[0].Rows.Count;
                    foreach (DataRow dr in dsDays.Tables[0].Rows)
                    {
                        MonthDays.Add(Year + "-" + String.Format("{0:00}", Month) + "-" + String.Format("{0:00}", Convert.ToInt16(dr["dateDays"])));
                        strData.Append(" <td class=xl8417484 width=64 style='border-left:none;width:62pt'>" + dr["WeekDayName"] + "</td> ");

                        if (Convert.ToInt16(dr["dateDays"]) == 8 || Convert.ToInt16(dr["dateDays"]) == 16 || Convert.ToInt16(dr["dateDays"]) == 24)
                        {
                            strData.Append(" <td class=xl6417484 width=64 style='border-left:none;width:62pt'>8</td> ");
                            strData.Append(" <td class=xl6417484 width=64 style='border-left:none;width:62pt'>8</td> ");
                        }
                        else if (count == Convert.ToInt16(dr["dateDays"]))
                        {
                            strData.Append(" <td class=xl6417484 width=64 style='border-left:none;width:62pt'>" + (Convert.ToInt16(dr["dateDays"]) - 24) + "</td> ");
                            strData.Append(" <td class=xl6417484 width=64 style='border-left:none;width:62pt'>" + (Convert.ToInt16(dr["dateDays"]) - 24) + "</td> ");
                        }
                    }
                }
                int totladays = MonthDays.Count;

                strData.Append(" <td class=xl8317484 width=64 style='border-left:none;width:62pt'>&nbsp;</td> ");
                strData.Append(" <td class=xl8317484 width=64 style='border-left:none;width:62pt'>&nbsp;</td> ");
                strData.Append(" <td class=xl8217484 width=64 style='border-left:none;width:62pt'>&nbsp;</td> ");
                strData.Append(" </tr> ");

                string CityName = "";
                int TotalActualTotal = 0, TotalActualGrandTotal = 0, TotalProjectedTotal = 0, TotalProjectedGrandTotal = 0;
                int counttemp = 0, BookingTargetTotal = 0, BookingTargetGrandTotal = 0;

                int Week1AchievedTotal = 0, Week1AchievedGrandTotal = 0, Week2AchievedTotal = 0, Week2AchievedGrandTotal = 0
                    , Week3AchievedTotal = 0, Week3AchievedGrandTotal = 0, Week4AchievedTotal = 0, Week4AchievedGrandTotal = 0;

                int Week1ProjectedTotal = 0, Week1ProjectedGrandTotal = 0, Week2ProjectedTotal = 0, Week2ProjectedGrandTotal = 0
                    , Week3ProjectedTotal = 0, Week3ProjectedGrandTotal = 0, Week4ProjectedTotal = 0, Week4ProjectedGrandTotal = 0;
                int[] SumTotal = new int[totladays];
                int[] GrandTotal = new int[totladays];
                int j = 0, totalRecord = 0;
                foreach (DataRow drDetails in dsDetails.Tables[0].Rows) //main loop start
                {
                    j = 0;
                    totalRecord += 1;
                    //sub total start   

                    /* below code commented due to duplicate calculation on date 28 April 2016

                      if (CityName != drDetails["cityName"].ToString() && CityName != "")
                      {
                       
                          BookingTargetGrandTotal = BookingTargetGrandTotal + BookingTargetTotal;

                          TotalActualGrandTotal = TotalActualGrandTotal + TotalActualTotal;
                          TotalProjectedGrandTotal = TotalProjectedGrandTotal + TotalProjectedTotal;

                          Week1AchievedGrandTotal = Week1AchievedGrandTotal + Week1AchievedTotal;
                          Week2AchievedGrandTotal = Week2AchievedGrandTotal + Week2AchievedTotal;
                          Week3AchievedGrandTotal = Week3AchievedGrandTotal + Week3AchievedTotal;
                          Week4AchievedGrandTotal = Week4AchievedGrandTotal + Week4AchievedTotal;

                          Week1ProjectedGrandTotal = Week1ProjectedGrandTotal + Week1ProjectedTotal;
                          Week2ProjectedGrandTotal = Week2ProjectedGrandTotal + Week2ProjectedTotal;
                          Week3ProjectedGrandTotal = Week3ProjectedGrandTotal + Week3ProjectedTotal;
                          Week4ProjectedGrandTotal = Week4ProjectedGrandTotal + Week4ProjectedTotal;


                          strData.Append("<tr class=xl1517484 height=20 style='height:15.0pt'>");
                          strData.Append("<td height=20 class=xl7617484 style='height:15.0pt;border-top:none'><span style='mso-spacerun:yes'></span>TOTAL</td>");
                          //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>12</td> ");
                          for (int i = 0; i <= totladays -1 ; i++)
                          {
                              if (i == 0)
                              {
                                  strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + BookingTargetTotal.ToString() + "</td> ");
                                  strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                                  strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                              }
                              else
                              {
                                
                                  if (i == 8)
                                  {
                                      //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + SumTotal[j] + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week1AchievedTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week1ProjectedTotal.ToString() + "</td> ");
                                      j += 1;
                                  }
                                  if (i == 16)
                                  {
                                      //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week2AchievedTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week2ProjectedTotal.ToString() + "</td> ");
                                  }
                                  if (i == 24)
                                  {
                                      //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week3AchievedTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week3ProjectedTotal.ToString() + "</td> ");
                                  }
                                  if (i == totladays - 1)
                                  {
                                      strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + SumTotal[j] + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week4AchievedTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week4ProjectedTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalProjectedTotal.ToString() + "</td> ");
                                      strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                                      j += 1;
                                  }
                                  else
                                  {
                                      strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + SumTotal[j] + "</td> ");
                                      j += 1;
                                  }
                                
                              }
                          }
                          strData.Append(" </tr> ");
                          TotalActualTotal = 0;
                          TotalProjectedTotal = 0;
                          BookingTargetTotal = 0;

                          Week1ProjectedTotal = 0;
                          Week2ProjectedTotal = 0;
                          Week3ProjectedTotal = 0;
                          Week4ProjectedTotal = 0;

                          Week1AchievedTotal = 0;
                          Week2AchievedTotal = 0;
                          Week3AchievedTotal = 0;
                          Week4AchievedTotal = 0;
                          SumTotal = new int[totladays];
                      }
                     End of duplicate calculation  28 April 2016*/
                    //sub total end

                    //new city start with displaying city name start
                    if (CityName != drDetails["cityName"].ToString())
                    {

                        strData.Append(" <tr class=xl1517484 height=24 style='mso-height-source:userset;height:18.0pt'> ");
                        //strData.Append(" <td height=24 class=xl6617484 style='height:18.0pt;border-top:none'>Delhi &amp; NCR Vikram Malhotra</td> ");
                        strData.Append(" <td height=24 class=xl6617484 style='height:18.0pt;border-top:none'>" + drDetails["cityName"] + "</td> ");
                        //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>12</td> ");
                        for (int i = 0; i <= totladays + 13; i++)
                        {
                            strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                        }
                        strData.Append(" </tr> ");
                    }
                    //new city start with displaying city name end

                    //column heading start
                    if (CityName == "")
                    {
                        strData.Append("<tr class=xl1517484 height=52 style='mso-height-source:userset;height:39.0pt'> ");
                        strData.Append("<td height=52 class=xl6717484 style='height:39.0pt;border-top:none'>Person Responsible</td> ");
                        strData.Append("<td class=xl6817484 style='border-top:none;border-left:none; width:52pt'>Total no of bookings daily target</td> ");
                        strData.Append("<td class=xl6917484 style='border-top:none;border-left:none; width:43pt'>Client booking potential</td> ");
                        strData.Append("<td class=xl6917484 style='border-top:none;border-left:none; width:62pt'>Average booking per day</td> ");

                        int weekcount = 1;
                        foreach (string daysname in MonthDays)
                        {
                            if (counttemp == 8 || counttemp == 16 || counttemp == 24)
                            {
                                strData.Append(" <td class=xl7117484 width=64 style='border-top:none;border-left:none; width:62pt'>WK-" + weekcount.ToString() + " (Achieved)</td> ");
                                strData.Append(" <td class=xl7117484 width=64 style='border-top:none;border-left:none; width:62pt'>WK-" + weekcount.ToString() + " (Projections)</td> ");
                                strData.Append(" <td class=xl7017484 width=64 style='border-top:none;border-left:none; width:62pt; white-space: nowrap;'>" + daysname + "</td> ");
                                weekcount = weekcount + 1;
                            }
                            else if (counttemp == totladays - 1)
                            {
                                strData.Append(" <td class=xl7017484 width=64 style='border-top:none;border-left:none; width:62pt; white-space: nowrap;'>" + daysname + "</td> ");
                                strData.Append(" <td class=xl7117484 width=64 style='border-top:none;border-left:none; width:62pt'>WK-" + weekcount.ToString() + " (Achieved)</td> ");
                                strData.Append(" <td class=xl7117484 width=64 style='border-top:none;border-left:none; width:62pt'>WK-" + weekcount.ToString() + " (Projections)</td> ");
                            }
                            else
                            {
                                strData.Append(" <td class=xl7017484 width=64 style='border-top:none;border-left:none; ");
                                strData.Append(" width:62pt; white-space: nowrap;'>" + daysname + "</td> ");
                            }
                            counttemp += 1;
                        }

                        strData.Append(" <td class=xl6917484 width=64 style='border-top:none;border-left:none; ");
                        strData.Append(" width:62pt'>Actual Cummulative " + ddlMonth.SelectedItem.Text.ToString().Substring(0, 3) + "'" + ddlYear.SelectedItem.Text + "</td> ");
                        strData.Append(" <td class=xl6917484 width=64 style='border-top:none;border-left:none; ");
                        strData.Append(" width:62pt'>Projected Cummulative " + ddlMonth.SelectedItem.Text.ToString().Substring(0, 3) + "'" + ddlYear.SelectedItem.Text + "</td> ");
                        strData.Append(" <td class=xl7117484 width=64 style='border-top:none;border-left:none; ");
                        strData.Append(" width:62pt'>% of Achivement by Individuals<span ");
                        strData.Append(" style='mso-spacerun:yes'> </span></td> ");
                        strData.Append(" </tr> ");
                    }
                    //column heading end

                    counttemp = 0;


                    //rows data start
                    strData.Append(" <tr class=xl1517484 height=20 style='height:15.0pt'> ");
                    strData.Append(" <td height=20 class=xl8117484 style='height:15.0pt;border-top:none'>" + drDetails["PersonResponsible"] + " (" + drDetails["Clientconame"] + ") " + "</td>");
                    BookingTargetTotal = BookingTargetTotal + Convert.ToInt32(drDetails["BookingTarget"]);
                    TotalActualTotal = TotalActualTotal + Convert.ToInt32(drDetails["TotalCount"]);
                    TotalProjectedTotal = TotalProjectedTotal + Convert.ToInt32(drDetails["TotalProjected"]);

                    Week1AchievedTotal = Week1AchievedTotal + Convert.ToInt32(drDetails["FirstWeek"]);
                    Week2AchievedTotal = Week2AchievedTotal + Convert.ToInt32(drDetails["SecondWeek"]);
                    Week3AchievedTotal = Week3AchievedTotal + Convert.ToInt32(drDetails["ThirdWeek"]);
                    Week4AchievedTotal = Week4AchievedTotal + Convert.ToInt32(drDetails["FourthWeek"]);

                    Week1ProjectedTotal = Week1ProjectedTotal + Convert.ToInt32(drDetails["FirstWeekProjected"]);
                    Week2ProjectedTotal = Week2ProjectedTotal + Convert.ToInt32(drDetails["SecondWeekProjected"]);
                    Week3ProjectedTotal = Week3ProjectedTotal + Convert.ToInt32(drDetails["ThirdWeekProjected"]);
                    Week4ProjectedTotal = Week4ProjectedTotal + Convert.ToInt32(drDetails["FourthWeekProjected"]);

                    strData.Append(" <td class=xl7517484 style='border-top:none;border-left:none'>" + drDetails["BookingTarget"] + "</td> ");
                    strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + drDetails["AvgBooking"] + "</td> ");


                    //start Fetch data
                    if (MonthDays.Count > 0)
                    {
                        foreach (string date in MonthDays)
                        {
                            if (counttemp == 8 || counttemp == 16 || counttemp == 24)
                            {
                                if (counttemp == 8)
                                {
                                    strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>" + drDetails["FirstWeek"] + "</td> ");
                                    strData.Append(" <td class=xl8017484 style='border-top:none;border-left:none'>" + drDetails["FirstWeekProjected"] + "</td> ");
                                    //weeklyCount = Convert.ToInt32(drDetails["FirstWeek"]);
                                }
                                else if (counttemp == 16)
                                {
                                    strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>" + drDetails["SecondWeek"] + "</td> ");
                                    strData.Append(" <td class=xl8017484 style='border-top:none;border-left:none'>" + drDetails["SecondWeekProjected"] + "</td> ");
                                    //weeklyCount = Convert.ToInt32(drDetails["SecondWeek"]);
                                }
                                else if (counttemp == 24)
                                {
                                    strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>" + drDetails["ThirdWeek"] + "</td> ");
                                    strData.Append(" <td class=xl8017484 style='border-top:none;border-left:none'>" + drDetails["ThirdWeekProjected"] + "</td> ");
                                    //weeklyCount = Convert.ToInt32(drDetails["ThirdWeek"]);
                                }
                                //strData.Append(" <td class=xl8017484 style='border-top:none;border-left:none'>" + (Convert.ToInt32(drDetails["BookingTarget"]) * 8).ToString() + "</td> ");
                                strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + drDetails[date] + "</td> ");
                                SumTotal[counttemp] = SumTotal[counttemp] + Convert.ToInt32(drDetails[date]);
                                GrandTotal[counttemp] = GrandTotal[counttemp] + Convert.ToInt32(drDetails[date]);

                                //TotalActual = TotalActual + weeklyCount;
                                //TotalActualSub = TotalActualSub + weeklyCount;

                                //TotalProjected = TotalProjected + (Convert.ToInt32(drDetails["BookingTarget"]) * 8);
                                //TotalProjectedSub = TotalProjectedSub + (Convert.ToInt32(drDetails["BookingTarget"]) * 8);
                                //weeklyCount = 0;

                            }
                            else if (counttemp == MonthDays.Count - 1)
                            {
                                //weeklyCount = AddValue(drDetails[date].ToString(), weeklyCount);

                                //TotalActual = TotalActual + Convert.ToInt32(drDetails["FourthWeek"]);
                                //TotalActualSub = TotalActualSub + Convert.ToInt32(drDetails["FourthWeek"]);

                                //TotalProjected = Convert.ToInt32(drDetails["TotalProjected"]); //TotalProjected + (Convert.ToInt32(drDetails["BookingTarget"]) * (counttemp - 23));
                                //TotalProjectedSub = TotalProjectedSub + (Convert.ToInt32(drDetails["BookingTarget"]) * (counttemp - 23));


                                strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + drDetails[date] + "</td> ");
                                strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>" + drDetails["FourthWeek"] + "</td> ");
                                //strData.Append(" <td class=xl8017484 style='border-top:none;border-left:none'>" + (Convert.ToInt32(drDetails["BookingTarget"]) * (counttemp - 23)).ToString() + "</td> ");
                                strData.Append(" <td class=xl8017484 style='border-top:none;border-left:none'>" + drDetails["FourthWeekProjected"] + "</td> ");
                                strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + drDetails["TotalCount"] + "</td> ");
                                strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + drDetails["TotalProjected"] + "</td> ");
                                strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + drDetails["Achivement"] + "%</td> ");
                                SumTotal[counttemp] = SumTotal[counttemp] + Convert.ToInt32(drDetails[date]);
                                GrandTotal[counttemp] = GrandTotal[counttemp] + Convert.ToInt32(drDetails[date]);
                                //weeklyCount = 0;
                            }
                            else
                            {
                                strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + drDetails[date] + "</td> ");
                                SumTotal[counttemp] = SumTotal[counttemp] + Convert.ToInt32(drDetails[date]);
                                GrandTotal[counttemp] = GrandTotal[counttemp] + Convert.ToInt32(drDetails[date]);
                            }
                            //weeklyCount = AddValue(drDetails[date].ToString(), weeklyCount);
                            counttemp += 1;
                        }
                    } //End Fetch Data
                    strData.Append(" </tr> ");
                    //rows data end

                    //sub total start
                    j = 0;
                    if ((CityName != drDetails["cityName"].ToString() && CityName != "") || (totalRecord == dsDetails.Tables[0].Rows.Count))
                    {
                        strData.Append("<tr class=xl1517484 height=20 style='height:15.0pt'>");
                        strData.Append("<td height=20 class=xl7617484 style='height:15.0pt;border-top:none'><span style='mso-spacerun:yes'></span>TOTAL</td>");


                        for (int i = 0; i <= totladays - 1; i++)
                        {
                            if (i == 0)
                            {
                                strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + BookingTargetTotal.ToString() + "</td> ");
                                strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                                strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                            }
                            else
                            {
                                if (i == 8)
                                {
                                    //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + SumTotal[j] + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week1AchievedTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week1ProjectedTotal.ToString() + "</td> ");
                                    j += 1;
                                }
                                if (i == 16)
                                {
                                    //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week2AchievedTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week2ProjectedTotal.ToString() + "</td> ");
                                }
                                if (i == 24)
                                {
                                    //strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week3AchievedTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week3ProjectedTotal.ToString() + "</td> ");
                                }
                                if (i == totladays - 1)
                                {
                                    strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + SumTotal[j] + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week4AchievedTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + Week4ProjectedTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalActualTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>" + TotalProjectedTotal.ToString() + "</td> ");
                                    strData.Append(" <td class=xl7317484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                                    j += 1;
                                }
                                else
                                {
                                    strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + SumTotal[j] + "</td> ");
                                    j += 1;
                                }
                            }
                        }
                        strData.Append(" </tr> ");
                        BookingTargetGrandTotal = BookingTargetGrandTotal + BookingTargetTotal;
                        TotalActualGrandTotal = TotalActualGrandTotal + TotalActualTotal;
                        TotalProjectedGrandTotal = TotalProjectedGrandTotal + TotalProjectedTotal;


                        Week1ProjectedGrandTotal = Week1ProjectedGrandTotal + Week1ProjectedTotal;
                        Week2ProjectedGrandTotal = Week2ProjectedGrandTotal + Week2ProjectedTotal;
                        Week3ProjectedGrandTotal = Week3ProjectedGrandTotal + Week3ProjectedTotal;
                        Week4ProjectedGrandTotal = Week4ProjectedGrandTotal + Week4ProjectedTotal;

                        Week1AchievedGrandTotal = Week1AchievedGrandTotal + Week1AchievedTotal;
                        Week2AchievedGrandTotal = Week2AchievedGrandTotal + Week2AchievedTotal;
                        Week3AchievedGrandTotal = Week3AchievedGrandTotal + Week3AchievedTotal;
                        Week4AchievedGrandTotal = Week4AchievedGrandTotal + Week4AchievedTotal;


                        BookingTargetTotal = 0;
                        TotalActualTotal = 0;
                        TotalProjectedTotal = 0;

                        Week1ProjectedTotal = 0;
                        Week2ProjectedTotal = 0;
                        Week3ProjectedTotal = 0;
                        Week4ProjectedTotal = 0;

                        Week1AchievedTotal = 0;
                        Week2AchievedTotal = 0;
                        Week3AchievedTotal = 0;
                        Week4AchievedTotal = 0;

                    }
                    ////sub total end

                    CityName = drDetails["cityName"].ToString();
                } //main loop end

                //blank line before grand total start
                strData.Append(" <tr class=xl1517484 height=24 style='mso-height-source:userset;height:18.0pt'>");
                strData.Append(" <td height=24 class=xl6617484 style='height:18.0pt;border-top:none'>&nbsp;</td> ");
                for (int i = 0; i <= totladays + 13; i++)
                {
                    strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                }
                strData.Append(" </tr> ");
                //blank line before grand total end

                //Grand Total start
                strData.Append(" <tr class=xl7617484 height=20 style='height:15.0pt'> ");
                //strData.Append(" <td height=20 class=xl7317484 style='height:15.0pt;border-top:none'>All India TOTAL</td> ");
                strData.Append("<td height=20 class=xl7617484 style='height:15.0pt;border-top:none'><span style='mso-spacerun:yes'></span>All India TOTAL</td>");
                j = 0;
                for (int i = 0; i <= totladays - 1; i++)
                {
                    if (i == 0)
                    {
                        strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + BookingTargetGrandTotal.ToString() + "</td> ");
                        strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                        strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                    }
                    else
                    {
                        if (i == 8)
                        {
                            strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + GrandTotal[j] + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week1AchievedGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week1ProjectedGrandTotal.ToString() + "</td> ");
                            j += 1;
                        }
                        if (i == 16)
                        {
                            //strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week2AchievedGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week2ProjectedGrandTotal.ToString() + "</td> ");
                        }
                        if (i == 24)
                        {
                            //strData.Append(" <td class=xl8217484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week3AchievedGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week3ProjectedGrandTotal.ToString() + "</td> ");
                        }
                        if (i == totladays - 1)
                        {
                            strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + GrandTotal[j] + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week4AchievedGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + Week4ProjectedGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + TotalActualGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>" + TotalProjectedGrandTotal.ToString() + "</td> ");
                            strData.Append(" <td class=xl7717484 style='border-top:none;border-left:none'>&nbsp;</td> ");
                            j += 1;

                        }
                        else
                        {
                            strData.Append(" <td class=xl8417484 style='border-top:none;border-left:none'>" + GrandTotal[j] + "</td> ");
                            j += 1;
                        }
                    }
                }
                strData.Append(" </tr> ");
                //Grand Total end
                strData.Append(" </table> ");
                strData.Append(" </div> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" <!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD--> ");
                strData.Append(" <!-----------------------------> ");
                strData.Append(" </body> ");
                strData.Append(" </html> ");
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Record not available";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return strData;
    }

    public int AddValue(string Count, int Total)
    {
        //if (!string.IsNullOrEmpty(Count))
        //{
        Total = Total + Convert.ToInt32(Count);
        //}
        return Total;
    }

    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "RentalPlan";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();

    }
}