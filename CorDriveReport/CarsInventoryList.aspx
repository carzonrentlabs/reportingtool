﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CarsInventoryList.aspx.cs" Inherits="CorDriveReport_CarsInventoryList"  %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <link href="../greybox/gb_styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#<%=gvCarInventryList.ClientID%> a[id*='btnInsurance']").click(function () {
                // debugger;
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnInsurance", "hdInsurance")
                var hdImageID = $("#" + hdImageID).val();
                //alert(hdImageID);
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/VendorUpload/VendorCar/" + hdImageID + "/" + "Insurancecopy.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/39575/" + "Resume.pdf";
                    //alert(url);
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });

        $(function () {
            $("#<%=gvCarInventryList.ClientID%> a[id*='btnRC']").click(function () {
                //debugger;
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnRC", "hdRC")
                var hdImageID = $("#" + hdImageID).val();
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/VendorUpload/VendorCar/" + hdImageID + "/" + "RegistrationCopy.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/39575/" + "Resume.pdf";
                    //alert(url);
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
            });
        });

        $(function () {
            $("#<%=gvCarInventryList.ClientID%> a[id*='btnPermit']").click(function () {
                // debugger;
                var caption = "View Document";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdImageID = linkId.replace("btnPermit", "hdPermit")
                var hdImageID = $("#" + hdImageID).val();
                var clientCoindivId = hdImageID.split("_");

                if (hdImageID == 0) {

                    var url = "../Reports/FileNotFound.aspx"
                }
                else {
                    //var url = "http://insta.carzonrent.com/selfdrive/UploadImage/"+hdImageID.toString();
                    // alert(url)
                    var url = "http://insta.carzonrent.com/VendorUpload/VendorCar/" + hdImageID + "/" + "Annexure.pdf";
                    //var url = "http://insta.carzonrent.com/Vendorupload/chauffeur/39575/" + "Resume.pdf";
                    //                    alert(url);
                    //Myles Invoice Print link
                    //var url = "http://insta.carzonrent.com/SelfDrive/Invoice/SDPrintRentalAgreement.aspx?BookingID=6019745&WebPrint=1"
                }
                return GB_showFullScreen(caption, url)
                //return GB_showCenter(caption, url, 400, 500, callback_fn)
                //return GB_showImage(caption, url, callback_fn)
            });
        });

        function callback_fn() {
            alert('callback function');
        }
    </script>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td colspan="12" align ="center">
                        <b>Cars in Inventory List</b>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblCityName" runat="server">City Name&nbsp; </asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlCity" runat="server">
                        </asp:DropDownList>&nbsp; 
                    </td>
                    <td align="left">
                        <asp:Label ID="lblStatus" runat="server">Status</asp:Label>&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                            <asp:ListItem Value="2" Text="DeActive"></asp:ListItem>
                        </asp:DropDownList>&nbsp; 
                    </td>
                    <td align="left">
                        &nbsp;<asp:Label ID="lblCarModel" runat="server">Attachment Model</asp:Label>&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlCarModel" runat="server">
                        </asp:DropDownList>&nbsp;
                    </td>
                    <td align="left">
                        <asp:Label ID="lblVDP" runat="server">VDP (Y/N)</asp:Label>&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlVdpYN" runat="server">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="2" Text="No"></asp:ListItem>
                        </asp:DropDownList>&nbsp; 
                    </td>
                    <td align="left">
                        <asp:Label ID="lbl_CarAge" runat="server">Age of Car </asp:Label>&nbsp;
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlCarAge" runat="server">
                            <asp:ListItem Value="0" Text="All"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Greater Than 4 Years"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Less Than 3 Years"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Less Than 2 Years"></asp:ListItem>
                        </asp:DropDownList>&nbsp; 
                    </td>
                    <td align="left">
                        <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />&nbsp;
                    </td>
                    <td align="left">
                        <asp:Button ID="bntExprot" runat="server" Text="Exprot To Excel" OnClick="bntExprot_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <div class="Repeater" style="width:1450px; height: 600px; overflow: auto;">
            <asp:GridView ID="gvCarInventryList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                PageSize="100" OnPageIndexChanging="gvCarInventryList_PageIndexChanging" CellSpacing ="2" CellPadding ="2">
                <Columns>
                    <asp:TemplateField HeaderText="Sr.No" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblSrtNo" runat="server" Text='<% # Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Model" ItemStyle-Width="150px" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblCarModelName" runat="server" Text='<% # Eval("CarModelName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblCarCatName" runat="server" Text='<% # Eval("CarCatName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vehicle No" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblRegnNo" runat="server" Text='<% # Eval("RegnNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblCarVendorName" runat="server" Text='<% # Eval("CarVendorName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City">
                        <ItemTemplate>
                            <asp:Label ID="lblCityName" runat="server" Text='<% # Eval("CityName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VDP YN" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblVDPYN" runat="server" Text='<% # Eval("VDPYN") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<% # Eval("CarStatus") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Created Date" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblVehicleCreateDate" runat="server" Text='<% # Eval("VehicleCreateDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Modified Date" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDateofModifcation" runat="server" Text='<% # Eval("DateofModifcation", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Date" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDateofVendorCreationDate" runat="server" Text='<% # Eval("DateofVendorCreationDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attach Model" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAttachmentModel" runat="server" Text='<% # Eval("AttachmentModel") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Grade">
                        <ItemTemplate>
                            <asp:Label ID="lblGrade" runat="server" Text='<% # Eval("Grade") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Parking Location" ItemStyle-Wrap="true" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblParkingLocation" runat="server" Text='<% # Eval("ParkingLocation") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="COR Drive Status" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblCorDriveStatus" runat="server" Text='<% # Eval("CorDriveStatus") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GPS Status" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblGPSStatus" runat="server" Text='<% # Eval("GPSStatus") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date of De-Activation" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDateOfDeactivation" runat="server" Text='<% # Eval("DateOfDeactivation", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Doument Uploaded by" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDocumentUploadedBy" runat="server" Text='<% # Eval("DocumentUploadedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Doument Approved by" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDocumentApprovedBy" runat="server" Text='<% # Eval("DocumentApprovedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date of Modification" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDateofModification" runat="server" Text='<% # Eval("DateofModification", "{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Deactivated by" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbldeactivatedBy" runat="server" Text='<% # Eval("deactivatedBy") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Age of car as on Date" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAgeOfCar" runat="server" Text='<% # Eval("AgeOfCar") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Status" runat="server" Text='<% # Eval("CarStatus") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Car RC" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdRC" runat="server" Value='<%#Eval("VendorCarID") %>' />
                            <asp:LinkButton ID="btnRC" runat="server" Text="View"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Car Insurance" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdInsurance" runat="server" Value='<%#Eval("VendorCarID") %>' />
                            <asp:LinkButton ID="btnInsurance" runat="server" Text="View"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Car Permit" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdPermit" runat="server" Value='<%#Eval("VendorCarID") %>' />
                            <asp:LinkButton ID="btnPermit" runat="server" Text="View"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    
</asp:Content>
