using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReportingTool;

public partial class VendorReportDayWiseCrystal : System.Web.UI.Page
{
    string strFrom;
    string strTo;
    int strcityid;

    clsAdmin objAdmin = new clsAdmin();

    ReportDocument VendorUtilizationSummary = new ReportDocument();
    
    ParameterFields collectionParam = new ParameterFields();

    ParameterField paramDateFrom = new ParameterField();
    ParameterDiscreteValue paramDVDateFrom = new ParameterDiscreteValue();

    ParameterField paramDateTo = new ParameterField();
    ParameterDiscreteValue paramDVDateTo = new ParameterDiscreteValue();

    ParameterField paramcityid = new ParameterField();
    ParameterDiscreteValue paramDVcityid = new ParameterDiscreteValue();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["val"] != null)
        {
            string prms = Request.QueryString["val"];
            string[] prm = prms.Split(',');
            strFrom = prm[0];
            strTo = prm[1];
            strcityid = Convert.ToInt32(prm[2]);
            bindReport();
        }
    }

    private void bindReport()
    {
        paramDateFrom.Name = "@fromdate";
        paramDVDateFrom.Value = strFrom.ToString();
        paramDateFrom.CurrentValues.Add(paramDVDateFrom);

        paramDateTo.Name = "@todate";
        paramDVDateTo.Value = strTo.ToString();
        paramDateTo.CurrentValues.Add(paramDVDateTo);

        paramcityid.Name = "@cityid";
        paramDVcityid.Value = strcityid.ToString();
        paramcityid.CurrentValues.Add(paramDVcityid);

        collectionParam.Add(paramDateFrom);
        collectionParam.Add(paramDateTo);
        collectionParam.Add(paramcityid);

        CrystalReportViewer1.ParameterFieldInfo = collectionParam;
        VendorUtilizationSummary.Load(Server.MapPath("VendorReportDayWise.rpt"));
        VendorUtilizationSummary.SetDatabaseLogon(objAdmin.dbUser, objAdmin.dbPWD);
        CrystalReportViewer1.ReportSource = VendorUtilizationSummary;
        CrystalReportViewer1.DataBind();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        VendorUtilizationSummary.Close();
        VendorUtilizationSummary.Dispose();
    }
}
