﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using ReportingTool;

public partial class SalesReport_SalesRetails : System.Web.UI.Page
{
    private Sales objSales = null;
    clsAutomation objFleetTrackerSummary = new clsAutomation();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindYear(ddlFromYear);
            BindMonth(ddlFromMonth);
        }
    }

    private void BindMonth(DropDownList ddlMonth)
    {
        try
        {
            DataTable dtFromMonth = new DataTable();
            dtFromMonth = objFleetTrackerSummary.GetMonthName();
            ddlMonth.DataTextField = "monthName";
            ddlMonth.DataValueField = "number";
            ddlMonth.DataSource = dtFromMonth;
            ddlMonth.DataBind();
            ddlMonth.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlMonth.SelectedValue = System.DateTime.Now.Month.ToString();
        }
        catch (Exception)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }

    private void BindYear(DropDownList ddlYear)
    {
        try
        {
            for (int i = 2015; i <= System.DateTime.Now.Year; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.Items.Insert(0, new ListItem("--Select--", "0"));
            ddlYear.SelectedValue = System.DateTime.Now.Year.ToString();
        }
        catch (Exception)
        {

            throw new Exception("The method or operation is not implemented.");
        }
    }

    protected void bntGet_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
    }

    private StringBuilder BindExcelData()
    {
        StringBuilder strData = new StringBuilder();
        objSales = new Sales();
        DataSet dsDetails = objSales.GetRetailReportData(Convert.ToInt32(ddlFromMonth.SelectedValue), Convert.ToInt32(ddlFromYear.SelectedValue));
        DataView dv = new DataView(dsDetails.Tables[0]);
        try
        {

            if (dsDetails.Tables[0].Rows.Count > 0)
            {
               
                 strData.Append(" <html xmlns:o='urn:schemas-microsoft-com:office:office' ");
                 strData.Append(" xmlns:x='urn:schemas-microsoft-com:office:excel' ");
                 strData.Append(" xmlns='http://www.w3.org/TR/REC-html40'> ");
                 strData.Append(" <head> ");
                 strData.Append(" <meta http-equiv=Content-Type content='text/html; charset=windows-1252'> ");
                 strData.Append(" <meta name=ProgId content=Excel.Sheet> ");
                 strData.Append(" <meta name=Generator content='Microsoft Excel 14'> ");
                 strData.Append(" <link rel=File-List href='Page_files/filelist.xml'> ");
                 strData.Append(" <style id='Copy of BranchMonthWiseRevenueReport_1952_Styles'> ");
                 strData.Append(" <!--table ");
                 strData.Append(" {mso-displayed-decimal-separator:; ");
                 strData.Append(" mso-displayed-thousand-separator:;} ");
                 strData.Append(" .xl151952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:general; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:nowrap;} ");
                 strData.Append(" .xl651952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl661952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:none; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:.5pt solid black; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl671952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl681952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:'mmm\\-yy'; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:.5pt solid black; ");
                 strData.Append(" border-right:none; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:.5pt solid black; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl691952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:'mmm\\-yy'; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:.5pt solid black; ");
                 strData.Append(" border-right:none; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:none; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl701952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:'mmm\\-yy'; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:.5pt solid black; ");
                 strData.Append(" border-right:.5pt solid black; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:none; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl711952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:.5pt solid black; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:none; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl721952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:none; ");
                 strData.Append(" border-bottom:.5pt solid black; ");
                 strData.Append(" border-left:.5pt solid black; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl731952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:none; ");
                 strData.Append(" border-bottom:.5pt solid black; ");
                 strData.Append(" border-left:none; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl741952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:none; ");
                 strData.Append(" border-bottom:.5pt solid black; ");
                 strData.Append(" border-left:none; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl751952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:.5pt solid black; ");
                 strData.Append(" border-bottom:.5pt solid black; ");
                 strData.Append(" border-left:none; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl761952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:general; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border:.5pt solid black; ");
                 strData.Append(" background:#F2F2F2; ");
                 strData.Append(" mso-pattern:black none; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl771952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border:.5pt solid black; ");
                 strData.Append(" background:#F2F2F2; ");
                 strData.Append(" mso-pattern:black none; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl781952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:general; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border:.5pt solid black; ");
                 strData.Append(" background:#FFFF33; ");
                 strData.Append(" mso-pattern:black none; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl791952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border:.5pt solid black; ");
                 strData.Append(" background:#FFFF33; ");
                 strData.Append(" mso-pattern:black none; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl801952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:general; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border:.5pt solid black; ");
                 strData.Append(" background:#CCCCCC; ");
                 strData.Append(" mso-pattern:black none; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl811952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:right; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border:.5pt solid black; ");
                 strData.Append(" background:#CCCCCC; ");
                 strData.Append(" mso-pattern:black none; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl821952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:.5pt solid black; ");
                 strData.Append(" border-right:.5pt solid black; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:.5pt solid black; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl831952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:.5pt solid black; ");
                 strData.Append(" border-bottom:none; ");
                 strData.Append(" border-left:.5pt solid black; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" .xl841952 ");
                 strData.Append(" {padding-top:1px; ");
                 strData.Append(" padding-right:1px; ");
                 strData.Append(" padding-left:1px; ");
                 strData.Append(" mso-ignore:padding; ");
                 strData.Append(" color:black; ");
                 strData.Append(" font-size:11.0pt; ");
                 strData.Append(" font-weight:400; ");
                 strData.Append(" font-style:normal; ");
                 strData.Append(" text-decoration:none; ");
                 strData.Append(" font-family:Calibri, sans-serif; ");
                 strData.Append(" mso-font-charset:0; ");
                 strData.Append(" mso-number-format:General; ");
                 strData.Append(" text-align:center; ");
                 strData.Append(" vertical-align:bottom; ");
                 strData.Append(" border-top:none; ");
                 strData.Append(" border-right:.5pt solid black; ");
                 strData.Append(" border-bottom:.5pt solid black; ");
                 strData.Append(" border-left:.5pt solid black; ");
                 strData.Append(" mso-background-source:auto; ");
                 strData.Append(" mso-pattern:auto; ");
                 strData.Append(" white-space:normal;} ");
                 strData.Append(" --> ");
                 strData.Append(" </style> ");
                 strData.Append(" </head> ");
                 strData.Append(" <body> ");
                 strData.Append(" <!--[if !excel]>&nbsp;&nbsp;<![endif]--> ");
                 strData.Append(" <!--The following information was generated by Microsoft Excel's Publish as Web ");
                 strData.Append(" Page wizard.--> ");
                 strData.Append(" <!--If the same item is republished from Excel, all information between the DIV ");
                 strData.Append(" tags will be replaced.--> ");
                 strData.Append(" <!-----------------------------> ");
                 strData.Append(" <!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --> ");
                 strData.Append(" <!-----------------------------> ");
                 strData.Append(" <div id='Copy of BranchMonthWiseRevenueReport_1952' align=center ");
                 strData.Append(" x:publishsource='Excel'> ");
                 strData.Append(" <table border=0 cellpadding=0 cellspacing=0 width=612 style='border-collapse: ");
                 strData.Append("  collapse;table-layout:fixed;width:459pt'> ");
                 strData.Append(" <col width=215 style='mso-width-source:userset;mso-width-alt:7862;width:161pt'> ");
                 strData.Append(" <col width=36 style='mso-width-source:userset;mso-width-alt:1316;width:27pt'> ");
                 strData.Append(" <col width=62 style='mso-width-source:userset;mso-width-alt:2267;width:47pt'> ");
                 strData.Append(" <col width=67 style='mso-width-source:userset;mso-width-alt:2450;width:50pt'> ");
                 strData.Append(" <col width=108 style='mso-width-source:userset;mso-width-alt:3949;width:81pt'> ");
                 strData.Append(" <col width=124 style='mso-width-source:userset;mso-width-alt:4534;width:93pt'> ");
                 strData.Append(" <tr height=20 style='mso-height-source:userset;height:15.0pt'> ");
                 strData.Append(" <td rowspan=3 height=60 class=xl821952 width=215 style='border-bottom:.5pt solid black; ");
                 strData.Append(" height:45.0pt;width:161pt'>Branch</td> ");
                 strData.Append(" <td colspan=5 class=xl681952 width=397 style='border-right:.5pt solid black; ");
                 strData.Append(" border-left:none;width:298pt'>" + ddlFromMonth.SelectedItem.Text.ToString() + "-" + ddlFromYear.SelectedValue.ToString() + "</td> ");
                 strData.Append(" </tr> ");
                 strData.Append(" <tr height=20 style='height:15.0pt'> ");
                 strData.Append(" <td rowspan=2 height=40 class=xl661952 width=36 style='border-bottom:.5pt solid black; ");
                 strData.Append(" height:30.0pt;width:27pt'>Duty</td> ");
                 strData.Append(" <td class=xl651952 width=62 style='width:47pt'>Revenue</td> ");
                 strData.Append(" <td rowspan=2 class=xl651952 width=67 style='border-bottom:.5pt solid black; ");
                 strData.Append(" width:50pt'>Budgeted</td> ");
                 strData.Append(" <td rowspan=2 class=xl671952 width=108 style='border-bottom:.5pt solid black; ");
                 strData.Append(" width:81pt'>Expected Duties</td> ");
                 strData.Append(" <td rowspan=2 class=xl711952 width=124 style='border-bottom:.5pt solid black; ");
                 strData.Append(" width:93pt'>Expected Revenue</td> ");
                 strData.Append(" <!--<td class='redCopy' align='center'>Total (In Lacs)</td>--> ");
                 strData.Append(" </tr> ");
                 strData.Append(" <tr height=20 style='height:15.0pt'> ");
                 strData.Append(" <td height=20 class=xl731952 width=62 style='height:15.0pt;width:47pt'>(in ");
                 strData.Append(" Lacs)</td> ");
                 strData.Append(" </tr> ");

                 foreach (DataRow dr in dsDetails.Tables[0].Rows)
                 {

                     if (dr["CityName"].ToString().Contains("Total"))
                     {
                         strData.Append(" <tr height=20 style='height:15.0pt'> ");
                         strData.Append(" <td height=20 class=xl781952 width=215 style='height:15.0pt;border-top:none; ");
                         strData.Append(" width:161pt'>" + dr["CityName"].ToString() + "</td> ");
                         strData.Append(" <td class=xl791952 width=36 style='border-top:none;border-left:none; ");
                         strData.Append(" width:27pt'>" + dr["NoOfDuty"].ToString() + "</td> ");
                         strData.Append(" <td class=xl791952 width=62 style='border-top:none;border-left:none; ");
                         strData.Append(" width:47pt'>" + dr["Revenue"].ToString() + "</td> ");
                         strData.Append(" <td class=xl791952 width=67 style='border-top:none;border-left:none; ");
                         strData.Append(" width:50pt'>" + dr["target"].ToString() + "</td> ");
                         strData.Append(" <td class=xl791952 width=108 style='border-top:none;border-left:none; ");
                         strData.Append(" width:81pt'>" + dr["ExpectedDuty"].ToString() + "</td> ");
                         strData.Append(" <td class=xl791952 width=124 style='border-top:none;border-left:none; ");
                         strData.Append(" width:93pt'>" + dr["ExpectedRevenue"].ToString() + "</td> ");
                         strData.Append(" </tr> ");
                     }
                     else
                     {
                         strData.Append(" <tr height=20 style='height:15.0pt'> ");
                         strData.Append(" <td height=20 class=xl761952 width=215 style='height:15.0pt;border-top:none; ");
                         strData.Append(" width:161pt'>" + dr["CityName"].ToString() + "</td> ");
                         strData.Append(" <td class=xl771952 width=36 style='border-top:none;border-left:none; ");
                         strData.Append(" width:27pt'>" + dr["NoOfDuty"].ToString() + "</td> ");
                         strData.Append(" <td class=xl771952 width=62 style='border-top:none;border-left:none; ");
                         strData.Append(" width:47pt'>" + dr["Revenue"].ToString() + "</td> ");
                         strData.Append(" <td class=xl771952 width=67 style='border-top:none;border-left:none; ");
                         strData.Append(" width:50pt'>" + dr["target"].ToString() + "</td> ");
                         strData.Append(" <td class=xl771952 width=108 style='border-top:none;border-left:none; ");
                         strData.Append(" width:81pt'>" + dr["ExpectedDuty"].ToString() + "</td> ");
                         strData.Append(" <td class=xl771952 width=124 style='border-top:none;border-left:none; ");
                         strData.Append(" width:93pt'>" + dr["ExpectedRevenue"].ToString() + "</td> ");
                         strData.Append(" </tr> ");
                     }
                 }

                /*
                
                 strData.Append(" <tr height=20 style='height:15.0pt'> ");
                 strData.Append(" <td height=20 class=xl801952 width=215 style='height:15.0pt;border-top:none; ");
                 strData.Append(" width:161pt'>Total</td> ");
                 strData.Append(" <td class=xl811952 width=36 style='border-top:none;border-left:none; ");
                 strData.Append(" width:27pt'>1358</td> ");
                 strData.Append(" <td class=xl811952 width=62 style='border-top:none;border-left:none; ");
                 strData.Append(" width:47pt'>38.7</td> ");
                 strData.Append(" <td class=xl811952 width=67 style='border-top:none;border-left:none; ");
                 strData.Append(" width:50pt'>0</td> ");
                 strData.Append(" <td class=xl811952 width=108 style='border-top:none;border-left:none; ");
                 strData.Append(" width:81pt'>1358</td> ");
                 strData.Append(" <td class=xl811952 width=124 style='border-top:none;border-left:none; ");
                 strData.Append(" width:93pt'>38.7</td> ");
                 strData.Append(" </tr> ");
                 */ 
                 strData.Append(" <![if supportMisalignedColumns]> ");
                 strData.Append(" <tr height=0 style='display:none'> ");
                 strData.Append(" <td width=215 style='width:161pt'></td> ");
                 strData.Append(" <td width=36 style='width:27pt'></td> ");
                 strData.Append(" <td width=62 style='width:47pt'></td> ");
                 strData.Append(" <td width=67 style='width:50pt'></td> ");
                 strData.Append(" <td width=108 style='width:81pt'></td> ");
                 strData.Append(" <td width=124 style='width:93pt'></td> ");
                 strData.Append(" </tr> ");
                 strData.Append(" <![endif]> ");
                 strData.Append(" </table> ");
                 strData.Append(" </div> ");
                 strData.Append(" <!-----------------------------> ");
                 strData.Append(" <!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD--> ");
                 strData.Append(" <!-----------------------------> ");
                 strData.Append(" </body> ");
                 strData.Append(" </html> ");


            }

        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.Text = Ex.Message;
        }
        return strData;
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        StringBuilder strData = new StringBuilder();
        strData = BindExcelData();
        ltlSummary.Text = strData.ToString();
        Response.Clear();
        Response.Buffer = true;
        string strFileName = "BranchMonthWiseRevenueReport";
        strFileName = strFileName.Replace("(", "");
        strFileName = strFileName.Replace(")", "");
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName.ToString() + ".xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        ltlSummary.RenderControl(hw);
        Response.Output.Write(sw.ToString());
        Response.End();
    }
}