using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using ReportingTool;

public partial class SalesExceptionReport : System.Web.UI.Page
{
    clsAdmin objAdmin = new clsAdmin();
    //string sFromDate ;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
            DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(null);
            DateTime month = Convert.ToDateTime("1/1/2000");
            int k=0;


            //for (int i = 0; i < 12; i++)
            //{
            //    ListItem list = new ListItem();
            //    list.Text = "Select";
            //    list.Value = "0";
            //    ddlMonth.Items.Add(list);
            //    k++;
            //    //list.Text = "Select";
            //    //list.Value = "0";
            //    //ddlMonth.Items.Add(list);
            //    DateTime NextMont = month.AddMonths(i);
            //    list.Text = NextMont.ToString("MMMM");
            //    list.Value = NextMont.Month.ToString();
            //    ddlMonth.Items.Add(list);
            //}
            //ddlMonth.Items.Insert(0, "Select");

            int cYear;
            cYear = System.DateTime.Now.Year;


            for (int i = 2007; i <= cYear; i++)
            {
                ddlYear.Items.Add((i).ToString());
            }
           ddlYear.Items.Insert(0, "--Select--");
                 
            
        }
        btnSubmit.Attributes.Add("onclick", "return validate();");
        btnExport.Attributes.Add("onclick", "return validate();");
       
    }

    protected void BindCity()
    {
        ddlCityName.DataTextField = "CityName";
        ddlCityName.DataValueField = "CityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetLocations_UserAccessWise(Convert.ToInt32(Session["UserID"]));
        //dsLocation = objAdmin.GetLocations_UserAccessWise(173);
        ddlCityName.DataSource = dsLocation;
        ddlCityName.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //sFromDate = txtFrom.Value.Trim();
        FillGrid();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
        string Monthname = ddlMonth.SelectedItem.Text;
        int sMonth = Convert.ToInt32(ddlMonth.SelectedValue); ;
        int sYear = Convert.ToInt32(ddlYear.SelectedValue);
        string strSelDate = 01 + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue;
        DateTime enteredDate = Convert.ToDateTime(strSelDate, dtfi);
         
        
        


        DateTime PreDate = new DateTime(sYear, sMonth, 1).AddMonths(-1);

        string PrevMonthname = GetMonthName(PreDate);
        string PreDate1 = GetMonthName(PreDate);
        int PPrevsYear = Convert.ToDateTime(PreDate).Year;
        int PPrevsMonth = Convert.ToDateTime(PreDate).Month;

        //DateTime.Parse(PreDate).Month;
        DateTime PrevPreDate = new DateTime(PPrevsYear, PPrevsMonth, 1).AddMonths(-1);
        string PPrevsMonthName = GetMonthName(PrevPreDate);        
       
        
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable dtEx = new DataTable();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        Response.AppendHeader("content-disposition", "attachment;filename=SalesExceptionalReport.xls");

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";

        this.EnableViewState = false;
        Response.Write("\r\n");

        dtEx = objAdmin.GetSalesException(sMonth,sYear, cityid);

        if (dtEx.Rows.Count > 0)
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            int[] iColumns = { 1, 15, 14, 13, 12, 2, 4, 6, 8, 10 }; //, 6, 10 };
            for (int i = 0; i < dtEx.Rows.Count; i++)
            {
                if (i == 0)
                {
                    Response.Write("<tr>");
                    Response.Write("<td align='left'><b></b></td>");                    
                    Response.Write("<td align='center' colspan='2'><b>"+PPrevsMonthName+"</b></td>");
                    Response.Write("<td align='center' colspan='2'><b>"+PrevMonthname+"</b></td>");
                    Response.Write("<td align='center' colspan='4'><b>"+Monthname+"</b></td>");
                    Response.Write("<td align='center' colspan='1'><b></b></td>");
                    Response.Write("</tr>");

                    Response.Write("<tr>");
                    Response.Write("<td align='left'><b></b></td>");
                    Response.Write("<td align='center' colspan='1'><b></b></td>");
                    Response.Write("<td align='center' colspan='1'><b></b></td>");
                    Response.Write("<td align='center' colspan='1'><b></b></td>");
                    Response.Write("<td align='center' colspan='1'><b></b></td>");
                    Response.Write("<td align='center' colspan='1'><b>WK 1</b></td>");
                    Response.Write("<td align='center' colspan='1'><b>WK 2</b></td>");
                    Response.Write("<td align='center' colspan='1'><b>WK 3</b></td>");
                    Response.Write("<td align='center' colspan='1'><b>WK 4</b></td>");
                    Response.Write("<td align='center' colspan='1'><b></b></td>");
                    Response.Write("</tr>");


                    Response.Write("<tr>");
                    for (int j = 0; j < iColumns.Length; j++)
                    {
                        if (dtEx.Columns[iColumns[j]].Caption.ToString() == "Clientconame")
                        {
                            Response.Write("<td align='left'><b>Top 10 Clients</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PrevPrevDuties")
                        {
                            Response.Write("<td align='left'><b>No. of Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PrePrevAmount")
                        {
                            Response.Write("<td align='left'><b>Amt</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PrevDuties")
                        {
                            Response.Write("<td align='left'><b>No. of Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "PrevAmount")
                        {
                            Response.Write("<td align='left'><b>Amt</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "FirstWeekDuties")
                        {
                            Response.Write("<td align='left'><b>No. of Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "SecondWeekDuties")
                        {
                            Response.Write("<td align='left'><b>No. of Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ThirdWeekDuties")
                        {
                            Response.Write("<td align='left'><b>No. of Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "ForthWeekDuties")
                        {
                            Response.Write("<td align='left'><b>No. of Duties</b></td>");
                        }
                        else if (dtEx.Columns[iColumns[j]].Caption.ToString() == "TotalDuties")
                        {
                            Response.Write("<td align='left'><b>Total of DS of the current Month</b></td>");
                        }
                      
                       
                    }
                    Response.Write("</tr>");
                }
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='left'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.End();
        }
        else
        {
            Response.Write("<table border = 1 align = 'center'  width = 100%> ");
            Response.Write("<td align='center'><b>No Record Found</b></td>");
            Response.Write("</table>");
            Response.End();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        int sMonth = Convert.ToInt32(ddlMonth.SelectedValue); ;
        int sYear = Convert.ToInt32(ddlYear.SelectedValue);       
        int cityid;
        cityid = Convert.ToInt16(ddlCityName.SelectedValue);

        DataTable GetBatchDetail = new DataTable();
        GetBatchDetail = objAdmin.GetSalesException(sMonth,sYear, cityid);

        if (GetBatchDetail.Rows.Count > 0)
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = GetBatchDetail;
            GridView1.DataBind();
        }
    }

    public static string GetMonthName(DateTime givenDate)
    {
        DateTimeFormatInfo formatInfoinfo = new DateTimeFormatInfo();
        string[] monthName = formatInfoinfo.MonthNames;
        return monthName[givenDate.Month - 1];
    }

   


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
       
        //string sFromDate = txtFrom.Value;
        //DateTime enteredDate = Convert.ToDateTime(sFromDate, dtfi);
        string Monthname = ddlMonth.SelectedItem.Text;
        int sMonth =Convert.ToInt32( ddlMonth.SelectedValue); ;
        int sYear = Convert.ToInt32( ddlYear.SelectedValue);        
        string strSelDate=  01 +  "/" +   ddlMonth.SelectedValue + "/"+ ddlYear.SelectedValue;
        DateTime enteredDate = Convert.ToDateTime(strSelDate, dtfi);
       
        
        
        
        DateTime PreDate = new DateTime(sYear, sMonth, 1).AddMonths(-1);

        string PrevMonthname = GetMonthName(PreDate);
        string PreDate1 = GetMonthName(PreDate);
        int PPrevsYear = Convert.ToDateTime(PreDate).Year;
        int PPrevsMonth = Convert.ToDateTime(PreDate).Month;


        DateTime PrevPreDate = new DateTime(PPrevsYear, PPrevsMonth, 1).AddMonths(-1);
        string PPrevsMonthName = GetMonthName(PrevPreDate);



        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell HeaderCell = new TableCell();

            HeaderCell.ColumnSpan = 1;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = PPrevsMonthName.ToString();
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = PrevMonthname.ToString();
            HeaderCell.ColumnSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = Monthname.ToString();
            HeaderCell.ColumnSpan = 4;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.ColumnSpan = 1;
            //HeaderCell.RowSpan = 2;
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.Text = "";
            HeaderGridRow.Cells.Add(HeaderCell);

            GridView1.Controls[0].Controls.AddAt(0, HeaderGridRow);

            //*******************************************************************************************************************//
            //To Add Second Row


            GridView HeaderGrid1 = (GridView)sender;
            GridViewRow HeaderGridRow1 = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);

            TableCell HeaderCell1 = new TableCell();

            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell1.Text = "";
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell1.Text = "";
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell1.Text = "";
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.Text = "";
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.Text = "";
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.Text = "WK 1";
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.Text = "WK 2";
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.Text = "WK 3";
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow1.Cells.Add(HeaderCell1);

            HeaderCell1 = new TableCell();
            HeaderCell1.Text = "WK 4";
            HeaderCell1.ColumnSpan = 1;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderGridRow1.Cells.Add(HeaderCell1);


            HeaderCell1 = new TableCell();
            HeaderCell1.ColumnSpan = 1;
            //HeaderCell.RowSpan = 2;
            HeaderCell1.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell1.Text = "";
            HeaderGridRow1.Cells.Add(HeaderCell1);


            GridView1.Controls[0].Controls.AddAt(1, HeaderGridRow1);
        }
    }
}
