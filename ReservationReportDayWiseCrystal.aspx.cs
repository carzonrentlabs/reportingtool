using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using ReportingTool;

public partial class ReservationReportDayWiseCrystal : System.Web.UI.Page
{

    int strcityid;
    int strdays;

    clsAdmin objAdmin = new clsAdmin();

    ReportDocument ReservationDayWise  = new ReportDocument();

    ParameterFields collectionParam = new ParameterFields();

    ParameterField paramdays = new ParameterField();
    ParameterDiscreteValue paramDVdays = new ParameterDiscreteValue();


    ParameterField paramcityid = new ParameterField();
    ParameterDiscreteValue paramDVcityid = new ParameterDiscreteValue();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["val"] != null)
        {
            string prms = Request.QueryString["val"];
            string[] prm = prms.Split(',');
            strdays = Convert.ToInt32(prm[0]);           
            strcityid = Convert.ToInt32(prm[1]);
            bindReport();
        }
    }
    private void bindReport()
    {
        paramdays.Name = "@NoOfDays";
        paramDVdays.Value = strdays;
        paramdays.CurrentValues.Add(paramDVdays);       

        paramcityid.Name = "@cityid";
        paramDVcityid.Value = strcityid.ToString();
        paramcityid.CurrentValues.Add(paramDVcityid);

        collectionParam.Add(paramdays);       
        collectionParam.Add(paramcityid);

        CrystalReportViewer1.ParameterFieldInfo = collectionParam;
        ReservationDayWise.Load(Server.MapPath("ReservationDaywise.rpt"));
        ReservationDayWise.SetDatabaseLogon(objAdmin.dbUser, objAdmin.dbPWD);
        CrystalReportViewer1.ReportSource = ReservationDayWise;
        CrystalReportViewer1.DataBind();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        ReservationDayWise.Close();
        ReservationDayWise.Dispose();
    }
}
