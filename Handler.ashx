<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
public class Handler : IHttpHandler {
    
    
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/plain";
        int cabId = Convert.ToInt32(context.Request.QueryString["cabId"].ToString());
       
        DataSet dsNotAvailableDetails = new DataSet();
        CorDrive objCordrive = new CorDrive();
        dsNotAvailableDetails = objCordrive.GetNotAvailableDetails(cabId);
        if (dsNotAvailableDetails.Tables[0].Rows.Count > 0)
        {
            string ConcatenateString = dsNotAvailableDetails.Tables[0].Rows[0]["CarId"].ToString() + "#" + dsNotAvailableDetails.Tables[0].Rows[0]["ReasonType"].ToString() + "#" + dsNotAvailableDetails.Tables[0].Rows[0]["FromDate"].ToString() + "#" + dsNotAvailableDetails.Tables[0].Rows[0]["ToDate"].ToString() + "#" + dsNotAvailableDetails.Tables[0].Rows[0]["Remarks"].ToString();
            context.Response.Write(ConcatenateString);
            context.Response.End();
        }
        else {
            context.Response.Write("Record Not Found");
            context.Response.End();
        }
              
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}