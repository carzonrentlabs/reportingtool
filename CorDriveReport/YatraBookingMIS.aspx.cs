﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;
using ExcelUtil;

public partial class CorDriveReport_YatraBookingMIS : System.Web.UI.Page
{
    private CorDrive objCordrive = null;
    DataSet dsExportToExcel = new DataSet();
    string OriginalCode = "YATRA";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Text = string.Empty;
    }
    protected void bntGet_Click(object sender, EventArgs e)
    {
        objCordrive = new CorDrive();
        System.Threading.Thread.Sleep(5000);
        DataSet dsYatraMIS = new DataSet();
        try
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
                return;
            }
            else
            {
                dsYatraMIS = objCordrive.GetYatraMIS(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), OriginalCode);
                if (dsYatraMIS.Tables[0].Rows.Count > 0)
                {
                    grdYatraMIS.DataSource = dsYatraMIS.Tables[0];
                    grdYatraMIS.DataBind();
                }
                else
                {
                    grdYatraMIS.DataSource = null;
                    grdYatraMIS.DataBind();
                    lblMessage.Text = "Record not available.";
                    lblMessage.Visible = true;
                    lblMessage.ForeColor = Color.Red;
                }
            }
        }
        catch (Exception Ex)
        {
            lblMessage.Visible = true;
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = Ex.Message;
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtFromDate.Text.ToString()) || string.IsNullOrEmpty(txtFromDate.Text.ToString()))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "javascript:alert('Pickup from date or pickup to date can not empty.')", true);
            return;
        }
        else
        {
            objCordrive = new CorDrive();
            dsExportToExcel = objCordrive.GetYatraMIS(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), OriginalCode);
            WorkbookEngine.ExportDataSetToExcel(dsExportToExcel, "YatraBookingMIS.xls");
        }
    }
}