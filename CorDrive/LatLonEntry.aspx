﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LatLonEntry.aspx.cs" Inherits="CorDrive_LatLonEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPage" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:ScriptManagerProxy runat="server" ID="scriptManager">
        <Scripts>
            <asp:ScriptReference Path="http://maps.googleapis.com/maps/api/js?sensor=true&client=gme-carzonrentindiapvt&v=3.20&libraries=places" />
            <asp:ScriptReference Path="../JQuery/GoogleMapAPIWrapper.js" />
            <asp:ScriptReference Path="../JQuery/jquery-1.11.3.min.js" />
            <asp:ScriptReference Path="../JQuery/bootstrap.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <script src="../JQuery/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../JQuery/ui.core.js" type="text/javascript"></script>
    <script src="../JQuery/ui.datepicker.js" type="text/javascript"></script>
    <script src="../ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <link href="../CSSReveal/reveal.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function pageLoad(sender, args) {
            $("#<%=txtFromDate.ClientID%>").datepicker();
            $("#<%=txtToDate.ClientID%>").datepicker();

            $('#city_location_text').keydown(function (e) {
                if (e.which == 13 && $('.input-large:visible').length) return false;
            });


            $('a[data-reveal-id]').live('click', function (e) {
                e.preventDefault();
                var id = $(this).attr("id")
                var bookingId = $("#" + id).text()
                $("#<%=txtBookingId.ClientID%>").val(bookingId);
                $("#city_location_text").val("");
                $("#<%=city_location_lat.ClientID%>").val("");
                $("#<%=city_location_long.ClientID%>").val("");
                var modalLocation = $(this).attr('data-reveal-id');
                $('#' + modalLocation).reveal($(this).data());
            });



            $("#<%=bntGet.ClientID%>").click(function () {
                if (document.getElementById('<%=txtFromDate.ClientID %>').value == "") {
                    alert("Please Select the From Date");
                    document.getElementById('<%=txtFromDate.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=txtToDate.ClientID %>').value == "") {
                    alert("Please Select the To Date");
                    document.getElementById('<%=txtToDate.ClientID %>').focus();
                    return false;
                }
                else if (Date.parse($("#<%=txtFromDate.ClientID%>").val()) > Date.parse($("#<%=txtToDate.ClientID%>").val())) {
                    alert("Pickup From Date should not be greather than Pickup To date ")
                }
                else {
                    ShowProgress();
                }
            });

            $("#<%=bntSubmit.ClientID%>").click(function () {
                if ($("#<%=city_location_lat.ClientID%>").val() == "" || $("#<%=city_location_long.ClientID%>").val() == "") {
                    alert("Please select guest location");
                    return false;
                }
                else {
                    $("#<%=hdCityLocationText.ClientID%>").val($("#city_location_text").val())
                    return true;
                }
            });


            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                                            sURLVariables = sPageURL.split('&'),
                                            sParameterName,
                                            i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };


            var office_location_prediction_autocomplete = null;
            function officeLocationAutoCompleteHandler() {
                if (office_location_prediction_autocomplete != null) {
                    var place = office_location_prediction_autocomplete.getPlace();
                    $("#<%=city_location_lat.ClientID%>").val(place.geometry.location.lat());
                    $("#<%=city_location_long.ClientID%>").val(place.geometry.location.lng());
                }
            }

            function initializePlaceAutoComplete(element_id, callback_function, element_obj) {
                var input = document.getElementById(element_id);
                if (element_obj != null) {
                    input = element_obj;
                }
                if (input != null) {
                    var options = {};
                }
                options['componentRestrictions'] = { country: 'in' };
                if (google == null || google.maps == null) {
                    loading();
                    alert("OOPS. There was a network error in loading this page. Please reload");
                    return;
                }
                office_location_prediction_autocomplete = new google.maps.places.Autocomplete(input, options);
                google.maps.event.addListener(office_location_prediction_autocomplete, "place_changed", callback_function, false);

            }

            $(function () {
                initializePlaceAutoComplete("city_location_text", officeLocationAutoCompleteHandler);
                // getCities();
            });

            function parseJSON(obj) {
                if (obj.constructor === "test".constructor) {
                    return JSON.parse(obj);
                }
                return obj;
            }

            $("#bntCancel").click(function () {
                $(".close-reveal-modal").trigger("click");
            });
        }

        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

    </script>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 60%">
            <tr>
                <td colspan="5" align="center">
                    <b>Enter Guest Pick-up Location</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMessate" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Pickup From Date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Pickup To date</b>&nbsp;&nbsp;
                    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>&nbsp;&nbsp;
                </td>
                <td>
                    <asp:Button ID="bntGet" runat="Server" Text="Get It" OnClick="bntGet_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div>
            <asp:GridView ID="grvLatLon" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84"
                BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2"
                AllowPaging="true" PageSize="20" OnRowCancelingEdit="grvLatLon_RowCancelingEdit"
                OnRowEditing="grvLatLon_RowEditing" OnPageIndexChanging="grvLatLon_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="SNo.">
                        <ItemTemplate>
                            <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.DataItemIndex+1  %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--  <asp:BoundField HeaderText="BookingId" DataField="BookingID" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>--%>
                    <asp:BoundField HeaderText="Company Name" DataField="ClientCoName" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Pickup City" DataField="CityName" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Guest Name" DataField="GuestName" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Guest Mobile No" DataField="GuestMobileNo" HeaderStyle-Wrap="false">
                        <HeaderStyle Wrap="False"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Remark" DataField="Remarks" HeaderStyle-Wrap="true">
                        <HeaderStyle Wrap="True"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Pickup Address" DataField="PickUpAdd" HeaderStyle-Wrap="true">
                        <HeaderStyle Wrap="True"></HeaderStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Update Pickup Location">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkBGetBookingId" runat="server" class="big-link" data-reveal-id="myModal"
                                Text='<%#Eval("BookingID") %>' ForeColor="Red" CommandName="Edit"></asp:LinkButton>
                            <%--<a href="#" class="big-link" data-reveal-id="myModal" runat ="server" ><font color="red">Insert Location</color></a>
                             <input class="input-large" type="text" id="city_location_text" placeholder="Type Area...."/>
                            <input id="city_location_lat" runat="server" type="hidden" />
                            <input id="city_location_long" runat="server" type="hidden" />
                            <input id="hdGuestPickupLocation" runat="server" type="hidden" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            </asp:GridView>
        </div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="../images/loader.gif" alt="" />
        </div>
        <div style="width: 400px" id="myModal" class="reveal-modal">
            <fieldset style="border-color: Green">
                <legend><b>Guest Pick-up Location Lat Lon</b></legend>
                <table>
                    <tr>
                        <td style="height: 22px">
                            BookingID: &nbsp;
                            <input type="text" id="txtBookingId" runat="server" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 22px">
                            Guest Pick-up Location: &nbsp;
                            <input class="input-large" type="text" id="city_location_text" placeholder="Type Area...."
                                value="" size="60" />
                            <asp:HiddenField ID="hdCityLocationText" runat ="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input id="city_location_lat" runat="server" type="hidden" />
                            <input id="city_location_long" runat="server" type="hidden" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <%-- <input type="button" id="ResetPasswordButton" value="Reset Password" class="button_quote"
                                runat="server" />--%>
                            <asp:Button ID="bntSubmit" runat="server" Text="Submit" OnClick="bntSubmit_Click" />
                            &nbsp; &nbsp; &nbsp;
                            <input type="button" id="bntCancel" value="Cancel" class="button_quote" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>
        </div>
    </center>
</asp:Content>
