﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ReportingTool;

/// <summary>
/// Summary description for clsModifyCar
/// </summary>
public class clsModifyCar
{
	public clsModifyCar()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet GetModifycarGroupDetails(string RegnNo)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@RegnNo", RegnNo);      
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetModifycarGroupDetails", ObjParam);
    }
    public int ModifyCarGroup(string RegnNo, string NewCarGroup, string OldcarGroup, int UpdatedBy, int ValidTill, string Comments)
    {
        SqlParameter[] ObjparamUser = new SqlParameter[6];
        ObjparamUser[0] = new SqlParameter("@RegnNo", RegnNo);
        ObjparamUser[1] = new SqlParameter("@NewCarGroup", NewCarGroup);
        ObjparamUser[2] = new SqlParameter("@OldcarGroup", OldcarGroup);
        ObjparamUser[3] = new SqlParameter("@UpdatedBy", UpdatedBy);
        ObjparamUser[4] = new SqlParameter("@ValidTill", ValidTill);
        ObjparamUser[5] = new SqlParameter("@Comments", Comments);
       return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_ModifyCarGroup", ObjparamUser);
    }
}