using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReportingTool;

public partial class VDPReportDayWiseCrystal : System.Web.UI.Page
{
    int days;
    int strcityid;

    clsAdmin objAdmin = new clsAdmin();

    ReportDocument VendorUtilizationSummary = new ReportDocument();

    ParameterFields collectionParam = new ParameterFields();

    ParameterField paramdays = new ParameterField();
    ParameterDiscreteValue paramDVdays = new ParameterDiscreteValue();

    ParameterField paramcityid = new ParameterField();
    ParameterDiscreteValue paramDVcityid = new ParameterDiscreteValue();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] != null && Convert.ToInt32(Session["Userid"]) > 0)
        {
            if (Request.QueryString["val"] != null)
            {
                string prms = Request.QueryString["val"];
                string[] prm = prms.Split(',');
                days = Convert.ToInt32(prm[0]);
                strcityid = Convert.ToInt32(prm[1]);
                bindReport();
            }
        }
        else
        {
            Response.Redirect("Logout.aspx");
        }
    }

    private void bindReport()
    {


        paramcityid.Name = "@cityid";
        paramDVcityid.Value = strcityid.ToString();
        paramcityid.CurrentValues.Add(paramDVcityid);

        paramdays.Name = "@NoOfDays";
        paramDVdays.Value = days;
        paramdays.CurrentValues.Add(paramDVdays);

        collectionParam.Add(paramdays);
        collectionParam.Add(paramcityid);

        CrystalReportViewer1.ParameterFieldInfo = collectionParam;
        VendorUtilizationSummary.Load(Server.MapPath("VDPReportDayWise.rpt"));
        VendorUtilizationSummary.SetDatabaseLogon(objAdmin.dbUser, objAdmin.dbPWD);
        CrystalReportViewer1.ReportSource = VendorUtilizationSummary;
        CrystalReportViewer1.DataBind();
    }
    protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
    {
        VendorUtilizationSummary.Close();
        VendorUtilizationSummary.Dispose();
    }
}