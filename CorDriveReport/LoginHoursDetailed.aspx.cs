﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class CorDriveReport_LoginHoursDetailed : System.Web.UI.Page
{
    CorDrive objCordrive = new CorDrive();
    RentMeterService.wsRentmeter _objService = new RentMeterService.wsRentmeter();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrachDropDownlist();
        }
        bntGet.Attributes.Add("Onclick", "return validate()");
        bntExprot.Attributes.Add("Onclick", "return validate()");
    }
    #region "Dropdownlist Bind Function"
    private void BindBrachDropDownlist()
    {
        try
        {
            DataSet dsLocation = new DataSet();
            dsLocation = objCordrive.GetLocations_UserAccessWise_Unit(Convert.ToInt32(Session["UserID"]));
            if (dsLocation != null)
            {
                if (dsLocation.Tables.Count > 0)
                {
                    ddlBranch.DataTextField = "Cityname";
                    ddlBranch.DataValueField = "CityId";
                    ddlBranch.DataSource = dsLocation;
                    ddlBranch.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    private void BindLoginHoursControl()
    {
        try
        {
            int _LoginStatus = 0, _cityId = 0, _count = 0, _v_count = 0;
            DateTime _fromdate, _todate;
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _LoginStatus = Convert.ToInt32(ddlLoginLogout.SelectedValue.ToString() == null || ddlLoginLogout.SelectedValue.ToString() == "" ? "0" : ddlLoginLogout.SelectedValue.ToString());
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            DataSet _objDs = new DataSet();
            _objDs = objCordrive.GetLoginHoursDetail(_fromdate, _todate, _cityId, _LoginStatus);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dvPaging.Visible = true;
                    _count = _objDs.Tables.Count;
                    PagedDataSource objPage_data = new PagedDataSource();
                    objPage_data.DataSource = _objDs.Tables[0].DefaultView;
                    objPage_data.AllowPaging = true;
                    objPage_data.PageSize = 50;
                    objPage_data.CurrentPageIndex = _PgNum;

                    _v_count = _count / objPage_data.PageSize;
                    if (_PgNum < 1)
                    {
                        lnkPrev.Visible = false;
                    }
                    else if (_PgNum > 0)
                    {
                        lnkPrev.Visible = true;
                    }
                    else if (_PgNum < _v_count)
                    {
                        lnkNext.Visible = true;
                    }
                    rptLoginHoursDetail.DataSource = objPage_data;
                    rptLoginHoursDetail.DataBind();
                }
                else
                {
                    dvPaging.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    #endregion

    protected void bntGet_Click(object sender, EventArgs e)
    {
        try
        {
            BindLoginHoursControl();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void bntExprot_Click(object sender, EventArgs e)
    {
        try
        {

            int _LoginStatus = 0, _cityId = 0;
            DateTime _fromdate, _todate;
            DataTable dt = new DataTable();
            string _current_Location = "";
            _fromdate = Convert.ToDateTime(txtFromDate.Text);
            _todate = Convert.ToDateTime(txtToDate.Text);
            _LoginStatus = Convert.ToInt32(ddlLoginLogout.SelectedValue.ToString() == null || ddlLoginLogout.SelectedValue.ToString() == "" ? "0" : ddlLoginLogout.SelectedValue.ToString());
            _cityId = Convert.ToInt32(ddlBranch.SelectedValue.ToString() == null || ddlBranch.SelectedValue.ToString() == "" ? "0" : ddlBranch.SelectedValue.ToString());
            DataSet _objDs = new DataSet();
            _objDs = objCordrive.GetLoginHoursDetail(_fromdate, _todate, _cityId, _LoginStatus);
            if (_objDs != null)
            {
                if (_objDs.Tables.Count > 0)
                {
                    dt = _objDs.Tables[0];
                }
            }

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    Response.ClearContent();
                    Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
                    Response.Clear();
                    Response.AppendHeader("content-disposition", "attachment;filename=CORDriveLoginHoursDetailed.xls");
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    this.EnableViewState = false;
                    Response.Write("\r\n");
                    Response.Write("<table border = '1' align = 'center'> ");
                    int[] iColumns = { 0, 1, 5, 6, 4, 2, 3, 7, 8, 9, 10, 12, 13 };
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            Response.Write("<tr>");
                            for (int j = 0; j < iColumns.Length; j++)
                            {
                                if (dt.Columns[iColumns[j]].Caption.ToString() == "Id")
                                {
                                    Response.Write("<td align='left'><b>S.No.</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DeviceId")
                                {
                                    Response.Write("<td align='left'><b>Registration No.</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "chauffuername")
                                {
                                    Response.Write("<td align='left'><b>Chauffeur Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "MobileNo")
                                {
                                    Response.Write("<td align='left'><b>Chauffeur Contact</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "CityName")
                                {
                                    Response.Write("<td align='left'><b>City Name</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "Login_Date")
                                {
                                    Response.Write("<td align='left'><b>Login Date</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "LogOut_date")
                                {
                                    Response.Write("<td align='left'><b>Logout Date</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "LoginStatus")
                                {
                                    Response.Write("<td align='left'><b>Current Status</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "LoginTime")
                                {
                                    Response.Write("<td align='left'><b>Hours Logged in</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "DutySlipStatus")
                                {
                                    Response.Write("<td align='left'><b>Duty Status</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "bookingid")
                                {
                                    Response.Write("<td align='left'><b>BookingID</b></td>");
                                }
                                else if (dt.Columns[iColumns[j]].Caption.ToString() == "cordriveversion")
                                {
                                    Response.Write("<td align='left'><b>Version No</b></td>");
                                }
                                else
                                {
                                    Response.Write("<td align='left'><b>Current Location</b></td>");
                                }
                            }
                            Response.Write("</tr>");
                        }
                        Response.Write("<tr>");
                        for (int j = 0; j < iColumns.Length; j++)
                        {

                            //For the Current Location Lati and Log index is 10
                            if (iColumns[j] == 10)
                            {
                                double _lat = Convert.ToDouble(dt.Rows[i]["Lat"].ToString());
                                double _lon = Convert.ToDouble(dt.Rows[i]["Lon"].ToString());

                                if (_lat != 0 && _lon != 0)
                                {
                                    _current_Location = _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());
                                }
                                else
                                {
                                    _current_Location = "";
                                }

                                Response.Write("<td align='left'>" + _current_Location.ToString() + "</td>");
                            }
                            else
                            {
                                Response.Write("<td align='left'>" + dt.Rows[i][iColumns[j]].ToString() + "</td>");
                            }

                        }
                        Response.Write("</tr>");
                    }
                    Response.Write("</table>");
                    Response.End();


                }
                else
                {

                    Response.Write("<table border = 1 align = 'center' width = '100%'>");
                    Response.Write("<td align='center'><b>No Record Found</b></td>");
                    Response.Write("</table>");
                    Response.End();

                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void rptLoginHoursDetail_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        double _lat = 0.00, _lon = 0.00;
        string _currentLocation = "";
        _objService = new RentMeterService.wsRentmeter();
        LinkButton lkbLink = (LinkButton)e.Item.FindControl("lnkView");
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label _lbl_CurrntLocation = (Label)e.Item.FindControl("lbl_CurrntLocation");
                _lat = (double)DataBinder.Eval(e.Item.DataItem, "lat");
                _lon = (double)DataBinder.Eval(e.Item.DataItem, "Lon");

                if (_lat != 0 && _lon != 0)
                {
                    _currentLocation = _objService.GetCurrentLocation_LatLon(_lat.ToString(), _lon.ToString());
                    lkbLink.Text = "View";
                }
                else
                {
                    _currentLocation = "";
                }

                _lbl_CurrntLocation.Text = _currentLocation;
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void lnkPrev_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum -= 1;
            BindLoginHoursControl();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        try
        {
            _PgNum += 1;
            BindLoginHoursControl();
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    public int _PgNum
    {
        get
        {
            if (ViewState["PageNum"] != null)
            {
                return Convert.ToInt32(ViewState["PageNum"].ToString());
            }
            else
            {
                return 0;
            }
        }
        set
        {
            ViewState["PageNum"] = value;
        }
    }
}